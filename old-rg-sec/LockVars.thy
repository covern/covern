(*
Author: Robert Sison
*)

section {* Lock Primitive Support *}

theory LockVars
imports Preliminaries
begin

subsection {* Syntax *}

type_synonym 'var Lock = "('var set) \<times> ('var set)"

text {*
  Variables are now either locks or ordinary program variables.
*}
datatype ('lock, 'var) Var = Lock 'lock | Var 'var

text {*
  Convert a full memory with locks into one with just program variables. We use this
  simpler type of memory to evaluate the simple program semantics.
*}
definition
  to_prog_mem :: "(('lock,'var) Var,'val) Mem \<Rightarrow> ('var,'val) Mem"
where
  "to_prog_mem m \<equiv> \<lambda>v. m (Var v)"

definition
  to_lock_mem :: "(('lock,'var) Var,'val) Mem \<Rightarrow> ('lock,'val) Mem"
where
  "to_lock_mem m \<equiv> \<lambda>l. m (Lock l)"

definition
  to_full_mem :: "('var,'val) Mem \<Rightarrow> ('lock,'val) Mem \<Rightarrow> (('lock,'var) Var, 'val) Mem"
where
  "to_full_mem vm lm \<equiv> \<lambda>v. case v of (Var p) \<Rightarrow> vm p | (Lock l) \<Rightarrow> lm l"

lemma fullmemI:
  "\<lbrakk>to_prog_mem m = to_prog_mem m'; to_lock_mem m = to_lock_mem m'\<rbrakk> \<Longrightarrow> m = m'"
  using to_prog_mem_def to_lock_mem_def
  by (metis Var.exhaust ext)

lemma fullmemD:
  "m = m' \<Longrightarrow> to_prog_mem m = to_prog_mem m' \<and> to_lock_mem m = to_lock_mem m'"
  by metis

definition
  to_prog_var_set :: "('lock,'var) Var set \<Rightarrow> 'var set"
where
  "to_prog_var_set vs \<equiv> {v. Var v \<in> vs}"

definition
  to_prog_mds :: "('lock,'var) Var Mds \<Rightarrow> 'var Mds"
where
  "to_prog_mds mds \<equiv> \<lambda> m. {v. Var v \<in> mds m}"

definition lock_held_mds_correct
where
  "lock_held_mds_correct lock_interp mds l \<equiv>
                \<forall>x. (x \<in> fst (lock_interp l) \<longrightarrow>
                        Var x \<notin> mds GuarNoWrite \<and> Var x \<in> mds AsmNoWrite)
                    \<and>
                    (x \<in> snd (lock_interp l) \<longrightarrow>
                        Var x \<notin> mds GuarNoReadOrWrite \<and> Var x \<in> mds AsmNoReadOrWrite)"

definition lock_not_held_mds_correct
where
  "lock_not_held_mds_correct lock_interp mds l \<equiv>
               (\<forall>x. (x \<in> fst (lock_interp l) \<longrightarrow>
                        Var x \<in> mds GuarNoWrite \<and> Var x \<notin> mds AsmNoWrite)
                    \<and>
                    (x \<in> snd (lock_interp l) \<longrightarrow>
                        Var x \<in> mds GuarNoReadOrWrite \<and> Var x \<notin> mds AsmNoReadOrWrite))"

definition lock_acq_upd :: "('Lock \<Rightarrow> ('Var set \<times> 'Var set)) \<Rightarrow> 'Lock \<Rightarrow> ('Lock, 'Var) Var Mds \<Rightarrow> ('Lock, 'Var) Var Mds"
where
  "lock_acq_upd lock_interp l mds \<equiv> case (lock_interp l) of (NoW_vars, NoRW_vars) \<Rightarrow> 
    \<lambda> m. case m of 
      GuarNoWrite \<Rightarrow> mds GuarNoWrite - Var ` NoW_vars |
      AsmNoWrite \<Rightarrow> mds AsmNoWrite \<union> Var ` NoW_vars |
      GuarNoReadOrWrite \<Rightarrow> mds GuarNoReadOrWrite - Var ` NoRW_vars |
      AsmNoReadOrWrite \<Rightarrow> mds AsmNoReadOrWrite \<union> Var ` NoRW_vars "
      
definition lock_rel_upd :: "('Lock \<Rightarrow> ('Var set \<times> 'Var set)) \<Rightarrow> 'Lock \<Rightarrow> ('Lock, 'Var) Var Mds \<Rightarrow> ('Lock, 'Var) Var Mds"
where
  "lock_rel_upd lock_interp l mds \<equiv> case (lock_interp l) of (NoW_vars, NoRW_vars) \<Rightarrow> 
    \<lambda> m. case m of 
      GuarNoWrite \<Rightarrow> mds GuarNoWrite \<union> Var ` NoW_vars |
      AsmNoWrite \<Rightarrow> mds AsmNoWrite - Var ` NoW_vars |
      GuarNoReadOrWrite \<Rightarrow> mds GuarNoReadOrWrite \<union> Var ` NoRW_vars |
      AsmNoReadOrWrite \<Rightarrow> mds AsmNoReadOrWrite - Var ` NoRW_vars "

definition no_lock_mds :: "('Lock, 'Var) Var Mds \<Rightarrow> bool"
where
  "no_lock_mds mds \<equiv> \<forall>l m. Lock l \<notin> mds m"

definition one_mode_per_var :: "('Lock, 'Var) Var Mds \<Rightarrow> bool"
where
  "one_mode_per_var mds \<equiv> \<forall>v m. Var v \<in> mds m \<longrightarrow> Var v \<notin> \<Union>(mds ` (UNIV - {m}))"

definition empty_mds
where
  "empty_mds lock_interp \<equiv>
     \<lambda>m. case m of GuarNoWrite \<Rightarrow> (Var `  \<Union>(fst ` (lock_interp ` UNIV)))
                 | GuarNoReadOrWrite \<Rightarrow> Var ` \<Union>(snd ` (lock_interp ` UNIV))
                 | _ \<Rightarrow> {}"

subsection {* Potential requirements on lock_interp and reachable mode states *}

definition only_lock_governed_mds
where
  "only_lock_governed_mds lock_interp mds \<equiv>
     \<forall> v m. Var v \<in> mds m \<longrightarrow> (\<exists>l. v \<in> fst (lock_interp l) \<or> v \<in> snd (lock_interp l))"

definition lock_consistent_mds
where
  "lock_consistent_mds lock_interp mds \<equiv>
    no_lock_mds mds \<and>
    only_lock_governed_mds lock_interp mds \<and>
    (\<forall>l. lock_not_held_mds_correct lock_interp mds l \<or> lock_held_mds_correct lock_interp mds l) \<and>
    one_mode_per_var mds"

definition lone_lock_per_var
where
  "lone_lock_per_var lock_interp \<equiv>
     \<forall> v l. v \<in> fst (lock_interp l) \<union> snd (lock_interp l) \<longrightarrow>
            (\<forall> l'.  v \<in> fst (lock_interp l') \<union> snd (lock_interp l') \<longrightarrow> l' = l)"

definition lock_interp_no_overlap
where
  "lock_interp_no_overlap lock_interp \<equiv> \<forall>l. fst (lock_interp l) \<inter> snd (lock_interp l) = {}"

definition no_vacuous_locks
where
  "no_vacuous_locks lock_interp \<equiv> \<forall> l. fst (lock_interp l) \<union> snd (lock_interp l) \<noteq> {}"

end