(*
Author: Robert Sison
*)

theory Havoc
imports Compositionality
begin

section {* Helper definitions for mode-permitted havoc *}

context sifum_security_init
begin

subsection {* Definitions of equivalence modulo mode-permitted havoc *}

(* Compare: Low-equivalence modulo modes *)
thm low_mds_eq_def
(* This set of equivalence relations, instead of allowing differences in High-rated variables,
  instead allows mds-permitted outside interference. *)

(* Memory equivalence modulo mode-permitted havoc *)
definition
  mds_havoc_eq :: "'Var Mds \<Rightarrow> ('Var, 'Val) Mem \<Rightarrow> (_, _) Mem \<Rightarrow> bool"
  ("_ =\<index>\<^sup>h _" [100, 100] 80)
where
  "(mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>h mem\<^sub>2) \<equiv> \<forall> x. var_asm_not_written mds x \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x \<and> dma mem\<^sub>1 x = dma mem\<^sub>2 x"

lemma mds_havoc_eq_sym:
  "(mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>h mem\<^sub>2) \<Longrightarrow> (mem\<^sub>2 =\<^bsub>mds\<^esub>\<^sup>h mem\<^sub>1)"
  by (simp add: mds_havoc_eq_def)

lemma mds_havoc_eq_refl:
  "mem =\<^bsub>mds\<^esub>\<^sup>h mem"
  by (simp add: mds_havoc_eq_def)

lemma mds_havoc_eq_trans:
  "(mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>h mem\<^sub>2) \<Longrightarrow> (mem\<^sub>2 =\<^bsub>mds\<^esub>\<^sup>h mem\<^sub>3) \<Longrightarrow> (mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>h mem\<^sub>3)"
  by (simp add: mds_havoc_eq_def)

(* Local configuration equivalence modulo mode-permitted havoc *)
definition
  lc_havoc_eq :: "('Com, 'Var, 'Val) LocalConf \<Rightarrow> ('Com, 'Var, 'Val) LocalConf \<Rightarrow> bool"
  ("_ =\<^sup>h _" [100, 100] 80)
where
  "(lc\<^sub>1 =\<^sup>h lc\<^sub>2) \<equiv> fst lc\<^sub>1 = fst lc\<^sub>2 \<and> snd lc\<^sub>1 =\<^bsub>snd (fst lc\<^sub>1)\<^esub>\<^sup>h snd lc\<^sub>2"

lemma lc_havoc_eq_sym:
  "lc_havoc_eq lc\<^sub>1 lc\<^sub>2 = lc_havoc_eq lc\<^sub>2 lc\<^sub>1"
  by (auto simp: lc_havoc_eq_def mds_havoc_eq_sym)

lemma lc_havoc_eq_refl:
  "lc_havoc_eq lc lc"
  unfolding lc_havoc_eq_def by (simp add:mds_havoc_eq_refl)

lemma lc_havoc_eq_trans:
  "lc_havoc_eq lc\<^sub>1 lc\<^sub>2 \<Longrightarrow> lc_havoc_eq lc\<^sub>2 lc\<^sub>3 \<Longrightarrow> lc_havoc_eq lc\<^sub>1 lc\<^sub>3"
  unfolding lc_havoc_eq_def mds_havoc_eq_def by clarsimp

subsection {* Local reachability modulo mode-permitted havoc with history *}

(* Compare: loc_reach *)
thm loc_reach_def
(* The following definition is a little more explicit because it requires supplying the
  path (history?) of states by which the configuration was reached. *)

(* Local reachability of local configurations modulo mode-permitted havoc *)
primrec
  loc_reach_sig :: "('Com, 'Var, 'Val) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> (_, _, _) LocalConf list \<Rightarrow> bool"
where
  "loc_reach_sig src dst [] = src =\<^sup>h dst" |
  "loc_reach_sig src dst (lc#lcs) = (src =\<^sup>h lc \<and> (\<exists> lc'. lc \<leadsto> lc' \<and> loc_reach_sig lc' dst lcs))"

lemma loc_reach_sig_nontrivial_nonempty:
  "loc_reach_sig \<langle>c, mds, mem\<rangle> \<langle>c', mds', mem'\<rangle> sig \<Longrightarrow> c \<noteq> c' \<Longrightarrow> sig \<noteq> []"
  by (clarsimp simp:lc_havoc_eq_def)

lemma loc_reach_sig_append:
  "loc_reach_sig src mid lcs \<Longrightarrow>
   loc_reach_sig mid dst lcs' \<Longrightarrow>
   loc_reach_sig src dst (lcs @ lcs')"
  apply(induct lcs arbitrary:src)
   apply clarsimp
   apply(induct lcs')
    using lc_havoc_eq_trans apply fastforce
   apply clarsimp
   using lc_havoc_eq_trans apply blast
  by fastforce

(* Sanity check: The following lemmas establish that loc_reach_sig and loc_reach are equivalent *)
lemma loc_reach_imp_sig:
  "\<langle>c', mds', mem'\<rangle> \<in> loc_reach src \<Longrightarrow> \<exists> lcs. loc_reach_sig src \<langle>c', mds', mem'\<rangle> lcs"
  apply(induct rule:loc_reach.induct)
    apply(rule_tac x="[]" in exI)
    using lc_havoc_eq_refl apply clarsimp
   apply clarsimp
   apply(rename_tac lcs)
   apply(rule_tac x="lcs @ [\<langle>c', mds', mem'\<rangle>]" in exI)
   apply(rule loc_reach_sig_append)
    apply simp
   apply clarsimp
   using lc_havoc_eq_refl apply blast
  apply clarsimp
  apply(rule_tac x="x" in exI)
proof -
  fix c'a :: 'Com and mds'a :: "Mode \<Rightarrow> 'Var set" and mem'a :: "'Var \<Rightarrow> 'Val" and mem'' :: "'Var \<Rightarrow> 'Val" and x :: "(('Com \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) list"
  assume a1: "loc_reach_sig src \<langle>c'a, mds'a, mem'a\<rangle> x"
  assume "\<forall>x. var_asm_not_written mds'a x \<longrightarrow> mem'a x = mem'' x \<and> dma mem'a x = dma mem'' x"
  then have "mem'a =\<^bsub>mds'a\<^esub>\<^sup>h mem''"
    using mds_havoc_eq_def by presburger
  then show "loc_reach_sig src \<langle>c'a, mds'a, mem''\<rangle> x"
    using a1 by (metis (no_types) append_Nil2 lc_havoc_eq_def loc_reach_sig.simps(1) prod.sel loc_reach_sig_append)
qed

lemma loc_reach_if_sig:
  "loc_reach_sig src \<langle>c', mds', mem'\<rangle> lcs \<Longrightarrow> \<langle>c', mds', mem'\<rangle> \<in> loc_reach src"
  apply(induct lcs arbitrary:src)
   apply clarsimp
   apply(subgoal_tac "\<langle>a, b, ba\<rangle> \<in> loc_reach \<langle>a, b, ba\<rangle>")
    prefer 2
    using loc_reach.intros(1) apply force
   apply (simp add: lc_havoc_eq_def loc_reach.mem_diff mds_havoc_eq_def)
  apply clarsimp
  apply(unfold lc_havoc_eq_def)
  apply clarsimp
  apply(rename_tac c mds mem lcs mem'' c'' mds'' mem''')
  apply(subgoal_tac "\<langle>c, mds, mem\<rangle> \<in> loc_reach \<langle>c, mds, mem''\<rangle>")
   prefer 2
   apply(rule_tac mem'=mem'' in mem_diff)
    using refl apply simp
   apply(unfold mds_havoc_eq_def)
   apply clarsimp
  using loc_reach_mem_diff_eq loc_reach_subset by blast

lemma loc_reach_iff_sig:
  "\<langle>c', mds', mem'\<rangle> \<in> loc_reach src \<equiv> \<exists> lcs. loc_reach_sig src \<langle>c', mds', mem'\<rangle> lcs"
  apply(rule eq_reflection)
  using loc_reach_imp_sig loc_reach_if_sig
  by blast

lemma sig_head_mem_diff:
  "mem =\<^bsub>mds\<^esub>\<^sup>h mem' \<Longrightarrow>
   loc_reach_sig \<langle>c, mds, mem\<rangle> \<langle>c', mds', mem'''\<rangle> sig \<Longrightarrow>
   loc_reach_sig \<langle>c, mds, mem'\<rangle> \<langle>c', mds', mem'''\<rangle> sig"
  apply(subgoal_tac "loc_reach_sig \<langle>c, mds, mem'\<rangle> \<langle>c, mds, mem\<rangle> []")
   apply(drule_tac mid="\<langle>c, mds, mem\<rangle>" and dst="\<langle>c', mds', mem'''\<rangle>" and lcs'=sig in loc_reach_sig_append)
    apply simp
   apply simp
  by (clarsimp simp:lc_havoc_eq_def mds_havoc_eq_sym)

lemma eval_implies_loc_reach_sig:
  "lc \<leadsto> lc' \<Longrightarrow> loc_reach_sig lc lc' [lc]"
  apply clarsimp
  by (metis lc_havoc_eq_refl prod.collapse)

end
end