#!/bin/bash

ARTIFACT_NAME=wr-compiler-itp19

if [ -z ${VANILLA_ISABELLE} ]; then
  echo "VANILLA_ISABELLE not set."
  exit 0;
fi

if [ -z ${DESTINATION_DIR} ]; then
  echo "DESTINATION_DIR not set."
  exit 0;
fi

set -e
set -x

mkdir -p ${DESTINATION_DIR}

tar -C ${DESTINATION_DIR} -xf ${ARTIFACT_NAME}.tar

cd ${DESTINATION_DIR}/${ARTIFACT_NAME}

${VANILLA_ISABELLE}/bin/isabelle build -d . -v -b ExampleWorker

${VANILLA_ISABELLE}/bin/isabelle build -d . -v -b ExamplesHighBranchRefine

${VANILLA_ISABELLE}/bin/isabelle build -d . -v -b WhileRISCCompiler

${VANILLA_ISABELLE}/bin/isabelle build -d . -v -b ExamplesCDDCWRCompiler
