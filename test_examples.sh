#!/bin/bash

if [ -z ${VANILLA_ISABELLE} ]; then
  echo "VANILLA_ISABELLE not set."
  exit 0;
fi

set -x

${VANILLA_ISABELLE}/bin/isabelle build -d . -v -b Examples

${VANILLA_ISABELLE}/bin/isabelle build -d . -v -b ExamplesCDDCLogic

${VANILLA_ISABELLE}/bin/isabelle build -d . -v -b ExampleWorker

${VANILLA_ISABELLE}/bin/isabelle build -d . -v -b ExamplesCDDCWRCompiler

${VANILLA_ISABELLE}/bin/isabelle build -d . -v -b ExamplesHighBranchRefine
