(*
Author: Robert Sison
*)

theory RISCRefinementSafe
imports RISCRefinement
begin

context risc_compilation
begin

subsubsection {* Helpers for coupling invariant proofs *}

lemma eval\<^sub>r_non_branching_cases_pc:
  "pc < length P \<Longrightarrow> \<not> (\<exists> l r. current_instB (pc, P) = Jz l r) \<Longrightarrow>
   \<not> (\<exists> k. current_instB (pc, P) = LockAcq k) \<Longrightarrow>
   \<not> (\<exists> k. current_instB (pc, P) = LockRel k) \<Longrightarrow>
   (\<langle>((pc, P), regs\<^sub>1), mds\<^sub>1\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C, \<langle>((pc\<^sub>1', P), regs\<^sub>1'), mds\<^sub>1\<^sub>C', mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RL.eval\<^sub>r \<Longrightarrow>
   (\<langle>((pc, P), regs\<^sub>2), mds\<^sub>2\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C, \<langle>((pc\<^sub>2', P), regs\<^sub>2'), mds\<^sub>2\<^sub>C', mem\<^sub>2\<^sub>C'\<rangle>\<^sub>C) \<in> RL.eval\<^sub>r \<Longrightarrow>
   pc\<^sub>1' = pc\<^sub>2'"
  by (case_tac "current_instB (pc, P)", auto)

lemma eval\<^sub>r_non_locking_cases_mds:
  "\<not> (\<exists> k. current_instB (pc, P) = LockAcq k) \<Longrightarrow>
   \<not> (\<exists> k. current_instB (pc, P) = LockRel k) \<Longrightarrow>
   (\<langle>((pc, P), regs\<^sub>1), mds\<^sub>1\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C, \<langle>((pc\<^sub>1', P), regs\<^sub>1'), mds\<^sub>1\<^sub>C', mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RL.eval\<^sub>r \<Longrightarrow>
   (\<langle>((pc, P), regs\<^sub>2), mds\<^sub>2\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C, \<langle>((pc\<^sub>2', P), regs\<^sub>2'), mds\<^sub>2\<^sub>C', mem\<^sub>2\<^sub>C'\<rangle>\<^sub>C) \<in> RL.eval\<^sub>r \<Longrightarrow>
   mds\<^sub>1\<^sub>C = mds\<^sub>2\<^sub>C \<Longrightarrow>
   mds\<^sub>1\<^sub>C' = mds\<^sub>2\<^sub>C'"
  by (case_tac "current_instB (pc, P)", auto)

lemma \<R>_jz_only_occurs_when_if_leftmost:
assumes
  a1: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" and
  a2: "pc < length P" and
  a3: "current_instB (pc, P) = Jz l r"
shows
  "\<exists> e c\<^sub>1 c\<^sub>2. WL.leftmost_cmd c\<^sub>A = If e c\<^sub>1 c\<^sub>2"
  using a1 a2 a3
  apply(induct c\<^sub>A mds\<^sub>A mem\<^sub>A pc P regs mds\<^sub>C mem\<^sub>C rule:\<R>_induct)
                  using risc_compilation_axioms apply force
                 using a1 apply force
                apply(force simp:cmd_seq_def nth_append)
               apply force
              apply force
             apply force
            apply(frule_tac i=pc in compile_expr_output_forms)
             apply force
            apply(force simp:compile_expr_output_forms_def nth_append Let_def split:prod.splits)
           apply(force simp:compile_expr_output_forms_def nth_append Let_def split:prod.splits)
          apply force
         apply(force simp:nth_append compile_cmd_input_reqs_def split:prod.splits if_splits)
        apply(force simp:nth_append compile_cmd_input_reqs_def split:prod.splits if_splits)
       apply(subgoal_tac "\<not> compile_expr_output_forms (current_instB (pc, P))")
        prefer 2
        apply(force simp:compile_expr_output_forms_def)
       apply(clarsimp simp:nth_append compile_cmd_input_reqs_def split:prod.splits if_splits)
          apply(metis compile_expr_output_forms length_greater_0_conv)
         apply(metis compile_expr_output_forms length_greater_0_conv)
        using compile_expr_emptyD apply fastforce
       using compile_expr_emptyD apply fastforce
      apply(force simp:nth_append compile_cmd_input_reqs_def split:prod.splits if_splits)
     apply(force simp:nth_append compile_cmd_input_reqs_def split:prod.splits if_splits)
    apply(force simp:epilogue_step_forms_def)
   apply force
  by force

lemma \<R>_jz_if_leftmost_consistent_expr:
assumes
  a1: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" and
  a2: "pc < length P" and
  a3: "current_instB (pc, P) = Jz l r" and
  a4: "WL.leftmost_cmd c\<^sub>A = If e c\<^sub>1 c\<^sub>2"
shows
  "\<exists> (C\<^sub>1::('Reg, 'Val, 'Var, 'OpId) CompRec).
    regrec C\<^sub>1 r = Some e \<and>
    regrec_mem_consistent C\<^sub>1 regs mem\<^sub>C"
  using a1 a2 a3 a4
  apply(induct c\<^sub>A mds\<^sub>A mem\<^sub>A pc P regs mds\<^sub>C mem\<^sub>C rule:\<R>_induct)
                  using risc_compilation_axioms apply force
                 using a1 apply force
                apply(force dest:\<R>_pairs_stop_implies_halt_or_epilogue simp:cmd_seq_def epilogue_step_forms_def nth_append)
               apply force
              apply force
             apply force
            apply(frule_tac i=pc in compile_expr_output_forms)
             apply force
            apply(force simp:compile_expr_output_forms_def nth_append split:prod.splits)
           apply force
          apply(case_tac "pc < length P\<^sub>e")
           apply(frule_tac i=pc in compile_expr_output_forms)
            apply force
           apply(force simp:compile_expr_output_forms_def nth_append compile_cmd_input_reqs_def split:prod.splits)
          apply(clarsimp simp:compile_cmd_input_reqs_def split:prod.splits)
          apply(frule_tac A="{}" in compile_expr_correctness')
            apply force
           apply force
          apply force
         apply(force simp:nth_append compile_cmd_input_reqs_def split:prod.splits if_splits)
        apply(force simp:nth_append compile_cmd_input_reqs_def split:prod.splits if_splits)
       apply(case_tac "pc < length P\<^sub>e")
        apply(frule_tac i=pc in compile_expr_output_forms)
         apply force
        apply(force simp:compile_expr_output_forms_def compile_cmd_input_reqs_def nth_append split:prod.splits)
       apply(case_tac "P\<^sub>e = []")
        apply force
       apply(rule_tac x=C\<^sub>e in exI)
       apply(clarsimp simp:nth_append compile_cmd_input_reqs_def split:prod.splits)
       apply(rename_tac c C la nl regs mem\<^sub>C mds\<^sub>C mds\<^sub>A mem\<^sub>A x1 x2 x2a x1b C\<^sub>e fail\<^sub>e x1d x1e x1f x2f fail\<^sub>c)
       apply(rule_tac C="C\<lparr>regrec := Map.empty\<rparr>" and A="{}" in compile_expr_correctness')
         apply force
        apply force
       apply force
      apply(force simp:nth_append compile_cmd_input_reqs_def split:prod.splits if_splits)
     apply(force simp:nth_append split:prod.splits if_splits)
    apply(force simp:epilogue_step_forms_def)
   apply force
  by force

definition req_ev\<^sub>B_eq
where
  "req_ev\<^sub>B_eq lc\<^sub>1 lc\<^sub>2 \<equiv>
     let ((c\<^sub>1\<^sub>A, mds), mem\<^sub>1\<^sub>A) = lc\<^sub>1;
         ((c\<^sub>2\<^sub>A, mds), mem\<^sub>2\<^sub>A) = lc\<^sub>2 in
      \<forall>e a b. WL.leftmost_cmd c\<^sub>1\<^sub>A = If e a b \<longrightarrow>
              exp_ev\<^sub>B (to_prog_mem mem\<^sub>1\<^sub>A) e = exp_ev\<^sub>B (to_prog_mem mem\<^sub>2\<^sub>A) e"

lemma bisim_ev\<^sub>B_eq2:
  "WL.bisim_ev\<^sub>B_eq \<B> = (\<forall>c\<^sub>1\<^sub>A mds mem\<^sub>1\<^sub>A c\<^sub>2\<^sub>A mem\<^sub>2\<^sub>A.
       (\<langle>c\<^sub>1\<^sub>A, mds, mem\<^sub>1\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>2\<^sub>A, mds, mem\<^sub>2\<^sub>A\<rangle>\<^sub>A) \<in> \<B> \<longrightarrow>
       req_ev\<^sub>B_eq \<langle>c\<^sub>1\<^sub>A, mds, mem\<^sub>1\<^sub>A\<rangle>\<^sub>A \<langle>c\<^sub>2\<^sub>A, mds, mem\<^sub>2\<^sub>A\<rangle>\<^sub>A)"
  apply(clarsimp simp:WL.bisim_ev\<^sub>B_eq_def req_ev\<^sub>B_eq_def)
  by blast

definition req_simple
where
  "req_simple lc\<^sub>1 lc\<^sub>2 \<equiv> fst (fst lc\<^sub>1) = fst (fst lc\<^sub>2)"

lemma bisim_simple\<^sub>w_def2:
  "WL.bisim_simple\<^sub>w \<B> = (\<forall>c\<^sub>1\<^sub>A mds mem\<^sub>1\<^sub>A c\<^sub>2\<^sub>A mem\<^sub>2\<^sub>A.
       (\<langle>c\<^sub>1\<^sub>A, mds, mem\<^sub>1\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>2\<^sub>A, mds, mem\<^sub>2\<^sub>A\<rangle>\<^sub>A) \<in> \<B> \<longrightarrow>
       req_simple \<langle>c\<^sub>1\<^sub>A, mds, mem\<^sub>1\<^sub>A\<rangle>\<^sub>A \<langle>c\<^sub>2\<^sub>A, mds, mem\<^sub>2\<^sub>A\<rangle>\<^sub>A)"
  by (clarsimp simp:WL.bisim_simple\<^sub>w_def req_simple_def)

definition bisim_extras
where
  "bisim_extras lc\<^sub>1 lc\<^sub>2 \<equiv> req_simple lc\<^sub>1 lc\<^sub>2 \<and> req_ev\<^sub>B_eq lc\<^sub>1 lc\<^sub>2"


subsubsection {* Concrete coupling invariant \<I> and its properties *}

inductive_set \<I> :: "(('Reg, 'Var, 'Lock, 'Val, 'OpId) RL.PerThread, ('Lock, 'Var) Var, 'Val) LocalConf rel"
where
  (* sufficient only for naive refinement that doesn't support high-branching *)
  \<I>_default: "\<lbrakk>c = c'\<rbrakk> \<Longrightarrow> (\<langle>(c, regs), mds, mem\<rangle>\<^sub>C, \<langle>(c', regs'), mds', mem'\<rangle>\<^sub>C) \<in> \<I>"

lemma \<I>_stops_consistently:
  "(\<langle>c\<^sub>1\<^sub>C, mds\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C, \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C) \<in> \<I> \<Longrightarrow>
   stops\<^sub>C \<langle>c\<^sub>1\<^sub>C, mds\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C = stops\<^sub>C \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C"
  unfolding stops\<^sub>C_def
  by (metis \<I>.simps surj_pair RL.eval\<^sub>r_halt_iff_oob)

subsubsection {* That \<R> is simpler-refinement-safe for simple consistently-branching bisimulations *}

(* The only step to change mds is LockAcq/LockRel, for which the destination pc and mds are
  dependent only on Locks, which we've determined must be Low-rated non-control variables
  that have no mode state, the last of which is the no_locks_mds invariant maintained by \<R>. *)
lemma \<R>_preserves_simple_ev\<^sub>B_eq_\<B>_pc_mds:
  "WL.bisim_ev\<^sub>B_eq \<B> \<Longrightarrow>
   (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>1\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>2\<^sub>A\<rangle>\<^sub>A) \<in> \<B> \<Longrightarrow>
   (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>1\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs\<^sub>1), mds\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C) \<in> \<R> \<Longrightarrow>
   (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>2\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs\<^sub>2), mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C) \<in> \<R> \<Longrightarrow>
   eval_abv\<^sub>C \<langle>((pc, P), regs\<^sub>1), mds\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C \<langle>((pc\<^sub>1', P), regs\<^sub>1'), mds\<^sub>1\<^sub>C', mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   eval_abv\<^sub>C \<langle>((pc, P), regs\<^sub>2), mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C \<langle>((pc\<^sub>2', P), regs\<^sub>2'), mds\<^sub>2\<^sub>C', mem\<^sub>2\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   WL.low_mds_eq mds\<^sub>A mem\<^sub>1\<^sub>A mem\<^sub>2\<^sub>A \<Longrightarrow>
   pc\<^sub>1' = pc\<^sub>2' \<and> mds\<^sub>1\<^sub>C' = mds\<^sub>2\<^sub>C'"
  apply(case_tac "(\<exists> l r. current_instB (pc, P) = Jz l r)")
   apply(frule RL.eval\<^sub>r_bounds)
   apply clarsimp
   apply(frule_tac mem\<^sub>A=mem\<^sub>1\<^sub>A in \<R>_jz_only_occurs_when_if_leftmost)
     apply force
    apply force
   apply clarsimp
   apply(frule_tac mem\<^sub>A=mem\<^sub>1\<^sub>A in \<R>_jz_if_leftmost_consistent_expr)
      apply force
     apply force
    apply force
   apply(frule_tac mem\<^sub>A=mem\<^sub>2\<^sub>A in \<R>_jz_only_occurs_when_if_leftmost)
     apply force
    apply force
   apply clarsimp
   apply(frule_tac mem\<^sub>A=mem\<^sub>2\<^sub>A in \<R>_jz_if_leftmost_consistent_expr)
      apply force
     apply force
    apply force
   apply(clarsimp simp:regrec_mem_consistent_def)
   (* Here we lean on an extra requirement that says that the bisimulation is one
     that only permits branching on expressions that evaluate to the same value
     in low-equal memories. *)
   apply(subgoal_tac "exp_ev\<^sub>B (to_prog_mem mem\<^sub>1\<^sub>A) e = exp_ev\<^sub>B (to_prog_mem mem\<^sub>2\<^sub>A) e")
    prefer 2
    apply(clarsimp simp:WL.bisim_ev\<^sub>B_eq_def)
    apply blast
   using preserves_modes_mem_\<R>
   apply(drule preserves_modes_memD)+
   apply(erule_tac x=r in allE)
   apply(erule_tac x=e in allE)
   apply(erule_tac x=r in allE)
   apply(erule_tac x=e in allE)
   apply(case_tac "regs\<^sub>1 r = 0")
    apply(clarsimp simp:exp_ev\<^sub>B_def mem\<^sub>A_of_def)
    using RL.jz_s_dest[where regs=regs\<^sub>1]
    apply fast
   apply(clarsimp simp:exp_ev\<^sub>B_def mem\<^sub>A_of_def)
   apply(frule RL.jz_f_dest[where regs=regs\<^sub>1])
     apply force
    apply force
   apply(rule conjI)
    apply force
   apply force
  apply(subgoal_tac "no_lock_mds mds\<^sub>A")
   prefer 2
   apply(force dest:conds_\<R> simp:\<R>_conds preserves_modes_mem_pair_def mds\<^sub>A_of_def lock_consistent_mds_def)
  apply(case_tac "(\<exists> k. current_instB (pc, P) = LockAcq k)")
   apply(clarsimp simp:WL.low_mds_eq_def)
   using no_H_locks no_locks_in_\<C>
   apply(erule_tac x="Lock k" in allE)
   apply(clarsimp simp:no_lock_mds_def)
   apply(subgoal_tac "mem\<^sub>1\<^sub>C (Lock k) = mem\<^sub>2\<^sub>C (Lock k)")
    prefer 2
    apply(force dest:conds_\<R> simp:\<R>_conds preserves_modes_mem_pair_def mem\<^sub>A_of_def)
   apply(case_tac "eval\<^sub>L (mem\<^sub>1\<^sub>C (Lock k))")
    apply(frule RL.lock_spin_dest[where regs=regs\<^sub>1])
      apply blast
     apply blast
    apply(drule RL.lock_spin_dest[where regs=regs\<^sub>2])
      apply blast
     apply presburger
    apply blast
   apply(frule RL.lock_acq_dest[where regs=regs\<^sub>1])
     apply blast
    apply blast
   apply(drule RL.lock_acq_dest[where regs=regs\<^sub>2])
     apply blast
    apply presburger
   apply blast
  apply(case_tac "(\<exists> k. current_instB (pc, P) = LockRel k)")
   apply(clarsimp simp:WL.low_mds_eq_def)
   using no_H_locks no_locks_in_\<C>
   apply(erule_tac x="Lock k" in allE)
   apply(clarsimp simp:no_lock_mds_def)
   apply(subgoal_tac "mem\<^sub>1\<^sub>C (Lock k) = mem\<^sub>2\<^sub>C (Lock k)")
    prefer 2
    apply(force dest:conds_\<R> simp:\<R>_conds preserves_modes_mem_pair_def mem\<^sub>A_of_def)
   apply(case_tac "lock_held_mds_correct lock_interp mds\<^sub>C k")
    apply(frule RL.lock_rel_dest[where regs=regs\<^sub>1])
      apply blast
     apply blast
    apply(drule RL.lock_rel_dest[where regs=regs\<^sub>2])
      apply blast
     apply presburger
    apply blast
   apply(frule RL.lock_abort_dest[where regs=regs\<^sub>1])
     apply blast
    apply blast
   apply(drule RL.lock_abort_dest[where regs=regs\<^sub>2])
     apply blast
    apply blast
   apply blast
  using eval\<^sub>r_non_branching_cases_pc eval\<^sub>r_non_locking_cases_mds
  by (force simp:RL.eval\<^sub>r_bounds)

lemma simpler_refinement_safe_\<R>_simple_ev\<^sub>B_eq_\<B>:
  "WL.strong_low_bisim_mm \<B> \<Longrightarrow> WL.bisim_simple_ev\<^sub>B_eq \<B> \<Longrightarrow> simpler_refinement_safe \<B> \<R> \<I> abs_steps"
  unfolding simpler_refinement_safe_def
  apply clarsimp
  apply(clarsimp simp:\<I>.simps)
  apply(rename_tac c\<^sub>1\<^sub>A mds\<^sub>A mem\<^sub>1\<^sub>A c\<^sub>2\<^sub>A mem\<^sub>2\<^sub>A regs\<^sub>1 mds\<^sub>C mem\<^sub>1\<^sub>C pc P regs\<^sub>2 mem\<^sub>2\<^sub>C)
  apply(rule conjI)
   apply(rule \<I>_stops_consistently)
   apply(force simp:\<I>.simps)
  apply(rule conjI)
   apply(force simp:WL.bisim_simple_ev\<^sub>B_eq_def WL.bisim_simple\<^sub>w_def)
  apply clarsimp
  apply(frule_tac regs=regs\<^sub>1 in RL.eval\<^sub>r_no_modify)
  apply(frule_tac regs=regs\<^sub>2 in RL.eval\<^sub>r_no_modify)
  apply clarsimp
  apply(rename_tac c\<^sub>1\<^sub>A mds\<^sub>A mem\<^sub>1\<^sub>A c\<^sub>2\<^sub>A mem\<^sub>2\<^sub>A regs\<^sub>1 mds\<^sub>C mem\<^sub>1\<^sub>C pc regs\<^sub>2 mem\<^sub>2\<^sub>C mds\<^sub>1\<^sub>C' mds\<^sub>2\<^sub>C' mem\<^sub>1\<^sub>C' mem\<^sub>2\<^sub>C' pc\<^sub>1' P regs\<^sub>1' pc\<^sub>2' regs\<^sub>2')
  apply(subgoal_tac "WL.low_mds_eq mds\<^sub>A mem\<^sub>1\<^sub>A mem\<^sub>2\<^sub>A")
   prefer 2
   apply(force simp:WL.strong_low_bisim_mm_def)
  apply(subgoal_tac "c\<^sub>1\<^sub>A = c\<^sub>2\<^sub>A")
   prefer 2
   apply(force simp:WL.bisim_simple_ev\<^sub>B_eq_def WL.bisim_simple\<^sub>w_def)
  by (meson WL.bisim_simple_ev\<^sub>B_eq_def \<R>_preserves_simple_ev\<^sub>B_eq_\<B>_pc_mds)

subsubsection {* That \<R> is therefore a secure refinement with \<I> for simple consistently-branching bisimulations *}

lemma secure_refinement_simpler_\<R>_simple_ev\<^sub>B_eq_\<B>:
  "WL.strong_low_bisim_mm \<B> \<Longrightarrow> WL.bisim_simple_ev\<^sub>B_eq \<B> \<Longrightarrow> secure_refinement_simpler \<B> \<R> \<I> abs_steps"
  unfolding secure_refinement_simpler_def
  apply(rule conjI)
   apply(rule closed_others_\<R>)
  apply(rule conjI)
   apply(rule preserves_modes_mem_\<R>)
  apply(rule conjI)
   unfolding new_vars_private_def
   apply(force simp:new_vars_private_\<R>)
  apply(rule conjI)
   apply(rule simpler_refinement_safe_\<R>_simple_ev\<^sub>B_eq_\<B>)
    apply force
   apply force
  apply(rule conjI)
   unfolding RL.closed_glob_consistent_def
   using \<I>.simps RL.low_mds_eq_def apply auto[1]
  using \<R>_preservation RL.eval\<^sub>r_no_modify prod.collapse by fastforce

lemma secure_refinement_\<R>_simple_ev\<^sub>B_eq_\<B>:
  "WL.strong_low_bisim_mm \<B> \<Longrightarrow> WL.bisim_simple_ev\<^sub>B_eq \<B> \<Longrightarrow> secure_refinement \<B> \<R> \<I>"
  using secure_refinement_simpler_\<R>_simple_ev\<^sub>B_eq_\<B> secure_refinement_simpler
  by fast

lemma strong_low_bisim_mm_\<B>\<^sub>C_of_\<R>_simple_ev\<^sub>B_eq_\<B>:
  "WL.strong_low_bisim_mm \<B> \<Longrightarrow> WL.bisim_simple_ev\<^sub>B_eq \<B> \<Longrightarrow> RL.strong_low_bisim_mm (R\<^sub>C_of \<B> \<R> \<I>)"
  apply(rule R\<^sub>C_of_strong_low_bisim_mm)
    apply blast
   using secure_refinement_\<R>_simple_ev\<^sub>B_eq_\<B> apply blast
  by (simp add:sym_def \<I>.simps)

subsection {* Preliminaries about initial compilation and mode states *}

thm empty_mds_def (* TODO: use or alias this instead? *)
definition
  mds\<^sub>0
where
  "mds\<^sub>0 \<equiv> \<lambda>m. case m of GuarNoWrite \<Rightarrow> (Var `  \<Union>(fst ` (lock_interp ` UNIV)))
                      | GuarNoReadOrWrite \<Rightarrow> Var ` \<Union>(snd ` (lock_interp ` UNIV))
                      | _ \<Rightarrow> {}"

lemma no_lock_mds\<^sub>0:
  "no_lock_mds mds\<^sub>0"
  by (clarsimp simp:no_lock_mds_def mds\<^sub>0_def image_def split:Mode.splits)

lemma lock_not_held_mds\<^sub>0_correct:
  "lock_not_held_mds_correct lock_interp mds\<^sub>0 l"
  apply(clarsimp simp:lock_not_held_mds_correct_def mds\<^sub>0_def image_def)
  using prod.collapse by blast

lemma mds\<^sub>0_no_asms:
  "mds\<^sub>0 AsmNoReadOrWrite = {} \<and> mds\<^sub>0 AsmNoWrite = {}"
  by (clarsimp simp:mds\<^sub>0_def)

lemma no_locks\<^sub>w_mds\<^sub>0_management_conds:
  "WL.no_locks_acquired mem \<Longrightarrow>
   list_all (\<lambda>(c, mds). mds = mds\<^sub>0) cms \<Longrightarrow>
   WL.no_lock_mds_gc (cms, mem) \<and>
   WL.lock_managed_vars_mds_mem_correct (cms, mem) \<and>
   WL.unmanaged_var_modes_compatible (cms, mem)"
  apply(clarsimp simp:list_all_def split:prod.splits)
  apply(rule conjI)
   apply(clarsimp simp:WL.no_lock_mds_gc_def WL.modes_of_gc_def)
   using no_lock_mds\<^sub>0 apply force
  apply(rule conjI)
   apply(clarsimp simp:WL.lock_managed_vars_mds_mem_correct_def WL.no_locks_acquired_def WL.modes_of_gc_def)
   using lock_not_held_mds\<^sub>0_correct
   apply(metis nth_mem prod.collapse)
  apply(clarsimp simp:WL.unmanaged_var_modes_compatible_def WL.modes_of_gc_def)
  apply(subgoal_tac "\<forall>k < length cms. snd (cms ! k) = mds\<^sub>0")
   prefer 2
   apply(metis (no_types) nth_mem prod.collapse)
  using mds\<^sub>0_no_asms
  by force

(* duplicated proof. Move up? *)
lemma no_locks\<^sub>r_mds\<^sub>0_management_conds:
  "RL.no_locks_acquired mem \<Longrightarrow>
   list_all (\<lambda>(c, mds). mds = mds\<^sub>0) cms \<Longrightarrow>
   RL.no_lock_mds_gc (cms, mem) \<and>
   RL.lock_managed_vars_mds_mem_correct (cms, mem) \<and>
   RL.unmanaged_var_modes_compatible (cms, mem)"
  apply(clarsimp simp:list_all_def split:prod.splits)
  apply(rule conjI)
   apply(clarsimp simp:RL.no_lock_mds_gc_def RL.modes_of_gc_def)
   using no_lock_mds\<^sub>0 apply force
  apply(rule conjI)
   apply(clarsimp simp:RL.lock_managed_vars_mds_mem_correct_def RL.no_locks_acquired_def RL.modes_of_gc_def)
   using lock_not_held_mds\<^sub>0_correct
   apply(metis nth_mem prod.collapse)
  apply(clarsimp simp:RL.unmanaged_var_modes_compatible_def RL.modes_of_gc_def)
  apply(subgoal_tac "\<forall>k < length cms. snd (cms ! k) = mds\<^sub>0")
   prefer 2
   apply(metis (no_types) nth_mem prod.collapse)
  using mds\<^sub>0_no_asms
  by force

lemma no_locks\<^sub>r_mds\<^sub>0_global_soundness:
  "RL.no_locks_acquired mem \<Longrightarrow>
   list_all (\<lambda>(c, mds). mds = mds\<^sub>0) cms \<Longrightarrow>
   RL.globally_sound_mode_use (cms, mem)"
  using no_locks\<^sub>r_mds\<^sub>0_management_conds RL.management_requirements_ensure_global_soundness
  by blast

definition
  C\<^sub>0 :: "('Reg, 'Val, 'Var, 'OpId) CompRec"
where
  "C\<^sub>0 \<equiv> \<lparr> regrec = Map.empty, asmrec = ({}, {}) \<rparr>"

(* TODO: can this move up to LockVar.thy and become a proof about empty_mds? *)
lemma no_locks_acquired_C\<^sub>0_mds\<^sub>0_init_reqs:
  "RL.no_locks_acquired mem \<Longrightarrow>
   compiled_cmd_init_reqs C\<^sub>0 regs mds\<^sub>0 mem"
  apply(clarsimp simp:compiled_cmd_init_reqs_def regrec_mem_consistent_def asmrec_mds_consistent'_def no_lock_mds\<^sub>0 C\<^sub>0_def mds\<^sub>0_no_asms lock_consistent_mds_def)
  apply(rule conjI)
   apply(clarsimp simp:RL.no_locks_acquired_def only_lock_governed_mds_def mds\<^sub>0_def image_def split:Mode.splits)
   apply(metis Mode.exhaust fst_conv snd_conv)
  apply(rule conjI)
   apply(clarsimp simp:RL.no_locks_acquired_def lock_held_mds_correct_def lock_not_held_mds_correct_def mds\<^sub>0_def)
   apply blast
  using RL.lone_lock_per_var lock_interp_no_overlap
  unfolding lone_lock_per_var_def lock_interp_no_overlap_def
  apply(clarsimp simp:RL.no_locks_acquired_def one_mode_per_var_def mds\<^sub>0_def split:Mode.splits)
  by blast

subsection {* That the While-to-RISC compiler preserves security for simple consistently-branching programs *}

lemma compile_cmd_correctness_\<B>\<^sub>C_of:
  "WL.strong_low_bisim_mm \<B> \<Longrightarrow>
   (zip P Cs, l', nl', C', False) = compile_cmd C l nl c \<Longrightarrow>
   length P = length Cs \<Longrightarrow>
   compile_cmd_input_reqs C l nl c \<Longrightarrow>
   compiled_cmd_init_reqs C regs mds mem\<^sub>1 \<Longrightarrow>
   compiled_cmd_init_reqs C regs mds mem\<^sub>2 \<Longrightarrow>
   (\<langle>c, mds, mem\<^sub>1\<rangle>\<^sub>A, \<langle>c, mds, mem\<^sub>2\<rangle>\<^sub>A) \<in> \<B> \<Longrightarrow>
   (\<langle>((0, P), regs), mds, mem\<^sub>1\<rangle>\<^sub>C, \<langle>((0, P), regs), mds, mem\<^sub>2\<rangle>\<^sub>C) \<in> R\<^sub>C_of \<B> \<R> \<I>"
  apply(frule_tac mem\<^sub>C=mem\<^sub>1 in compile_cmd_correctness_\<R>, simp+)
  apply(frule_tac mem\<^sub>C=mem\<^sub>2 in compile_cmd_correctness_\<R>, simp+)
  apply(clarsimp simp:R\<^sub>C_of_def)
  apply(rule_tac x=c in exI)
  apply(rule_tac x="mds" in exI)
  apply(rule_tac x="mem\<^sub>1" in exI)
  apply(clarsimp simp:mds\<^sub>A_of_def mem\<^sub>A_of_def)
  apply(rule_tac x=c in exI)
  apply(rule_tac x="mds" in exI)
  apply(rule_tac x="mem\<^sub>2" in exI)
  by (simp add:\<I>.simps mds\<^sub>A_of_def mem\<^sub>A_of_def WL.strong_low_bisim_mm_def)

theorem compile_cmd_simple_ev\<^sub>B_eq_secure:
assumes BISIM_REQS\<^sub>w: "\<forall>\<B>. BISIM_REQS\<^sub>w \<B> \<longrightarrow> WL.bisim_simple_ev\<^sub>B_eq \<B>"
assumes INIT\<^sub>r_INIT\<^sub>w: "\<forall>mem. INIT\<^sub>r mem \<longrightarrow> INIT\<^sub>w mem"
shows
  (* We obtain the assumptions for strong_low_bisim_mm_\<B>\<^sub>C_of_\<R>_simple_ev\<^sub>B_eq_\<B>,
    i.e. that the refinement relation \<R> is a secure refinement under those assumptions
    from com_sifum_secure with the needed BISIM_REQS\<^sub>w *)
  "WL.com_sifum_secure (c, mds\<^sub>0) \<Longrightarrow>
  (* These are the compiler input assumptions for compile_cmd_correctness_\<R>,
    i.e. that compile_cmd's input and output programs are related by \<R> under these assumptions.
    We obtain the needed runtime init assumptions using no_locks_acquired_C\<^sub>0_mds\<^sub>0_init_reqs *)
   (zip P Cs, l', nl', C', False) = (compile_cmd C\<^sub>0 l nl c) \<Longrightarrow>
   length P = length Cs \<Longrightarrow>
   compile_cmd_input_reqs C\<^sub>0 l nl c \<Longrightarrow>
   RL.com_sifum_secure (((0, P), regs), mds\<^sub>0)"
  apply(clarsimp simp:WL.com_sifum_secure_def WL.low_indistinguishable_def)
  apply(clarsimp simp:RL.com_sifum_secure_def RL.low_indistinguishable_def)
  apply(rename_tac mem\<^sub>1 mem\<^sub>2)
  apply(erule_tac x=mem\<^sub>1 in allE)
  apply(erule_tac x=mem\<^sub>2 in allE)
  apply(subgoal_tac "compiled_cmd_init_reqs C\<^sub>0 regs mds\<^sub>0 mem\<^sub>1 \<and> compiled_cmd_init_reqs C\<^sub>0 regs mds\<^sub>0 mem\<^sub>2")
   prefer 2
   using RL.INIT_no_locks_acquired no_locks_acquired_C\<^sub>0_mds\<^sub>0_init_reqs
   apply fastforce
  using INIT\<^sub>r_INIT\<^sub>w apply clarsimp
  apply(erule WL.mm_equiv_elim)
  apply(rename_tac \<B>)
  apply(rule_tac \<R>="R\<^sub>C_of \<B> \<R> \<I>" in RL.mm_equiv_intro)
    using BISIM_REQS\<^sub>w strong_low_bisim_mm_\<B>\<^sub>C_of_\<R>_simple_ev\<^sub>B_eq_\<B> apply blast
   apply blast
  using compile_cmd_correctness_\<B>\<^sub>C_of by blast

(* A (hopefully) more usable version of the per-thread theorem *)
theorem per_thread_compilation_secure:
assumes BISIM_REQS\<^sub>w: "\<forall>\<B>. BISIM_REQS\<^sub>w \<B> \<longrightarrow> WL.bisim_simple_ev\<^sub>B_eq \<B>"
assumes INIT\<^sub>r_INIT\<^sub>w: "\<forall>mem. INIT\<^sub>r mem \<longrightarrow> INIT\<^sub>w mem"
shows
  "WL.com_sifum_secure (c, mds\<^sub>0) \<Longrightarrow>
   let (PCs, l', nl', C', regs') = comp_output
    in (PCs, l', nl', C', False) = (compile_cmd C\<^sub>0 l nl c) \<and>
       P = map fst PCs \<and>
       regs = regs' \<Longrightarrow>
   compile_cmd_input_reqs C\<^sub>0 l nl c \<Longrightarrow>
   RL.com_sifum_secure (((0, P), regs), mds\<^sub>0)"
  apply clarsimp
  using assms compile_cmd_simple_ev\<^sub>B_eq_secure
  by (metis length_map zip_map_fst_snd)

(* NEW: whole-system compilation stuff *)

definition no_lock_mds_cms
where
  "no_lock_mds_cms cms \<equiv> \<forall> mds \<in> snd ` (set cms). no_lock_mds mds"

(* Proof that While-level 'management requirements' for rely-guarantee compatibility are sufficient
  to establish rely-guarantee compatibility for a compiled concurrent program at the RISC level.
  Note that it's only possible to mix the type of (mem|mds)\<^sub>A and (mem|mds)\<^sub>C like this because
  in the While-to-RISC compilation, we've specified var\<^sub>C_of to be 'id' *)
lemma whole_system_global_soundness:
  "length comp_inputs = length cms \<Longrightarrow>
   length init_regs = length cms \<Longrightarrow>
   list_all (\<lambda>((C, l, nl), ((PCs, l', nl', C', regs), (c, mds))).
       compile_cmd_input_reqs C l nl c \<and>
       (PCs, l', nl', C', False) = (compile_cmd C l nl c) \<and>
       compiled_cmd_init_reqs C regs mds mem)
     (zip comp_inputs (zip comp_outputs cms)) \<Longrightarrow>
   WL.no_locks_acquired mem \<Longrightarrow>
   WL.no_lock_mds_gc (cms, mem) \<Longrightarrow>
   WL.lock_managed_vars_mds_mem_correct (cms, mem) \<Longrightarrow>
   WL.unmanaged_var_modes_compatible (cms, mem) \<Longrightarrow>
   cms\<^sub>C = map (\<lambda>((C, l, nl), ((PCs, l', nl', C', regs), (c, mds))).
      (((0, map fst PCs), regs), mds))
     (zip comp_inputs (zip comp_outputs cms)) \<Longrightarrow>
   RL.globally_sound_mode_use (cms\<^sub>C, mem)"
  apply(rule RL.management_requirements_ensure_global_soundness)
    apply(clarsimp simp:list_all_def WL.no_lock_mds_gc_def RL.no_lock_mds_gc_def WL.modes_of_gc_def RL.modes_of_gc_def no_lock_mds_def split:prod.splits)
    apply(metis set_zip_rightD snd_conv)
   apply(fastforce simp:WL.lock_managed_vars_mds_mem_correct_def WL.no_locks_acquired_def WL.modes_of_gc_def
     RL.lock_managed_vars_mds_mem_correct_def RL.no_locks_acquired_def RL.modes_of_gc_def
     no_lock_mds_cms_def lock_not_held_mds_correct_def no_lock_mds_def split:prod.splits)
  apply(fastforce simp:WL.unmanaged_var_modes_compatible_def WL.NoRW_lock_managed_vars_def WL.NoW_lock_managed_vars_def WL.modes_of_gc_def WL.no_locks_acquired_def
    RL.unmanaged_var_modes_compatible_def RL.NoRW_lock_managed_vars_def RL.NoW_lock_managed_vars_def RL.modes_of_gc_def RL.no_locks_acquired_def
    no_lock_mds_cms_def no_lock_mds_def split:prod.splits)
  done

lemma closed_others_\<R>'':
  "(\<langle>c, mds, mem\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds, mem\<rangle>\<^sub>C) \<in> \<R> \<Longrightarrow>
   mem =\<^bsub>mds\<^esub>\<^sup>r mem' \<Longrightarrow>
   (\<langle>c, mds, mem'\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds, mem'\<rangle>\<^sub>C) \<in> \<R>"
  using closed_others_\<R>'
  by (clarsimp simp:mds\<^sub>A_of_def mem\<^sub>A_of_def)

lemma locsound_initial:
  "WL.locally_sound_mode_use \<langle>c, mds, mem\<rangle>\<^sub>A \<Longrightarrow>
   WL.respects_own_guarantees (c, mds)"
  unfolding WL.locally_sound_mode_use_def2
  using WL.loc_reach.refl by fastforce

lemma is_sig\<^sub>A_of_append':
  "sig\<^sub>A'' = sig\<^sub>A @ sig\<^sub>A' \<Longrightarrow>
   sig\<^sub>C'' = sig\<^sub>C @ sig\<^sub>C' \<Longrightarrow>
   is_sig\<^sub>A_of sig\<^sub>A mid sig\<^sub>C \<Longrightarrow>
   if sig\<^sub>A' = [] then WL.lc_havoc_eq mid dst else WL.lc_havoc_eq mid (hd sig\<^sub>A') \<Longrightarrow>
   is_sig\<^sub>A_of sig\<^sub>A' dst sig\<^sub>C' \<Longrightarrow>
   is_sig\<^sub>A_of sig\<^sub>A'' dst sig\<^sub>C''"
  using is_sig\<^sub>A_of_append by blast

lemma \<R>_closed_over_pairwise_reachability'':
  "(\<langle>c, mds, mem\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds, mem\<rangle>\<^sub>C) \<in> \<R> \<Longrightarrow>
   RL.loc_reach_sig \<langle>((pc, P), regs), mds, mem\<rangle>\<^sub>C \<langle>((pc', P'), regs'), mds', mem'\<rangle>\<^sub>C sig \<Longrightarrow>
   \<exists> c'. reachable_pair (\<langle>c, mds, mem\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds, mem\<rangle>\<^sub>C)
                        (\<langle>c', mds', mem'\<rangle>\<^sub>A, \<langle>((pc', P'), regs'), mds', mem'\<rangle>\<^sub>C) sig \<and>
         (\<langle>c', mds', mem'\<rangle>\<^sub>A, \<langle>((pc', P'), regs'), mds', mem'\<rangle>\<^sub>C) \<in> \<R>"
  apply(clarsimp simp:reachable_pair_def lc\<^sub>A_of_def mds\<^sub>A_of_def mem\<^sub>A_of_def)
  apply(induct sig arbitrary:c pc P regs mds mem)
   apply(force dest:closed_others_\<R>'' simp:RL.lc_havoc_eq_def reachable_pair_def WL.lc_havoc_eq_def lc\<^sub>A_of_def mds\<^sub>A_of_def mem\<^sub>A_of_def)
  apply(clarsimp simp:RL.lc_havoc_eq_def reachable_pair_def WL.lc_havoc_eq_def lc\<^sub>A_of_def mds\<^sub>A_of_def mem\<^sub>A_of_def)
  apply(frule RL.eval\<^sub>r_no_modify)
  apply clarsimp
  apply(frule RL.eval\<^sub>r_bounds)
  apply clarsimp
  apply(rename_tac pc regs mds mem''' sig c mem pc'' P regs'' mds'' mem'')
  apply(drule closed_others_\<R>'')
   apply force
  apply(drule \<R>_preservation)
   apply force
  apply clarsimp
  apply(frule conds_\<R>)
  apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def mds\<^sub>A_of_def mem\<^sub>A_of_def)
  apply(rename_tac c'')
  apply(erule_tac x=c'' in meta_allE)
  apply(erule_tac x=pc'' in meta_allE)
  apply(erule_tac x=P in meta_allE)
  apply(erule_tac x=regs'' in meta_allE)
  apply(erule_tac x=mds'' in meta_allE)
  apply(erule_tac x=mem'' in meta_allE)
  apply clarsimp
  apply(rule_tac x=c' in exI)
  apply(case_tac "abs_steps' c (pc, P)")
   apply clarsimp
   apply(erule WL.neval_ZeroE)
   apply clarsimp
   apply(rule_tac x=sig\<^sub>A in exI)
   apply(rule conjI)
    apply(rule_tac sig\<^sub>A="[]" and sig\<^sub>A'=sig\<^sub>A and mid="\<langle>c, mds, mem'''\<rangle>\<^sub>A" and
                   sig\<^sub>C="[\<langle>((pc, P), regs), mds, mem'''\<rangle>\<^sub>C]" and sig\<^sub>C'=sig in is_sig\<^sub>A_of_append')
        apply force
       apply force
      apply force
     apply(clarsimp simp:WL.lc_havoc_eq_def)
     apply(case_tac sig\<^sub>A)
      apply(force simp:WL.lc_havoc_eq_def)
     apply(force simp:WL.lc_havoc_eq_def)
    apply force
   using WL.sig_head_mem_diff RL.mds_havoc_eq_sym
   apply blast
  apply clarsimp
  apply(rule_tac x="\<langle>c, mds, mem'''\<rangle>\<^sub>A # sig\<^sub>A" in exI)
  apply(rule conjI)
   apply(rule_tac sig\<^sub>A="[\<langle>c, mds, mem'''\<rangle>\<^sub>A]" and sig\<^sub>A'=sig\<^sub>A and mid="\<langle>c'', mds'', mem''\<rangle>\<^sub>A" and
                  sig\<^sub>C="[\<langle>((pc, P), regs), mds, mem'''\<rangle>\<^sub>C]" and sig\<^sub>C'=sig in is_sig\<^sub>A_of_append')
       apply force
      apply force
     apply(force simp:lc\<^sub>A_of_def mds\<^sub>A_of_def mem\<^sub>A_of_def)
    apply clarsimp
    apply(case_tac sig\<^sub>A)
     apply clarsimp
    apply clarsimp
   apply clarsimp
  apply(clarsimp simp:WL.lc_havoc_eq_def)
  apply(erule WL.neval_SucE)
  apply clarsimp
  by (metis (no_types, lifting) Nat.add_0_right One_nat_def WL.neval_S_n WL.neval_Suc_simp WL.neval_det abs_steps_not_0_1 add_Suc_right lessI neq0_conv not_add_less2)

(* TODO: Could it have been possible to use locally_sound_mode_use_preservation lemma instead?
  One thing to note is that in proving preserves_local_guarantee_compliance_\<R>',
  we showed that concrete programs related by this particular \<R> comply with their guarantees,
  regardless of whether their abstract program did. That fact might be of stand-alone interest.*)
lemma preserves_locally_sound_mode_use_\<R>:
  "(* WL.locally_sound_mode_use \<langle>c, mds, mem\<rangle>\<^sub>A \<Longrightarrow> *)
   (\<langle>c, mds, mem\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds, mem\<rangle>\<^sub>C) \<in> \<R> \<Longrightarrow>
   RL.locally_sound_mode_use \<langle>((pc, P), regs), mds, mem\<rangle>\<^sub>C"
  apply(clarsimp simp:RL.locally_sound_mode_use_def2)
  apply(clarsimp simp:meta_eq_to_obj_eq[OF RL.loc_reach_iff_sig])
  apply(rename_tac pc' P' regs' mds' mem' sig)
  apply(frule \<R>_closed_over_pairwise_reachability'')
   apply force
  apply clarsimp
  apply(frule_tac c\<^sub>A=c' in preserves_local_guarantee_compliance_\<R>')
  by force

lemma compile_cmd_correctness_\<R>':
  "(zip P Cs, l', nl', C', False) = (compile_cmd C l nl c) \<Longrightarrow>
    compile_cmd_input_reqs C l nl c \<Longrightarrow>
    length P = length Cs \<Longrightarrow>
    compiled_cmd_init_reqs C regs mds mem \<Longrightarrow>
    (\<langle>c, mds, mem\<rangle>\<^sub>A, \<langle>((0, P), regs), mds, mem\<rangle>\<^sub>C) \<in> \<R>"
  using compile_cmd_correctness_\<R> mds\<^sub>A_of_def mem\<^sub>A_of_def
  by force

theorem whole_system_compilation_secure:
assumes BISIM_REQS\<^sub>w: "\<forall>\<B>. BISIM_REQS\<^sub>w \<B> \<longrightarrow> WL.bisim_simple_ev\<^sub>B_eq \<B>"
assumes INIT\<^sub>r_INIT\<^sub>w: "\<forall>mem. INIT\<^sub>r mem \<longrightarrow> INIT\<^sub>w mem"
shows
  "length comp_inputs = length cms \<Longrightarrow>
   length comp_outputs = length cms \<Longrightarrow>
   list_all (\<lambda>((C, l, nl), ((PCs, l', nl', C', regs), (c, mds))).
     (* Actually the LocallySound type system establishes this for ALL memories,
       but here let's be more frugal and ask it only of INIT\<^sub>w mems, because we can. *)
     (\<forall>mem. INIT\<^sub>w mem \<longrightarrow> WL.locally_sound_mode_use \<langle>c, mds, mem\<rangle>\<^sub>A) \<and>
     WL.com_sifum_secure (c, mds) \<and>
     C = C\<^sub>0 \<and>
     mds = mds\<^sub>0 \<and>
     compile_cmd_input_reqs C l nl c \<and>
     (PCs, l', nl', C', False) = (compile_cmd C l nl c))
     (zip comp_inputs (zip comp_outputs cms)) \<Longrightarrow>
   cms\<^sub>C = map (\<lambda>((C, l, nl), ((PCs, l', nl', C', regs), (c, mds))).
      (((0, map fst PCs), regs), mds))
     (zip comp_inputs (zip comp_outputs cms)) \<Longrightarrow>
   RL.prog_sifum_secure_cont cms\<^sub>C"
  apply(rule RL.sifum_compositionality_cont)
   (* Compositional value-dependent noninterference for each thread *)
   apply(clarsimp simp:list_all_def RL.com_sifum_secure_def RL.low_indistinguishable_def split:prod.splits)
   apply(rename_tac C l nl PCs l' nl' C' regs c mds mem\<^sub>1 mem\<^sub>2)
   apply(subgoal_tac "compiled_cmd_init_reqs C regs mds mem\<^sub>1 \<and> compiled_cmd_init_reqs C regs mds mem\<^sub>2")
    prefer 2
    using RL.INIT_no_locks_acquired no_locks_acquired_C\<^sub>0_mds\<^sub>0_init_reqs
    apply fastforce
   apply(subgoal_tac "WL.com_sifum_secure (c, mds)")
    prefer 2
    apply fastforce
   apply(clarsimp simp:WL.com_sifum_secure_def WL.low_indistinguishable_def)
   apply(erule_tac x=mem\<^sub>1 in allE)
   apply(erule_tac x=mem\<^sub>2 in allE)
   apply(clarsimp simp:INIT\<^sub>r_INIT\<^sub>w)
   apply(erule WL.mm_equiv_elim)
   apply(rename_tac \<B>)
   apply(rule_tac \<R>="R\<^sub>C_of \<B> \<R> \<I>" in RL.mm_equiv_intro)
     using BISIM_REQS\<^sub>w strong_low_bisim_mm_\<B>\<^sub>C_of_\<R>_simple_ev\<^sub>B_eq_\<B>
     apply blast
    apply blast
   using compile_cmd_correctness_\<B>\<^sub>C_of
   apply(metis length_map length_map zip_map_fst_snd)
  apply clarsimp
  apply(rule conjI)
   (* Locally sound mode use (guarantee compliance) for each thread *)
   apply(clarsimp simp:list_all_def split:prod.splits)
   apply(rename_tac mem C l nl PCs l' nl' C' regs c mds)
   apply(subgoal_tac "compile_cmd_input_reqs C l nl c \<and>
                      compiled_cmd_init_reqs C regs mds mem \<and>
                      (PCs, l', nl', C', False) = (compile_cmd C l nl c) \<and>
                      WL.locally_sound_mode_use \<langle>c, mds, mem\<rangle>\<^sub>A")
    prefer 2
    using INIT\<^sub>r_INIT\<^sub>w RL.INIT_no_locks_acquired no_locks_acquired_C\<^sub>0_mds\<^sub>0_init_reqs
    apply fastforce
   apply clarify
   apply(subgoal_tac "(\<langle>c, mds, mem\<rangle>\<^sub>A, \<langle>((0, map fst PCs), regs), mds, mem\<rangle>\<^sub>C) \<in> \<R>")
    prefer 2
    apply(rule_tac P="map fst PCs" and Cs="map snd PCs" in compile_cmd_correctness_\<R>')
       apply(force simp:zip_map_fst_snd)
      apply force
     apply force
    apply force
   using preserves_locally_sound_mode_use_\<R>
   apply force
  (* Globally sound mode use (rely-guarantee compatibility) for the entire concurrent program *)
  apply(subgoal_tac "list_all (\<lambda>(c, mds). mds = mds\<^sub>0) cms\<^sub>C")
   prefer 2
   apply(force simp:list_all_def split:prod.splits)
  using RL.INIT_no_locks_acquired no_locks\<^sub>r_mds\<^sub>0_global_soundness
  by blast

(* TODO: Is an adaptation of the whole-system refinement local possible in order
  to try using refined_prog_secure here? It may be that this is not the right place,
  because the instantiation of that locale takes the compiled concurrent program
  itself as a parameter, and we do not have that program yet in this theory. *)
thm CompositionalRefinement.sifum_refinement_sys_init.refined_prog_secure

end
end