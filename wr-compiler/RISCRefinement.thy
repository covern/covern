(*
Author: Robert Sison
*)

section {* Dependent SIFUM-refinement from While to RISC *}

theory RISCRefinement
imports RISCCompiledRegRec
  "~~/src/HOL/Eisbach/Eisbach_Tools"
begin

context risc_compilation
begin

subsection {* Invariant conditions for the refinement relation *}

definition preserves_modes_mem_pair :: "(_, _, _) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
where
  "preserves_modes_mem_pair lc\<^sub>A lc\<^sub>C \<equiv> snd (fst lc\<^sub>A) = mds\<^sub>A_of (snd (fst lc\<^sub>C)) \<and> snd lc\<^sub>A = mem\<^sub>A_of (snd lc\<^sub>C)"

lemma preserves_modes_mem_pair_eq:
  "preserves_modes_mem R \<equiv> \<forall> p \<in> R. preserves_modes_mem_pair (fst p) (snd p)"
  unfolding preserves_modes_mem_def2 preserves_modes_mem_pair_def
  by (rule eq_reflection, fastforce)

definition cmds_stop_consistently :: "
  (('Var, 'Lock, ('Val, 'Var, 'OpId) exp, ('Val, 'Var, 'OpId) exp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  (('Reg, 'Var, 'Lock, 'Val, 'OpId) RL.PerThread, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow> bool"
where
  "cmds_stop_consistently lc\<^sub>A lc\<^sub>C \<equiv>
     (\<exists> lc\<^sub>C'. (lc\<^sub>C, lc\<^sub>C') \<in> RL.eval\<^sub>r) \<longrightarrow> (\<exists> lc\<^sub>A'. WL.neval lc\<^sub>A (abs_steps lc\<^sub>A lc\<^sub>C) lc\<^sub>A')"

(* Note that this is id now that the registers have been moved into the local state, whereas
  previously they were permanently-NoRW "shared" memory. Keep this name for clarity, though. *)
definition regs_noRW_pair :: "(_, _, _) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
where
  "regs_noRW_pair _ lc\<^sub>C \<equiv> \<forall>v\<^sub>C. v\<^sub>C \<notin> range id \<longrightarrow> v\<^sub>C \<in> snd (fst lc\<^sub>C) AsmNoReadOrWrite"

definition \<R>_conds:
  "\<R>_conds lc\<^sub>A lc\<^sub>C \<equiv> preserves_modes_mem_pair lc\<^sub>A lc\<^sub>C \<and>
                      regs_noRW_pair lc\<^sub>A lc\<^sub>C \<and>
                      cmds_stop_consistently lc\<^sub>A lc\<^sub>C \<and>
                      lock_consistent_mds lock_interp (snd (fst lc\<^sub>C))"

lemma \<R>_conds_closed_others:
  "mem\<^sub>C =\<^bsub>mds\<^sub>C\<^esub>\<^sup>r mem\<^sub>C' \<Longrightarrow>
   \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<Longrightarrow>
   \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C"
  apply(clarsimp simp: \<R>_conds)
  apply(rule conjI)
   apply(force simp:preserves_modes_mem_pair_def)
  apply(rule conjI)
   apply(force simp:regs_noRW_pair_def)
  apply(clarsimp simp:cmds_stop_consistently_def abs_steps'_def split:if_splits)
     apply(metis WL.leftmost_while_enabled)
    apply(metis WL.neval_0)
   apply(metis WL.neval_0)
  by (meson RL.eval\<^sub>r_halt_iff_oob WL.eval\<^sub>w_enabled_mem_diff)

lemma \<R>_conds_append:
  "pc < length P \<Longrightarrow>
   \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<Longrightarrow>
   \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P @ P'), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C"
  apply(clarsimp simp: \<R>_conds)
  apply(rule conjI)
   apply(clarsimp simp:preserves_modes_mem_pair_def)
  apply(rule conjI)
   apply(clarsimp simp:regs_noRW_pair_def)
  apply(clarsimp simp:cmds_stop_consistently_def)
  using abs_steps'_head\<^sub>C RL.eval\<^sub>r_halt_iff_oob by force

lemma \<R>_conds_end_append:
  "pc = length P \<Longrightarrow>
   \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<Longrightarrow>
   cmds_stop_consistently \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((length P, P @ P'), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<Longrightarrow>
   \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P @ P'), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C"
  apply(clarsimp simp: \<R>_conds)
  apply(rule conjI)
   apply(clarsimp simp:preserves_modes_mem_pair_def)
  by (clarsimp simp:regs_noRW_pair_def)

lemma \<R>_conds_head:
  "pc < length P \<Longrightarrow>
   \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P @ P'), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<Longrightarrow>
   \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C"
  apply(clarsimp simp: \<R>_conds)
  apply(rule conjI)
   apply(clarsimp simp:preserves_modes_mem_pair_def)
  apply(rule conjI)
   apply(clarsimp simp:regs_noRW_pair_def)
  apply(clarsimp simp:cmds_stop_consistently_def)
  using abs_steps'_head\<^sub>C RL.eval\<^sub>r_halt_iff_oob by force

lemma compiled_expr_eval_preserves_\<R>_conds:
assumes
  a1: "\<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C" and
  a2: "eval_abv\<^sub>C \<langle>((pc, map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C" and
  a3: "compile_expr C {} l e = (P, r, C', False)"
shows
  "\<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc', map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C"
  using a1 a2 a3
  apply -
  apply(frule RL.eval\<^sub>r_bounds)
  apply clarsimp
  apply(subgoal_tac "compile_expr_output_forms (snd (fst (P ! pc)))")
   prefer 2
   apply(metis compile_expr_output_forms)
  apply(clarsimp simp:\<R>_conds)
  apply(frule compiled_expr_inst_properties[rotated])
   apply force
  apply clarsimp
  apply(rule conjI)
   apply(frule compiled_expr_no_abs_written[rotated])
    apply force
   apply(force simp:preserves_modes_mem_pair_def)
  apply(rule conjI)
   apply(force simp:regs_noRW_pair_def)
  apply(clarsimp simp:cmds_stop_consistently_def abs_steps'_def split:if_splits)
   apply(metis WL.neval_0)
  apply(case_tac "pc' < length P")
   apply clarsimp
   apply(subgoal_tac "compile_expr_output_forms (snd (fst (P ! pc')))")
    prefer 2
    apply(metis compile_expr_output_forms)
   apply force
  apply clarsimp
  using RL.eval\<^sub>r_halt_iff_oob
  by (metis eq_imp_le length_map)

subsection {* Consistency between mode state and the variable stability records for a compiled command *}

(* In TypeSystem.thy: (\<S> = (mds AsmNoWrite, mds AsmNoReadOrWrite)) *)
definition asmrec_mds_consistent':: "'Var AsmRec \<Rightarrow> ('Lock, 'Var) Var Mds \<Rightarrow> bool"
where
  "asmrec_mds_consistent' \<S> mds \<equiv> \<S> = ({v. Var v \<in> mds AsmNoWrite}, {v. Var v \<in> mds AsmNoReadOrWrite})"

abbreviation asmrec_mds_consistent
where
  "asmrec_mds_consistent C mds \<equiv> asmrec_mds_consistent' (asmrec C) mds"

lemma compiled_expr_asmrec_constancy_end:
  "compile_expr C A l e = (P, r, C', False) \<Longrightarrow>
   asmrec C = asmrec C'"
  apply(induct e arbitrary: C A l P r C')
    apply(force split:option.splits)
   apply(force simp:Let_def split:option.splits)
  by (force simp:Let_def split:prod.splits if_splits)

lemma compiled_expr_asmrec_constancy_inner:
  "compile_expr C A l e = (P, r, C', False) \<Longrightarrow>
   pc < length P \<Longrightarrow>
   asmrec ((map snd P) ! pc) = asmrec C"
  apply(induct e arbitrary: C A l P r C' pc)
    apply(force split:option.splits)
   apply(force simp:Let_def split:option.splits)
  apply(clarsimp simp:Let_def split:prod.splits if_splits)
    apply(metis compile_expr_empty_maintains_comprec)
   using compiled_expr_asmrec_constancy_end
   apply(metis less_Suc_eq nth_append nth_append_length snd_conv)
  using compiled_expr_asmrec_constancy_end
  by (simp add: nth_append)

lemma compiled_expr_eval_maintains_asmrec_consistency:
  "compile_expr C A l e = (P, r, C', False) \<Longrightarrow>
   asmrec_mds_consistent (snd (P ! pc)) mds\<^sub>C \<Longrightarrow>
   pc < length P \<Longrightarrow>
   eval_abv\<^sub>C \<langle>((pc, map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   if pc' < length P then asmrec_mds_consistent ((map snd P) ! pc') mds\<^sub>C'
                     else asmrec_mds_consistent C' mds\<^sub>C'"
  apply(subgoal_tac "pc' = Suc pc")
   prefer 2
   using compiled_expr_only_increments[where P'="[]", simplified]
   apply blast
  apply(clarsimp split del:if_split)
  apply clarsimp
  apply(frule_tac pc=pc in compiled_expr_asmrec_constancy_inner)
   apply force
  apply(frule compiled_expr_no_modes[where P'="[]"])
    apply force
   apply force
  apply clarsimp
  apply(rule conjI)
   apply(force dest:compiled_expr_asmrec_constancy_inner)
  by (force dest: compiled_expr_asmrec_constancy_end)

subsection {* Static check that a command contains only stable expressions *}

definition lock_governed :: "'Var \<Rightarrow> bool"
where
  "lock_governed v \<equiv> \<exists>l. v \<in> (fst (lock_interp l) \<union> snd (lock_interp l))"

primrec banned_unstable_exprs :: "('Var, 'Lock, ('Val, 'Var, 'OpId) exp, ('Val, 'Var, 'OpId) exp) Stmt \<Rightarrow>
  'Var AsmRec \<Rightarrow> bool \<times> 'Var AsmRec"
where
  "banned_unstable_exprs (Stmt.LockAcq l) \<S> =
     (False, (fst \<S> \<union> fst (lock_interp l), snd \<S> \<union> snd (lock_interp l)))" |
  "banned_unstable_exprs (Stmt.LockRel l) \<S> =
     (False, (fst \<S> - fst (lock_interp l), snd \<S> - snd (lock_interp l)))" |
  "banned_unstable_exprs Stop \<S> = (False, \<S>)" |
  "banned_unstable_exprs (Seq a b) \<S> =
     (let (a_unstable, \<S>') = (banned_unstable_exprs a \<S>);
          (b_unstable, \<S>'') = (banned_unstable_exprs b \<S>')
       in (a_unstable \<or> b_unstable, \<S>''))" |
  "banned_unstable_exprs (If e a b) \<S> =
     (let (a_unstable, \<S>\<^sub>a) = (banned_unstable_exprs a \<S>);
          (b_unstable, \<S>\<^sub>b) = (banned_unstable_exprs b \<S>)
       in ((\<not> asmrec_expr_stable' \<S> e) \<or>
           \<S>\<^sub>a \<noteq> \<S>\<^sub>b \<or> (* Let's roll the branch stability inconsistency check into here too *)
           (a_unstable \<or> b_unstable), \<S>\<^sub>b))" |
  "banned_unstable_exprs (While e c) \<S> =
     (let (c_unstable, \<S>') = (banned_unstable_exprs c \<S>)
       in ((\<not> asmrec_expr_stable' \<S> e) \<or>
           \<S> \<noteq> \<S>' \<or> (* Assert that the loop body restores the original mds *)
           c_unstable, \<S>'))" |
  "banned_unstable_exprs (Assign v e) \<S> =
     ((\<not> asmrec_expr_stable' \<S> e) \<or>
      (* Assert that the var assigned to is not lock-governed if unstable.
        i.e. ban assignments to unstable, lock-governed variables
        NB: This is arguably a separate concern, but it's convenient to put it here. *)
      (lock_governed v \<and> (\<not> asmrec_var_stable' \<S> v)), \<S>)" |
  "banned_unstable_exprs Skip \<S> = (False, \<S>)"

definition
  "no_unstable_exprs c C \<equiv> \<not> fst (banned_unstable_exprs c (asmrec C))"

lemma compile_cmd_consistent_banned_unstable_exprs:
  "(compile_cmd C l nl c) = (P, l', nl', C', False) \<Longrightarrow>
   \<not> banned_WhileLang_forms c \<Longrightarrow>
   banned_unstable_exprs c (asmrec C) = (False, \<S>) \<Longrightarrow>
   asmrec C' = \<S>"
  apply(induct c arbitrary: P l' nl' C' C l nl \<S>)
         apply(fastforce split:prod.splits simp:compiled_expr_asmrec_constancy_end Let_def)
        apply force
       apply force
      apply force
     apply(fastforce split:prod.splits)
    apply(fastforce split:prod.splits dest:compiled_expr_asmrec_constancy_end)
   apply(fastforce split:prod.splits dest:compiled_expr_asmrec_constancy_end)
  by force

subsection {* Input requirements for command compilation *}

definition regrec_stable
where
  "regrec_stable C \<equiv> \<forall>e \<in> ran (regrec C). asmrec_expr_stable C e"

definition compile_cmd_input_reqs
where
  "compile_cmd_input_reqs C l nl c \<equiv> \<not> banned_WhileLang_forms c \<and>
                                     (\<forall>x. l = Some x \<longrightarrow> x < nl) \<and>
                                     no_unstable_exprs c C \<and>
                                     regrec_stable C \<and>
                                     finite (dom (regrec C))"

subsection {* That register records refer only to stable variables *}

lemma asmrec_subexpr_stable:
  "asmrec_expr_stable C (EOp opid e\<^sub>1 e\<^sub>2) \<Longrightarrow> asmrec_expr_stable C e\<^sub>1 \<and> asmrec_expr_stable C e\<^sub>2"
  by (clarsimp simp:asmrec_expr_stable'_def)

lemma regrec_le_asmrec_consistent:
  "regrec_stable C \<Longrightarrow> regrec C' \<subseteq>\<^sub>m regrec C \<Longrightarrow> asmrec C' = asmrec C \<Longrightarrow> regrec_stable C'"
  apply(clarsimp simp:regrec_stable_def map_le_def asmrec_expr_stable'_def ran_def)
  by (metis (no_types, lifting) domI)

lemma regrec_purge_leq:
  "\<forall>v. regrec_purge_var \<Phi> v \<subseteq>\<^sub>m \<Phi>"
  by (clarsimp simp:map_le_def regrec_purge_var_def)

lemma regrec_purge_leq':
  "\<forall>v. regrec_purge_var (regrec C) v \<subseteq>\<^sub>m (regrec C)"
  using regrec_purge_leq by simp

lemma regrec_add_stable:
  "regrec_stable C \<Longrightarrow> asmrec_expr_stable C e \<Longrightarrow> regrec_stable (C\<lparr>regrec := (regrec C)(r \<mapsto> e)\<rparr>)"
  apply(clarsimp simp:regrec_stable_def asmrec_expr_stable'_def asmrec_var_stable'_def ran_def)
  by (metis (mono_tags))

lemma compiled_expr_maintains_regrec_stability:
  "compile_expr C A l e = (P, r, C', False) \<Longrightarrow>
   regrec_stable C \<Longrightarrow>
   asmrec_expr_stable C e \<Longrightarrow>
   if pc < length P then regrec_stable ((map snd P) ! pc)
                     else regrec_stable C'"
  apply(induct e arbitrary: C A l P r C' pc)
    apply(force simp:Let_def regrec_stable_def asmrec_expr_stable'_def ran_def split:if_splits option.splits)
   apply(force simp:Let_def regrec_stable_def asmrec_expr_stable'_def ran_def split:if_splits option.splits)
  apply(clarsimp split:prod.splits)
  apply(rule conjI)
   apply(fastforce dest:compile_expr_empty_maintains_comprec simp:regrec_stable_def nth_append ran_def)
  apply clarsimp
  apply(frule asmrec_subexpr_stable)
  apply(rule conjI)
   apply clarsimp
   apply(clarsimp simp:nth_append split:if_splits)
    apply(metis compile_expr_empty_maintains_comprec nth_map)
   apply(metis compiled_expr_asmrec_constancy_end nth_map)
  apply(clarsimp simp:nth_append split:if_splits)
   apply(frule compile_expr_empty_maintains_comprec)
   apply clarsimp
   apply(rename_tac C' fail')
   apply(subgoal_tac "regrec_stable C'")
    prefer 2
    apply(meson finite_insert less_Suc_eq)
   apply(clarsimp simp:asmrec_expr_stable'_def regrec_stable_def ran_def)
   apply(metis (no_types, lifting) compiled_expr_asmrec_constancy_end exp_vars.simps(3))
  apply(rename_tac C\<^sub>1 fail\<^sub>1 P\<^sub>2 r\<^sub>2 C\<^sub>2 fail\<^sub>2)
  apply(subgoal_tac "regrec_stable C\<^sub>1")
   prefer 2
   apply(meson not_add_less1)
  apply(subgoal_tac "regrec_stable C\<^sub>2")
   prefer 2
   apply(metis compiled_expr_asmrec_constancy_end not_add_less2)
  apply(clarsimp simp:asmrec_expr_stable'_def regrec_stable_def ran_def)
  by (metis (no_types, lifting) compiled_expr_asmrec_constancy_end exp_vars.simps(3))

lemma compiled_expr_maintains_regrec_stability':
  "(P, r, C', False) = compile_expr C A l e \<Longrightarrow>
   regrec_stable C \<Longrightarrow>
   asmrec_expr_stable C e \<Longrightarrow>
   if pc < length P then regrec_stable ((map snd P) ! pc)
                     else regrec_stable C'"
  using compiled_expr_maintains_regrec_stability
  by metis

lemma compiled_cmd_maintains_regrec_stability:
  "(zip P Cs, l', nl', C', False) = (compile_cmd C l nl c) \<Longrightarrow>
   compile_cmd_input_reqs C l nl c \<Longrightarrow>
   length P = length Cs \<Longrightarrow>
   if pc < length P then regrec_stable (Cs ! pc)
                     else regrec_stable C'"
  unfolding compile_cmd_input_reqs_def
  apply(induct c arbitrary: P Cs l' nl' C' C l nl pc)
         (* Assign *)
         apply(clarsimp simp:zip_eq_conv Let_def RL.lc_havoc_eq_def split:prod.splits split del:if_split)
         apply(rename_tac v e C l nl pc P\<^sub>e r C\<^sub>e fail\<^sub>e)
         apply(case_tac "pc < Suc (length P\<^sub>e)")
          apply clarsimp
          apply(frule_tac pc=pc in compiled_expr_maintains_regrec_stability)
            apply force
           apply(force simp:no_unstable_exprs_def)
          apply(force simp:nth_append)
         apply(clarsimp simp:no_unstable_exprs_def)
         apply(subgoal_tac "regrec_stable C\<^sub>e")
          prefer 2
          apply(metis compiled_expr_maintains_regrec_stability' less_Suc_eq)
         apply(frule_tac C=C\<^sub>e and C'="C\<^sub>e\<lparr>regrec := regrec_purge_var (regrec C\<^sub>e) v\<rparr>"
           in regrec_le_asmrec_consistent)
           apply clarsimp
           using regrec_purge_leq'
           apply(erule_tac x=C\<^sub>e in meta_allE)
           apply(erule_tac x="v" in allE)
           apply force
          apply force
         apply(rule conjI)
          apply clarsimp
          apply(subgoal_tac "asmrec_var_stable' (asmrec C\<^sub>e) v")
           prefer 2
           apply(force simp: compiled_expr_asmrec_constancy_end)
          apply(frule_tac C="C\<^sub>e\<lparr>regrec := regrec_purge_var (regrec C\<^sub>e) v\<rparr>" and
                          e="ELoad v" and r=r in regrec_add_stable)
           apply(clarsimp simp:asmrec_expr_stable'_def)
          apply force
         apply force
        (* Skip *)
        apply(solves \<open>clarsimp simp:zip_eq_conv\<close>)
       (* LockAcq *)
       apply(clarsimp simp:zip_eq_conv regrec_stable_def asmrec_expr_stable'_def asmrec_var_stable'_def)
       apply fastforce
      (* LockRel *)
      apply(clarsimp simp:zip_eq_conv regrec_stable_def asmrec_expr_stable'_def asmrec_var_stable'_def)
      apply(fastforce simp:regrec_purge_vars_def restrict_map_def ran_def exp_vars_and_\<C>s_def split:if_splits)
     (* Seq *)
     apply(clarsimp simp:zip_eq_conv split:prod.splits)
     apply(rename_tac c1 c2 C l nl pc P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 l' nl' C' fail')
     apply(subgoal_tac "\<forall>x. l\<^sub>1 = Some x \<longrightarrow> x < nl\<^sub>1")
      prefer 2
      using compile_cmd_maintains_labelrec_freshness
      apply force
     apply(subgoal_tac "RL.joinable (map fst P\<^sub>1) (map fst P\<^sub>2)")
      prefer 2
      using compiled_cmds_joinable
      apply force
     apply(clarsimp simp:RL.joinable_def)
     apply(subgoal_tac "no_unstable_exprs c1 C")
      prefer 2
      apply(force simp:no_unstable_exprs_def split:prod.splits)
     apply(subgoal_tac "no_unstable_exprs c2 C\<^sub>1")
      prefer 2
      apply(force simp:no_unstable_exprs_def compile_cmd_consistent_banned_unstable_exprs split:prod.splits)
     (* in P\<^sub>1 *)
     apply(erule_tac x="map fst P\<^sub>1" in meta_allE)
     apply(erule_tac x="map snd P\<^sub>1" in meta_allE)
     apply(erule_tac x="l\<^sub>1" in meta_allE)
     apply(erule_tac x="nl\<^sub>1" in meta_allE)
     apply(erule_tac x="C\<^sub>1" in meta_allE)
     apply(erule_tac x="C" in meta_allE)
     apply(erule_tac x="l" in meta_allE)
     apply(erule_tac x="nl" in meta_allE)
     apply(erule_tac x="pc" in meta_allE)
     apply(clarsimp simp:zip_map_fst_snd nth_append)
     apply(case_tac "pc < length P\<^sub>1")
      apply(force simp:nth_append)
     apply clarsimp
     (* in P\<^sub>2 *)
     apply(erule_tac x="map fst P\<^sub>2" in meta_allE)
     apply(erule_tac x="map snd P\<^sub>2" in meta_allE)
     apply(erule_tac x="l'" in meta_allE)
     apply(erule_tac x="nl'" in meta_allE)
     apply(erule_tac x="C'" in meta_allE)
     apply(erule_tac x="C\<^sub>1" in meta_allE)
     apply(erule_tac x="l\<^sub>1" in meta_allE)
     apply(erule_tac x="nl\<^sub>1" in meta_allE)
     apply(erule_tac x="pc - length (map fst P\<^sub>1)" in meta_allE)
     apply(clarsimp simp:zip_map_fst_snd nth_append)
     apply(frule compile_cmd_preserves_finite_regrec)
       apply force
      apply force
     apply(case_tac "pc - length (map fst P\<^sub>1) < length (map fst P\<^sub>2)")
      apply(force simp:nth_append)
     apply force
    (* If *)
    apply(clarsimp simp:zip_eq_conv Let_def RL.lc_havoc_eq_def split:prod.splits)
    apply(rename_tac b c\<^sub>1 c\<^sub>2 C l nl pc P\<^sub>0 r C\<^sub>1 fail\<^sub>1 br nl' ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 fail\<^sub>2 P\<^sub>2 l\<^sub>2 nl\<^sub>2 C\<^sub>3 fail\<^sub>3)
    apply(subgoal_tac "no_unstable_exprs c\<^sub>1 C\<^sub>1 \<and> no_unstable_exprs c\<^sub>2 C\<^sub>1 \<and> asmrec C\<^sub>2 = asmrec C\<^sub>3")
     prefer 2
     apply(force simp:no_unstable_exprs_def compiled_expr_asmrec_constancy_end compile_cmd_consistent_banned_unstable_exprs split:prod.splits)
    apply(frule_tac pc=pc in compiled_expr_maintains_regrec_stability)
      apply force
     apply(force simp:no_unstable_exprs_def split:prod.splits)
    apply(case_tac "pc < length P\<^sub>0")
     (* Case: in the compiled boolean expr *)
     apply(subgoal_tac "compile_expr_output_forms (snd (fst (P\<^sub>0 ! pc)))")
      prefer 2
      apply(metis compile_expr_output_forms)
      apply clarsimp
     apply(force simp:nth_append)
    (* Case: at the Jz br *)
    apply(case_tac "pc = length P\<^sub>0")
     apply(force simp:nth_append)
    (* Case: in P\<^sub>1 *)
    apply(subgoal_tac "pc > length P\<^sub>0")
     prefer 2
     apply force
    apply clarsimp
    apply(erule_tac x="map fst P\<^sub>1" in meta_allE)
    apply(erule_tac x="map snd P\<^sub>1" in meta_allE)
    apply(erule_tac x="l\<^sub>1" in meta_allE)
    apply(erule_tac x="nl\<^sub>1" in meta_allE)
    apply(erule_tac x="C\<^sub>2" in meta_allE)
    apply(erule_tac x="C\<^sub>1" in meta_allE)
    apply(erule_tac x="None" in meta_allE)
    apply(erule_tac x="nl''" in meta_allE)
    apply(erule_tac x="pc - Suc (length P\<^sub>0)" in meta_allE)
    apply(frule compile_expr_preserves_finite_regrec)
     apply force
    apply(clarsimp simp:zip_map_fst_snd nth_append)
    apply(case_tac "pc < Suc (length P\<^sub>0 + length P\<^sub>1)")
     apply(subgoal_tac "pc - Suc (length P\<^sub>0) < length P\<^sub>1")
      prefer 2
      apply force
     apply force
    (* Case: at the Jmp ex *)
    apply(case_tac "pc = Suc (length P\<^sub>0 + length P\<^sub>1)")
     apply(force simp:nth_append)
    (* Case: in P\<^sub>2 *)
    apply(subgoal_tac "pc \<ge> Suc (Suc (length P\<^sub>0 + length P\<^sub>1))")
     prefer 2
     apply force
    apply clarsimp
    apply(erule_tac x="map fst P\<^sub>2" in meta_allE)
    apply(erule_tac x="map snd P\<^sub>2" in meta_allE)
    apply(erule_tac x="l\<^sub>2" in meta_allE)
    apply(erule_tac x="nl\<^sub>2" in meta_allE)
    apply(erule_tac x="C\<^sub>3" in meta_allE)
    apply(erule_tac x="C\<^sub>1" in meta_allE)
    apply(erule_tac x="Some br" in meta_allE)
    apply(erule_tac x="nl\<^sub>1" in meta_allE)
    apply(erule_tac x="pc - Suc (Suc (length P\<^sub>0 + length P\<^sub>1))" in meta_allE)
    apply(clarsimp simp:zip_map_fst_snd nth_append)
    apply(subgoal_tac "br < nl\<^sub>1")
     prefer 2
     apply(clarsimp simp:RL.fresh_label_def)
     apply(meson Suc_leD Suc_le_lessD compile_cmd_output_labelrec)
    apply(case_tac "pc < Suc (Suc (length P\<^sub>0 + (length P\<^sub>1 + length P\<^sub>2)))")
     apply(subgoal_tac "pc - Suc (Suc (length P\<^sub>0 + length P\<^sub>1)) < length P\<^sub>2")
      prefer 2
      apply force
     apply force
    (* Case: at final Tau *)
    apply clarsimp
    apply(case_tac "pc = Suc (Suc (length P\<^sub>0 + (length P\<^sub>1 + length P\<^sub>2)))")
     apply(force simp:nth_append)
    (* Case: end *)
    apply clarsimp
    apply(rule_tac C=C\<^sub>3 in regrec_le_asmrec_consistent)
      apply force
     apply clarsimp
     apply(metis (no_types) regrec_inter_le regrec_inter_sym)
    apply force
   (* While *)
   apply(clarsimp simp:zip_eq_conv split:prod.splits split del:if_split)
   apply(rename_tac b c C l nl pc lp nl' ex nl'' P\<^sub>e r C\<^sub>B fail\<^sub>e P l' nl''' C\<^sub>E fail\<^sub>c)
   apply(clarsimp simp:RL.fresh_label_def split del:if_split)
   apply(subgoal_tac "asmrec C = asmrec C\<^sub>B")
    prefer 2
    apply(force dest:compiled_expr_asmrec_constancy_end)
   apply(subgoal_tac "no_unstable_exprs c C \<and> asmrec C\<^sub>B = asmrec C\<^sub>E")
    prefer 2
    apply(force simp:no_unstable_exprs_def compiled_expr_asmrec_constancy_end compile_cmd_consistent_banned_unstable_exprs split:prod.splits)
   apply(frule_tac pc=pc in compiled_expr_maintains_regrec_stability)
     apply(force simp:regrec_stable_def)
    apply(force simp:no_unstable_exprs_def split:prod.splits)
   apply(case_tac "pc < length P\<^sub>e")
    (* Case: in the 1st compiled boolean expr *)
    apply(subgoal_tac "compile_expr_output_forms (snd (fst (P\<^sub>e ! pc)))")
     prefer 2
     apply(metis (no_types, lifting) compile_expr_output_forms)
    apply(force simp:nth_append)
   (* Case: at the Jz ex *)
   apply(case_tac "pc = length P\<^sub>e")
    apply(force simp:nth_append)
   (* Case: In the inner command *)
   apply(erule_tac x="map fst P" in meta_allE)
   apply(erule_tac x="map snd P" in meta_allE)
   apply(erule_tac x="l'" in meta_allE)
   apply(erule_tac x="nl'''" in meta_allE)
   apply(erule_tac x="C\<^sub>E" in meta_allE)
   apply(erule_tac x="C\<^sub>B" in meta_allE)
   apply(erule_tac x="None" in meta_allE)
   apply(erule_tac x="Suc ex" in meta_allE)
   apply(erule_tac x="pc - Suc (length P\<^sub>e)" in meta_allE)
   apply(clarsimp simp:zip_map_fst_snd nth_append)
   apply(subgoal_tac "finite (dom (regrec C\<^sub>B))")
    prefer 2
    apply(rule_tac C="C\<lparr>regrec := Map.empty\<rparr>" in compile_expr_preserves_finite_regrec)
     apply force
    apply force
   apply(case_tac "pc < (Suc (length P\<^sub>e + (length P)))")
    apply(force simp:no_unstable_exprs_def )
   (* Case: at the Jmp *)
   apply(case_tac "pc = (Suc (length P\<^sub>e + (length P)))")
    apply(force simp:no_unstable_exprs_def)
   apply(force simp:regrec_stable_def)
  (* Stop *)
  by force

lemma compiled_cmd_maintains_regrec_stability':
  "(P, l', nl', C', False) = (compile_cmd C l nl c) \<Longrightarrow>
   compile_cmd_input_reqs C l nl c \<Longrightarrow>
   if pc < length P then regrec_stable (map snd P ! pc)
                     else regrec_stable C'"
  using compiled_cmd_maintains_regrec_stability
  by (metis (no_types, lifting) length_map zip_map_fst_snd)

subsection {* Helpers for command sequencing *}

lemma neval_seq:
  "WL.neval \<langle>c\<^sub>1, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A n \<langle>c\<^sub>1', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A \<Longrightarrow>
   \<exists>a ba bb. WL.neval \<langle>c\<^sub>1 ;; c\<^sub>2, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A n \<langle>a, ba, bb\<rangle>\<^sub>A"
  apply(induct "\<langle>c\<^sub>1, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A" n "\<langle>c\<^sub>1', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A" arbitrary:c\<^sub>1 mds\<^sub>A mem\<^sub>A rule:WL.neval.induct)
   apply clarsimp
   using WL.neval_0 apply blast
  apply clarsimp
  by (metis (mono_tags) WL.neval.cases WL.neval_S_n WL.neval_Suc_simp WL.eval\<^sub>w.seq1 WL.eval\<^sub>w.seq2 prod.collapse WL.stop_no_eval)

lemma neval_seq1:
  "WL.neval \<langle>c\<^sub>1, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A n \<langle>c\<^sub>1', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A \<Longrightarrow>
   c\<^sub>1' \<noteq> Stop \<Longrightarrow>
   WL.neval \<langle>c\<^sub>1 ;; c\<^sub>2, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A n \<langle>c\<^sub>1' ;; c\<^sub>2, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A"
  apply(induct "\<langle>c\<^sub>1, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A" n "\<langle>c\<^sub>1', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A" arbitrary:c\<^sub>1 mds\<^sub>A mem\<^sub>A c\<^sub>1' rule:WL.neval.induct)
   apply clarsimp
   using WL.neval_0 apply blast
  apply clarsimp
  apply(rename_tac c'' mds'' mem'' n c\<^sub>1 mds\<^sub>A mem\<^sub>A c\<^sub>1')
  apply(erule_tac x=c'' in meta_allE)
  apply(erule_tac x=mds'' in meta_allE)
  apply(erule_tac x=mem'' in meta_allE)
  apply clarsimp
  apply(subgoal_tac "eval_abv\<^sub>A \<langle>c\<^sub>1 ;; c\<^sub>2, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>c'' ;; c\<^sub>2, mds'', mem''\<rangle>\<^sub>A")
   prefer 2
   apply(case_tac "c'' = Stop")
    apply clarsimp
    using WL.neval.cases apply force
   apply(erule WL.eval\<^sub>w.seq1)
   apply force
  by (force simp:WL.neval_S_n)

definition cmd_seq
where
  "cmd_seq c c' \<equiv> if c = Stop then c' else c ;; c'"

lemma neval_cmd_seq:
  "WL.neval \<langle>c\<^sub>1, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A n \<langle>c\<^sub>1', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A \<Longrightarrow>
   WL.neval \<langle>cmd_seq c\<^sub>1 c\<^sub>2, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A n \<langle>cmd_seq c\<^sub>1' c\<^sub>2, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A"
  apply(induct "\<langle>c\<^sub>1, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A" n "\<langle>c\<^sub>1', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A" arbitrary:c\<^sub>1 mds\<^sub>A mem\<^sub>A c\<^sub>1' rule:WL.neval.induct)
   apply(clarsimp simp:cmd_seq_def)
   using WL.neval_0 apply blast
  apply clarsimp
  apply(rename_tac c'' mds'' mem'' n c\<^sub>1 mds\<^sub>A mem\<^sub>A c\<^sub>1')
  apply(erule_tac x=c'' in meta_allE)
  apply(erule_tac x=mds'' in meta_allE)
  apply(erule_tac x=mem'' in meta_allE)
  apply clarsimp
  apply(subgoal_tac "eval_abv\<^sub>A \<langle>cmd_seq c\<^sub>1 c\<^sub>2, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>cmd_seq c'' c\<^sub>2, mds'', mem''\<rangle>\<^sub>A")
   prefer 2
   apply(force simp:cmd_seq_def WL.eval\<^sub>w.seq1 WL.eval\<^sub>w.seq2)
  by (force simp:WL.neval_S_n)

subsection {* Refinement relation specification *}

definition compiled_cmd_init_reqs
where
  "compiled_cmd_init_reqs C regs mds\<^sub>C mem\<^sub>C \<equiv>
     (* The starting mem has to be consistent with the starting regrec *)
     regrec_mem_consistent C regs mem\<^sub>C \<and>
     (* The starting mds has to be consistent reflect new_vars_private (i.e. the regs).
       Note this one now mostly gets simplified away trivially because of id *)
     (\<forall>v\<^sub>C. v\<^sub>C \<notin> range id \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite) \<and>
     (* The starting mds has to be consistent with the starting asmrec *)
     asmrec_mds_consistent C mds\<^sub>C \<and>
     (* Evaluation ought to ensure this is invariant *)
     lock_consistent_mds lock_interp mds\<^sub>C"

(* We actually end up deriving this for most cases.
  The last remaining case that mentions it is epilogue_step, and getting rid of it there might
  not actually be desirable if it forces us to add restrictions that are arguably unnecessary.
  e.g. saying that Suc pc = length P or something like that. *)
abbreviation closed_over_pairwise_reachability :: "
  (('Var, 'Lock, ('Val, 'Var, 'OpId) exp, ('Val, 'Var, 'OpId) exp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  ((('Reg, 'Var, 'Lock, 'Val, 'OpId) RL.PerThread), ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  ((('Var, 'Lock, ('Val, 'Var, 'OpId) exp, ('Val, 'Var, 'OpId) exp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<times>
   ((('Reg, 'Var, 'Lock, 'Val, 'OpId) RL.PerThread), ('Lock, 'Var) Var, 'Val) LocalConf) set \<Rightarrow>
  bool"
where
  "closed_over_pairwise_reachability lc\<^sub>A lc\<^sub>C R \<equiv> 
     (\<forall>sig\<^sub>C lc\<^sub>A' lc\<^sub>C'.
       length sig\<^sub>C > 0 \<and> (* this disallows mem havoc without an eval step *)
       reachable_pair (lc\<^sub>A, lc\<^sub>C) (lc\<^sub>A', lc\<^sub>C') sig\<^sub>C \<longrightarrow> (lc\<^sub>A', lc\<^sub>C') \<in> R)"

inductive_set \<R> :: "(
  (('Var, 'Lock, ('Val, 'Var, 'OpId) exp, ('Val, 'Var, 'OpId) exp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf
  \<times>
  ((('Reg, 'Var, 'Lock, 'Val, 'OpId) RL.PerThread), ('Lock, 'Var) Var, 'Val) LocalConf
  ) set"
where
  seq: "\<lbrakk>c\<^sub>A = cmd_seq c\<^sub>1\<^sub>A c\<^sub>2\<^sub>A;
    (P', l\<^sub>2, nl\<^sub>2, C\<^sub>2, False) = compile_cmd C l nl (c\<^sub>1\<^sub>A' ;; c\<^sub>2\<^sub>A);
    compile_cmd_input_reqs C l nl (c\<^sub>1\<^sub>A' ;; c\<^sub>2\<^sub>A);
    (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>1, False) = compile_cmd C l nl c\<^sub>1\<^sub>A';
    (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>2, False) = compile_cmd C\<^sub>1 l\<^sub>1 nl\<^sub>1 c\<^sub>2\<^sub>A;
    pc < length P\<^sub>1;
    P = (map fst P\<^sub>1) @ (map fst P\<^sub>2);
    RL.joinable (map fst P\<^sub>1) (map fst P\<^sub>2);
    c\<^sub>2\<^sub>A \<noteq> Stop;
    P\<^sub>2 \<noteq> []; (* TODO: Possible to derive this from compiled_cmd_nonempty? *)
    (\<langle>c\<^sub>1\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, map fst P\<^sub>1), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>;
    regrec_mem_consistent (map snd P' ! pc) regs mem\<^sub>C;
    asmrec_mds_consistent (map snd P' ! pc) mds\<^sub>C;
    regrec_stable (map snd P' ! pc);
    \<forall> mds\<^sub>C' mem\<^sub>C' regs'.
        compiled_cmd_init_reqs C\<^sub>1 regs' mds\<^sub>C' mem\<^sub>C' \<and>
        regrec_stable C\<^sub>1 \<longrightarrow>
        (\<langle>c\<^sub>2\<^sub>A, mds\<^sub>A_of mds\<^sub>C', mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>((0, map fst P\<^sub>2), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> \<R>
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  join: "\<lbrakk> pc = length P\<^sub>1 + pc\<^sub>2;
    pc\<^sub>2 < length P\<^sub>2;
    P = P\<^sub>1 @ P\<^sub>2;
    RL.joinable P\<^sub>1 P\<^sub>2;
    P\<^sub>2 \<noteq> [];
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc\<^sub>2, P\<^sub>2), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  stop: "\<lbrakk>c\<^sub>A = Stop;
    pc = length P;
    \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  skip_nop: "\<lbrakk>c\<^sub>A = Skip;
    Suc pc = length P;
    snd (P ! pc) = Nop;
    \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  assign_expr: "\<lbrakk>c\<^sub>A = (v \<leftarrow> e);
    (* Instructions that correspond to the expression evaluation *)
    (P', l', nl\<^sub>2, C', False) = compile_cmd C l nl c\<^sub>A;
    compile_cmd_input_reqs C l nl c\<^sub>A;
    P = map fst P';
    (* explicitly lay out some of the (otherwise derivable) detail so we can refer to it *)
    (P\<^sub>e, r, C\<^sub>e, False) = compile_expr C {} l e;
    pc < length P\<^sub>e;
    (* Instructions are paired with the record that should hold when the pc is at the instruction,
       before executing it. *)
    (* We consider only memories consistent with the intermediate regrecs *)
    (* Note: this definition considers all expressions tracked by the regrec,
       not just the ones relevant to the expression being compiled *)
    regrec_mem_consistent (map snd P\<^sub>e ! pc) regs mem\<^sub>C;
    (* We consider only mode states consistent with the intermediate asmrecs *)
    asmrec_mds_consistent (map snd P\<^sub>e ! pc) mds\<^sub>C;
    (* The expr being compiled must only refer to stable variables *)
    asmrec_expr_stable C e;
    (* The var assigned to must be stable if lock-governed, and non-lock-governed if unstable *)
    lock_governed v \<longrightarrow> asmrec_var_stable C v;
    finite (dom (regrec C));
    \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  assign_store: "\<lbrakk>c\<^sub>A = (v \<leftarrow> e);
    (* The Store instruction that 'commits' the evaluated expression to memory *)
    (P', l', nl\<^sub>2, C', False) = compile_cmd C l nl c\<^sub>A;
    compile_cmd_input_reqs C l nl c\<^sub>A;
    P = map fst P';
    (* explicitly lay out some of the (otherwise derivable) detail so we can refer to it *)
    (P\<^sub>e, r, C\<^sub>e, False) = compile_expr C {} l e;
    Suc pc = length P;
    (* We consider only memories consistent with the expression sequence's final register state *)
    regrec_mem_consistent C\<^sub>e regs mem\<^sub>C;
    (* We consider only mode states consistent with the intermediate asmrecs *)
    asmrec_mds_consistent C\<^sub>e mds\<^sub>C;
    (* The expr being compiled must only refer to stable variables *)
    asmrec_expr_stable C e;
    (* The var assigned to must be stable if lock-governed, and non-lock-governed if unstable *)
    lock_governed v \<longrightarrow> asmrec_var_stable C v;
    finite (dom (regrec C));
    \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  if_expr: "\<lbrakk>c\<^sub>A = If e c\<^sub>1 c\<^sub>2;
    (* This case covers all instructions in the expression, as well as the Jz at the end of it *)
    (P', l', nl\<^sub>2, C', False) = compile_cmd C l nl c\<^sub>A;
    compile_cmd_input_reqs C l nl c\<^sub>A;
    P = map fst P';
    (* explicitly lay out (otherwise derivable) detail so we can refer to P\<^sub>e length *)
    (P\<^sub>e, r, C\<^sub>1, False) = compile_expr C {} l e;
    (br, nl') = RL.fresh_label nl;
    (ex, nl'') = RL.fresh_label nl';
    (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) = compile_cmd C\<^sub>1 None nl'' c\<^sub>1;
    (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) = (compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2);
    pc \<le> length P\<^sub>e;
    regrec_mem_consistent (map snd P' ! pc) regs mem\<^sub>C;
    asmrec_mds_consistent (map snd P' ! pc) mds\<^sub>C;
    regrec_stable (map snd P' ! pc);
    \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C;
    \<forall> mds\<^sub>C' mem\<^sub>C' regs'.
        compiled_cmd_init_reqs C\<^sub>1 regs' mds\<^sub>C' mem\<^sub>C' \<and>
        regrec_stable C\<^sub>1 \<longrightarrow>
        ((\<langle>c\<^sub>1, mds\<^sub>A_of mds\<^sub>C', mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>((0, map fst P\<^sub>1), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> \<R> \<and>
         (\<langle>c\<^sub>2, mds\<^sub>A_of mds\<^sub>C', mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>((0, map fst P\<^sub>2), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> \<R>)
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  if_c1: "\<lbrakk> (* NB: c\<^sub>A is anywhere between c\<^sub>1 and Stop, including Stop i.e. epilogue steps *)
    (P', l', nl\<^sub>2, C', False) = compile_cmd C l nl (If e c\<^sub>1 c\<^sub>2);
    compile_cmd_input_reqs C l nl (If e c\<^sub>1 c\<^sub>2);
    P = map fst P';
    (P\<^sub>e, r, C\<^sub>1, False) = compile_expr C {} l e;
    (br, nl') = RL.fresh_label nl;
    (ex, nl'') = RL.fresh_label nl';
    (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) = compile_cmd C\<^sub>1 None nl'' c\<^sub>1;
    (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) = (compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2);
    pc \<ge> Suc (length P\<^sub>e);
    pc < Suc (length P\<^sub>e + length P\<^sub>1);
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>1), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  if_c2: "\<lbrakk> (* NB: c\<^sub>A is anywhere between c\<^sub>2 and Stop, including Stop i.e. epilogue steps *)
    (P', l', nl\<^sub>2, C', False) = compile_cmd C l nl (If e c\<^sub>1 c\<^sub>2);
    compile_cmd_input_reqs C l nl (If e c\<^sub>1 c\<^sub>2);
    P = map fst P';
    (P\<^sub>e, r, C\<^sub>1, False) = compile_expr C {} l e;
    (br, nl') = RL.fresh_label nl;
    (ex, nl'') = RL.fresh_label nl';
    (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) = compile_cmd C\<^sub>1 None nl'' c\<^sub>1;
    (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) = (compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2);
    pc \<ge> Suc (Suc (length P\<^sub>e + length P\<^sub>1));
    pc < Suc (Suc (length P\<^sub>e + length P\<^sub>1 + length P\<^sub>2));
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc - Suc (Suc (length P\<^sub>e + length P\<^sub>1)), map fst P\<^sub>2), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  while_expr: "\<lbrakk>c\<^sub>A = (if pc = 0 then While b c else If b (c ;; While b c) Stop);
    (* This case covers all instructions in the expression, as well as the Jz at the end of it *)
    (P', Some ex, nl\<^sub>c, (C\<lparr>regrec := Map.empty\<rparr>), False) = compile_cmd C l nl (While b c);
    compile_cmd_input_reqs C l nl (While b c);
    P = map fst P';
    (* explicitly lay out (otherwise derivable) detail so we can refer to P\<^sub>e length *)
    (lp, nl') = (if l = None then RL.fresh_label nl else (the l, nl));
    (ex, nl'') = RL.fresh_label nl';
    (P\<^sub>e, r, C\<^sub>e, False) = compile_expr (C\<lparr>regrec := Map.empty\<rparr>) {} (Some lp) b; (* NB: regrec wiped! *)
    (P\<^sub>c, l\<^sub>c, nl\<^sub>c, C\<^sub>c, False) = (compile_cmd C\<^sub>e None nl'' c);
    pc \<le> length P\<^sub>e;
    regrec_mem_consistent (map snd P' ! pc) regs mem\<^sub>C;
    asmrec_mds_consistent (map snd P' ! pc) mds\<^sub>C;
    regrec_stable (map snd P' ! pc);
    \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C;
    \<forall> mds\<^sub>C' mem\<^sub>C' regs'.
        compiled_cmd_init_reqs C\<^sub>e regs' mds\<^sub>C' mem\<^sub>C' \<and>
        regrec_stable C\<^sub>e \<longrightarrow>
        (\<langle>c, mds\<^sub>A_of mds\<^sub>C', mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>((0, map fst P\<^sub>c), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> \<R>
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  while_inner: "\<lbrakk> (* NB: c\<^sub>A is between c and Stop, EXCLUDING Stop i.e. epilogue steps *)
    c\<^sub>A = c\<^sub>I ;; While b c;
    c\<^sub>I \<noteq> Stop;
    (P', Some ex, nl\<^sub>c, (C\<lparr>regrec := Map.empty\<rparr>), False) = compile_cmd C l nl (While b c);
    compile_cmd_input_reqs C l nl (While b c);
    P = map fst P';
    (lp, nl') = (if l = None then RL.fresh_label nl else (the l, nl));
    (ex, nl'') = RL.fresh_label nl';
    (P\<^sub>e, r, C\<^sub>e, False) = compile_expr (C\<lparr>regrec := Map.empty\<rparr>) {} (Some lp) b; (* NB: regrec wiped! *)
    (P\<^sub>c, l\<^sub>c, nl\<^sub>c, C\<^sub>c, False) = (compile_cmd C\<^sub>e None nl'' c);
    pc \<ge> Suc (length P\<^sub>e);
    pc < Suc (length P\<^sub>e + length P\<^sub>c);
    asmrec_mds_consistent (map snd P' ! pc) mds\<^sub>C;
    (\<langle>c\<^sub>I, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>c), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>;
    (* Propagate compiler correctness guard for all valid initial states for the inner cmd *)
    \<forall> mds\<^sub>C' mem\<^sub>C' regs'.
        compiled_cmd_init_reqs C\<^sub>e regs' mds\<^sub>C' mem\<^sub>C' \<and>
        regrec_stable C\<^sub>e \<longrightarrow>
        (\<langle>c, mds\<^sub>A_of mds\<^sub>C', mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>((0, map fst P\<^sub>c), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> \<R>
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  while_loop: "\<lbrakk> (* NB: This case handles inner epilogue steps and the Jmp lp for the While-loop *)
    c\<^sub>A = While b c;
    (P', Some ex, nl\<^sub>c, (C\<lparr>regrec := Map.empty\<rparr>), False) = compile_cmd C l nl c\<^sub>A;
    compile_cmd_input_reqs C l nl c\<^sub>A;
    P = map fst P';
    (lp, nl') = (if l = None then RL.fresh_label nl else (the l, nl));
    (ex, nl'') = RL.fresh_label nl';
    (P\<^sub>e, r, C\<^sub>e, False) = compile_expr (C\<lparr>regrec := Map.empty\<rparr>) {} (Some lp) b; (* NB: regrec wiped! *)
    (P\<^sub>c, l\<^sub>c, nl\<^sub>c, C\<^sub>c, False) = (compile_cmd C\<^sub>e None nl'' c);
    pc \<ge> Suc (length P\<^sub>e);
    pc \<le> Suc (length P\<^sub>e + length P\<^sub>c);
    asmrec_mds_consistent (map snd P' ! pc) mds\<^sub>C;
    (\<langle>Stop, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>c), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>;
    (* Propagate compiler correctness guard for all valid initial states for the inner cmd *)
    \<forall> mds\<^sub>C' mem\<^sub>C' regs'.
        compiled_cmd_init_reqs C\<^sub>e regs' mds\<^sub>C' mem\<^sub>C' \<and>
        regrec_stable C\<^sub>e \<longrightarrow>
        (\<langle>c, mds\<^sub>A_of mds\<^sub>C', mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>((0, map fst P\<^sub>c), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> \<R>
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  epilogue_step: "\<lbrakk>c\<^sub>A = Stop;
    pc < length P;
    epilogue_step_forms (current_instB (pc, P));
    \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C;
    closed_over_pairwise_reachability \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<R>
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  lock_acq: "\<lbrakk>c\<^sub>A = Stmt.LockAcq k;
    Suc pc = length P;
    snd (P ! pc) = InstB.LockAcq k;
    \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" |

  lock_rel: "\<lbrakk>c\<^sub>A = Stmt.LockRel k;
    Suc pc = length P;
    snd (P ! pc) = InstB.LockRel k;
    \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>"

lemmas \<R>_induct = \<R>.induct[split_format(complete),
  where op_ev=op_ev and dma=dma and \<C>_vars=\<C>_vars and bin_op=bin_op
  and reg_alloc=reg_alloc and reg_alloc_cached=reg_alloc_cached and eval\<^sub>L=eval\<^sub>L
  and lock_interp=lock_interp and lock_val_True=lock_val_True and lock_val_False=lock_val_False
  and INIT\<^sub>w=INIT\<^sub>w and INIT\<^sub>r=INIT\<^sub>r and some_reg=some_reg]

lemma \<R>_pairs_stop_implies_halt_or_epilogue:
assumes
  a1: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" and
  a2: "c\<^sub>A = Stop"
shows "pc = length P \<or> epilogue_step_forms (current_instB (pc, P))"
  using a1 a2
  apply(induct c\<^sub>A mds\<^sub>A mem\<^sub>A pc P regs mds\<^sub>C mem\<^sub>C rule:\<R>_induct)
                  using risc_compilation_axioms apply force
                 using a1 apply force
                apply(force simp:cmd_seq_def split:if_splits)
               apply force+
         apply(clarsimp simp:compile_cmd_input_reqs_def split:prod.splits split del:if_split)
         apply(erule disjE)
          apply force
         apply(force simp: nth_append)
        apply(clarsimp simp:compile_cmd_input_reqs_def nth_append split:prod.splits)
        apply(rule conjI)
         apply force
        apply force
       apply(force split:if_splits)
      apply force
     apply force
    apply force
   apply force
  by force

lemma epilogue_step_not_expr_output:
  "epilogue_step_forms x \<Longrightarrow> \<not> compile_expr_output_forms x"
  unfolding epilogue_step_forms_def compile_expr_output_forms_def
  by blast

lemma abs_steps_not_0_1[simp]:
  "abs_steps' x y > 0 = (abs_steps' x y = 1)"
  by (metis abs_steps_range insertE less_imp_neq singletonD zero_less_one)

(* Sanity check: that the so-called 'invariant' conditions of \<R> are indeed invariant for \<R> *)
lemma conds_\<R>:
  "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R> \<Longrightarrow>
   \<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C"
proof (induct rule:\<R>.induct)
  case (seq c\<^sub>A c\<^sub>1\<^sub>A c\<^sub>2\<^sub>A P' l\<^sub>2 nl\<^sub>2 C\<^sub>2 C l nl c\<^sub>1\<^sub>A' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 P\<^sub>2 pc P mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  hence "c\<^sub>1\<^sub>A = Stop \<Longrightarrow> epilogue_step_forms (snd (P ! pc))"
    by (force dest:\<R>_pairs_stop_implies_halt_or_epilogue simp:nth_append)
  hence "c\<^sub>1\<^sub>A = Stop \<Longrightarrow> abs_steps' c\<^sub>A (pc, P) = 0"
    by (force simp:abs_steps'_def nth_append epilogue_step_not_expr_output split:if_splits)
  with seq show ?case
    apply(clarsimp simp:\<R>_conds)
    apply(rule conjI)
     apply(force simp:preserves_modes_mem_pair_def)
    apply(rule conjI)
     apply(clarsimp simp:regs_noRW_pair_def)
    apply(clarsimp simp:cmds_stop_consistently_def RL.joinable_def cmd_seq_def split:if_splits)
     apply(metis WL.neval_0)
    by (metis (no_types, hide_lams) RL.eval\<^sub>r_join_left RL.eval\<^sub>r_no_modify length_map neval_append_heavy)
next
  case (join c\<^sub>A pc P\<^sub>1 pc\<^sub>2 P\<^sub>2 P mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    apply(clarsimp simp:\<R>_conds)
    apply(rule conjI)
     using preserves_modes_mem_pair_def
     apply(metis fst_conv snd_conv)
    apply(rule conjI)
     apply(clarsimp simp:regs_noRW_pair_def)
    apply(unfold cmds_stop_consistently_def)
    apply(case_tac "length P\<^sub>2 = 0")
     apply force
    apply(clarsimp simp:abs_steps'_def split:if_splits)
       apply(metis WL.leftmost_while_enabled)
      apply(metis WL.neval_0)
     apply(metis WL.neval_0)
    using abs_steps'_head\<^sub>C RL.eval\<^sub>r_halt_iff_oob by force
next
  case stop thus ?case by (clarsimp simp:\<R>_conds)
next
  case skip_nop thus ?case by (clarsimp simp:\<R>_conds)
next
  case assign_expr thus ?case by (clarsimp simp:\<R>_conds)
next
  case assign_store thus ?case by (clarsimp simp:\<R>_conds)
next
  case if_expr thus ?case by (clarsimp simp:\<R>_conds)
next
  case (if_c1 P' l' nl\<^sub>2 C' C l nl e c\<^sub>1 c\<^sub>2 P P\<^sub>e r C\<^sub>1 br nl' ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 P\<^sub>2 l\<^sub>2 C\<^sub>3 pc c\<^sub>A mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    apply(clarsimp split:prod.splits split del:if_split simp:\<R>_conds)
    apply(rule conjI)
     apply(force simp:preserves_modes_mem_pair_def)
    apply(rule conjI)
     apply(force simp:regs_noRW_pair_def)
    apply(clarsimp simp:cmds_stop_consistently_def split del:if_split)
    apply(erule impE)
     apply(subgoal_tac "pc - length P\<^sub>e < Suc (length P\<^sub>1)")
      prefer 2
      apply linarith
     apply(metis (no_types, hide_lams) Suc_diff_Suc Suc_le_lessD RL.eval\<^sub>r_halt_iff_oob leD length_map not_less_eq_eq)
    apply(clarsimp split del:if_split)
    apply(subgoal_tac "abs_steps' c\<^sub>A
             (pc, map fst P\<^sub>e @ (if P\<^sub>e = [] then l else None, Jz br r) #
                  map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]) =
             abs_steps' c\<^sub>A (pc - Suc (length P\<^sub>e), map fst P\<^sub>1)")
     prefer 2
     apply(clarsimp simp:nth_append abs_steps'_def)
     apply linarith
    apply clarsimp
    by blast
next
  case (if_c2 P' l' nl\<^sub>2 C' C l nl e c\<^sub>1 c\<^sub>2 P P\<^sub>e r C\<^sub>1 br nl' ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 P\<^sub>2 l\<^sub>2 C\<^sub>3 pc c\<^sub>A mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    apply(clarsimp split:prod.splits split del:if_split simp:\<R>_conds)
    apply(rule conjI)
     apply(force simp:preserves_modes_mem_pair_def)
    apply(rule conjI)
     apply(force simp:regs_noRW_pair_def)
    apply(clarsimp simp:cmds_stop_consistently_def split del:if_split)
    apply(erule impE)
     apply(subgoal_tac "pc - Suc (length P\<^sub>e + length P\<^sub>1) < Suc (length P\<^sub>2)")
      prefer 2
      apply linarith
     apply(metis (no_types, hide_lams) Suc_diff_Suc Suc_le_lessD RL.eval\<^sub>r_halt_iff_oob leD length_map not_less_eq_eq)
    apply(clarsimp split del:if_split)
    apply(rename_tac a b ba bb aa bc bd)
    apply(subgoal_tac "abs_steps' c\<^sub>A (pc, P) =
             abs_steps' c\<^sub>A (pc - Suc (Suc (length P\<^sub>e + length P\<^sub>1)), map fst P\<^sub>2)")
     prefer 2
     apply(clarsimp simp:nth_append abs_steps'_def)
     apply linarith
    apply clarsimp
    by blast
next
  case while_expr thus ?case by (clarsimp simp:\<R>_conds)
next
  case (while_inner c\<^sub>A c\<^sub>I b c P' ex nl\<^sub>c C l nl P lp nl' nl'' P\<^sub>e r C\<^sub>e P\<^sub>c l\<^sub>c C\<^sub>c pc mds\<^sub>C mds\<^sub>A mem\<^sub>A regs mem\<^sub>C)
  from while_inner(1-14) show ?case
    apply(clarsimp split:prod.splits split del:if_split simp:\<R>_conds)
    apply(rule conjI)
     apply(force simp:preserves_modes_mem_pair_def)
    apply(rule conjI)
     apply(force simp:regs_noRW_pair_def)
    apply(clarsimp simp:cmds_stop_consistently_def split del:if_split)
    apply(erule impE)
     apply(subgoal_tac "pc - Suc (length P\<^sub>e) < length P\<^sub>c")
      prefer 2
      apply linarith
     apply(metis (no_types, hide_lams) RL.eval\<^sub>r_halt_iff_oob leD length_map)
    apply(clarsimp split del:if_split)
    apply(rename_tac a b ba bb aa bc bd)
    apply(subgoal_tac "abs_steps' c\<^sub>A (pc, P) =
             abs_steps' c\<^sub>I (pc - Suc (length P\<^sub>e), map fst P\<^sub>c)")
     prefer 2
     apply(clarsimp simp:nth_append abs_steps'_def)
     apply linarith
    apply(clarsimp split del:if_split)
    using neval_seq by blast
next
  case (while_loop c\<^sub>A b c P' ex nl\<^sub>c C l nl P lp nl' nl'' P\<^sub>e r C\<^sub>e P\<^sub>c l\<^sub>c C\<^sub>c pc mds\<^sub>C mds\<^sub>A mem\<^sub>A regs mem\<^sub>C)
  from while_loop(1-13) show ?case
    apply clarsimp
    apply(clarsimp simp:\<R>_conds)
    apply(rule conjI)
     apply(force simp:preserves_modes_mem_pair_def)
    apply(rule conjI)
     apply(force simp:regs_noRW_pair_def)
    apply(clarsimp simp:cmds_stop_consistently_def)
    apply(case_tac "pc - Suc (length P\<^sub>e) < length P\<^sub>c")
     (* Case: In P\<^sub>c *)
     apply(frule \<R>_pairs_stop_implies_halt_or_epilogue)
      apply blast
     apply(clarsimp split:prod.splits)
     apply(subgoal_tac "current_instB (pc - Suc (length P\<^sub>e), map fst P\<^sub>c) = current_instB (pc, P)")
      prefer 2
      apply(force simp:nth_append)
     apply(clarsimp simp:abs_steps'_def split:if_splits)
        using WL.while_eval\<^sub>w' apply blast
       using WL.while_eval\<^sub>w' apply blast
      using WL.neval_0 apply blast
     using WL.neval_0 apply blast
    (* Case: at the Jmp *)
    apply(subgoal_tac "pc = Suc (length P\<^sub>e + length P\<^sub>c)")
     prefer 2
     apply linarith
    apply(clarsimp split:prod.splits split del:if_split)
    apply(subgoal_tac "current_instB (Suc (length P\<^sub>e + length P\<^sub>c), P) = Jmp lp")
     prefer 2
     apply(force simp:nth_append)
    apply(clarsimp simp:abs_steps'_def epilogue_step_forms_def compile_expr_output_forms_def)
    using WL.neval_0 by blast
next
  (* Odd. Apparently Isar had figured out that epilogue_step had the goal in its guards.
     Anyway, I put this back here so nobody reading it gets confused by a missing case. *)
  case epilogue_step thus ?case by simp
next
  case lock_acq thus ?case by (clarsimp simp:\<R>_conds)
next
  case lock_rel thus ?case by (clarsimp simp:\<R>_conds)
qed

(* TODO: Retire this in a separate commit, along with regs_noRW from \<R>_conds more generally
  if we decide we no longer want to mark that it once existed. *)
lemma regs_noRW_\<R>:
  "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R> \<Longrightarrow>
   regs_noRW_pair \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C"
  apply(drule conds_\<R>)
  apply(clarsimp simp:\<R>_conds)
  done

subsection {* Proof that the specified relation preserves guarantee compliance *}

definition compile_expr_loads_exp_vars
where
  "compile_expr_loads_exp_vars e x \<equiv> \<forall> r v. x = Load r v \<longrightarrow> v \<in> exp_vars e"

lemma compile_expr_loads_exp_vars:
  "(apm, r, C', False) = compile_expr C A l e \<Longrightarrow>
   i < length apm \<Longrightarrow>
   compile_expr_loads_exp_vars e (snd (fst (apm ! i)))"
  unfolding compile_expr_loads_exp_vars_def
  apply(induct e arbitrary:A l C r C' apm i)
    apply(clarsimp simp:Let_def nth_append split:if_splits prod.splits option.splits)+
   apply metis
  by metis

lemma control_vars_mds:
  "Var x \<in> mds m \<Longrightarrow>
   lock_consistent_mds lock_interp mds \<Longrightarrow>
   Var v \<in> \<C>_vars (Var x) \<Longrightarrow>
   Var v \<in> mds m"
  apply(clarsimp simp:lock_consistent_mds_def)
  apply(clarsimp simp:one_mode_per_var_def lock_not_held_mds_correct_def lock_held_mds_correct_def)
  apply(subgoal_tac "\<exists>l. x \<in> fst (lock_interp l) \<or> x \<in> snd (lock_interp l)")
   prefer 2
   apply(clarsimp simp:only_lock_governed_mds_def)
   apply blast
  apply clarsimp
  apply(rename_tac k)
  apply(erule_tac x=k in allE)
  by (case_tac m, (fastforce simp:lock_interp_\<C>_vars\<^sub>V)+)

(* Adapted from LocallySoundLockUse.thy *)

lemma lock_acq_doesnt_modify_vars:
  "snd (P ! pc) = InstB.LockAcq k \<Longrightarrow>
   RL.doesnt_modify ((pc, P), regs) (Var x)"
  unfolding RL.doesnt_modify_def
  by (force simp:RL.no_locks_in_\<C>_vars RL.dma_\<C>_vars)

lemma lock_acq_doesnt_read_vars:
  "snd (P ! pc) = InstB.LockAcq k \<Longrightarrow>
   RL.doesnt_read_or_modify ((pc, P), regs) (Var x)"
  unfolding RL.doesnt_read_or_modify_def RL.doesnt_read_or_modify_vars_def
  apply clarsimp
  apply(case_tac "eval\<^sub>L (mem (Lock k))")
   apply(frule RL.lock_spin_dest)
     apply force
    apply force
   apply(rule conjI)
    apply(force intro:RL.lock_spin simp:RL.eval\<^sub>r_bounds)
   apply clarsimp
   apply(rename_tac x' v)
   apply(case_tac x')
    using RL.no_locks_in_\<C>_vars apply force
   apply(force intro:RL.lock_spin simp:RL.eval\<^sub>r_bounds)
  apply(frule RL.lock_acq_dest)
    apply force
   apply force
  apply(rule conjI)
   apply clarsimp
   apply(solves \<open>rule RL.lock_acq', (force simp:RL.eval\<^sub>r_bounds)+\<close>)
  apply clarsimp
  apply(rename_tac x' v)
  apply(case_tac x')
   using RL.no_locks_in_\<C>_vars apply force
  by (solves \<open>rule RL.lock_acq', (force simp:RL.eval\<^sub>r_bounds)+\<close>)

lemma lock_rel_doesnt_modify_vars:
  "snd (P ! pc) = InstB.LockRel k \<Longrightarrow>
   RL.doesnt_modify ((pc, P), regs) (Var x)"
  unfolding RL.doesnt_modify_def
  by (force simp:RL.no_locks_in_\<C>_vars RL.dma_\<C>_vars)

lemma lock_rel_doesnt_read_vars:
  "snd (P ! pc) = InstB.LockRel k \<Longrightarrow>
   RL.doesnt_read_or_modify ((pc, P), regs) (Var x)"
  unfolding RL.doesnt_read_or_modify_def RL.doesnt_read_or_modify_vars_def
  apply clarsimp
  apply(case_tac "\<not> lock_held_mds_correct lock_interp mds k")
   apply(frule RL.lock_abort_dest)
     apply force
    apply force
   apply(rule conjI)
    apply(force intro:RL.lock_invalid simp:RL.eval\<^sub>r_bounds)
   apply clarsimp
   apply(rename_tac x' v)
   apply(case_tac x')
    using RL.no_locks_in_\<C>_vars apply force
   apply(force intro:RL.lock_invalid simp:RL.eval\<^sub>r_bounds)
  apply(frule RL.lock_rel_dest)
    apply force
   apply force
  apply(rule conjI)
   apply clarsimp
   apply(solves \<open>rule RL.lock_rel', (force simp:RL.eval\<^sub>r_bounds)+\<close>)
  apply clarsimp
  apply(rename_tac x' v)
  apply(case_tac x')
   using RL.no_locks_in_\<C>_vars apply force
  by (solves \<open>rule RL.lock_rel', (force simp:RL.eval\<^sub>r_bounds)+\<close>)

lemma compiled_expr_respects_guarantees:
assumes
  "pc < length P\<^sub>e"
  "P = map fst P\<^sub>e @ tail"
  "(P\<^sub>e, r, C', False) = compile_expr C {} l e"
  "asmrec_mds_consistent (map snd P\<^sub>e ! pc) mds" and
  asmrec_expr_stable: "asmrec_expr_stable C e" and
  "\<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds, mem\<rangle>\<^sub>C"
shows
  "RL.respects_own_guarantees (((pc, P), regs), mds)"
using assms
  apply(clarsimp simp:
    RL.respects_own_guarantees_def RL.doesnt_read_or_modify_def RL.doesnt_modify_def RL.doesnt_read_or_modify_vars_def
    \<R>_conds preserves_modes_mem_pair_def mds\<^sub>A_of_def mem\<^sub>A_of_def)
  (* I want to argue that the only variables ever read must be ones that are mentioned by
    the expression. These must be stable because of asmrec_expr_stable C e,
    and because asmrec_mds_consistent and ag_consistent_mds,
    none of them may have any guarantees on them. *)
  (* I think I somehow need to conclude that the x here in the GuarNoReadOrWrite case,
    if read, *must* be the variable v in a Load r v currently being executed. *)
  apply(rename_tac x)
  apply(subgoal_tac "compile_expr_output_forms (snd (P ! pc))")
   prefer 2
   apply(force dest:compile_expr_output_forms simp:nth_append)
  apply(clarsimp simp:compile_expr_output_forms_def)
  (* Let's get rid of the easy cases first *)
  apply(rule conjI)
   (* Case: GuarNoWrite - none of these instructions write *)
   prefer 2
   apply(metis (no_types, lifting) RL.load_dest RL.op_dest RL.move_k_dest)
  apply(erule disjE)
   prefer 2
   (* Case: GuarNoReadOrWrite for Op, MoveK - neither of these instructions read from a var *)
   apply(erule disjE)
    apply(metis (no_types, lifting) RL.eval\<^sub>r.op RL.eval\<^sub>r_bounds RL.op_dest)
   apply(metis (no_types, lifting) RL.eval\<^sub>r.move_k RL.eval\<^sub>r_bounds RL.move_k_dest)
  (* Now for the main case: GuarNoReadOrWrite for Load r v *)
  apply clarsimp
  apply(frule RL.load_dest)
   apply force
  apply clarsimp
  apply(rename_tac x r' v' mem' mds')
  apply(rule conjI)
   (* Here we prove for cases of x, the variable we're guaranteeing not to read nor write *)
   apply clarsimp
   apply(rename_tac x r' v' mem' mds' v'')
   apply(clarsimp simp only:Suc_eq_plus1)
   apply(case_tac x)
    (* x can't be a lock because locks can't have mode state *)
    apply(force simp:lock_consistent_mds_def no_lock_mds_def)
   apply clarsimp
   apply(rename_tac x)
   apply(case_tac "x \<noteq> v'")
    (* Case: x is not the variable being loaded *)
    apply(subgoal_tac "(regs(r' := (mem' (Var v'))) = regs(r' := ((mem'(Var x := v'')) (Var v'))))")
     prefer 2
     apply force
    apply(clarsimp simp only:Suc_eq_plus1)
    apply(metis RL.eval\<^sub>r.load RL.eval\<^sub>r_bounds)
   apply clarsimp
   (* This leaves the final case: x is the variable v' being loaded *)
   apply(subgoal_tac "compile_expr_loads_exp_vars e (snd (P ! pc))")
    prefer 2
    apply(force dest:compile_expr_loads_exp_vars simp:nth_append)
   apply(clarsimp simp:compile_expr_loads_exp_vars_def)
   apply(subgoal_tac "asmrec_expr_stable (map snd P\<^sub>e ! pc) e")
    prefer 2
    apply(metis compiled_expr_asmrec_constancy_inner)
   (* Now we know from asmrec_expr_stable that we must have AsmNoW or AsmNoRW on v'.
     Therefore we know we cannot have GuarNoRW. *)
   apply(clarsimp simp:asmrec_expr_stable'_def asmrec_mds_consistent'_def asmrec_var_stable'_def lock_consistent_mds_def to_prog_mds_def no_lock_mds_def)
   apply(clarsimp simp:one_mode_per_var_def)
   apply blast
  apply(clarsimp simp:asmrec_expr_stable'_def)
  (* This time prove for cases of xc, a control variable of the var
    we're guaranteeing not to read nor write *)
  apply(case_tac x)
   (* but still rule out x being a lock, because we need that fact for later *)
   apply(force simp:lock_consistent_mds_def no_lock_mds_def)
  apply(clarsimp simp only:Suc_eq_plus1)
  apply(rename_tac xc v'' x)
  apply(case_tac xc)
   (* xc can't be a lock because locks can't be control vars *)
   apply clarsimp
   using no_locks_in_\<C> RL.\<C>_def
   apply blast
  apply clarsimp
  apply(rename_tac xc)
  apply(case_tac "xc \<noteq> v'")
   (* Case: xc is not the variable being loaded *)
   apply(subgoal_tac "(regs(r' := (mem' (Var v'))) = regs(r' := ((mem'(Var xc := v'')) (Var v'))))")
    prefer 2
    apply force
   apply(clarsimp simp only:Suc_eq_plus1)
   apply(metis RL.eval\<^sub>r.load RL.eval\<^sub>r_bounds)
  apply clarsimp
  (* This leaves the final case: xc is the variable v' being loaded *)
  apply(subgoal_tac "compile_expr_loads_exp_vars e (snd (P ! pc))")
   prefer 2
   apply(force dest:compile_expr_loads_exp_vars simp:nth_append)
  apply(clarsimp simp:compile_expr_loads_exp_vars_def)
  apply(subgoal_tac "asmrec_expr_stable (map snd P\<^sub>e ! pc) e")
   prefer 2
   using compiled_expr_asmrec_constancy_inner asmrec_expr_stable
   apply metis
  (* Use the knowledge that x is lock-governed, along with
    the knowledge that a variable's control vars must be governed in the same way by the
    same lock (lock_interp_\<C>_vars\<^sub>V) to conclude via the lock_consistent_mds invariant
    that x's control variables must have the same mode as x.
    Because x has a guarantee, xc must also have a guarantee, which gets us a contradiction
    via one_mode_per_var. *)
  apply(frule control_vars_mds)
    apply force
   apply force
  (* We know from asmrec_expr_stable that we must have AsmNoW or AsmNoRW on v'. *)
  apply(clarsimp simp:asmrec_expr_stable'_def asmrec_mds_consistent'_def asmrec_var_stable'_def lock_consistent_mds_def to_prog_mds_def no_lock_mds_def)
  by (auto simp:one_mode_per_var_def)

lemma compiled_expr_respects_guarantees':
assumes
  "pc < length P\<^sub>e"
  "P = map fst P\<^sub>e @ tail"
  (* Merely flips this equality *)
  "compile_expr C {} l e = (P\<^sub>e, r, C', False)"
  "asmrec_mds_consistent (map snd P\<^sub>e ! pc) mds" and
  asmrec_expr_stable: "asmrec_expr_stable C e" and
  "\<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds, mem\<rangle>\<^sub>C"
shows
  "RL.respects_own_guarantees (((pc, P), regs), mds)"
  using compiled_expr_respects_guarantees assms by metis

lemma preserves_local_guarantee_compliance_\<R>':
assumes
  a1: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" (* and
  a2: "WL.respects_own_guarantees (c\<^sub>A, mds\<^sub>A)" *)
shows
  "RL.respects_own_guarantees (((pc, P), regs), mds\<^sub>C)"
using a1 (* a2 *)
proof (induct c\<^sub>A mds\<^sub>A mem\<^sub>A pc P regs mds\<^sub>C mem\<^sub>C rule:\<R>_induct)
  case 1 thus ?case by (simp add: risc_compilation_axioms)
next
  case 2 show ?case
    using a1 by simp
next (* Seq *)
  case (3 c\<^sub>A c\<^sub>1\<^sub>A c\<^sub>2\<^sub>A P' l\<^sub>2 nl\<^sub>2 C\<^sub>2 C l nl c\<^sub>1\<^sub>A' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 P\<^sub>2 pc P mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  from "3.hyps"(1-15) "3.prems" show ?case
    apply(clarsimp simp:RL.respects_own_guarantees_def RL.joinable_def)
    apply(rule conjI)
     apply(clarsimp simp:RL.doesnt_read_or_modify_def RL.doesnt_read_or_modify_vars_def)
     apply(frule RL.eval\<^sub>r_no_modify)
     apply(force simp:RL.eval\<^sub>r_join_left RL.eval\<^sub>r_append)
    apply(clarsimp simp:RL.doesnt_modify_def)
    apply(frule RL.eval\<^sub>r_no_modify)
    by (force intro:RL.eval\<^sub>r_join_left)
next (* Join *)
  case (4 pc P\<^sub>1 pc\<^sub>2 P\<^sub>2 P c\<^sub>A mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    apply(clarsimp simp:RL.respects_own_guarantees_def RL.joinable_def)
    apply(rename_tac x)
    apply(rule conjI)
     apply(clarsimp simp:RL.doesnt_read_or_modify_def RL.doesnt_read_or_modify_vars_def)
     apply(frule RL.eval\<^sub>r_no_modify)
     apply(erule_tac x=x in allE)
     apply clarsimp
     using RL.eval\<^sub>r_no_backtrack_over_join le_add1
     apply(metis RL.eval\<^sub>r_join_right[where P\<^sub>1=P\<^sub>1] RL.eval\<^sub>r_prepend_join le_add_diff_inverse)
    apply(clarsimp simp:RL.doesnt_modify_def)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(erule_tac x=x in allE)
    using RL.eval\<^sub>r_no_backtrack_over_join le_add1
    by (metis RL.eval\<^sub>r_join_right[where P\<^sub>1=P\<^sub>1] le_Suc_ex)
next (* Stop *)
  case 5
  thus ?case
    apply(clarsimp simp:RL.respects_own_guarantees_def RL.doesnt_read_or_modify_def
      RL.doesnt_modify_def RL.doesnt_read_or_modify_vars_def)
    by (simp add: RL.eval\<^sub>r_oob_pc_halt)
next (* Skip *)
  case 6
  thus ?case
    apply(clarsimp simp:RL.respects_own_guarantees_def RL.doesnt_read_or_modify_def
      RL.doesnt_modify_def RL.doesnt_read_or_modify_vars_def)
    by (metis (no_types, lifting) RL.eval\<^sub>r.nop RL.eval\<^sub>r_bounds RL.nop_dest)
next (* Assign - expr *)
  case (7 c\<^sub>A v e P' l' nl\<^sub>2 C' C l nl P P\<^sub>e r C\<^sub>e pc regs mem\<^sub>C mds\<^sub>C mds\<^sub>A mem\<^sub>A)
  thus ?case
    apply(clarsimp simp:Let_def split:prod.splits split del:if_split)
    using compiled_expr_respects_guarantees'
    by force
next (* Assign - store *)
  case (8 c\<^sub>A v e P' l' nl\<^sub>2 C' C l nl P P\<^sub>e r C\<^sub>e pc regs mem\<^sub>C mds\<^sub>C mds\<^sub>A mem\<^sub>A)
  thus ?case
    apply(clarsimp simp:RL.respects_own_guarantees_def RL.doesnt_read_or_modify_def
      RL.doesnt_modify_def RL.doesnt_read_or_modify_vars_def epilogue_step_forms_def
      \<R>_conds preserves_modes_mem_pair_def mds\<^sub>A_of_def mem\<^sub>A_of_def
      Let_def split:prod.splits split del:if_split)
    apply(subgoal_tac "pc < length P")
     prefer 2
     apply force
    apply(rename_tac x)
    apply(case_tac x)
     (* x can't be a lock because locks can't have mode state *)
     apply(force simp:lock_consistent_mds_def no_lock_mds_def)
    apply(clarsimp split del:if_split)
    apply(rename_tac x)
    apply(subgoal_tac "asmrec C = asmrec C\<^sub>e")
     prefer 2
     using compiled_expr_asmrec_constancy_end
     apply metis
    apply(rule conjI)
     (* Case 1: GuarNoRW *)
     apply(clarsimp split del:if_split)
     apply(frule RL.store_dest[rotated])
      apply(force simp:nth_append)
     apply(clarsimp split del:if_split)
     apply(rule conjI)
      (* 1-a: modified var x *)
      apply(clarsimp split del:if_split)
      apply(frule_tac A="{}" in compile_expr_correctness')
        apply force
       apply force
      apply(case_tac "x = v")
       apply(case_tac "\<not> asmrec_var_stable C\<^sub>e v")
        (* 1-a-i: If v is unstable, it is non-lock-governed and therefore not guaranteed *)
        apply(force simp:lock_governed_def lock_consistent_mds_def only_lock_governed_mds_def split del:if_split)
       (* 1-a-ii: If v stable, can't be Var v because it would've been locked *)
       apply(clarsimp simp:asmrec_var_stable'_def asmrec_mds_consistent'_def split del:if_split)
       apply(force simp:lock_consistent_mds_def one_mode_per_var_def)
      apply(clarsimp simp:fun_upd_twist split del:if_split)
      apply(clarsimp simp:regrec_mem_consistent_def split del:if_split)
      apply(clarsimp simp only:Suc_eq_plus1)
      apply(metis (no_types) length_map nth_append_length RL.eval\<^sub>r.store RL.eval\<^sub>r_bounds snd_conv)
     (* 1-b: modified control var of x *)
     apply(clarsimp split del:if_split)
     apply(rename_tac xc v')
     apply(case_tac xc)
      (* xc can't be a lock because locks can't be control vars *)
      apply clarsimp
      using no_locks_in_\<C> RL.\<C>_def
      apply blast
     apply(clarsimp split del:if_split)
     apply(rename_tac xc)
     apply(case_tac "xc = v")
      apply(case_tac "\<not> asmrec_var_stable C\<^sub>e v")
       (* 1-b-i: If v is unstable, it is non-lock-governed and therefore not guaranteed *)
       apply(clarsimp simp:lock_governed_def lock_consistent_mds_def only_lock_governed_mds_def split del:if_split)
       (* but if it was a control var of x then it would have to be guaranteed, a contradiction *)
       using lock_interp_\<C>_vars\<^sub>V apply blast
      (* 1-b-ii: If v stable, can't be Var v because it would've been locked *)
      apply(clarsimp simp:asmrec_var_stable'_def asmrec_mds_consistent'_def split del:if_split)
      apply(frule control_vars_mds)
        apply force
       apply force
      (* We know from asmrec_expr_stable that we must have AsmNoW or AsmNoRW on v'. *)
      apply(clarsimp simp:asmrec_expr_stable'_def asmrec_mds_consistent'_def asmrec_var_stable'_def lock_consistent_mds_def to_prog_mds_def no_lock_mds_def)
      apply(force simp:one_mode_per_var_def)
     apply(clarsimp simp:fun_upd_twist split del:if_split)
     apply(clarsimp simp:regrec_mem_consistent_def split del:if_split)
     apply(clarsimp simp only:Suc_eq_plus1)
     apply(metis (no_types) length_map nth_append_length RL.eval\<^sub>r.store RL.eval\<^sub>r_bounds snd_conv)
    (* Case 2: GuarNoW *)
    apply(clarsimp split del:if_split)
    (* Just have to rule out that x is the variable being assigned to... I think? *)
    apply(frule RL.store_dest[rotated])
     apply(force simp:nth_append)
    apply clarsimp
    apply(rule conjI)
     apply(case_tac "\<not> asmrec_var_stable C\<^sub>e v")
      apply(force simp:lock_governed_def lock_consistent_mds_def only_lock_governed_mds_def split del:if_split)
     (* Again, x can't be v because it would've been either non-guaranteed, or locked *)
     apply(clarsimp simp:asmrec_var_stable'_def asmrec_mds_consistent'_def split del:if_split)
     apply(force simp:lock_consistent_mds_def one_mode_per_var_def)
    apply clarsimp
    apply(rule RL.dma_\<C>_vars)
    apply clarsimp
    apply(frule control_vars_mds)
      apply force
     apply force
    apply(case_tac "\<not> asmrec_var_stable C\<^sub>e v")
     (* If v is unstable, it is non-lock-governed and therefore not guaranteed *)
     apply(clarsimp simp:lock_governed_def lock_consistent_mds_def only_lock_governed_mds_def split del:if_split)
     (* but if it was a control var of x then it would have to be guaranteed, a contradiction *)
     using lock_interp_\<C>_vars\<^sub>V apply blast
    (* Otherwise it is locked like x *)
    apply(clarsimp simp:asmrec_var_stable'_def asmrec_mds_consistent'_def split del:if_split)
    by (force simp:lock_consistent_mds_def one_mode_per_var_def)
next (* If - expr *)
  case (9 c\<^sub>A e c\<^sub>1 c\<^sub>2 P' l' nl\<^sub>2 C' C l nl P P\<^sub>e r C\<^sub>1 br nl' ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 P\<^sub>2 l\<^sub>2 C\<^sub>3 pc regs mem\<^sub>C mds\<^sub>C
       mds\<^sub>A mem\<^sub>A)
  from "9.hyps"(1-14) "9.prems"
  show ?case
    apply(clarsimp split:prod.splits split del:if_split)
    apply(case_tac "pc < length P\<^sub>e")
     (* Case: in the expression *)
     apply(clarsimp simp:compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)
     apply(force intro:compiled_expr_respects_guarantees' simp:nth_append)
    (* Case: at the Jz *)
    apply(clarsimp simp:RL.respects_own_guarantees_def RL.doesnt_read_or_modify_def RL.doesnt_modify_def RL.doesnt_read_or_modify_vars_def
      \<R>_conds preserves_modes_mem_pair_def mds\<^sub>A_of_def mem\<^sub>A_of_def split del:if_split)
    apply(case_tac "regs r \<noteq> 0")
     apply(metis (no_types, lifting) RL.eval\<^sub>r.jz_f RL.jz_f_dest RL.eval\<^sub>r_bounds length_map nth_append_length snd_conv)
    by (metis (no_types, lifting) RL.eval\<^sub>r.jz_s RL.jz_s_dest RL.eval\<^sub>r_bounds length_map nth_append_length snd_conv)
next (* If - then *)
  case (10 P' l' nl\<^sub>2 C' C l nl e c\<^sub>1 c\<^sub>2 P P\<^sub>e r C\<^sub>1 br nl' ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 P\<^sub>2 l\<^sub>2 C\<^sub>3 pc c\<^sub>A mds\<^sub>A mem\<^sub>A regs
       mds\<^sub>C mem\<^sub>C)
  thus ?case
    apply(clarsimp split:prod.splits split del:if_split)
    apply(clarsimp simp:RL.respects_own_guarantees_def RL.joinable_def split del:if_split)
    apply(rule conjI)
     apply(clarsimp simp:RL.doesnt_read_or_modify_def RL.doesnt_read_or_modify_vars_def split del:if_split)
     apply(frule RL.eval\<^sub>r_no_modify)
     apply(erule_tac x=x in allE)
     apply(clarsimp simp:compile_cmd_input_reqs_def split del:if_split)
     apply(rename_tac x mds mem pc' regs' mds' mem')
     apply(subgoal_tac
       "(\<forall>v. eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>1), regs), mds, mem(x := v)\<rangle>\<^sub>C \<langle>((pc' - Suc (length P\<^sub>e), map fst P\<^sub>1), regs'), mds', mem'(x := v)\<rangle>\<^sub>C) \<and>
        (\<forall>x\<in>\<C>_vars x. \<forall>v. eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>1), regs), mds, mem(x := v)\<rangle>\<^sub>C \<langle>((pc' - Suc (length P\<^sub>e), map fst P\<^sub>1), regs'), mds', mem'(x := v)\<rangle>\<^sub>C)")
      prefer 2
      using if_c1_pc_backward_helper if_c1_pc_forward_helper if_c1_eval_helper
      apply force
     apply(subgoal_tac "RL.joinable_forward (map fst P\<^sub>1) ((l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])")
      prefer 2
      apply(force simp:RL.joinable_forward_def compile_cmd_output_label)
     (* There might be a more elegant, less redundant way to use these helpers
        but for now just solve the goal.
     using RL.eval\<^sub>r_append[where pc="pc - Suc (length P\<^sub>e)"]
           RL.eval\<^sub>r_no_backtrack_over_join
           RL.eval\<^sub>r_prepend_join[where pc\<^sub>2="pc - Suc (length P\<^sub>e)"] *)
     apply(frule_tac c\<^sub>2=c\<^sub>2 in if_c1_joinable_backward_helper, (simp split del:if_split)+)
     apply(clarsimp split del:if_split)
     apply(rule conjI)
      apply(clarsimp split del:if_split)
      apply(erule_tac x=v in allE)
      apply(frule_tac pc="pc - Suc (length P\<^sub>e)" and mem="mem(x := v)" and mem'="mem'(x := v)" in RL.eval\<^sub>r_append)
        apply(simp split del:if_split)+
      apply(frule_tac pc\<^sub>2="pc - Suc (length P\<^sub>e)" and mem="mem(x := v)" and mem'="mem'(x := v)" in RL.eval\<^sub>r_prepend_join)
          apply(simp split del:if_split)+
      apply(solves \<open>frule RL.eval\<^sub>r_no_backtrack_over_join, simp+\<close>)
     apply(clarsimp split del:if_split)
     apply(rename_tac cv v)
     apply(thin_tac "\<forall>v. eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>1), regs), mds, mem(x := v)\<rangle>\<^sub>C
            \<langle>((pc' - Suc (length P\<^sub>e), map fst P\<^sub>1), regs'), mds', mem'(x := v)\<rangle>\<^sub>C")
     apply(erule_tac x=cv in ballE)
      apply(erule_tac x=v in allE)
      apply(frule_tac pc="pc - Suc (length P\<^sub>e)" and mem="mem(cv := v)" and mem'="mem'(cv := v)" in RL.eval\<^sub>r_append)
        apply(simp split del:if_split)+
      apply(frule_tac pc\<^sub>2="pc - Suc (length P\<^sub>e)" and mem="mem(cv := v)" and mem'="mem'(cv := v)" in RL.eval\<^sub>r_prepend_join)
          apply(simp split del:if_split)+
      apply(solves \<open>frule RL.eval\<^sub>r_no_backtrack_over_join, simp+\<close>)
     apply blast
    apply(clarsimp simp:RL.doesnt_modify_def split del:if_split)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(erule_tac x=x in allE)
    apply(clarsimp simp:compile_cmd_input_reqs_def split del:if_split)
    apply(rename_tac x mds mem pc' regs' mds' mem')
    apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>1), regs), mds, mem\<rangle>\<^sub>C \<langle>((pc' - Suc (length P\<^sub>e), map fst P\<^sub>1), regs'), mds', mem'\<rangle>\<^sub>C")
     prefer 2
     using if_c1_pc_backward_helper if_c1_pc_forward_helper if_c1_eval_helper
     apply force
    by force
next (* If - else *)
  case (11 P' l' nl\<^sub>2 C' C l nl e c\<^sub>1 c\<^sub>2 P P\<^sub>e r C\<^sub>1 br nl' ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 P\<^sub>2 l\<^sub>2 C\<^sub>3 pc c\<^sub>A mds\<^sub>A
       mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    (* Mostly adapted from If-then proof, same kind of redundancy for now unfortunately *)
    apply(clarsimp split:prod.splits split del:if_split)
    apply(clarsimp simp:RL.respects_own_guarantees_def RL.joinable_def split del:if_split)
    apply(rule conjI)
     apply(clarsimp simp:RL.doesnt_read_or_modify_def RL.doesnt_read_or_modify_vars_def split del:if_split)
     apply(frule RL.eval\<^sub>r_no_modify)
     apply(erule_tac x=x in allE)
     apply(clarsimp simp:compile_cmd_input_reqs_def split del:if_split)
     apply(rename_tac x mds mem pc' regs' mds' mem')
     apply(subgoal_tac
       "(\<forall>v. eval_abv\<^sub>C \<langle>((pc - Suc (Suc (length P\<^sub>e + length P\<^sub>1)), map fst P\<^sub>2), regs), mds, mem(x := v)\<rangle>\<^sub>C \<langle>((pc' - Suc (Suc (length P\<^sub>e + length P\<^sub>1)), map fst P\<^sub>2), regs'), mds', mem'(x := v)\<rangle>\<^sub>C) \<and>
        (\<forall>x\<in>\<C>_vars x. \<forall>v. eval_abv\<^sub>C \<langle>((pc - Suc (Suc (length P\<^sub>e + length P\<^sub>1)), map fst P\<^sub>2), regs), mds, mem(x := v)\<rangle>\<^sub>C \<langle>((pc' - Suc (Suc (length P\<^sub>e + length P\<^sub>1)), map fst P\<^sub>2), regs'), mds', mem'(x := v)\<rangle>\<^sub>C)")
      prefer 2
      using if_c2_pc_backward_helper if_c2_pc_forward_helper if_c2_eval_helper
      apply force
     apply(subgoal_tac "RL.joinable_forward (map fst P\<^sub>2) [(l\<^sub>2, Tau)]")
      prefer 2
      apply(force simp:RL.joinable_forward_def compile_cmd_output_label)
     apply(frule_tac c\<^sub>2=c\<^sub>2 in if_c2_joinable_backward_helper, (simp split del:if_split)+)
     apply(clarsimp split del:if_split)
     apply(rule conjI)
      apply(clarsimp split del:if_split)
      apply(erule_tac x=v in allE)
      apply(frule_tac pc="pc - Suc (Suc (length P\<^sub>e + length P\<^sub>1))" and mem="mem(x := v)" and mem'="mem'(x := v)" in RL.eval\<^sub>r_append)
        apply(simp split del:if_split)+
      apply(frule_tac pc\<^sub>2="pc - Suc (Suc (length P\<^sub>e + length P\<^sub>1))" and mem="mem(x := v)" and mem'="mem'(x := v)" in RL.eval\<^sub>r_prepend_join)
          apply(simp split del:if_split)+
      apply(frule RL.eval\<^sub>r_no_backtrack_over_join, (simp split del:if_split)+)
      apply(subgoal_tac "(Suc (Suc (pc - 2))) = pc \<and> (Suc (Suc (pc' - 2))) = pc'")
       prefer 2
       apply linarith
      apply force
     apply(clarsimp split del:if_split)
     apply(rename_tac cv v)
     apply(thin_tac "\<forall>v. eval_abv\<^sub>C \<langle>((pc - Suc (Suc (length P\<^sub>e + length P\<^sub>1)), map fst P\<^sub>2), regs), mds, mem(x := v)\<rangle>\<^sub>C
           \<langle>((pc' - Suc (Suc (length P\<^sub>e + length P\<^sub>1)), map fst P\<^sub>2), regs'), mds', mem'(x := v)\<rangle>\<^sub>C")
     apply(erule_tac x=cv in ballE)
      apply(erule_tac x=v in allE)
      apply(frule_tac pc="pc - Suc (Suc (length P\<^sub>e + length P\<^sub>1))" and mem="mem(cv := v)" and mem'="mem'(cv := v)" in RL.eval\<^sub>r_append)
        apply(simp split del:if_split)+
      apply(frule_tac pc\<^sub>2="pc - Suc (Suc (length P\<^sub>e + length P\<^sub>1))" and mem="mem(cv := v)" and mem'="mem'(cv := v)" in RL.eval\<^sub>r_prepend_join)
          apply(simp split del:if_split)+
      apply(frule RL.eval\<^sub>r_no_backtrack_over_join, (simp split del:if_split)+)
      apply(subgoal_tac "(Suc (Suc (pc - 2))) = pc \<and> (Suc (Suc (pc' - 2))) = pc'")
       prefer 2
       apply linarith
      apply force
     apply blast
    apply(clarsimp simp:RL.doesnt_modify_def split del:if_split)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(erule_tac x=x in allE)
    apply(clarsimp simp:compile_cmd_input_reqs_def split del:if_split)
    apply(rename_tac x mds mem pc' regs' mds' mem')
    apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - Suc (Suc (length P\<^sub>e + length P\<^sub>1)), map fst P\<^sub>2), regs), mds, mem\<rangle>\<^sub>C \<langle>((pc' - Suc (Suc (length P\<^sub>e + length P\<^sub>1)), map fst P\<^sub>2), regs'), mds', mem'\<rangle>\<^sub>C")
     prefer 2
     using if_c2_pc_backward_helper if_c2_pc_forward_helper if_c2_eval_helper
     apply force
    by force
next (* While - expr *)
  case (12 c\<^sub>A pc b c P' ex nl\<^sub>c C l nl P lp nl' nl'' P\<^sub>e r C\<^sub>e P\<^sub>c l\<^sub>c C\<^sub>c regs mem\<^sub>C mds\<^sub>C mds\<^sub>A mem\<^sub>A)
  thus ?case
    apply(clarsimp split:prod.splits)
    apply(case_tac "pc < length P\<^sub>e")
     (* Case: in the expression *)
     apply(clarsimp simp:compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)
     apply(force intro:compiled_expr_respects_guarantees' simp:nth_append)
    (* Case: at the Jz *)
    apply(clarsimp simp:RL.respects_own_guarantees_def RL.doesnt_read_or_modify_def RL.doesnt_modify_def RL.doesnt_read_or_modify_vars_def
      \<R>_conds preserves_modes_mem_pair_def mds\<^sub>A_of_def mem\<^sub>A_of_def split del:if_split)
    apply(case_tac "regs r \<noteq> 0")
     apply(metis (no_types, lifting) RL.eval\<^sub>r.jz_f RL.jz_f_dest RL.eval\<^sub>r_bounds length_map nth_append_length snd_conv)
    by (metis (no_types, lifting) RL.eval\<^sub>r.jz_s RL.jz_s_dest RL.eval\<^sub>r_bounds length_map nth_append_length snd_conv)
next (* While - inner *)
  case (13 c\<^sub>A c\<^sub>I b c P' ex nl\<^sub>c C l nl P lp nl' nl'' P\<^sub>e r C\<^sub>e P\<^sub>c l\<^sub>c C\<^sub>c pc mds\<^sub>C mds\<^sub>A mem\<^sub>A regs mem\<^sub>C)
  from "13.hyps"(1-14) "13.prems" show ?case
    apply(clarsimp split:prod.splits)
    apply(clarsimp simp:RL.respects_own_guarantees_def RL.joinable_def)
    apply(rule conjI)
     apply(clarsimp simp:RL.doesnt_read_or_modify_def RL.doesnt_read_or_modify_vars_def)
     apply(frule RL.eval\<^sub>r_no_modify)
     apply(erule_tac x=x in allE)
     apply(clarsimp simp:compile_cmd_input_reqs_def)
     apply(rename_tac x mds mem pc' regs' mds' mem')
     apply(clarsimp simp:RL.fresh_label_def)
     apply(frule while_pc_forward_helper, simp+)
     apply(frule while_pc_backward_helper, simp+)
     apply(subgoal_tac
       "(\<forall>v. eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>c), regs), mds, mem(x := v)\<rangle>\<^sub>C \<langle>((pc' - Suc (length P\<^sub>e), map fst P\<^sub>c), regs'), mds', mem'(x := v)\<rangle>\<^sub>C) \<and>
        (\<forall>x\<in>\<C>_vars x. \<forall>v. eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>c), regs), mds, mem(x := v)\<rangle>\<^sub>C \<langle>((pc' - Suc (length P\<^sub>e), map fst P\<^sub>c), regs'), mds', mem'(x := v)\<rangle>\<^sub>C)")
      prefer 2
      apply(solves\<open>frule while_pc_eval_helper, simp+\<close>)
     apply clarsimp
     apply(frule while_joinable_backward_helper, simp+)
     apply(subgoal_tac "RL.joinable_forward (map fst P\<^sub>c) [(l\<^sub>c, Jmp lp)]")
      prefer 2
      apply(force simp:RL.joinable_forward_def compile_cmd_output_label)
     apply(rule conjI)
      apply clarsimp
      apply(erule_tac x=v in allE)
      apply(solves\<open>frule_tac pc\<^sub>2="pc - (Suc (length P\<^sub>e))" and mem="mem(x := v)" and mem'="mem'(x := v)" in RL.eval\<^sub>r_context_joins, simp+\<close>)
     apply clarsimp
     apply(thin_tac "\<forall>v. eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>c), regs), mds, mem(x := v)\<rangle>\<^sub>C
             \<langle>((pc' - Suc (length P\<^sub>e), map fst P\<^sub>c), regs'), mds', mem'(x := v)\<rangle>\<^sub>C")
     apply(rename_tac cv v)
     apply(erule_tac x=cv in ballE)
      apply(erule_tac x=v in allE)
      apply(solves\<open>frule_tac pc\<^sub>2="pc - (Suc (length P\<^sub>e))" and mem="mem(cv := v)" and mem'="mem'(cv := v)" in RL.eval\<^sub>r_context_joins, simp+\<close>)
     apply blast
    apply(clarsimp simp:RL.doesnt_modify_def)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(erule_tac x=x in allE)
    apply(clarsimp simp:compile_cmd_input_reqs_def)
    apply(rename_tac x mds mem pc' regs' mds' mem')
    apply(clarsimp simp:RL.fresh_label_def)
    apply(frule while_pc_forward_helper, simp+)
    apply(frule while_pc_backward_helper, simp+)
    apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>c), regs), mds, mem\<rangle>\<^sub>C \<langle>((pc' - Suc (length P\<^sub>e), map fst P\<^sub>c), regs'), mds', mem'\<rangle>\<^sub>C")
     prefer 2
     apply(solves\<open>frule while_pc_eval_helper, simp+\<close>)
    by force
next (* While - loop *)
  case (14 c\<^sub>A b c P' ex nl\<^sub>c C l nl P lp nl' nl'' P\<^sub>e r C\<^sub>e P\<^sub>c l\<^sub>c C\<^sub>c pc mds\<^sub>C mds\<^sub>A mem\<^sub>A regs mem\<^sub>C)
  from "14.hyps"(1-12) show ?case
    apply(clarsimp split:prod.splits)
    apply(case_tac "pc < Suc (length P\<^sub>e + length P\<^sub>c)")
     (* Case: Inner epilogue steps *)
     apply(frule \<R>_pairs_stop_implies_halt_or_epilogue)
      apply blast
     apply(erule disjE)
      apply force
     apply(subgoal_tac "snd (P ! pc) = snd (fst (P\<^sub>c ! (pc - Suc (length P\<^sub>e))))")
      prefer 2
      apply(force simp:nth_append)
     apply(clarsimp simp:RL.respects_own_guarantees_def RL.doesnt_read_or_modify_def
       RL.doesnt_modify_def RL.doesnt_read_or_modify_vars_def epilogue_step_forms_def)
     apply(erule disjE)
      apply(metis (no_types, lifting) RL.eval\<^sub>r.jmp RL.eval\<^sub>r_bounds RL.jmp_dest)
     apply(metis (no_types, lifting) RL.eval\<^sub>r.tau RL.eval\<^sub>r_bounds RL.tau_dest)
    (* Case: Jmp *)
    apply(clarsimp simp:RL.respects_own_guarantees_def RL.doesnt_read_or_modify_def
      RL.doesnt_modify_def RL.doesnt_read_or_modify_vars_def)
    apply(subgoal_tac "snd (P ! pc) = Jmp lp")
     prefer 2
     apply(force simp:nth_append)
    by (metis (no_types, lifting) RL.eval\<^sub>r.jmp RL.eval\<^sub>r_bounds RL.jmp_dest)
next (* Epilogue step *)
  case 15
  thus ?case
    apply(clarsimp simp:RL.respects_own_guarantees_def RL.doesnt_read_or_modify_def
      RL.doesnt_modify_def RL.doesnt_read_or_modify_vars_def epilogue_step_forms_def)
    apply(erule disjE)
     apply(metis (no_types, lifting) RL.eval\<^sub>r.jmp RL.jmp_dest)
    by (metis (no_types, lifting) RL.eval\<^sub>r.tau RL.tau_dest)
next (* Lock - acq *)
  case (16 c\<^sub>A k pc P mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    apply(clarsimp simp:RL.respects_own_guarantees_def)
    apply(rename_tac x)
    apply(case_tac x)
     apply(force simp:\<R>_conds lock_consistent_mds_def no_lock_mds_def)
    using lock_acq_doesnt_modify_vars lock_acq_doesnt_read_vars
    by blast
next (* Lock - rel *)
  case (17 c\<^sub>A k pc P mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    apply(clarsimp simp:RL.respects_own_guarantees_def)
    apply(rename_tac x)
    apply(case_tac x)
     apply(force simp:\<R>_conds lock_consistent_mds_def no_lock_mds_def)
    using lock_rel_doesnt_modify_vars lock_rel_doesnt_read_vars
    by blast
qed

lemma preserves_local_guarantee_compliance_\<R>:
  "preserves_local_guarantee_compliance \<R>"
  using preserves_local_guarantee_compliance_\<R>'
  unfolding preserves_local_guarantee_compliance_def2
  by clarify

(* TODO: Need something to link respects_own_guarantees back to locally_sound_mode_use,
  seeing as all we'll have here coming out of the refinement relation is the former. *)
thm WL.locally_sound_mode_use_def2

subsection {* Proof that the specified relation is indeed a refinement *}

subsubsection {* That \<R> is closed under mode-permitted havoc *}

lemma closed_others_\<R>':
assumes
  a1: "(\<langle>c\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" and
  a2: "mem\<^sub>C =\<^bsub>mds\<^sub>C\<^esub>\<^sup>r mem\<^sub>C'"
shows "(\<langle>c\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> \<R>"
using a1 a2
proof (induct c\<^sub>A "mds\<^sub>A_of mds\<^sub>C" "mem\<^sub>A_of mem\<^sub>C" pc P regs mds\<^sub>C mem\<^sub>C rule:\<R>_induct)
  case 1 thus ?case by (simp add: risc_compilation_axioms)
next
  case 2 show "(\<langle>c\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>"
    using a1 by simp
next
  case (3 c\<^sub>A c\<^sub>1\<^sub>A c\<^sub>2\<^sub>A P' l\<^sub>2 nl\<^sub>2 C\<^sub>2 C l nl c\<^sub>1\<^sub>A' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 P\<^sub>2 pc P regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    (* Seq *)
    apply clarsimp
    apply(frule conds_\<R>)
    apply(clarsimp simp:\<R>_conds)
    apply(rule \<R>.seq, simp+)
       apply(clarsimp simp:RL.mds_havoc_eq_def var_asm_not_written_def regrec_mem_consistent_def)
       apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def regs_noRW_pair_def)
       apply(clarsimp simp:regrec_stable_def asmrec_expr_stable'_def asmrec_mds_consistent'_def ran_def WL.eval_vars_det\<^sub>A mem\<^sub>A_of_def id_def asmrec_var_stable'_def to_prog_mem_def)
       apply(meson WL.eval_vars_det\<^sub>A)
      apply force
     apply force
    by blast
next
  case 4 thus ?case
    (* Join *)
    by (simp add:\<R>.join)
next
  case 5 thus ?case
    (* Stop *)
    by (fastforce intro:\<R>.stop simp:\<R>_conds_closed_others)
next
  case 6 thus ?case
    (* Skip *)
    by (fastforce intro:\<R>.skip_nop simp:\<R>_conds_closed_others)
next
  case (7 c\<^sub>A v e P' l' nl\<^sub>2 C' C l nl P P\<^sub>e r C\<^sub>e pc regs mem\<^sub>C mds\<^sub>C)
  thus ?case
    (* Assign - expr *)
    apply(rule_tac \<R>.assign_expr)
               apply(clarsimp simp:Let_def)
               apply(clarsimp simp:RL.mds_havoc_eq_def var_asm_not_written_def regrec_mem_consistent_def)
               apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def regs_noRW_pair_def)
               apply(force simp: mem\<^sub>A_of_def id_def)
              apply force+
         apply(clarsimp simp:RL.mds_havoc_eq_def var_asm_not_written_def regrec_mem_consistent_def)
         apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def regs_noRW_pair_def)
         apply(clarsimp simp:compile_cmd_input_reqs_def Let_def split:prod.splits)
         (* This is asking us to prove that every expression in the register record
           evaluates to the same value in the two memories.
           It's clear this is only possible if we've ensured register records can only refer
           to stable variables. *)
         apply(frule_tac pc=pc in compiled_expr_maintains_regrec_stability, simp+)
         apply(clarsimp simp:regrec_stable_def asmrec_expr_stable'_def asmrec_mds_consistent'_def ran_def WL.eval_vars_det\<^sub>A mem\<^sub>A_of_def id_def asmrec_var_stable'_def to_prog_mem_def)
         apply(meson WL.eval_vars_det\<^sub>A)
        apply force
       apply force
      apply force
     apply force
    using \<R>_conds \<R>_conds_closed_others by auto[1]
next
  case (8 c\<^sub>A v e P' l' nl\<^sub>2 C' C l nl P P\<^sub>e r C\<^sub>e pc regs mem\<^sub>C mds\<^sub>C)
  thus ?case
    (* Assign - store *)
    apply -
    apply(rule_tac C'=C' in \<R>.assign_store, simp+)
         apply(clarsimp simp:RL.mds_havoc_eq_def var_asm_not_written_def regrec_mem_consistent_def)
         apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def regs_noRW_pair_def)
         apply(clarsimp simp:compile_cmd_input_reqs_def Let_def split:prod.splits)
         apply(frule_tac pc=pc in compiled_expr_maintains_regrec_stability, simp+)
         apply(clarsimp simp:regrec_stable_def asmrec_expr_stable'_def asmrec_mds_consistent'_def ran_def WL.eval_vars_det\<^sub>A mem\<^sub>A_of_def id_def asmrec_var_stable'_def to_prog_mem_def)
         apply(meson WL.eval_vars_det\<^sub>A)
        apply force
       apply force
      apply force
     apply force
    using \<R>_conds \<R>_conds_closed_others by auto[1]
next
  case (9 c\<^sub>A e c\<^sub>1 c\<^sub>2 P' l' nl\<^sub>2 C' C l nl P P\<^sub>e r C\<^sub>1 br nl' ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 P\<^sub>2 l\<^sub>2 C\<^sub>3 pc regs mem\<^sub>C mds\<^sub>C)
  thus ?case
    (* If - expr *)
    apply -
    apply(rule \<R>.if_expr, simp+)
        apply(clarsimp simp:RL.mds_havoc_eq_def var_asm_not_written_def regrec_mem_consistent_def)
        apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def regs_noRW_pair_def)
        apply(clarsimp simp:compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)
        apply(frule_tac pc=pc in compiled_expr_maintains_regrec_stability, simp+)
        apply(clarsimp simp:regrec_stable_def asmrec_expr_stable'_def asmrec_mds_consistent'_def ran_def WL.eval_vars_det\<^sub>A mem\<^sub>A_of_def id_def asmrec_var_stable'_def to_prog_mem_def)
        apply(meson WL.eval_vars_det\<^sub>A)
       apply force
      apply force
     using \<R>_conds \<R>_conds_closed_others apply auto[1]
    by force
next
  case 10 thus ?case using \<R>.if_c1 by simp
next
  case 11 thus ?case using \<R>.if_c2 by simp
next
  (* While - expr *)
  case (12 c\<^sub>A pc b c P' ex nl\<^sub>c C l nl P lp nl' nl'' P\<^sub>e r C\<^sub>e P\<^sub>c l\<^sub>c C\<^sub>c regs mem\<^sub>C mds\<^sub>C)
  hence "regrec_stable (C\<lparr>regrec := Map.empty\<rparr>)"
    by (clarsimp simp:regrec_stable_def)
  thus ?case using 12
    apply -
    apply(rule \<R>.while_expr, simp+)
        apply(clarsimp simp:RL.mds_havoc_eq_def var_asm_not_written_def regrec_mem_consistent_def split:prod.splits)
        apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def regs_noRW_pair_def)
        apply(frule_tac pc=pc in compiled_expr_maintains_regrec_stability, simp+)
         apply(force simp:compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)
        apply(clarsimp simp:regrec_stable_def asmrec_expr_stable'_def asmrec_mds_consistent'_def ran_def WL.eval_vars_det\<^sub>A mem\<^sub>A_of_def id_def asmrec_var_stable'_def to_prog_mem_def)
        apply(meson WL.eval_vars_det\<^sub>A)
       apply force
      apply force
     using \<R>_conds \<R>_conds_closed_others apply auto[1]
    by blast
next
  case 13 thus ?case using \<R>.while_inner by simp
next
  case 14 thus ?case using \<R>.while_loop by simp
next
  case 15 thus ?case
    apply -
    apply(rule \<R>.epilogue_step, simp+)
     using \<R>_conds_closed_others apply blast
    using reachable_pair_mem_diff reachable_pair_trans by fastforce
next
  case 16 thus ?case
    by (fastforce intro:\<R>.lock_acq simp:\<R>_conds_closed_others)
next
  case 17 thus ?case
    by (fastforce intro:\<R>.lock_rel simp:\<R>_conds_closed_others)
qed

lemma closed_others_\<R>:
  "closed_others \<R>"
  unfolding closed_others_def
  apply clarsimp
  apply(frule_tac mem\<^sub>C'=mem\<^sub>C' in closed_others_\<R>')
   using RL.mds_havoc_eq_def
   apply blast
  by force

subsubsection {* That \<R> preserves mode and memory state *}

lemma preserves_modes_mem_\<R>:
  "preserves_modes_mem \<R>"
  unfolding preserves_modes_mem_pair_eq
  by (fastforce dest: conds_\<R> simp: \<R>_conds)

subsubsection {* That \<R> introduces only private variables *}

lemma new_vars_private_\<R>:
assumes
  a1: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" and
  a2: "eval_abv\<^sub>C \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', P'), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C"
shows
  "(\<forall>v\<^sub>C. (mem\<^sub>C' v\<^sub>C \<noteq> mem\<^sub>C v\<^sub>C \<or> dma\<^sub>r mem\<^sub>C' v\<^sub>C < dma\<^sub>r mem\<^sub>C v\<^sub>C) \<and> v\<^sub>C \<notin> range id \<longrightarrow>
          v\<^sub>C \<in> mds\<^sub>C' AsmNoReadOrWrite) \<and>
   mds\<^sub>C AsmNoReadOrWrite - range id \<subseteq> mds\<^sub>C' AsmNoReadOrWrite - range id"
  (* TRIVIAL with id mapping between mems! *)
  by force

subsubsection {* That \<R> itself is preserved by pairwise evaluation *}

lemma abs_steps_left_seq_join:
  "pc < length P\<^sub>1 \<Longrightarrow> abs_steps' c\<^sub>1\<^sub>A (pc, P\<^sub>1) = n \<Longrightarrow> abs_steps' (c\<^sub>1\<^sub>A ;; c\<^sub>2\<^sub>A) (pc, P\<^sub>1 @ P\<^sub>2) = n"
  by (clarsimp simp:abs_steps'_def nth_append split:if_splits)

lemma \<R>_pairs_halt_implies_stop:
assumes
  a1: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((length P, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>"
shows "c\<^sub>A = Stop"
  using a1 risc_compilation_axioms
  by (induct c\<^sub>A mds\<^sub>A mem\<^sub>A "length P" P regs mds\<^sub>C mem\<^sub>C rule:\<R>_induct,
      (force split:prod.splits simp:compile_cmd_input_reqs_def Let_def)+)

lemma to_prog_mem_exp_ev_commute:
  "(to_prog_mem mem\<^sub>A)(v := exp_ev (to_prog_mem mem\<^sub>A) e) =
    to_prog_mem (mem\<^sub>A(Var v := exp_ev (to_prog_mem mem\<^sub>A) e))"
  unfolding mem\<^sub>A_of_def
  by (induct e, (simp add: to_prog_mem_def)+)

(* NB: Box of tools currently laid out for \<R>_preservation *)
lemma compiled_cmd_eval_maintains_regrec_consistency':
  "(P, l', nl', C', False) = (compile_cmd C l nl c) \<Longrightarrow>
   compile_cmd_input_reqs C l nl c \<Longrightarrow>
   (\<langle>((pc, map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C, \<langle>((pc', map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RL.eval\<^sub>r \<Longrightarrow>
   (* Restrictions on the current state *)
   pc < length P \<Longrightarrow>
   regrec_mem_consistent (map snd P ! pc) regs mem\<^sub>C \<Longrightarrow>
   \<forall>v\<^sub>C. v\<^sub>C \<notin> range id \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite \<Longrightarrow>
   if pc' < length P then regrec_mem_consistent (map snd P ! pc') regs' mem\<^sub>C'
                     else regrec_mem_consistent C' regs' mem\<^sub>C'"
  unfolding compile_cmd_input_reqs_def
  using compiled_cmd_eval_maintains_regrec_consistency
  by (metis (no_types, lifting) length_map zip_map_fst_snd)

(* TODO: Move up, or merge with compile_cmd_first_regrec_subset_input *)
lemma compile_cmd_first_asmrec_input:
  "(P, l', nl', C', False) = (compile_cmd C l nl c) \<Longrightarrow>
   \<not> banned_WhileLang_forms c \<Longrightarrow>
   P \<noteq> [] \<Longrightarrow>
   asmrec (map snd P ! 0) = asmrec C"
  apply(induct c arbitrary:P l' nl' C' C l nl)
         apply(clarsimp simp:Let_def last_conv_nth zip_eq_conv split:prod.splits if_splits)
            using compile_expr_empty_maintains_comprec map_le_def apply fastforce
           using compile_expr_first_comprec_input map_le_def apply(force simp: nth_append)
          using compile_expr_empty_maintains_comprec map_le_def apply fastforce
         using compile_expr_first_comprec_input map_le_def apply(force simp: nth_append)
        apply(force simp:last_conv_nth zip_eq_conv map_le_def Let_def split:prod.splits)
       apply(force simp:last_conv_nth zip_eq_conv map_le_def)
      apply(force simp:last_conv_nth zip_eq_conv map_le_def)
     apply(clarsimp simp:last_conv_nth zip_eq_conv split:prod.splits)
     apply(metis (no_types, lifting) compiled_cmd_nonempty fst_conv length_greater_0_conv nth_append)
    apply(clarsimp simp:Let_def last_conv_nth zip_eq_conv restrict_map_def map_le_def split:prod.splits)
    apply(metis (no_types, hide_lams) compile_expr_empty_maintains_comprec compile_expr_first_comprec_input length_greater_0_conv nth_append nth_map)
   apply(simp add:last_conv_nth zip_eq_conv split:prod.splits split del:if_split)
   apply(clarsimp simp:Let_def nth_append split del:if_split)
   apply clarsimp
   apply(rule conjI)
    using compile_expr_first_comprec_input
    apply(force simp: map_le_def)
   (* contradiction because the expr would only be empty if the input regrec was nonempty *)
   apply(force dest: compile_expr_emptyD)
  by simp

lemma compile_cmd_first_asmrec_input':
  "compile_cmd C l nl c = (P, l', nl', C', False) \<Longrightarrow>
   \<not> banned_WhileLang_forms c \<Longrightarrow>
   P \<noteq> [] \<Longrightarrow>
   asmrec (map snd P ! 0) = asmrec C"
  using compile_cmd_first_asmrec_input by metis

lemma compiled_cmd_eval_maintains_asmrec_consistency:
  "(zip P Cs, l', nl', C', False) = (compile_cmd C l nl c) \<Longrightarrow>
   \<not> banned_WhileLang_forms c \<Longrightarrow>
   no_unstable_exprs c C \<Longrightarrow>
   \<forall> x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   length P = length Cs \<Longrightarrow>
   pc < length P \<Longrightarrow>
   asmrec_mds_consistent (Cs ! pc) mds\<^sub>C \<Longrightarrow>
   (\<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C, \<langle>((pc', P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RL.eval\<^sub>r \<Longrightarrow>
   if pc' < length P then asmrec_mds_consistent (Cs ! pc') mds\<^sub>C'
                     else asmrec_mds_consistent C' mds\<^sub>C'"
  apply(induct c arbitrary: P Cs l' nl' C' C l nl pc pc')
         (* Assign *)
         apply(clarsimp simp:zip_eq_conv Let_def RL.lc_havoc_eq_def split:prod.splits)
         apply(rename_tac v e C l nl pc pc' P r C' fail')
         apply(rule conjI)
          (* pc' not at the end *)
          apply clarsimp
          apply(case_tac "pc < length P")
           apply(subgoal_tac "compile_expr_output_forms (snd (fst (P ! pc)))")
            prefer 2
            using compile_expr_output_forms
            apply metis
           apply clarsimp
           apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc, map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
                              \<langle>((pc', map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
            prefer 2
            using RL.eval\<^sub>r_head
            apply(metis Suc_leI length_map not_less_eq_eq)
           apply(simp add:nth_append)
           apply(frule compiled_expr_eval_maintains_asmrec_consistency, simp+)
           apply fastforce
          apply(simp add:nth_append)
          apply(subgoal_tac "pc = length P")
           prefer 2
           apply fastforce
          apply clarsimp
          apply(fastforce dest:RL.store_dest[rotated] simp:nth_append)
         (* pc' at the end *)
         apply clarsimp
         apply(subgoal_tac "pc' = Suc (length P)")
          prefer 2
          apply(force dest:RL.eval\<^sub>r_bounds)
         apply(case_tac "pc < length P")
          (* shouldn't be possible because there are no jumps in compiled exprs *)
          using compiled_expr_only_increments
          apply blast
         apply(subgoal_tac "pc = length P")
          prefer 2
          apply clarsimp
         apply clarsimp
         (* somewhat a replay of the case 1 RL.store_dest proof *)
         apply(drule_tac v=v and r=r in RL.store_dest[rotated])
          apply(force simp:nth_append)
         apply(fastforce simp add:nth_append WL.eval_vars_det\<^sub>A mem\<^sub>A_of_def id_def)
        (* Skip *)
        apply(force dest:RL.nop_dest[rotated] simp:zip_eq_conv)
       (* LockAcq *)
       apply(force elim:RL.lock_acq_elim[rotated] simp:zip_eq_conv asmrec_mds_consistent'_def no_unstable_exprs_def lock_acq_upd_def split:prod.splits)
      (* LockRel *)
      apply(force elim:RL.lock_rel_elim[rotated] simp:zip_eq_conv asmrec_mds_consistent'_def no_unstable_exprs_def lock_rel_upd_def split:prod.splits)
     (* Seq *)
     apply(clarsimp simp:zip_eq_conv split:prod.splits)
     apply(rename_tac c1 c2 C l nl pc pc' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 l' nl' C' fail')
     apply(subgoal_tac "\<forall>x. l\<^sub>1 = Some x \<longrightarrow> x < nl\<^sub>1")
      prefer 2
      using compile_cmd_maintains_labelrec_freshness
      apply force
     apply(subgoal_tac "RL.joinable (map fst P\<^sub>1) (map fst P\<^sub>2)")
      prefer 2
      using compiled_cmds_joinable
      apply force
     apply(clarsimp simp:RL.joinable_def)
     apply(subgoal_tac "no_unstable_exprs c1 C")
      prefer 2
      apply(force simp:no_unstable_exprs_def split:prod.splits)
     apply(subgoal_tac "no_unstable_exprs c2 C\<^sub>1")
      prefer 2
      apply(force simp:no_unstable_exprs_def compile_cmd_consistent_banned_unstable_exprs split:prod.splits)
     apply(case_tac "pc < length P\<^sub>1")
      (* in P\<^sub>1 *)
      apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc, map fst P\<^sub>1), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', map fst P\<^sub>1), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
       prefer 2
       apply(metis RL.eval\<^sub>r_head RL.eval\<^sub>r_head_bounds length_map)
      apply(subgoal_tac "if pc' < length P\<^sub>1 then asmrec_mds_consistent ((map snd P\<^sub>1) ! pc') mds\<^sub>C'
          else asmrec_mds_consistent C\<^sub>1 mds\<^sub>C'")
       prefer 2
       apply(erule_tac x="map fst P\<^sub>1" in meta_allE)
       apply(erule_tac x="map snd P\<^sub>1" in meta_allE)
       apply(erule_tac x="l\<^sub>1" in meta_allE)
       apply(erule_tac x="nl\<^sub>1" in meta_allE)
       apply(erule_tac x="C\<^sub>1" in meta_allE)
       apply(erule_tac x="C" in meta_allE)
       apply(erule_tac x="l" in meta_allE)
       apply(erule_tac x="nl" in meta_allE)
       apply(erule_tac x="pc" in meta_allE)
       apply(erule_tac x="pc'" in meta_allE)
       apply(force simp:zip_map_fst_snd nth_append)
      apply(rule conjI)
       apply(clarsimp split:if_splits simp:nth_append)
       apply(frule_tac C=C\<^sub>1 in compile_cmd_first_asmrec_input')
         apply force
        apply force
       apply(metis (no_types, hide_lams) compiled_cmd_nonempty diff_is_0_eq' RL.eval\<^sub>r_bounds fst_conv length_greater_0_conv length_map nth_map)
      apply(metis (no_types, hide_lams) Suc_eq_plus1 add_diff_cancel_left' compiled_cmd_nonempty diff_is_0_eq RL.eval\<^sub>r_bounds fst_conv le_imp_less_Suc length_append length_greater_0_conv length_map map_append nat_neq_iff not_less_eq)
     (* in P\<^sub>2 *)
     apply(subgoal_tac "pc - length (map fst P\<^sub>1) < length (map fst P\<^sub>2)")
      prefer 2
      apply force
     apply(subgoal_tac "pc' - length (map fst P\<^sub>1) \<le> length (map fst P\<^sub>2)")
      prefer 2
      apply(metis (no_types, lifting) diff_diff_left diff_is_0_eq RL.eval\<^sub>r_bounds length_append)
     apply(subgoal_tac "pc' = length (map fst P\<^sub>1) + (pc' - length (map fst P\<^sub>1))")
      prefer 2
      apply(metis (no_types) Nat.add_diff_assoc Suc_le_lessD add_diff_cancel_left' RL.eval\<^sub>r_no_backtrack_over_join length_map not_less_eq_eq)
     apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - length (map fst P\<^sub>1), map fst P\<^sub>2), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc' - length (map fst P\<^sub>1), map fst P\<^sub>2), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      apply(rule_tac P\<^sub>1="map fst P\<^sub>1" in RL.eval\<^sub>r_join_right, simp+)
     apply(subgoal_tac "if pc' - length (map fst P\<^sub>1) < length (map fst P\<^sub>2)
         then asmrec_mds_consistent ((map snd P\<^sub>2) ! (pc' - length (map fst P\<^sub>1))) mds\<^sub>C'
         else asmrec_mds_consistent C' mds\<^sub>C'")
      prefer 2
      apply(rotate_tac 6)
      apply(erule_tac x="map fst P\<^sub>2" in meta_allE)
      apply(erule_tac x="map snd P\<^sub>2" in meta_allE)
      apply(erule_tac x="l'" in meta_allE)
      apply(erule_tac x="nl'" in meta_allE)
      apply(erule_tac x="C'" in meta_allE)
      apply(erule_tac x="C\<^sub>1" in meta_allE)
      apply(erule_tac x="l\<^sub>1" in meta_allE)
      apply(erule_tac x="nl\<^sub>1" in meta_allE)
      apply(erule_tac x="pc - length (map fst P\<^sub>1)" in meta_allE)
      apply(erule_tac x="pc' - length (map fst P\<^sub>1)" in meta_allE)
      apply(force simp:zip_map_fst_snd nth_append)
     apply(force simp add: nth_append)
    (* If *)
    apply(clarsimp simp:zip_eq_conv Let_def RL.lc_havoc_eq_def split:prod.splits)
    apply(rename_tac b c\<^sub>1 c\<^sub>2 C l nl pc pc' P\<^sub>0 r C\<^sub>1 fail\<^sub>1 br nl' ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 fail\<^sub>2 P\<^sub>2 l\<^sub>2 nl\<^sub>2 C\<^sub>3 fail\<^sub>3)
    apply(subgoal_tac "no_unstable_exprs c\<^sub>1 C\<^sub>1 \<and> no_unstable_exprs c\<^sub>2 C\<^sub>1 \<and> asmrec C\<^sub>2 = asmrec C\<^sub>3")
     prefer 2
     apply(force simp:no_unstable_exprs_def compiled_expr_asmrec_constancy_end compile_cmd_consistent_banned_unstable_exprs split:prod.splits)
    apply(case_tac "pc < length P\<^sub>0")
     (* Case: in the compiled boolean expr *)
     apply(subgoal_tac "pc' = Suc pc")
      prefer 2
      using compiled_expr_only_increments apply force
     apply(subgoal_tac "compile_expr_output_forms (snd (fst (P\<^sub>0 ! pc)))")
      prefer 2
      apply(metis compile_expr_output_forms)
      apply clarsimp
     apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc, map fst P\<^sub>0), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((Suc pc, map fst P\<^sub>0), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      apply(metis (no_types, hide_lams) RL.eval\<^sub>r_head Suc_leI length_map)
     apply(simp add:nth_append)
     apply(frule compiled_expr_eval_maintains_asmrec_consistency, simp+)
     apply fastforce
    (* Case: at the Jz br *)
    apply(case_tac "pc = length P\<^sub>0")
     (* sub-case: Jz jumped to br *)
     apply(case_tac "regs r = 0")
      apply(frule_tac l=br and r=r in RL.jz_s_dest[rotated])
        apply(simp add:nth_append)+
      (* Prove that br is located at the beginning of P\<^sub>2 *)
      apply(subgoal_tac "pc' = Suc (Suc (length P\<^sub>0 + length P\<^sub>1))")
       prefer 2
       using if_jz_br_hit_helper apply force
      apply(clarsimp simp:nth_append)
      using compile_cmd_first_asmrec_input compiled_cmd_nonempty
      apply(metis (no_types, hide_lams) fst_conv length_greater_0_conv nth_map)
     (* sub-case: Jz didn't jump *)
     apply clarsimp
     apply(frule_tac l=br and r=r in RL.jz_f_dest[rotated])
       apply(simp add:nth_append)+
     using compile_cmd_first_asmrec_input compiled_cmd_nonempty
     apply(metis (no_types, hide_lams) fst_conv length_greater_0_conv nth_map)
    (* Case: in P\<^sub>1 *)
    apply(subgoal_tac "pc > length P\<^sub>0")
     prefer 2
     apply force
    apply clarsimp
    apply(case_tac "pc < Suc (length P\<^sub>0 + length P\<^sub>1)")
     apply(subgoal_tac "pc - Suc (length P\<^sub>0) < length P\<^sub>1")
      prefer 2
      apply force
     (* Firstly rule out a jump backwards into P\<^sub>0 *)
     apply(subgoal_tac "pc' \<ge> Suc (length P\<^sub>0)")
      prefer 2
      using if_c1_pc_backward_helper apply force
     (* Now rule out a jump any further than the end of P\<^sub>1.
       l\<^sub>1 is the label for the Jmp ex immediately after it, so this should catch any
       oob jumps from P\<^sub>1 assuming l\<^sub>1 is the only oob jump label of P\<^sub>1 *)
     apply(subgoal_tac "pc' \<le> Suc (length P\<^sub>0 + length P\<^sub>1)")
      prefer 2
      using if_c1_pc_forward_helper apply force
     apply(erule_tac x="map fst P\<^sub>1" in meta_allE)
     apply(erule_tac x="map snd P\<^sub>1" in meta_allE)
     apply(erule_tac x="l\<^sub>1" in meta_allE)
     apply(erule_tac x="nl\<^sub>1" in meta_allE)
     apply(erule_tac x="C\<^sub>2" in meta_allE)
     apply(erule_tac x="C\<^sub>1" in meta_allE)
     apply(erule_tac x="None" in meta_allE)
     apply(erule_tac x="nl''" in meta_allE)
     apply(erule_tac x="pc - Suc (length P\<^sub>0)" in meta_allE)
     apply(erule_tac x="pc' - Suc (length P\<^sub>0)" in meta_allE)
     apply(clarsimp simp:zip_map_fst_snd nth_append)
     apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>0), map fst P\<^sub>1), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
         \<langle>((pc' - Suc (length P\<^sub>0), map fst P\<^sub>1), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      using if_c1_eval_helper apply force
     apply force
    (* Case: at the Jmp ex *)
    apply(case_tac "pc = Suc (length P\<^sub>0 + length P\<^sub>1)")
     apply(frule_tac l=ex in RL.jmp_dest[rotated])
      apply(simp add:nth_append)+
     (* Prove that ex is not in the program *)
     apply(subgoal_tac "pc' = Suc (Suc (Suc (length P\<^sub>0 + (length P\<^sub>1 + length P\<^sub>2))))")
      prefer 2
      using if_jz_ex_helper apply force
     apply force
    (* Case: in P\<^sub>2 *)
    apply(subgoal_tac "pc \<ge> Suc (Suc (length P\<^sub>0 + length P\<^sub>1))")
     prefer 2
     apply force
    apply clarsimp
    apply(case_tac "pc < Suc (Suc (length P\<^sub>0 + (length P\<^sub>1 + length P\<^sub>2)))")
     apply(subgoal_tac "pc - Suc (Suc (length P\<^sub>0 + length P\<^sub>1)) < length P\<^sub>2")
      prefer 2
      apply force
     (* Firstly rule out a jump backwards into P\<^sub>0 or P\<^sub>1 *)
     apply(subgoal_tac "pc' \<ge> Suc (Suc (length P\<^sub>0 + length P\<^sub>1))")
      prefer 2
      using if_c2_pc_backward_helper apply force
     (* Now rule out a jump any further than the end of P\<^sub>2 *)
     apply(subgoal_tac "pc' \<le> Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2))")
      prefer 2
      using if_c2_pc_forward_helper apply force
     apply(subgoal_tac "br < nl\<^sub>1")
      prefer 2
      apply(clarsimp simp:RL.fresh_label_def)
      apply(meson Suc_leD Suc_le_lessD compile_cmd_output_labelrec)
     apply(subgoal_tac "asmrec_mds_consistent (snd (P\<^sub>2 ! (pc - Suc (Suc (length P\<^sub>0 + length P\<^sub>1))))) mds\<^sub>C")
      prefer 2
      apply(force simp:nth_append split:if_splits)
     apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - Suc (Suc (length P\<^sub>0 + length P\<^sub>1)), map fst P\<^sub>2), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
         \<langle>((pc' - Suc (Suc (length P\<^sub>0 + length P\<^sub>1)), map fst P\<^sub>2), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      using if_c2_eval_helper apply force
     apply(rotate_tac 6)
     apply(erule_tac x="map fst P\<^sub>2" in meta_allE)
     apply(erule_tac x="map snd P\<^sub>2" in meta_allE)
     apply(erule_tac x="l\<^sub>2" in meta_allE)
     apply(erule_tac x="nl\<^sub>2" in meta_allE)
     apply(erule_tac x="C\<^sub>3" in meta_allE)
     apply(erule_tac x="C\<^sub>1" in meta_allE)
     apply(erule_tac x="Some br" in meta_allE)
     apply(erule_tac x="nl\<^sub>1" in meta_allE)
     apply(erule_tac x="pc - Suc (Suc (length P\<^sub>0 + length P\<^sub>1))" in meta_allE)
     apply(erule_tac x="pc' - Suc (Suc (length P\<^sub>0 + length P\<^sub>1))" in meta_allE)
     apply(force simp:zip_map_fst_snd nth_append)
    (* Case: at final Tau *)
    apply(case_tac "pc = Suc (Suc (length P\<^sub>0 + (length P\<^sub>1 + length P\<^sub>2)))")
     apply clarsimp
     apply(frule RL.tau_dest[rotated])
      apply(force simp:nth_append)
     apply(force simp:nth_append)
    (* Case: end *)
    apply force
   (* While *)
   apply(clarsimp simp:zip_eq_conv split:prod.splits split del:if_split)
   apply(rename_tac b c C l nl pc pc' lp nl' ex nl'' P\<^sub>e r C\<^sub>B fail\<^sub>e P l' nl''' C\<^sub>E fail\<^sub>c)
   apply(clarsimp simp:RL.fresh_label_def split del:if_split)
   apply(subgoal_tac "asmrec C = asmrec C\<^sub>B")
    prefer 2
    apply(force dest:compiled_expr_asmrec_constancy_end)
   apply(subgoal_tac "no_unstable_exprs c C \<and> asmrec C\<^sub>B = asmrec C\<^sub>E")
    prefer 2
    apply(force simp:no_unstable_exprs_def compiled_expr_asmrec_constancy_end compile_cmd_consistent_banned_unstable_exprs split:prod.splits)
   apply(case_tac "pc < length P\<^sub>e")
    (* Case: in the 1st compiled boolean expr *)
    apply(subgoal_tac "pc' = Suc pc")
     prefer 2
     using compiled_expr_only_increments apply force
    apply(subgoal_tac "compile_expr_output_forms (snd (fst (P\<^sub>e ! pc)))")
     prefer 2
     apply(metis (no_types, lifting) compile_expr_output_forms)
    apply clarsimp
    apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc, map fst P\<^sub>e), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((Suc pc, map fst P\<^sub>e), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
     prefer 2
     apply(metis (no_types, hide_lams) RL.eval\<^sub>r_head Suc_leI length_map)
    apply(simp add:nth_append)
    apply(frule compiled_expr_eval_maintains_asmrec_consistency, simp+)
    apply force
   (* Case: at the Jz ex *)
   apply(case_tac "pc = length P\<^sub>e")
    (* sub-case: Jz jumped to ex *)
    apply(case_tac "regs r = 0")
     apply(frule_tac l=ex and r=r in RL.jz_s_dest[rotated])
       apply(simp add:nth_append)+
     (* Prove that ex is located at the end of the program *)
     apply(subgoal_tac "pc' = Suc (Suc (length P\<^sub>e + (length P)))")
      prefer 2
      using while_ex_helper apply force
     apply force
    (* sub-case: Jz didn't jump *)
    apply clarsimp
    apply(frule_tac l=ex and r=r in RL.jz_f_dest[rotated])
      apply(simp add:nth_append split del:if_split)+
    apply(case_tac "P = []")
     apply(metis (no_types, lifting) compiled_cmd_nonempty fst_conv)
    apply clarsimp
    using compile_cmd_first_asmrec_input
    apply(metis (no_types, hide_lams) length_greater_0_conv nth_map)
   (* Case: In the inner command *)
   apply(case_tac "pc < (Suc (length P\<^sub>e + (length P)))")
    apply clarsimp
    apply(rule conjI)
     apply clarsimp
     apply(clarsimp simp:nth_append)
     apply(subgoal_tac "pc' \<ge> Suc (length P\<^sub>e)")
      prefer 2
      using while_pc_backward_helper apply force
     apply clarsimp
     apply(subgoal_tac "pc' \<le> Suc (length P\<^sub>e + length P)")
      prefer 2
      (* We appear to get this one for free *)
      apply force
     apply(erule_tac x="map fst P" in meta_allE)
     apply(erule_tac x="map snd P" in meta_allE)
     apply(erule_tac x="l'" in meta_allE)
     apply(erule_tac x="nl'''" in meta_allE)
     apply(erule_tac x="C\<^sub>E" in meta_allE)
     apply(erule_tac x="C\<^sub>B" in meta_allE)
     apply(erule_tac x="None" in meta_allE)
     apply(erule_tac x="Suc ex" in meta_allE)
     apply(erule_tac x="pc - Suc (length P\<^sub>e)" in meta_allE)
     apply(erule_tac x="pc' - Suc (length P\<^sub>e)" in meta_allE)
     apply(clarsimp simp:zip_map_fst_snd nth_append)
     apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
         \<langle>((pc' - Suc (length P\<^sub>e), map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      using while_pc_eval_helper apply force
     apply clarsimp
     apply(subgoal_tac "asmrec_mds_consistent (snd (P ! (pc - Suc (length P\<^sub>e)))) mds\<^sub>C")
      prefer 2
      using RL.eval\<^sub>r_bounds apply fastforce
     apply(force simp:no_unstable_exprs_def)
    apply clarsimp
    apply(frule while_pc_forward_helper, simp+)
   (* Case: at the Jmp *)
   apply(case_tac "pc = (Suc (length P\<^sub>e + (length P)))")
    apply(frule_tac l=lp in RL.jmp_dest[rotated])
     apply(simp add:nth_append split del:if_split)+
    apply(clarsimp simp:regrec_mem_consistent_def)
    apply(case_tac "P\<^sub>e = []")
     apply clarsimp
     apply(fastforce dest:compile_expr_emptyD)
    (* First confirm that lp is at pc 0 *)
    apply(subgoal_tac "RL.res\<^sub>P lp (map fst P\<^sub>e) = 0")
     prefer 2
     using compile_expr_input_label_res\<^sub>P apply force
    apply(subgoal_tac "RL.res\<^sub>P lp (map fst P\<^sub>e @ (None, Jz ex r) # map fst P @ [(l', Jmp lp)]) = 0")
     prefer 2
     using RL.res\<^sub>P_ib_append apply blast
    apply clarsimp
    apply(frule compile_expr_first_comprec_input)
     apply force
    apply force
   apply force
  (* Stop *)
  by force

(* More usable versions of things. TODO: Move up once we know they're in the right shape *)

lemma compiled_cmd_eval_maintains_asmrec_consistency':
  "(P, l', nl', C', False) = (compile_cmd C l nl c) \<Longrightarrow>
   compile_cmd_input_reqs C l nl c \<Longrightarrow>
   (\<langle>((pc, map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C, \<langle>((pc', map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RL.eval\<^sub>r \<Longrightarrow>
   (* Restrictions on the current state *)
   pc < length P \<Longrightarrow>
   asmrec_mds_consistent (map snd P ! pc) mds\<^sub>C \<Longrightarrow>
   if pc' < length P then asmrec_mds_consistent (map snd P ! pc') mds\<^sub>C'
                     else asmrec_mds_consistent C' mds\<^sub>C'"
  unfolding compile_cmd_input_reqs_def
  using compiled_cmd_eval_maintains_asmrec_consistency
  by (metis (no_types, lifting) length_map zip_map_fst_snd)

lemma compiled_cmd_eval_maintains_asmrec_consistency'':
  "compile_cmd C l nl c = (P, l', nl', C', False) \<Longrightarrow>
   compile_cmd_input_reqs C l nl c \<Longrightarrow>
   (\<langle>((pc, map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C, \<langle>((pc', map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RL.eval\<^sub>r \<Longrightarrow>
   (* Restrictions on the current state *)
   pc < length P \<Longrightarrow>
   asmrec_mds_consistent (map snd P ! pc) mds\<^sub>C \<Longrightarrow>
   if pc' < length P then asmrec_mds_consistent (map snd P ! pc') mds\<^sub>C'
                     else asmrec_mds_consistent C' mds\<^sub>C'"
  using compiled_cmd_eval_maintains_asmrec_consistency'
  by metis

lemma compiled_expr_eval_maintains_regrec_consistency':
  "compile_expr C A l e = (P, r, C', False) \<Longrightarrow>
   regrec_mem_consistent (snd (P ! pc)) regs mem\<^sub>C \<Longrightarrow>
   \<forall>v\<^sub>C. v\<^sub>C \<notin> range id \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite \<Longrightarrow>
   finite (dom (regrec C)) \<Longrightarrow>
   finite A \<Longrightarrow>
   pc < length P \<Longrightarrow>
   eval_abv\<^sub>C \<langle>((pc, map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   if pc' < length P then regrec_mem_consistent (map snd P ! pc') regs' mem\<^sub>C'
                     else regrec_mem_consistent C' regs' mem\<^sub>C'"
  using compiled_expr_eval_maintains_regrec_consistency
  by metis

lemma compile_expr_output_forms':
  "compile_expr C A l e = (apm, r, C', False) \<Longrightarrow>
   i < length apm \<Longrightarrow>
   compile_expr_output_forms (snd (fst (apm ! i)))"
  using compile_expr_output_forms by metis

lemma compiled_expr_eval_regrecs':
  "regrec_mem_consistent (snd (P ! pc)) regs mem\<^sub>C \<Longrightarrow>
   \<forall>v\<^sub>C. v\<^sub>C \<notin> range id \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite \<Longrightarrow>
   (P, r, C', False) = compile_expr C A l e \<Longrightarrow>
   finite (dom (regrec C)) \<Longrightarrow>
   finite A \<Longrightarrow>
   pc < length P \<Longrightarrow>
   eval_abv\<^sub>C \<langle>((pc, map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   pc' < length P \<Longrightarrow>
   regrec_mem_consistent (snd (P ! pc')) regs' mem\<^sub>C'"
  using compiled_expr_eval_maintains_regrec_consistency
  by (metis (no_types, hide_lams) nth_map)

lemma \<R>_preservation:
assumes
  a1: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>" and
  a2: "eval_abv\<^sub>C \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', P'), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C"
shows
  "(\<exists>c\<^sub>A' mds\<^sub>A' mem\<^sub>A'.
       WL.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A
        (abs_steps \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C)
        \<langle>c\<^sub>A', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A \<and>
       (\<langle>c\<^sub>A', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>((pc', P'), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> \<R>)"
using a1 a2
proof (induct c\<^sub>A mds\<^sub>A mem\<^sub>A pc P regs mds\<^sub>C mem\<^sub>C arbitrary:pc' P' rule:\<R>_induct)
  case 1
  show ?case by (simp add: risc_compilation_axioms)
next
  case 2
  show ?case using a1 by clarsimp
next (* Seq *)
  case (3 c\<^sub>A c\<^sub>1\<^sub>A c\<^sub>2\<^sub>A P'' l\<^sub>2 nl\<^sub>2 C\<^sub>2 C l nl c\<^sub>1\<^sub>A' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 P\<^sub>2 pc P mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  hence "c\<^sub>1\<^sub>A = Stop \<Longrightarrow> epilogue_step_forms (snd (fst (P\<^sub>1 ! pc)))"
    apply(clarsimp simp:cmd_seq_def)
    by (frule \<R>_pairs_stop_implies_halt_or_epilogue, simp+)
  with 3 have stop_neval_0:
      "c\<^sub>1\<^sub>A = Stop \<Longrightarrow> (abs_steps' c\<^sub>A (pc, P)) = (abs_steps' Stop (pc, map fst P\<^sub>1)) \<and>
                     (abs_steps' Stop (pc, map fst P\<^sub>1)) = 0"
    by (force simp:abs_steps'_def nth_append epilogue_step_not_expr_output)
  from 3 have inner_step: "eval_abv\<^sub>C \<langle>((pc, map fst P\<^sub>1), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', map fst P\<^sub>1), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C"
    apply(clarsimp simp:cmd_seq_def)
    apply(frule RL.eval\<^sub>r_no_modify)
    by (force simp:RL.joinable_def RL.eval\<^sub>r_join_left)
  hence "pc' \<le> length P\<^sub>1"
    by (metis RL.eval\<^sub>r_bounds length_map)
  (* cases to cover:
       epilogue step that doesn't exit: \<R>.seq from c\<^sub>2\<^sub>A to c\<^sub>2\<^sub>A
       epilogue step that exits: \<R>.join from c\<^sub>2\<^sub>A to c\<^sub>2\<^sub>A
       non-epilogue step that doesn't exit: \<R>.seq from c\<^sub>1\<^sub>A ;; c\<^sub>2\<^sub>A to c\<^sub>1\<^sub>A'' ;; c\<^sub>2\<^sub>A
       non-epilogue step that exits: \<R>.join from c\<^sub>1\<^sub>A ;; c\<^sub>2\<^sub>A to c\<^sub>2\<^sub>A
     All of these steps are generalisable to: from (cmd_seq c\<^sub>1\<^sub>A c\<^sub>2\<^sub>A) to (cmd_seq c\<^sub>1\<^sub>A'' c\<^sub>2\<^sub>A)
     where c\<^sub>1\<^sub>A = c\<^sub>1\<^sub>A'' = Stop for epilogue steps, and c\<^sub>1\<^sub>A'' = Stop for an exiting non-epilogue step. *)
  with 3 inner_step stop_neval_0 show "?case"
    apply clarsimp
    apply(subgoal_tac "compile_cmd_input_reqs C l nl c\<^sub>1\<^sub>A'")
     prefer 2
     apply(force simp:compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)
    apply(erule_tac x=pc' in meta_allE)
    apply(erule_tac x="map fst P\<^sub>1" in meta_allE)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply clarsimp
    apply(rename_tac c\<^sub>A'' mds\<^sub>A'' mem\<^sub>A'')
    apply(rule_tac x="cmd_seq c\<^sub>A'' c\<^sub>2\<^sub>A" in exI)
    apply(rule_tac x="mds\<^sub>A''" in exI)
    apply(rule_tac x="mem\<^sub>A''" in exI)
    apply(rule conjI)
     apply(case_tac "c\<^sub>1\<^sub>A = Stop")
      apply(clarsimp simp:cmd_seq_def)
      apply(erule WL.neval_ZeroE)
      using WL.neval_0 apply blast
     apply(clarsimp simp:cmd_seq_def)
     apply(rule conjI)
      apply clarsimp
      apply(case_tac "abs_steps' c\<^sub>1\<^sub>A (pc, map fst P\<^sub>1) = 0")
       using abs_steps_left_seq_join
       apply(metis WL.neval_ZeroE fst_conv)
      apply clarsimp
      using abs_steps_left_seq_join
      apply(metis (no_types, lifting) WL.neval_Suc_simp Zero_not_Suc RL.eval\<^sub>r_bounds neval_append)
     apply(force simp:abs_steps_left_seq_join neval_seq1)
    apply(case_tac "pc' = length P\<^sub>1")
     (* Note we only unlock this I.H. now we actually know we're at the join *)
     apply(erule_tac x="mds\<^sub>C'" in allE)
     apply(erule_tac x="mem\<^sub>C'" in allE)
     apply(erule_tac x="regs'" in allE)
     apply(erule impE)
      apply(rule conjI)
       using "3.hyps"(2)
       apply(frule_tac pc=pc in compiled_cmd_eval_maintains_regrec_consistency', force+)
         apply(force split:prod.splits simp:nth_append)
        apply(force dest:conds_\<R> simp:\<R>_conds regs_noRW_pair_def)
       apply(clarsimp simp:compiled_cmd_init_reqs_def)
       apply(rule conjI)
        apply(frule compiled_cmd_eval_maintains_asmrec_consistency', force+)
         apply(force split:prod.splits simp:nth_append)
        apply force
       apply(frule conds_\<R>)
       using compiled_cmd_init_reqs_def
       apply(force dest:conds_\<R> simp:\<R>_conds)
      apply(force dest:compiled_cmd_maintains_regrec_stability'[where pc=pc'])
     apply clarsimp
     apply(rule_tac P\<^sub>1="map fst P\<^sub>1" and P\<^sub>2="map fst P\<^sub>2" and pc\<^sub>2=0 in \<R>.join)
          apply force
         apply force
        apply force
       apply force
      apply force
     apply(case_tac "c\<^sub>1\<^sub>A = Stop")
      apply(clarsimp simp:cmd_seq_def)
      apply(erule WL.neval_ZeroE)
      apply(force dest:conds_\<R> simp:\<R>_conds preserves_modes_mem_pair_def)
     apply(clarsimp simp:cmd_seq_def \<R>_pairs_halt_implies_stop[where P="map fst P\<^sub>1"])
     apply(force dest:conds_\<R> simp:\<R>_conds preserves_modes_mem_pair_def)
    apply(rule_tac P'=P'' and P\<^sub>1=P\<^sub>1 and P\<^sub>2=P\<^sub>2 in \<R>.seq)
                  apply force
                 using "3.hyps"(2) apply blast
                apply force
               apply force
              apply force
             apply force
            apply force
           apply force
          apply force
         apply force
        apply force
       apply(frule_tac pc=pc in compiled_cmd_eval_maintains_regrec_consistency', simp+)
         apply(force split:prod.splits simp:nth_append)
        apply(force dest:conds_\<R> simp:\<R>_conds regs_noRW_pair_def)
       apply(force split:prod.splits simp:nth_append)
      apply(frule compiled_cmd_eval_maintains_asmrec_consistency', simp+)
       apply(force split:prod.splits simp:nth_append)
      apply(force split:prod.splits simp:nth_append)
     apply(frule compiled_cmd_maintains_regrec_stability'[where pc=pc'], simp+)
     apply(force simp:nth_append split:prod.splits)
    using "3.hyps" by blast
next (* Join *)
  case (4 pc P\<^sub>1 pc\<^sub>2 P\<^sub>2 P c\<^sub>A mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  hence "pc' \<ge> length P\<^sub>1"
    unfolding RL.joinable_def
    by (metis RL.eval\<^sub>r_no_backtrack_over_join RL.eval\<^sub>r_no_modify le_add1)
  with 4 obtain pc\<^sub>2' where
    pc\<^sub>2'_def: "length P\<^sub>1 + pc\<^sub>2' = pc'"
    using le_Suc_ex by blast
  with 4 have "P' = P" using RL.eval\<^sub>r_no_modify by blast
  with 4 pc\<^sub>2'_def show ?case
    apply clarsimp
    apply(erule_tac x="pc\<^sub>2'" in meta_allE)
    apply(erule_tac x=P\<^sub>2 in meta_allE)
    apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc\<^sub>2, P\<^sub>2), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc\<^sub>2', P\<^sub>2), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
     prefer 2
     using RL.eval\<^sub>r_join_right
     apply metis
    apply clarsimp
    apply(rename_tac c\<^sub>A'' mds\<^sub>A'' mem\<^sub>A'')
    apply(subgoal_tac "abs_steps' c\<^sub>A (pc\<^sub>2, P\<^sub>2) = abs_steps' c\<^sub>A (length P\<^sub>1 + pc\<^sub>2, P\<^sub>1 @ P\<^sub>2)")
     prefer 2
     apply(clarsimp simp:abs_steps'_def)
    apply clarsimp
    apply(rule_tac x=c\<^sub>A'' in exI)
    apply(rule_tac x=mds\<^sub>A'' in exI)
    apply(rule_tac x=mem\<^sub>A'' in exI)
    apply(rule conjI)
     apply blast
    apply(frule_tac c\<^sub>A=c\<^sub>A'' in conds_\<R>)
    apply(case_tac "c\<^sub>A''=Stop")
     apply(case_tac "pc\<^sub>2' = length P\<^sub>2")
      apply(rule \<R>.stop, simp+)
      (* TODO: can/should this become a lemma? *)
      apply(clarsimp simp:\<R>_conds)
      apply(rule conjI)
       apply(force simp:preserves_modes_mem_pair_def)
      apply(rule conjI)
       apply(force simp:regs_noRW_pair_def)
      apply(force simp:cmds_stop_consistently_def RL.eval\<^sub>r_oob_pc_halt)
     apply(frule_tac c\<^sub>A=c\<^sub>A'' in \<R>_pairs_stop_implies_halt_or_epilogue, simp+)
     apply clarsimp
     apply(rule_tac P\<^sub>2=P\<^sub>2 in \<R>.join, simp+)
         using RL.eval\<^sub>r_bounds apply fastforce
        apply force
       apply force
      apply force
     apply force
    by (metis (no_types, lifting) \<R>.join \<R>_pairs_halt_implies_stop RL.eval\<^sub>r_bounds le_neq_implies_less)
next (* Stop *)
  case 5
  thus ?case by (simp add: RL.eval\<^sub>r_oob_pc_halt)
next (* Skip *)
  case (6 c\<^sub>A pc P mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    apply clarsimp
    apply(subgoal_tac "(abs_steps' Skip (pc, P)) = 1")
     prefer 2
     apply(force simp:abs_steps'_def compile_expr_output_forms_def epilogue_step_forms_def)
    apply clarsimp
    apply(rule_tac x=Stop in exI)
    apply(rule_tac x=mds\<^sub>A in exI)
    apply(rule_tac x=mem\<^sub>A in exI)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(rule conjI)
     apply(force intro:WL.skip_eval\<^sub>w)
    apply(drule RL.nop_dest[rotated])
     apply force
    apply(rule \<R>.stop)
      apply force
     apply force
    apply(subgoal_tac "mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C")
     prefer 2
     apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def)
    apply(clarsimp simp:\<R>_conds)
    apply(rule conjI)
     apply(force simp:preserves_modes_mem_pair_def)
    apply(rule conjI)
     apply(force simp:regs_noRW_pair_def)
    apply(clarsimp simp:cmds_stop_consistently_def)
    by (meson RL.eval\<^sub>r_halt_iff_oob eq_imp_le)
next (* Assign - expr *)
  case (7 c\<^sub>A v e P' l' nl\<^sub>2 C' C l nl P P\<^sub>e r C\<^sub>e pc regs mem\<^sub>C mds\<^sub>C mds\<^sub>A mem\<^sub>A)
  thus ?case
    apply(clarsimp split:prod.splits simp:Let_def compile_cmd_input_reqs_def)
    apply(rule conjI)
     apply force
    apply clarsimp
    apply(rule_tac x=c\<^sub>A in exI)
    apply(rule_tac x=mds\<^sub>A in exI)
    apply(rule_tac x=mem\<^sub>A in exI)
    apply clarsimp
    apply(rule conjI)
     apply(force dest:compile_expr_output_forms' simp:abs_steps'_def nth_append WL.neval_0)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(subgoal_tac "\<forall>v\<^sub>C. v\<^sub>C \<notin> range id \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite")
     prefer 2
     apply(force simp:\<R>_conds regs_noRW_pair_def)
    apply(subgoal_tac "pc' = Suc pc")
     prefer 2
     apply(metis compiled_expr_only_increments)
    apply clarsimp
    apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc, map fst P\<^sub>e), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', map fst P\<^sub>e), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
     prefer 2
     apply(metis RL.eval\<^sub>r_head length_map Suc_leI)
    apply(subgoal_tac "\<R>_conds \<langle>v \<leftarrow> e, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc', map fst P\<^sub>e), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
     prefer 2
     apply(metis compiled_expr_eval_preserves_\<R>_conds \<R>_conds_head RL.eval\<^sub>r_bounds)
    apply(case_tac "pc' < length P\<^sub>e")
     apply(rule \<R>.assign_expr, force+)
               using "7.hyps" apply blast
              using "7.hyps" apply blast
             apply simp+
          using compiled_expr_eval_regrecs[where C=C and A="{}" and l=l and e=e and r=r and C'=C\<^sub>e, simplified]
          apply fastforce
         (* This is asking us to prove that knowing one regrec entry is stable,
           the next one in the sequence will be too. *)
         apply(metis (no_types, lifting) compiled_expr_eval_maintains_asmrec_consistency)
        apply force
       apply force
      apply force
     apply clarsimp
     using \<R>_conds_append
     apply force
    apply clarsimp
    apply(subgoal_tac "length P\<^sub>e \<le> pc'")
     prefer 2
     apply(metis Suc_leI le_neq_implies_less)
    apply clarsimp
    apply(frule_tac A="{}" in compile_expr_correctness')
      apply force
     apply force
    apply(rule_tac r=r and C\<^sub>e=C\<^sub>e in \<R>.assign_store, force+)
             using "7.hyps" apply blast
            using "7.hyps" apply blast
           apply simp+
         using compiled_expr_eval_regrec_end[where C=C and A="{}" and l=l and e=e and P=P\<^sub>e and r=r and C'=C\<^sub>e, simplified]
         apply force
        using compiled_expr_eval_maintains_asmrec_consistency nat_less_le not_less_eq
        apply auto[1]
       apply force
      apply force
     apply force
    apply(rule \<R>_conds_end_append, simp+)
    apply(clarsimp simp:cmds_stop_consistently_def abs_steps'_def compile_expr_output_forms_def epilogue_step_forms_def nth_append)
    using WL.assign_eval\<^sub>w by blast
next (* Assign - store *)
  case (8 c\<^sub>A v e P' l' nl\<^sub>2 C' C l nl P P\<^sub>e r C\<^sub>e pc regs mem\<^sub>C mds\<^sub>C mds\<^sub>A mem\<^sub>A)
  hence "snd (P ! pc) = Store v r"
    by (clarsimp simp:nth_append Let_def split:prod.splits)
  moreover hence "(abs_steps' (v \<leftarrow> e) (pc, P)) = 1"
    by (fastforce simp:abs_steps'_def compile_expr_output_forms_def epilogue_step_forms_def)
  moreover have "regrec C\<^sub>e r = Some e" using 8
    using compile_expr_correctness by blast
  ultimately show ?case using 8
    apply clarsimp
    apply(rule_tac x=Stop in exI)
    apply(rule_tac x=mds\<^sub>A in exI)
    apply(rule_tac x="mem\<^sub>A(Var v := exp_ev (to_prog_mem mem\<^sub>A) e)" in exI)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(subgoal_tac "(to_prog_mem mem\<^sub>A)(v := exp_ev (to_prog_mem mem\<^sub>A) e) =
                        to_prog_mem (mem\<^sub>A(Var v := exp_ev (to_prog_mem mem\<^sub>A) e))")
     prefer 2
     using to_prog_mem_exp_ev_commute apply force
    apply(rule conjI)
     using WL.assign_eval\<^sub>w apply blast
    apply(drule RL.store_dest[rotated])
     apply force
    apply(rule \<R>.stop)
      apply force
     apply force
    apply(subgoal_tac "mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C")
     prefer 2
     apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def)
    apply(clarsimp simp:\<R>_conds split del:if_split)
    apply(rule conjI)
     apply(clarsimp simp:preserves_modes_mem_pair_def regrec_mem_consistent_def)
     using id_apply preserves_modes_mem_mem\<^sub>A_simp
     subgoal
     proof -
       assume "regs = regs'"
       assume "mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C"
       assume a1: "regrec C\<^sub>e r = Some e"
       assume "\<forall>r e. regrec C\<^sub>e r = Some e \<longrightarrow> regs' r = exp_ev (to_prog_mem mem\<^sub>C) e"
       then show "(\<lambda>va. if va = Var v then exp_ev (to_prog_mem (mem\<^sub>A_of mem\<^sub>C)) e else mem\<^sub>A_of mem\<^sub>C va) = mem\<^sub>A_of (\<lambda>va. if va = Var v then regs' r else mem\<^sub>C va)"
         using a1 by (metis (no_types) id_apply preserves_modes_mem_mem\<^sub>A_simp)
     qed
    apply(rule conjI)
     apply(force simp:regs_noRW_pair_def)
    apply(clarsimp simp:cmds_stop_consistently_def split del:if_split)
    using RL.eval\<^sub>r_halt_iff_oob eq_imp_le
    by (metis (no_types, lifting) "8.hyps"(4,6))
next (* if_expr *)
  case (9 c\<^sub>A e c\<^sub>1 c\<^sub>2 P'' l' nl\<^sub>2 C' C l nl P P\<^sub>e r C\<^sub>1 br nl' ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 P\<^sub>2 l\<^sub>2 C\<^sub>3 pc regs mem\<^sub>C mds\<^sub>C mds\<^sub>A mem\<^sub>A)
  from "9.hyps"(1-14) "9.prems" show ?case
    apply(clarsimp split:prod.splits split del:if_split)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(case_tac "pc < length P\<^sub>e")
     (* Case: In the expr *)
     apply(subgoal_tac "pc' = Suc pc")
      prefer 2
      using compiled_expr_only_increments apply blast
     apply(subgoal_tac "\<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, map fst P\<^sub>e), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C")
      prefer 2
      apply(force intro: \<R>_conds_head)
     apply(subgoal_tac "compile_expr_output_forms (snd (fst (P\<^sub>e ! pc)))")
      prefer 2
      apply(metis (no_types, hide_lams) compile_expr_output_forms)
     apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc, map fst P\<^sub>e), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((Suc pc, map fst P\<^sub>e), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      apply(metis (no_types, hide_lams) RL.eval\<^sub>r_head Suc_leI length_map)
     apply(subgoal_tac "\<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((Suc pc, map fst P\<^sub>e), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      apply(force dest:compiled_expr_eval_preserves_\<R>_conds)
     apply(subgoal_tac "preserves_modes_mem_pair \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc', P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      apply(force simp:\<R>_conds preserves_modes_mem_pair_def)
     apply(subgoal_tac "abs_steps' c\<^sub>A (pc, P) = 0")
      prefer 2
      apply(clarsimp simp:compile_cmd_input_reqs_def split:prod.splits)
      apply(force simp:abs_steps'_def nth_append)
     apply(clarsimp split del:if_split)
     apply(rule_tac x=c\<^sub>A in exI)
     apply(rule_tac x=mds\<^sub>A in exI)
     apply(rule_tac x=mem\<^sub>A in exI)
     apply(rule conjI)
      using WL.neval_0 apply force
     apply(rule \<R>.if_expr)
                   apply force
                  using "9.hyps" apply blast
                 apply force+
         apply(frule_tac pc=pc and mem\<^sub>C=mem\<^sub>C and mds\<^sub>C=mds\<^sub>C in compiled_expr_eval_maintains_regrec_consistency')
              apply(force simp:nth_append)
             apply(force simp:\<R>_conds regs_noRW_pair_def)
            apply(force simp:compile_cmd_input_reqs_def)
           apply force+
         apply(force simp:nth_append)
        apply(frule_tac pc=pc and mem\<^sub>C=mem\<^sub>C and mds\<^sub>C=mds\<^sub>C in compiled_expr_eval_maintains_asmrec_consistency)
           apply(force simp:nth_append)
          apply force+
        apply(force simp:nth_append)
       apply(frule_tac pc=pc' in compiled_expr_maintains_regrec_stability)
         apply(force simp:compile_cmd_input_reqs_def nth_append)
        apply(force simp:compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)
       apply(force simp:nth_append)
      apply(clarsimp split del:if_split simp:\<R>_conds)
      apply(rule conjI)
       apply(force simp:regs_noRW_pair_def)
      apply(clarsimp simp:nth_append cmds_stop_consistently_def split del:if_split)
      apply(metis (no_types, lifting) One_nat_def WL.neval_Suc_simp abs_steps'_def WL.if_eval\<^sub>w)
     using "9.hyps"(15) apply force
    (* Case: At the Jz *)
    apply(subgoal_tac "abs_steps' c\<^sub>A (pc, P) = 1")
     prefer 2
     apply(force simp:abs_steps'_def nth_append compile_expr_output_forms_def epilogue_step_forms_def)
    apply(subgoal_tac "RL.res\<^sub>P br P = Suc (Suc (length P\<^sub>e + length P\<^sub>1))")
     prefer 2
     apply(clarsimp simp:compile_cmd_input_reqs_def split del:if_split)
     using if_jz_br_hit_helper apply force
    apply(clarsimp split del:if_split)
    apply(case_tac "regs r \<noteq> 0")
     (* subcase: to c1 *)
     apply(subgoal_tac "exp_ev\<^sub>B (to_prog_mem mem\<^sub>A) e")
      prefer 2
      apply(clarsimp simp:regrec_mem_consistent_def nth_append)
      apply(subgoal_tac "finite (dom (regrec C))")
       prefer 2
       apply(force simp:compile_cmd_input_reqs_def)
      apply(frule_tac A="{}" in compile_expr_correctness')
        apply force
       apply force
      using exp_ev\<^sub>B_def preserves_modes_memD preserves_modes_mem_\<R>
      apply(metis (no_types, hide_lams) id_apply preserves_modes_mem_mem\<^sub>A_simp)
     apply(rule_tac x=c\<^sub>1 in exI)
     apply(rule_tac x=mds\<^sub>A in exI)
     apply(rule_tac x=mem\<^sub>A in exI)
     apply(rule conjI)
      using WL.if_eval\<^sub>w apply presburger
     apply(frule RL.jz_f_dest[rotated])
       apply force
      apply(force simp:nth_append)
     apply(rule_tac P'=P'' and br=br in \<R>.if_c1)
               using "9.hyps"(2) apply blast
              apply force+
      apply(clarsimp simp:compile_cmd_input_reqs_def)
      apply(metis (no_types, lifting) compiled_cmd_nonempty fst_conv)
     apply clarsimp
     using "9.hyps"(15)
     apply clarsimp
     apply(erule_tac x=mds\<^sub>C' in allE)
     apply(erule_tac x=mem\<^sub>C' in allE)
     apply(erule_tac x=regs' in allE)
     apply(clarsimp simp:compiled_cmd_init_reqs_def)
     apply(erule impE)
      apply(rule conjI)
       apply(metis (no_types, lifting) length_map nth_append_length)
      apply(rule conjI)
       apply(metis (no_types, lifting) length_map nth_append_length)
      apply(rule conjI)
       apply(force dest:conds_\<R> simp:\<R>_conds)
      apply(metis (no_types, lifting) length_map nth_append_length)
     apply(force simp:\<R>_conds preserves_modes_mem_pair_def)
    (* subcase: to c2 *)
    apply(subgoal_tac "\<not> exp_ev\<^sub>B (to_prog_mem mem\<^sub>A) e")
     prefer 2
     apply(clarsimp simp:regrec_mem_consistent_def nth_append)
     apply(subgoal_tac "finite (dom (regrec C))")
      prefer 2
      apply(force simp:compile_cmd_input_reqs_def)
     apply(frule_tac A="{}" in compile_expr_correctness')
       apply force
      apply force
     using exp_ev\<^sub>B_def preserves_modes_memD preserves_modes_mem_\<R>
     apply(metis (no_types, hide_lams) id_apply preserves_modes_mem_mem\<^sub>A_simp)
    apply(rule_tac x=c\<^sub>2 in exI)
    apply(rule_tac x=mds\<^sub>A in exI)
    apply(rule_tac x=mem\<^sub>A in exI)
    apply(rule conjI)
     using WL.if_eval\<^sub>w apply presburger
    apply(frule RL.jz_s_dest[rotated])
      apply force
     apply(force simp:nth_append)
    apply(rule_tac P'=P'' and br=br in \<R>.if_c2)
              using "9.hyps"(2) apply blast
             apply force+
     apply(clarsimp simp:compile_cmd_input_reqs_def)
     apply(metis (no_types, lifting) compiled_cmd_nonempty fst_conv)
    apply clarsimp
    using "9.hyps"(15)
    apply clarsimp
    apply(erule_tac x=mds\<^sub>C' in allE)
    apply(erule_tac x=mem\<^sub>C' in allE)
    apply(erule_tac x=regs' in allE)
    apply(clarsimp simp:compiled_cmd_init_reqs_def)
    apply(erule impE)
     apply(rule conjI)
      apply(metis (no_types, lifting) length_map nth_append_length)
     apply(rule conjI)
      apply(metis (no_types, lifting) length_map nth_append_length)
     apply(rule conjI)
      apply(force dest:conds_\<R> simp:\<R>_conds)
     apply(metis (no_types, lifting) length_map nth_append_length)
    by (force simp:\<R>_conds preserves_modes_mem_pair_def)
next (* if_c1 *)
  case (10 P'' l' nl\<^sub>2 C' C l nl e c\<^sub>1 c\<^sub>2 P P\<^sub>e r C\<^sub>1 br nl' ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 P\<^sub>2 l\<^sub>2 C\<^sub>3 pc c\<^sub>A mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    apply(clarsimp split:prod.splits split del:if_split)
    apply(erule_tac x="pc' - Suc (length P\<^sub>e)" in meta_allE)
    apply(erule_tac x="map fst P\<^sub>1" in meta_allE)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(clarsimp simp:compile_cmd_input_reqs_def split del:if_split)
    apply(subgoal_tac "pc' \<ge> Suc (length P\<^sub>e)")
     prefer 2
     using if_c1_pc_backward_helper apply force
    apply(subgoal_tac "pc' \<le> Suc (length P\<^sub>e + length P\<^sub>1)")
     prefer 2
     using if_c1_pc_forward_helper apply force
    apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>1), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
                       \<langle>((pc' - Suc (length P\<^sub>e), map fst P\<^sub>1), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
     prefer 2
     using if_c1_eval_helper apply force
    apply(clarsimp split del:if_split)
    apply(rule_tac x=c\<^sub>A'' in exI)
    apply(rule_tac x=mds\<^sub>A'' in exI)
    apply(rule_tac x=mem\<^sub>A'' in exI)
    apply(rule conjI)
     apply(subgoal_tac "abs_steps' c\<^sub>A
          (pc, map fst P\<^sub>e @
               (if P\<^sub>e = [] then l else None, Jz br r) #
               map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]) =
          abs_steps' c\<^sub>A (pc - Suc (length P\<^sub>e), map fst P\<^sub>1)")
      prefer 2
      apply(force simp:abs_steps'_def nth_append)
     apply force
    apply(case_tac "pc' < Suc (length P\<^sub>e + length P\<^sub>1)")
     apply(rule_tac P'=P'' and br=br in \<R>.if_c1)
               using "10.hyps" apply blast
              using "10.hyps" apply blast
             apply force+
    apply(clarsimp split del:if_split)
    apply(subgoal_tac "c\<^sub>A'' = Stop")
     prefer 2
     using \<R>_pairs_halt_implies_stop
     apply(metis (no_types, hide_lams) length_map)
    apply(clarsimp split del:if_split)
    apply(rule \<R>.epilogue_step)
        apply force
       apply force
      apply(force simp:nth_append epilogue_step_forms_def split del:if_split)
     apply(clarsimp simp:\<R>_conds split del:if_split)
     apply(frule_tac c\<^sub>A=Stop in conds_\<R>)
     apply(clarsimp simp:lc\<^sub>A_of_def WL.lc_havoc_eq_def \<R>_conds preserves_modes_mem_pair_def WL.mds_havoc_eq_refl split del:if_split)
     apply(rule conjI)
      apply(force simp:\<R>_conds regs_noRW_pair_def)
     apply(clarsimp simp:cmds_stop_consistently_def split del:if_split)
     apply(subgoal_tac "abs_steps' Stop
           (pc', map fst P\<^sub>e @
                (if P\<^sub>e = [] then l else None, Jz br r) #
                map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]) = 0")
      prefer 2
      apply(force simp:abs_steps'_def epilogue_step_forms_def nth_append)
     apply clarsimp
     using WL.neval_0 apply blast
    apply(clarsimp split del:if_split)
    apply(frule reachable_pair_no_modify_pmem)
    apply(frule reachable_pair_stopD)
    apply(clarsimp split del:if_split)
    apply(rename_tac mem\<^sub>A''' pc'' regs'' mds\<^sub>C''' mem\<^sub>C''')
    (* Case 1: pc'' is still in P\<^sub>1. Impossible. *)
    apply(case_tac "pc'' < Suc (length P\<^sub>e + length P\<^sub>1)")
     apply(clarsimp simp:reachable_pair_def lc\<^sub>A_of_def split del:if_split)
     apply(case_tac sig\<^sub>C)
      apply force
     apply(clarsimp simp:RL.lc_havoc_eq_def split del:if_split)
     apply(drule_tac pc="Suc (length P\<^sub>e + length P\<^sub>1)" and l=ex in RL.jmp_dest[rotated])
      apply(force simp:nth_append)
     apply(clarsimp simp:if_jz_ex_helper)
     apply(frule_tac pc="Suc (Suc (Suc (length P\<^sub>e + (length P\<^sub>1 + length P\<^sub>2))))" in loc_reach_sig_haltD)
      apply force
     apply force
    (* Case 2: pc'' is at the end of the program *)
    apply(case_tac "pc'' = length P")
     apply(rule \<R>.stop)
       apply force
      apply force
     apply(frule_tac pc="length P\<^sub>1" in conds_\<R>)
     apply(clarsimp simp:\<R>_conds split del: if_split)
     apply(rule conjI)
      apply(force simp:preserves_modes_mem_pair_def reachable_pair_def lc\<^sub>A_of_def)
     apply(rule conjI)
      apply(force simp:regs_noRW_pair_def)
     apply(rule conjI)
      apply(force simp:cmds_stop_consistently_def RL.eval\<^sub>r_oob_pc_halt)
     apply(force dest:reach\<^sub>r_preserves_lock_consistent_mds simp:reachable_pair_def)
    (* Case 3: pc'' is at the Jmp *)
    apply(case_tac "pc'' = Suc (length P\<^sub>e + length P\<^sub>1)")
     (* For the case that pc'' is at the Jmp, we can establish that this pair is related by
       \<R>.epilogue_step, and the only pairs reachable from it are at pc'' = length P.
       Then, we should be able to show that all other pc'' are unreachable from pc'. *)
    apply(rule \<R>.epilogue_step)
        apply force
       apply force
      apply(force simp:nth_append epilogue_step_forms_def)
     apply(frule_tac pc="length P\<^sub>1" in conds_\<R>)
     apply(clarsimp simp:\<R>_conds split del: if_split)
     apply(rule conjI)
      apply(force simp:preserves_modes_mem_pair_def reachable_pair_def lc\<^sub>A_of_def)
     apply(rule conjI)
      apply(force simp:regs_noRW_pair_def)
     apply(rule conjI)
      apply(clarsimp simp:cmds_stop_consistently_def split del:if_split)
      apply(subgoal_tac "abs_steps' Stop (Suc (length P\<^sub>e + length P\<^sub>1), P) = 0")
       prefer 2
       apply(force simp:abs_steps'_def nth_append epilogue_step_forms_def)
      using WL.neval_0
      apply(metis (no_types, hide_lams))
     apply(force dest:reach\<^sub>r_preserves_lock_consistent_mds simp:reachable_pair_def)
    (* Here we are showing that the only reachable pair from the jmp is at length P,
      in which case the pair is related by \<R>.stop *)
    apply(clarsimp simp:reachable_pair_def lc\<^sub>A_of_def split del:if_split)
    apply(case_tac sig\<^sub>C')
     apply force
    apply(clarsimp simp:RL.lc_havoc_eq_def split del:if_split)
    apply(drule_tac pc="Suc (length P\<^sub>e + length P\<^sub>1)" in RL.jmp_dest[rotated])
     apply(force simp:nth_append)
    apply(clarsimp simp:if_jz_ex_helper)
    apply(frule_tac pc="Suc (Suc (Suc (length P\<^sub>e + (length P\<^sub>1 + length P\<^sub>2))))" in loc_reach_sig_haltD)
     apply force
    apply(clarsimp split del:if_split)
    apply(frule_tac sig\<^sub>A=sig\<^sub>A' in loc_reach_sig_stopD)
    apply(clarsimp split del:if_split)
    apply(rule \<R>.stop)
      apply force
     apply force
    apply(frule_tac pc="length P\<^sub>1" in conds_\<R>)
     apply(clarsimp simp:\<R>_conds split del: if_split)
     apply(rule conjI)
      apply(force simp:preserves_modes_mem_pair_def reachable_pair_def lc\<^sub>A_of_def)
     apply(rule conjI)
      apply(force simp:regs_noRW_pair_def)
     apply(rule conjI)
      apply(force simp:cmds_stop_consistently_def RL.eval\<^sub>r_oob_pc_halt)
     apply(force dest:reach\<^sub>r_preserves_lock_consistent_mds simp:reachable_pair_def)
    (* Case 4: all remaining cases - should be unreachable *)
    apply(clarsimp simp:reachable_pair_def lc\<^sub>A_of_def split del: if_split)
    apply(frule_tac c\<^sub>1=c\<^sub>1 and c\<^sub>2=c\<^sub>2 and pc'=pc' in if_c1_reach_helper)
    by (simp split del:if_split)+
next (* if_c2 *)
  case (11 P'' l' nl\<^sub>2 C' C l nl e c\<^sub>1 c\<^sub>2 P P\<^sub>e r C\<^sub>1 br nl' ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 P\<^sub>2 l\<^sub>2 C\<^sub>3 pc c\<^sub>A mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    (* This is largely adapted from the if_c1 proof *)
    apply(clarsimp split:prod.splits split del:if_split)
    apply(erule_tac x="pc' - Suc (Suc (length P\<^sub>e + length P\<^sub>1))" in meta_allE)
    apply(erule_tac x="map fst P\<^sub>2" in meta_allE)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(clarsimp simp:compile_cmd_input_reqs_def split del:if_split)
    apply(subgoal_tac "Suc (Suc (length P\<^sub>e + length P\<^sub>1)) \<le> pc'")
     prefer 2
     using if_c2_pc_backward_helper apply force
    apply(subgoal_tac "pc' \<le> Suc (Suc (length P\<^sub>e + length P\<^sub>1 + length P\<^sub>2))")
     prefer 2
     using if_c2_pc_forward_helper apply force
    apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - Suc (Suc (length P\<^sub>e + length P\<^sub>1)), map fst P\<^sub>2), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
                       \<langle>((pc' - Suc (Suc (length P\<^sub>e + length P\<^sub>1)), map fst P\<^sub>2), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
     prefer 2
     using if_c2_eval_helper apply force
    apply(clarsimp split del:if_split)
    apply(rule_tac x=c\<^sub>A'' in exI)
    apply(rule_tac x=mds\<^sub>A'' in exI)
    apply(rule_tac x=mem\<^sub>A'' in exI)
    apply(subgoal_tac "P\<^sub>1 \<noteq> []")
     prefer 2
     apply(metis (no_types, hide_lams) fst_conv compiled_cmd_nonempty)
    apply(rule conjI)
     apply(subgoal_tac "abs_steps' c\<^sub>A (pc, P) =
          abs_steps' c\<^sub>A (pc - Suc (Suc (length P\<^sub>e + length P\<^sub>1)), map fst P\<^sub>2)")
      prefer 2
      apply(force simp:abs_steps'_def nth_append)
     apply force
    apply(case_tac "pc' < Suc (Suc (length P\<^sub>e + length P\<^sub>1 + length P\<^sub>2))")
     apply(rule_tac P'=P'' and br=br in \<R>.if_c2)
               using "11.hyps" apply blast
              using "11.hyps" apply blast
             apply force+
    apply(clarsimp split del:if_split)
    apply(subgoal_tac "c\<^sub>A'' = Stop")
     prefer 2
     using \<R>_pairs_halt_implies_stop
     apply(metis (no_types, hide_lams) length_map)
    apply(clarsimp split del:if_split)
    apply(rule \<R>.epilogue_step)
        apply force
       apply force
      apply(force simp:nth_append epilogue_step_forms_def split del:if_split)
     apply(clarsimp simp:\<R>_conds split del:if_split)
     apply(frule_tac c\<^sub>A=Stop in conds_\<R>)
     apply(clarsimp simp:lc\<^sub>A_of_def WL.lc_havoc_eq_def \<R>_conds preserves_modes_mem_pair_def WL.mds_havoc_eq_refl split del:if_split)
     apply(rule conjI)
      apply(force simp:\<R>_conds regs_noRW_pair_def)
     apply(clarsimp simp:cmds_stop_consistently_def split del:if_split)
     apply(subgoal_tac "abs_steps' Stop
           (pc', map fst P\<^sub>e @
                (if P\<^sub>e = [] then l else None, Jz br r) #
                map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]) = 0")
      prefer 2
      apply(force simp:abs_steps'_def epilogue_step_forms_def nth_append)
     apply clarsimp
     using WL.neval_0 apply blast
    apply(clarsimp split del:if_split)
    apply(frule reachable_pair_no_modify_pmem)
    apply(frule reachable_pair_stopD)
    apply(clarsimp split del:if_split)
    apply(rename_tac mem\<^sub>A''' pc'' regs'' mds\<^sub>C''' mem\<^sub>C''')
    (* Case 1: pc'' is still in P\<^sub>2. Impossible. *)
    apply(case_tac "pc'' < Suc (Suc (length P\<^sub>e + length P\<^sub>1 + length P\<^sub>2))")
     apply(clarsimp simp:reachable_pair_def lc\<^sub>A_of_def split del:if_split)
     apply(case_tac sig\<^sub>C)
      apply force
     apply(clarsimp simp:RL.lc_havoc_eq_def split del:if_split)
     apply(drule_tac pc="Suc (Suc (length P\<^sub>e + length P\<^sub>1 + length P\<^sub>2))" in RL.tau_dest[rotated])
      apply(force simp:nth_append)
     apply(clarsimp simp:if_jz_ex_helper)
     apply(frule_tac pc="Suc (Suc (Suc (length P\<^sub>e + length P\<^sub>1 + length P\<^sub>2)))" in loc_reach_sig_haltD)
      apply force
     apply force
    (* Case 2: pc'' is at the end of the program *)
    apply(case_tac "pc'' = length P")
     apply(rule \<R>.stop)
       apply force
      apply force
     apply(frule_tac pc="length P\<^sub>2" in conds_\<R>)
     apply(clarsimp simp:\<R>_conds split del: if_split)
     apply(rule conjI)
      apply(force simp:preserves_modes_mem_pair_def reachable_pair_def lc\<^sub>A_of_def)
     apply(rule conjI)
      apply(force simp:regs_noRW_pair_def)
     apply(rule conjI)
      apply(force simp:cmds_stop_consistently_def RL.eval\<^sub>r_oob_pc_halt)
     apply(force dest:reach\<^sub>r_preserves_lock_consistent_mds simp:reachable_pair_def)
    (* Case 3: pc'' is at the Tau *)
    apply(case_tac "pc'' = Suc (Suc (length P\<^sub>e + length P\<^sub>1 + length P\<^sub>2))")
     (* For the case that pc'' is at the Tau, we can establish that this pair is related by
       \<R>.epilogue_step, and the only pairs reachable from it are at pc'' = length P.
       Then, we should be able to show that all other pc'' are unreachable from pc'. *)
    apply(rule \<R>.epilogue_step)
        apply force
       apply force
      apply(force simp:nth_append epilogue_step_forms_def)
     apply(frule_tac pc="length P\<^sub>2" in conds_\<R>)
     apply(clarsimp simp:\<R>_conds split del: if_split)
     apply(rule conjI)
      apply(force simp:preserves_modes_mem_pair_def reachable_pair_def lc\<^sub>A_of_def)
     apply(rule conjI)
      apply(force simp:regs_noRW_pair_def)
     apply(rule conjI)
      apply(clarsimp simp:cmds_stop_consistently_def split del:if_split)
      apply(subgoal_tac "abs_steps' Stop (Suc (Suc (length P\<^sub>e + length P\<^sub>1 + length P\<^sub>2)), P) = 0")
       prefer 2
       apply(force simp:abs_steps'_def nth_append epilogue_step_forms_def)
      using WL.neval_0
      apply(metis (no_types, hide_lams))
     apply(force dest:reach\<^sub>r_preserves_lock_consistent_mds simp:reachable_pair_def)
    (* Here we are showing that the only reachable pair from the Tau is at length P,
      in which case the pair is related by \<R>.stop *)
    apply(clarsimp simp:reachable_pair_def lc\<^sub>A_of_def split del:if_split)
    apply(case_tac sig\<^sub>C')
     apply force
    apply(clarsimp simp:RL.lc_havoc_eq_def split del:if_split)
    apply(drule_tac pc="Suc (Suc (length P\<^sub>e + length P\<^sub>1 + length P\<^sub>2))" in RL.tau_dest[rotated])
     apply(force simp:nth_append)
    apply(clarsimp simp:if_jz_ex_helper)
    apply(frule_tac pc="Suc (Suc (Suc (length P\<^sub>e + length P\<^sub>1 + length P\<^sub>2)))" in loc_reach_sig_haltD)
     apply force
    apply(clarsimp split del:if_split)
    apply(frule_tac sig\<^sub>A=sig\<^sub>A' in loc_reach_sig_stopD)
    apply(clarsimp split del:if_split)
    apply(rule \<R>.stop)
      apply force
     apply force
    apply(frule_tac pc="length P\<^sub>2" in conds_\<R>)
     apply(clarsimp simp:\<R>_conds split del: if_split)
     apply(rule conjI)
      apply(force simp:preserves_modes_mem_pair_def reachable_pair_def lc\<^sub>A_of_def)
     apply(rule conjI)
      apply(force simp:regs_noRW_pair_def)
     apply(rule conjI)
      apply(force simp:cmds_stop_consistently_def RL.eval\<^sub>r_oob_pc_halt)
     apply(force dest:reach\<^sub>r_preserves_lock_consistent_mds simp:reachable_pair_def)
    (* Case 4: all remaining cases - should be unreachable *)
    apply(clarsimp simp:reachable_pair_def lc\<^sub>A_of_def split del: if_split)
    apply(frule_tac c\<^sub>1=c\<^sub>1 and c\<^sub>2=c\<^sub>2 and pc'=pc' in if_c2_reach_helper)
    by (simp split del:if_split)+
next (* while_expr *)
  case (12 c\<^sub>A pc b c P'' ex nl\<^sub>c C l nl P lp nl' nl'' P\<^sub>e r C\<^sub>e P\<^sub>c l\<^sub>c C\<^sub>c regs mem\<^sub>C mds\<^sub>C mds\<^sub>A mem\<^sub>A)
  (* Much in common with the proof for \<R>.if_expr *)
  from "12.hyps"(1-13) "12.prems" show ?case
    apply(clarsimp split:prod.splits split del:if_split)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(case_tac "pc < length P\<^sub>e")
     (* Case: In the expr *)
     apply(subgoal_tac "pc' = Suc pc")
      prefer 2
      using compiled_expr_only_increments apply blast
     apply(subgoal_tac "\<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, map fst P\<^sub>e), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C")
      prefer 2
      apply(force intro: \<R>_conds_head)
     apply(subgoal_tac "compile_expr_output_forms (snd (fst (P\<^sub>e ! pc)))")
      prefer 2
      apply(metis compile_expr_output_forms)
     apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc, map fst P\<^sub>e), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((Suc pc, map fst P\<^sub>e), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      apply(metis (no_types, hide_lams) RL.eval\<^sub>r_head Suc_leI length_map)
     apply(subgoal_tac "\<R>_conds \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((Suc pc, map fst P\<^sub>e), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      apply(force dest:compiled_expr_eval_preserves_\<R>_conds)
     apply(subgoal_tac "preserves_modes_mem_pair \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc', P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      apply(force simp:\<R>_conds preserves_modes_mem_pair_def)
     apply(clarsimp split del:if_split)
     apply(rule_tac x="Stmt.If b (c ;; While b c) Stop" in exI)
     apply(rule_tac x=mds\<^sub>A in exI)
     apply(rule_tac x=mem\<^sub>A in exI)
     apply(rule conjI)
      apply(case_tac "pc = 0")
       apply clarsimp
       apply(subgoal_tac "abs_steps' c\<^sub>A (0, P) = 1")
        prefer 2
        using abs_steps'_def current_instB_head
        apply force
       apply clarsimp
       using WL.while_eval\<^sub>w apply blast
      apply(subgoal_tac "abs_steps' c\<^sub>A (pc, P) = 0")
       prefer 2
       using abs_steps'_def current_instB_head
       apply force
      using WL.neval_0 apply force
     apply(rule \<R>.while_expr)
                  apply force
                 using "12.hyps" apply blast
                apply force+
         apply(frule_tac pc=pc and mem\<^sub>C=mem\<^sub>C and mds\<^sub>C=mds\<^sub>C in compiled_expr_eval_maintains_regrec_consistency')
              apply(force simp:nth_append)
             apply(force simp:\<R>_conds regs_noRW_pair_def)
            apply force+
         apply(force simp:nth_append)
        apply(frule_tac pc=pc and mem\<^sub>C=mem\<^sub>C and mds\<^sub>C=mds\<^sub>C in compiled_expr_eval_maintains_asmrec_consistency)
           apply(force simp:nth_append)
          apply(force simp:\<R>_conds regs_noRW_pair_def)
         apply force
        apply(force simp:nth_append)
       apply(frule_tac pc=pc' in compiled_expr_maintains_regrec_stability)
         apply(force simp:regrec_stable_def)
        apply(force simp:compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)
       apply(force simp:nth_append)
      apply(case_tac "pc' < length P\<^sub>e \<and> pc > 0")
       using \<R>_conds_append apply force
      apply(clarsimp split del:if_split simp:\<R>_conds)
      apply(rule conjI)
       apply(force simp:preserves_modes_mem_pair_def)
      apply(rule conjI)
       apply(force simp:regs_noRW_pair_def)
      apply(clarsimp simp:nth_append cmds_stop_consistently_def split del:if_split)
      apply(metis (no_types, lifting) One_nat_def WL.neval.simps abs_steps'_def WL.if_eval\<^sub>w)
     using "12.hyps"(14) apply fastforce
    (* Case: At the Jz *)
    apply(case_tac "P\<^sub>e = []")
     apply(force dest:compile_expr_emptyD)
    apply clarsimp
    apply(subgoal_tac "abs_steps' c\<^sub>A (pc, P) = 1")
     prefer 2
     apply(force simp:abs_steps'_def nth_append compile_expr_output_forms_def epilogue_step_forms_def)
    apply(subgoal_tac "RL.res\<^sub>P ex P = Suc (Suc (length P\<^sub>e + length P\<^sub>c))")
     prefer 2
     apply(clarsimp simp:RL.fresh_label_def compile_cmd_input_reqs_def split del:if_split)
     using while_ex_helper apply force
    apply(clarsimp split del:if_split)
    apply(case_tac "regs r \<noteq> 0")
     (* subcase: to inner program *)
     apply(subgoal_tac "exp_ev\<^sub>B (to_prog_mem mem\<^sub>A) b")
      prefer 2
      apply(clarsimp simp:regrec_mem_consistent_def nth_append)
      apply(subgoal_tac "regrec C\<^sub>e r = Some b")
       prefer 2
       apply(rule_tac C="C\<lparr>regrec := Map.empty\<rparr>" and A="{}" in compile_expr_correctness')
         apply force
        apply force
       apply force
      using exp_ev\<^sub>B_def preserves_modes_memD preserves_modes_mem_\<R>
      apply(metis (no_types) id_apply preserves_modes_mem_mem\<^sub>A_simp)
     apply(rule_tac x="c ;; While b c" in exI)
     apply(rule_tac x=mds\<^sub>A in exI)
     apply(rule_tac x=mem\<^sub>A in exI)
     apply(rule conjI)
      using WL.if_eval\<^sub>w apply presburger
     apply(frule RL.jz_f_dest[rotated])
       apply force
      apply(force simp:nth_append)
     apply(rule_tac P'=P'' and c\<^sub>I=c in \<R>.while_inner)
                  apply force
                 apply(force simp:compile_cmd_input_reqs_def)
                using "12.hyps"(2) apply blast
               apply force+
        apply(clarsimp simp:compile_cmd_input_reqs_def)
        apply(metis (no_types, lifting) compiled_cmd_nonempty fst_conv)
       using "12.hyps"(2)
       apply(fastforce dest:compiled_cmd_eval_maintains_asmrec_consistency')
      apply clarsimp
      using "12.hyps"(14)
      apply clarsimp
      apply(erule_tac x=mds\<^sub>C' in allE)
      apply(erule_tac x=mem\<^sub>C' in allE)
      apply(erule_tac x=regs' in allE)
      apply(clarsimp simp:compiled_cmd_init_reqs_def)
      apply(erule impE)
       apply(rule conjI)
        apply(metis length_map nth_append_length)
       apply(rule conjI)
        apply(metis length_map nth_append_length)
       apply(rule conjI)
        apply(force dest:conds_\<R> simp:\<R>_conds)
       apply(metis length_map nth_append_length)
      apply(force simp:\<R>_conds preserves_modes_mem_pair_def)
     using "12.hyps"(14) apply fastforce
    (* subcase: to exit *)
    apply(subgoal_tac "\<not> exp_ev\<^sub>B (to_prog_mem mem\<^sub>A) b")
     prefer 2
     apply(clarsimp simp:regrec_mem_consistent_def nth_append)
     apply(subgoal_tac "regrec C\<^sub>e r = Some b")
      prefer 2
      apply(rule_tac C="C\<lparr>regrec := Map.empty\<rparr>" and A="{}" in compile_expr_correctness')
        apply force
       apply force
      apply force
     using exp_ev\<^sub>B_def preserves_modes_memD preserves_modes_mem_\<R>
     apply(metis (no_types) id_apply preserves_modes_mem_mem\<^sub>A_simp)
    apply(rule_tac x=Stop in exI)
    apply(rule_tac x=mds\<^sub>A in exI)
    apply(rule_tac x=mem\<^sub>A in exI)
    apply(rule conjI)
     using WL.if_eval\<^sub>w apply presburger
    apply(drule RL.jz_s_dest[rotated])
      apply force
     apply(force simp:nth_append)
    apply(rule \<R>.stop)
      apply force
     apply force
    apply(clarsimp simp:\<R>_conds split del:if_split)
    apply(rule conjI)
     apply(force simp:preserves_modes_mem_pair_def)
    apply(rule conjI)
     apply(force simp:regs_noRW_pair_def)
    apply(clarsimp simp:nth_append cmds_stop_consistently_def split del:if_split)
    using compile_cmd_exit_label_oob RL.eval\<^sub>r_bounds RL.eval\<^sub>r_no_modify RL.oob_labels_res\<^sub>P RL.res\<^sub>P_oob
    by (metis (no_types, lifting) "12.hyps"(2,4) compile_cmd_input_reqs_def)
next (* while_inner *)
  case (13 c\<^sub>A c\<^sub>I b c P'' ex nl\<^sub>c C l nl P lp nl' nl'' P\<^sub>e r C\<^sub>e P\<^sub>c l\<^sub>c C\<^sub>c pc mds\<^sub>C mds\<^sub>A mem\<^sub>A regs mem\<^sub>C)
  from "13.hyps"(1-15) "13.prems" show ?case
    apply(clarsimp split:prod.splits split del:if_split)
    apply(erule_tac x="pc' - Suc (length P\<^sub>e)" in meta_allE)
    apply(erule_tac x="map fst P\<^sub>c" in meta_allE)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(clarsimp split del:if_split)
    apply(subgoal_tac "pc' \<le> Suc (length P\<^sub>e + length P\<^sub>c)")
     prefer 2 
     apply(clarsimp simp:RL.fresh_label_def compile_cmd_input_reqs_def)
     using while_pc_forward_helper apply force
    apply(subgoal_tac "pc' \<ge> Suc (length P\<^sub>e)")
     prefer 2
     apply(clarsimp simp:RL.fresh_label_def compile_cmd_input_reqs_def)
     using while_pc_backward_helper apply force
    apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>c), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
                       \<langle>((pc' - Suc (length P\<^sub>e), map fst P\<^sub>c), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
     prefer 2
     apply(clarsimp simp:RL.fresh_label_def compile_cmd_input_reqs_def)
     using while_pc_eval_helper apply force
    apply(clarsimp split del:if_split)
    apply(subgoal_tac "abs_steps' c\<^sub>A
         (pc, map fst P\<^sub>e @
              (None, Jz ex r) #
              map fst P\<^sub>c @ [(l\<^sub>c, Jmp lp)]) =
         abs_steps' c\<^sub>I (pc - Suc (length P\<^sub>e), map fst P\<^sub>c)")
     prefer 2
     apply(force simp:abs_steps'_def nth_append)
    apply(rule_tac x="cmd_seq c\<^sub>A'' (While b c)" in exI)
    apply(rule_tac x=mds\<^sub>A'' in exI)
    apply(rule_tac x=mem\<^sub>A'' in exI)
    apply(clarsimp split del:if_split)
    apply(frule_tac c\<^sub>2="While b c" in neval_cmd_seq)
    apply(rule conjI)
     apply(force simp:cmd_seq_def)
    apply(clarsimp simp:cmd_seq_def split del:if_split)
    apply(case_tac "c\<^sub>A'' \<noteq> Stop")
     apply(case_tac "pc' < Suc (length P\<^sub>e + length P\<^sub>c)")
      apply(rule_tac c\<^sub>I=c\<^sub>A'' and P'=P'' in \<R>.while_inner)
                   apply force
                  apply force
                 using "13.hyps" apply blast
                apply force
               apply force
              apply force
             apply force
            apply force
           apply force
          apply force
         apply force
        using "13.hyps"(3)
        apply(fastforce dest:compiled_cmd_eval_maintains_asmrec_consistency')
       apply force
      apply force
     apply(subgoal_tac "c\<^sub>A'' = Stop")
      prefer 2
      using \<R>_pairs_halt_implies_stop
      apply(metis add_diff_cancel_left' diff_Suc_Suc le_neq_implies_less length_map)
     apply force
    apply(clarsimp split del:if_split)
    apply(rule \<R>.while_loop)
                apply force
               using "13.hyps" apply blast
              apply force
             apply force
            apply force
           apply force
          apply force
         apply force
        apply force
       apply force
      using "13.hyps"(3)
      apply(fastforce dest:compiled_cmd_eval_maintains_asmrec_consistency')
     apply force
    by force
next (* while_loop *)
  case (14 c\<^sub>A b c P'' ex nl\<^sub>c C l nl P lp nl' nl'' P\<^sub>e r C\<^sub>e P\<^sub>c l\<^sub>c C\<^sub>c pc mds\<^sub>C mds\<^sub>A mem\<^sub>A regs mem\<^sub>C)
  from "14.hyps"(1-13) "14.prems" show ?case
    apply(clarsimp split:prod.splits split del:if_split)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(case_tac "P\<^sub>e = []")
     apply(fastforce dest:compile_expr_emptyD)
    apply(frule_tac c\<^sub>A=Stop in \<R>_pairs_stop_implies_halt_or_epilogue, simp+)
    (* Consider the Jmp case alone first *)
    apply(case_tac "pc \<ge> Suc (length P\<^sub>e + length P\<^sub>c)")
     apply(subgoal_tac "RL.res\<^sub>P lp P = 0")
      prefer 2
      apply(subgoal_tac "RL.res\<^sub>P lp (map fst P\<^sub>e) = 0")
       prefer 2
       using compile_expr_input_label_res\<^sub>P apply blast
      apply(clarsimp split del:if_split)
      using RL.res\<^sub>P_ib_append apply blast
     apply(frule RL.jmp_dest[rotated])
      apply(force simp:nth_append)
     apply(clarsimp split del:if_split)
     apply(rule_tac x="(While b c)" in exI)
     apply(rule_tac x=mds\<^sub>A in exI)
     apply(rule_tac x=mem\<^sub>A in exI)
     apply(rule conjI)
      apply(force simp:abs_steps'_def compile_expr_output_forms_def epilogue_step_forms_def nth_append WL.neval_0)
     apply(frule compile_expr_first_comprec_input)
      apply force
     apply(rule \<R>.while_expr)
                  apply force
                 using "14.hyps" apply blast
                apply force
               apply force
              apply force
             apply force
            apply force
           apply force
          apply force
         apply(force simp:nth_append regrec_mem_consistent_def)
        using "14.hyps"(2)
        apply(fastforce dest:compiled_cmd_eval_maintains_asmrec_consistency')
       apply(force simp:compile_cmd_input_reqs_def nth_append no_unstable_exprs_def regrec_stable_def)
      apply(clarsimp simp:\<R>_conds)
      apply(rule conjI)
       apply(frule conds_\<R>)
       apply(force simp:\<R>_conds preserves_modes_mem_pair_def)
      apply(rule conjI)
       apply(frule conds_\<R>)
       apply(force simp:\<R>_conds regs_noRW_pair_def)
      apply(rule conjI)
       apply(clarsimp simp:cmds_stop_consistently_def)
       apply(case_tac "(abs_steps' (While b c) (0, P)) = 0")
        apply clarsimp
        using WL.neval_0 apply meson
       apply clarsimp
       using WL.while_eval\<^sub>w apply blast
      apply(force dest:conds_\<R> simp:\<R>_conds)
     using "14.hyps"(14) apply blast
    (* NB: almost everything below this is identical to the while_inner proof.
      Consider merging this case of \<R> with that one? *)
    apply(erule_tac x="pc' - Suc (length P\<^sub>e)" in meta_allE)
    apply(erule_tac x="map fst P\<^sub>c" in meta_allE)
    apply(frule RL.eval\<^sub>r_no_modify)
    apply(clarsimp split del:if_split)
    apply(subgoal_tac "pc' \<le> Suc (length P\<^sub>e + length P\<^sub>c)")
     prefer 2 
     apply(clarsimp simp:RL.fresh_label_def compile_cmd_input_reqs_def)
     using while_pc_forward_helper apply force
    apply(subgoal_tac "pc' \<ge> Suc (length P\<^sub>e)")
     prefer 2
     apply(clarsimp simp:RL.fresh_label_def compile_cmd_input_reqs_def)
     using while_pc_backward_helper apply force
    apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P\<^sub>c), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
                       \<langle>((pc' - Suc (length P\<^sub>e), map fst P\<^sub>c), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
     prefer 2
     apply(clarsimp simp:RL.fresh_label_def compile_cmd_input_reqs_def)
     using while_pc_eval_helper apply force
    apply(clarsimp split del:if_split)
    apply(erule disjE)
     apply force
    apply(subgoal_tac "abs_steps' Stop (pc - Suc (length P\<^sub>e), map fst P\<^sub>c) = 0")
     prefer 2
     apply(force simp:abs_steps'_def)
    apply(subgoal_tac "abs_steps' c\<^sub>A (pc, P) = 0")
     prefer 2
     apply(force simp:abs_steps'_def nth_append epilogue_step_not_expr_output epilogue_step_forms_def)
    apply(rule_tac x="While b c" in exI)
    apply(rule_tac x=mds\<^sub>A'' in exI)
    apply(rule_tac x=mem\<^sub>A'' in exI)
    apply clarsimp
    apply(erule WL.neval_ZeroE)
    apply clarsimp
    apply(rule conjI)
     using WL.neval_0 apply blast
    apply(rule \<R>.while_loop)
                apply force
               using "14.hyps" apply blast
              apply force
             apply force
            apply force
           apply force
          apply force
         apply force
        apply force
       apply force
      using "14.hyps"(2)
      apply(fastforce dest:compiled_cmd_eval_maintains_asmrec_consistency')
     apply force
    using "14.hyps"(14) by blast
next (* epilogue_step *)
  case (15 c\<^sub>A pc P mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    apply clarsimp
    (* Derive it almost trivially from the reach-closedness clause *)
    apply(erule_tac x="[\<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C]" in allE)
    apply(erule_tac x=c\<^sub>A in allE)
    apply(erule_tac x=mds\<^sub>A in allE)
    apply(erule_tac x=mem\<^sub>A in allE)
    apply(erule_tac x=pc' in allE)
    apply(erule_tac x=P' in allE)
    apply(erule_tac x=regs' in allE)
    apply(erule_tac x=mds\<^sub>C' in allE)
    apply(erule_tac x=mem\<^sub>C' in allE)
    apply clarsimp
    apply(frule RL.eval\<^sub>r_no_modify)
    apply clarsimp
    apply(erule impE)
     apply(clarsimp simp:reachable_pair_def RL.lc_havoc_eq_def RL.mds_havoc_eq_refl)
     apply(rule conjI)
      apply(rule_tac x=mem\<^sub>C' in exI)
      apply(force simp:RL.mds_havoc_eq_refl)
     apply(rule_tac x="[]" in exI)
     apply(clarsimp simp:abs_steps'_def WL.lc_havoc_eq_refl lc\<^sub>A_of_def \<R>_conds preserves_modes_mem_pair_def)
     apply(clarsimp simp:epilogue_step_forms_def)
     apply(erule disjE)
      apply clarsimp
      apply(frule_tac RL.jmp_dest[rotated])
       apply force
      apply force
     apply(frule_tac RL.tau_dest[rotated])
      apply force
     apply force
    apply(clarsimp simp:abs_steps'_def)
    using WL.neval_0 by blast
next (* lock_acq *)
  case (16 c\<^sub>A k pc P mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    apply clarsimp
    apply(subgoal_tac "(abs_steps' (Stmt.LockAcq k) (pc, P)) = 1")
     prefer 2
     apply(force simp:abs_steps'_def compile_expr_output_forms_def epilogue_step_forms_def)
    apply clarsimp
    apply(erule RL.lock_acq_elim)
      apply force
     apply(force intro:WL.eval\<^sub>w.lock_spin simp:\<R>_conds preserves_modes_mem_pair_def mem\<^sub>A_of_def)
    apply(rule_tac x=Stop in exI)
    apply(rule_tac x="lock_acq_upd lock_interp k mds\<^sub>A" in exI)
    apply(rule_tac x="mem\<^sub>A(Lock k := lock_val_True)" in exI)
    apply(rule conjI)
     apply(force intro:WL.eval\<^sub>w.lock_acq simp:\<R>_conds preserves_modes_mem_pair_def mem\<^sub>A_of_def)
    apply(rule \<R>.stop)
      apply force
     apply force
    apply(subgoal_tac "mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C")
     prefer 2
     apply(force simp:\<R>_conds preserves_modes_mem_pair_def)
    apply(clarsimp simp:\<R>_conds)
    apply(rule conjI)
     apply(force simp:preserves_modes_mem_pair_def mem\<^sub>A_of_def mds\<^sub>A_of_def)
    apply(rule conjI)
     apply(force simp:regs_noRW_pair_def)
    apply(rule conjI)
     apply(clarsimp simp:cmds_stop_consistently_def)
     apply(meson RL.eval\<^sub>r_halt_iff_oob eq_imp_le)
    using RL.eval\<^sub>r_preserves_lock_consistent_mds RL.lone_lock_per_var lock_interp_no_overlap by blast
next (* lock_rel *)
  case (17 c\<^sub>A k pc P mds\<^sub>A mem\<^sub>A regs mds\<^sub>C mem\<^sub>C)
  thus ?case
    apply clarsimp
    apply(subgoal_tac "(abs_steps' (Stmt.LockRel k) (pc, P)) = 1")
     prefer 2
     apply(force simp:abs_steps'_def compile_expr_output_forms_def epilogue_step_forms_def)
    apply clarsimp
    apply(erule RL.lock_rel_elim)
      apply force
     (* Alternatively, can we implement a static check that asserts lock_held_mds_correct
        seeing as mds is thread-local anyway? *)
     apply(force intro:WL.eval\<^sub>w.lock_invalid simp:\<R>_conds preserves_modes_mem_pair_def mem\<^sub>A_of_def mds\<^sub>A_of_def)
    apply(rule_tac x=Stop in exI)
    apply(rule_tac x="lock_rel_upd lock_interp k mds\<^sub>A" in exI)
    apply(rule_tac x="mem\<^sub>A(Lock k := lock_val_False)" in exI)
    apply(rule conjI)
     apply(force intro:WL.eval\<^sub>w.lock_rel simp:\<R>_conds preserves_modes_mem_pair_def mem\<^sub>A_of_def mds\<^sub>A_of_def)
    apply(rule \<R>.stop)
      apply force
     apply force
    apply(subgoal_tac "mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C")
     prefer 2
     apply(force simp:\<R>_conds preserves_modes_mem_pair_def)
    apply(clarsimp simp:\<R>_conds)
    apply(rule conjI)
     apply(force simp:preserves_modes_mem_pair_def mem\<^sub>A_of_def mds\<^sub>A_of_def)
    apply(rule conjI)
     apply(force simp:regs_noRW_pair_def)
    apply(rule conjI)
     apply(clarsimp simp:cmds_stop_consistently_def)
     apply(meson RL.eval\<^sub>r_halt_iff_oob eq_imp_le)
    using RL.eval\<^sub>r_preserves_lock_consistent_mds RL.lone_lock_per_var lock_interp_no_overlap by blast
qed

subsection {* That the refinement relation is closed over pairwise reachability *}

(* Deriving \<R>'s closedness over pairwise reachability is essential for the Seq case
  of compiler correctness, where we use it to obtain that we're still in \<R>
  at the end of the execution of the first program, knowing only that we're in \<R> initially *)

(* insight that the sig\<^sub>A_of a single-entry sig\<^sub>C can't be any longer than that sig\<^sub>C
   because abs_steps can only ever be 0 or 1 *)
lemma is_sig\<^sub>A_of_smaller_than_sig\<^sub>C:
  "is_sig\<^sub>A_of sig\<^sub>A \<langle>c\<^sub>A', mds\<^sub>A_of mds\<^sub>f, mem\<^sub>A_of mem\<^sub>f\<rangle>\<^sub>A [\<langle>((pc, P), regs''), mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C] \<Longrightarrow>
   eval_abv\<^sub>C \<langle>((pc, P), regs''), mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C \<langle>((pc\<^sub>f, P\<^sub>f), regs\<^sub>f), mds\<^sub>f, mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   WL.loc_reach_sig \<langle>c\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A \<langle>c\<^sub>A', mds\<^sub>A_of mds\<^sub>f, mem\<^sub>A_of mem\<^sub>f\<rangle>\<^sub>A sig\<^sub>A \<Longrightarrow>
   length sig\<^sub>A \<ge> 2 \<Longrightarrow> False"
  apply(induct sig\<^sub>A "\<langle>c\<^sub>A', mds\<^sub>A_of mds\<^sub>f, mem\<^sub>A_of mem\<^sub>f\<rangle>\<^sub>A" "[\<langle>((pc, P), regs''), mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C]" rule:is_sig\<^sub>A_of.induct)
   apply clarsimp
   apply(clarsimp split:if_splits)
  apply clarsimp
  done

lemma is_sig\<^sub>A_of_intermediate_next:
  "is_sig\<^sub>A_of sig\<^sub>A \<langle>c\<^sub>A', mds\<^sub>A_of mds\<^sub>f, mem\<^sub>A_of mem\<^sub>f\<rangle>\<^sub>A (\<langle>((pc, P), regs''), mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C # sig\<^sub>C) \<Longrightarrow>
   WL.loc_reach_sig \<langle>c\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A \<langle>c\<^sub>A', mds\<^sub>A_of mds\<^sub>f, mem\<^sub>A_of mem\<^sub>f\<rangle>\<^sub>A sig\<^sub>A \<Longrightarrow>
   sig\<^sub>C \<noteq> [] \<Longrightarrow>
   abs_steps' c\<^sub>A (pc, P) = 0 \<Longrightarrow>
   is_sig\<^sub>A_of sig\<^sub>A \<langle>c\<^sub>A', mds\<^sub>A_of mds\<^sub>f, mem\<^sub>A_of mem\<^sub>f\<rangle>\<^sub>A sig\<^sub>C"
  apply(cases "(sig\<^sub>A, \<langle>c\<^sub>A', mds\<^sub>A_of mds\<^sub>f, mem\<^sub>A_of mem\<^sub>f\<rangle>\<^sub>A, (\<langle>((pc, P), regs''), mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C # sig\<^sub>C))" rule:is_sig\<^sub>A_of.cases)
    apply simp_all
  apply(clarsimp simp:WL.lc_havoc_eq_def)
  done

lemma \<R>_closed_over_pairwise_reachability':
  "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R> \<Longrightarrow>
   if sig\<^sub>C = [] then (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) = dst else
   reachable_pair (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) dst sig\<^sub>C \<Longrightarrow>
   dst \<in> \<R>"
  apply(induct sig\<^sub>C arbitrary: c\<^sub>A mds\<^sub>A mem\<^sub>A pc P regs mds\<^sub>C mem\<^sub>C)
   apply clarsimp
  apply clarsimp
  apply(rename_tac pc' P' regs' mds\<^sub>C' mem\<^sub>C' sig\<^sub>C c\<^sub>A mds\<^sub>A mem\<^sub>A pc P regs mds\<^sub>C mem\<^sub>C)
  apply(clarsimp simp:reachable_pair_def RL.lc_havoc_eq_def lc\<^sub>A_of_def)
  apply(rename_tac pc P regs mds\<^sub>C mem\<^sub>C'' sig\<^sub>C c\<^sub>A mds\<^sub>A mem\<^sub>A mem\<^sub>C sig\<^sub>A c\<^sub>A' pc\<^sub>f P\<^sub>f regs\<^sub>f mds\<^sub>f mem\<^sub>f pc' P' regs' mds\<^sub>C' mem\<^sub>C')
  apply(frule conds_\<R>)
  apply(clarsimp simp: \<R>_conds preserves_modes_mem_pair_def WL.lc_havoc_eq_def)
  apply(frule closed_others_\<R>', simp)
  apply(frule_tac mem\<^sub>C=mem\<^sub>C'' in \<R>_preservation)
   apply force
  apply clarsimp
  apply(case_tac "sig\<^sub>C=[]")
   apply(clarsimp simp:RL.lc_havoc_eq_def)
   apply(subgoal_tac "\<R>_conds \<langle>c\<^sub>A'', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A \<langle>((pc\<^sub>f, P\<^sub>f), regs\<^sub>f), mds\<^sub>f, mem\<^sub>C'\<rangle>\<^sub>C")
    prefer 2
    using conds_\<R> apply blast
   apply(clarsimp simp: \<R>_conds preserves_modes_mem_pair_def WL.lc_havoc_eq_def)
   apply(subgoal_tac "c\<^sub>A' = c\<^sub>A''")
    prefer 2
    apply(subgoal_tac "\<not> 2 \<le> length sig\<^sub>A")
     prefer 2
     apply(meson is_sig\<^sub>A_of_smaller_than_sig\<^sub>C)
    apply(subgoal_tac "length sig\<^sub>A = 0 \<or> length sig\<^sub>A = 1")
     prefer 2
     apply linarith
    apply clarsimp
    apply(erule disjE)
     apply(clarsimp simp:WL.lc_havoc_eq_def)
     apply(erule WL.neval_ZeroE)
     apply force
    apply clarsimp
    apply(case_tac sig\<^sub>A)
     apply force
    apply(clarsimp simp:WL.lc_havoc_eq_def split:if_splits)
    apply(rename_tac pc P regs mds\<^sub>C mem\<^sub>C'' mem\<^sub>C c\<^sub>A' pc\<^sub>f P\<^sub>f regs\<^sub>f mds\<^sub>f mem\<^sub>f mem\<^sub>C' c\<^sub>A'' c\<^sub>A ba bb)
    apply(subgoal_tac "abs_steps' c\<^sub>A (pc, P) = 1")
     prefer 2
     apply(force simp:abs_steps'_def split:if_splits)
    apply(clarsimp simp:lc\<^sub>A_of_def)
    using WL.eval\<^sub>w_det
    apply blast
   apply clarsimp
   using closed_others_\<R>' apply blast
  apply clarsimp
  apply(erule_tac x=c\<^sub>A'' in meta_allE)
  apply(erule_tac x=mds\<^sub>A' in meta_allE)
  apply(erule_tac x=mem\<^sub>A' in meta_allE)
  apply(erule_tac x=pc' in meta_allE)
  apply(erule_tac x=P' in meta_allE)
  apply(erule_tac x=regs' in meta_allE)
  apply(erule_tac x=mds\<^sub>C' in meta_allE)
  apply(erule_tac x=mem\<^sub>C' in meta_allE)
  apply clarsimp
  apply(subgoal_tac "reachable_pair (\<langle>c\<^sub>A'', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>((pc', P'), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C)
         (\<langle>c\<^sub>A', mds\<^sub>A_of mds\<^sub>f, mem\<^sub>A_of mem\<^sub>f\<rangle>\<^sub>A, \<langle>((pc\<^sub>f, P\<^sub>f), regs\<^sub>f), mds\<^sub>f, mem\<^sub>f\<rangle>\<^sub>C) sig\<^sub>C")
  apply clarsimp
  apply(thin_tac "(reachable_pair (\<langle>c\<^sub>A'', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>((pc', P'), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C)
         (\<langle>c\<^sub>A', mds\<^sub>A_of mds\<^sub>f, mem\<^sub>A_of mem\<^sub>f\<rangle>\<^sub>A, \<langle>((pc\<^sub>f, P\<^sub>f), regs\<^sub>f), mds\<^sub>f, mem\<^sub>f\<rangle>\<^sub>C) sig\<^sub>C \<Longrightarrow>
        (\<langle>c\<^sub>A', mds\<^sub>A_of mds\<^sub>f, mem\<^sub>A_of mem\<^sub>f\<rangle>\<^sub>A, \<langle>((pc\<^sub>f, P\<^sub>f), regs\<^sub>f), mds\<^sub>f, mem\<^sub>f\<rangle>\<^sub>C) \<in> \<R>)")
  apply(clarsimp simp:reachable_pair_def)
  apply(case_tac "abs_steps' c\<^sub>A (pc, P) = 0")
   apply clarsimp
   apply(erule_tac WL.neval_ZeroE)
   apply clarsimp
   apply(rule_tac x=sig\<^sub>A in exI)
   apply(rule conjI)
    (* We chop one off sig\<^sub>C and leave sig\<^sub>A because it's an intermediate step *)
    using is_sig\<^sub>A_of_intermediate_next
    apply blast
   apply(rule conjI)
    using WL.sig_head_mem_diff mds_havoc_eq\<^sub>A_of apply blast
   apply(force simp:lc\<^sub>A_of_def)
  apply clarsimp
  apply(case_tac sig\<^sub>A)
   apply(force simp: WL.lc_havoc_eq_def)
  apply(rename_tac sig\<^sub>A')
  apply(rule_tac x=sig\<^sub>A' in exI)
  apply(clarsimp simp:WL.lc_havoc_eq_def lc\<^sub>A_of_def)
  using WL.eval\<^sub>w_det by blast

(* Originally tried this by induction on \<R>, but had much better success
   with this approach making use of \<R>_preservation to induct over sig\<^sub>C *)
lemma \<R>_closed_over_pairwise_reachability:
  "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R> \<Longrightarrow>
   closed_over_pairwise_reachability \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<R>"
  apply clarsimp
  using \<R>_closed_over_pairwise_reachability'
  by meson

subsection {* Proof that the While-to-RISC compiler only outputs refinements of its input *}

(* Note that most cases of \<R> are inductive on guards reflecting requirements imposed
  on the input program to the compiler, and the initial state for output programs.
  For cases of \<R> inductive on evaluation, closedness under pairwise reachability is *derived*
  (see lemma \<R>_closed_over_pairwise_reachability)
  from its closedness over a single step (see lemma \<R>_preservation)
  and not baked into the inductiveness of \<R>.
  Thus for these cases, the bulk of the proof work will fall under \<R>_preservation rather than
  this lemma here, which simply "releases the ball at the top of the ramp", so to speak.
  Note the only remaining such case is \<R>.epilogue_step. *)

lemma compile_cmd_input_reqs_seq1:
  "compile_cmd_input_reqs C l nl (c\<^sub>1 ;; c\<^sub>2) \<Longrightarrow> compile_cmd_input_reqs C l nl c\<^sub>1"
  by (clarsimp simp:compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)

lemma compiled_cmd_maintains_regrec_stability'':
  "compile_cmd C l nl c = (P, l', nl', C', False) \<Longrightarrow>
   compile_cmd_input_reqs C l nl c \<Longrightarrow>
   if pc < length P then regrec_stable (map snd P ! pc)
                     else regrec_stable C'"
  using compiled_cmd_maintains_regrec_stability
  by (metis (no_types, lifting) length_map zip_map_fst_snd)

lemma compile_cmd_input_reqs_seq2:
  "compile_cmd_input_reqs C l nl (c\<^sub>1 ;; c\<^sub>2) \<Longrightarrow>
   (compile_cmd C l nl c\<^sub>1) = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>1, False) \<Longrightarrow>
   compile_cmd_input_reqs C\<^sub>1 l\<^sub>1 nl\<^sub>1 c\<^sub>2"
  apply(clarsimp simp:compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)
  apply(subgoal_tac "\<forall>x. l\<^sub>1 = Some x \<longrightarrow> x < nl\<^sub>1")
   prefer 2
   using compile_cmd_maintains_labelrec_freshness apply force
  apply(frule compile_cmd_consistent_banned_unstable_exprs)
    apply force
   apply force
  apply(clarsimp simp:compile_cmd_input_reqs_def no_unstable_exprs_def)
  apply(rule conjI)
   apply(force dest:compiled_cmd_maintains_regrec_stability''[where pc="length P\<^sub>1"]
               simp:compile_cmd_input_reqs_def no_unstable_exprs_def)
  by (force dest: compile_cmd_preserves_finite_regrec)

lemma compile_cmd_correctness_\<R>:
  "(zip P Cs, l', nl', C', False) = (compile_cmd C l nl c) \<Longrightarrow>
    compile_cmd_input_reqs C l nl c \<Longrightarrow>
    length P = length Cs \<Longrightarrow>
    compiled_cmd_init_reqs C regs mds\<^sub>C mem\<^sub>C \<Longrightarrow>
    (\<langle>c, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A, \<langle>((0, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> \<R>"
unfolding compiled_cmd_init_reqs_def
proof(induct c arbitrary: P Cs l' nl' C' C l nl regs mds\<^sub>C mem\<^sub>C)
  case (LockAcq k) thus ?case
    apply(clarsimp simp:zip_eq_conv)
    apply(rule \<R>.lock_acq, simp+)
    apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def regs_noRW_pair_def)
    apply(clarsimp simp:cmds_stop_consistently_def abs_steps'_def compile_expr_output_forms_def epilogue_step_forms_def)
    apply(clarsimp simp:id_def)
    by (metis WL.eval\<^sub>w.lock_acq WL.eval\<^sub>w.lock_spin)
next
  case (LockRel k) thus ?case
    apply(clarsimp simp:zip_eq_conv)
    apply(rule \<R>.lock_rel, simp+)
    apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def regs_noRW_pair_def)
    apply(clarsimp simp:cmds_stop_consistently_def abs_steps'_def compile_expr_output_forms_def epilogue_step_forms_def)
    apply(clarsimp simp:id_def)
    by (metis WL.eval\<^sub>w.lock_rel WL.eval\<^sub>w.lock_invalid)
next
  case (Assign v e) thus ?case
    apply(clarsimp simp:Let_def zip_eq_conv compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)
    apply(rename_tac P\<^sub>e r C\<^sub>e fail\<^sub>e)
    apply(rule conjI)
     (* Assign case 1: empty compile_expr output due to V-cached *)
     apply clarsimp
     apply(rule_tac P'="zip P Cs" and l'=l' and nl\<^sub>2=nl' and nl=nl and r=r and C'=C' and C=C and l=l
           in \<R>.assign_store)
                apply force
               using Assign.prems(1) apply blast
              using Assign.prems(2) apply blast
             apply force
            apply force
           apply force
          using compile_expr_empty_maintains_comprec apply metis
         apply(force dest:compiled_expr_asmrec_constancy_end)
        apply force
       apply force
      apply force
     apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def regs_noRW_pair_def)
     apply(clarsimp simp:cmds_stop_consistently_def abs_steps'_def compile_expr_output_forms_def epilogue_step_forms_def)
     apply(clarsimp simp:id_def)
     using WL.assign_eval\<^sub>w apply blast
    (* Assign case 2: non-empty compile_expr output *)
    apply clarsimp
    apply(subgoal_tac "\<forall>i < length P\<^sub>e. compile_expr_output_forms (snd (fst (P\<^sub>e ! i)))")
     prefer 2
     apply(metis compile_expr_output_forms)
    apply(subgoal_tac "\<R>_conds \<langle>v \<leftarrow> e, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A \<langle>((0, map fst P\<^sub>e @ [(None, Store v r)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C")
     prefer 2
     apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def regs_noRW_pair_def)
     apply(clarsimp simp:cmds_stop_consistently_def)
     apply(subgoal_tac "(abs_steps' (v \<leftarrow> e) (0, map fst P\<^sub>e @ [(None, Store v r)])) = 0")
      prefer 2
      apply(force simp add: abs_steps'_def nth_append)
     apply clarsimp
     using "WL.neval_0"
     apply blast
    apply(rule_tac P\<^sub>e=P\<^sub>e and r=r and C=C and C'=C' and l=l and nl=nl in \<R>.assign_expr, simp+)
             using Assign.prems(2) apply blast
            apply force
           apply force
          apply force
         using compile_expr_first_comprec_input apply force
        using compile_expr_first_comprec_input apply force
       apply force
      apply force
     apply force
    by (force simp:id_def)
next
  case Skip thus ?case
    apply(clarsimp simp:zip_eq_conv)
    apply(rule \<R>.skip_nop, simp+)
    apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def regs_noRW_pair_def)
    apply(clarsimp simp:cmds_stop_consistently_def abs_steps'_def compile_expr_output_forms_def epilogue_step_forms_def)
    apply(clarsimp simp:id_def)
    using WL.skip_eval\<^sub>w by blast
next
  case (Seq c1 c2) thus ?case
    apply(clarsimp simp:zip_eq_conv split:prod.splits)
    apply(rename_tac P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 fail\<^sub>2)
    apply(frule compile_cmd_input_reqs_seq1)
    apply(erule_tac x="map fst P\<^sub>1" in meta_allE)
    apply(erule_tac x="map snd P\<^sub>1" in meta_allE)
    apply(erule_tac x="l\<^sub>1" in meta_allE)
    apply(erule_tac x="nl\<^sub>1" in meta_allE)
    apply(erule_tac x="C\<^sub>1" in meta_allE)
    apply(erule_tac x="C" in meta_allE)
    apply(erule_tac x="l" in meta_allE)
    apply(erule_tac x="nl" in meta_allE)
    apply(erule_tac x="regs" in meta_allE)
    apply(erule_tac x="mem\<^sub>C" in meta_allE)
    apply(erule_tac x="mds\<^sub>C" in meta_allE)
    apply(clarsimp simp:zip_map_fst_snd)
    apply(subgoal_tac "P\<^sub>1 \<noteq> []")
     prefer 2
     apply(clarsimp simp:compile_cmd_input_reqs_def)
     using compiled_cmd_nonempty apply(metis fst_conv)
    apply(subgoal_tac "regrec (map snd P\<^sub>1 ! 0) \<subseteq>\<^sub>m regrec C")
     prefer 2
     apply(clarsimp simp:compile_cmd_input_reqs_def)
     using compile_cmd_first_regrec_subset_input
     apply(metis (no_types, lifting) length_greater_0_conv nth_map)
    apply(subgoal_tac "asmrec (map snd P\<^sub>1 ! 0) = asmrec C")
     prefer 2
     apply(clarsimp simp:compile_cmd_input_reqs_def)
     using compile_cmd_first_asmrec_input
     apply(metis (no_types, lifting) length_greater_0_conv nth_map)
    apply(rule_tac c\<^sub>1\<^sub>A'=c1 and c\<^sub>1\<^sub>A=c1 and c\<^sub>2\<^sub>A=c2 and P\<^sub>1="P\<^sub>1" and P\<^sub>2="P\<^sub>2" in \<R>.seq)
                  apply(force simp:cmd_seq_def compile_cmd_input_reqs_def)
                 using Seq.prems(1) apply blast
                apply force
               apply force
              apply force
             apply force
            apply force
           apply(force simp:compiled_cmds_joinable compile_cmd_input_reqs_def)
          apply(force simp:compile_cmd_input_reqs_def)
         apply(clarsimp simp:compile_cmd_input_reqs_def)
         using compiled_cmd_nonempty apply(metis fst_conv)
        apply force
       apply clarsimp
       using compile_cmd_first_regrec_subset_input regrec_le_mem_consistent
       apply(metis (no_types, lifting) length_greater_0_conv length_map nth_append nth_map)
      apply(force simp:nth_append)
     apply clarsimp
     using regrec_le_asmrec_consistent compile_cmd_input_reqs_def
     apply(metis (no_types, hide_lams) length_greater_0_conv length_map nth_append nth_map)
    apply clarsimp
    apply(rename_tac P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 fail\<^sub>2 mds\<^sub>C'' mem\<^sub>C'' regs')
    apply(erule_tac x="map fst P\<^sub>2" in meta_allE)
    apply(erule_tac x="map snd P\<^sub>2" in meta_allE)
    apply(erule_tac x="l'" in meta_allE)
    apply(erule_tac x="nl'" in meta_allE)
    apply(erule_tac x="C'" in meta_allE)
    apply(erule_tac x="C\<^sub>1" in meta_allE)
    apply(erule_tac x="l\<^sub>1" in meta_allE)
    apply(erule_tac x="nl\<^sub>1" in meta_allE)
    apply(erule_tac x="regs'" in meta_allE)
    apply(erule_tac x="mem\<^sub>C''" in meta_allE)
    apply(erule_tac x="mds\<^sub>C''" in meta_allE)
    apply(clarsimp simp:zip_map_fst_snd)
    apply(frule compile_cmd_input_reqs_seq2)
     apply force
    by (force simp:compiled_cmd_init_reqs_def id_def)
next
  case (If e c1 c2)
  thus ?case
    apply -
    apply(clarsimp split:prod.splits)
    apply(rename_tac P\<^sub>e r C\<^sub>1 fail\<^sub>1 br nlx ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 fail\<^sub>2 P\<^sub>2 l\<^sub>2 C\<^sub>3 fail\<^sub>3)
    using If.prems(1)
    apply -
    apply(frule compile_cmd_first_asmrec_input)
      apply(force simp:compile_cmd_input_reqs_def)
     apply force
    apply(frule compile_cmd_first_regrec_subset_input)
      apply(force simp:compile_cmd_input_reqs_def)
     apply force
    apply(rule_tac br=br and nl'=nlx in \<R>.if_expr)
                  apply force
                 using If.prems(1) apply blast
                apply force
               apply(metis (no_types, hide_lams) map_fst_zip)
              apply force
             apply force
            apply force
           apply force
          apply force
         apply force
        apply(fastforce dest:regrec_le_mem_consistent)
       apply force
      apply(fastforce dest:regrec_le_asmrec_consistent simp:compile_cmd_input_reqs_def)
     apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def regs_noRW_pair_def cmds_stop_consistently_def)
     apply(case_tac "(abs_steps' (Stmt.If e c1 c2) (0, P)) = 0")
      apply(clarsimp simp:id_def)
      using WL.neval_0 apply meson
     apply(clarsimp simp:id_def)
     using WL.if_eval\<^sub>w apply meson
    apply clarsimp
    apply(rename_tac P\<^sub>e r C\<^sub>1 fail\<^sub>1 br nlx ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 fail\<^sub>2 P\<^sub>2 l\<^sub>2 C\<^sub>3 fail\<^sub>3 mds\<^sub>C'' mem\<^sub>C'' regs')
    apply(erule_tac x="map fst P\<^sub>1" in meta_allE)
    apply(erule_tac x="map snd P\<^sub>1" in meta_allE)
    apply(erule_tac x=l\<^sub>1 in meta_allE)
    apply(erule_tac x=nl\<^sub>1 in meta_allE)
    apply(erule_tac x=C\<^sub>2 in meta_allE)
    apply(erule_tac x=C\<^sub>1 in meta_allE)
    apply(erule_tac x=None in meta_allE)
    apply(erule_tac x=nl'' in meta_allE)
    apply(erule_tac x=regs' in meta_allE)
    apply(erule_tac x=mem\<^sub>C'' in meta_allE)
    apply(erule_tac x=mds\<^sub>C'' in meta_allE)
    apply(frule compiled_expr_asmrec_constancy_end)
    apply(frule_tac pc="length P\<^sub>e" in compiled_expr_maintains_regrec_stability)
      apply(force simp add:compile_cmd_input_reqs_def)
     apply(force simp add:compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)
    apply(clarsimp simp:zip_map_fst_snd compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)
    apply(erule_tac x="map fst P\<^sub>2" in meta_allE)
    apply(erule_tac x="map snd P\<^sub>2" in meta_allE)
    apply(erule_tac x=l\<^sub>2 in meta_allE)
    apply(erule_tac x=nl' in meta_allE)
    apply(erule_tac x=C\<^sub>3 in meta_allE)
    apply(erule_tac x=C\<^sub>1 in meta_allE)
    apply(erule_tac x="Some br" in meta_allE)
    apply(erule_tac x=nl\<^sub>1 in meta_allE)
    apply(erule_tac x=regs' in meta_allE)
    apply(erule_tac x=mem\<^sub>C'' in meta_allE)
    apply(erule_tac x=mds\<^sub>C'' in meta_allE)
    apply(clarsimp simp:zip_map_fst_snd RL.fresh_label_def)
    apply(subgoal_tac "nl < nl\<^sub>1")
     prefer 2
     apply(meson Suc_leD Suc_le_lessD compile_cmd_output_labelrec)
    apply(frule compiled_expr_asmrec_constancy_end)
    apply(frule_tac pc="length P\<^sub>e" in compiled_expr_maintains_regrec_stability)
      apply force
     apply force
    using compile_expr_preserves_finite_regrec
    by (force simp:compiled_cmd_init_reqs_def id_def)
next
  case (While b c)
  thus ?case
    apply -
    apply(clarsimp split:prod.splits)
    apply(rename_tac lp nl'x ex nl'' P\<^sub>e r C\<^sub>e fail\<^sub>e P\<^sub>c l\<^sub>c C\<^sub>c fail\<^sub>c)
    using While.prems(1)
    apply -
    apply(frule compile_cmd_first_asmrec_input)
      apply(force simp:compile_cmd_input_reqs_def)
     apply force
    apply(frule compile_cmd_first_regrec_subset_input)
      apply(force simp:compile_cmd_input_reqs_def)
     apply force
    apply(rule \<R>.while_expr)
                 apply force
                using While.prems(1) apply blast
               apply force
              apply(metis (no_types, hide_lams) map_fst_zip)
             apply force
            apply force
           apply force
          apply force
         apply force
        apply(fastforce dest:regrec_le_mem_consistent)
       apply force
      apply(fastforce dest:regrec_le_asmrec_consistent simp:compile_cmd_input_reqs_def)
     apply(clarsimp simp:\<R>_conds preserves_modes_mem_pair_def regs_noRW_pair_def cmds_stop_consistently_def)
     apply(case_tac "(abs_steps' (While b c) (0, P)) = 0")
      apply(clarsimp simp:id_def)
      using WL.neval_0 apply meson
     apply(clarsimp simp:id_def)
     using WL.while_eval\<^sub>w apply meson
    apply clarsimp
    apply(rename_tac lp nl'x ex nl'' P\<^sub>e r C\<^sub>e fail\<^sub>e P\<^sub>c l\<^sub>c C\<^sub>c fail\<^sub>c mds\<^sub>C'' mem\<^sub>C'' regs')
    apply(erule_tac x="map fst P\<^sub>c" in meta_allE)
    apply(erule_tac x="map snd P\<^sub>c" in meta_allE)
    apply(erule_tac x=l\<^sub>c in meta_allE)
    apply(erule_tac x=nl' in meta_allE)
    apply(erule_tac x=C\<^sub>c in meta_allE)
    apply(erule_tac x=C\<^sub>e in meta_allE)
    apply(erule_tac x=None in meta_allE)
    apply(erule_tac x=nl'' in meta_allE)
    apply(erule_tac x=regs' in meta_allE)
    apply(erule_tac x=mem\<^sub>C'' in meta_allE)
    apply(erule_tac x=mds\<^sub>C'' in meta_allE)
    apply clarsimp
    apply(frule compiled_expr_asmrec_constancy_end)
    apply clarsimp
    apply(frule_tac pc="length P\<^sub>e" in compiled_expr_maintains_regrec_stability)
      apply(force simp:regrec_stable_def)
     apply(force simp:compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)
    apply(clarsimp simp:zip_map_fst_snd RL.fresh_label_def)
    apply(subgoal_tac "compile_cmd_input_reqs C\<^sub>e None (Suc ex) c")
     prefer 2
     apply(clarsimp simp:compile_cmd_input_reqs_def no_unstable_exprs_def split:prod.splits)
     apply(subgoal_tac "finite (dom (regrec C\<^sub>e))")
      prefer 2
      apply(rule_tac C="C\<lparr>regrec := Map.empty\<rparr>" in compile_expr_preserves_finite_regrec)
       apply force
      apply force
     apply force
    by (force simp:compiled_cmd_init_reqs_def id_def)
next
  case Stop thus ?case unfolding compile_cmd_input_reqs_def by force
qed

end
end
