(*
Author: Robert Sison
*)

section {* Dependent SIFUM-security-preserving compilation step from While to RISC *}

(* This is an adaptation of the type-directed compilation step of TedescoSR14/16, which preserved
  only the SS (strong security) property for SS programs (a subset of SIFUM-secure programs).

  Note that the single-step compilation presented partially in the main part of TedescoSR14/16
  (the rest in TSR'14 Appendix G, H; Fig 13, 14) is an abstraction of the two-part compilation
  via intermediate language i-while that they actually proved SS preservation for.

  The intermediate 'i-while' language is a register-aware version of the 'while' language.
  It requires that the conditional expression of If and While commands is already compiled
  into an evaluation step for that expression + a conditional If'/While' on that register.
  (Note this is a lot like the refinement step we did in the illustrative Eg1Eg2 example
  for the Dependent_SIFUM_refinement package.)

  In this theory however, we are skipping the i-while intermediate language.
  We attempt to adapt directly the abstract type system given in the main paper of TedescoSR14/16
  going directly from While to RISC, as opposed to the one via i-while given in the Appendix. *)

theory RISCCompilation
imports
  "../old-type-system/GloballySoundWhileLockUse"
  "../old-type-system/GloballySoundRISCLockUse"
  "../refinement/CompositionalRefinement"
  "ExprsMemWithBinOps"
begin

subsection {* Compilation preliminaries *}

(* This register record is an augmented version of the one from TedescoSR14.
  Instead of mapping from a register to a variable, it maps to an expression on variables.
  The old usage corresponds to when there's a mapping exactly to an expression of just one var. *)
type_synonym ('reg, 'val, 'var, 'opid) RegRec = "'reg \<rightharpoonup> ('val, 'var, 'opid) exp"

(* Corresponds to the stable set \<S> of [Murray et al. 2016] *)
type_synonym 'var AsmRec = "('var set \<times> 'var set)"

record ('reg, 'val, 'var, 'opid) CompRec =
  regrec :: "('reg, 'val, 'var, 'opid) RegRec"
  asmrec :: "'var AsmRec"

locale risc_compilation = op_set_param_lang op_ev +
  WL: sifum_global_modes exp_ev exp_ev\<^sub>B eval\<^sub>L exp_vars exp_vars lock_interp lock_val_True lock_val_False dma \<C>_vars \<C> INIT\<^sub>w BISIM_REQS\<^sub>w +
  RL: risc_global_modes bin_op some_reg some_var some_lock eval\<^sub>L lock_interp lock_val_True lock_val_False dma \<C>_vars \<C> INIT\<^sub>r "\<lambda>_.True"
  for op_ev :: "'OpId \<Rightarrow> 'Val::{zero} \<Rightarrow> 'Val \<Rightarrow> 'Val"
  and dma :: "(('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> ('Lock, 'Var) Var \<Rightarrow> Sec"
  and \<C>_vars :: "('Lock, 'Var) Var \<Rightarrow> ('Lock, 'Var) Var set"
  and \<C> :: "('Lock, 'Var) Var set"
  and bin_op :: "'OpId \<Rightarrow> 'Val::{zero} \<Rightarrow> 'Val \<Rightarrow> 'Val"
  and some_reg :: "'Reg"
  and some_var :: "'Var"
  and some_lock :: "'Lock"
  and eval\<^sub>L :: "'Val \<Rightarrow> bool"
  and lock_interp :: "'Lock \<Rightarrow> ('Var set \<times> 'Var set)"
  and lock_val_True :: "'Val"
  and lock_val_False :: "'Val"
  and reg_alloc_cached :: "('Reg, 'Val, 'Var, 'OpId) RegRec \<Rightarrow> 'Reg set \<Rightarrow>
                    (* Not clear to me yet if this kind of versatility would be reasonable:
                    ('Val, 'Var, 'OpId) exp \<Rightarrow>
                      So just reflect what's currently required for now: *)
                    'Var \<Rightarrow> 'Reg option"
  and reg_alloc :: "('Reg, 'Val, 'Var, 'OpId) RegRec \<Rightarrow> 'Reg set \<Rightarrow> 'Reg option"
  and INIT\<^sub>w :: "(('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> bool"
  and INIT\<^sub>r :: "(('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> bool"
  and BISIM_REQS\<^sub>w :: "(('Var, 'Lock, ('Val, 'Var, 'OpId) exp, ('Val, 'Var, 'OpId) exp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf rel \<Rightarrow> bool" +
  assumes no_H_locks: "\<forall> l mem. dma mem (Lock l) = Low"
  (* From GloballySoundLockUse.thy
    NB: We may not actually need all of them, but we definitely need locks not to be control vars.
  assumes no_locks_in_\<C>_vars : "\<forall> v l. (Lock l) \<notin> \<C>_vars v" *)
  assumes no_locks_in_\<C> : "\<forall> l. (Lock l) \<notin> \<C>"
  assumes lock_interp_no_overlap: "lock_interp_no_overlap lock_interp"
  (* Taken from the agrels/cddc-buf development head.
     Note that we won't be able to use the existing type system to prove security for any programs
     that use such locks. We'll first need to adapt the type system changes that allow typechecking
     of lock operations on locks that cover both a var and its control vars. *)
  assumes lock_interp_\<C>_vars\<^sub>V:
      "\<And>c x f. f \<in> {fst,snd} \<Longrightarrow> Var c \<in> \<C>_vars (Var x) \<Longrightarrow> 
                 (c \<in> f (lock_interp l)) = (x \<in> f (lock_interp l))"
  assumes reg_alloc_correct:
    "\<And>r. finite (dom \<Phi>) \<Longrightarrow> finite A \<Longrightarrow>
          reg_alloc \<Phi> A = Some r \<Longrightarrow> r \<notin> A"
                           (* ^ NB: this on its own allows regs to be overwritten randomly
                              \<and> r \<notin> dom \<Phi>
                              ^ including this would disallow random overwriting of regs *)
  assumes reg_alloc_cached_correct:
    "\<And>r. finite (dom \<Phi>) \<Longrightarrow> finite A \<Longrightarrow>
          reg_alloc_cached \<Phi> A v = Some r \<Longrightarrow> \<Phi> r = Some (ELoad v) \<and> r \<notin> A"
    "finite (dom \<Phi>) \<Longrightarrow> finite A \<Longrightarrow>
     reg_alloc_cached \<Phi> A v = None \<Longrightarrow> \<not> (\<exists>r. \<Phi> r = Some (ELoad v) \<and> r \<notin> A)"
  (* TODO: Why don't we actually just use the same bin_op function, then?
    NB: Fixed on a later (as-yet unpublished) development head. *)
  assumes same_binary_ops:
    "bin_op = op_ev"

context risc_compilation
begin

definition asmrec_var_stable'
where
  "asmrec_var_stable' \<S> v \<equiv> v \<in> fst \<S> \<union> snd \<S> \<and>
     (* This should be provable from the requirement
       for variables and their control vars to be governed by the same lock *)
     (\<forall>v'. (Var v') \<in> \<C>_vars (Var v) \<longrightarrow> v' \<in> fst \<S> \<union> snd \<S>)"

abbreviation asmrec_var_stable
where
  "asmrec_var_stable C v \<equiv> asmrec_var_stable' (asmrec C) v"

definition asmrec_expr_stable'
where
  "asmrec_expr_stable' \<S> e \<equiv> \<forall>v \<in> exp_vars e. asmrec_var_stable' \<S> v"

abbreviation asmrec_expr_stable
where
  "asmrec_expr_stable C e \<equiv> asmrec_expr_stable' (asmrec C) e"

subsection {* While-to-RISC compilation step as a function *}

(* An intersection of register records is defined as the subset of mappings on which they agree *)
abbreviation regrec_inter ::
  "('reg, 'val, 'var, 'opid) RegRec \<Rightarrow> ('reg, 'val, 'var, 'opid) RegRec \<Rightarrow> ('reg, 'val, 'var, 'opid) RegRec"
  (infix "\<sqinter>\<^sub>R" 100)
where
  "\<Phi> \<sqinter>\<^sub>R \<Phi>' \<equiv> restrict_map \<Phi> {r. \<Phi> r = \<Phi>' r}"

lemma regrec_inter_sym:
  "\<Phi> \<sqinter>\<^sub>R \<Phi>' = \<Phi>' \<sqinter>\<^sub>R \<Phi>"
  unfolding restrict_map_def
  by auto

lemma regrec_inter_le:
  "(\<Phi> \<sqinter>\<^sub>R \<Phi>') \<subseteq>\<^sub>m  \<Phi>"
  by (clarsimp simp:map_le_def)

(* Note: the list of fsts of elements in this list will be a PMem, which is just an Inst list *)
type_synonym ('reg, 'val, 'var, 'lock, 'opid) AugPMem =
  "((('reg, 'var, 'lock, 'val, 'opid) Inst \<times> ('reg, 'val, 'var, 'opid) CompRec) list)"

(* This compilation is slightly different to the one in TedescoSR16 in that it outputs an AugPMem,
  which is just a PMem zipped up with snapshots of the RegRec current for each instruction.
  We'll need the RegRecs packaged with this AugPMem to generate a refinement relation \<R> relating
  each command of the original program with each instruction of the resulting RISC one. *)

definition higher_of :: "Sec \<Rightarrow> Sec \<Rightarrow> Sec"
where
  "higher_of s\<^sub>1 s\<^sub>2 \<equiv> if s\<^sub>1 = High \<or> s\<^sub>2 = High then High else Low"

(* IMPORTANT: it's possible this can return [], in which case it has NOT applied the label l.
   Thus, invocations of compile_expr have to check if the returned list is empty and
   if so, pass the label l forward for appendage to the next compiled instruction.
   Also note that this function never outputs a Store or Jz instruction. *)
(* NB: I don't think there's a non-painful way to prevent random register overwrite
  and still keep nice space-savers like V-Cached. Realistically we should expect some
  register trashing, and keeping that abstract for now allows for any such implementation.
  Thinking about the specific register allocation scheme at this early stage is too much
  trouble (over-optimising) merely on account of the While construct. Something to revisit. *)
primrec compile_expr :: "('Reg, 'Val, 'Var, 'OpId) CompRec \<Rightarrow> 'Reg set \<Rightarrow> Lab option \<Rightarrow> ('Val, 'Var, 'OpId) exp
  \<Rightarrow> ('Reg, 'Val, 'Var, 'Lock, 'OpId) AugPMem \<times> 'Reg \<times> ('Reg, 'Val, 'Var, 'OpId) CompRec \<times> bool"
where
  "compile_expr C A l (EConst k) =
    (case reg_alloc (regrec C) A of
     Some r \<Rightarrow> ([((l, MoveK r k), C)], r, C\<lparr>regrec := (regrec C)(r := Some (EConst k))\<rparr>, False) |
     None \<Rightarrow> ([], some_reg, C, True))" |
  "compile_expr C A l (ELoad v) =
    (case reg_alloc_cached (regrec C) A v of
     (* V-cached *)
     (* Note: In this case, the label l is not applied and must be passed forward.
        We do this instead of having an 'empty' instruction as TedescoSR16 did. *)
     Some r \<Rightarrow> ([], r, C, False) |
     (* V-uncached *)
     None \<Rightarrow> (case reg_alloc (regrec C) A of
              Some r \<Rightarrow> ([((l, Load r v), C)], r, C\<lparr>regrec := (regrec C)(r := Some (ELoad v))\<rparr>, False) |
              None \<Rightarrow> ([], some_reg, C, True)))" |
  "compile_expr C A l (EOp opid x y) =
    (let (P\<^sub>1, r, C\<^sub>1, fail\<^sub>1) = compile_expr C A l x;
         (P\<^sub>2, r', C\<^sub>2, fail\<^sub>2) = compile_expr C\<^sub>1 (A \<union> {r}) (if P\<^sub>1 = [] then l else None) y;
         C' = C\<^sub>2\<lparr>regrec := (regrec C\<^sub>2)(r := Some (EOp opid x y))\<rparr>
      in (P\<^sub>1 @
          P\<^sub>2 @
          (* This Op should be parameterised by a RISC-level opcode *)
          [((if P\<^sub>1 = [] \<and> P\<^sub>2 = [] then l else None, Op opid r r'), C\<^sub>2)],
          r, C', fail\<^sub>1 \<or> fail\<^sub>2))"

definition exp_vars_and_\<C>s :: "(_, 'Var, 'OpId) exp \<Rightarrow> 'Var set"
where
  "exp_vars_and_\<C>s e \<equiv> exp_vars e \<union> {v. Var v \<in> \<Union>(\<C>_vars ` Var ` exp_vars e)}"

(* Purge all register records that mention the variable v.
  This helps us achieve what the original compilation of TedescoSR14 expressed as "\<Phi>[r \<leftrightarrow> x]",
  adapted for the fact that we've extended RegRecs to keep track of entire expressions. *)
definition regrec_purge_var ::
  "('Reg, 'Val, 'Var, 'OpId) RegRec \<Rightarrow> 'Var \<Rightarrow> ('Reg, 'Val, 'Var, 'OpId) RegRec"
where
  "regrec_purge_var \<Phi> v \<equiv> (restrict_map \<Phi> {r. r \<in> dom \<Phi> \<longrightarrow> v \<notin> exp_vars (the (\<Phi> r))})"

(* Purge all register records that mention any of the vars in the given set or their control vars. *)
definition regrec_purge_vars ::
  "('Reg, 'Val, 'Var, 'OpId) RegRec \<Rightarrow> 'Var set \<Rightarrow> ('Reg, 'Val, 'Var, 'OpId) RegRec"
where
  "regrec_purge_vars \<Phi> vs \<equiv> (restrict_map \<Phi> {r. r \<in> dom \<Phi> \<longrightarrow> (vs \<inter> exp_vars_and_\<C>s (the (\<Phi> r))) = Set.empty})"

(* Lab option is a label for the first instruction, where None indicates \<epsilon>\<^sub>L.
  LabRec (both arg and ret) tracks the next label available for use, updated as necessary.
  Note assumption that expr compilation does not generate any new labels. *)
primrec compile_cmd :: "('Reg, 'Val, 'Var, 'OpId) CompRec \<Rightarrow> Lab option \<Rightarrow> LabRec \<Rightarrow>
  ('Var, 'Lock, ('Val, 'Var, 'OpId) exp, ('Val, 'Var, 'OpId) exp) Stmt
  \<Rightarrow> ('Reg, 'Val, 'Var, 'Lock, 'OpId) AugPMem \<times> Lab option \<times> LabRec \<times> ('Reg, 'Val, 'Var, 'OpId) CompRec \<times> bool"
where
  "compile_cmd C l nl (Stmt.LockAcq k) =
    (let C' = C\<lparr>asmrec := (fst (asmrec C) \<union> fst (lock_interp k), snd (asmrec C) \<union> snd (lock_interp k))\<rparr>
      in ([((l, InstB.LockAcq k), C)], None, nl, C', False))" |
  "compile_cmd C l nl (Stmt.LockRel k) =
    (let C' = C\<lparr>asmrec := (fst (asmrec C) - fst (lock_interp k), snd (asmrec C) - snd (lock_interp k)),
                (* clear RegRec entries that would become unstable *)
                regrec := regrec_purge_vars (regrec C) (fst (lock_interp k) \<union> snd (lock_interp k))\<rparr>
      in ([((l, InstB.LockRel k), C)], None, nl, C', False))" |
  "compile_cmd C l nl (Assign v a) =
    (let (P, r, C', fail) = (compile_expr C {} l a);
         (* clear RegRec entries that mention v, so r becomes the sole Reg talking about v *)
         C'' = C'\<lparr>regrec := (regrec_purge_var (regrec C') v)\<rparr>;
         (* ... but only if v is stable. Otherwise r retains the cached value of the expression *)
         C''' = C''\<lparr>regrec := (regrec C'')(r := Some (ELoad v))\<rparr>
      in (P @ [((if P = [] then l else None, Store v r), C')], None, nl,
          if asmrec_var_stable C v then C''' else C'', fail))" |
  "compile_cmd C l nl Skip =
    ([((l, Nop), C)], None, nl, C, False)" |
  "compile_cmd C l nl (Seq c\<^sub>1 c\<^sub>2) =
    (let (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>1, fail\<^sub>1) = (compile_cmd C l nl c\<^sub>1);
         (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>2, fail\<^sub>2) = (compile_cmd C\<^sub>1 l\<^sub>1 nl\<^sub>1 c\<^sub>2)
      in ((P\<^sub>1 @ P\<^sub>2), l\<^sub>2, nl\<^sub>2, C\<^sub>2, fail\<^sub>1 \<or> fail\<^sub>2))" |
  "compile_cmd C l nl (If b c\<^sub>1 c\<^sub>2) =
    (* Note: This only securely handles Low-conditional branches.
      We assume this to be the case via a consistent-branching condition (bisim_ev\<^sub>B_eq).
      For if-any, need to determine the write-effect of both branches c\<^sub>1, c\<^sub>2
      and check that they are no lower than the classification of the conditional b,
      to ensure that no Low writes happen in any High conditionals (Denning CACM'77) *)
    (let (P\<^sub>0, r, C\<^sub>1, fail\<^sub>e) = (compile_expr C {} l b);
         (br, nl') = RL.fresh_label nl;
         (ex, nl'') = RL.fresh_label nl';
         (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, fail\<^sub>1) = (compile_cmd C\<^sub>1 None nl'' c\<^sub>1);
         (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, fail\<^sub>2) = (compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2);
         C' = \<lparr>regrec = regrec C\<^sub>2 \<sqinter>\<^sub>R regrec C\<^sub>3,
               (* Here we lean on a pre-compilation check that asmrec C\<^sub>2 = asmrec C\<^sub>3 *)
               asmrec = asmrec C\<^sub>2\<rparr>
      in (P\<^sub>0 @
          [((if P\<^sub>0 = [] then l else None, Jz br r), C\<^sub>1)] @
          P\<^sub>1 @
          [((l\<^sub>1, Jmp ex), C\<^sub>2)] @
          P\<^sub>2 @
          [((l\<^sub>2, Tau), C\<^sub>3)], Some ex, nl\<^sub>2, C', fail\<^sub>e \<or> fail\<^sub>1 \<or> fail\<^sub>2))" |
  (* The While command of TedescoSR14 had a var instead of a generic bexpr conditional
    but here we'll try to allow for the generic form. *)
  "compile_cmd C l nl (While b c) =
    (* There is a subtlety with generating \<R> here if \<Phi>\<^sub>B after the initial check
       doesn't match up with \<Phi>\<^sub>B' after Jmp lp.
      I think the key is we somehow have to implement the following two guards
      from the While rule in TedescoSR14's type system (for some output regrec \<Phi>\<^sub>B):
       \<Phi>\<^sub>B \<sqsubseteq> \<Phi>[r \<leftrightarrow> x]; \<Phi>\<^sub>B \<sqsubseteq> \<Phi>\<^sub>E[r \<leftrightarrow> x]
        which in our case is really (for some regrec \<Phi>\<^sub>O to be true at the Jz and exit):
       \<Phi>\<^sub>O \<sqsubseteq> \<Phi>; \<Phi>\<^sub>O \<sqsubseteq> \<Phi>\<^sub>E
        I tried proving that certain wiping schemes establish \<Phi>\<^sub>O \<sqsubseteq> \<Phi>\<^sub>B', for any unknown \<Phi>\<^sub>B',
      by arguing that the command and expression in question cannot cause the removal
      of any mappings in \<Phi>\<^sub>O because all relevant mappings have already been removed.
        But it turns out this is not true for our expression compiler - neither if we wipe all
      the l-values of the target program, nor even if we wipe *every* mentioned variable
      in the program - because it allows random trashing of any register that isn't in A.
        So we bite the bullet for now and take the most conservative regrec
      possible for the While-loop body - wipe the regrec. *)
    (* Another thing I fixed here was I got rid of the duplication of the expr by TedescoSR14.
      Now the loop point is the beginning of the program, but (I believe) we still need
      to wipe the regrec at this point, where the regrecs have to reconcile with each other. *)
    (* Note: This only securely handles Low-conditional branches.
      We assume this to be the case via a consistent-branching condition (bisim_ev\<^sub>B_eq).
      We'll only be able to support High conditionals once we can account for the
      subsequent write-effects and ensure they are Low-only *)
   (let (lp, nl') = if l = None then RL.fresh_label nl else (the l, nl);
        (ex, nl'') = RL.fresh_label nl';
        (P\<^sub>e, r, C\<^sub>B, fail\<^sub>e) = compile_expr (C\<lparr>regrec := Map.empty\<rparr>) {} (Some lp) b; (* NB: regrec wiped! *)
        (P, l', nl''', C\<^sub>E, fail\<^sub>c) = (compile_cmd C\<^sub>B None nl'' c)
     in (P\<^sub>e @
         (* NB: Here we are assuming that P\<^sub>e will never be empty because it was compiled with
            an empty input regrec. This gets rid of an annoying 'if' to deal with in proofs. *)
         (* [((if P\<^sub>e = [] then Some lp else None, Jz ex r), C\<^sub>B)] @ *)
         [((None, Jz ex r), C\<^sub>B)] @
         P @
         [((l', Jmp lp), C\<^sub>E)], Some ex, nl''',
           (* Lean on lemmas/pre-compilation check that asmrec C = asmrec C\<^sub>B = asmrec C\<^sub>E *)
           C\<lparr>regrec := Map.empty\<rparr>, fail\<^sub>e \<or> fail\<^sub>c))" |
  "compile_cmd C l nl Stop = ([], None, nl, C, True)"

end (* context risc_compilation *)

sublocale risc_compilation \<subseteq> sifum_refinement_init dma dma \<C>_vars \<C>_vars \<C> \<C> WL.eval\<^sub>w RL.eval\<^sub>r undefined INIT\<^sub>w INIT\<^sub>r BISIM_REQS\<^sub>w id
  (* These become pretty trivial now that we've moved the regs into thread-local state
    and both languages have the same Lock+Var variable space. *)
  by (unfold_locales, force+)

context risc_compilation
begin

subsection {* RISC language instantiation helpers *}

abbreviation current_instB :: "('Reg, 'Var, 'Lock, 'Val, 'OpId) RL.PMCmd \<Rightarrow> ('Reg, 'Var, 'Lock, 'Val, 'OpId) InstB"
where
  "current_instB p \<equiv> snd ((snd p) ! (fst p))"

lemma current_instB_head:
  "pc < length P \<Longrightarrow> current_instB (pc, P) = current_instB (pc, P @ tail)"
  by (simp add: nth_append)

(* RISC language helpers adapted for this locale *)

lemma load_dest':
  "(\<langle>c, regs, mds, mem\<rangle>\<^sub>r, \<langle>c', regs', mds', mem'\<rangle>\<^sub>r) \<in> RL.eval\<^sub>r \<Longrightarrow> current_instB c = Load r v \<Longrightarrow>
   fst c' = fst c + 1 \<and> snd c = snd c' \<and> regs' = regs(r := mem (Var v)) \<and> mds = mds' \<and> mem = mem'"
proof -
  assume a1: "current_instB c = Load r v"
  assume "eval_abv\<^sub>C \<langle>(c, regs), mds, mem\<rangle>\<^sub>C \<langle>(c', regs'), mds', mem'\<rangle>\<^sub>C"
  then have "eval_abv\<^sub>C \<langle>((fst c, snd c), regs), mds, mem\<rangle>\<^sub>C \<langle>((fst c', snd c'), regs'), mds', mem'\<rangle>\<^sub>C"
    by auto
  then show ?thesis
    using a1 by blast
qed

lemma op_dest':
  "(\<langle>c, regs, mds, mem\<rangle>\<^sub>r, \<langle>c', regs', mds', mem'\<rangle>\<^sub>r) \<in> RL.eval\<^sub>r \<Longrightarrow> current_instB c = Op opid r r' \<Longrightarrow>
   fst c' = fst c + 1 \<and> snd c = snd c' \<and> regs' = regs(r := bin_op opid (regs r) (regs r')) \<and> mds = mds' \<and> mem = mem'"
proof -
  assume a1: "current_instB c = Op opid r r'"
  assume "eval_abv\<^sub>C \<langle>(c, regs), mds, mem\<rangle>\<^sub>C \<langle>(c', regs'), mds', mem'\<rangle>\<^sub>C"
  then have "eval_abv\<^sub>C \<langle>((fst c, snd c), regs), mds, mem\<rangle>\<^sub>C \<langle>((fst c', snd c'), regs'), mds', mem'\<rangle>\<^sub>C"
    by simp
  then show ?thesis
    using a1 by blast
qed

lemma move_k_dest':
  "(\<langle>c, regs, mds, mem\<rangle>\<^sub>r, \<langle>c', regs', mds', mem'\<rangle>\<^sub>r) \<in> RL.eval\<^sub>r \<Longrightarrow> current_instB c = MoveK r n \<Longrightarrow>
   fst c' = fst c + 1 \<and> snd c = snd c' \<and> regs' = regs(r := n) \<and> mds = mds' \<and> mem = mem'"
proof -
  assume a1: "current_instB c = MoveK r n"
  assume "eval_abv\<^sub>C \<langle>(c, regs), mds, mem\<rangle>\<^sub>C \<langle>(c', regs'), mds', mem'\<rangle>\<^sub>C"
  then have "eval_abv\<^sub>C \<langle>((fst c, snd c), regs), mds, mem\<rangle>\<^sub>C \<langle>((fst c', snd c'), regs'), mds', mem'\<rangle>\<^sub>C"
    by simp
  then show ?thesis
    using a1 by blast
qed

lemma store_dest':
  "(\<langle>c, regs, mds, mem\<rangle>\<^sub>r, \<langle>c', regs', mds', mem'\<rangle>\<^sub>r) \<in> RL.eval\<^sub>r \<Longrightarrow> current_instB c = Store v r \<Longrightarrow>
   fst c' = fst c + 1 \<and> snd c = snd c' \<and> regs = regs' \<and> mds = mds' \<and> mem' = mem(Var v := regs r)"
proof -
  assume a1: "current_instB c = Store v r"
  assume "eval_abv\<^sub>C \<langle>(c, regs), mds, mem\<rangle>\<^sub>C \<langle>(c', regs'), mds', mem'\<rangle>\<^sub>C"
  then have "eval_abv\<^sub>C \<langle>((fst c, snd c), regs), mds, mem\<rangle>\<^sub>C \<langle>((fst c', snd c'), regs'), mds', mem'\<rangle>\<^sub>C"
    by simp
  then show ?thesis
    using a1 by blast
qed

(* We leverage these "forms" definitions later by noting that certain RISC forms only appear
  as translations of certain While forms. *)

definition compile_expr_output_forms
where
  "compile_expr_output_forms x \<equiv> \<exists> k r r' v opid. x \<in> {Load r v,
     Op opid r r',
     MoveK r k}"

definition epilogue_step_forms
where
  "epilogue_step_forms x \<equiv> \<exists> l. x \<in> {Jmp l, Tau}"

subsection {* Refinement theory instantiation helpers *}

(* Now we specify abs_steps as demanded by the refinement theory *)

definition abs_steps' :: "('Var, 'Lock, ('Val, 'Var, 'OpId) exp, ('Val, 'Var, 'OpId) exp) Stmt \<Rightarrow>
  ('Reg, 'Var, 'Lock, 'Val, 'OpId) RL.PMCmd \<Rightarrow> nat"
where
  "abs_steps' c\<^sub>A c\<^sub>C \<equiv>
   if compile_expr_output_forms (current_instB c\<^sub>C)
   then (if \<exists> b c. WL.leftmost_cmd c\<^sub>A = While b c then 1 else 0)
   else if epilogue_step_forms (current_instB c\<^sub>C) then 0
   (* Now that the WhileLanguage semantics skips over 'Stop ;;', we need not add 1 any more *)
   else 1"

lemma abs_steps_range:
  "abs_steps' c\<^sub>A c\<^sub>C \<in> {0, 1}"
  unfolding abs_steps'_def
  by simp

lemma abs_steps'_head\<^sub>C:
  "pc < length P \<Longrightarrow> abs_steps' c\<^sub>A (pc, P) = abs_steps' c\<^sub>A (pc, P @ tail\<^sub>C)"
  unfolding abs_steps'_def
  using current_instB_head
  by (clarsimp split:Stmt.splits)

fun abs_steps :: "
  (('Var, 'Lock, ('Val, 'Var, 'OpId) exp, ('Val, 'Var, 'OpId) exp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  (('Reg, 'Var, 'Lock, 'Val, 'OpId) RL.PerThread, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow> nat"
where
  "abs_steps \<langle>c\<^sub>A,_,_\<rangle>\<^sub>A \<langle>c\<^sub>C,_,_\<rangle>\<^sub>C = abs_steps' c\<^sub>A (fst c\<^sub>C)"

subsection {* Restrictions on the compilation input *}

(* Some of these bans are scaffolding for future language features not yet supported,
  only the Stop one is expected to be kept permanently - our compiler won't compile Stop. *)

primrec banned_WhileLang_Stop
where
  "banned_WhileLang_Stop (Stmt.LockAcq l) = False" |
  "banned_WhileLang_Stop (Stmt.LockRel l) = False" |
  "banned_WhileLang_Stop Stop = True" |
  "banned_WhileLang_Stop (Seq a b) = (banned_WhileLang_Stop a \<or> banned_WhileLang_Stop b)" |
  "banned_WhileLang_Stop (If _ a b) = (banned_WhileLang_Stop a \<or> banned_WhileLang_Stop b)" |
  "banned_WhileLang_Stop (While _ c) = banned_WhileLang_Stop c" |
  "banned_WhileLang_Stop (Assign _ _) = False" |
  "banned_WhileLang_Stop Skip = False"

primrec has_locking
where
  "has_locking (Stmt.LockAcq l) = True" |
  "has_locking (Stmt.LockRel l) = True" |
  "has_locking Stop = False" |
  "has_locking (Seq a b) = (has_locking a \<or> has_locking b)" |
  "has_locking (If _ a b) = (has_locking a \<or> has_locking b)" |
  "has_locking (While _ c) = has_locking c" |
  "has_locking (Assign _ _) = False" |
  "has_locking Skip = False"

lemma no_locking_eval\<^sub>w_no_modes:
  "\<not> has_locking c \<Longrightarrow>
   (\<langle>c, mds, mem\<rangle>\<^sub>A, \<langle>c', mds', mem'\<rangle>\<^sub>A) \<in> WL.eval\<^sub>w \<Longrightarrow>
   mds = mds'"
  by (induct c arbitrary:mds mem c' mds' mem', auto)

lemma no_locking_eval\<^sub>w_inv:
  "eval_abv\<^sub>A \<langle>c, mds, mem\<rangle>\<^sub>A \<langle>c', mds', mem'\<rangle>\<^sub>A \<Longrightarrow>
   \<not> has_locking c \<Longrightarrow>
   \<not> has_locking c'"
  apply(induct c arbitrary:mds mem c' mds' mem')
         using has_locking.simps apply blast
        using has_locking.simps apply blast
       apply force
      apply force
     apply(metis has_locking.simps(4) WL.seq_dest)
    apply auto[1]
   apply(drule WL.while_dest)
   apply force
  apply force
  done

abbreviation banned_WhileLang_forms
where
  "banned_WhileLang_forms c \<equiv> banned_WhileLang_Stop c"

lemma allowed_WhileLang_seqD:
  "(\<not> banned_WhileLang_forms (Seq a b)) \<Longrightarrow> (\<not> banned_WhileLang_forms a \<and> \<not> banned_WhileLang_forms b)"
  by simp

lemma compiled_cmd_nonempty:
  "\<not> banned_WhileLang_forms c \<Longrightarrow> fst (compile_cmd C l nl c) \<noteq> []"
  by (induct c, (clarsimp simp:Let_def split:prod.splits)+)

subsection {* Expression compilation properties *}

(* Various helpful properties of the expression compilation *)

lemma compile_expr_output_forms:
  "(apm, r, C', fail) = compile_expr C A l e \<Longrightarrow>
   i < length apm \<Longrightarrow>
   compile_expr_output_forms (snd (fst (apm ! i)))"
  unfolding compile_expr_output_forms_def
  apply(induct e arbitrary:A l C r C' fail apm i)
     apply(clarsimp simp:Let_def nth_append split:prod.splits option.splits)+
  apply(safe, (simp|metis)+)
  done

lemma compile_expr_all_output_forms:
  "compile_expr C A l e = (apm, r, C', fail) \<Longrightarrow>
   \<forall>i < length apm. compile_expr_output_forms (snd (fst (apm ! i)))"
  using compile_expr_output_forms
  by metis

lemma compile_expr_correctness:
  "finite (dom (regrec C)) \<Longrightarrow> finite A \<Longrightarrow>
   (apm, r, C', False) = compile_expr C A l e \<Longrightarrow> regrec C' r = Some e"
  apply(induct e arbitrary:A l C r C' apm)
    apply(force simp:Let_def split:option.splits)
   apply(force simp:Let_def ran_def reg_alloc_cached_correct split:option.splits)
  by (force simp:Let_def split:prod.splits)

lemma compile_expr_correctness':
  "finite (dom (regrec C)) \<Longrightarrow> finite A \<Longrightarrow>
   compile_expr C A l e = (apm, r, C', False) \<Longrightarrow> regrec C' r = Some e"
  using compile_expr_correctness
  by metis

lemma compile_expr_emptyD:
  "compile_expr C A l e = ([], r, C', False) \<Longrightarrow>
   finite (dom (regrec C)) \<Longrightarrow> finite A \<Longrightarrow>
   regrec C r = Some e \<and> r \<notin> A"
  apply(induct e)
    apply(force simp:Let_def split:option.splits)
   apply(force simp:Let_def reg_alloc_cached_correct split:option.splits)
  by (force simp:Let_def split:prod.splits)

lemma compile_expr_preserves_finite_regrec:
  "finite (dom (regrec C)) \<Longrightarrow>
   compile_expr C A l e = (apm, r, C', False) \<Longrightarrow> finite (dom (regrec C'))"
proof (induct e arbitrary:A l C r C' apm)
  case EConst thus ?case
    by (clarsimp simp:Let_def simp del:fun_upd_apply split:option.splits)
next
  case ELoad thus ?case
    by (force simp:Let_def simp del:fun_upd_apply split:option.splits)
next
  case (EOp opid e1 e2 A l C r C' apm)
  from EOp(3-4) show ?case
    apply(clarsimp simp:Let_def split:prod.splits)
    apply(rename_tac apm\<^sub>1 C\<^sub>1 fail\<^sub>1 apm\<^sub>2 r\<^sub>2 C\<^sub>2 fail\<^sub>2)
    apply(subgoal_tac "finite (dom (regrec C\<^sub>1))")
     prefer 2
     using EOp(1) apply blast
    apply(subgoal_tac "finite (dom (regrec C\<^sub>2))")
     prefer 2
     using EOp(2) apply blast
    by blast
qed

lemma compile_expr_no_jumps:
  "compile_expr C A l e = (apm, r, C', fail) \<Longrightarrow>
   RL.jump_labels (map fst apm) = {}"
  using compile_expr_all_output_forms
  unfolding RL.jump_labels_def compile_expr_output_forms_def
  by fastforce

lemma compiled_expr_only_increments:
  "compile_expr C A l e = (P, r, C', fail) \<Longrightarrow>
   pc < length P \<Longrightarrow>
   \<langle>(pc, (map fst P) @ P'), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', (map fst P) @ P'), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   pc' = Suc pc"
  apply(frule compile_expr_all_output_forms)
  apply(erule_tac x=pc in allE)
  apply(clarsimp simp:compile_expr_output_forms_def)
  by (erule RL.eval\<^sub>r_elim, (simp add:nth_append)+)

lemma compiled_expr_no_modes:
  "compile_expr C A l e = (P, r, C', fail) \<Longrightarrow>
   pc < length P \<Longrightarrow>
   \<langle>(pc, (map fst P) @ P'), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', (map fst P) @ P'), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   mds = mds'"
  apply(frule compile_expr_all_output_forms)
  apply(erule_tac x=pc in allE)
  apply(clarsimp simp:compile_expr_output_forms_def)
  by (erule RL.eval\<^sub>r_elim, (simp add:nth_append)+)

lemma compiled_expr_doesnt_trash_A:
  "compile_expr C A l e = (P, r, C', False) \<Longrightarrow>
   finite (dom (regrec C)) \<Longrightarrow>
   finite A \<Longrightarrow>
   r' \<in> A \<Longrightarrow>
   regrec C' r' = regrec C r' \<and> r \<notin> A"
  apply(induct e arbitrary:C A l P r C' r')
    apply(force simp:Let_def reg_alloc_correct split:option.splits)
   apply(rule conjI)
    apply(force simp:Let_def reg_alloc_cached_correct reg_alloc_correct split:option.splits)
   apply(force simp:Let_def reg_alloc_cached_correct reg_alloc_correct split:option.splits)
  apply(rename_tac opid e1 e2 C A l P r C' r')
  apply(clarsimp simp:Let_def split:prod.splits)
  apply(rename_tac opid e1 e2 C A l r r' apm\<^sub>1 C\<^sub>1 fail\<^sub>1 apm\<^sub>2 r\<^sub>2 C\<^sub>2 fail\<^sub>2)
  apply(frule_tac e=e1 in compile_expr_preserves_finite_regrec)
   apply force
  apply(frule_tac C=C\<^sub>1 and e=e2 in compile_expr_preserves_finite_regrec)
   apply force
  apply(subgoal_tac "finite (insert r A)")
   prefer 2
   apply force
  apply clarsimp
  by metis

lemma compiled_expr_no_abs_written:
  "compile_expr_output_forms (current_instB c\<^sub>C) \<Longrightarrow>
   (\<langle>c\<^sub>C, regs, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>r, \<langle>c\<^sub>C', regs', mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>r) \<in> RL.eval\<^sub>r \<Longrightarrow>
   mem\<^sub>A_of mem\<^sub>C = mem\<^sub>A_of mem\<^sub>C'"
  unfolding compile_expr_output_forms_def mem\<^sub>A_of_def
  by (fastforce dest:load_dest' op_dest' move_k_dest')

lemma compiled_expr_inst_properties:
  "compile_expr_output_forms (current_instB (pc, P)) \<Longrightarrow>
   \<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   pc' = Suc pc \<and> P = P' \<and> mds = mds'"
  apply(clarsimp simp:compile_expr_output_forms_def)
  by (erule RL.eval\<^sub>r_elim, (force simp:nth_append)+)

subsection {* Compilation properties to do with register records *}

(* Register records track the values held in registers as expressions of concrete variables.
  Thus they are only meaningful if they refer to variables that are currently stable. *)

lemma compile_expr_empty_maintains_comprec:
  "compile_expr C A l e = ([], r, C', False) \<Longrightarrow>
   C' = C"
  apply(induct e)
    apply(force simp:Let_def split:option.splits)
   apply(force simp:Let_def reg_alloc_cached_correct split:option.splits)
  by (force simp:Let_def split:prod.splits)

lemma compile_expr_first_comprec_input:
  "compile_expr C A l e = (P, r, C', False) \<Longrightarrow>
   P \<noteq> [] \<Longrightarrow>
   map snd P ! 0 = C"
  apply(induct e arbitrary: C C' P r A l)
    apply(force simp:Let_def split:option.splits)
   apply(force simp:Let_def reg_alloc_cached_correct split:option.splits)
  apply(clarsimp simp:Let_def compile_expr_empty_maintains_comprec nth_append split:prod.splits if_splits)
  by (metis compile_expr_empty_maintains_comprec)

lemma compile_cmd_first_regrec_subset_input:
  "(P, l', nl', C', False) = (compile_cmd C l nl c) \<Longrightarrow>
   \<not> banned_WhileLang_forms c \<Longrightarrow>
   P \<noteq> [] \<Longrightarrow>
   regrec (map snd P ! 0) \<subseteq>\<^sub>m regrec C"
  apply(induct c arbitrary:P l' nl' C' C l nl)
         apply(clarsimp simp:Let_def last_conv_nth zip_eq_conv split:prod.splits if_splits)
             using compile_expr_empty_maintains_comprec map_le_def apply fastforce
            using compile_expr_first_comprec_input map_le_def apply(force simp: nth_append)
           using compile_expr_empty_maintains_comprec map_le_def apply fastforce
          using compile_expr_first_comprec_input map_le_def apply(force simp: nth_append)
        apply(force simp:last_conv_nth zip_eq_conv map_le_def)
       apply(force split:prod.splits)
      apply(force split:prod.splits)
     apply(clarsimp simp:last_conv_nth zip_eq_conv split:prod.splits)
     apply(metis (no_types, lifting) compiled_cmd_nonempty fst_conv length_greater_0_conv nth_append)
    apply(clarsimp simp:Let_def last_conv_nth zip_eq_conv restrict_map_def map_le_def split:prod.splits)
    using compile_expr_empty_maintains_comprec compile_expr_first_comprec_input length_greater_0_conv nth_append nth_map
      apply(metis (no_types, hide_lams) compile_expr_empty_maintains_comprec compile_expr_first_comprec_input length_greater_0_conv nth_append nth_map)
   apply(simp add:last_conv_nth zip_eq_conv split:prod.splits split del:if_split)
   apply(clarsimp simp:Let_def nth_append split del:if_split)
   apply clarsimp
   apply(rule conjI)
    using compile_expr_first_comprec_input
    apply(force simp: map_le_def)
   (* contradiction because the expr would only be empty if the input regrec was nonempty *)
   apply(force dest: compile_expr_emptyD)
  by simp

lemma compile_cmd_preserves_finite_regrec:
  "finite (dom (regrec C)) \<Longrightarrow>
   compile_cmd C l nl c = (P, l', nl', C', False) \<Longrightarrow>
   \<not> banned_WhileLang_forms c \<Longrightarrow>
   finite (dom (regrec C'))"
proof (induct c arbitrary:P l' nl' C' C l nl)
  case Assign thus ?case
    by (clarsimp simp:regrec_purge_var_def compile_expr_preserves_finite_regrec Let_def split:prod.splits)
next
  case Skip thus ?case by force
next
  case LockAcq thus ?case by force
next
  case LockRel thus ?case
    by (force simp:regrec_purge_vars_def)
next
  case (Seq c1 c2 P l' nl' C' C l nl)
  from Seq(3-5) show ?case
    apply(clarsimp split:prod.splits)
    apply(rename_tac P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 fail\<^sub>2)
    apply(subgoal_tac "finite (dom (regrec C\<^sub>1))")
     prefer 2
     using Seq(1) apply blast
    using Seq(2) by blast
next
  case If
  from If(3-5) show ?case
    apply(clarsimp split:prod.splits)
    apply(rename_tac x1a x1aa x2a x1b x2b x1c x2c x1d x1e x1f C\<^sub>1 fail\<^sub>1 x1g x1h C\<^sub>2 fail\<^sub>2)
    apply(frule compile_expr_preserves_finite_regrec)
     apply force
    apply(subgoal_tac "finite (dom (regrec C\<^sub>1))")
     prefer 2
     using If(1) apply blast
    apply(subgoal_tac "finite (dom (regrec C\<^sub>2))")
     prefer 2
     using If(2) apply blast
    by blast
next
  case While thus ?case by (force split:prod.splits)
next
  case Stop thus ?case by force
qed

subsection {* Compilation properties to do with labels *}

(* Ultimately to establish backward and forward joinability of consecutively compiled commands.
  Skip to lemma 'compiled_cmds_joinable' if immense details of label plumbing are boring. *)

lemma compile_expr_noninput_label:
  "compile_expr C A l e = (P, r, C', False) \<Longrightarrow> l \<noteq> Some l' \<Longrightarrow> RL.res\<^sub>P l' (map fst P) = length P"
  apply(induct e arbitrary:C A l P r C')
    apply(force simp:Let_def RL.res\<^sub>P_def RL.res\<^sub>P'_def Option.bind_def List.extract_def split:option.splits)
   apply(force simp:Let_def RL.res\<^sub>P_def RL.res\<^sub>P'_def Option.bind_def List.extract_def split:option.splits if_splits)
  apply clarsimp
  apply(clarsimp split:prod.splits)
  apply(rename_tac x1a e1 e2 C A l r P\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 r' C\<^sub>2 fail\<^sub>2)
  apply(subgoal_tac "RL.res\<^sub>P l' (map fst P\<^sub>1) = length (map fst P\<^sub>1)")
   prefer 2
   apply force
  apply(drule_tac tail="map fst P\<^sub>2" in RL.res\<^sub>P_exit_append)
   apply force
  apply(subgoal_tac "RL.res\<^sub>P l' (map fst P\<^sub>2) = length (map fst P\<^sub>2)")
   prefer 2
   apply force
  apply(drule_tac tail="[(None, Op x1a r r')]" in RL.res\<^sub>P_exit_append)
   apply force
  apply(subgoal_tac "\<forall> l''. l'' \<noteq> Some l' \<longrightarrow> RL.res\<^sub>P l' [(l'', Op x1a r r')] = Suc 0")
   prefer 2
   apply(force simp:RL.res\<^sub>P_def RL.res\<^sub>P'_def Option.bind_def List.extract_def)
  apply(rule conjI)
   apply blast
  by force

lemma compile_expr_input_label:
  "compile_expr C A l e = (apm, r, C', False) \<Longrightarrow> apm \<noteq> [] \<Longrightarrow> fst (map fst apm ! 0) = l"
  apply(induct e arbitrary:C A l apm r C')
    apply(force simp:Let_def split:option.splits)
   apply(force simp:Let_def reg_alloc_cached_correct split:option.splits)
  by (clarsimp simp:Let_def nth_append split:prod.splits if_splits)

lemma compile_expr_input_label_res\<^sub>P:
  "compile_expr C A (Some l) e = (apm, r, C', False) \<Longrightarrow> apm \<noteq> [] \<Longrightarrow> RL.res\<^sub>P l (map fst apm) = 0"
  using compile_expr_input_label RL.res\<^sub>P_misses by blast

(* compile_cmd must always ensure if given an input label to apply it to its first instruction *)
lemma compile_cmd_input_label:
  "\<not> banned_WhileLang_forms c \<Longrightarrow>
   compile_cmd C l nl c = (P, l', nl', C', False) \<Longrightarrow>
   l \<noteq> None \<Longrightarrow>
   fst (map fst P ! 0) = l"
  apply(induct c arbitrary:C l nl P l' nl' C')
         apply(force dest:compile_expr_input_label simp:Let_def nth_append split:prod.splits if_splits)
        apply force
       apply(force split:prod.splits)
      apply(force split:prod.splits)
     apply(clarsimp simp:nth_append split:prod.splits)
     apply(metis (no_types, hide_lams) compiled_cmd_nonempty fst_conv length_greater_0_conv nth_map)
    apply(clarsimp simp:Let_def nth_append split:prod.splits)
    using compile_expr_input_label apply force
   apply(clarsimp simp:nth_append split:prod.splits)
   apply(rule conjI)
    using compile_expr_input_label compile_expr_correctness compile_expr_empty_maintains_comprec
    apply(metis (mono_tags, lifting) length_greater_0_conv nth_map)
   apply(force dest:compile_expr_emptyD)
  by force

lemma compile_cmd_input_label_res\<^sub>P:
  "\<not> banned_WhileLang_forms c \<Longrightarrow>
   compile_cmd C (Some l) nl c = (P, l', nl', C', False) \<Longrightarrow>
   RL.res\<^sub>P l (map fst P) = 0"
  using compile_cmd_input_label RL.res\<^sub>P_misses by blast

(* compile_cmd must ensure that all its oob jumps target the label it outputs at the end *)
lemma compile_cmd_output_label:
  "\<not> banned_WhileLang_forms c \<Longrightarrow>
   compile_cmd C l nl c = (P, l', nl', C', False) \<Longrightarrow> \<forall>l \<in> RL.oob_jump_labels (map fst P). Some l = l'"
  unfolding RL.oob_jump_labels_def
  unfolding RL.jump_labels_def
  apply clarsimp
  apply(rename_tac l'' pc)
  apply(induct c arbitrary:C l nl P l' nl' C')
         (* Assign shouldn't have any jump labels because none of its instructions should jump *)
         apply(clarsimp simp:Let_def split:prod.splits)
         apply(rename_tac head r C'' fail)
         apply(case_tac "pc < length head")
          using compile_expr_no_jumps
          apply(force simp add:RL.jump_labels_def nth_append)
         apply(subgoal_tac "pc = length head")
          apply force
         apply force
        (* Skip *)
        apply fastforce
       (* LockAcq *)
       apply(force split:prod.splits)
      (* LockRel *)
      apply(force split:prod.splits)
     (* Seq *)
     apply(clarsimp simp add: nth_append split:prod.splits)
     apply(rename_tac P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 fail\<^sub>2)
     apply(subgoal_tac "RL.res\<^sub>P l'' (map fst P\<^sub>1) = length P\<^sub>1")
      prefer 2
      apply(metis (no_types, lifting) RL.exit_res\<^sub>P length_append length_map nth_append RL.res\<^sub>P_misses trans_less_add1)
     apply(subgoal_tac "RL.res\<^sub>P l'' (map fst P\<^sub>2) = length P\<^sub>2")
      prefer 2
      apply(simp add: RL.res\<^sub>P_exit_append)
     apply(split if_splits)
      (* pc in P\<^sub>1 *)
      (* contradiction because compile_cmd should've applied l'' to the beginning of P\<^sub>2 *)
      using compile_cmd_input_label compiled_cmd_nonempty
      apply(metis (no_types, lifting) fst_conv length_greater_0_conv option.distinct(1) RL.res\<^sub>P_misses)
     (* pc in P\<^sub>2 *)
     apply(metis add.commute less_diff_conv2 not_less)
    (* If *)
    apply(rename_tac b c1 c2 l'' pc C l nl P l' nl' C')
    apply(clarsimp simp:Let_def split:prod.splits)
    apply(rename_tac nl\<^sub>2 P\<^sub>0 r C\<^sub>1 fail\<^sub>e br nl' ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 fail\<^sub>1 P\<^sub>2 l\<^sub>2 C\<^sub>3 fail\<^sub>2)
    (* This result for P\<^sub>1 ... *)
    apply(subgoal_tac "RL.res\<^sub>P l'' (map fst P\<^sub>1) = length P\<^sub>1")
     prefer 2
     apply(drule RL.res\<^sub>P_misses)
     apply(subgoal_tac "\<forall>n<length P\<^sub>1. fst (map fst P\<^sub>1 ! n) \<noteq> Some l''")
      prefer 2
      apply clarsimp
      apply(erule_tac x="Suc n + length P\<^sub>0" in allE)
      apply clarsimp
      apply(clarsimp simp add: nth_append)
     apply(clarsimp simp add: nth_append)
     apply(rule RL.exit_res\<^sub>P)
      apply simp
     apply simp
    (* ... and this same one for P\<^sub>2 ... *)
    apply(subgoal_tac "RL.res\<^sub>P l'' (map fst P\<^sub>2) = length P\<^sub>2")
     prefer 2
     apply(drule RL.res\<^sub>P_misses)
     apply(subgoal_tac "\<forall>n<length P\<^sub>2. fst (map fst P\<^sub>2 ! n) \<noteq> Some l''")
      prefer 2
      apply clarsimp
      apply(erule_tac x="Suc (Suc n) + length P\<^sub>0 + length P\<^sub>1" in allE)
      apply clarsimp
      apply(clarsimp simp add: nth_append)
     apply(clarsimp simp add: nth_append)
     apply(rule RL.exit_res\<^sub>P)
      apply simp
     apply simp
     (* ... could probably go away with a helper carrying RL.res\<^sub>P_misses for sublists of P. *)
    (* One case at a time: pc in P\<^sub>0 *)
    apply(case_tac "pc < length P\<^sub>0")
     (* P\<^sub>0 shouldn't have any jump labels because none of its instructions should jump *)
     using compile_expr_no_jumps
     apply(force simp add:RL.jump_labels_def nth_append)
    (* pc at the Jz to br *)
    apply(case_tac "pc = length P\<^sub>0")
     (* I think this relies on RL.fresh_label ensuring that br and ex are unique *)
     apply(clarsimp simp:RL.fresh_label_def)
     apply(metis compile_cmd_input_label compiled_cmd_nonempty fst_conv length_greater_0_conv option.distinct(1) RL.res\<^sub>P_misses)
    (* pc in P\<^sub>1 *)
    apply(case_tac "pc < Suc (length P\<^sub>0 + length P\<^sub>1)")
     apply(subgoal_tac "pc - Suc (length P\<^sub>0) < length P\<^sub>1")
      prefer 2
      apply linarith
     apply clarsimp
     (* expect a contradiction because compile_cmd should apply l'' to the Jmp ex *)
     apply(drule RL.res\<^sub>P_misses)
     apply(erule_tac x="Suc (length P\<^sub>0 + length P\<^sub>1)" in allE)
     apply clarsimp
     apply(fastforce simp:nth_append)
    (* pc at the Jmp *)
    apply(case_tac "pc = Suc (length P\<^sub>0 + length P\<^sub>1)")
     apply(simp add:nth_append)
    apply clarsimp
    (* pc in P\<^sub>2 *)
    apply(case_tac "pc < Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2))")
     apply clarsimp
     apply(subgoal_tac "pc - Suc (length P\<^sub>0) \<ge> length P\<^sub>1")
      prefer 2
      apply linarith
     apply(subgoal_tac "pc - Suc (Suc (length P\<^sub>0 + length P\<^sub>1)) < length P\<^sub>2")
      prefer 2
      apply linarith
     apply(clarsimp simp add: nth_append)
     (* expect a contradiction because compile_cmd should apply l'' to the Nop *)
     apply(drule RL.res\<^sub>P_misses)
     apply(erule_tac x="Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2))" in allE)
     apply(simp add: nth_append)
    (* pc at the Nop *)
    apply clarsimp
    apply(subgoal_tac "pc = Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2))")
     prefer 2
     apply clarsimp
    apply(clarsimp simp:nth_append)
   (* While *)
   apply(rename_tac b c l'' pc C l nl P l' nl''' C')
   apply(clarsimp split:prod.splits)
   apply(rename_tac l nl nl''' lp nl' ex nl'' P\<^sub>e r C\<^sub>B fail\<^sub>e P l' C\<^sub>E fail\<^sub>c)
   apply(case_tac "pc < length P\<^sub>e")
    using compile_expr_no_jumps
    apply(force simp add:RL.jump_labels_def nth_append)
   apply(case_tac "pc = length P\<^sub>e")
    apply force
   apply(case_tac "pc < Suc (length P\<^sub>e + length P)")
    apply(subgoal_tac "pc - Suc (length P\<^sub>e) < length P")
     prefer 2
     apply linarith
    apply(clarsimp simp add:nth_append)
    apply(erule_tac x=l'' in meta_allE)
    apply(erule_tac x="pc - Suc (length P\<^sub>e)" in meta_allE)
    apply(erule_tac x=C\<^sub>B in meta_allE)
    apply(erule_tac x=None in meta_allE)
    apply(erule_tac x=nl'' in meta_allE)
    apply(erule_tac x=P in meta_allE)
    apply(erule_tac x=l' in meta_allE)
    apply(erule_tac x=nl''' in meta_allE)
    apply(erule_tac x=C\<^sub>E in meta_allE)
    apply clarsimp
    (* NB: another instance of sublist miss - deduplication may be possible here *)
    apply(subgoal_tac "RL.res\<^sub>P l'' (map fst P) = length P")
     prefer 2
     apply(drule RL.res\<^sub>P_misses)
     apply(subgoal_tac "\<forall>n<length P. fst (map fst P ! n) \<noteq> Some l''")
      prefer 2
      apply clarsimp
      apply(erule_tac x="(Suc n) + length P\<^sub>e" in allE)
      apply clarsimp
      apply(clarsimp simp add: nth_append)
     apply(clarsimp simp add: nth_append)
     apply(rule RL.exit_res\<^sub>P)
      apply simp
     apply simp
    apply(drule RL.res\<^sub>P_misses)
    apply(erule_tac x="Suc (length P\<^sub>e + length P)" in allE)
    apply(force simp add: nth_append)
   apply clarsimp
   apply(case_tac "pc < Suc (length P\<^sub>e + length P)")
    apply force
   apply(subgoal_tac "pc = Suc (length P\<^sub>e + length P)")
    prefer 2
    apply force
   apply(clarsimp simp add: nth_append)
   apply(subgoal_tac "length P\<^sub>e < Suc (Suc (length P\<^sub>e + length P))")
    prefer 2
    apply linarith
   apply(subgoal_tac "fst (((map fst P\<^sub>e @ [(None, Jz ex r)]) @ map fst P @ [(l', Jmp l'')]) ! length P\<^sub>e) \<noteq> Some l''")
    prefer 2
    apply(metis (no_types) WL.app_Cons_rewrite RL.res\<^sub>P_misses)
   apply(subgoal_tac "map fst P\<^sub>e \<noteq> []")
    prefer 2
    apply(force dest:compile_expr_emptyD)
   apply(simp add:compile_expr_input_label_res\<^sub>P RL.res\<^sub>P_ib_append)
  by force

(* The following "labelrec" properties are specific to our ever-rising label allocation scheme.
  All I'm getting at here is that all the newly used labels should be fresh, so that we can say
  that a run of the compiler for a given block doesn't produce jumps back into previous blocks. *)

lemma compile_cmd_output_labelrec:
  "\<not> banned_WhileLang_forms c \<Longrightarrow>
   compile_cmd C l nl c = (P, l', nl', C', fail) \<Longrightarrow>
   nl' \<ge> nl"
  apply(induct c arbitrary: C l nl P l' nl' C' fail)
        apply(clarsimp simp:Let_def split:prod.splits)+
     using order_trans apply blast
    apply(clarsimp simp:Let_def RL.fresh_label_def split:prod.splits)
    apply(meson Suc_leD order_trans)
   apply(clarsimp simp:Let_def RL.fresh_label_def split:prod.splits if_splits)
    apply(meson Suc_leD order_trans)
   apply(meson Suc_leD order_trans)
  by (clarsimp simp:Let_def split:prod.splits)

lemma compile_expr_only_input_label:
  "compile_expr C A lo e = (P, r, C', fail) \<Longrightarrow>
   l \<in> RL.present_labels (map fst P) \<Longrightarrow> Some l = lo"
  apply(induct e arbitrary:C A lo P r C' fail l)
    apply(clarsimp simp:RL.present_labels_def Let_def split:option.splits)
   apply(clarsimp simp:RL.present_labels_def Let_def split:if_splits option.splits)
  apply(clarsimp simp:Let_def split:prod.splits)
  apply(clarsimp simp: RL.present_labels_append)
  apply(clarsimp split:if_splits)
    apply(clarsimp simp:RL.present_labels_def)
   apply(clarsimp simp:RL.present_labels_def2)
   apply(metis One_nat_def Suc_eq_plus1 fst_conv list.size(4) nth_Cons_0 option.discI RL.res\<^sub>P_hits zero_less_Suc)
  apply(clarsimp simp:RL.present_labels_def2)
  apply(metis One_nat_def Suc_eq_plus1 fst_conv list.size(4) nth_Cons_0 option.discI RL.res\<^sub>P_hits zero_less_Suc)
  done

lemma compile_cmd_maintains_labelrec_freshness:
  "\<not> banned_WhileLang_forms c \<Longrightarrow>
   compile_cmd C l nl c = (P, l', nl', C', fail) \<Longrightarrow>
   \<forall> x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>  
   \<forall> x. l' = Some x \<longrightarrow> x < nl'"
  apply(induct c arbitrary: C l nl P l' nl' C' fail)
         apply(force simp:Let_def split:prod.splits if_splits)
        apply force
       apply(force split:prod.splits)
      apply(force split:prod.splits)
     apply(force split:prod.splits)
    apply(clarsimp simp:Let_def split:prod.splits)
    apply(metis (no_types, hide_lams) compile_cmd_output_labelrec RL.fresh_label_def fst_conv le_imp_less_Suc less_le_trans linear snd_conv)
   apply(clarsimp split:prod.splits)
   using compile_cmd_output_labelrec RL.fresh_label_def apply fastforce
  by clarsimp

lemma compile_cmd_exit_label_fresh:
  "\<not> banned_WhileLang_forms c \<Longrightarrow>
   compile_cmd C l nl c = (P, Some l', nl', C', fail) \<Longrightarrow> l' \<ge> nl"
  apply(induct c arbitrary: C l nl P l' nl' C' fail)
         apply(force simp:Let_def split:prod.splits)
        apply force
       apply(force split:prod.splits)
      apply(force split:prod.splits)
     apply(clarsimp split:prod.splits)
     apply(meson compile_cmd_output_labelrec order_trans)
    apply(clarsimp simp:Let_def RL.fresh_label_def split:prod.splits)
   apply(clarsimp simp:Let_def RL.fresh_label_def split:prod.splits if_splits)
  by clarsimp

lemma cons_append_form:
  "a @ b # c = a @ [b] @ c"
  apply simp
  done

lemma compile_cmd_present_label_upper_bound:
  "\<not> banned_WhileLang_forms c \<Longrightarrow>
   compile_cmd C l nl c = (P, l', nl', C', fail) \<Longrightarrow>
   \<forall> x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   (\<forall> l \<in> RL.present_labels (map fst P). l < nl')"
  apply(induct c arbitrary: C l nl P l' nl' C' fail)
         apply(clarsimp simp:Let_def split:prod.splits if_splits)
            apply(force simp:RL.present_labels_def)
           apply(clarsimp simp:RL.present_labels_append)
           apply(force dest:compile_expr_only_input_label simp:RL.present_labels_def)
          apply(force simp:RL.present_labels_def)
         apply(clarsimp simp:RL.present_labels_append)
         apply(force dest:compile_expr_only_input_label simp:RL.present_labels_def)
        apply(force simp:RL.present_labels_def)
       apply(force simp:RL.present_labels_def split:prod.splits)
      apply(force simp:RL.present_labels_def split:prod.splits)
     (* Seq *)
     apply(clarsimp split:prod.splits)
     apply(clarsimp simp:RL.present_labels_append)
     apply(erule disjE)
      apply(meson compile_cmd_output_labelrec less_le_trans)
     apply(rename_tac c1 c2 C l nl l' nl' C' x1 l\<^sub>1 nl\<^sub>1 x2b fail x1c fail' la)
     apply(subgoal_tac "\<forall>x. l\<^sub>1 = Some x \<longrightarrow> x < nl\<^sub>1")
      prefer 2
      apply(simp add:compile_cmd_maintains_labelrec_freshness)
     apply(meson compile_cmd_output_labelrec less_le_trans)
    (* If *)
    apply(clarsimp simp:Let_def split:prod.splits)
    apply(clarsimp simp:RL.present_labels_append)
    apply(erule disjE)
     apply(metis (no_types, lifting) Suc_leD compile_cmd_output_labelrec compile_expr_only_input_label dual_order.trans RL.fresh_label_def leD not_le_imp_less snd_conv)
    apply(rename_tac x1 c1 c2 C l nl nl' P\<^sub>e x1b fail\<^sub>e x2a l\<^sub>2 x2b x1d nl\<^sub>1 P\<^sub>1 x1f nl\<^sub>2 x2f fail P\<^sub>2 x1i x2i fail' la)
    (* Yuck. not sure of a nicer way to get #s into []@ form, and
       RL.present_labels_cons turned out to be useless *)
    apply(subgoal_tac "la \<in> RL.present_labels
              ([(if P\<^sub>e = [] then l else None, Jz l\<^sub>2 x1b)] @
               map fst P\<^sub>1 @ [(x1f, Jmp x1d)] @ map fst P\<^sub>2 @ [(x1i, Tau)])")
     prefer 2
     apply clarsimp
    apply(thin_tac "la \<in> RL.present_labels
              ((if P\<^sub>e = [] then l else None, Jz l\<^sub>2 x1b) #
               map fst P\<^sub>1 @ (x1f, Jmp x1d) # map fst P\<^sub>2 @ [(x1i, Tau)])")
    apply(clarsimp simp only:RL.present_labels_append)
    apply clarsimp
    apply(erule disjE)
     apply(clarsimp simp:RL.present_labels_def RL.fresh_label_def split:if_splits)
     apply(metis (no_types, hide_lams) Suc_leD compile_cmd_output_labelrec less_le_trans)
    apply(subgoal_tac "\<forall>x. None = Some x \<longrightarrow> x < nl\<^sub>1")
     prefer 2
     apply clarsimp
    apply(erule disjE)
     apply(meson compile_cmd_output_labelrec less_le_trans)
    apply(erule disjE)
     apply(clarsimp simp:RL.present_labels_def RL.fresh_label_def split:if_splits)
     apply(metis (no_types, lifting) compile_cmd_maintains_labelrec_freshness compile_cmd_output_labelrec less_le_trans option.discI)
    apply(subgoal_tac "\<forall>x. Some l\<^sub>2 = Some x \<longrightarrow> x < nl\<^sub>2")
     prefer 2
     apply(metis Pair_inject compile_cmd_output_labelrec RL.fresh_label_def less_eq_Suc_le less_or_eq_imp_le option.inject)
    apply(erule disjE)
     apply(meson compile_cmd_output_labelrec less_le_trans)
    apply(clarsimp simp:RL.present_labels_def)
    apply(simp add: compile_cmd_maintains_labelrec_freshness)
   (* While *)
   apply(clarsimp split:prod.splits)
   apply(simp only:cons_append_form)
   apply(clarsimp simp only:RL.present_labels_append)
   apply clarsimp
   apply(erule disjE)
    apply(metis (no_types, lifting) Suc_leD Suc_le_lessD compile_cmd_output_labelrec compile_expr_only_input_label dual_order.strict_trans1 RL.fresh_label_def fst_conv option.collapse option.sel snd_conv)
   apply(erule disjE)
    apply(force simp:RL.present_labels_def RL.fresh_label_def)
   apply(erule disjE)
    apply force
   apply(erule disjE)
    apply(force simp:RL.present_labels_def compile_cmd_maintains_labelrec_freshness)
   apply(force simp:RL.present_labels_def)
   by force

lemma compile_cmd_output_label_is_jumped_to:
  "\<not> banned_WhileLang_forms c \<Longrightarrow>
   compile_cmd C l nl c = (P, l', nl', C', fail) \<Longrightarrow>
   l' = Some l'' \<Longrightarrow> l'' \<in> RL.jump_labels (map fst P)"
  apply(induct c arbitrary: C l nl P l' nl' C' fail l'')
        apply(clarsimp simp:Let_def RL.jump_labels_def nth_append split:prod.splits if_splits)
       apply(clarsimp simp:RL.jump_labels_def split:prod.splits)+
     apply(clarsimp simp:nth_append split:prod.splits if_splits)
     apply fastforce
    apply(clarsimp simp:Let_def RL.jump_labels_def nth_append split:prod.splits if_splits)
     apply(metis Suc_leI le_Suc_eq le_add1 le_imp_less_Suc length_map nth_Cons_Suc nth_append_length snd_conv)
    using compile_expr_no_jumps
    apply(clarsimp simp:RL.jump_labels_def)
    apply(subgoal_tac "\<And>n na. \<not> (n::nat) + na < n")
     prefer 2
     apply simp
    apply(subgoal_tac "\<And>p ps. length ((p::nat option \<times> ('Reg, 'Var, 'Lock, 'Val, 'OpId) InstB) # ps) = Suc (length ps)")
     prefer 2
     apply(metis One_nat_def Suc_eq_plus1 list.size(4))
    apply(metis (no_types) Suc_le_eq add_Suc_right add_diff_cancel_left' append.simps(2) diff_Suc_1 length_map nat_add_left_cancel_less not_le_imp_less nth_append_length snd_conv)
   using compile_expr_no_jumps
   apply(clarsimp simp:RL.jump_labels_def)
   apply(clarsimp simp:Let_def RL.jump_labels_def nth_append split:prod.splits if_splits)
  by auto

(* "Pen-and-paper" argument:
  - jump labels can only be (1) present labels, or (2) no more than one exit label
  joinable_forward says that all exit labels of the LHS block are the entry label of the RHS block
  RL.joinable_backward says that no jump labels of the RHS block are present in the LHS block
  - no exit label of a block is present in that block
  - the input nl of a block is the output nl of the previous block
  - the output nl of a block is greater than all present labels in that block

  Jump labels of block 2:
    present labels
      entry label
      - The entry label of a block is the exit label of the previous block
        Therefore it is not present in the previous block
      the rest
      - apart from the entry label, the labels present in a block \<ge> input nl
        Therefore they are not in the previous block
    exit label
    - the exit label of a block \<ge> block 2's input nl = block 1's output nl
      Therefore they are not in the previous block

  Or, as it were, better to case split the block 2 jump labels into:
    - entry label, which is the exit label of block 1
    - the rest, which are fresh by virtue of being \<ge> block 2's input nl
*)

lemma compile_cmd_jump_labels_are_entry_or_fresh:
  "\<not> banned_WhileLang_forms c \<Longrightarrow>
   compile_cmd C l nl c = (P, l', nl', C', fail) \<Longrightarrow>
   \<forall> l'' \<in> RL.jump_labels (map fst P). l = Some l'' \<or> l'' \<ge> nl"
  apply(induct c arbitrary: C l nl P l' nl' C' fail)
         using compile_expr_no_jumps
         apply(force simp:Let_def RL.jump_labels_def nth_append split:prod.splits if_splits)
        apply(force simp:RL.jump_labels_def)
       apply(force simp:RL.jump_labels_def split:prod.splits)
      apply(force simp:RL.jump_labels_def split:prod.splits)
     (* Seq *)
     apply(clarsimp simp:nth_append split:prod.splits if_splits)
     apply(clarsimp simp:RL.jump_labels_append)
     apply(rename_tac c1 c2 C l nl l' nl' C' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 fail\<^sub>2 l'')
     apply(erule disjE)
      apply(clarsimp simp:RL.jump_labels_def)
      apply(metis (mono_tags, lifting) nth_map)
     apply(subgoal_tac "nl \<le> l''")
      prefer 2
      apply(thin_tac "\<not> nl \<le> l''")
      apply(subgoal_tac "l\<^sub>1 = Some l'' \<or> nl\<^sub>1 \<le> l''")
       prefer 2
       apply blast
      apply(erule disjE)
       apply clarsimp
       (* l'' is the exit label of P\<^sub>1 *)
       using compile_cmd_exit_label_fresh
       apply simp
      apply(subgoal_tac "nl \<le> nl\<^sub>1")
       prefer 2
       using compile_cmd_output_labelrec apply clarsimp
      apply simp
     apply simp
    (* If *)
    apply(clarsimp simp:Let_def RL.fresh_label_def split:prod.splits)
    apply(rename_tac e c1 c2 C l nl nl' P r C' fail\<^sub>e P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 l\<^sub>2 C\<^sub>2 fail\<^sub>2 l'')
    apply(subgoal_tac "l'' \<in> RL.jump_labels
               (map fst P @
                [(if P = [] then l else None, Jz nl r)] @
                map fst P\<^sub>1 @ [(l\<^sub>1, Jmp (Suc nl))] @ map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])")
     prefer 2
     apply clarsimp
    apply(thin_tac "l'' \<in> RL.jump_labels
               (map fst P @
                (if P = [] then l else None, Jz nl r) #
                map fst P\<^sub>1 @ (l\<^sub>1, Jmp (Suc nl)) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])")
    apply(clarsimp simp only:RL.jump_labels_append)
    apply clarsimp
    apply(erule disjE)
     using compile_expr_no_jumps apply simp
    apply(erule disjE)
     apply(clarsimp simp:RL.jump_labels_def)
    apply(erule disjE)
     apply(clarsimp simp:RL.jump_labels_def)
     apply(metis (mono_tags, hide_lams) le_Suc_eq not_less_eq_eq nth_map option.discI)
    apply(erule disjE)
     apply(clarsimp simp:RL.jump_labels_def)
    apply(subgoal_tac "nl \<le> nl\<^sub>1")
     prefer 2
     apply(metis (no_types) Suc_eq_plus1 compile_cmd_output_labelrec le_add1 order.trans)
    apply(erule disjE)
     apply(clarsimp simp:RL.jump_labels_def)
     apply fastforce
    apply(clarsimp simp:RL.jump_labels_def)
   (* While *)
   apply(clarsimp simp:RL.fresh_label_def split:prod.splits)
   apply(simp only:cons_append_form)
   apply(clarsimp simp only:RL.jump_labels_append)
   apply clarsimp
   apply(erule disjE)
    using compile_expr_no_jumps apply force
   apply(erule disjE)
    apply(clarsimp simp:RL.jump_labels_def)
    apply(metis Suc_leD RL.fresh_label_def less_or_eq_imp_le snd_conv)
   apply(erule disjE)
    apply(clarsimp simp:RL.jump_labels_def)
    apply(metis (no_types, lifting) Pair_inject Suc_le_lessD RL.fresh_label_def less_or_eq_imp_le nth_map option.discI)
   apply(erule disjE)
    apply(force simp:RL.fresh_label_def RL.jump_labels_def split:if_splits)
   apply(force simp:RL.jump_labels_def)
  (* Stop *)
  apply simp
  done

lemma compile_cmd_present_labels_are_entry_or_fresh:
  "\<not> banned_WhileLang_forms c \<Longrightarrow>
   compile_cmd C l nl c = (P, l', nl', C', False) \<Longrightarrow>
   \<forall> x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   (\<forall> l'' \<in> RL.present_labels (map fst P). l = Some l'' \<or> l'' \<ge> nl)"
  apply(induct c arbitrary: C l nl P l' nl' C')
         apply(clarsimp simp:Let_def split:prod.splits if_splits)
            apply(force simp:RL.present_labels_def)
           apply(clarsimp simp:RL.present_labels_append)
           apply(force dest:compile_expr_only_input_label simp:RL.present_labels_def)           
          apply(force simp:RL.present_labels_def)
         apply(clarsimp simp:RL.present_labels_append)
         apply(force dest:compile_expr_only_input_label simp:RL.present_labels_def)
        apply(force simp:RL.present_labels_def)
       apply(force simp:RL.present_labels_def split:prod.splits)
      apply(force simp:RL.present_labels_def split:prod.splits)
     (* Seq *)
     apply(clarsimp split:prod.splits)
     apply(clarsimp simp:RL.present_labels_append)
     apply(erule disjE)
      apply(meson compile_cmd_output_labelrec less_le_trans)
     apply(rename_tac c1 c2 C l nl l' nl' C' x1 l\<^sub>1 nl\<^sub>1 x2b fail' x1c fail'' l'')
     apply(subgoal_tac "\<forall>x. l\<^sub>1 = Some x \<longrightarrow> x < nl\<^sub>1")
      prefer 2
      apply(simp add:compile_cmd_maintains_labelrec_freshness)
     apply(subgoal_tac "nl \<le> nl\<^sub>1")
      prefer 2
      apply(simp add: compile_cmd_output_labelrec)
     apply(subgoal_tac "nl \<le> l''")
      prefer 2
      apply(thin_tac "\<not> nl \<le> l''")
      apply(subgoal_tac "l\<^sub>1 = Some l'' \<or> nl\<^sub>1 \<le> l''")
       prefer 2
       apply blast
      apply(erule disjE)
       apply clarsimp
       (* l'' is the exit label of P\<^sub>1 *)
       using compile_cmd_exit_label_fresh
       apply simp
      apply(subgoal_tac "nl \<le> nl\<^sub>1")
       prefer 2
       using compile_cmd_output_labelrec apply clarsimp
      apply simp
     apply simp
    (* If *)
    apply(clarsimp simp:Let_def RL.fresh_label_def split:prod.splits)
    apply(rename_tac e c1 c2 C l nl nl' P r C' fail' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 l\<^sub>2 C\<^sub>2 fail\<^sub>2 l'')
    apply(subgoal_tac "l'' \<in> RL.present_labels
               (map fst P @
                [(if P = [] then l else None, Jz nl r)] @
                map fst P\<^sub>1 @ [(l\<^sub>1, Jmp (Suc nl))] @ map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])")
     prefer 2
     apply clarsimp
    apply(thin_tac "l'' \<in> RL.present_labels
               (map fst P @
                (if P = [] then l else None, Jz nl r) #
                map fst P\<^sub>1 @ (l\<^sub>1, Jmp (Suc nl)) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])")
    apply(clarsimp simp only:RL.present_labels_append)
    apply clarsimp
    apply(erule disjE)
     using compile_expr_only_input_label
     apply clarsimp
    apply(erule disjE)
     apply(clarsimp simp:RL.present_labels_def)
     apply(metis option.discI)
    apply(erule disjE)
     apply(clarsimp simp:RL.present_labels_def)
     apply(metis (no_types, hide_lams) Suc_leD nth_map option.discI)
    apply(erule disjE)
     apply(clarsimp simp:RL.present_labels_def)
     apply(meson Suc_leD compile_cmd_exit_label_fresh)
    apply(erule disjE)
     apply(clarsimp simp:RL.present_labels_def)
     apply(subgoal_tac "nl < nl\<^sub>1")
      prefer 2
      apply(metis (no_types) Suc_eq_plus1 compile_cmd_output_labelrec dual_order.strict_trans1 le_add1 lessI)
     apply fastforce
    apply(clarsimp simp:RL.present_labels_def)
    apply(meson Suc_leD compile_cmd_exit_label_fresh compile_cmd_output_labelrec dual_order.trans)
   (* While *)
   apply(clarsimp simp:RL.fresh_label_def split:prod.splits)
   apply(simp only:cons_append_form)
   apply(clarsimp simp only:RL.present_labels_append)
   apply clarsimp
   apply(erule disjE)
    apply(clarsimp simp:RL.present_labels_def split:if_splits)
     using compile_expr_noninput_label
     apply(metis Pair_inject RL.fresh_label_def less_or_eq_imp_le nth_map option.sel RL.res\<^sub>P_misses)
    apply(metis compile_expr_noninput_label nth_map option.sel RL.res\<^sub>P_misses)
   apply(erule disjE)
    apply(force simp:RL.present_labels_def)
   apply(erule disjE)
    apply(metis (no_types, lifting) Pair_inject Suc_le_lessD RL.fresh_label_def less_or_eq_imp_le option.discI)
   apply(erule disjE)
    apply(clarsimp simp:RL.present_labels_def split:if_splits)
     using compile_expr_only_input_label
     apply(metis Pair_inject Suc_le_lessD compile_cmd_exit_label_fresh RL.fresh_label_def less_imp_le_nat)
    apply(metis Suc_le_lessD compile_cmd_exit_label_fresh less_imp_le_nat)
   apply(force simp:RL.present_labels_def)
  (* Stop *)
  apply simp
  done

lemma compile_cmd_exit_label_oob:
  "\<not> banned_WhileLang_forms c \<Longrightarrow>
   compile_cmd C l nl c = (P, Some l', nl', C', False) \<Longrightarrow>
   \<forall> x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   l' \<in> RL.oob_jump_labels (map fst P)"
  apply(induct c arbitrary:C l nl P l' nl' C')
         apply(force split:prod.splits simp:Let_def)
        apply force
       apply(force split:prod.splits)
      apply(force split:prod.splits)
     (* Seq *)
     apply(clarsimp split:prod.splits)
     apply(clarsimp simp:RL.oob_jump_labels_def2)
     apply(rename_tac c1 c2 C l nl l' nl' C' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 fail\<^sub>2)
     apply(subgoal_tac "\<forall>x. l\<^sub>1 = Some x \<longrightarrow> x < nl\<^sub>1 ")
      prefer 2
      using compile_cmd_maintains_labelrec_freshness apply simp
     apply(rule conjI)
      apply(clarsimp simp:RL.jump_labels_append)
     apply clarsimp
     apply(clarsimp simp:RL.present_labels_append)
     apply(subgoal_tac "nl\<^sub>1 \<le> l'")
      prefer 2
      using compile_cmd_exit_label_fresh apply simp
     apply(metis leD compile_cmd_present_label_upper_bound)
    (* If *)
    apply(clarsimp split:prod.splits simp:Let_def RL.fresh_label_def)
    apply(clarsimp simp:RL.oob_jump_labels_def2)
    apply(rename_tac e c1 c2 C l nl nl' P r C' fail' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 l\<^sub>2 C\<^sub>2 fail\<^sub>2)
    apply(rule conjI)
     apply clarsimp
     apply(subgoal_tac "Suc nl \<in> RL.jump_labels ([(l, Jz nl r)] @ map fst P\<^sub>1 @ [(l\<^sub>1, Jmp (Suc nl))] @ map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]) \<and>
        Suc nl \<notin> RL.present_labels ([(l, Jz nl r)] @ map fst P\<^sub>1 @ [(l\<^sub>1, Jmp (Suc nl))] @ map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])")
      apply clarsimp
     apply(clarsimp simp only:RL.jump_labels_append RL.present_labels_append)
     apply clarsimp
     apply(rule conjI)
      apply(clarsimp simp:RL.jump_labels_def)
     apply(rule conjI)
      apply(force simp:RL.present_labels_def)
     apply(rule conjI)
      using compile_cmd_present_labels_are_entry_or_fresh Suc_n_not_le_n option.discI
      apply fastforce
     apply(subgoal_tac "\<forall>x. l\<^sub>1 = Some x \<longrightarrow> x < nl\<^sub>1")
      prefer 2
      using compile_cmd_maintains_labelrec_freshness apply simp
     apply(rule conjI)
      apply(clarsimp simp:RL.present_labels_def)
      apply(meson Suc_n_not_le_n compile_cmd_exit_label_fresh)
     apply(subgoal_tac "(Suc (Suc nl)) \<le> nl\<^sub>1")
      prefer 2
      using compile_cmd_output_labelrec apply clarsimp
     apply(rule conjI)
      using compile_cmd_present_labels_are_entry_or_fresh
      apply(metis (no_types, hide_lams) Suc_le_lessD Suc_lessD not_less_eq_eq option.sel)
     apply(clarsimp simp:RL.present_labels_def)
     apply(meson compile_cmd_exit_label_fresh not_less_eq_eq)
    apply clarsimp
    (* and almost all over again.. *)
    apply(subgoal_tac "Suc nl \<in> RL.jump_labels (map fst P @ [(None, Jz nl r)] @ map fst P\<^sub>1 @ [(l\<^sub>1, Jmp (Suc nl))] @ map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]) \<and>
       Suc nl \<notin> RL.present_labels (map fst P @ [(None, Jz nl r)] @ map fst P\<^sub>1 @ [(l\<^sub>1, Jmp (Suc nl))] @ map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])")
     apply clarsimp
    apply(clarsimp simp only:RL.jump_labels_append RL.present_labels_append)
    apply clarsimp
    apply(rule conjI)
     apply(clarsimp simp:RL.jump_labels_def)
    apply(rule conjI)
     using compile_expr_only_input_label
     apply(metis Suc_eq_plus1 le_add1 le_imp_less_Suc not_less_eq)
    apply(rule conjI)
     apply(clarsimp simp:RL.present_labels_def)
    apply(rule conjI)
     using compile_cmd_present_labels_are_entry_or_fresh Suc_n_not_le_n option.discI
     apply fastforce
    apply(subgoal_tac "\<forall>x. l\<^sub>1 = Some x \<longrightarrow> x < nl\<^sub>1")
     prefer 2
     using compile_cmd_maintains_labelrec_freshness apply simp
    apply(rule conjI)
     apply(clarsimp simp:RL.present_labels_def)
     apply(meson Suc_n_not_le_n compile_cmd_exit_label_fresh)
    apply(subgoal_tac "(Suc (Suc nl)) \<le> nl\<^sub>1")
     prefer 2
     using compile_cmd_output_labelrec apply clarsimp
    apply(rule conjI)
     using compile_cmd_present_labels_are_entry_or_fresh
     apply(metis (no_types, hide_lams) Suc_le_lessD Suc_lessD not_less_eq_eq option.sel)
    apply(clarsimp simp:RL.present_labels_def)
    apply(meson compile_cmd_exit_label_fresh not_less_eq_eq)
   (* While *)
   apply(clarsimp split:prod.splits simp:RL.fresh_label_def RL.oob_jump_labels_def2 split del:if_split)
   apply(simp only:cons_append_form)
   apply(clarsimp simp only:RL.present_labels_append RL.jump_labels_append)
   apply(rule conjI)
    apply(clarsimp split del:if_split)
    apply(force simp:RL.jump_labels_def)
   apply(clarsimp split del:if_split)
   apply(rule conjI)
    apply(clarsimp split:if_splits simp:RL.fresh_label_def RL.present_labels_def)
     apply(metis (no_types, lifting) compile_expr_noninput_label n_not_Suc_n nth_map option.sel RL.res\<^sub>P_misses)
    apply(metis compile_expr_noninput_label nat_neq_iff nth_map option.inject RL.res\<^sub>P_misses)
   apply(rule conjI)
    apply(force split:if_splits simp:RL.fresh_label_def RL.present_labels_def)
   apply(subgoal_tac "(Suc l') \<le> nl'")
    prefer 2
    using compile_cmd_output_labelrec apply force
   apply(rule conjI)
    using compile_cmd_present_labels_are_entry_or_fresh
    apply fastforce
   apply(clarsimp simp:RL.present_labels_def)
   apply(metis Suc_n_not_le_n compile_cmd_jump_labels_are_entry_or_fresh option.simps(3))
  (* Stop *)
  by simp

lemma compiled_cmds_joinable_backward:
  "\<not> (banned_WhileLang_forms c1 \<or> banned_WhileLang_forms c2) \<Longrightarrow>
   compile_cmd C l nl c1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>1, False) \<Longrightarrow>
   \<forall> x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   compile_cmd C\<^sub>1 l\<^sub>1 nl\<^sub>1 c2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>2, False) \<Longrightarrow>
   RL.joinable_backward (map fst P\<^sub>1) (map fst P\<^sub>2)"
  unfolding RL.joinable_backward_def
  apply clarsimp
  apply(rename_tac la)
  apply(subgoal_tac "l\<^sub>1 = Some la \<or> nl\<^sub>1 \<le> la")
   prefer 2
   using compile_cmd_jump_labels_are_entry_or_fresh apply blast
  apply(erule disjE)
   (* jumps to the entry label, which is the previous block's exit label *)
   apply clarsimp
   apply(subgoal_tac "la \<in> RL.oob_jump_labels (map fst P\<^sub>1)")
    prefer 2
    using compile_cmd_exit_label_oob apply blast
   apply(subgoal_tac "nl \<le> la")
    prefer 2
    using compile_cmd_exit_label_fresh apply blast
   apply(simp add: RL.oob_jump_labels_def2)
  (* jumps to any other label, which lie above the upper bound of the previous block's labels *)
  using compile_cmd_present_label_upper_bound leD by blast

lemma compiled_cmds_joinable_forward:
  "\<not> (banned_WhileLang_forms c1 \<or> banned_WhileLang_forms c2) \<Longrightarrow>
   compile_cmd C l nl c1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>1, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 l\<^sub>1 nl\<^sub>1 c2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>2, False) \<Longrightarrow>
   RL.joinable_forward (map fst P\<^sub>1) (map fst P\<^sub>2)"
  using compile_cmd_output_label compile_cmd_input_label
  unfolding RL.joinable_forward_def
  by (clarsimp, metis)

lemma compiled_cmds_joinable:
  "\<not> (banned_WhileLang_forms c1 \<or> banned_WhileLang_forms c2) \<Longrightarrow>
   compile_cmd C l nl c1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>1, False) \<Longrightarrow>
   \<forall> x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   compile_cmd C\<^sub>1 l\<^sub>1 nl\<^sub>1 c2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>2, False) \<Longrightarrow>
   RL.joinable (map fst P\<^sub>1) (map fst P\<^sub>2)"
  unfolding RL.joinable_def
  using compiled_cmds_joinable_forward compiled_cmds_joinable_backward
  by simp

subsection {* Other refinement theory instantiation machinery *}

(* Only Seq or If can evaluate to a Seq statement *)
lemma eval_seq_forms:
  "\<not> (\<exists> head tail. c = head ;; tail) \<Longrightarrow>
   \<not> (\<exists> b t e. c = If b t e) \<Longrightarrow>
   eval_abv\<^sub>A \<langle>c, mds, mem\<rangle>\<^sub>A \<langle>c', mds', mem'\<rangle>\<^sub>A \<Longrightarrow>
   \<not> (\<exists> head' tail'. c' = head' ;; tail')"
  by (case_tac c, auto)

lemma eval\<^sub>r_oob_halt':
  "fst c \<ge> length (snd c) \<Longrightarrow> \<not> \<langle>c, regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>c', regs', mds', mem'\<rangle>\<^sub>r"
  by (metis prod.collapse RL.eval\<^sub>r_oob_pc_halt)

lemma eval\<^sub>r_preserves_P:
  "(\<langle>c, regs, mds, mem\<rangle>\<^sub>r, \<langle>c', regs', mds', mem'\<rangle>\<^sub>r) \<in> RL.eval\<^sub>r \<Longrightarrow> snd c = snd c' \<and> (\<exists> pc. pc = fst c')"
proof -
  assume "\<langle>c, regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>c', regs', mds', mem'\<rangle>\<^sub>r"
  then have "\<langle>(fst c, snd c), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(fst c', snd c'), regs', mds', mem'\<rangle>\<^sub>r"
    by simp
  then show ?thesis
    by (fastforce elim:RL.eval\<^sub>r_elim)
qed

lemma neval_S_n': "n' = n + 1 \<Longrightarrow> WL.eval_abv x y \<Longrightarrow> WL.neval y n z \<Longrightarrow> WL.neval x n' z"
  using WL.neval_S_n
  by clarsimp

lemma neval_append_heavy:
assumes
  "pc < length P"
  "c\<^sub>A \<noteq> Stop"
  "WL.neval \<langle>c\<^sub>A, mds, mem\<rangle>\<^sub>A (abs_steps' c\<^sub>A (pc, P)) \<langle>c\<^sub>A', mds', mem'\<rangle>\<^sub>A"
shows
  "if c\<^sub>A' = Stop \<and> (abs_steps' c\<^sub>A (pc, P)) \<noteq> 0
   then WL.neval \<langle>c\<^sub>A ;; tail\<^sub>A, mds, mem\<rangle>\<^sub>A (abs_steps' (c\<^sub>A ;; tail\<^sub>A) (pc, P @ tail\<^sub>C))
     \<langle>tail\<^sub>A, mds', mem'\<rangle>\<^sub>A
   else WL.neval \<langle>c\<^sub>A ;; tail\<^sub>A, mds, mem\<rangle>\<^sub>A (abs_steps' (c\<^sub>A ;; tail\<^sub>A) (pc, P @ tail\<^sub>C))
     \<langle>c\<^sub>A' ;; tail\<^sub>A, mds', mem'\<rangle>\<^sub>A"
proof -
  from assms
  have "WL.neval \<langle>c\<^sub>A, mds, mem\<rangle>\<^sub>A (abs_steps' (c\<^sub>A ;; tail\<^sub>A) (pc, P @ tail\<^sub>C)) \<langle>c\<^sub>A', mds', mem'\<rangle>\<^sub>A"
    by (force simp:abs_steps'_def compile_expr_output_forms nth_append split:if_splits)
  thus ?thesis
    using assms abs_steps_range
    apply clarsimp
    apply(case_tac "abs_steps' (c\<^sub>A ;; tail\<^sub>A) (pc, P @ tail\<^sub>C) = 0")
     using WL.neval_0 WL.neval_ZeroE prod.inject
     apply(metis Suc_leI Suc_n_not_le_n)
    apply clarsimp
    apply(subgoal_tac "abs_steps' (c\<^sub>A ;; tail\<^sub>A) (pc, P @ tail\<^sub>C) = 1")
     prefer 2
     apply force
    apply clarsimp
    apply(case_tac "c\<^sub>A' = Stop")
     apply(metis WL.neval_ZeroE WL.eval\<^sub>w.seq2 WL.stop_no_eval)
    apply(simp add: assms WL.eval\<^sub>w.seq1)
    done
qed

lemma neval_append:
assumes
  "pc < length P"
  "c\<^sub>A \<noteq> Stop"
  "WL.neval \<langle>c\<^sub>A, mds, mem\<rangle>\<^sub>A (abs_steps' c\<^sub>A (pc, P)) \<langle>c\<^sub>A', mds', mem'\<rangle>\<^sub>A"
shows
  "c\<^sub>A' = Stop \<and> (abs_steps' c\<^sub>A (pc, P)) \<noteq> 0 \<longrightarrow>
   WL.neval \<langle>c\<^sub>A ;; tail\<^sub>A, mds, mem\<rangle>\<^sub>A (abs_steps' (c\<^sub>A ;; tail\<^sub>A) (pc, P @ tail\<^sub>C))
     \<langle>tail\<^sub>A, mds', mem'\<rangle>\<^sub>A"
  using assms neval_append_heavy
  by simp

end
end