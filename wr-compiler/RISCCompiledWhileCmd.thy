(*
Author: Robert Sison
*)

theory RISCCompiledWhileCmd
imports RISCCompiledReachability
begin

context risc_compilation
begin

subsection {* RISC evaluation helpers for compiled While-loop commands *}

lemma while_joinable_backward_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>e @
          (None, Jz ex r) #
          map fst P @ [(l', Jmp lp)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>e @
           (None, Jz ex r) #
           map fst P @ [(l', Jmp lp)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   \<not> banned_WhileLang_forms c \<Longrightarrow>
   (if l = None then RL.fresh_label nl else (the l, nl)) = (lp, ex) \<Longrightarrow>
   compile_expr (C\<lparr>regrec := Map.empty\<rparr>) {} (Some lp) b = (P\<^sub>e, r, C\<^sub>B, False) \<Longrightarrow>
   compile_cmd C\<^sub>B None (Suc ex) c = (P, l', nl''', C\<^sub>E, False) \<Longrightarrow>
   pc > length P\<^sub>e \<Longrightarrow>
   pc < Suc (length P\<^sub>e + length P) \<Longrightarrow>
   pc' < Suc (Suc (length P\<^sub>e + length P)) \<Longrightarrow>
   RL.joinable_backward
    (map fst P\<^sub>e @ [(None, Jz ex r)])
    (map fst P)"
  apply(simp only:RL.joinable_backward_def)
  apply(rule ballI)
  apply(rename_tac jl)
  apply(subgoal_tac "jl \<ge> (Suc ex)")
   prefer 2
   apply(simp only:cons_append_form RL.jump_labels_append)
   using compile_cmd_jump_labels_are_entry_or_fresh apply blast
  apply(rule ccontr)
  apply clarsimp
  apply(frule_tac l=jl in compile_expr_only_input_label)
   apply(simp only:RL.present_labels_append)
   apply(force split:if_splits simp:RL.present_labels_def RL.fresh_label_def)
  by (force split:if_splits simp:RL.fresh_label_def)

lemma while_pc_backward_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>e @
          (None, Jz ex r) #
          map fst P @ [(l', Jmp lp)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>e @
           (None, Jz ex r) #
           map fst P @ [(l', Jmp lp)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   \<not> banned_WhileLang_forms c \<Longrightarrow>
   (if l = None then RL.fresh_label nl else (the l, nl)) = (lp, ex) \<Longrightarrow>
   compile_expr (C\<lparr>regrec := Map.empty\<rparr>) {} (Some lp) b = (P\<^sub>e, r, C\<^sub>B, False) \<Longrightarrow>
   compile_cmd C\<^sub>B None (Suc ex) c = (P, l', nl''', C\<^sub>E, False) \<Longrightarrow>
   pc > length P\<^sub>e \<Longrightarrow>
   pc < Suc (length P\<^sub>e + length P) \<Longrightarrow>
   pc' < Suc (Suc (length P\<^sub>e + length P)) \<Longrightarrow>
   pc' \<ge> Suc (length P\<^sub>e)"
  apply(subgoal_tac "pc - Suc (length P\<^sub>e) \<le> length P")
   prefer 2
   apply force
  (* Clearly the two are not RL.joinable_backward as-is because lp is at the beginning of P\<^sub>1.
     We need to chop off the end of P\<^sub>2 first.
  apply(subgoal_tac "RL.joinable_backward
    (map fst P\<^sub>e @ [(None, Jz ex r)])
    ((map fst P) @ [(l', Jmp lp)])")
   *)
  apply(frule while_joinable_backward_helper, force+)
  apply(frule_tac pc=pc and pc'=pc' and mds=mds\<^sub>C and mem=mem\<^sub>C and mds'=mds\<^sub>C' and mem'=mem\<^sub>C'
    and regs=regs and regs'=regs'
    in RL.eval\<^sub>r_no_backtrack_over_join)
    apply force
   apply(subgoal_tac "Suc (length P\<^sub>e + length P) = length ((map fst P\<^sub>e @ [(None, Jz ex r)]) @ map fst P)")
    prefer 2
    apply force
   apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc, (map fst P\<^sub>e @ [(None, Jz ex r)]) @ map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', (map fst P\<^sub>e @ [(None, Jz ex r)]) @ map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
    prefer 2
    apply(metis (no_types) WL.app_Cons_rewrite append_assoc RL.eval\<^sub>r_head not_le_imp_less not_less_eq)
   apply force
  by force

lemma while_pc_forward_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>e @
          (None, Jz ex r) #
          map fst P @ [(l', Jmp lp)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>e @
           (None, Jz ex r) #
           map fst P @ [(l', Jmp lp)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   \<not> banned_WhileLang_forms c \<Longrightarrow>
   (if l = None then RL.fresh_label nl else (the l, nl)) = (lp, ex) \<Longrightarrow>
   compile_expr (C\<lparr>regrec := Map.empty\<rparr>) {} (Some lp) b = (P\<^sub>e, r, C\<^sub>B, False) \<Longrightarrow>
   compile_cmd C\<^sub>B None (Suc ex) c = (P, l', nl''', C\<^sub>E, False) \<Longrightarrow>
   pc > length P\<^sub>e \<Longrightarrow>
   pc < Suc (length P\<^sub>e + length P) \<Longrightarrow>
   pc' \<le> Suc (length P\<^sub>e + length P)"
  apply(subgoal_tac "RL.joinable_forward (map fst P) [(l', Jmp lp)]")
   prefer 2
   using RL.joinable_forward_def compile_cmd_output_label
   apply fastforce
  apply(frule_tac pc=pc and pc'=pc' and mds=mds\<^sub>C and mem=mem\<^sub>C and mds'=mds\<^sub>C' and mem'=mem\<^sub>C' and
    P\<^sub>0="map fst P\<^sub>e @ [(None, Jz ex r)]" and P="map fst P" and
    tail="[(l', Jmp lp)]"
    in RL.eval\<^sub>r_mid_bounds)
     apply force
    apply force
   apply force
  by force

lemma while_ex_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   \<not> banned_WhileLang_forms c \<Longrightarrow>
   (if l = None then RL.fresh_label nl else (the l, nl)) = (lp, ex) \<Longrightarrow>
   compile_expr (C\<lparr>regrec := Map.empty\<rparr>) {} (Some lp) b = (P\<^sub>e, r, C\<^sub>B, False) \<Longrightarrow>
   compile_cmd C\<^sub>B None (Suc ex) c = (P, l', nl''', C\<^sub>E, False) \<Longrightarrow>
   RL.res\<^sub>P ex
    (map fst P\<^sub>e @ (None, Jz ex r) # map fst P @ [(l', Jmp lp)]) =
   Suc (Suc (length P\<^sub>e + length P))"
  apply(subgoal_tac "RL.res\<^sub>P ex (map fst P\<^sub>e) = length P\<^sub>e")
   prefer 2
   apply(metis compile_expr_noninput_label RL.fresh_label_def fst_conv leD n_not_Suc_n nat_le_linear option.collapse option.inject snd_conv)
  apply(subgoal_tac "RL.res\<^sub>P ex (map fst P) = length P")
   prefer 2
   apply(frule compile_cmd_present_labels_are_entry_or_fresh[simplified,rotated])
     apply force
    apply force
   apply(clarsimp simp:RL.present_labels_def2)
   apply(metis Suc_n_not_le_n length_map RL.res\<^sub>P_oob)
  apply(subgoal_tac "RL.res\<^sub>P ex [(None, Jz ex r)] = Suc 0 \<and>
    RL.res\<^sub>P ex [(l', Jmp lp)] = Suc 0")
   prefer 2
   apply(clarsimp simp:RL.res\<^sub>P_def RL.res\<^sub>P'_def List.extract_def)
   using Suc_n_not_le_n compile_cmd_exit_label_fresh compile_expr_emptyD
   apply blast
  apply(drule_tac l=ex and tail="[(None, Jz ex r)]" in RL.res\<^sub>P_exit_append)
   apply force
  apply(drule_tac l=ex and P="map fst P\<^sub>e @ [(None, Jz ex r)]" and tail="map fst P" in RL.res\<^sub>P_exit_append)
   apply force
  apply(drule_tac P="(map fst P\<^sub>e @ [(None, Jz ex r)]) @ map fst P" and tail="[(l', Jmp lp)]" in RL.res\<^sub>P_exit_append)
   apply force
  by force

lemma while_pc_eval_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>e @
          (None, Jz ex r) #
          map fst P @ [(l', Jmp lp)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>e @
           (None, Jz ex r) #
           map fst P @ [(l', Jmp lp)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   (if l = None then RL.fresh_label nl else (the l, nl)) = (lp, ex) \<Longrightarrow>
   compile_expr (C\<lparr>regrec := Map.empty\<rparr>) {} (Some lp) b = (P\<^sub>e, r, C\<^sub>B, False) \<Longrightarrow>
   compile_cmd C\<^sub>B None (Suc ex) c = (P, l', nl''', C\<^sub>E, False) \<Longrightarrow>
   pc > length P\<^sub>e \<Longrightarrow>
   pc < Suc (length P\<^sub>e + length P) \<Longrightarrow>
   pc' \<ge> Suc (length P\<^sub>e) \<Longrightarrow>
   pc' \<le> Suc (length P\<^sub>e + length P) \<Longrightarrow>
   eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc' - Suc (length P\<^sub>e), map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C"
  apply(rule_tac P\<^sub>1="map fst P\<^sub>e @ [(None, Jz ex r)]" in RL.eval\<^sub>r_join_right)
     apply force
    apply force
   apply force
  by (force intro:RL.eval\<^sub>r_head)

end
end