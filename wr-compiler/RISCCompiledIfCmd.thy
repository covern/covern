(*
Author: Robert Sison
*)

theory RISCCompiledIfCmd
imports RISCCompiledReachability
begin

context risc_compilation
begin

subsection {* RISC evaluation helpers for compiled If-conditional commands *}

lemma if_c1_joinable_backward_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>0 @
          (if P\<^sub>0 = [] then l else None, Jz br r) #
          map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>1 \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>2 \<Longrightarrow>
   compile_expr C {} l b = (P\<^sub>0, r, C\<^sub>1, False) \<Longrightarrow>
   RL.fresh_label nl = (br, nl') \<Longrightarrow>
   RL.fresh_label nl' = (ex, nl'') \<Longrightarrow>
   compile_cmd C\<^sub>1 None nl'' c\<^sub>1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) \<Longrightarrow>
   pc > length P\<^sub>0 \<Longrightarrow>
   pc \<le> Suc (length P\<^sub>0 + length P\<^sub>1) \<Longrightarrow>
   RL.joinable_backward
    (map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)])
    ((map fst P\<^sub>1) @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])"
  apply(simp only:RL.joinable_backward_def)
  apply(rule ballI)
  apply(rename_tac jl)
  apply(subgoal_tac "jl \<ge> nl \<and> jl \<ge> br")
   prefer 2
   apply(simp only:cons_append_form RL.jump_labels_append)
   apply clarsimp
   apply(erule disjE)
    using compile_cmd_jump_labels_are_entry_or_fresh
    apply(metis Suc_le_lessD RL.fresh_label_def fst_conv less_imp_le_nat option.discI snd_conv)
   apply(erule disjE)
    apply(force simp:RL.fresh_label_def RL.jump_labels_def)
   apply(erule disjE)
    apply(subgoal_tac "nl\<^sub>1 \<ge> nl''")
     prefer 2
     using compile_cmd_output_labelrec apply force
    apply(clarsimp simp:RL.fresh_label_def)
    apply(subgoal_tac "nl\<^sub>1 \<le> jl \<longrightarrow> br \<le> jl")
     prefer 2
     apply(meson Suc_leD dual_order.trans)
    using compile_cmd_jump_labels_are_entry_or_fresh less_or_eq_imp_le
    apply(meson option.inject)
   apply(erule disjE)
    apply(force simp:RL.jump_labels_def)
   apply(force simp:RL.jump_labels_def)
  apply(rule ccontr)
  apply clarsimp
  apply(frule_tac l=jl in compile_expr_only_input_label)
   apply(simp only:RL.present_labels_append)
   apply(force split:if_splits simp:RL.present_labels_def)
  by force

lemma if_c1_pc_backward_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>0 @
          (if P\<^sub>0 = [] then l else None, Jz br r) #
          map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>1 \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>2 \<Longrightarrow>
   compile_expr C {} l b = (P\<^sub>0, r, C\<^sub>1, False) \<Longrightarrow>
   RL.fresh_label nl = (br, nl') \<Longrightarrow>
   RL.fresh_label nl' = (ex, nl'') \<Longrightarrow>
   compile_cmd C\<^sub>1 None nl'' c\<^sub>1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) \<Longrightarrow>
   pc > length P\<^sub>0 \<Longrightarrow>
   pc \<le> Suc (length P\<^sub>0 + length P\<^sub>1) \<Longrightarrow>
   pc' \<ge> Suc (length P\<^sub>0)"
  apply(subgoal_tac "pc - Suc (length P\<^sub>0) \<le> length P\<^sub>1")
   prefer 2
   apply force
  apply(frule_tac c\<^sub>2=c\<^sub>2 in if_c1_joinable_backward_helper, force+)
  apply(frule_tac pc=pc and pc'=pc' and mds=mds\<^sub>C and mem=mem\<^sub>C and mds'=mds\<^sub>C' and mem'=mem\<^sub>C'
    in RL.eval\<^sub>r_no_backtrack_over_join)
    apply force
   apply force
  by force

lemma if_c1_pc_forward_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>0 @
          (if P\<^sub>0 = [] then l else None, Jz br r) #
          map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>1 \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>2 \<Longrightarrow>
   compile_expr C {} l b = (P\<^sub>0, r, C\<^sub>1, False) \<Longrightarrow>
   RL.fresh_label nl = (br, nl') \<Longrightarrow>
   RL.fresh_label nl' = (ex, nl'') \<Longrightarrow>
   compile_cmd C\<^sub>1 None nl'' c\<^sub>1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) \<Longrightarrow>
   pc > length P\<^sub>0 \<Longrightarrow>
   pc < Suc (length P\<^sub>0 + length P\<^sub>1) \<Longrightarrow>
   pc' \<le> Suc (length P\<^sub>0 + length P\<^sub>1)"
  apply(subgoal_tac "RL.joinable_forward (map fst P\<^sub>1) ((l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])")
   prefer 2
   apply(force simp:RL.joinable_forward_def compile_cmd_output_label)
  apply(frule_tac pc=pc and pc'=pc' and mds=mds\<^sub>C and mem=mem\<^sub>C and mds'=mds\<^sub>C' and mem'=mem\<^sub>C' and
    P\<^sub>0="map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)]" and P="map fst P\<^sub>1" and
    tail="((l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])"
    in RL.eval\<^sub>r_mid_bounds)
     apply force
    apply force
   apply force
  by force

lemma if_c1_eval_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>0 @
          (if P\<^sub>0 = [] then l else None, Jz br r) #
          map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   \<not> banned_WhileLang_Stop c\<^sub>1 \<Longrightarrow>
   \<not> banned_WhileLang_Stop c\<^sub>2 \<Longrightarrow>
   compile_expr C {} l b = (P\<^sub>0, r, C\<^sub>1, False) \<Longrightarrow>
   RL.fresh_label nl = (br, nl') \<Longrightarrow>
   RL.fresh_label nl' = (ex, nl'') \<Longrightarrow>
   compile_cmd C\<^sub>1 None nl'' c\<^sub>1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) \<Longrightarrow>
   pc > length P\<^sub>0 \<Longrightarrow>
   pc < Suc (length P\<^sub>0 + length P\<^sub>1) \<Longrightarrow>
   pc' \<ge> Suc (length P\<^sub>0) \<Longrightarrow>
   pc' \<le> Suc (length P\<^sub>0 + length P\<^sub>1) \<Longrightarrow>
   eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>0), map fst P\<^sub>1), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
         \<langle>((pc' - Suc (length P\<^sub>0), map fst P\<^sub>1), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C"
  apply(rule_tac P\<^sub>1="map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)]" in RL.eval\<^sub>r_join_right)
     apply force
    apply force
   apply force
  apply(rule_tac tail="((l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])" in RL.eval\<^sub>r_head)
    apply force
   apply force
  by force

lemma if_jz_br_miss_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>0 @
          (if P\<^sub>0 = [] then l else None, Jz br r) #
          map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>1 \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>2 \<Longrightarrow>
   compile_expr C {} l b = (P\<^sub>0, r, C\<^sub>1, False) \<Longrightarrow>
   RL.fresh_label nl = (br, nl') \<Longrightarrow>
   RL.fresh_label nl' = (ex, nl'') \<Longrightarrow>
   compile_cmd C\<^sub>1 None nl'' c\<^sub>1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) \<Longrightarrow>
   RL.res\<^sub>P br (map fst P\<^sub>0) = length P\<^sub>0 \<and>
   RL.res\<^sub>P br (map fst P\<^sub>1) = length P\<^sub>1"
  apply(rule conjI)
   apply(metis compile_expr_noninput_label RL.fresh_label_def fst_conv nat_less_le)
  apply(clarsimp simp:RL.fresh_label_def)
  apply(frule compile_cmd_present_labels_are_entry_or_fresh[simplified,rotated])
    apply force
   apply force
  apply(clarsimp simp:RL.present_labels_def2)
  by (metis (no_types) Suc_eq_plus1 le_add1 le_neq_implies_less length_map not_less_eq_eq RL.res\<^sub>P_bounds)

lemma if_jz_br_hit_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>0 @
          (if P\<^sub>0 = [] then l else None, Jz br r) #
          map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>1 \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>2 \<Longrightarrow>
   compile_expr C {} l b = (P\<^sub>0, r, C\<^sub>1, False) \<Longrightarrow>
   RL.fresh_label nl = (br, nl') \<Longrightarrow>
   RL.fresh_label nl' = (ex, nl'') \<Longrightarrow>
   compile_cmd C\<^sub>1 None nl'' c\<^sub>1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) \<Longrightarrow>
   RL.res\<^sub>P br (map fst P\<^sub>0 @
          (if P\<^sub>0 = [] then l else None, Jz br r) #
          map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]) = Suc (Suc (length P\<^sub>0 + length P\<^sub>1))"
  apply(frule_tac c\<^sub>1=c\<^sub>1 and c\<^sub>2=c\<^sub>2 in if_jz_br_miss_helper, force+)
  apply clarsimp
  apply(subgoal_tac "RL.res\<^sub>P br [(l\<^sub>1, Jmp ex)] = Suc 0 \<and>
    RL.res\<^sub>P br [(if P\<^sub>0 = [] then l else None, Jz br r)] = Suc 0")
   prefer 2
   apply(clarsimp simp:RL.res\<^sub>P_def RL.res\<^sub>P'_def Option.bind_def List.extract_def RL.fresh_label_def)
   apply(metis Suc_n_not_le_n compile_cmd_exit_label_fresh le_Suc_eq nat_less_le)
  apply(subgoal_tac "RL.res\<^sub>P br (map fst P\<^sub>2) = 0")
   prefer 2
   apply(force simp:compile_cmd_input_label_res\<^sub>P)
  apply(drule_tac tail="[(if P\<^sub>0 = [] then l else None, Jz br r)]" in RL.res\<^sub>P_exit_append)
   apply force
  apply(drule_tac P="map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)]" and tail="map fst P\<^sub>1" in RL.res\<^sub>P_exit_append)
   apply force
  apply(drule_tac P="(map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)]) @ map fst P\<^sub>1" and tail="[(l\<^sub>1, Jmp ex)]" in RL.res\<^sub>P_exit_append)
   apply force
  apply(drule_tac P="((map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)]) @ map fst P\<^sub>1) @ [(l\<^sub>1, Jmp ex)]" and tail="map fst P\<^sub>2" in RL.res\<^sub>P_exit_append)
   apply force
  apply(drule_tac P="(((map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)]) @ map fst P\<^sub>1) @ [(l\<^sub>1, Jmp ex)]) @ map fst P\<^sub>2" and tail="[(l\<^sub>2, Tau)]" in RL.res\<^sub>P_ib_append)
   apply clarsimp
   apply(metis (no_types, hide_lams) compiled_cmd_nonempty fst_conv)
  by force

lemma if_jz_ex_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>1 \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>2 \<Longrightarrow>
   compile_expr C {} l b = (P\<^sub>0, r, C\<^sub>1, False) \<Longrightarrow>
   RL.fresh_label nl = (br, nl') \<Longrightarrow>
   RL.fresh_label nl' = (ex, nl'') \<Longrightarrow>
   compile_cmd C\<^sub>1 None nl'' c\<^sub>1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) \<Longrightarrow>
   RL.res\<^sub>P ex (map fst P\<^sub>0 @
          (if P\<^sub>0 = [] then l else None, Jz br r) #
          map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]) =
     Suc (Suc (Suc (length P\<^sub>0 + (length P\<^sub>1 + length P\<^sub>2))))"
  apply(subgoal_tac "RL.res\<^sub>P ex (map fst P\<^sub>0) = length P\<^sub>0")
   prefer 2
   apply(metis compile_expr_noninput_label RL.fresh_label_def Suc_n_not_le_n  fst_conv less_or_eq_imp_le snd_conv)
  apply(subgoal_tac "RL.res\<^sub>P ex (map fst P\<^sub>1) = length P\<^sub>1")
   prefer 2
   apply(clarsimp simp:RL.fresh_label_def)
   apply(frule compile_cmd_present_labels_are_entry_or_fresh[simplified,rotated])
     apply force
    apply force
   apply(clarsimp simp:RL.present_labels_def2)
   apply(metis Suc_n_not_le_n length_map RL.res\<^sub>P_oob)
  apply(subgoal_tac "RL.res\<^sub>P ex (map fst P\<^sub>2) = length P\<^sub>2")
   prefer 2
   apply(clarsimp simp:RL.fresh_label_def)
   apply(frule_tac c=c\<^sub>2 in compile_cmd_present_labels_are_entry_or_fresh[simplified,rotated])
     apply clarsimp
     apply(meson Suc_leD Suc_le_lessD compile_cmd_output_labelrec)
    apply force
   apply(clarsimp simp:RL.present_labels_def2)
   apply(metis compile_cmd_output_labelrec length_map not_less_eq_eq RL.res\<^sub>P_oob)
  apply(subgoal_tac "RL.res\<^sub>P ex [(l\<^sub>1, Jmp ex)] = Suc 0 \<and>
    RL.res\<^sub>P ex [(if P\<^sub>0 = [] then l else None, Jz br r)] = Suc 0 \<and>
    RL.res\<^sub>P ex [(l\<^sub>2, Tau)] = Suc 0")
   prefer 2
   apply(clarsimp simp:RL.res\<^sub>P_def RL.res\<^sub>P'_def Option.bind_def List.extract_def RL.fresh_label_def)
   apply(metis (no_types, lifting) Suc_n_not_le_n compile_cmd_exit_label_fresh compile_cmd_output_labelrec less_imp_le_nat not_less_eq_eq)
  apply(drule_tac l=ex and tail="[(if P\<^sub>0 = [] then l else None, Jz br r)]" in RL.res\<^sub>P_exit_append)
   apply force
  apply(drule_tac l=ex and P="map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)]" and tail="map fst P\<^sub>1" in RL.res\<^sub>P_exit_append)
   apply force
  apply(drule_tac P="(map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)]) @ map fst P\<^sub>1" and tail="[(l\<^sub>1, Jmp ex)]" in RL.res\<^sub>P_exit_append)
   apply force
  apply(drule_tac P="((map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)]) @ map fst P\<^sub>1) @ [(l\<^sub>1, Jmp ex)]" and tail="map fst P\<^sub>2" in RL.res\<^sub>P_exit_append)
   apply force
  apply(drule_tac P="(((map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)]) @ map fst P\<^sub>1) @ [(l\<^sub>1, Jmp ex)]) @ map fst P\<^sub>2" and tail="[(l\<^sub>2, Tau)]" in RL.res\<^sub>P_exit_append)
   apply force
  by force

lemma if_c1_reach_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>1 \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>2 \<Longrightarrow>
   compile_expr C {} l b = (P\<^sub>0, r, C\<^sub>1, False) \<Longrightarrow>
   RL.fresh_label nl = (br, nl') \<Longrightarrow>
   RL.fresh_label nl' = (ex, nl'') \<Longrightarrow>
   compile_cmd C\<^sub>1 None nl'' c\<^sub>1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) \<Longrightarrow>
   pc' \<ge> Suc (length P\<^sub>0) \<Longrightarrow>
   pc' \<le> Suc (length P\<^sub>0 + length P\<^sub>1) \<Longrightarrow>
   RL.loc_reach_sig
      \<langle>((pc', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C
      \<langle>((pc'', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs''), mds\<^sub>C'', mem\<^sub>C'''\<rangle>\<^sub>C sig\<^sub>C \<Longrightarrow>
   pc'' \<ge> Suc (length P\<^sub>0 + length P\<^sub>1) \<Longrightarrow>
   pc'' = Suc (length P\<^sub>0 + length P\<^sub>1) \<or> pc'' = Suc (Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2)))"
  apply(induct sig\<^sub>C arbitrary:pc' regs' mds\<^sub>C' mem\<^sub>C')
   apply(force simp:RL.lc_havoc_eq_def)
  apply(clarsimp simp:RL.lc_havoc_eq_def)
  apply(frule RL.eval\<^sub>r_no_modify)
  apply clarsimp
  apply(rename_tac pc\<^sub>0 regs\<^sub>0 mds\<^sub>0 mem\<^sub>0 sig\<^sub>C mem\<^sub>C' pc\<^sub>1 regs\<^sub>1 mds\<^sub>1 mem\<^sub>1)
  apply(erule_tac x=pc\<^sub>1 in meta_allE)
  apply(erule_tac x=regs\<^sub>1 in meta_allE)
  apply(erule_tac x=mds\<^sub>1 in meta_allE)
  apply(erule_tac x=mem\<^sub>1 in meta_allE)
  apply(subgoal_tac "pc\<^sub>1 \<ge> Suc (length P\<^sub>0)")
   prefer 2
   apply(rule_tac c\<^sub>1=c\<^sub>1 and c\<^sub>2=c\<^sub>2 in if_c1_pc_backward_helper, simp+)
  apply(case_tac "pc\<^sub>0 = Suc (length P\<^sub>0 + length P\<^sub>1)")
   apply(subgoal_tac "RL.res\<^sub>P ex (map fst P\<^sub>0 @ (if P\<^sub>0 = [] then l else None, Jz br r) # map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])
       = Suc (Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2)))")
    prefer 2
    using if_jz_ex_helper
    apply force
   apply(frule_tac l=ex in RL.jmp_dest[rotated])
    apply(force simp:nth_append)
   apply(frule loc_reach_sig_haltD)
    apply force
   apply force
  apply(subgoal_tac "pc\<^sub>1 \<le> Suc (length P\<^sub>0 + length P\<^sub>1)")
   prefer 2
   apply(rule_tac c\<^sub>1=c\<^sub>1 and c\<^sub>2=c\<^sub>2 in if_c1_pc_forward_helper, simp+)
  done

lemma if_c2_joinable_backward_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>0 @
          (if P\<^sub>0 = [] then l else None, Jz br r) #
          map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>1 \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>2 \<Longrightarrow>
   compile_expr C {} l b = (P\<^sub>0, r, C\<^sub>1, False) \<Longrightarrow>
   RL.fresh_label nl = (br, nl') \<Longrightarrow>
   RL.fresh_label nl' = (ex, nl'') \<Longrightarrow>
   compile_cmd C\<^sub>1 None nl'' c\<^sub>1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) \<Longrightarrow>
   Suc (Suc (length P\<^sub>0 + length P\<^sub>1)) \<le> pc \<Longrightarrow>
   pc \<le> Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2)) \<Longrightarrow>
   RL.joinable_backward
    (map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)] @ (map fst P\<^sub>1) @ [(l\<^sub>1, Jmp ex)])
    (map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])"
  apply(simp only:RL.joinable_backward_def)
  apply(rule ballI)
  apply(rename_tac jl)
  apply(subgoal_tac "nl\<^sub>1 \<ge> nl''")
   prefer 2
   using compile_cmd_output_labelrec apply force
  apply(subgoal_tac "br = jl \<or> nl\<^sub>1 \<le> jl")
   prefer 2
   apply(simp only:cons_append_form RL.jump_labels_append)
   apply clarsimp
   apply(erule disjE)
    using compile_cmd_jump_labels_are_entry_or_fresh
    apply blast
   apply(erule disjE)
    apply(force simp:RL.jump_labels_def)
   apply(force simp:RL.jump_labels_def)
  apply(erule disjE)
   apply(frule_tac c\<^sub>1=c\<^sub>1 and c\<^sub>2=c\<^sub>2 in if_jz_br_miss_helper, force+)
   apply(simp only:RL.present_labels_append)
   apply(clarsimp split del:if_split)
   apply(rule conjI)
    apply(force simp:RL.present_labels_def2)
   apply(rule conjI)
    apply(solves\<open>clarsimp simp:RL.present_labels_def RL.fresh_label_def\<close>)
   apply(rule conjI)
    apply(force simp:RL.present_labels_def2)
   apply(clarsimp simp:RL.present_labels_def RL.fresh_label_def)
   using Suc_n_not_le_n compile_cmd_exit_label_fresh le_Suc_eq apply blast
  apply(clarsimp split del:if_split)
  apply(simp only:cons_append_form RL.present_labels_append)
  apply(clarsimp split del:if_split simp:RL.fresh_label_def)
  apply(erule disjE)
   apply(frule_tac l=jl in compile_expr_only_input_label)
    apply force
   apply force
  apply(erule disjE)
   apply(force simp:RL.present_labels_def split:if_splits)
  apply(erule disjE)
   using compile_cmd_present_label_upper_bound apply fastforce
  apply(erule disjE)
   apply(clarsimp simp:RL.present_labels_def)
   using compile_cmd_maintains_labelrec_freshness leD apply blast
  by (force simp:RL.present_labels_def)

lemma if_c2_pc_backward_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>0 @
          (if P\<^sub>0 = [] then l else None, Jz br r) #
          map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>1 \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>2 \<Longrightarrow>
   compile_expr C {} l b = (P\<^sub>0, r, C\<^sub>1, False) \<Longrightarrow>
   RL.fresh_label nl = (br, nl') \<Longrightarrow>
   RL.fresh_label nl' = (ex, nl'') \<Longrightarrow>
   compile_cmd C\<^sub>1 None nl'' c\<^sub>1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) \<Longrightarrow>
   Suc (Suc (length P\<^sub>0 + length P\<^sub>1)) \<le> pc \<Longrightarrow>
   pc \<le> Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2)) \<Longrightarrow>
   pc' \<ge> Suc (Suc (length P\<^sub>0 + length P\<^sub>1))"
  apply(subgoal_tac "pc - Suc (Suc (length P\<^sub>0 + length P\<^sub>1)) \<le> length P\<^sub>2")
   prefer 2
   using RL.eval\<^sub>r_oob_pc_halt le_Suc_eq apply auto[1]
  apply(frule_tac c\<^sub>2=c\<^sub>2 in if_c2_joinable_backward_helper, force+)
  apply(frule_tac pc=pc and pc'=pc' and mds=mds\<^sub>C and mem=mem\<^sub>C and mds'=mds\<^sub>C' and mem'=mem\<^sub>C'
    in RL.eval\<^sub>r_no_backtrack_over_join)
    apply force
   apply force
  by force

lemma if_c2_pc_forward_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>0 @
          (if P\<^sub>0 = [] then l else None, Jz br r) #
          map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>1 \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>2 \<Longrightarrow>
   compile_expr C {} l b = (P\<^sub>0, r, C\<^sub>1, False) \<Longrightarrow>
   RL.fresh_label nl = (br, nl') \<Longrightarrow>
   RL.fresh_label nl' = (ex, nl'') \<Longrightarrow>
   compile_cmd C\<^sub>1 None nl'' c\<^sub>1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) \<Longrightarrow>
   Suc (Suc (length P\<^sub>0 + length P\<^sub>1)) \<le> pc \<Longrightarrow>
   pc < Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2)) \<Longrightarrow>
   pc' \<le> Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2))"
  apply(subgoal_tac "RL.joinable_forward (map fst P\<^sub>2) [(l\<^sub>2, Tau)]")
   prefer 2
   apply(force simp:RL.joinable_forward_def compile_cmd_output_label)
  apply(frule_tac pc=pc and pc'=pc' and mds=mds\<^sub>C and mem=mem\<^sub>C and mds'=mds\<^sub>C' and mem'=mem\<^sub>C' and
    P\<^sub>0="map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)] @ (map fst P\<^sub>1) @ [(l\<^sub>1, Jmp ex)]" and
    P="map fst P\<^sub>2" and
    tail="[(l\<^sub>2, Tau)]"
    in RL.eval\<^sub>r_mid_bounds)
     apply force
    apply force
   apply force
  by force

lemma if_c2_eval_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   eval_abv\<^sub>C
    \<langle>((pc, map fst P\<^sub>0 @
          (if P\<^sub>0 = [] then l else None, Jz br r) #
          map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
    \<langle>((pc', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   compile_expr C {} l b = (P\<^sub>0, r, C\<^sub>1, False) \<Longrightarrow>
   RL.fresh_label nl = (br, nl') \<Longrightarrow>
   RL.fresh_label nl' = (ex, nl'') \<Longrightarrow>
   compile_cmd C\<^sub>1 None nl'' c\<^sub>1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) \<Longrightarrow>
   Suc (Suc (length P\<^sub>0 + length P\<^sub>1)) \<le> pc \<Longrightarrow>
   pc < Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2)) \<Longrightarrow>
   pc' \<ge> Suc (Suc (length P\<^sub>0 + length P\<^sub>1)) \<Longrightarrow>
   pc' \<le> Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2)) \<Longrightarrow>
   eval_abv\<^sub>C \<langle>((pc - Suc (Suc (length P\<^sub>0 + length P\<^sub>1)), map fst P\<^sub>2), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
         \<langle>((pc' - Suc (Suc (length P\<^sub>0 + length P\<^sub>1)), map fst P\<^sub>2), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C"
  apply(rule_tac P\<^sub>1="map fst P\<^sub>0 @ [(if P\<^sub>0 = [] then l else None, Jz br r)] @ map fst P\<^sub>1 @ [(l\<^sub>1, Jmp ex)]" in RL.eval\<^sub>r_join_right)
     apply force
    apply force
   apply force
  apply(rule_tac tail="[(l\<^sub>2, Tau)]" in RL.eval\<^sub>r_head)
    apply force
   apply force
  by (simp add: Suc_diff_Suc numeral_2_eq_2)

lemma if_c2_reach_helper:
  "\<forall>x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>1 \<Longrightarrow>
   \<not> banned_WhileLang_forms c\<^sub>2 \<Longrightarrow>
   compile_expr C {} l b = (P\<^sub>0, r, C\<^sub>1, False) \<Longrightarrow>
   RL.fresh_label nl = (br, nl') \<Longrightarrow>
   RL.fresh_label nl' = (ex, nl'') \<Longrightarrow>
   compile_cmd C\<^sub>1 None nl'' c\<^sub>1 = (P\<^sub>1, l\<^sub>1, nl\<^sub>1, C\<^sub>2, False) \<Longrightarrow>
   compile_cmd C\<^sub>1 (Some br) nl\<^sub>1 c\<^sub>2 = (P\<^sub>2, l\<^sub>2, nl\<^sub>2, C\<^sub>3, False) \<Longrightarrow>
   pc' \<ge> Suc (Suc (length P\<^sub>0 + length P\<^sub>1)) \<Longrightarrow>
   pc' \<le> Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2)) \<Longrightarrow>
   RL.loc_reach_sig
      \<langle>((pc', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C
      \<langle>((pc'', map fst P\<^sub>0 @
           (if P\<^sub>0 = [] then l else None, Jz br r) #
           map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)]), regs''), mds\<^sub>C'', mem\<^sub>C'''\<rangle>\<^sub>C sig\<^sub>C \<Longrightarrow>
   pc'' \<ge> Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2)) \<Longrightarrow>
   pc'' = Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2)) \<or> pc'' = Suc (Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2)))"
  apply(induct sig\<^sub>C arbitrary:pc' regs' mds\<^sub>C' mem\<^sub>C')
   apply(force simp:RL.lc_havoc_eq_def)
  apply(clarsimp simp:RL.lc_havoc_eq_def)
  apply(frule RL.eval\<^sub>r_no_modify)
  apply clarsimp
  apply(rename_tac pc\<^sub>0 regs\<^sub>0 mds\<^sub>0 mem\<^sub>0 sig\<^sub>C mem\<^sub>C' pc\<^sub>1 regs\<^sub>1 mds\<^sub>1 mem\<^sub>1)
  apply(erule_tac x=pc\<^sub>1 in meta_allE)
  apply(erule_tac x=regs\<^sub>1 in meta_allE)
  apply(erule_tac x=mds\<^sub>1 in meta_allE)
  apply(erule_tac x=mem\<^sub>1 in meta_allE)
  apply clarsimp
  apply(subgoal_tac "pc\<^sub>1 \<ge> Suc (Suc (length P\<^sub>0 + length P\<^sub>1))")
   prefer 2
   apply(rule_tac c\<^sub>1=c\<^sub>1 and c\<^sub>2=c\<^sub>2 in if_c2_pc_backward_helper, simp+)
  apply(case_tac "pc\<^sub>0 = Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2))")
   apply(subgoal_tac "RL.res\<^sub>P ex (map fst P\<^sub>0 @ (if P\<^sub>0 = [] then l else None, Jz br r) # map fst P\<^sub>1 @ (l\<^sub>1, Jmp ex) # map fst P\<^sub>2 @ [(l\<^sub>2, Tau)])
       = Suc (Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2)))")
    prefer 2
    using if_jz_ex_helper
    apply force
   apply(frule RL.tau_dest[rotated])
    apply(force simp:nth_append)
   apply(frule loc_reach_sig_haltD)
    apply force
   apply force
  apply(subgoal_tac "pc\<^sub>1 \<le> Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2))")
   prefer 2
   apply(rule_tac c\<^sub>1=c\<^sub>1 and c\<^sub>2=c\<^sub>2 in if_c2_pc_forward_helper, simp+)
  done

end
end