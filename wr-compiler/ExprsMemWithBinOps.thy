(*
Author: Robert Sison
*)
theory ExprsMemWithBinOps
imports "../refinement/CompositionalRefinement"
begin

subsection {* While-language expression instantiation *}

datatype ('val, 'var, 'op) exp =
    EConst "'val"
  | ELoad "'var"
  | EOp 'op "('val, 'var, 'op) exp" "('val, 'var, 'op) exp"

locale op_set_param_lang =
  fixes op_ev :: "'OpId \<Rightarrow> 'Val::zero \<Rightarrow> 'Val \<Rightarrow> 'Val"

context op_set_param_lang
begin

(* Address expression evaluation *)
primrec
  exp_ev :: "('Var, 'Val) Mem \<Rightarrow> ('Val, 'Var, 'OpId) exp \<Rightarrow> 'Val"
where
  "exp_ev mem (EConst v) = v" |
  "exp_ev mem (ELoad x) = mem x" |
  "exp_ev mem (EOp opid x y) = op_ev opid (exp_ev mem x) (exp_ev mem y)"

(* Boolean expression evaluation *)
definition
  exp_ev\<^sub>B :: "('Var, 'Val) Mem \<Rightarrow> ('Val, 'Var, 'OpId) exp \<Rightarrow> bool"
where
  "exp_ev\<^sub>B mem e \<equiv> exp_ev mem e \<noteq> 0"

primrec
  exp_vars :: "(_, 'Var, 'OpId) exp \<Rightarrow> 'Var set"
where
  "exp_vars (EConst _) = {}" |
  "exp_vars (ELoad v) = {v}" |
  "exp_vars (EOp opid x y) = (exp_vars x) \<union> (exp_vars y)"

end

end