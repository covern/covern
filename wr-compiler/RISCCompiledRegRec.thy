(*
Author: Robert Sison
*)

theory RISCCompiledRegRec
imports RISCCompilation RISCCompiledIfCmd RISCCompiledWhileCmd
  "~~/src/HOL/Eisbach/Eisbach_Tools"
begin

context risc_compilation
begin

subsection {* Consistency between memory and the register records for a compiled command *}

(* Given we're essentially establishing in this section that regrec-mem-consistency is invariant,
  it might be a good idea to incorporate this into \<R>_conds *)

definition regrec_mem_consistent
where
  "regrec_mem_consistent C regs mem\<^sub>C \<equiv> \<forall>r e. (regrec C) r = Some e \<longrightarrow> regs r = exp_ev (to_prog_mem mem\<^sub>C) e"

lemma compiled_expr_eval_maintains_regrec_consistency:
  "regrec_mem_consistent (snd (P ! pc)) regs mem\<^sub>C \<Longrightarrow>
   \<forall>v\<^sub>C. v\<^sub>C \<notin> range id \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite \<Longrightarrow>
   compile_expr C A l e = (P, r, C', False) \<Longrightarrow>
   finite (dom (regrec C)) \<Longrightarrow>
   finite A \<Longrightarrow>
   pc < length P \<Longrightarrow>
   eval_abv\<^sub>C \<langle>((pc, map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   if pc' < length P then regrec_mem_consistent ((map snd P) ! pc') regs' mem\<^sub>C'
                     else regrec_mem_consistent C' regs' mem\<^sub>C'"
  apply(frule_tac pc=pc and P'="[]" in compiled_expr_only_increments)
    apply force
   apply force
  apply(induct e arbitrary: C A l P r C' pc pc')
    apply(force dest:RL.move_k_dest[rotated] simp:regrec_mem_consistent_def mem\<^sub>A_of_def id_def split:option.splits)
   apply(force dest:RL.load_dest[rotated] simp:Let_def reg_alloc_cached_correct regrec_mem_consistent_def to_prog_mem_def split:option.splits)
  apply(clarsimp split:prod.splits)
  apply(rename_tac opid e1 e2 C A l r pc P\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 r' C\<^sub>2 fail\<^sub>2)
  (* case by case for where pc is *)
  (* in P\<^sub>1 *)
  apply(case_tac "pc < length P\<^sub>1")
   apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc, map fst P\<^sub>1), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((Suc pc, map fst P\<^sub>1), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
    prefer 2
    using RL.eval\<^sub>r_head RL.eval\<^sub>r_bounds
    apply(metis Suc_leI length_map)
   apply clarsimp
   apply(simp add: nth_append)
   apply(subgoal_tac "if Suc pc < length P\<^sub>1 then regrec_mem_consistent (map snd P\<^sub>1 ! Suc pc) regs' mem\<^sub>C' else regrec_mem_consistent C\<^sub>1 regs' mem\<^sub>C'")
    prefer 2
    apply metis
   apply(clarsimp split:if_splits)
   using compile_expr_first_comprec_input
   apply(metis (no_types, lifting) compile_expr_empty_maintains_comprec length_greater_0_conv nth_map)
  apply(subgoal_tac "pc \<ge> length P\<^sub>1")
   prefer 2
   apply clarsimp
  apply(frule_tac e=e1 in compile_expr_preserves_finite_regrec)
   apply force
  apply(frule_tac C=C\<^sub>1 and e=e2 in compile_expr_preserves_finite_regrec)
   apply force
  (* in P\<^sub>2 *)
  apply(case_tac "pc < length P\<^sub>1 + length P\<^sub>2")
   apply clarsimp
   apply(subgoal_tac "pc - length P\<^sub>1 < length P\<^sub>2")
    prefer 2
    apply linarith
   apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - length P\<^sub>1, map fst P\<^sub>2 @ [(if P\<^sub>1 = [] \<and> P\<^sub>2 = [] then l else None, Op opid r r')]), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
     \<langle>((Suc (pc - length P\<^sub>1), map fst P\<^sub>2 @ [(if P\<^sub>1 = [] \<and> P\<^sub>2 = [] then l else None, Op opid r r')]), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
    prefer 2
    apply(rule_tac P\<^sub>1="map fst P\<^sub>1" in RL.eval\<^sub>r_join_right, simp+)
   apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - length P\<^sub>1, map fst P\<^sub>2), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((Suc (pc - length P\<^sub>1), map fst P\<^sub>2), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
    prefer 2
    using RL.eval\<^sub>r_head RL.eval\<^sub>r_bounds
    apply(metis Suc_leI length_map)
   apply(simp add:nth_append)
   apply(subgoal_tac "finite (insert r A)")
    prefer 2
    apply clarsimp
   apply(subgoal_tac "if Suc (pc - length P\<^sub>1) < length P\<^sub>2
     then regrec_mem_consistent (map snd P\<^sub>2 ! Suc (pc - length P\<^sub>1)) regs' mem\<^sub>C'
     else regrec_mem_consistent C\<^sub>2 regs' mem\<^sub>C'")
    prefer 2
    apply metis
   apply(clarsimp split:if_splits)
   using compile_expr_first_comprec_input
   apply(metis Nat.add_diff_assoc2 Suc_eq_plus1)
  (* in the Op *)
  apply(subgoal_tac "pc = length P\<^sub>1 + length P\<^sub>2")
   prefer 2
   apply clarsimp
  apply clarsimp
  apply(drule_tac r=r and r'=r' and opid=opid in RL.op_dest[rotated], simp+)
   apply(metis (no_types, lifting) Nat.add_0_right Suc_leI add_diff_cancel_left' le_add1 length_map not_less_eq_eq nth_append nth_append_length snd_conv)
  apply clarsimp
  apply(rule conjI)
   apply(clarsimp simp:regrec_mem_consistent_def mem\<^sub>A_of_def id_def)
   apply(metis compile_expr_correctness compile_expr_empty_maintains_comprec same_binary_ops finite.insertI)
  apply(clarsimp simp:regrec_mem_consistent_def)
  apply(subgoal_tac "regrec C\<^sub>1 r = Some e1 \<and> regrec C\<^sub>2 r' = Some e2")
   prefer 2
   using compile_expr_correctness
   apply(metis finite.insertI)
  apply(clarsimp simp:mem\<^sub>A_of_def id_def)
  apply(subgoal_tac "regrec C\<^sub>2 r = Some e1")
   using same_binary_ops
   apply fastforce
  (* the 1st's register should be in the 2nd compile_expr's A.
    So that should tell the 2nd compile_expr not to trash it. *)
  using compiled_expr_doesnt_trash_A
  by (metis finite_insert insertCI)

lemma compiled_expr_eval_regrecs:
  "regrec_mem_consistent (snd (P ! pc)) regs mem\<^sub>C \<Longrightarrow>
   \<forall>v\<^sub>C. v\<^sub>C \<notin> range id \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite \<Longrightarrow>
   compile_expr C A l e = (P, r, C', False) \<Longrightarrow>
   finite (dom (regrec C)) \<Longrightarrow>
   finite A \<Longrightarrow>
   pc < length P \<Longrightarrow>
   eval_abv\<^sub>C \<langle>((pc, map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   pc' < length P \<Longrightarrow>
   regrec_mem_consistent (snd (P ! pc')) regs' mem\<^sub>C'"
  using compiled_expr_eval_maintains_regrec_consistency
  by (metis (no_types, hide_lams) nth_map)

lemma compiled_expr_eval_regrec_end:
  "regrec_mem_consistent (snd (P ! pc)) regs mem\<^sub>C \<Longrightarrow>
   \<forall>v\<^sub>C. v\<^sub>C \<notin> range id \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite \<Longrightarrow>
   compile_expr C A l e = (P, r, C', False) \<Longrightarrow>
   finite (dom (regrec C)) \<Longrightarrow>
   finite A \<Longrightarrow>
   pc < length P \<Longrightarrow>
   eval_abv\<^sub>C \<langle>((pc, map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   pc' \<ge> length P \<Longrightarrow>
   regrec_mem_consistent C' regs' mem\<^sub>C'"
  using compiled_expr_eval_maintains_regrec_consistency
  by auto

(* regrec_mem_consistent can only get easier with a regrec that gets smaller! *)
lemma regrec_le_mem_consistent:
  "regrec_mem_consistent C regs mem\<^sub>C \<Longrightarrow> regrec C' \<subseteq>\<^sub>m regrec C \<Longrightarrow> regrec_mem_consistent C' regs mem\<^sub>C"
  apply(clarsimp simp:regrec_mem_consistent_def map_le_def)
  by (metis (no_types, lifting) domI)

lemma compiled_cmd_eval_maintains_regrec_consistency:
  "(zip P Cs, l', nl', C', False) = (compile_cmd C l nl c) \<Longrightarrow>
   finite (dom (regrec C)) \<Longrightarrow>
   \<not> banned_WhileLang_forms c \<Longrightarrow>
   \<forall> x. l = Some x \<longrightarrow> x < nl \<Longrightarrow>
   length P = length Cs \<Longrightarrow>
   pc < length P \<Longrightarrow>
   regrec_mem_consistent (Cs ! pc) regs mem\<^sub>C \<Longrightarrow>
   \<forall>v\<^sub>C. v\<^sub>C \<notin> range id \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite \<Longrightarrow>
   (\<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C, \<langle>((pc', P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RL.eval\<^sub>r \<Longrightarrow>
   if pc' < length P then regrec_mem_consistent (Cs ! pc') regs' mem\<^sub>C'
                     else regrec_mem_consistent C' regs' mem\<^sub>C'"
  apply(induct c arbitrary: P Cs l' nl' C' C l nl pc pc')
         (* Assign *)
         apply(case_tac "pc' < length P")
          apply(clarsimp simp:zip_eq_conv Let_def RL.lc_havoc_eq_def split:prod.splits)
          apply(rename_tac v e C l nl pc pc' P r C' fail')
          (* pc' not at the end *)
          apply(case_tac "pc < length P")
           apply(subgoal_tac "compile_expr_output_forms (snd (fst (P ! pc)))")
            prefer 2
            using compile_expr_output_forms
            apply metis
           apply clarsimp
           apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc, map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
                              \<langle>((pc', map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
            prefer 2
            using RL.eval\<^sub>r_head
            apply(metis Suc_leI compiled_expr_only_increments length_map)
           apply(simp add:nth_append)
           apply(frule compiled_expr_eval_maintains_regrec_consistency, simp+)
           using compiled_expr_only_increments apply fastforce
          apply(fastforce dest:RL.store_dest[rotated] simp:nth_append)
         (* pc' at the end *)
         apply(clarsimp simp:zip_eq_conv Let_def RL.lc_havoc_eq_def nth_append split:prod.splits split del:if_split)
         apply(rename_tac v e C l nl pc pc' P r C' fail')
         apply(subgoal_tac "pc' = Suc (length P)")
          prefer 2
          apply(metis (no_types) Suc_eq_plus1 Suc_leI compiled_expr_only_increments le_neq_implies_less length_map less_Suc_eq nth_append_length snd_conv RL.store_dest[rotated])
         apply(case_tac "pc < length P")
          (* shouldn't be possible because there are no jumps in compiled exprs *)
          using compiled_expr_only_increments
          apply blast
         apply(subgoal_tac "pc = length P")
          prefer 2
          apply force
         apply(clarsimp split del:if_split)
         (* somewhat a replay of the case 1 store_elim proof *)
         apply(drule_tac v=v and r=r in RL.store_dest[rotated])
          apply(force simp add:nth_append)
         apply(clarsimp simp:regrec_mem_consistent_def split del:if_split)
         apply(clarsimp simp:regrec_purge_var_def restrict_map_def split:if_splits)
           apply(fastforce simp:to_prog_mem_def)
          apply(fastforce simp add:nth_append WL.eval_vars_det\<^sub>A to_prog_mem_def)
         apply(fastforce simp add:nth_append WL.eval_vars_det\<^sub>A to_prog_mem_def)
        (* Skip *)
        apply(force dest:RL.nop_dest[rotated] simp:zip_eq_conv)
       (* LockAcq *)
       apply(force elim:RL.lock_acq_elim[rotated] simp:zip_eq_conv regrec_mem_consistent_def to_prog_mem_def)
      (* LockRel *)
      apply(clarsimp simp:zip_eq_conv regrec_mem_consistent_def to_prog_mem_def)
      apply(force elim:RL.lock_rel_elim[rotated] simp:regrec_purge_vars_def restrict_map_def)
     (* Seq *)
     apply(clarsimp simp:zip_eq_conv split:prod.splits)
     apply(rename_tac c1 c2 C l nl pc pc' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>1 fail\<^sub>1 P\<^sub>2 l' nl' C' fail')
     apply(subgoal_tac "\<forall>x. l\<^sub>1 = Some x \<longrightarrow> x < nl\<^sub>1")
      prefer 2
      using compile_cmd_maintains_labelrec_freshness
      apply force
     apply(subgoal_tac "RL.joinable (map fst P\<^sub>1) (map fst P\<^sub>2)")
      prefer 2
      using compiled_cmds_joinable
      apply force
     apply(clarsimp simp:RL.joinable_def)
     apply(case_tac "pc < length P\<^sub>1")
      (* in P\<^sub>1 *)
      apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc, map fst P\<^sub>1), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', map fst P\<^sub>1), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
       prefer 2
       apply(metis RL.eval\<^sub>r_head RL.eval\<^sub>r_head_bounds length_map)
      apply(subgoal_tac "if pc' < length P\<^sub>1 then regrec_mem_consistent ((map snd P\<^sub>1) ! pc') regs' mem\<^sub>C'
          else regrec_mem_consistent C\<^sub>1 regs' mem\<^sub>C'")
       prefer 2
       apply(erule_tac x="map fst P\<^sub>1" in meta_allE)
       apply(erule_tac x="map snd P\<^sub>1" in meta_allE)
       apply(erule_tac x="l\<^sub>1" in meta_allE)
       apply(erule_tac x="nl\<^sub>1" in meta_allE)
       apply(erule_tac x="C\<^sub>1" in meta_allE)
       apply(erule_tac x="C" in meta_allE)
       apply(erule_tac x="l" in meta_allE)
       apply(erule_tac x="nl" in meta_allE)
       apply(erule_tac x="pc" in meta_allE)
       apply(erule_tac x="pc'" in meta_allE)
       apply(force simp:zip_map_fst_snd nth_append)
      apply(rule conjI)
       apply(clarsimp split:if_splits simp:nth_append)
       apply(subgoal_tac "regrec (snd (P\<^sub>2 ! 0)) \<subseteq>\<^sub>m regrec C\<^sub>1")
        prefer 2
        using compile_cmd_first_regrec_subset_input
        apply(metis (no_types, lifting) compiled_cmd_nonempty fst_conv length_greater_0_conv nth_map)
       using regrec_le_mem_consistent
       apply(metis (no_types, lifting) diff_is_0_eq' RL.eval\<^sub>r_bounds length_map)
      apply(metis (no_types, hide_lams) Suc_eq_plus1 add_diff_cancel_left' compiled_cmd_nonempty diff_is_0_eq RL.eval\<^sub>r_bounds fst_conv le_imp_less_Suc length_append length_greater_0_conv length_map map_append nat_neq_iff not_less_eq)
     (* in P\<^sub>2 *)
     apply(subgoal_tac "pc - length (map fst P\<^sub>1) < length (map fst P\<^sub>2)")
      prefer 2
      apply force
     apply(subgoal_tac "pc' - length (map fst P\<^sub>1) \<le> length (map fst P\<^sub>2)")
      prefer 2
      apply(metis (no_types, lifting) diff_diff_left diff_is_0_eq RL.eval\<^sub>r_bounds length_append)
     apply(subgoal_tac "pc' = length (map fst P\<^sub>1) + (pc' - length (map fst P\<^sub>1))")
      prefer 2
      apply(metis (no_types) Nat.add_diff_assoc Suc_le_lessD add_diff_cancel_left' RL.eval\<^sub>r_no_backtrack_over_join length_map not_less_eq_eq)
     apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - length (map fst P\<^sub>1), map fst P\<^sub>2), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc' - length (map fst P\<^sub>1), map fst P\<^sub>2), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      apply(rule_tac P\<^sub>1="map fst P\<^sub>1" in RL.eval\<^sub>r_join_right, simp+)
     apply(subgoal_tac "if pc' - length (map fst P\<^sub>1) < length (map fst P\<^sub>2)
         then regrec_mem_consistent ((map snd P\<^sub>2) ! (pc' - length (map fst P\<^sub>1))) regs' mem\<^sub>C'
         else regrec_mem_consistent C' regs' mem\<^sub>C'")
      prefer 2
      apply(rotate_tac 6)
      apply(erule_tac x="map fst P\<^sub>2" in meta_allE)
      apply(erule_tac x="map snd P\<^sub>2" in meta_allE)
      apply(erule_tac x="l'" in meta_allE)
      apply(erule_tac x="nl'" in meta_allE)
      apply(erule_tac x="C'" in meta_allE)
      apply(erule_tac x="C\<^sub>1" in meta_allE)
      apply(erule_tac x="l\<^sub>1" in meta_allE)
      apply(erule_tac x="nl\<^sub>1" in meta_allE)
      apply(erule_tac x="pc - length (map fst P\<^sub>1)" in meta_allE)
      apply(erule_tac x="pc' - length (map fst P\<^sub>1)" in meta_allE)
      using compile_cmd_preserves_finite_regrec
      apply(force simp:zip_map_fst_snd nth_append)
     apply(force simp add: nth_append)
    (* If *)
    apply(clarsimp simp:zip_eq_conv Let_def RL.lc_havoc_eq_def split:prod.splits)
    apply(rename_tac b c\<^sub>1 c\<^sub>2 C l nl pc pc' P\<^sub>0 r C\<^sub>1 fail\<^sub>1 br nl' ex nl'' P\<^sub>1 l\<^sub>1 nl\<^sub>1 C\<^sub>2 fail\<^sub>2 P\<^sub>2 l\<^sub>2 nl\<^sub>2 C\<^sub>3 fail\<^sub>3)
    apply(frule compile_expr_preserves_finite_regrec)
     apply force
    apply(frule_tac C=C\<^sub>1 and c=c\<^sub>1 in compile_cmd_preserves_finite_regrec)
      apply force
     apply force
    apply(frule_tac C=C\<^sub>1 and c=c\<^sub>2 in compile_cmd_preserves_finite_regrec)
      apply force
     apply force
    apply(case_tac "pc < length P\<^sub>0")
     (* Case: in the compiled boolean expr *)
     apply(subgoal_tac "pc' = Suc pc")
      prefer 2
      using compiled_expr_only_increments apply force
     apply(subgoal_tac "compile_expr_output_forms (snd (fst (P\<^sub>0 ! pc)))")
      prefer 2
      apply(metis compile_expr_output_forms)
      apply clarsimp
     apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc, map fst P\<^sub>0), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((Suc pc, map fst P\<^sub>0), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      apply(metis (no_types, hide_lams) RL.eval\<^sub>r_head Suc_leI length_map)
     apply(simp add:nth_append)
     apply(frule compiled_expr_eval_maintains_regrec_consistency, simp+)
     apply fastforce
    (* Case: at the Jz br *)
    apply(case_tac "pc = length P\<^sub>0")
     (* sub-case: Jz jumped to br *)
     apply(case_tac "regs r = 0")
      apply(frule_tac l=br and r=r in RL.jz_s_dest[rotated])
        apply(simp add:nth_append)+
      (* Prove that br is located at the beginning of P\<^sub>2 *)
      apply(subgoal_tac "pc' = Suc (Suc (length P\<^sub>0 + length P\<^sub>1))")
       prefer 2
       using if_jz_br_hit_helper apply force
      apply(clarsimp simp:nth_append)
      using compile_cmd_first_regrec_subset_input compiled_cmd_nonempty regrec_le_mem_consistent
      apply(metis (no_types, hide_lams) fst_conv length_greater_0_conv nth_map)
     (* sub-case: Jz didn't jump *)
     apply clarsimp
     apply(frule_tac l=br and r=r in RL.jz_f_dest[rotated])
       apply(simp add:nth_append)+
     using compile_cmd_first_regrec_subset_input compiled_cmd_nonempty regrec_le_mem_consistent
     apply(metis (no_types, hide_lams) fst_conv length_greater_0_conv nth_map)
    (* Case: in P\<^sub>1 *)
    apply(subgoal_tac "pc > length P\<^sub>0")
     prefer 2
     apply force
    apply clarsimp
    apply(case_tac "pc < Suc (length P\<^sub>0 + length P\<^sub>1)")
     apply(subgoal_tac "pc - Suc (length P\<^sub>0) < length P\<^sub>1")
      prefer 2
      apply force
     (* Firstly rule out a jump backwards into P\<^sub>0 *)
     apply(subgoal_tac "pc' \<ge> Suc (length P\<^sub>0)")
      prefer 2
      using if_c1_pc_backward_helper apply force
     (* Now rule out a jump any further than the end of P\<^sub>1.
       l\<^sub>1 is the label for the Jmp ex immediately after it, so this should catch any
       oob jumps from P\<^sub>1 assuming l\<^sub>1 is the only oob jump label of P\<^sub>1 *)
     apply(subgoal_tac "pc' \<le> Suc (length P\<^sub>0 + length P\<^sub>1)")
      prefer 2
      using if_c1_pc_forward_helper apply force
     apply(erule_tac x="map fst P\<^sub>1" in meta_allE)
     apply(erule_tac x="map snd P\<^sub>1" in meta_allE)
     apply(erule_tac x="l\<^sub>1" in meta_allE)
     apply(erule_tac x="nl\<^sub>1" in meta_allE)
     apply(erule_tac x="C\<^sub>2" in meta_allE)
     apply(erule_tac x="C\<^sub>1" in meta_allE)
     apply(erule_tac x="None" in meta_allE)
     apply(erule_tac x="nl''" in meta_allE)
     apply(erule_tac x="pc - Suc (length P\<^sub>0)" in meta_allE)
     apply(erule_tac x="pc' - Suc (length P\<^sub>0)" in meta_allE)
     apply(clarsimp simp:zip_map_fst_snd nth_append)
     apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>0), map fst P\<^sub>1), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
         \<langle>((pc' - Suc (length P\<^sub>0), map fst P\<^sub>1), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      using if_c1_eval_helper apply force
     apply force
    (* Case: at the Jmp ex *)
    apply(case_tac "pc = Suc (length P\<^sub>0 + length P\<^sub>1)")
     apply(frule_tac l=ex in RL.jmp_dest[rotated])
      apply(simp add:nth_append)+
     (* Prove that ex is not in the program *)
     apply(subgoal_tac "pc' = Suc (Suc (Suc (length P\<^sub>0 + (length P\<^sub>1 + length P\<^sub>2))))")
      prefer 2
      using if_jz_ex_helper apply force
     apply(force simp:restrict_map_def regrec_mem_consistent_def)
    (* Case: in P\<^sub>2 *)
    apply(subgoal_tac "pc \<ge> Suc (Suc (length P\<^sub>0 + length P\<^sub>1))")
     prefer 2
     apply force
    apply clarsimp
    apply(case_tac "pc < Suc (Suc (length P\<^sub>0 + (length P\<^sub>1 + length P\<^sub>2)))")
     apply(subgoal_tac "pc - Suc (Suc (length P\<^sub>0 + length P\<^sub>1)) < length P\<^sub>2")
      prefer 2
      apply force
     (* Firstly rule out a jump backwards into P\<^sub>0 or P\<^sub>1 *)
     apply(subgoal_tac "pc' \<ge> Suc (Suc (length P\<^sub>0 + length P\<^sub>1))")
      prefer 2
      using if_c2_pc_backward_helper apply force
     (* Now rule out a jump any further than the end of P\<^sub>2 *)
     apply(subgoal_tac "pc' \<le> Suc (Suc (length P\<^sub>0 + length P\<^sub>1 + length P\<^sub>2))")
      prefer 2
      using if_c2_pc_forward_helper apply force
     apply(subgoal_tac "br < nl\<^sub>1")
      prefer 2
      apply(clarsimp simp:RL.fresh_label_def)
      apply(meson Suc_leD Suc_le_lessD compile_cmd_output_labelrec)
     apply(subgoal_tac "regrec_mem_consistent (snd (P\<^sub>2 ! (pc - Suc (Suc (length P\<^sub>0 + length P\<^sub>1))))) regs mem\<^sub>C")
      prefer 2
      apply(force simp:nth_append split:if_splits)
     apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - Suc (Suc (length P\<^sub>0 + length P\<^sub>1)), map fst P\<^sub>2), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
         \<langle>((pc' - Suc (Suc (length P\<^sub>0 + length P\<^sub>1)), map fst P\<^sub>2), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      using if_c2_eval_helper apply force
     apply(rotate_tac 6)
     apply(erule_tac x="map fst P\<^sub>2" in meta_allE)
     apply(erule_tac x="map snd P\<^sub>2" in meta_allE)
     apply(erule_tac x="l\<^sub>2" in meta_allE)
     apply(erule_tac x="nl\<^sub>2" in meta_allE)
     apply(erule_tac x="C\<^sub>3" in meta_allE)
     apply(erule_tac x="C\<^sub>1" in meta_allE)
     apply(erule_tac x="Some br" in meta_allE)
     apply(erule_tac x="nl\<^sub>1" in meta_allE)
     apply(erule_tac x="pc - Suc (Suc (length P\<^sub>0 + length P\<^sub>1))" in meta_allE)
     apply(erule_tac x="pc' - Suc (Suc (length P\<^sub>0 + length P\<^sub>1))" in meta_allE)
     apply(force simp:zip_map_fst_snd nth_append)
    (* Case: at final Tau *)
    apply(case_tac "pc = Suc (Suc (length P\<^sub>0 + (length P\<^sub>1 + length P\<^sub>2)))")
     apply clarsimp
     apply(frule RL.tau_dest[rotated])
      apply(force simp:nth_append)
     apply(simp add:nth_append)
     apply(force simp:restrict_map_def regrec_mem_consistent_def)
    (* Case: end *)
    apply force
   (* While *)
   apply(clarsimp simp:zip_eq_conv split:prod.splits split del:if_split)
   apply(rename_tac b c C l nl pc pc' lp nl' ex nl'' P\<^sub>e r C\<^sub>B fail\<^sub>e P l' nl''' C\<^sub>E fail\<^sub>c)
   apply(clarsimp simp:RL.fresh_label_def split del:if_split)
   apply(subgoal_tac "finite (dom (regrec C\<^sub>B))")
    prefer 2
    apply(rule_tac C="(C\<lparr>regrec := Map.empty\<rparr>)" in compile_expr_preserves_finite_regrec)
     apply force
    apply force
   apply(case_tac "pc < length P\<^sub>e")
    (* Case: in the 1st compiled boolean expr *)
    apply(subgoal_tac "pc' = Suc pc")
     prefer 2
     using compiled_expr_only_increments apply force
    apply(subgoal_tac "compile_expr_output_forms (snd (fst (P\<^sub>e ! pc)))")
     prefer 2
     apply(metis (no_types, lifting) compile_expr_output_forms)
    apply clarsimp
    apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc, map fst P\<^sub>e), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((Suc pc, map fst P\<^sub>e), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
     prefer 2
     apply(metis (no_types, hide_lams) RL.eval\<^sub>r_head Suc_leI length_map)
    apply(simp add:nth_append)
    apply(frule compiled_expr_eval_maintains_regrec_consistency, simp+)
    apply force
   (* Case: at the Jz ex *)
   apply(case_tac "pc = length P\<^sub>e")
    (* sub-case: Jz jumped to ex *)
    apply(case_tac "regs r = 0")
     apply(frule_tac l=ex and r=r in RL.jz_s_dest[rotated])
       apply(simp add:nth_append)+
     (* Prove that ex is located at the end of the program *)
     apply(subgoal_tac "pc' = Suc (Suc (length P\<^sub>e + (length P)))")
      prefer 2
      using while_ex_helper apply force
     apply clarsimp
     apply(force simp:regrec_mem_consistent_def)
    (* sub-case: Jz didn't jump *)
    apply clarsimp
    apply(frule_tac l=ex and r=r in RL.jz_f_dest[rotated])
      apply(simp add:nth_append split del:if_split)+
    apply(case_tac "P = []")
     apply(metis (no_types, lifting) compiled_cmd_nonempty fst_conv)
    apply clarsimp
    using compile_cmd_first_regrec_subset_input regrec_le_mem_consistent
    apply(metis (no_types, hide_lams) length_greater_0_conv nth_map)
   (* Case: In the inner command *)
   apply(case_tac "pc < (Suc (length P\<^sub>e + (length P)))")
    apply clarsimp
    apply(rule conjI)
     apply clarsimp
     apply(clarsimp simp:nth_append)
     apply(subgoal_tac "pc' \<ge> Suc (length P\<^sub>e)")
      prefer 2
      using while_pc_backward_helper apply force
     apply clarsimp
     apply(subgoal_tac "pc' \<le> Suc (length P\<^sub>e + length P)")
      prefer 2
      (* We appear to get this one for free *)
      apply force
     apply(erule_tac x="map fst P" in meta_allE)
     apply(erule_tac x="map snd P" in meta_allE)
     apply(erule_tac x="l'" in meta_allE)
     apply(erule_tac x="nl'''" in meta_allE)
     apply(erule_tac x="C\<^sub>E" in meta_allE)
     apply(erule_tac x="C\<^sub>B" in meta_allE)
     apply(erule_tac x="None" in meta_allE)
     apply(erule_tac x="Suc ex" in meta_allE)
     apply(erule_tac x="pc - Suc (length P\<^sub>e)" in meta_allE)
     apply(erule_tac x="pc' - Suc (length P\<^sub>e)" in meta_allE)
     apply(clarsimp simp:zip_map_fst_snd nth_append)
     apply(subgoal_tac "eval_abv\<^sub>C \<langle>((pc - Suc (length P\<^sub>e), map fst P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C
         \<langle>((pc' - Suc (length P\<^sub>e), map fst P), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C")
      prefer 2
      using while_pc_eval_helper apply force
     apply clarsimp
     apply(subgoal_tac "regrec_mem_consistent (snd (P ! (pc - Suc (length P\<^sub>e)))) regs mem\<^sub>C")
      prefer 2
      using RL.eval\<^sub>r_bounds apply fastforce
     apply force
    apply(force simp:regrec_mem_consistent_def)
   (* Case: at the Jmp *)
   apply(case_tac "pc = (Suc (length P\<^sub>e + (length P)))")
    apply(frule_tac l=lp in RL.jmp_dest[rotated])
     apply(simp add:nth_append split del:if_split)+
    apply(clarsimp simp:regrec_mem_consistent_def)
    apply(case_tac "P\<^sub>e = []")
     apply clarsimp
     apply(fastforce dest:compile_expr_emptyD)
    (* First confirm that lp is at pc 0 *)
    apply(subgoal_tac "RL.res\<^sub>P lp (map fst P\<^sub>e) = 0")
     prefer 2
     using compile_expr_input_label_res\<^sub>P apply force
    apply(subgoal_tac "RL.res\<^sub>P lp (map fst P\<^sub>e @ (None, Jz ex r) # map fst P @ [(l', Jmp lp)]) = 0")
     prefer 2
     using RL.res\<^sub>P_ib_append apply blast
    apply clarsimp
    apply(frule compile_expr_first_comprec_input)
     apply force
    apply clarsimp
    apply(force simp:nth_append)
   apply force
  (* Stop *)
  by force

end
end