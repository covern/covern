(*
Author: Robert Sison
*)

theory RISCCompiledReachability
imports RISCCompilation
  "../old-rg-sec/Havoc"
  "~~/src/HOL/Eisbach/Eisbach_Tools"
begin

context risc_compilation
begin

subsection {* Reachability properties for While and RISC programs *}

lemma no_jumps_loc_reach_sig_bounds:
  "RL.jump_labels P = {} \<Longrightarrow>
   RL.loc_reach_sig \<langle>((pc, P), regs), mds, mem\<rangle>\<^sub>C \<langle>((pc', P), regs'), mds', mem'\<rangle>\<^sub>C sig\<^sub>C \<Longrightarrow>
   pc \<le> pc'"
  apply(induct sig\<^sub>C arbitrary:pc regs mds mem)
   apply(force simp:RL.lc_havoc_eq_def)
  apply(clarsimp simp:RL.lc_havoc_eq_def)
  using RL.no_jumps_eval\<^sub>r_only_stays_or_increments
  by (metis Suc_le_lessD RL.eval\<^sub>r_no_modify less_imp_le_nat)

lemma reach\<^sub>r_preserves_no_lock_mds:
  "RL.loc_reach_sig \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', P'), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C sig\<^sub>C \<Longrightarrow>
   no_lock_mds mds\<^sub>C \<Longrightarrow>
   no_lock_mds mds\<^sub>C'"
  apply(induct sig\<^sub>C arbitrary:pc regs mds\<^sub>C mem\<^sub>C)
   apply(force simp:RL.lc_havoc_eq_def)
  apply(clarsimp simp:RL.lc_havoc_eq_def)
  apply(frule RL.eval\<^sub>r_no_modify)
  apply clarsimp
  apply(rename_tac pc\<^sub>0 regs\<^sub>0 mds\<^sub>0 mem\<^sub>0 sig\<^sub>C mem\<^sub>C pc\<^sub>1 regs\<^sub>1 mds\<^sub>1 mem\<^sub>1)
  apply(erule_tac x=pc\<^sub>1 in meta_allE)
  apply(erule_tac x=regs\<^sub>1 in meta_allE)
  apply(erule_tac x=mds\<^sub>1 in meta_allE)
  apply(erule_tac x=mem\<^sub>1 in meta_allE)
  apply clarsimp
  apply(frule RL.eval\<^sub>r_preserves_no_lock_mds)
   apply blast
  by blast

lemma reach\<^sub>r_preserves_lock_consistent_mds:
  "RL.loc_reach_sig \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', P'), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C sig\<^sub>C \<Longrightarrow>
   lock_consistent_mds lock_interp mds\<^sub>C \<Longrightarrow>
   lock_consistent_mds lock_interp mds\<^sub>C'"
  apply(induct sig\<^sub>C arbitrary:pc regs mds\<^sub>C mem\<^sub>C)
   apply(force simp:RL.lc_havoc_eq_def)
  apply(clarsimp simp:RL.lc_havoc_eq_def)
  apply(frule RL.eval\<^sub>r_no_modify)
  apply clarsimp
  apply(rename_tac pc\<^sub>0 regs\<^sub>0 mds\<^sub>0 mem\<^sub>0 sig\<^sub>C mem\<^sub>C pc\<^sub>1 regs\<^sub>1 mds\<^sub>1 mem\<^sub>1)
  apply(erule_tac x=pc\<^sub>1 in meta_allE)
  apply(erule_tac x=regs\<^sub>1 in meta_allE)
  apply(erule_tac x=mds\<^sub>1 in meta_allE)
  apply(erule_tac x=mem\<^sub>1 in meta_allE)
  apply clarsimp
  apply(frule RL.eval\<^sub>r_preserves_lock_consistent_mds)
     using RL.lone_lock_per_var apply blast
    using lock_interp_no_overlap apply blast
   apply blast
  by blast

(* Redefine abbreviations to avoid ambiguities because the memory types are now the same.
  This is less complicated than extracting Havoc from the security locale, as it turns out,
  because despite the memory types being the same now, the configurations are still distinct. *)

abbreviation mds_havoc_eq\<^sub>r ::
  "('Lock, 'Var) Var Mds \<Rightarrow> (('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> (_, _) Mem \<Rightarrow> bool"
  ("_ =\<index>\<^sup>r _" [100, 100] 80)
where
  "mem\<^sub>C =\<^bsub>mds\<^sub>C\<^esub>\<^sup>r mem\<^sub>C' \<equiv> RL.mds_havoc_eq mds\<^sub>C mem\<^sub>C mem\<^sub>C'"

abbreviation mds_havoc_eq\<^sub>w ::
  "('Lock, 'Var) Var Mds \<Rightarrow> (('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> (_, _) Mem \<Rightarrow> bool"
  ("_ =\<index>\<^sup>w _" [100, 100] 80)
where
  "mem =\<^bsub>mds\<^esub>\<^sup>w mem' \<equiv> WL.mds_havoc_eq mds mem mem'"

lemma loc_reach_sig_haltD:
  "RL.loc_reach_sig \<langle>((pc, P), regs), mds, mem\<rangle>\<^sub>C \<langle>((pc', P'), regs'), mds', mem'\<rangle>\<^sub>C sig \<Longrightarrow>
   pc = length P \<Longrightarrow>
   sig = [] \<and> pc' = length P \<and> P' = P \<and> regs' = regs \<and> mds' = mds \<and> mem' =\<^bsub>mds\<^esub>\<^sup>r mem"
  apply(subgoal_tac "sig = []")
   prefer 2
   apply(rule ccontr)
   apply(case_tac sig)
    apply force
   apply(force simp:RL.lc_havoc_eq_def RL.eval\<^sub>r_oob_pc_halt)
  by (clarsimp simp:RL.lc_havoc_eq_def RL.mds_havoc_eq_sym)

lemma loc_reach_sig_no_modify_pmem:
  "RL.loc_reach_sig \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', P'), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C sig\<^sub>C \<Longrightarrow>
   P' = P"
  apply(induct sig\<^sub>C arbitrary:pc P regs mds\<^sub>C mem\<^sub>C)
   apply(force simp:RL.lc_havoc_eq_def)
  apply(clarsimp simp:RL.lc_havoc_eq_def)
  using RL.eval\<^sub>r_no_modify
  by blast

lemma loc_reach_sig\<^sub>A_no_modify_modes:
  "\<not> has_locking c \<Longrightarrow>
   WL.loc_reach_sig \<langle>c, mds, mem\<rangle>\<^sub>A \<langle>c', mds', mem'\<rangle>\<^sub>A sig \<Longrightarrow>
   mds' = mds"
  apply(induct sig arbitrary:c c' mds mem)
   apply(clarsimp simp:WL.lc_havoc_eq_def)
  apply(clarsimp simp:WL.lc_havoc_eq_def)
  apply(frule no_locking_eval\<^sub>w_no_modes, simp)
  apply clarsimp
  apply(rename_tac c mem''' sig c' mem c'' mds mem'')
  using no_locking_eval\<^sub>w_inv by blast

lemma loc_reach_sig_no_backtrack_over_join:
  "RL.joinable P\<^sub>1 P\<^sub>2 \<Longrightarrow>
   pc \<ge> length P\<^sub>1 \<Longrightarrow>
   RL.loc_reach_sig \<langle>((pc, P\<^sub>1 @ P\<^sub>2), regs), mds, mem\<rangle>\<^sub>C \<langle>((pc', P\<^sub>1 @ P\<^sub>2), regs'), mds', mem'\<rangle>\<^sub>C sig\<^sub>C \<Longrightarrow>
   pc' \<ge> length P\<^sub>1"
  apply(induct sig\<^sub>C arbitrary:pc pc' regs regs' mem mds)
   apply(clarsimp simp:RL.lc_havoc_eq_def)
  apply(clarsimp simp:RL.lc_havoc_eq_def)
  using RL.eval\<^sub>r_no_backtrack_over_join RL.joinable_def
  by (metis RL.eval\<^sub>r_no_modify)

lemma loc_reach_sig_pc_bounds:
  "RL.loc_reach_sig \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C \<langle>((pc', P'), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C sig\<^sub>C \<Longrightarrow>
   pc \<le> length P \<Longrightarrow>
   pc' \<le> length P'"
  apply(induct sig\<^sub>C arbitrary:pc P regs mds\<^sub>C mem\<^sub>C pc')
   apply(clarsimp simp:RL.lc_havoc_eq_def)
  apply(clarsimp simp:RL.lc_havoc_eq_def)
  by (metis RL.eval\<^sub>r_no_modify RL.eval\<^sub>r_bounds)

lemma loc_reach_sig_stopD:
  "WL.loc_reach_sig \<langle>Stop, mds, mem\<rangle>\<^sub>A \<langle>c', mds', mem'\<rangle>\<^sub>A sig\<^sub>A \<Longrightarrow>
   c' = Stop \<and> mds = mds' \<and> mem =\<^bsub>mds\<^esub>\<^sup>w mem' \<and> sig\<^sub>A = []"
  apply(induct sig\<^sub>A)
   apply(clarsimp simp:WL.lc_havoc_eq_def)
  apply(clarsimp simp:WL.lc_havoc_eq_def)
  done

subsection {* While-RISC local configuration pairs and their pairwise execution paths *}

definition lc\<^sub>A_of
where
  "lc\<^sub>A_of lc\<^sub>C c\<^sub>A \<equiv> \<langle>c\<^sub>A, mds\<^sub>A_of (snd (fst lc\<^sub>C)), mem\<^sub>A_of (snd lc\<^sub>C)\<rangle>\<^sub>A"

(* NB: This doesn't check that each entry lines up (via eval and mds_havoc_eq) with its successor.
  loc_reach_sig will be responsible for that. *)
fun is_sig\<^sub>A_of ::
  "(('Var, 'Lock, ('Val, 'Var, 'OpId) exp, ('Val, 'Var, 'OpId) exp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf list \<Rightarrow>
   (('Var, 'Lock, ('Val, 'Var, 'OpId) exp, ('Val, 'Var, 'OpId) exp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
   (('Reg, 'Var, 'Lock, 'Val, 'OpId) RL.PerThread, ('Lock, 'Var) Var, 'Val) LocalConf list \<Rightarrow>
   bool"
where
  "is_sig\<^sub>A_of (lc\<^sub>A#sig\<^sub>A) dst (lc\<^sub>C#sig\<^sub>C) =
    (* abort if sig\<^sub>C contains a halting configuration *)
    ((fst (fst (fst (fst lc\<^sub>C))) < length (snd (fst (fst (fst lc\<^sub>C))))) \<and>
     (if abs_steps lc\<^sub>A lc\<^sub>C = 0 (* We're going to assume here that if abs_steps isn't 0, it's 1 *)
      then is_sig\<^sub>A_of (lc\<^sub>A#sig\<^sub>A) dst sig\<^sub>C
      else lc\<^sub>A = lc\<^sub>A_of lc\<^sub>C (fst (fst lc\<^sub>A)) \<and> is_sig\<^sub>A_of sig\<^sub>A dst sig\<^sub>C))" |
  "is_sig\<^sub>A_of [] dst (lc\<^sub>C#sig\<^sub>C) =
    ((fst (fst (fst (fst lc\<^sub>C))) < length (snd (fst (fst (fst lc\<^sub>C))))) \<and>
     abs_steps dst lc\<^sub>C = 0 \<and> is_sig\<^sub>A_of [] dst sig\<^sub>C)" |
  "is_sig\<^sub>A_of sig\<^sub>A _ [] = (sig\<^sub>A = [])"

lemma is_sig\<^sub>A_of_append:
  "is_sig\<^sub>A_of sig\<^sub>A mid sig\<^sub>C \<Longrightarrow>
   if sig\<^sub>A' = [] then WL.lc_havoc_eq mid dst else WL.lc_havoc_eq mid (hd sig\<^sub>A') \<Longrightarrow>
   is_sig\<^sub>A_of sig\<^sub>A' dst sig\<^sub>C' \<Longrightarrow>
   is_sig\<^sub>A_of (sig\<^sub>A @ sig\<^sub>A') dst (sig\<^sub>C @ sig\<^sub>C')"
  apply(induct sig\<^sub>A mid sig\<^sub>C rule:is_sig\<^sub>A_of.induct)
  apply clarsimp
  apply simp_all
  apply(clarsimp split:if_splits)
   apply(case_tac dst)
   apply(force simp:WL.lc_havoc_eq_def)
  apply(case_tac sig\<^sub>A')
   apply clarsimp
  apply(clarsimp simp:lc\<^sub>A_of_def WL.lc_havoc_eq_def)
  done

(* Trivial now that the memories are the same *)
lemma mds_havoc_eq\<^sub>A_of:
  "mem\<^sub>C =\<^bsub>mds\<^sub>C\<^esub>\<^sup>r mem\<^sub>C' \<Longrightarrow> mem\<^sub>A_of mem\<^sub>C =\<^bsub>mds\<^sub>A_of mds\<^sub>C\<^esub>\<^sup>w mem\<^sub>A_of mem\<^sub>C'"
  unfolding mem\<^sub>A_of_def mds\<^sub>A_of_def WL.mds_havoc_eq_def var_asm_not_written_def
  by simp

(* Pairwise reachability of While-RISC local configuration pairs *)
definition reachable_pair :: "
  ((('Var, 'Lock, ('Val, 'Var, 'OpId) exp, ('Val, 'Var, 'OpId) exp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf
   \<times>
   ((('Reg, 'Var, 'Lock, 'Val, 'OpId) RL.PerThread), ('Lock, 'Var) Var, 'Val) LocalConf)
  \<Rightarrow>
  ((('Var, 'Lock, ('Val, 'Var, 'OpId) exp, ('Val, 'Var, 'OpId) exp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf
   \<times>
   ((('Reg, 'Var, 'Lock, 'Val, 'OpId) RL.PerThread), ('Lock, 'Var) Var, 'Val) LocalConf)
  \<Rightarrow>
  ((('Reg, 'Var, 'Lock, 'Val, 'OpId) RL.PerThread), ('Lock, 'Var) Var, 'Val) LocalConf list
  \<Rightarrow> bool"
where
  "reachable_pair p p' sig\<^sub>C \<equiv> \<exists> sig\<^sub>A.
                          let (lc\<^sub>A, lc\<^sub>C) = p; (lc\<^sub>A', lc\<^sub>C') = p' in
                          RL.loc_reach_sig lc\<^sub>C lc\<^sub>C' sig\<^sub>C \<and>
                          (* This checks that configs line up all the way through *)
                          is_sig\<^sub>A_of sig\<^sub>A lc\<^sub>A' sig\<^sub>C \<and>
                          WL.loc_reach_sig lc\<^sub>A lc\<^sub>A' sig\<^sub>A \<and>
                          (* This checks that the final configs line up,
                             which isn't covered by is_sig\<^sub>A_of *)
                          lc\<^sub>A' = lc\<^sub>A_of lc\<^sub>C' (fst (fst lc\<^sub>A'))"

lemma reachable_pair_self:
  "reachable_pair (\<langle>c\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C)
                  (\<langle>c\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) []"
  unfolding reachable_pair_def
  by (clarsimp simp:WL.lc_havoc_eq_def RL.lc_havoc_eq_def mds_havoc_eq\<^sub>A_of lc\<^sub>A_of_def RL.mds_havoc_eq_refl)

lemma reachable_pair_mem_diff:
  "mem\<^sub>C =\<^bsub>mds\<^sub>C\<^esub>\<^sup>r mem\<^sub>C' \<Longrightarrow>
   reachable_pair (\<langle>c\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C)
                  (\<langle>c\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) []"
  unfolding reachable_pair_def
  by (clarsimp simp:WL.lc_havoc_eq_def RL.lc_havoc_eq_def mds_havoc_eq\<^sub>A_of lc\<^sub>A_of_def)

lemma reachable_pair_no_modify_pmem:
  "reachable_pair (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C)
       (\<langle>c\<^sub>A', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>((pc', P'), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) sig\<^sub>C \<Longrightarrow>
   P' = P"
  by (clarsimp simp:reachable_pair_def loc_reach_sig_no_modify_pmem)

(* If pair B is reachable from A, then every C reachable from B is also reachable from A *)
lemma reachable_pair_trans:
  "reachable_pair p p' sig\<^sub>C \<Longrightarrow> reachable_pair p' p'' sig\<^sub>C' \<Longrightarrow> reachable_pair p p'' (sig\<^sub>C @ sig\<^sub>C')"
  unfolding reachable_pair_def
  apply clarsimp
  apply(frule RL.loc_reach_sig_append, simp)
  apply(frule WL.loc_reach_sig_append, simp)
  apply clarsimp
  apply(rule_tac x="sig\<^sub>A@sig\<^sub>A'" in exI)
  apply simp
  apply(rename_tac sig\<^sub>A c\<^sub>A mds\<^sub>A mem\<^sub>A pc P regs mds\<^sub>C mem\<^sub>C c\<^sub>A' mds\<^sub>A' mem\<^sub>A' pc' P' regs' mds\<^sub>C' mem\<^sub>C' sig\<^sub>A' c\<^sub>A'' mds\<^sub>A'' mem\<^sub>A'' pc'' P'' regs'' mds\<^sub>C'' mem\<^sub>C'')
  apply(subgoal_tac "if sig\<^sub>A' = [] then WL.lc_havoc_eq (fst p') (fst p'') else WL.lc_havoc_eq (fst p') (hd sig\<^sub>A')")
   prefer 2
   apply clarsimp
   apply(rule conjI)
    apply clarsimp
   apply clarsimp
   apply(metis WL.loc_reach_sig.simps(2) list.sel(1) neq_Nil_conv)
  using is_sig\<^sub>A_of_append
  by (metis fst_conv)

lemma reachable_pair_no_backtrack_over_join:
  "RL.joinable P\<^sub>1 P\<^sub>2 \<Longrightarrow>
   pc \<ge> length P\<^sub>1 \<Longrightarrow>
   reachable_pair (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P\<^sub>1 @ P\<^sub>2), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C)
       (\<langle>c\<^sub>A', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>((pc', P\<^sub>1 @ P\<^sub>2), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) sig\<^sub>C \<Longrightarrow>
   pc' \<ge> length P\<^sub>1"
  apply(clarsimp simp:reachable_pair_def lc\<^sub>A_of_def)
  using loc_reach_sig_no_backtrack_over_join by blast

lemma reachable_pair_pc_bounds:
  "reachable_pair (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C)
       (\<langle>c\<^sub>A', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>((pc', P'), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) sig\<^sub>C \<Longrightarrow>
   pc \<le> length P \<Longrightarrow>
   pc' \<le> length P'"
  by (clarsimp simp:reachable_pair_def loc_reach_sig_pc_bounds)

lemma reachable_pair_stopD:
  "reachable_pair (\<langle>Stop, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>((pc, P), regs), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C)
       (\<langle>c\<^sub>A', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>((pc', P'), regs'), mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) sig\<^sub>C \<Longrightarrow>
   c\<^sub>A' = Stop \<and> mds\<^sub>A' = mds\<^sub>A \<and> mem\<^sub>A =\<^bsub>mds\<^sub>A\<^esub>\<^sup>w mem\<^sub>A'"
  apply(clarsimp simp:reachable_pair_def lc\<^sub>A_of_def)
  using loc_reach_sig_stopD by blast

end
end