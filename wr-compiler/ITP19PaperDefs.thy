(*
Author: Robert Sison
Based on the Dependent_SIFUM_(Refinement|Type_Systems) AFP entries, whose authors are:
  Toby Murray, Robert Sison, Edward Pierzchalski, Christine Rizkallah
Dependent_SIFUM_Type_Systems was based on the SIFUM_Type_Systems AFP entry, whose authors are:
  Sylvia Grewe, Heiko Mantel, Daniel Schoepe
*)

theory ITP19PaperDefs
imports "../old-rg-sec/Security"
        "../refinement/CompositionalRefinement"
begin

context sifum_security_init begin

(* Some nitty-gritty details of the proof that paper vs original definitions of
  closedness over globally consistent changes are the same.
  Scroll down to string "Definition 1" to skip to paper content. *)

definition 
  closed_glob_consistent_paper1 :: "(('Com, 'Var, 'Val) LocalConf) rel \<Rightarrow> bool"
where
  "closed_glob_consistent_paper1 \<R> =
  (\<forall> c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mem\<^sub>2. (\<langle> c\<^sub>1, mds, mem\<^sub>1 \<rangle>, \<langle> c\<^sub>2, mds, mem\<^sub>2 \<rangle>) \<in> \<R> \<longrightarrow>
   (\<forall> mem\<^sub>1' mem\<^sub>2'. ((\<forall>x. (mem\<^sub>1 x \<noteq> mem\<^sub>1' x \<or> mem\<^sub>2 x \<noteq> mem\<^sub>2' x) \<longrightarrow> \<not> var_asm_not_written mds x) \<and>
          (\<forall>x. dma mem\<^sub>1' x \<noteq> dma mem\<^sub>1 x \<longrightarrow> \<not> var_asm_not_written mds x) \<and>
          (\<forall>x. dma mem\<^sub>1' x = Low \<and> (x \<notin> mds AsmNoReadOrWrite \<or> x \<in> \<C>) \<longrightarrow> mem\<^sub>1' x = mem\<^sub>2' x)) \<longrightarrow>
         (\<langle> c\<^sub>1, mds, mem\<^sub>1' \<rangle>, \<langle> c\<^sub>2, mds, mem\<^sub>2' \<rangle>) \<in> \<R>))"

lemma cg_consistent_paper1:
  "closed_glob_consistent = closed_glob_consistent_paper1"
  apply(rule ext)
  unfolding closed_glob_consistent_def closed_glob_consistent_paper1_def
  apply clarsimp
  apply(rename_tac \<R>)
  apply(rule iffI)
   apply clarsimp
   apply(erule_tac x=c\<^sub>1 in allE)
   apply(erule_tac x=mds in allE)
   apply(erule_tac x=mem\<^sub>1 in allE)
   apply(erule_tac x=c\<^sub>2 in allE)
   apply(erule_tac x=mem\<^sub>2 in allE)
   apply clarsimp
   apply(erule_tac x="\<lambda> y. if mem\<^sub>1' y = mem\<^sub>1 y \<and> mem\<^sub>2' y = mem\<^sub>2 y then None else Some (mem\<^sub>1' y, mem\<^sub>2' y)" in allE)
   apply(clarsimp split:if_splits)
   apply(clarsimp simp:apply_adaptation_def split:if_splits)
   apply(erule impE)
    apply(rule conjI)
     apply(rule allI)
     apply(rule not_mono)
     apply clarsimp
     subgoal proof -
     fix \<R> :: "((('Com \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) \<times> ('Com \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) set" and c\<^sub>1 :: 'Com and mds :: "Mode \<Rightarrow> 'Var set" and mem\<^sub>1 :: "'Var \<Rightarrow> 'Val" and c\<^sub>2 :: 'Com and mem\<^sub>2 :: "'Var \<Rightarrow> 'Val" and mem\<^sub>1' :: "'Var \<Rightarrow> 'Val" and mem\<^sub>2' :: "'Var \<Rightarrow> 'Val" and x :: 'Var
       assume a1: "\<forall>x. dma mem\<^sub>1' x \<noteq> dma mem\<^sub>1 x \<longrightarrow> \<not> var_asm_not_written mds x"
       assume a2: "var_asm_not_written mds x"
       have f3: "\<forall>v f fa. (\<forall>va. va \<in> \<C>_vars v \<longrightarrow> f va = fa va) \<longrightarrow> dma f v = dma fa v"
         by (metis dma_\<C>_vars)
       obtain vv :: "('Var \<Rightarrow> 'Val) \<Rightarrow> ('Var \<Rightarrow> 'Val) \<Rightarrow> 'Var \<Rightarrow> 'Var" where
         "\<forall>x0 x1 x2. (\<exists>v3. v3 \<in> \<C>_vars x2 \<and> x1 v3 \<noteq> x0 v3) = (vv x0 x1 x2 \<in> \<C>_vars x2 \<and> x1 (vv x0 x1 x2) \<noteq> x0 (vv x0 x1 x2))"
         by moura
       then have f4: "\<forall>v f fa. vv fa f v \<in> \<C>_vars v \<and> f (vv fa f v) \<noteq> fa (vv fa f v) \<or> dma f v = dma fa v"
         using f3 by meson
       have "vv (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) mem\<^sub>1' x \<notin> \<C>_vars x \<or> mem\<^sub>1' (vv (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) mem\<^sub>1' x) = (case if mem\<^sub>1' (vv (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) mem\<^sub>1' x) = mem\<^sub>1 (vv (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) mem\<^sub>1' x) \<and> mem\<^sub>2' (vv (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) mem\<^sub>1' x) = mem\<^sub>2 (vv (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) mem\<^sub>1' x) then None else Some (mem\<^sub>1' (vv (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) mem\<^sub>1' x), mem\<^sub>2' (vv (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) mem\<^sub>1' x)) of None \<Rightarrow> mem\<^sub>1 (vv (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) mem\<^sub>1' x) | Some (v, va) \<Rightarrow> if True then v else va)"
         by (simp add: old.prod.case)
       then have "dma mem\<^sub>1' x = dma (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x"
         using f4 by blast
       then show "dma (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x = dma mem\<^sub>1 x"
         using a2 a1 by force
     qed
    apply(rule allI)
    apply(rule conjI)
     apply clarsimp
     subgoal proof -
     fix \<R> :: "((('Com \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) \<times> ('Com \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) set" and c\<^sub>1 :: 'Com and mds :: "Mode \<Rightarrow> 'Var set" and mem\<^sub>1 :: "'Var \<Rightarrow> 'Val" and c\<^sub>2 :: 'Com and mem\<^sub>2 :: "'Var \<Rightarrow> 'Val" and mem\<^sub>1' :: "'Var \<Rightarrow> 'Val" and mem\<^sub>2' :: "'Var \<Rightarrow> 'Val" and x :: 'Var
     assume a1: "\<forall>x. dma mem\<^sub>1' x = Low \<and> (x \<in> mds AsmNoReadOrWrite \<longrightarrow> x \<in> \<C>) \<longrightarrow> mem\<^sub>1' x = mem\<^sub>2' x"
     assume a2: "mem\<^sub>1 x \<noteq> mem\<^sub>2 x"
     assume a3: "mem\<^sub>1' x = mem\<^sub>1 x"
     assume a4: "mem\<^sub>2' x = mem\<^sub>2 x"
       assume a5: "dma (\<lambda>x. case if mem\<^sub>1' x = mem\<^sub>1 x \<and> mem\<^sub>2' x = mem\<^sub>2 x then None else Some (mem\<^sub>1' x, mem\<^sub>2' x) of None \<Rightarrow> mem\<^sub>1 x | Some (xa, xb) \<Rightarrow> if True then xa else xb) x = Low"
       obtain vv :: "('Var \<Rightarrow> 'Val) \<Rightarrow> ('Var \<Rightarrow> 'Val) \<Rightarrow> 'Var \<Rightarrow> 'Var" where
         "\<forall>x0 x1 x2. (\<exists>v3. v3 \<in> \<C>_vars x2 \<and> x1 v3 \<noteq> x0 v3) = (vv x0 x1 x2 \<in> \<C>_vars x2 \<and> x1 (vv x0 x1 x2) \<noteq> x0 (vv x0 x1 x2))"
         by moura
       then have f6: "\<forall>v f fa. vv fa f v \<in> \<C>_vars v \<and> f (vv fa f v) \<noteq> fa (vv fa f v) \<or> dma f v = dma fa v"
         by (meson dma_\<C>_vars)
       have "(case if mem\<^sub>1' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) = mem\<^sub>1 (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) \<and> mem\<^sub>2' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) = mem\<^sub>2 (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) then None else Some (mem\<^sub>1' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x), mem\<^sub>2' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x)) of None \<Rightarrow> mem\<^sub>1 (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) | Some (v, va) \<Rightarrow> if True then v else va) \<noteq> mem\<^sub>1' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) \<longrightarrow> (case if mem\<^sub>1' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) = mem\<^sub>1 (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) \<and> mem\<^sub>2' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) = mem\<^sub>2 (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) then None else Some (mem\<^sub>1' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x), mem\<^sub>2' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x)) of None \<Rightarrow> mem\<^sub>1 (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) | Some (v, va) \<Rightarrow> if True then v else va) = mem\<^sub>1' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x)"
         by auto
       then have "dma (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x = dma mem\<^sub>1' x"
         using f6 by meson
       then show "x \<in> mds AsmNoReadOrWrite \<and> x \<notin> \<C>"
         using a5 a4 a3 a2 a1 by force
     qed
    apply clarsimp
    subgoal proof -
    fix \<R> :: "((('Com \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) \<times> ('Com \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) set" and c\<^sub>1 :: 'Com and mds :: "Mode \<Rightarrow> 'Var set" and mem\<^sub>1 :: "'Var \<Rightarrow> 'Val" and c\<^sub>2 :: 'Com and mem\<^sub>2 :: "'Var \<Rightarrow> 'Val" and mem\<^sub>1' :: "'Var \<Rightarrow> 'Val" and mem\<^sub>2' :: "'Var \<Rightarrow> 'Val" and x :: 'Var
    assume a1: "\<forall>x. dma mem\<^sub>1' x = Low \<and> (x \<in> mds AsmNoReadOrWrite \<longrightarrow> x \<in> \<C>) \<longrightarrow> mem\<^sub>1' x = mem\<^sub>2' x"
    assume a2: "mem\<^sub>1' x \<noteq> mem\<^sub>2' x"
    assume a3: "dma (\<lambda>x. case if mem\<^sub>1' x = mem\<^sub>1 x \<and> mem\<^sub>2' x = mem\<^sub>2 x then None else Some (mem\<^sub>1' x, mem\<^sub>2' x) of None \<Rightarrow> mem\<^sub>1 x | Some (xa, xb) \<Rightarrow> if True then xa else xb) x = Low"
    obtain vv :: "('Var \<Rightarrow> 'Val) \<Rightarrow> ('Var \<Rightarrow> 'Val) \<Rightarrow> 'Var \<Rightarrow> 'Var" where
      "\<forall>x0 x1 x2. (\<exists>v3. v3 \<in> \<C>_vars x2 \<and> x1 v3 \<noteq> x0 v3) = (vv x0 x1 x2 \<in> \<C>_vars x2 \<and> x1 (vv x0 x1 x2) \<noteq> x0 (vv x0 x1 x2))"
      by moura
    then have f4: "\<forall>v f fa. vv fa f v \<in> \<C>_vars v \<and> f (vv fa f v) \<noteq> fa (vv fa f v) \<or> dma f v = dma fa v"
      by (meson dma_\<C>_vars)
    have "(case if mem\<^sub>1' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) = mem\<^sub>1 (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) \<and> mem\<^sub>2' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) = mem\<^sub>2 (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) then None else Some (mem\<^sub>1' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x), mem\<^sub>2' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x)) of None \<Rightarrow> mem\<^sub>1 (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) | Some (v, va) \<Rightarrow> if True then v else va) \<noteq> mem\<^sub>1' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) \<longrightarrow> (case if mem\<^sub>1' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) = mem\<^sub>1 (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) \<and> mem\<^sub>2' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) = mem\<^sub>2 (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) then None else Some (mem\<^sub>1' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x), mem\<^sub>2' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x)) of None \<Rightarrow> mem\<^sub>1 (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x) | Some (v, va) \<Rightarrow> if True then v else va) = mem\<^sub>1' (vv mem\<^sub>1' (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x)"
      by auto
    then have "dma (\<lambda>v. case if mem\<^sub>1' v = mem\<^sub>1 v \<and> mem\<^sub>2' v = mem\<^sub>2 v then None else Some (mem\<^sub>1' v, mem\<^sub>2' v) of None \<Rightarrow> mem\<^sub>1 v | Some (v, va) \<Rightarrow> if True then v else va) x = dma mem\<^sub>1' x"
      using f4 by meson
    then show "x \<in> mds AsmNoReadOrWrite \<and> x \<notin> \<C>"
      using a3 a2 a1 by force
    qed
   apply(erule Set.set_insert)
   apply clarsimp
   apply(erule disjE)
    apply(force dest:fun_cong split:if_splits)
   apply(erule Set.set_insert)
   apply clarsimp
   apply auto[1]
  apply clarsimp
  apply(erule_tac x=c\<^sub>1 in allE)
  apply(erule_tac x=mds in allE)
  apply(erule_tac x=mem\<^sub>1 in allE)
  apply(erule_tac x=c\<^sub>2 in allE)
  apply(erule_tac x=mem\<^sub>2 in allE)
  apply clarsimp
  apply(erule_tac x="mem\<^sub>1 [\<parallel>\<^sub>1 A]" in allE)
  apply(erule_tac x="mem\<^sub>2 [\<parallel>\<^sub>2 A]" in allE)
  apply clarsimp
  apply(clarsimp simp:apply_adaptation_def split:if_splits)
  apply(erule impE)
    subgoal proof -
     fix \<R> :: "((('Com \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) \<times> ('Com \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) set" and c\<^sub>1 :: 'Com and mds :: "Mode \<Rightarrow> 'Var set" and mem\<^sub>1 :: "'Var \<Rightarrow> 'Val" and c\<^sub>2 :: 'Com and mem\<^sub>2 :: "'Var \<Rightarrow> 'Val" and A :: "'Var \<Rightarrow> ('Val \<times> 'Val) option" and x :: 'Var
     assume a1: "\<forall>x. case A x of None \<Rightarrow> True | Some (v, v') \<Rightarrow> mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> v' \<longrightarrow> \<not> var_asm_not_written mds x"
     assume a2: "var_asm_not_written mds x"
     obtain pp :: "('Val \<times> 'Val) option \<Rightarrow> ('Val \<times> 'Val \<Rightarrow> bool) \<Rightarrow> 'Val \<times> 'Val" where
       "\<forall>x0 x1. (\<exists>v3. x0 = Some v3 \<and> x1 v3) = (x0 = Some (pp x0 x1) \<and> x1 (pp x0 x1))"
       by moura
     then have "A x = None \<or> A x = Some (pp (A x) (\<lambda>(v, va). mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> va \<longrightarrow> \<not> var_asm_not_written mds x)) \<and> (case pp (A x) (\<lambda>(v, va). mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> va \<longrightarrow> \<not> var_asm_not_written mds x) of (v, va) \<Rightarrow> mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> va \<longrightarrow> \<not> var_asm_not_written mds x)"
       using a1 by (meson case_optionE)
     then show "mem\<^sub>1 x = (case A x of None \<Rightarrow> mem\<^sub>1 x | Some (xa, xb) \<Rightarrow> if True then xa else xb)"
       using a2 by force
   qed
  proof -
    fix \<R> :: "((('Com \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) \<times> ('Com \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) set" and c\<^sub>1 :: 'Com and mds :: "Mode \<Rightarrow> 'Var set" and mem\<^sub>1 :: "'Var \<Rightarrow> 'Val" and c\<^sub>2 :: 'Com and mem\<^sub>2 :: "'Var \<Rightarrow> 'Val" and A :: "'Var \<Rightarrow> ('Val \<times> 'Val) option" and x :: 'Var
    assume a1: "mem\<^sub>2 x \<noteq> (case A x of None \<Rightarrow> mem\<^sub>2 x | Some (xa, xb) \<Rightarrow> if False then xa else xb)"
    assume a2: "var_asm_not_written mds x"
    assume a3: "\<forall>x. case A x of None \<Rightarrow> True | Some (v, v') \<Rightarrow> mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> v' \<longrightarrow> \<not> var_asm_not_written mds x"
    have f4: "A x \<noteq> None"
      using a1 by (metis option.case_eq_if)
    have "(if A x = None then True else case the (A x) of (uua, uub) \<Rightarrow> mem\<^sub>1 x \<noteq> uua \<or> mem\<^sub>2 x \<noteq> uub \<longrightarrow> \<not> var_asm_not_written mds x) = (A x = None \<or> (case the (A x) of (uua, uub) \<Rightarrow> mem\<^sub>1 x \<noteq> uua \<or> mem\<^sub>2 x \<noteq> uub \<longrightarrow> \<not> var_asm_not_written mds x))"
      by presburger
    then have "(case A x of None \<Rightarrow> True | Some (v, va) \<Rightarrow> mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> va \<longrightarrow> \<not> var_asm_not_written mds x) = (A x = None \<or> (case the (A x) of (v, va) \<Rightarrow> mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> va \<longrightarrow> \<not> var_asm_not_written mds x))"
      by (simp add: option.case_eq_if)
    then show False
      using f4 a3 a2 a1 by auto
  qed

definition 
  closed_glob_consistent_paper2 :: "(('Com, 'Var, 'Val) LocalConf) rel \<Rightarrow> bool"
where
  "closed_glob_consistent_paper2 \<R> \<equiv>
   \<forall> tps\<^sub>1 mds mem\<^sub>1 tps\<^sub>2 mem\<^sub>2. (\<langle> tps\<^sub>1, mds, mem\<^sub>1 \<rangle>, \<langle> tps\<^sub>2, mds, mem\<^sub>2 \<rangle>) \<in> \<R> \<longrightarrow>
   (\<forall> mem\<^sub>1' mem\<^sub>2'. (\<forall>x. (mem\<^sub>1 x \<noteq> mem\<^sub>1' x \<or> mem\<^sub>2 x \<noteq> mem\<^sub>2' x \<or>
         dma mem\<^sub>1' x \<noteq> dma mem\<^sub>1 x) \<longrightarrow> \<not> var_asm_not_written mds x) \<and> low_mds_eq mds mem\<^sub>1' mem\<^sub>2' \<longrightarrow>
         (\<langle> tps\<^sub>1, mds, mem\<^sub>1' \<rangle>, \<langle> tps\<^sub>2, mds, mem\<^sub>2' \<rangle>) \<in> \<R>)"

lemma cg_consistent_paper1_paper2:
  "closed_glob_consistent_paper1 = closed_glob_consistent_paper2"
  unfolding closed_glob_consistent_paper1_def closed_glob_consistent_paper2_def low_mds_eq_def
  by blast

(* Definition 1: Low-equivalent memory modulo modes *)

abbreviation readable
where
  "readable mds x \<equiv> x \<notin> mds AsmNoReadOrWrite"

definition 
  low_mds_eq_paper :: "'Var Mds \<Rightarrow> ('Var, 'Val) Mem \<Rightarrow> (_, _) Mem \<Rightarrow> bool"
  ("_ =\<index>\<^sup>L\<^sup>o\<^sup>w _" [100, 100] 80)
where
  "mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>L\<^sup>o\<^sup>w mem\<^sub>2 \<equiv>
   \<forall> x. x \<in> \<C> \<or> dma mem\<^sub>1 x = Low \<and> readable mds x \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x"

lemma low_mds_eq_paper_original:
  "low_mds_eq_paper = low_mds_eq"
  unfolding low_mds_eq_def low_mds_eq_paper_def
  using \<C>_Low
  by blast

definition 
  lc_low_mds_eq :: "(('Com, 'Var, 'Val) LocalConf) \<Rightarrow> (('Com, 'Var, 'Val) LocalConf) \<Rightarrow> bool"
  ("_ =\<^sub>m\<^sub>d\<^sub>s\<^sup>L\<^sup>o\<^sup>w _" [100, 100] 80)
where
  "(lc\<^sub>1 =\<^sub>m\<^sub>d\<^sub>s\<^sup>L\<^sup>o\<^sup>w lc\<^sub>2) \<equiv> snd (fst lc\<^sub>1) = snd (fst lc\<^sub>2) \<and> low_mds_eq (snd (fst lc\<^sub>2)) (snd lc\<^sub>1) (snd lc\<^sub>2)"

definition 
  lc_same_mds :: "(('Com, 'Var, 'Val) LocalConf) \<Rightarrow> (('Com, 'Var, 'Val) LocalConf) \<Rightarrow> bool"
  ("_ =\<^sub>m\<^sub>d\<^sub>s _" [100, 100] 80)
where
  "(lc\<^sub>1 =\<^sub>m\<^sub>d\<^sub>s lc\<^sub>2) \<equiv> snd (fst lc\<^sub>1) = snd (fst lc\<^sub>2)"

(* Strong low bisimulations modulo modes.
  Note that we reference the original closed_glob_consistent here,
  but we prove the paper one equivalent to this one further below.
  -- See under string "Definition 3". *)
definition 
  strong_low_bisim_mm_paper :: "(('Com, 'Var, 'Val) LocalConf) rel \<Rightarrow> bool"
where
  "strong_low_bisim_mm_paper \<B> \<equiv> closed_glob_consistent \<B> \<and> sym \<B> \<and>
  (\<forall> lc\<^sub>1 lc\<^sub>2. (lc\<^sub>1, lc\<^sub>2) \<in> \<B> \<and> lc\<^sub>1 =\<^sub>m\<^sub>d\<^sub>s lc\<^sub>2 \<longrightarrow> lc\<^sub>1 =\<^sub>m\<^sub>d\<^sub>s\<^sup>L\<^sup>o\<^sup>w lc\<^sub>2 \<and>
    (\<forall> lc\<^sub>1'. lc\<^sub>1 \<leadsto> lc\<^sub>1' \<longrightarrow> (\<exists> lc\<^sub>2'. lc\<^sub>2 \<leadsto> lc\<^sub>2' \<and> lc\<^sub>1' =\<^sub>m\<^sub>d\<^sub>s lc\<^sub>2' \<and> (lc\<^sub>1', lc\<^sub>2') \<in> \<B>)))"

lemma strong_low_bisim_mm_paper_original:
  "strong_low_bisim_mm_paper = strong_low_bisim_mm"
  unfolding strong_low_bisim_mm_def strong_low_bisim_mm_paper_def lc_low_mds_eq_def lc_same_mds_def
  by force

(* Definition 2: Per-thread compositional CVDNI property *)

thm com_sifum_secure_def

(* For brevity the paper features a simplified presentation of com_sifum_secure
  with no custom requirements. *)
definition
  com_sifum_secure_paper :: "'Com \<times> 'Var Mds \<Rightarrow> bool"
where
  "com_sifum_secure_paper cmd \<equiv> let (tps, mds) = cmd in
   \<forall>mem\<^sub>1 mem\<^sub>2. mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>L\<^sup>o\<^sup>w mem\<^sub>2  \<longrightarrow>
     (\<exists>\<B>. strong_low_bisim_mm_paper \<B> \<and> (\<langle>tps, mds, mem\<^sub>1\<rangle>, \<langle>tps, mds, mem\<^sub>2\<rangle>) \<in> \<B>)"

lemma com_sifum_secure_paper_original:
assumes
  (* These two parameters to the theory allow the instantiator to impose on the bisimulation
    even stricter custom requirements not required by the general CVDNI security theory.
    For example, the "no-high-branching" requirement introduced in Section 5 of the paper
    (defined as bisim_simple_ev\<^sub>B_eq in WhileLockLanguage.thy) is assumed in the proofs of
    RISCRefinementSafe.thy to be imposed by the BISIM_REQS parameter for the While language,
    when instantiating the refinement theory for the wr-compiler. *)
  "INIT = (\<lambda>_. True)"
  "BISIM_REQS = (\<lambda>_. True)"
shows
  "com_sifum_secure_paper = com_sifum_secure"
  apply(rule ext)
  unfolding com_sifum_secure_paper_def com_sifum_secure_def
  apply clarsimp
  apply(clarsimp simp:strong_low_bisim_mm_paper_original low_mds_eq_paper_original)
  apply(rule iffI)
   unfolding low_indistinguishable_def
   apply(fastforce simp:assms)
  by (fastforce simp:assms)

(* Definition 3: Closedness under globally consistent changes *)

abbreviation writable
where
  "writable mds x \<equiv> \<not> var_asm_not_written mds x"

lemma "writable mds x \<equiv> x \<notin> mds AsmNoWrite \<and> x \<notin> mds AsmNoReadOrWrite"
  unfolding var_asm_not_written_def
  by simp

definition 
  closed_glob_consistent_paper :: "(('Com, 'Var, 'Val) LocalConf) rel \<Rightarrow> bool"
where
  "closed_glob_consistent_paper \<B> \<equiv> \<forall> tps\<^sub>1 mem\<^sub>1 tps\<^sub>2 mem\<^sub>2 mds.
       (\<langle> tps\<^sub>1, mds, mem\<^sub>1 \<rangle>, \<langle> tps\<^sub>2, mds, mem\<^sub>2 \<rangle>) \<in> \<B> \<longrightarrow>
       (\<forall> mem\<^sub>1' mem\<^sub>2'. (\<forall>x. (mem\<^sub>1 x \<noteq> mem\<^sub>1' x \<or> mem\<^sub>2 x \<noteq> mem\<^sub>2' x \<or>
         dma mem\<^sub>1 x \<noteq> dma mem\<^sub>1' x) \<longrightarrow> writable mds x) \<and> mem\<^sub>1' =\<^bsub>mds\<^esub>\<^sup>L\<^sup>o\<^sup>w mem\<^sub>2' \<longrightarrow>
       (\<langle> tps\<^sub>1, mds, mem\<^sub>1' \<rangle>, \<langle> tps\<^sub>2, mds, mem\<^sub>2' \<rangle>) \<in> \<B>)"

lemma cg_consistent_paper2_paper:
  "closed_glob_consistent_paper2 = closed_glob_consistent_paper"
  apply(rule ext)
  unfolding closed_glob_consistent_paper2_def closed_glob_consistent_paper_def
  apply(clarsimp simp:low_mds_eq_paper_original low_mds_eq_def)
  apply(rename_tac \<B>)
  apply(rule iffI)
   apply clarsimp
   apply(erule_tac x=tps\<^sub>1 in allE)
   apply(erule_tac x=mds in allE)
   apply(erule_tac x=mem\<^sub>1 in allE)
   apply(erule_tac x=tps\<^sub>2 in allE)
   apply(erule_tac x=mem\<^sub>2 in allE)
   apply metis
  apply clarsimp
  apply(erule_tac x=tps\<^sub>1 in allE)
  apply(erule_tac x=mem\<^sub>1 in allE)
  apply(erule_tac x=tps\<^sub>2 in allE)
  apply(erule_tac x=mem\<^sub>2 in allE)
  apply(erule_tac x=mds in allE)
  by metis

lemma "closed_glob_consistent = closed_glob_consistent_paper"
  using cg_consistent_paper1 cg_consistent_paper1_paper2 cg_consistent_paper2_paper
  by simp

end

context sifum_refinement_same_mem_init
begin

definition 
  lc_same_mds_mem :: "(('Com\<^sub>A, 'Var, 'Val) LocalConf) \<Rightarrow> (('Com\<^sub>C, 'Var, 'Val) LocalConf) \<Rightarrow> bool"
  ("_ =\<^sub>m\<^sub>d\<^sub>s\<^sup>m\<^sup>e\<^sup>m _" [100, 100] 80)
where
  "(lc\<^sub>1 =\<^sub>m\<^sub>d\<^sub>s\<^sup>m\<^sup>e\<^sup>m lc\<^sub>2) \<equiv> snd (fst lc\<^sub>1) = snd (fst lc\<^sub>2) \<and> snd lc\<^sub>1 = snd lc\<^sub>2"

definition
  coupling_inv_pres_original
where
  "coupling_inv_pres_original \<R>\<^sub>A \<R> P \<equiv>
  (\<forall> c\<^sub>1\<^sub>A mds\<^sub>A mem\<^sub>1\<^sub>A c\<^sub>1\<^sub>C mds\<^sub>C mem\<^sub>1\<^sub>C. 
   (\<langle> c\<^sub>1\<^sub>A, mds\<^sub>A, mem\<^sub>1\<^sub>A \<rangle>\<^sub>A, \<langle> c\<^sub>1\<^sub>C, mds\<^sub>C, mem\<^sub>1\<^sub>C \<rangle>\<^sub>C) \<in> \<R> \<longrightarrow>
    (\<forall> c\<^sub>1\<^sub>C' mds\<^sub>C' mem\<^sub>1\<^sub>C'. \<langle> c\<^sub>1\<^sub>C, mds\<^sub>C, mem\<^sub>1\<^sub>C \<rangle>\<^sub>C \<leadsto>\<^sub>C \<langle> c\<^sub>1\<^sub>C', mds\<^sub>C', mem\<^sub>1\<^sub>C' \<rangle>\<^sub>C \<longrightarrow>
     (\<exists> n c\<^sub>1\<^sub>A' mds\<^sub>A' mem\<^sub>1\<^sub>A'. abs.neval \<langle> c\<^sub>1\<^sub>A, mds\<^sub>A, mem\<^sub>1\<^sub>A \<rangle>\<^sub>A n \<langle> c\<^sub>1\<^sub>A', mds\<^sub>A', mem\<^sub>1\<^sub>A' \<rangle>\<^sub>A \<and>
                   (\<langle> c\<^sub>1\<^sub>A', mds\<^sub>A', mem\<^sub>1\<^sub>A' \<rangle>\<^sub>A, \<langle> c\<^sub>1\<^sub>C', mds\<^sub>C', mem\<^sub>1\<^sub>C' \<rangle>\<^sub>C) \<in> \<R> \<and>
       (\<forall>c\<^sub>2\<^sub>A mem\<^sub>2\<^sub>A c\<^sub>2\<^sub>C mem\<^sub>2\<^sub>C c\<^sub>2\<^sub>A' mem\<^sub>2\<^sub>A'. 
         (\<langle> c\<^sub>1\<^sub>A, mds\<^sub>A, mem\<^sub>1\<^sub>A \<rangle>\<^sub>A, \<langle> c\<^sub>2\<^sub>A, mds\<^sub>A, mem\<^sub>2\<^sub>A \<rangle>\<^sub>A) \<in> \<R>\<^sub>A \<and>
         (\<langle> c\<^sub>2\<^sub>A, mds\<^sub>A, mem\<^sub>2\<^sub>A \<rangle>\<^sub>A, \<langle> c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C \<rangle>\<^sub>C) \<in> \<R> \<and>
         (\<langle> c\<^sub>1\<^sub>C, mds\<^sub>C, mem\<^sub>1\<^sub>C \<rangle>\<^sub>C, \<langle> c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C \<rangle>\<^sub>C) \<in> P \<and>
         abs.neval \<langle> c\<^sub>2\<^sub>A, mds\<^sub>A, mem\<^sub>2\<^sub>A \<rangle>\<^sub>A n \<langle> c\<^sub>2\<^sub>A', mds\<^sub>A', mem\<^sub>2\<^sub>A' \<rangle>\<^sub>A  \<longrightarrow>
           (\<exists> c\<^sub>2\<^sub>C' mem\<^sub>2\<^sub>C'. \<langle> c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C \<rangle>\<^sub>C \<leadsto>\<^sub>C \<langle> c\<^sub>2\<^sub>C', mds\<^sub>C', mem\<^sub>2\<^sub>C' \<rangle>\<^sub>C \<and>
                   (\<langle> c\<^sub>2\<^sub>A', mds\<^sub>A', mem\<^sub>2\<^sub>A' \<rangle>\<^sub>A, \<langle> c\<^sub>2\<^sub>C', mds\<^sub>C', mem\<^sub>2\<^sub>C' \<rangle>\<^sub>C) \<in> \<R> \<and>
                   (\<langle> c\<^sub>1\<^sub>C', mds\<^sub>C', mem\<^sub>1\<^sub>C' \<rangle>\<^sub>C, \<langle> c\<^sub>2\<^sub>C', mds\<^sub>C', mem\<^sub>2\<^sub>C' \<rangle>\<^sub>C) \<in> P)))))"

definition
  secure_refinement_original :: "('Com\<^sub>A, 'Var, 'Val) LocalConf rel \<Rightarrow> ('Com\<^sub>A, 'Var, 'Val, 'Com\<^sub>C, 'Var) state_relation \<Rightarrow> 
                          ('Com\<^sub>C, 'Var, 'Val) LocalConf rel \<Rightarrow> bool"
where
  "secure_refinement_original \<R>\<^sub>A \<R> P \<equiv>
  closed_others \<R> \<and>
  preserves_modes_mem \<R> \<and>
  conc.closed_glob_consistent P \<and>
  coupling_inv_pres_original \<R>\<^sub>A \<R> P"

lemma "secure_refinement = secure_refinement_original"
  unfolding secure_refinement_def secure_refinement_original_def coupling_inv_pres_original_def
  apply(rule ext)+
  apply(rule iffI)
   apply clarsimp
   apply(rename_tac \<R>\<^sub>A \<R> P c\<^sub>1\<^sub>A mds\<^sub>A mem\<^sub>1\<^sub>A c\<^sub>1\<^sub>C mds\<^sub>C mem\<^sub>1\<^sub>C c\<^sub>1\<^sub>C' mds\<^sub>C' mem\<^sub>1\<^sub>C')
   apply(erule_tac x=c\<^sub>1\<^sub>A in allE)
   apply(erule_tac x=mds\<^sub>C in allE)
   apply(erule_tac x=mem\<^sub>1\<^sub>C in allE)
   apply(erule_tac x=c\<^sub>1\<^sub>C in allE)
   apply(metis preserves_modes_memD)
  apply clarsimp
  apply(rename_tac \<R>\<^sub>A \<R> P c\<^sub>1\<^sub>A mds mem\<^sub>1 c\<^sub>1\<^sub>C c\<^sub>1\<^sub>C' mds' mem\<^sub>1')
  apply(erule_tac x=c\<^sub>1\<^sub>A in allE)
  apply(erule_tac x=mds in allE)
  apply(erule_tac x=mem\<^sub>1 in allE)
  apply(erule_tac x=c\<^sub>1\<^sub>C in allE)
  apply(erule_tac x=mds in allE)
  apply(erule_tac x=mem\<^sub>1 in allE)
  apply clarsimp
  apply(erule_tac x=c\<^sub>1\<^sub>C' in allE)
  apply(erule_tac x=mds' in allE)
  apply(erule_tac x=mem\<^sub>1' in allE)
  by (metis preserves_modes_memD)

(* Figure 1: Coupling invariant preservation *)

definition
  coupling_inv_pres_paper
where
  "coupling_inv_pres_paper \<B> \<R> \<I> \<equiv>
   \<forall> lc\<^sub>1\<^sub>A lc\<^sub>1\<^sub>C. (lc\<^sub>1\<^sub>A, lc\<^sub>1\<^sub>C) \<in> \<R> \<longrightarrow>
    (\<forall> lc\<^sub>1\<^sub>C'. lc\<^sub>1\<^sub>C \<leadsto>\<^sub>C lc\<^sub>1\<^sub>C' \<longrightarrow>
     (\<exists> n lc\<^sub>1\<^sub>A'. abs.neval lc\<^sub>1\<^sub>A n lc\<^sub>1\<^sub>A' \<and> (lc\<^sub>1\<^sub>A', lc\<^sub>1\<^sub>C') \<in> \<R> \<and>
       (\<forall>lc\<^sub>2\<^sub>A lc\<^sub>2\<^sub>C lc\<^sub>2\<^sub>A'. (lc\<^sub>1\<^sub>A, lc\<^sub>2\<^sub>A) \<in> \<B> \<and> lc\<^sub>1\<^sub>A =\<^sub>m\<^sub>d\<^sub>s lc\<^sub>2\<^sub>A \<and>
         (lc\<^sub>2\<^sub>A, lc\<^sub>2\<^sub>C) \<in> \<R> \<and> (lc\<^sub>1\<^sub>C, lc\<^sub>2\<^sub>C) \<in> \<I> \<and>
         lc\<^sub>1\<^sub>C =\<^sub>m\<^sub>d\<^sub>s lc\<^sub>2\<^sub>C \<and> abs.neval lc\<^sub>2\<^sub>A n lc\<^sub>2\<^sub>A' \<and> lc\<^sub>1\<^sub>A' =\<^sub>m\<^sub>d\<^sub>s lc\<^sub>2\<^sub>A'
          \<longrightarrow> (\<exists> lc\<^sub>2\<^sub>C'. lc\<^sub>2\<^sub>C \<leadsto>\<^sub>C lc\<^sub>2\<^sub>C' \<and> lc\<^sub>1\<^sub>C' =\<^sub>m\<^sub>d\<^sub>s lc\<^sub>2\<^sub>C' \<and>
                  (lc\<^sub>2\<^sub>A', lc\<^sub>2\<^sub>C') \<in> \<R> \<and> (lc\<^sub>1\<^sub>C', lc\<^sub>2\<^sub>C') \<in> \<I>))))"

lemma coupling_inv_pres_paper_original:
  "coupling_inv_pres_paper = coupling_inv_pres_original"
  apply(rule ext)+
  apply(rename_tac \<B> \<R> \<I>)
  unfolding coupling_inv_pres_original_def coupling_inv_pres_paper_def abs.lc_same_mds_def conc.lc_same_mds_def
  by clarsimp

(* Definition 4: Closedness of refinements under changes by others *)

thm closed_others_def

definition
  closed_others_paper :: "('Com\<^sub>A, 'Var, 'Val, 'Com\<^sub>C, 'Var) state_relation \<Rightarrow> bool"
where
  "closed_others_paper \<R> \<equiv> (\<forall> tps\<^sub>A tps\<^sub>C mds mem mem'.
     (\<langle> tps\<^sub>A, mds, mem \<rangle>\<^sub>A, \<langle> tps\<^sub>C, mds, mem \<rangle>\<^sub>C) \<in> \<R> \<and>
     (\<forall>x. (mem x \<noteq> mem' x \<or> dma mem x \<noteq> dma mem' x) \<longrightarrow> conc.writable mds x) \<longrightarrow>
     (\<langle> tps\<^sub>A, mds, mem' \<rangle>\<^sub>A, \<langle> tps\<^sub>C, mds, mem' \<rangle>\<^sub>C) \<in> \<R>)"

lemma closed_others_paper_original:
  "closed_others_paper = closed_others"
  unfolding closed_others_def closed_others_paper_def
  by blast

(* Definition 5: Preservation of modes and memory *)

thm preserves_modes_mem_def

definition
  preserves_modes_mem_paper :: "('Com\<^sub>A, 'Var, 'Val, 'Com\<^sub>C, 'Var) state_relation \<Rightarrow> bool"
where
  "preserves_modes_mem_paper \<R> \<equiv> 
  (\<forall> lc\<^sub>A lc\<^sub>C. (lc\<^sub>A, lc\<^sub>C) \<in> \<R> \<longrightarrow> lc\<^sub>A =\<^sub>m\<^sub>d\<^sub>s\<^sup>m\<^sup>e\<^sup>m lc\<^sub>C)"

lemma preserves_modes_mem_paper_original:
  "preserves_modes_mem_paper = preserves_modes_mem"
  apply(rule ext)
  unfolding preserves_modes_mem_paper_def preserves_modes_mem_def lc_same_mds_mem_def
  by force

(* Definition 6: Requirements for secure refinement of the per-thread CVDNI property *)

definition
  secure_refinement_paper :: "('Com\<^sub>A, 'Var, 'Val) LocalConf rel \<Rightarrow> ('Com\<^sub>A, 'Var, 'Val, 'Com\<^sub>C, 'Var) state_relation \<Rightarrow> 
                          ('Com\<^sub>C, 'Var, 'Val) LocalConf rel \<Rightarrow> bool"
where
  "secure_refinement_paper \<B> \<R> \<I> \<equiv>
  preserves_modes_mem \<R> \<and> closed_others_paper \<R> \<and>
  conc.closed_glob_consistent \<I> \<and> sym \<I> \<and> coupling_inv_pres_paper \<B> \<R> \<I>"

(* This one is unidirectional because sym \<I> is not asserted directly by secure_refinement
  but rather by R\<^sub>C_of_strong_low_bisim_mm where it is used. *)
lemma secure_refinement_paper_original:
  "secure_refinement_paper \<B> \<R> \<I> \<Longrightarrow> secure_refinement \<B> \<R> \<I>"
  unfolding secure_refinement_paper_def
  apply(clarsimp simp:coupling_inv_pres_paper_original closed_others_paper_original)
  unfolding coupling_inv_pres_original_def secure_refinement_def
  apply clarsimp
  apply(rename_tac c\<^sub>1\<^sub>A mds mem\<^sub>1 c\<^sub>1\<^sub>C c\<^sub>1\<^sub>C' mds' mem\<^sub>1')
  apply(erule_tac x=c\<^sub>1\<^sub>A in allE)
  apply(erule_tac x=mds in allE)
  apply(erule_tac x=mem\<^sub>1 in allE)
  apply(erule_tac x=c\<^sub>1\<^sub>C in allE)
  apply(erule_tac x=mds in allE)
  apply(erule_tac x=mem\<^sub>1 in allE)
  apply clarsimp
  apply(erule_tac x=c\<^sub>1\<^sub>C' in allE)
  apply(erule_tac x=mds' in allE)
  apply(erule_tac x=mem\<^sub>1' in allE)
  by (metis preserves_modes_memD)
  
(* Theorem 5.1 of Murray_SPR_16 *)

definition
  \<B>\<^sub>C_of_paper :: 
 "((('Com\<^sub>A \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) \<times>
    ('Com\<^sub>A \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) set \<Rightarrow> 
  ('Com\<^sub>A, 'Var, 'Val, 'Com\<^sub>C, 'Var) state_relation \<Rightarrow>
  ((('Com\<^sub>C \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) \<times>
    ('Com\<^sub>C \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) set \<Rightarrow>
  ((('Com\<^sub>C \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) \<times>
    ('Com\<^sub>C \<times> (Mode \<Rightarrow> 'Var set)) \<times> ('Var \<Rightarrow> 'Val)) set"
where
  "\<B>\<^sub>C_of_paper \<B> \<R> \<I> \<equiv> {(x,y). \<exists>x\<^sub>A y\<^sub>A. (x\<^sub>A,x) \<in> \<R> \<and> (y\<^sub>A,y) \<in> \<R> \<and>
     (x\<^sub>A,y\<^sub>A) \<in> \<B> \<and> conc.lc_low_mds_eq x y \<and> (x,y) \<in> \<I>}"

lemma \<B>\<^sub>C_of_paper_R\<^sub>C_of:
  "\<B>\<^sub>C_of_paper = gen_refine.R\<^sub>C_of"
  unfolding gen_refine.R\<^sub>C_of_def \<B>\<^sub>C_of_paper_def conc.lc_low_mds_eq_def
  by fastforce

lemma \<B>\<^sub>C_of_paper_strong_low_bisim_mm:
  "abs.strong_low_bisim_mm \<B> \<and> secure_refinement_paper \<B> \<R> \<I> \<Longrightarrow>
   conc.strong_low_bisim_mm (\<B>\<^sub>C_of_paper \<B> \<R> \<I>)"
  apply clarsimp
  apply(frule secure_refinement_paper_original)
  unfolding secure_refinement_paper_def
  apply(clarsimp simp:\<B>\<^sub>C_of_paper_R\<^sub>C_of)
  using R\<^sub>C_of_strong_low_bisim_mm
  by force

(* Definition 8: Side-conditions for CVDNI-preserving refinement decomposition *)

definition
  decomp_refinement_safe_paper
where
  "decomp_refinement_safe_paper \<B> \<R> \<I> abs_steps \<equiv> \<forall> lc\<^sub>1\<^sub>A lc\<^sub>2\<^sub>A lc\<^sub>1\<^sub>C lc\<^sub>2\<^sub>C. (lc\<^sub>1\<^sub>A, lc\<^sub>2\<^sub>A) \<in> \<B> \<and>
   abs.lc_same_mds lc\<^sub>1\<^sub>A lc\<^sub>2\<^sub>A \<and> (lc\<^sub>1\<^sub>A, lc\<^sub>1\<^sub>C) \<in> \<R> \<and> (lc\<^sub>2\<^sub>A, lc\<^sub>2\<^sub>C) \<in> \<R> \<and> (lc\<^sub>1\<^sub>C, lc\<^sub>2\<^sub>C) \<in> \<I> \<and> lc\<^sub>1\<^sub>C =\<^sub>m\<^sub>d\<^sub>s lc\<^sub>2\<^sub>C \<longrightarrow>
       (gen_refine.stops\<^sub>C lc\<^sub>1\<^sub>C = gen_refine.stops\<^sub>C lc\<^sub>2\<^sub>C) \<and>
       (abs_steps lc\<^sub>1\<^sub>A lc\<^sub>1\<^sub>C = abs_steps lc\<^sub>2\<^sub>A lc\<^sub>2\<^sub>C) \<and>
       (\<forall>lc\<^sub>1\<^sub>C' lc\<^sub>2\<^sub>C'. lc\<^sub>1\<^sub>C \<leadsto>\<^sub>C lc\<^sub>1\<^sub>C' \<and> lc\<^sub>2\<^sub>C \<leadsto>\<^sub>C lc\<^sub>2\<^sub>C' \<longrightarrow> (lc\<^sub>1\<^sub>C', lc\<^sub>2\<^sub>C') \<in> \<I> \<and> lc\<^sub>1\<^sub>C' =\<^sub>m\<^sub>d\<^sub>s lc\<^sub>2\<^sub>C')"

lemma decomp_refinement_safe_paper:
  "decomp_refinement_safe_paper = gen_refine.simpler_refinement_safe"
  apply(rule ext)+
  unfolding decomp_refinement_safe_paper_def gen_refine.simpler_refinement_safe_def abs.lc_same_mds_def conc.lc_same_mds_def
  apply clarsimp
  by blast

(* Definition 7: Decomposed requirements for CVDNI-preserving secure refinement *)

definition
  secure_refinement_decomp_paper
where
  "secure_refinement_decomp_paper \<B> \<R> \<I> abs_steps \<equiv>
  preserves_modes_mem \<R> \<and> closed_others \<R> \<and>
  conc.closed_glob_consistent \<I> \<and> sym \<I> \<and>
  decomp_refinement_safe_paper \<B> \<R> \<I> abs_steps \<and> (\<forall> lc\<^sub>A lc\<^sub>C. (lc\<^sub>A, lc\<^sub>C) \<in> \<R> \<longrightarrow>
    (\<forall> lc\<^sub>C'. lc\<^sub>C \<leadsto>\<^sub>C lc\<^sub>C' \<longrightarrow> (\<exists> lc\<^sub>A'. abs.neval lc\<^sub>A (abs_steps lc\<^sub>A lc\<^sub>C) lc\<^sub>A' \<and> (lc\<^sub>A', lc\<^sub>C') \<in> \<R>)))"

lemma secure_refinement_decomp_paper_simpler:
  "secure_refinement_decomp_paper \<B> \<R> \<I> abs_steps \<Longrightarrow> gen_refine.secure_refinement_simpler \<B> \<R> \<I> abs_steps"
  unfolding secure_refinement_decomp_paper_def
  apply(clarsimp simp:decomp_refinement_safe_paper)
  unfolding gen_refine.secure_refinement_simpler_def
  by force

(* Theorem 9: Soundness of secure-refinement-decomp *)

lemma secure_refinement_decomp_paper_sound:
  "secure_refinement_decomp_paper \<B> \<R> \<I> abs_steps \<Longrightarrow> secure_refinement \<B> \<R> \<I>"
  using secure_refinement_decomp_paper_simpler gen_refine.secure_refinement_simpler
  by fastforce

end

end
