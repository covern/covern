(*
Authors: Robert Sison
Based on the Dependent_SIFUM_Type_Systems AFP entry, whose authors are:
 Toby Murray, Robert Sison, Edward Pierzchalski, Christine Rizkallah
in turn based on the SIFUM_Type_Systems AFP entry, whose authors are:
 Sylvia Grewe, Heiko Mantel, Daniel Schoepe
*)

section {* Type System for Ensuring Locally Sound Use of Modes with Locking Primitives *}

theory LocallySoundWhileLockUse
imports Main "../old-rg-sec/Security" "../while-lang/WhileLockLanguage"
begin

subsection {* Typing Rules *}

locale sifum_modes = 
  sifum_lang_no_dma ev\<^sub>A ev\<^sub>B ev\<^sub>L aexp_vars bexp_vars lock_interp lock_val_True lock_val_False +
  sifum_security_init dma \<C>_vars \<C> eval\<^sub>w undefined no_locks_acquired "\<lambda>x. True"
  for ev\<^sub>A :: "('Var, 'Val) Mem \<Rightarrow> 'AExp \<Rightarrow> 'Val"
  and ev\<^sub>B :: "('Var, 'Val) Mem \<Rightarrow> 'BExp \<Rightarrow> bool"
  and ev\<^sub>L :: "'Val \<Rightarrow> bool"
  and aexp_vars :: "'AExp \<Rightarrow> 'Var set"
  and bexp_vars :: "'BExp \<Rightarrow> 'Var set"
  and lock_interp :: "'Lock \<Rightarrow> ('Var set \<times> 'Var set)"
  and dma :: "(('Lock, 'Var) Var,'Val) Mem \<Rightarrow> ('Lock, 'Var) Var \<Rightarrow> Sec"
  and \<C>_vars :: "('Lock, 'Var) Var \<Rightarrow> ('Lock, 'Var) Var set"
  and \<C> :: "('Lock, 'Var) Var set"
  and lock_val_True :: "'Val"
  and lock_val_False :: "'Val" +
  assumes no_locks_in_\<C>_vars : "\<forall> v l. (Lock l) \<notin> \<C>_vars v"
  assumes no_locks_in_\<C> : "\<forall> l. (Lock l) \<notin> \<C>"

context sifum_modes
begin

abbreviation eval_abv_modes :: "(_, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
  (infixl "\<leadsto>" 70)
  where
  "x \<leadsto> y \<equiv> (x, y) \<in> eval\<^sub>w"

inductive mode_type :: "('Lock, 'Var) Var Mds \<Rightarrow>
  ('Var, 'Lock, 'AExp, 'BExp) Stmt \<Rightarrow>
  ('Lock, 'Var) Var Mds \<Rightarrow> bool" ("\<turnstile> _ { _ } _")
  where
  skip: "no_lock_mds mds \<Longrightarrow> \<turnstile> mds { Skip } mds" |
  assign: "\<lbrakk> Var x \<notin> mds GuarNoWrite \<union> mds GuarNoReadOrWrite ;
             aexp_vars e \<inter> to_prog_var_set (mds GuarNoReadOrWrite) = {};
             \<forall>v. (Var x \<in> \<C>_vars v \<longrightarrow> v \<notin> mds GuarNoWrite \<union> mds GuarNoReadOrWrite) \<and>
                 (to_prog_var_set (\<C>_vars v) \<inter> aexp_vars e \<noteq> {} \<longrightarrow> v \<notin> mds GuarNoReadOrWrite);
             no_lock_mds mds\<rbrakk> \<Longrightarrow>
  \<turnstile> mds { (x \<leftarrow> e) } mds" |
  if_: "\<lbrakk> bexp_vars e \<inter> to_prog_var_set (mds GuarNoReadOrWrite) = {};
          \<forall> v. to_prog_var_set (\<C>_vars v) \<inter> bexp_vars e \<noteq> {} \<longrightarrow> v \<notin> mds GuarNoReadOrWrite;
          no_lock_mds mds;
          \<turnstile> mds { c\<^sub>1 } mds'' ;
          \<turnstile> mds { c\<^sub>2 } mds''\<rbrakk> \<Longrightarrow>
        \<turnstile> mds { If e c\<^sub>1 c\<^sub>2 } mds''" |
  while: "\<lbrakk> mds' = mds ; bexp_vars e \<inter> to_prog_var_set (mds' GuarNoReadOrWrite) = {};
            \<forall> v. to_prog_var_set (\<C>_vars v) \<inter> bexp_vars e \<noteq> {} \<longrightarrow> v \<notin> mds' GuarNoReadOrWrite;
            no_lock_mds mds;
            \<turnstile> mds' { c } mds' \<rbrakk> \<Longrightarrow>
  \<turnstile> mds { While e c } mds'" |
  seq: "\<lbrakk> no_lock_mds mds; \<turnstile> mds { c\<^sub>1 } mds' ; \<turnstile> mds' { c\<^sub>2 } mds'' \<rbrakk> \<Longrightarrow>
  \<turnstile> mds { c\<^sub>1 ;; c\<^sub>2 } mds''" |
  lock_acq: "\<lbrakk> lock_interp l = (NoW_vars, NoRW_vars);
               mds' = lock_acq_upd lock_interp l mds;
               no_lock_mds mds \<rbrakk> \<Longrightarrow>
  \<turnstile> mds { LockAcq l } mds'" |
  lock_rel: "\<lbrakk> lock_held_mds_correct lock_interp mds l; lock_interp l = (NoW_vars, NoRW_vars);
               mds' = lock_rel_upd lock_interp l mds;
               no_lock_mds mds \<rbrakk> \<Longrightarrow>
  \<turnstile> mds { LockRel l } mds'" |
  sub: "\<lbrakk> mds\<^sub>1 \<le> mds\<^sub>2 ; mds\<^sub>2' \<le> mds\<^sub>1' ; \<forall>l m. Lock l \<notin> mds\<^sub>1 m ; \<turnstile> mds\<^sub>2 { c } mds\<^sub>2' \<rbrakk> \<Longrightarrow>
  \<turnstile> mds\<^sub>1 { c } mds\<^sub>1'"

subsection {* Soundness of the Type System *}

(* The following part contains some lemmas about evaluation of
   commands annotated using \<otimes> and characterisations of loc_reach for
   commands. *)

lemma stop_loc_reach:
  "\<lbrakk> \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>Stop, mds, mem\<rangle> \<rbrakk> \<Longrightarrow>
  c' = Stop \<and> mds' = mds"
  apply (induct rule: loc_reach.induct)
  by (auto simp: stop_no_eval)

lemma stop_doesnt_access:
  "doesnt_modify Stop x \<and> doesnt_read_or_modify Stop x"
  unfolding doesnt_modify_def and doesnt_read_or_modify_def doesnt_read_or_modify_vars_def
  using stop_no_eval
  by auto

lemma skip_eval_step:
  "\<langle>Skip, mds, mem\<rangle> \<leadsto> \<langle>Stop, mds, mem\<rangle>"
  by (metis eval\<^sub>w.simple eval\<^sub>w_simple.skip)

lemma skip_eval_elim:
  "\<lbrakk> \<langle>Skip, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle> \<rbrakk> \<Longrightarrow> c' = Stop \<and> mds' = mds \<and> mem' = mem"
  apply (rule ccontr)
  apply (insert skip_eval_step deterministic)
  apply clarify
  by auto

lemma skip_doesnt_read:
  "doesnt_read_or_modify Skip x"
  apply (auto simp: doesnt_read_or_modify_def doesnt_read_or_modify_vars_def)
  by (metis skip_dest skip_eval_step)+

lemma skip_doesnt_write:
  "doesnt_modify Skip x"
  by (auto simp: doesnt_modify_def)

lemma skip_loc_reach:
  "\<lbrakk> \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>Skip, mds, mem\<rangle> \<rbrakk> \<Longrightarrow>
  (c' = Stop \<and> mds' = mds) \<or> (c' = Skip \<and> mds' = mds)"
  apply (induct rule: loc_reach.induct)
    apply (metis fst_conv snd_conv)
   apply (metis skip_eval_elim stop_no_eval)
  by metis

lemma skip_doesnt_access:
  "\<lbrakk> lc \<in> loc_reach \<langle>Skip, mds, mem\<rangle> ; lc = \<langle>c', mds', mem'\<rangle> \<rbrakk> \<Longrightarrow> doesnt_read_or_modify c' x \<and> doesnt_modify c' x"
  apply (subgoal_tac "(c' = Stop \<and> mds' = mds) \<or> (c' = Skip \<and> mds' = mds)")
   apply (rule conjI, erule disjE)
     apply (simp add: doesnt_read_or_modify_def doesnt_read_or_modify_vars_def stop_no_eval)
    apply (metis (lifting) skip_doesnt_read)
   apply (erule disjE)
    apply (simp add: doesnt_modify_def stop_no_eval)
   apply (metis (lifting) skip_doesnt_write)
  by (metis skip_loc_reach)

lemma assign_doesnt_modify:
  "\<lbrakk> Var x \<noteq> v; (Var x) \<notin> \<C>_vars v \<rbrakk> \<Longrightarrow> doesnt_modify (x \<leftarrow> e) v"
  apply (simp add: doesnt_modify_def)
  apply clarsimp
  apply(drule assign_dest)
  apply clarsimp
  by (metis fun_upd_apply dma_\<C>_vars)
  
lemma assign_eval:
  "\<langle>x \<leftarrow> e, mds, mem\<rangle> \<leadsto> \<langle>Stop, mds, mem (Var x := ev\<^sub>A (to_prog_mem mem) e)\<rangle>"
  apply (rule eval\<^sub>w.simple)
     apply (rule eval\<^sub>w_simple.assign)
    apply simp
   apply (metis prog_mem_assign_helper)
  unfolding to_lock_mem_def by simp

lemma assign_eval_dest:
  "\<lbrakk> \<langle>x \<leftarrow> e, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle> \<rbrakk> \<Longrightarrow>
  c' = Stop \<and> mds' = mds"
  apply (rule ccontr)
  apply (insert deterministic assign_eval)
  by auto

lemma assign_doesnt_read:
  "\<lbrakk> v \<noteq> Var x; \<forall> y. v = Var y \<longrightarrow> y \<notin> aexp_vars e; (Var x) \<notin> \<C>_vars v; to_prog_var_set (\<C>_vars v) \<inter> aexp_vars e = {} \<rbrakk> \<Longrightarrow> doesnt_read_or_modify (x \<leftarrow> e) v"
proof -
  assume  "v \<noteq> Var x"
          "\<forall> y. v = Var y \<longrightarrow> y \<notin> aexp_vars e"
          "(Var x) \<notin> \<C>_vars v"
          "to_prog_var_set (\<C>_vars v) \<inter> aexp_vars e = {}"
  thus "doesnt_read_or_modify (x \<leftarrow> e) v"
    unfolding doesnt_read_or_modify_def doesnt_read_or_modify_vars_def
    apply -
    apply (rule allI)+
    apply (rename_tac mds mem c' mds' mem')
    apply (rule impI)
    apply (subgoal_tac "c' = Stop \<and> mds' = mds \<and> mem' = mem (Var x := ev\<^sub>A (to_prog_mem mem) e)")
     apply simp
     apply clarify
     apply (rule conjI)
      apply clarify
      apply (rename_tac va)
      apply (subgoal_tac "mem (Var x := ev\<^sub>A (to_prog_mem mem) e, v := va) = mem (v := va, Var x := ev\<^sub>A (to_prog_mem mem) e)")
       apply simp
       apply (rule assign_eval\<^sub>w)
        apply simp
       apply(case_tac v)
        apply (auto simp: to_prog_mem_def)[1]
       apply(simp add:to_prog_mem_def)
       using eval_vars_det\<^sub>A apply auto[1]
      apply(metis fun_upd_twist)
     apply clarify
     apply (rename_tac y val)
     apply (subgoal_tac "mem ((Var x) := ev\<^sub>A (to_prog_mem mem) e, y := val) = mem (y := val, Var x := ev\<^sub>A (to_prog_mem mem) e)")
      apply simp
      apply (rule_tac assign_eval\<^sub>w')
       apply simp
      apply(case_tac y)
       apply clarsimp
       apply(simp add:to_prog_mem_def)
       using eval_vars_det\<^sub>A apply auto[1]
      apply (rename_tac x2)
      apply (subgoal_tac "x2 \<notin> aexp_vars e")
       apply(simp add:to_prog_mem_def)
       using eval_vars_det\<^sub>A apply auto[1]
      unfolding to_prog_var_set_def apply blast
      apply(simp add:to_lock_mem_def)
     apply (metis fun_upd_twist)
    apply (metis assign_dest)
    done
qed

lemma assign_loc_reach:
  "\<lbrakk> \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>x \<leftarrow> e, mds, mem\<rangle> \<rbrakk> \<Longrightarrow>
  (c' = Stop \<and> mds' = mds \<or> (c' = x \<leftarrow> e \<and> mds' = mds))"
  apply (induct rule: loc_reach.induct)
    apply simp_all
  by (metis assign_eval_dest stop_no_eval)

lemma lock_acq_loc_reach:
  "\<lbrakk> \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>LockAcq l, mds, mem\<rangle> \<rbrakk> \<Longrightarrow>
  (c' = Stop \<and> mds' = lock_acq_upd lock_interp l mds) \<or> (c' = LockAcq l \<and> mds = mds')"
  apply (induct rule: loc_reach.induct)
    apply simp_all
  using stop_no_eval lock_acq\<^sub>w_elim by blast 

lemma lock_acq_doesnt_modify_progvars:
  "doesnt_modify (LockAcq l) (Var x)"
  unfolding doesnt_modify_def
  apply clarsimp
  apply(erule lock_acq\<^sub>w_elim)
   apply force
  using no_locks_in_\<C>_vars dma_\<C>_vars fun_upd_other by auto

lemma lock_acq_doesnt_read_progvars:
  "doesnt_read_or_modify (LockAcq l) (Var x)"
  unfolding doesnt_read_or_modify_def doesnt_read_or_modify_vars_def
  apply clarsimp
  apply(erule lock_acq\<^sub>w_elim)
   apply clarsimp
   apply(metis (mono_tags, hide_lams) Var.distinct(1) eval\<^sub>w.lock_spin fun_upd_apply no_locks_in_\<C>_vars)
  apply clarsimp
  apply(rule conjI)
   apply clarsimp
   apply (simp add: eval\<^sub>w.lock_acq fun_upd_twist)
  by (metis (mono_tags) eval\<^sub>w.lock_acq fun_upd_apply fun_upd_twist no_locks_in_\<C>_vars)

lemma lock_rel_loc_reach:
  "\<lbrakk> \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>LockRel l, mds, mem\<rangle> \<rbrakk> \<Longrightarrow>
  (c' = Stop \<and> lock_held_mds_correct lock_interp mds l \<and> mds' = lock_rel_upd lock_interp l mds)
  \<or> (c' = LockRel l \<and> mds = mds')"
  apply (induct rule: loc_reach.induct)
    apply simp_all
  by (metis (no_types, lifting) lock_rel\<^sub>w_elim stop_no_eval)

lemma lock_rel_doesnt_modify_progvars:
  "doesnt_modify (LockRel l) (Var x)"
  unfolding doesnt_modify_def
  apply clarsimp
  apply(erule lock_rel\<^sub>w_elim)
   apply(metis Var.distinct(1) dma_\<C> fun_upd_apply no_locks_in_\<C>)
  by clarsimp

lemma lock_rel_doesnt_read_progvars:
  "doesnt_read_or_modify (LockRel l) (Var x)"
  unfolding doesnt_read_or_modify_def doesnt_read_or_modify_vars_def
  apply clarsimp
  apply(erule lock_rel\<^sub>w_elim)
   apply clarsimp
   apply(rule conjI)
    apply clarsimp
    apply (force simp add: eval\<^sub>w.lock_rel fun_upd_twist)
   apply clarsimp
   apply(metis (mono_tags, hide_lams) eval\<^sub>w.lock_rel fun_upd_twist no_locks_in_\<C>_vars)
  by (simp add: eval\<^sub>w.lock_invalid)

lemma if_doesnt_modify:
  "doesnt_modify (If e c\<^sub>1 c\<^sub>2) x"
  by (auto simp: doesnt_modify_def)

lemma vars_eval\<^sub>B:
  "x \<notin> bexp_vars e \<Longrightarrow> ev\<^sub>B (to_prog_mem mem) e = ev\<^sub>B (to_prog_mem (mem(Var x := v))) e"
  by (simp add: eval_vars_det\<^sub>B to_prog_mem_def)
  

lemma if_doesnt_read:
  "\<forall>v. x = Var v \<longrightarrow> v \<notin> bexp_vars e \<Longrightarrow> to_prog_var_set (\<C>_vars x) \<inter> bexp_vars e = {} \<Longrightarrow> doesnt_read_or_modify (If e c\<^sub>1 c\<^sub>2) x"
  apply (auto simp: doesnt_read_or_modify_def doesnt_read_or_modify_vars_def)
   apply (rename_tac mds mem c' mds' mem' v)
   apply (case_tac "ev\<^sub>B (to_prog_mem mem) e")
    apply (subgoal_tac "c' = c\<^sub>1 \<and> mds' = mds \<and> mem' = mem")
     apply auto (* What is with these "auto" proofs? FIXME *)
    apply (rule_tac pmem="to_prog_mem (mem(x := v))" in eval\<^sub>w.simple)
       apply (rule eval\<^sub>w_simple.if_true)
       apply(case_tac x)
        apply(simp add:to_prog_mem_def)
       apply (metis (lifting) vars_eval\<^sub>B)
      apply simp+
   apply (subgoal_tac "c' = c\<^sub>2 \<and> mds' = mds \<and> mem' = mem")
    apply auto
   apply (rule_tac pmem="to_prog_mem (mem(x := v))" in eval\<^sub>w.simple)
      apply (rule eval\<^sub>w_simple.if_false)
      apply(case_tac x)
       apply(simp add:to_prog_mem_def)
      apply (metis (lifting) vars_eval\<^sub>B)
     apply simp+
  apply (rename_tac mds mem c' mds' mem' va v)
  apply (case_tac "ev\<^sub>B (to_prog_mem mem) e")
   apply (subgoal_tac "c' = c\<^sub>1 \<and> mds' = mds \<and> mem' = mem")
    apply auto
   apply (rule_tac pmem="to_prog_mem (mem(va := v))" in eval\<^sub>w.simple)
      apply (rule eval\<^sub>w_simple.if_true)
      apply(rename_tac va v)
      apply(case_tac va)
       apply clarsimp
       apply (auto simp:to_prog_mem_def eval_vars_det\<^sub>A)[1]
      apply clarsimp
      apply(rename_tac x2)
      apply (subgoal_tac "x2 \<notin> bexp_vars e")
       apply (metis (lifting) vars_eval\<^sub>B)
      unfolding to_prog_var_set_def apply blast
     apply clarsimp+
  apply (subgoal_tac "c' = c\<^sub>2 \<and> mds' = mds \<and> mem' = mem")
   apply auto
  apply (rule_tac pmem="to_prog_mem (mem(va := v))" in eval\<^sub>w.simple)
     apply (rule eval\<^sub>w_simple.if_false)
     apply(rename_tac va v)
     apply(case_tac va)
      apply clarsimp
      apply (auto simp:to_prog_mem_def eval_vars_det\<^sub>A)[1]
     apply clarsimp
     apply(rename_tac x2)
     apply (subgoal_tac "x2 \<notin> bexp_vars e")
      apply (metis (lifting) vars_eval\<^sub>B)
     unfolding to_prog_var_set_def apply blast
    apply clarsimp+
  done
  
lemma if_eval_true:
  "\<lbrakk> ev\<^sub>B (to_prog_mem mem) e \<rbrakk> \<Longrightarrow>
  \<langle>If e c\<^sub>1 c\<^sub>2, mds, mem\<rangle> \<leadsto> \<langle>c\<^sub>1, mds, mem\<rangle>"
  by (metis eval\<^sub>w.simple eval\<^sub>w_simple.if_true)

lemma if_eval_false:
  "\<lbrakk> \<not> ev\<^sub>B (to_prog_mem mem) e \<rbrakk> \<Longrightarrow>
  \<langle>If e c\<^sub>1 c\<^sub>2, mds, mem\<rangle> \<leadsto> \<langle>c\<^sub>2, mds, mem\<rangle>"
  by (metis eval\<^sub>w.simple eval\<^sub>w_simple.if_false)

lemma if_eval_elim:
  "\<lbrakk> \<langle>If e c\<^sub>1 c\<^sub>2, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle> \<rbrakk> \<Longrightarrow>
  ((c' = c\<^sub>1 \<and> ev\<^sub>B (to_prog_mem mem) e) \<or> (c' = c\<^sub>2 \<and> \<not> ev\<^sub>B (to_prog_mem mem) e)) \<and> mds' = mds \<and> mem' = mem"
  apply (rule ccontr)
  apply (cases "ev\<^sub>B (to_prog_mem mem) e")
   apply (insert if_eval_true deterministic)
   apply blast
  using if_eval_false deterministic
by blast

lemma if_eval_elim':
  "\<lbrakk> \<langle>If e c\<^sub>1 c\<^sub>2, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle> \<rbrakk> \<Longrightarrow>
  ((c' = c\<^sub>1 \<and> ev\<^sub>B (to_prog_mem mem) e) \<or> (c' = c\<^sub>2 \<and> \<not> ev\<^sub>B (to_prog_mem mem) e)) \<and> mds' = mds \<and> mem' = mem"
  using if_eval_elim
  by auto

lemma loc_reach_refl':
  "\<langle>c, mds, mem\<rangle> \<in> loc_reach \<langle>c, mds, mem\<rangle>"
  apply (subgoal_tac "\<exists> lc. lc \<in> loc_reach lc \<and> lc = \<langle>c, mds, mem\<rangle>")
   apply blast
  by (metis loc_reach.refl fst_conv snd_conv)

lemma if_loc_reach:
  "\<lbrakk> \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>If e c\<^sub>1 c\<^sub>2, mds, mem\<rangle> \<rbrakk> \<Longrightarrow>
  (c' = If e c\<^sub>1 c\<^sub>2 \<and> mds' = mds) \<or>
  (\<exists> mem''. \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, mds, mem''\<rangle>) \<or>
  (\<exists> mem''. \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, mds, mem''\<rangle>)"
  apply (induct rule: loc_reach.induct)
    apply (metis fst_conv snd_conv)
   apply (erule disjE)
    apply (erule conjE)
    apply simp
    apply (drule if_eval_elim)
    apply (erule conjE)+
    apply (erule disjE)
     apply (erule conjE)
     apply simp
     apply (metis loc_reach_refl')
    apply (metis loc_reach_refl')
   apply (metis loc_reach.step)
  by (metis loc_reach.mem_diff)

lemma if_loc_reach':
  "\<lbrakk> \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>If e c\<^sub>1 c\<^sub>2, mds, mem\<rangle> \<rbrakk> \<Longrightarrow>
  (c' = If e c\<^sub>1 c\<^sub>2 \<and> mds' = mds) \<or>
  (\<exists> mem''. \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, mds, mem''\<rangle>) \<or>
  (\<exists> mem''. \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, mds, mem''\<rangle>)"
  using if_loc_reach
  by simp

lemma seq_loc_reach:
  "\<lbrakk> \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1 ;; c\<^sub>2, mds, mem\<rangle> \<rbrakk> \<Longrightarrow>
  (\<exists> c''. c' = c'' ;; c\<^sub>2 \<and> \<langle>c'', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, mds, mem\<rangle>) \<or>
  (\<exists> c'' mds'' mem''. \<langle>Stop, mds'', mem''\<rangle> \<in> loc_reach \<langle>c\<^sub>1, mds, mem\<rangle> \<and> 
                      \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, mds'', mem''\<rangle>)"
  apply (induct rule: loc_reach.induct)
    apply simp
    apply (metis  loc_reach_refl')
   apply simp
   apply (metis (no_types) loc_reach.step loc_reach_refl' seq_dest)
  by (metis (lifting) loc_reach.mem_diff)

lemma seq_doesnt_read:
  "\<lbrakk> doesnt_read_or_modify c x \<rbrakk> \<Longrightarrow> doesnt_read_or_modify (c ;; c') x"
  apply (clarsimp simp: doesnt_read_or_modify_def doesnt_read_or_modify_vars_def)
  by (metis (no_types, lifting) eval\<^sub>w.seq1 eval\<^sub>w.seq2 seq_dest)
  
lemma seq_doesnt_modify:
  "\<lbrakk> doesnt_modify c x \<rbrakk> \<Longrightarrow> doesnt_modify (c ;; c') x"
  by (auto simp: doesnt_modify_def)
  
inductive_cases seq_stop_elim': "\<langle>Stop ;; c, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle>"

lemma seq_split:
  "\<lbrakk> \<langle>Stop, mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1 ;; c\<^sub>2, mds, mem\<rangle> \<rbrakk> \<Longrightarrow>
  \<exists> mds'' mem''. \<langle>Stop, mds'', mem''\<rangle> \<in> loc_reach \<langle>c\<^sub>1, mds, mem\<rangle> \<and>
                 \<langle>Stop, mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, mds'', mem''\<rangle>"
  apply (drule seq_loc_reach)
  using Stmt.simps
  by fast

lemma while_eval:
  "\<langle>While e c, mds, mem\<rangle> \<leadsto> \<langle>(If e (c ;; While e c) Stop), mds, mem\<rangle>"
  apply (rule_tac pmem="to_prog_mem mem" and pmem'="to_prog_mem mem" in eval\<^sub>w.simple)
     apply (metis (lifting) eval\<^sub>w_simple.while)
    apply simp+
  done

lemma while_doesnt_read:
  "\<lbrakk> x \<notin> bexp_vars e \<rbrakk> \<Longrightarrow> doesnt_read_or_modify (While e c) (Var x)"
  unfolding doesnt_read_or_modify_def doesnt_read_or_modify_vars_def
  using while_eval while_dest
  by metis

lemma while_doesnt_modify:
  "doesnt_modify (While e c) x"
  unfolding doesnt_modify_def
  using while_dest
  by metis

(* "invariant" that lock mds are untouched *)
lemma loc_reach_lock_mds_untouched:
  "\<lbrakk>\<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c, mds, mem\<rangle>\<rbrakk> \<Longrightarrow> \<forall>l m. Lock l \<in> mds m \<longleftrightarrow> Lock l \<in> mds' m"
  by (induct rule:loc_reach.induct, (simp add: eval_lock_mds_untouched)+)


theorem mode_type_sound:
  assumes typeable: "\<turnstile> mds\<^sub>1 { c } mds\<^sub>1'"
  assumes mode_le: "mds\<^sub>2 \<le> mds\<^sub>1"
  shows "\<forall> mem. (\<langle>Stop, mds\<^sub>2', mem'\<rangle> \<in> loc_reach \<langle>c, mds\<^sub>2, mem\<rangle> \<longrightarrow> mds\<^sub>2' \<le> mds\<^sub>1') \<and> 
                locally_sound_mode_use \<langle>c, mds\<^sub>2, mem\<rangle>"
  using typeable mode_le
proof (induct arbitrary: mds\<^sub>2 mds\<^sub>2' mem' rule: mode_type.induct)
  case (skip mds)
  thus ?case
    apply auto
     apply (metis (lifting) skip_loc_reach)
    apply (simp add: locally_sound_mode_use_def)
    by (metis skip_doesnt_access)
next
  case (assign x mds e)
  hence "\<forall> mem. locally_sound_mode_use \<langle>x \<leftarrow> e, mds\<^sub>2, mem\<rangle>"
    unfolding locally_sound_mode_use_def
  proof (clarify)
    fix mem c' mds' mem' v
    assume asm: "\<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>x \<leftarrow> e, mds\<^sub>2, mem\<rangle>"
    hence "c' = x \<leftarrow> e \<and> mds' = mds\<^sub>2 \<or> c' = Stop \<and> mds' = mds\<^sub>2"
      using assign_loc_reach by blast
    thus "(v \<in> mds' GuarNoReadOrWrite \<longrightarrow> doesnt_read_or_modify c' v) \<and>
          (v \<in> mds' GuarNoWrite \<longrightarrow> doesnt_modify c' v)"
    proof
      assume "c' = x \<leftarrow> e \<and> mds' = mds\<^sub>2"
      thus ?thesis
      proof (safe)
        assume nin: "v \<in> mds\<^sub>2 GuarNoReadOrWrite"
        hence nin: "v \<in> mds GuarNoReadOrWrite"
          using assign.prems unfolding le_fun_def by blast
        hence "\<forall> y. v = Var y \<longrightarrow> y \<notin> aexp_vars e"
          using assign.hyps(2) assign.prems unfolding to_prog_var_set_def
          by blast
        moreover from nin assign.hyps(3)
        have "to_prog_var_set (\<C>_vars v) \<inter> aexp_vars e = {}"
          by (meson contra_subsetD)
        moreover from nin assign.hyps have "(Var x) \<notin> \<C>_vars v \<and> Var x \<noteq> v"
          by blast
        ultimately show "doesnt_read_or_modify (x \<leftarrow> e) v"
          using assign_doesnt_read
          by fastforce
      next
        assume "v \<in> mds\<^sub>2 GuarNoWrite"
        hence nin: "v \<in> mds GuarNoWrite"
          using assign.prems unfolding le_fun_def by blast
        hence "Var x \<noteq> v \<and> Var x \<notin> \<C>_vars v"
          using assign by blast
        with assign_doesnt_modify show "doesnt_modify (x \<leftarrow> e) v"
          by blast
      qed
    next
      assume "c' = Stop \<and> mds' = mds\<^sub>2"
      with stop_doesnt_access show ?thesis by blast
    qed
  qed
  thus ?case
    apply auto
    by (metis assign.prems assign_loc_reach)
next
  case (if_ e mds c\<^sub>1 mds'' c\<^sub>2)
    let ?c = "(If e c\<^sub>1 c\<^sub>2)"
  from if_ have modes_le': "mds\<^sub>2 \<le> mds"
    by simp
  from if_ show ?case
    apply (simp add: locally_sound_mode_use_def)
    apply clarify
    apply (rule conjI)
     apply clarify
     prefer 2
     apply clarify
  proof -
    fix mem
    assume "\<langle>Stop, mds\<^sub>2', mem'\<rangle> \<in> loc_reach \<langle>If e c\<^sub>1 c\<^sub>2, mds\<^sub>2, mem\<rangle>"
    with modes_le' and if_ show "mds\<^sub>2' \<le> mds''"
    using if_eval_false if_eval_true if_loc_reach stop_no_eval
      by (metis (no_types))
  next
    fix mem c' mds' mem' x
    assume "\<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>If e c\<^sub>1 c\<^sub>2, mds\<^sub>2, mem\<rangle>"
    hence "(c' = If e c\<^sub>1 c\<^sub>2 \<and> mds' = mds\<^sub>2) \<or>
           (\<exists> mem''. \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, mds\<^sub>2, mem''\<rangle>) \<or>
           (\<exists> mem''. \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, mds\<^sub>2, mem''\<rangle>)"
      using if_loc_reach by blast
    thus "(x \<in> mds' GuarNoReadOrWrite \<longrightarrow> doesnt_read_or_modify c' x) \<and>
          (x \<in> mds' GuarNoWrite \<longrightarrow> doesnt_modify c' x)"
    proof
      assume "c' = If e c\<^sub>1 c\<^sub>2 \<and> mds' = mds\<^sub>2"
      thus ?thesis
      proof (safe)
        assume "x \<in> mds\<^sub>2 GuarNoReadOrWrite"
        hence nin: "x \<in> mds GuarNoReadOrWrite"
          using if_ unfolding le_fun_def by auto
        with `bexp_vars e \<inter> to_prog_var_set (mds GuarNoReadOrWrite) = {}`
        have "\<forall>v. x = Var v \<longrightarrow> v \<notin> bexp_vars e"
          using to_prog_var_set_def by fastforce
        moreover from if_(2) nin have "to_prog_var_set (\<C>_vars x) \<inter> bexp_vars e = {}"
          by blast
        ultimately show "doesnt_read_or_modify (If e c\<^sub>1 c\<^sub>2) x"
          using if_doesnt_read by blast
      next
        assume "x \<in> mds\<^sub>2 GuarNoWrite"
        thus "doesnt_modify (If e c\<^sub>1 c\<^sub>2) x"
          using if_doesnt_modify by blast
      qed
    next
      assume "(\<exists>mem''. \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, mds\<^sub>2, mem''\<rangle>) \<or>
              (\<exists>mem''. \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, mds\<^sub>2, mem''\<rangle>)"
      with if_ show ?thesis
        by (metis locally_sound_mode_use_def modes_le')
    qed
  qed
next
  case (while mdsa mds e c)
  hence "mds\<^sub>2 \<le> mds"
    by simp
  have while_loc_reach: "\<And> c' mds' mem' mem.
  \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>While e c, mds\<^sub>2, mem\<rangle> \<Longrightarrow>
  c' = While e c \<and> mds' = mds\<^sub>2 \<or>
  c' = While e c \<and> mds' \<le> mdsa \<or>
  c' = Stmt.If e (c ;; While e c) Stop \<and> mds' \<le> mdsa \<or>
  c' = Stop \<and> mds' \<le> mdsa \<or>
  (\<exists>c'' mem'' mds\<^sub>3.
      c' = c'' ;; While e c \<and>
      mds\<^sub>3 \<le> mdsa \<and> \<langle>c'', mds', mem'\<rangle> \<in> loc_reach \<langle>c, mds\<^sub>3, mem''\<rangle>)"
  proof -
    fix mem c' mds' mem'
    assume "\<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>While e c, mds\<^sub>2, mem\<rangle>"
    thus "?thesis c' mds' mem' mem"
      apply (induct rule: loc_reach.induct)
        apply simp_all
       apply (erule disjE)
        apply simp
        apply (metis `mds\<^sub>2 \<le> mds` while.hyps(1) while_dest)
       apply (erule disjE)
        apply (metis while_dest)
       apply (erule disjE)
        apply simp
        apply (metis if_eval_elim' loc_reach_refl')
       apply (erule disjE)
        apply (metis stop_no_eval)
       apply (erule exE)
       apply (rename_tac c' mds' mem' c'' mds'' mem'' c''a)
       apply (case_tac "c''a = Stop")
        apply force
       apply(metis (no_types, lifting) loc_reach.step seq_dest while.hyps(6))
      by (metis (full_types) loc_reach.mem_diff)
  qed
  from while show ?case
  proof (safe)
    fix mem
    assume "\<langle>Stop, mds\<^sub>2', mem'\<rangle> \<in> loc_reach \<langle>While e c, mds\<^sub>2, mem\<rangle>"
    thus "mds\<^sub>2' \<le> mds"
      by (metis Stmt.distinct while.hyps(1) while.prems while_loc_reach)
  next
    fix mem
    from while have a: "bexp_vars e \<inter> to_prog_var_set (mds\<^sub>2 GuarNoReadOrWrite) = {}"
    proof -
      have f1: "bexp_vars e \<inter> to_prog_var_set (mds GuarNoReadOrWrite) = {}"
        using while.hyps by presburger
      have "mds\<^sub>2 GuarNoReadOrWrite \<subseteq> mds GuarNoReadOrWrite"
        by (metis (no_types) inf_fun_def le_iff_inf while.prems)
      thus ?thesis
        using f1 by (simp add: disjoint_iff_not_equal subsetD to_prog_var_set_def)
    qed
    from while have b: "\<forall>v. to_prog_var_set (\<C>_vars v) \<inter> bexp_vars e \<noteq> {} \<longrightarrow> v \<notin> mds\<^sub>2 GuarNoReadOrWrite"
      by (meson `mds\<^sub>2 \<le> mds` le_fun_def subsetCE)
    show "locally_sound_mode_use \<langle>While e c, mds\<^sub>2, mem\<rangle>"
      unfolding locally_sound_mode_use_def
      apply (rule allI)+
      apply (rule impI)
    proof -
      fix c' mds' mem'
      define lc where "lc \<equiv> \<langle>While e c, mds\<^sub>2, mem\<rangle>"
      assume "\<langle>c', mds', mem'\<rangle> \<in> loc_reach lc"
      thus "\<forall> x. (x \<in> mds' GuarNoReadOrWrite \<longrightarrow> doesnt_read_or_modify c' x) \<and>
                 (x \<in> mds' GuarNoWrite \<longrightarrow> doesnt_modify c' x)"
        apply (simp add: lc_def)
        apply (drule while_loc_reach)
      proof (auto del: conjI)
        fix x
        show "(x \<in> mds\<^sub>2 GuarNoReadOrWrite \<longrightarrow> doesnt_read_or_modify (While e c) x) \<and>
              (x \<in> mds\<^sub>2 GuarNoWrite \<longrightarrow> doesnt_modify (While e c) x)"
          unfolding doesnt_read_or_modify_def doesnt_read_or_modify_vars_def and doesnt_modify_def
          using while_eval and while_dest
          by blast
      next
        fix x
        assume "mds' \<le> mdsa"
        let ?c' = "If e (c ;; While e c) Stop"
        have "x \<in> mds' GuarNoReadOrWrite \<longrightarrow> doesnt_read_or_modify ?c' x"
          apply clarify
          apply (rule if_doesnt_read)
           using IntI `mds' \<le> mdsa` empty_iff le_fun_def while.hyps to_prog_var_set_def
           apply (metis (no_types) in_mono mem_Collect_eq)
          by (metis `mds' \<le> mdsa` le_fun_def set_rev_mp while.hyps(1,3))
        moreover
        have "x \<in> mds' GuarNoWrite \<longrightarrow> doesnt_modify ?c' x"
          by (metis if_doesnt_modify)
        ultimately show "(x \<in> mds' GuarNoReadOrWrite \<longrightarrow> doesnt_read_or_modify ?c' x) \<and>
                         (x \<in> mds' GuarNoWrite \<longrightarrow> doesnt_modify ?c' x)" ..
      next
        fix x
        assume "mds' \<le> mdsa"
        show "(x \<in> mds' GuarNoReadOrWrite \<longrightarrow> doesnt_read_or_modify Stop x) \<and>
              (x \<in> mds' GuarNoWrite \<longrightarrow> doesnt_modify Stop x)"
          by (metis stop_doesnt_access)
      next
        fix c'' x mem'' mds\<^sub>3
        assume "mds\<^sub>3 \<le> mdsa"
        assume "\<langle>c'', mds', mem'\<rangle> \<in> loc_reach \<langle>c, mds\<^sub>3, mem''\<rangle>"
        thus "(x \<in> mds' GuarNoReadOrWrite \<longrightarrow> doesnt_read_or_modify (c'' ;; While e c) x) \<and>
              (x \<in> mds' GuarNoWrite \<longrightarrow> doesnt_modify (c'' ;; While e c) x)"
          apply auto
           apply (rule seq_doesnt_read)
           apply (insert while)
           apply (metis `mds\<^sub>3 \<le> mdsa` locally_sound_mode_use_def)
          apply (rule seq_doesnt_modify)
          by (metis `mds\<^sub>3 \<le> mdsa` locally_sound_mode_use_def)
      next
        fix x
        assume "mds' \<le> mdsa"
        show "(x \<in> mds' GuarNoReadOrWrite \<longrightarrow> doesnt_read_or_modify (While e c) x) \<and>
              (x \<in> mds' GuarNoWrite \<longrightarrow> doesnt_modify (While e c) x)"
          unfolding doesnt_read_or_modify_def doesnt_read_or_modify_vars_def and doesnt_modify_def
          using while_eval and while_dest
          by blast
      qed
    qed
  qed
next
  case (seq mds c\<^sub>1 mds' c\<^sub>2 mds'')
  thus ?case
  proof (auto)
    fix mem
    assume "\<langle>Stop, mds\<^sub>2', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1 ;; c\<^sub>2, mds\<^sub>2, mem\<rangle>"
    then obtain mds\<^sub>2'' mem'' where "\<langle>Stop, mds\<^sub>2'', mem''\<rangle> \<in> loc_reach \<langle>c\<^sub>1, mds\<^sub>2, mem\<rangle>" and
      "\<langle>Stop, mds\<^sub>2', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, mds\<^sub>2'', mem''\<rangle>"
      using seq_split by blast
    thus "mds\<^sub>2' \<le> mds''"
      using seq by blast
  next
    fix mem
    from seq show "locally_sound_mode_use \<langle>c\<^sub>1 ;; c\<^sub>2, mds\<^sub>2, mem\<rangle>"
      apply (simp add: locally_sound_mode_use_def)
      apply clarify
      apply (drule seq_loc_reach)
      apply (erule disjE)
       apply (metis seq_doesnt_modify seq_doesnt_read)
      by metis
  qed
next
  case (lock_acq l NoW_vars NoRW_vars mds mds')
  thus ?case
  proof (safe)
    fix mem
    assume reach: "\<langle>Stop, mds\<^sub>2', mem'\<rangle> \<in> loc_reach \<langle>LockAcq l, mds\<^sub>2, mem\<rangle>"
    thus "mds\<^sub>2' \<le> lock_acq_upd lock_interp l mds'"
      using lock_acq
      by (fastforce dest:lock_acq_loc_reach le_funD intro: le_funI simp:lock_acq_upd_def split:Mode.split)
  next
    fix mem
    from lock_acq show "locally_sound_mode_use \<langle>LockAcq l, mds\<^sub>2, mem\<rangle>"
      apply(clarsimp simp: locally_sound_mode_use_def)
      apply(drule lock_acq_loc_reach)
      apply(erule disjE)
       apply(simp add: stop_doesnt_access)
      apply clarsimp
      apply(rename_tac x)
      apply(case_tac x)
       (* if it's a lock, it has no assumptions on it *)
       unfolding no_lock_mds_def
       apply (meson le_funD subsetCE)
      (* if it's a program variable, lock_acq doesn't access it *)
      using lock_acq_doesnt_read_progvars lock_acq_doesnt_modify_progvars
      by clarsimp
  qed
next
  case (lock_rel mds l NoW_vars NoRW_vars mds')
  thus ?case
  proof (safe)
    fix mem
    assume reach: "\<langle>Stop, mds\<^sub>2', mem'\<rangle> \<in> loc_reach \<langle>LockRel l, mds\<^sub>2, mem\<rangle>"
    thus "mds\<^sub>2' \<le> lock_rel_upd lock_interp l mds"
    using lock_rel
    by (fastforce dest:lock_rel_loc_reach le_funD intro: le_funI simp:lock_rel_upd_def split:Mode.split)
  next
    fix mem
    from lock_rel show "locally_sound_mode_use \<langle>LockRel l, mds\<^sub>2, mem\<rangle>"
      apply(clarsimp simp: locally_sound_mode_use_def)
      apply(drule lock_rel_loc_reach)
      apply(erule disjE)
       apply(simp add: stop_doesnt_access)
      apply clarsimp
      apply(rename_tac x)
      apply(case_tac x)
       (* if it's a lock, it has no assumptions on it *)
       unfolding no_lock_mds_def
       apply (meson le_funD subsetCE)
      (* if it's a program variable, lock_rel doesn't access it *)
      using lock_rel_doesnt_read_progvars lock_rel_doesnt_modify_progvars
      by clarsimp
  qed
next
  case (sub mds\<^sub>2'' c mds\<^sub>2' mds\<^sub>1 mds\<^sub>1' c\<^sub>1)
  thus ?case
    apply auto
    by (metis (hide_lams, no_types) inf_absorb2 le_infI1)
qed

end

end