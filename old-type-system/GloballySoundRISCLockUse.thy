(*
Author: Robert Sison
*)

section {* A Method for Establishing Globally Sound Use of Modes with Locking Primitives *}

theory GloballySoundRISCLockUse
imports Main "../old-rg-sec/Security" "../risc-lang/RISCLanguage"
begin

locale risc_global_modes =
  risc_lang_no_dma bin_op some_reg some_var some_lock ev\<^sub>L lock_interp lock_val_True lock_val_False +
  sifum_security_init_no_det dma \<C>_vars \<C> eval\<^sub>r undefined INIT BISIM_REQS
  for bin_op :: "'OpId \<Rightarrow> 'Val::{zero} \<Rightarrow> 'Val \<Rightarrow> 'Val"
  and some_reg :: "'Reg"
  and some_var :: "'Var"
  and some_lock :: "'Lock"
  and ev\<^sub>L :: "'Val \<Rightarrow> bool"
  and lock_interp :: "'Lock \<Rightarrow> ('Var set \<times> 'Var set)"
  and lock_val_True :: "'Val"
  and lock_val_False :: "'Val"
  and dma :: "(('Lock, 'Var) Var,'Val) Mem \<Rightarrow> ('Lock, 'Var) Var \<Rightarrow> Sec"
  and \<C>_vars :: "('Lock, 'Var) Var \<Rightarrow> ('Lock, 'Var) Var set"
  and \<C> :: "('Lock, 'Var) Var set"
  and INIT :: "(('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> bool"
  and BISIM_REQS :: "(('Reg, 'Var, 'Lock, 'Val, 'OpId) risc_lang_no_dma.PerThread, ('Lock, 'Var) Var, 'Val) LocalConf rel \<Rightarrow> bool" +
  assumes no_locks_in_\<C>_vars : "\<forall> v l. (Lock l) \<notin> \<C>_vars v"
  assumes no_locks_in_\<C> : "\<forall> l. (Lock l) \<notin> \<C>"
  (* side-condition: that no variable can be managed by more than one lock *)
  assumes lone_lock_per_var : "lone_lock_per_var lock_interp"
  (* simplifies the proofs, and I think this is pretty reasonable to require *)
  assumes no_vacuous_locks : "no_vacuous_locks lock_interp"
  assumes INIT_no_locks_acquired: "\<forall>mem. INIT mem \<longrightarrow> no_locks_acquired mem"

sublocale risc_global_modes \<subseteq> sifum_security_init dma \<C>_vars \<C> eval\<^sub>r undefined INIT BISIM_REQS
  apply(unfold_locales)
  apply(rename_tac lc lc' lc'')
  apply(case_tac lc, rename_tac cms mem, case_tac lc', rename_tac cms' mem', case_tac lc'', rename_tac cms'' mem'')
  apply(case_tac cms, case_tac cms', case_tac cms'')
  using eval\<^sub>r_det by force

context risc_global_modes
begin

lemma conflicts_unreachable_globally_sound:
  "(\<forall> mdss \<in> {mdss'. \<not> compatible_modes mdss'}. mdss \<notin> reachable_mode_states (cms, mem))
  \<Longrightarrow> globally_sound_mode_use (cms, mem)"
  unfolding globally_sound_mode_use_def
  by blast

definition coms_of_gc ::
  "(('Reg, 'Var, 'Lock, 'Val, 'OpId) PerThread, ('Lock, 'Var) Var, 'Val) GlobalConf
  \<Rightarrow> ('Reg, 'Var, 'Lock, 'Val, 'OpId) PerThread list"
where
  "coms_of_gc gc \<equiv> map fst (fst gc)"

definition modes_of_gc ::
  "(('Reg, 'Var, 'Lock, 'Val, 'OpId) PerThread, ('Lock, 'Var) Var, 'Val) GlobalConf
  \<Rightarrow> (('Lock, 'Var) Var Mds) list"
where
  "modes_of_gc gc \<equiv> map snd (fst gc)"

(* Now relies purely on lock_interp, not whether the program actually makes use of the locks. *)
definition NoW_lock_managed_vars :: "'Var set"
where
  "NoW_lock_managed_vars \<equiv> \<Union>l. fst (lock_interp l)"

definition NoRW_lock_managed_vars :: "'Var set"
where
  "NoRW_lock_managed_vars \<equiv> \<Union>l. snd (lock_interp l)"

definition lock_managed_vars :: "'Var set"
where
  "lock_managed_vars \<equiv> NoW_lock_managed_vars \<union> NoRW_lock_managed_vars"

definition no_lock_mds_gc ::
  "(('Reg, 'Var, 'Lock, 'Val, 'OpId) PerThread, ('Lock, 'Var) Var, 'Val) GlobalConf \<Rightarrow> bool"
where
  "no_lock_mds_gc gc \<equiv> \<forall>mds \<in> set (modes_of_gc gc). no_lock_mds mds"

lemma meval_preserves_no_lock_mds_gc:
  "\<lbrakk>((cms, mem), k, (cms', mem')) \<in> meval;
    no_lock_mds_gc (cms, mem)\<rbrakk>
   \<Longrightarrow>  no_lock_mds_gc (cms', mem')"
  unfolding no_lock_mds_gc_def no_lock_mds_def modes_of_gc_def
  apply clarsimp
  by (metis (no_types, lifting) eval_lock_mds_untouched' insertE list_update_overwrite set_swap set_update_memI set_update_subset_insert snd_conv subsetCE)

lemma locks_held_consistent:
  "\<lbrakk>lock_held_mds_correct lock_interp mds l; lock_not_held_mds_correct lock_interp mds l\<rbrakk> \<Longrightarrow> False"
  unfolding lock_held_mds_correct_def lock_not_held_mds_correct_def
            NoW_lock_managed_vars_def NoRW_lock_managed_vars_def
  using no_vacuous_locks no_vacuous_locks_def
  by force

(* 
   This property says that a lock's Guars must be held by all components if the lock is not held,
   and otherwise, that the lock's Asms must be held by exactly one component,
   and its Guars must be held by all others.

   Refusing to specify which component holds the lock at this point is I think the best we can do
   without making the language at the local level aware of component ids, which seems fraught.

   We expect this property to hold in the initial state, because no locks are held by anybody.
*)
definition lock_managed_vars_mds_mem_correct ::
  "(('Reg, 'Var, 'Lock, 'Val, 'OpId) PerThread, ('Lock, 'Var) Var, 'Val) GlobalConf \<Rightarrow> bool"
where
  "lock_managed_vars_mds_mem_correct gc \<equiv>
      \<forall> l. if ev\<^sub>L ((snd gc) (Lock l))
           then \<exists>!i. i < length (fst gc) \<and> (lock_held_mds_correct lock_interp ((modes_of_gc gc) ! i) l \<and>
                (\<forall>j < length (fst gc). i \<noteq> j \<longrightarrow>
                    lock_not_held_mds_correct lock_interp ((modes_of_gc gc) ! j) l))
           else \<forall>i < length (fst gc). lock_not_held_mds_correct lock_interp ((modes_of_gc gc) ! i) l"

(* to check that what we expect of the initial state makes sense *)
lemma "\<lbrakk>no_locks_acquired (snd gc);
        no_lock_mds_gc gc;
        lock_managed_vars_mds_mem_correct gc\<rbrakk> \<Longrightarrow>
         \<forall>i < length (fst gc). \<forall>l. lock_not_held_mds_correct lock_interp ((modes_of_gc gc) ! i) l"
  unfolding no_locks_acquired_def no_lock_mds_gc_def lock_managed_vars_mds_mem_correct_def
  by simp

definition unmanaged_var_modes_compatible ::
  "(('Reg, 'Var, 'Lock, 'Val, 'OpId) PerThread, ('Lock, 'Var) Var, 'Val) GlobalConf \<Rightarrow> bool"
where
  "unmanaged_var_modes_compatible gc \<equiv> (\<forall> (i :: nat) x. i < length (modes_of_gc gc) \<longrightarrow>
   (x \<notin> NoRW_lock_managed_vars \<longrightarrow>
    (Var x \<in> ((modes_of_gc gc) ! i) AsmNoReadOrWrite \<longrightarrow>
     (\<forall> j < length (modes_of_gc gc). j \<noteq> i \<longrightarrow> Var x \<in> ((modes_of_gc gc) ! j) GuarNoReadOrWrite))) \<and>
   (x \<notin> NoW_lock_managed_vars \<longrightarrow>
    (Var x \<in> ((modes_of_gc gc) ! i) AsmNoWrite \<longrightarrow>
     (\<forall> j < length (modes_of_gc gc). j \<noteq> i \<longrightarrow> Var x \<in> ((modes_of_gc gc) ! j) GuarNoWrite))))"

(* to check that compatible_modes (modes_of_gc gc) holds at least in the initial state *)
lemma "\<lbrakk>no_locks_acquired (snd gc);
        no_lock_mds_gc gc;
        lock_managed_vars_mds_mem_correct gc;
        unmanaged_var_modes_compatible gc\<rbrakk> \<Longrightarrow> compatible_modes (modes_of_gc gc)"
  unfolding no_locks_acquired_def no_lock_mds_gc_def lock_managed_vars_mds_mem_correct_def
            compatible_modes_def modes_of_gc_def no_lock_mds_def
  apply clarsimp
  apply(rename_tac i x)
  apply(case_tac x)
   apply clarsimp
  apply clarsimp
  apply(rename_tac i x)
  apply(unfold unmanaged_var_modes_compatible_def)
  apply(erule_tac x=i in allE)
  apply(erule_tac x=x in allE)
  apply(unfold NoRW_lock_managed_vars_def NoW_lock_managed_vars_def modes_of_gc_def)
  apply clarsimp
  unfolding lock_not_held_mds_correct_def
  by blast

(* to check that it implies compatible_modes (modes_of_gc gc) generally *)
lemma management_requirements_ensure_compatibility:
  "\<lbrakk>no_lock_mds_gc gc;
        lock_managed_vars_mds_mem_correct gc;
        unmanaged_var_modes_compatible gc\<rbrakk> \<Longrightarrow> compatible_modes (modes_of_gc gc)"
  unfolding no_lock_mds_gc_def lock_managed_vars_mds_mem_correct_def
            compatible_modes_def modes_of_gc_def no_lock_mds_def
  apply clarsimp
  apply(rename_tac i x)
  apply(case_tac x)
   apply clarsimp
  apply clarsimp
  apply(rename_tac i x)
  apply(unfold unmanaged_var_modes_compatible_def)
  apply(erule_tac x=i in allE)
  apply(erule_tac x=x in allE)
  apply(unfold NoRW_lock_managed_vars_def NoW_lock_managed_vars_def modes_of_gc_def)
  apply clarsimp
  unfolding lock_not_held_mds_correct_def lock_held_mds_correct_def
  by (metis (no_types, hide_lams) nth_map)

(* helpers to cherry-pick the results we want *)
lemma lock_acq_upd_locally_correct:
  "mds' = lock_acq_upd lock_interp l mds \<Longrightarrow>
   lock_held_mds_correct lock_interp mds' l
   \<and> (\<forall>l'. l' \<noteq> l \<longrightarrow> (lock_held_mds_correct lock_interp mds l' \<longrightarrow> lock_held_mds_correct lock_interp mds' l') \<and>
                      (lock_not_held_mds_correct lock_interp mds l' \<longrightarrow> lock_not_held_mds_correct lock_interp mds' l'))"
  apply(rule conjI)
   apply(force simp:lock_held_mds_correct_def lock_acq_upd_def split:prod.split)
  apply(clarsimp simp:lock_held_mds_correct_def lock_not_held_mds_correct_def lock_acq_upd_def image_def split:prod.split)
  using lone_lock_per_var lone_lock_per_var_def
  by (metis (no_types, lifting) Un_iff fst_conv snd_conv)

lemma lock_rel_upd_locally_correct:
  "mds' = lock_rel_upd lock_interp l mds \<Longrightarrow>
   lock_not_held_mds_correct lock_interp mds' l
   \<and> (\<forall>l'. l' \<noteq> l \<longrightarrow> (lock_held_mds_correct lock_interp mds l' \<longrightarrow> lock_held_mds_correct lock_interp mds' l') \<and>
                      (lock_not_held_mds_correct lock_interp mds l' \<longrightarrow> lock_not_held_mds_correct lock_interp mds' l'))"
  apply(rule conjI)
   apply(force simp:lock_not_held_mds_correct_def lock_rel_upd_def split:prod.split)
  apply(clarsimp simp:lock_held_mds_correct_def lock_not_held_mds_correct_def lock_rel_upd_def image_def split:prod.split)
  using lone_lock_per_var lone_lock_per_var_def
  by (metis (no_types, lifting) Un_iff fst_conv snd_conv)

(* Super-irritating gc list plumbing helpers *)

lemma held_stays_correct_others:
  "\<lbrakk>k \<noteq> i; lock_held_mds_correct lock_interp (map snd (fst gc) ! i) l'\<rbrakk>
   \<Longrightarrow> lock_held_mds_correct lock_interp (map snd (fst gc[k := (c', mds')]) ! i) l'"
  apply(clarsimp simp: lock_held_mds_correct_def lock_not_held_mds_correct_def)
  by (simp add: map_update) 

lemma held_stays_correct_k:
  "\<lbrakk>lock_held_mds_correct lock_interp mds' l'; k < length (fst gc)\<rbrakk>
   \<Longrightarrow> lock_held_mds_correct lock_interp (map snd (fst gc[k := (c', mds')]) ! k) l'"
  by (clarsimp simp: lock_held_mds_correct_def lock_not_held_mds_correct_def)

lemma not_held_stays_correct_others:
  "\<lbrakk>k \<noteq> i; lock_not_held_mds_correct lock_interp (map snd (fst gc) ! i) l'\<rbrakk>
   \<Longrightarrow> lock_not_held_mds_correct lock_interp (map snd (fst gc[k := (c', mds')]) ! i) l'"
  apply(clarsimp simp: lock_held_mds_correct_def lock_not_held_mds_correct_def)
  by (simp add: map_update) 

lemma not_held_stays_correct_k:
  "\<lbrakk>lock_not_held_mds_correct lock_interp mds' l'; k < length (fst gc)\<rbrakk>
   \<Longrightarrow> lock_not_held_mds_correct lock_interp (map snd (fst gc[k := (c', mds')]) ! k) l'"
  by (clarsimp simp: lock_held_mds_correct_def lock_not_held_mds_correct_def)

lemma untouched_locks_remain_untouched:
  "\<lbrakk>\<forall>x. x \<noteq> Lock l \<longrightarrow> mem' x = mem x; l \<noteq> l'; ev\<^sub>L (mem (Lock l'))\<rbrakk> \<Longrightarrow> ev\<^sub>L (mem' (Lock l'))"
  by (metis (full_types) Var.inject(1))

lemma lock_eval\<^sub>L_untouched:
  "\<lbrakk>mem (Lock l) = mem' (Lock l); ev\<^sub>L (mem (Lock l))\<rbrakk> \<Longrightarrow> ev\<^sub>L (mem' (Lock l))"
  by metis

declare modes_of_gc_def[simp]

(* Proof that lock_managed_vars_mds_mem_correct is preserved when we take a lock_acq *)
lemma lock_acq_preserves_correctness:
  "\<lbrakk>lock_managed_vars_mds_mem_correct (cms, mem);
    k < length cms;
    cms ! k = (((pc, P), regs), mdsk);
    mdsk' = lock_acq_upd lock_interp x mdsk;
    \<not> ev\<^sub>L (mem (Lock x))\<rbrakk> \<Longrightarrow>
   lock_managed_vars_mds_mem_correct (cms[k := (((Suc pc, P), regs), mdsk')], mem(Lock x := lock_val_True))"
  apply(frule lock_acq_upd_locally_correct)
  apply(clarsimp simp:lock_managed_vars_mds_mem_correct_def eval\<^sub>L_True)
  apply(rename_tac l)
  apply(erule_tac x=l in allE)
  apply(case_tac "x=l")
   apply clarsimp
   apply(erule_tac x=k in allE')
   apply(rule_tac a=k in ex1I)
    apply(rule conjI)
     apply force
    apply force
   apply clarsimp
   apply(metis locks_held_consistent nth_list_update_neq)
  apply clarsimp
  apply(split if_splits)
   apply(erule ex1E)
   apply(erule_tac x=i in allE)
   apply clarsimp
   apply(rule_tac a=i in ex1I)
    apply clarsimp
    apply(case_tac "k=i")
     apply force
    apply clarsimp
    apply(rename_tac j)
    apply(erule_tac x=j in allE)
    using nth_list_update apply fastforce
   apply clarsimp
   apply(metis locks_held_consistent nth_list_update)
  apply clarsimp
  by (metis nth_list_update snd_conv)

(* Proof that lock_managed_vars_mds_mem_correct is preserved when we take a lock_rel *)
lemma lock_rel_preserves_correctness:
  "\<lbrakk>lock_managed_vars_mds_mem_correct (cms, mem);
    k < length cms;
    cms ! k = (((pc, P), regs), mdsk);
    mdsk' = lock_rel_upd lock_interp x mdsk;
    lock_held_mds_correct lock_interp mdsk x\<rbrakk> \<Longrightarrow>
   lock_managed_vars_mds_mem_correct (cms[k := (((Suc pc, P), regs), mdsk')], mem (Lock x := lock_val_False))"
  apply(frule lock_rel_upd_locally_correct)
  apply(clarsimp simp:lock_managed_vars_mds_mem_correct_def eval\<^sub>L_False)
  apply(rename_tac l)
  apply(erule_tac x=l in allE)
  apply(case_tac "x=l")
   apply clarsimp
   apply(metis (no_types, hide_lams) fst_conv locks_held_consistent modes_of_gc_def nth_list_update_eq nth_list_update_neq nth_map snd_conv)
  apply clarsimp
  apply(split if_splits)
   apply(erule ex1E)
   apply(erule_tac x=i in allE)
   apply clarsimp
   apply(rule_tac a=i in ex1I)
    apply clarsimp
    apply(case_tac "k=i")
     apply force
    apply clarsimp
    apply(rename_tac j)
    apply(erule_tac x=j in allE)
    using nth_list_update apply fastforce
   apply clarsimp
   apply(metis locks_held_consistent nth_list_update)
  apply clarsimp
  by (metis nth_list_update snd_conv)

declare modes_of_gc_def[simp del]

(* helper for the commands that don't touch locks nor mds, i.e. everything(?) but lock_acq/rel *)
lemma non_lock_commands_preserve_correctness:
  "\<lbrakk>lock_managed_vars_mds_mem_correct (cms, mem);
        k < length cms;
        cms' = cms[k := (ck', mdsk')];
        cms ! k = (ck, mdsk);
        mdsk = mdsk';
        \<forall>l. mem (Lock l) = mem' (Lock l)\<rbrakk> \<Longrightarrow>
       lock_managed_vars_mds_mem_correct (cms', mem')"
  unfolding lock_managed_vars_mds_mem_correct_def
            modes_of_gc_def
  apply clarsimp
  apply(erule_tac x=l in allE)
  apply(erule_tac x=l in allE)
  apply(split if_splits)
   apply(erule ex1E)
   apply(erule conjE)+
   apply(rule conjI)
    apply(rule impI)
    apply(rule_tac a=i in ex1I)
     apply(rule conjI)
      apply(metis fst_conv)
     apply(rule conjI)
      apply(metis fst_conv list_update_id map_update snd_conv)
     apply(metis fst_conv nth_list_update_eq nth_list_update_neq nth_map snd_conv)
    apply(metis locks_held_consistent fst_conv nth_list_update_eq nth_list_update_neq nth_map snd_conv)
   apply blast
  apply clarsimp
  by (metis nth_list_update_eq nth_list_update_neq snd_conv)

lemma eval_preserves_correctness:
  "\<lbrakk>lock_managed_vars_mds_mem_correct (cms, mem);
    (\<langle>(pc, P), regs, mdsk, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mdsk', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r;
    k < length cms;
    cms ! k = (((pc, P), regs), mdsk)\<rbrakk> \<Longrightarrow>
   lock_managed_vars_mds_mem_correct (cms[k := (((pc', P'), regs'), mdsk')], mem')"
  apply(erule eval\<^sub>r_elim)
                apply(force simp:non_lock_commands_preserve_correctness)
               apply(force simp:lock_acq_preserves_correctness)
              apply(force simp:non_lock_commands_preserve_correctness)
             apply(force simp:lock_rel_preserves_correctness)
            apply(force simp:non_lock_commands_preserve_correctness)
           apply(force simp:non_lock_commands_preserve_correctness)
          apply(force simp:non_lock_commands_preserve_correctness)
         apply(force simp:non_lock_commands_preserve_correctness)
        apply(force simp:non_lock_commands_preserve_correctness)
       apply(force simp:non_lock_commands_preserve_correctness)
      apply(force simp:non_lock_commands_preserve_correctness)
     apply(force simp:non_lock_commands_preserve_correctness)
    apply(force simp:non_lock_commands_preserve_correctness)
   apply(force simp:non_lock_commands_preserve_correctness)
  apply(force simp:non_lock_commands_preserve_correctness)
  done

lemma meval_preserves_correctness:
  "\<lbrakk>lock_managed_vars_mds_mem_correct (cms, mem);
    ((cms, mem), k, (cms', mem')) \<in> meval
   \<rbrakk> \<Longrightarrow> lock_managed_vars_mds_mem_correct (cms', mem')"
  apply clarsimp
  apply(rename_tac ck' mdsk')
  apply(cases "cms ! k")
  apply clarsimp
  apply(rename_tac ck mdsk)
  using eval_preserves_correctness
  by simp

thm management_requirements_ensure_compatibility
    meval_preserves_correctness
    meval_preserves_no_lock_mds_gc

lemma non_lock_commands_preserve_unmanaged_modes_compatibility:
"\<lbrakk>unmanaged_var_modes_compatible (cms, mem);
        k < length cms;
        cms' = cms[k := (ck', mdsk')];
        cms ! k = (ck, mdsk);
        mdsk = mdsk';
        \<forall>l. mem (Lock l) = mem' (Lock l)\<rbrakk>
 \<Longrightarrow> unmanaged_var_modes_compatible (cms', mem')"
  apply(clarsimp simp:unmanaged_var_modes_compatible_def modes_of_gc_def)
  apply(erule_tac x=i in allE)
  apply(erule impE)
   apply clarsimp
  apply(erule_tac x=x in allE)
  apply clarsimp
  by (metis nth_list_update_eq nth_list_update_neq snd_conv)

lemma lock_acq_touches_only_managed_modes:
  "mds' = lock_acq_upd lock_interp l mds \<Longrightarrow>
    (\<forall>x. x \<notin> NoW_lock_managed_vars \<longrightarrow> (\<forall>m \<in> {AsmNoWrite, GuarNoWrite}.
      (Var x \<in> mds m \<longleftrightarrow> Var x \<in> mds' m)))
    \<and>
    (\<forall>x. x \<notin> NoRW_lock_managed_vars \<longrightarrow> (\<forall>m \<in> {AsmNoReadOrWrite, GuarNoReadOrWrite}.
      (Var x \<in> mds m \<longleftrightarrow> Var x \<in> mds' m)))"
  unfolding NoW_lock_managed_vars_def NoRW_lock_managed_vars_def
  apply(clarsimp simp:lock_acq_upd_def split:prod.splits)
  apply safe
     apply(metis fst_conv)
    apply(metis fst_conv)
   apply(metis snd_conv)
  apply(metis snd_conv)
  done

lemma lock_rel_touches_only_managed_modes:
  "mds' = lock_rel_upd lock_interp l mds \<Longrightarrow>
    (\<forall>x. x \<notin> NoW_lock_managed_vars \<longrightarrow> (\<forall>m \<in> {AsmNoWrite, GuarNoWrite}.
      (Var x \<in> mds m \<longleftrightarrow> Var x \<in> mds' m)))
    \<and>
    (\<forall>x. x \<notin> NoRW_lock_managed_vars \<longrightarrow> (\<forall>m \<in> {AsmNoReadOrWrite, GuarNoReadOrWrite}.
      (Var x \<in> mds m \<longleftrightarrow> Var x \<in> mds' m)))"
  unfolding NoW_lock_managed_vars_def NoRW_lock_managed_vars_def
  apply(clarsimp simp:lock_rel_upd_def split:prod.splits)
  apply safe
     apply(metis fst_conv)
    apply(metis fst_conv)
   apply(metis snd_conv)
  apply(metis snd_conv)
  done

lemma lock_acq_preserves_unmanaged_modes_compatibility:
  "\<lbrakk>unmanaged_var_modes_compatible (cms, mem);
    k < length cms;
    cms ! k = (((pc, P), regs), mdsk);
    mdsk' = lock_acq_upd lock_interp l mdsk\<rbrakk> \<Longrightarrow>
   unmanaged_var_modes_compatible (cms[k := (((Suc pc, P), regs), mdsk')], mem(Lock l := lock_val_True))"
  apply(clarsimp simp:unmanaged_var_modes_compatible_def modes_of_gc_def)
  by (metis (no_types, lifting) insert_iff lock_acq_touches_only_managed_modes nth_list_update snd_conv)

lemma lock_rel_preserves_unmanaged_modes_compatibility:
  "\<lbrakk>unmanaged_var_modes_compatible (cms, mem);
    k < length cms;
    cms ! k = (((pc, P), regs), mdsk);
    mdsk' = lock_rel_upd lock_interp l mdsk\<rbrakk> \<Longrightarrow>
   unmanaged_var_modes_compatible (cms[k := (((Suc pc, P), regs), mdsk')], mem(Lock l := lock_val_False))"
  apply(clarsimp simp:unmanaged_var_modes_compatible_def modes_of_gc_def)
  by (metis (no_types, lifting) insert_iff lock_rel_touches_only_managed_modes nth_list_update snd_conv)

lemma eval_preserves_unmanaged_var_modes_compatibility:
  "\<lbrakk>unmanaged_var_modes_compatible (cms, mem);
    (\<langle>(pc, P), regs, mdsk, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mdsk', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r;
    k < length cms;
    cms ! k = (((pc, P), regs), mdsk)\<rbrakk> \<Longrightarrow>
   unmanaged_var_modes_compatible (cms[k := (((pc', P'), regs'), mdsk')], mem')"
  apply(erule eval\<^sub>r_elim)
                apply(force simp:non_lock_commands_preserve_unmanaged_modes_compatibility)
               apply(force simp:lock_acq_preserves_unmanaged_modes_compatibility)
              apply(force simp:non_lock_commands_preserve_unmanaged_modes_compatibility)
             apply(force simp:lock_rel_preserves_unmanaged_modes_compatibility)
            apply(force simp:non_lock_commands_preserve_unmanaged_modes_compatibility)
           apply(force simp:non_lock_commands_preserve_unmanaged_modes_compatibility)
          apply(force simp:non_lock_commands_preserve_unmanaged_modes_compatibility)
         apply(force simp:non_lock_commands_preserve_unmanaged_modes_compatibility)
        apply(force simp:non_lock_commands_preserve_unmanaged_modes_compatibility)
       apply(force simp:non_lock_commands_preserve_unmanaged_modes_compatibility)
      apply(force simp:non_lock_commands_preserve_unmanaged_modes_compatibility)
     apply(force simp:non_lock_commands_preserve_unmanaged_modes_compatibility)
    apply(force simp:non_lock_commands_preserve_unmanaged_modes_compatibility)
   apply(force simp:non_lock_commands_preserve_unmanaged_modes_compatibility)
  apply(force simp:non_lock_commands_preserve_unmanaged_modes_compatibility)
  done

lemma meval_preserves_unmanaged_var_modes_compatibility:
  "\<lbrakk>((cms, mem), k, (cms', mem')) \<in> meval;
      unmanaged_var_modes_compatible (cms, mem)\<rbrakk>
      \<Longrightarrow> unmanaged_var_modes_compatible (cms', mem')"
  using eval_preserves_unmanaged_var_modes_compatibility
  by (metis prod.collapse meval.simps)

lemma meval_sched_preserves_no_lock_mds_gc:
  "\<lbrakk>no_lock_mds_gc (cms, mem);
       meval_sched sched (cms, mem) (cms', mem')\<rbrakk> \<Longrightarrow>
       no_lock_mds_gc (cms', mem')"
  using meval_preserves_no_lock_mds_gc
  apply(induct rule:meval_sched.induct)
  apply clarsimp+
  by blast

lemma meval_sched_preserves_correctness:
  "\<lbrakk>lock_managed_vars_mds_mem_correct (cms, mem);
       meval_sched sched (cms, mem) (cms', mem')\<rbrakk> \<Longrightarrow>
       lock_managed_vars_mds_mem_correct (cms', mem')"
  using meval_preserves_correctness
    apply(induct rule:meval_sched.induct)
  apply clarsimp+
  by blast

lemma meval_sched_preserves_unmanaged_var_modes_compatibility:
  "\<lbrakk>unmanaged_var_modes_compatible (cms, mem);
       meval_sched sched (cms, mem) (cms', mem')\<rbrakk> \<Longrightarrow>
       unmanaged_var_modes_compatible (cms', mem')"
  using meval_preserves_unmanaged_var_modes_compatibility
  apply(induct rule:meval_sched.induct)
  apply clarsimp+
  by blast

lemma reachable_gc_retain_management_requirements:
  "\<lbrakk>no_lock_mds_gc (cms, mem);
       lock_managed_vars_mds_mem_correct (cms, mem);
       unmanaged_var_modes_compatible (cms, mem);
       meval_sched sched (cms, mem) (cms', mem')\<rbrakk> \<Longrightarrow>
       no_lock_mds_gc (cms', mem') \<and> lock_managed_vars_mds_mem_correct (cms', mem')
       \<and> unmanaged_var_modes_compatible (cms', mem')"
  by (meson meval_sched_preserves_correctness meval_sched_preserves_no_lock_mds_gc meval_sched_preserves_unmanaged_var_modes_compatibility)

lemma reachable_mdss_are_compatible:
  "\<lbrakk> no_lock_mds_gc (cms, mem);
     lock_managed_vars_mds_mem_correct (cms, mem);
     unmanaged_var_modes_compatible (cms, mem);
     mdss \<in> reachable_mode_states (cms, mem) \<rbrakk> \<Longrightarrow>
     compatible_modes mdss"
proof -
  assume a1: "no_lock_mds_gc (cms, mem)"
  assume a2: "lock_managed_vars_mds_mem_correct (cms, mem)"
  assume a3: "unmanaged_var_modes_compatible (cms, mem)"
  assume "mdss \<in> reachable_mode_states (cms, mem)"
  then have "\<exists>ps f ns. meval_sched_abv (cms, mem) ns (ps, f) \<and> map snd ps = mdss"
    by (simp add: reachable_mode_states_def)
  then show ?thesis
    using a3 a2 a1 reachable_gc_retain_management_requirements
    by (metis (no_types) management_requirements_ensure_compatibility fst_conv modes_of_gc_def)
qed

theorem management_requirements_ensure_global_soundness:
  "\<lbrakk>no_lock_mds_gc gc;
    lock_managed_vars_mds_mem_correct gc;
    unmanaged_var_modes_compatible gc\<rbrakk> \<Longrightarrow> globally_sound_mode_use gc"
  unfolding globally_sound_mode_use_def
  using reachable_mdss_are_compatible
  by (metis prod.collapse)

end

end