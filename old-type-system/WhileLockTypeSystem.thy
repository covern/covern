(*
Authors: Robert Sison, Edward Pierzchalski
Based on the Dependent_SIFUM_Type_Systems AFP entry, whose authors are:
 Toby Murray, Robert Sison, Edward Pierzchalski, Christine Rizkallah
in turn based on the SIFUM_Type_Systems AFP entry, whose authors are:
 Sylvia Grewe, Heiko Mantel, Daniel Schoepe
*)

section {* Type System for Ensuring Dependent SIFUM-Security of Commands for the Lock Language *}

theory WhileLockTypeSystem
imports Main "../while-lang/WhileLockLanguage" "../old-rg-sec/Compositionality"
begin

subsection {* Typing Rules *}

text {*
  Types now depend on memories. To see why, consider an assignment in which some variable
  @{term x} for which we have a @{term AsmNoReadOrWrite} assumption is assigned the value in 
  variable @{term input}, but where @{term input}'s classification depends on some control
  variable. Then the new type of @{term x} depends on memory. If we were to just take the 
  upper bound of @{term input}'s classification, this would likely give us @{term High} as
  @{term x}'s type, but that would prevent us from treating @{term x} as @{term Low}
  if we later learn @{term input}'s original classification. 
  
  Instead we need to make @{term x}'s type explicitly depend on memory so later on, once we
  learn @{term input}'s classification, we can resolve @{term x}'s type to a concrete
  security level.
  
  We choose to deeply embed types as sets of boolean expressions. If any expression in the
  set evaluates to @{term True}, the type is @{term High}; otherwise it is @{term Low}.
*}
type_synonym 'BExp Type = "'BExp set"

text {*
  We require @{term \<Gamma>} to track all stable (i.e. @{term AsmNoWrite} or @{term AsmNoReadOrWrite}), 
  non-@{term \<C>} variables.
  
  This differs from Mantel a bit. Mantel would exclude from @{term \<Gamma>}, variables
  whose classification (according to @{term dma}) is @{term Low} for which we have only
  an @{term AsmNoWrite} assumption.
  
  We decouple the requirement for inclusion in @{term \<Gamma>} from a variable's classification
  so that we don't need to be updating @{term \<Gamma>} each time we alter a control variable.
  Even if we tried to keep @{term \<Gamma>} up-to-date in that case, we may not be able to 
  precisely compute the new classification of each variable after the modification anyway.
*}
type_synonym ('Var,'BExp) TyEnv = "'Var \<rightharpoonup> 'BExp Type"

text {*
  This records which variables are \emph{stable} in that we have an assumption
  implying that their value won't change. It duplicates a bit of info in
  @{term \<Gamma>} above but I haven't yet thought of a way to remove that duplication
  cleanly.
  
  The first component of the pair records variables for which we have
  @{term AsmNoWrite}; the second component is for @{term AsmNoReadOrWrite}.
  
  The reason we want to distinguish the different kinds of assumptions is to know whether
  a variable should remain in @{term \<Gamma>} when we drop an assumption on it. If we drop
  e.g. @{term AsmNoWrite} but also have @{term AsmNoReadOrWrite} then if we didn't track
  stability info this way we wouldn't know whether we had to remove the variable from
  @{term \<Gamma>} or not.
*}
type_synonym 'Var Stable = "('Var set \<times> 'Var set)"

text {*
  We track a set of predicates on memories as we execute. If we evaluate a boolean expression
  all of whose variables are stable, then we enrich this set predicate with that one.
  If we assign to a stable variable, then we enrich this predicate also.
  If we release an assumption making a variable unstable, we need to remove all predicates that
  pertain to it from this set.
  
  This needs to be deeply embedded (i.e. it cannot be stored as a predicate of type
  @{typ "('Var,'Val) Mem \<Rightarrow> bool"} or even @{typ "('Var,'Val) Mem set"}), because we need to be 
  able to identify each individual predicate and for each predicate identify all of the
  variables in it, so we can discard the right predicates each time a variable becomes unstable.
*}
type_synonym 'bexp preds = "'bexp set"

datatype 'Lock LockUpd = Acq 'Lock | Rel 'Lock

context sifum_lang_no_dma begin

definition
  pred :: "'BExp preds \<Rightarrow> (('Lock, 'Var) Var,'Val) Mem \<Rightarrow> bool"
where
  "pred P \<equiv> \<lambda>mem. (\<forall>p\<in>P. eval\<^sub>B (to_prog_mem mem) p)"

end

(* FIXME: The original type system uses ev\<^sub>B to express the predicates
   and here in this adaptation ev\<^sub>B only refers to Var variables,
   but the new lock language has dma functions that might refer to Lock variables.
   For now we will consider only security policies with dma (domain assignment)
   functions that do not refer to Lock states *)

locale sifum_types =
  sifum_lang_no_dma ev\<^sub>A ev\<^sub>B ev\<^sub>L aexp_vars bexp_vars lock_interp lock_val_True lock_val_False +
  sifum_security_init dma \<C>_vars \<C> eval\<^sub>w undefined no_locks_acquired bisim_simple_ev\<^sub>B_eq
  for ev\<^sub>A :: "('Var, 'Val) Mem \<Rightarrow> 'AExp \<Rightarrow> 'Val"
  and ev\<^sub>B :: "('Var, 'Val) Mem \<Rightarrow> 'BExp \<Rightarrow> bool"
  and ev\<^sub>L :: "'Val \<Rightarrow> bool"
  and aexp_vars :: "'AExp \<Rightarrow> 'Var set"
  and bexp_vars :: "'BExp \<Rightarrow> 'Var set"
  and lock_interp :: "'Lock \<Rightarrow> ('Var set \<times> 'Var set)"
  and dma :: "(('Lock, 'Var) Var,'Val) Mem \<Rightarrow> ('Lock, 'Var) Var \<Rightarrow> Sec"
  and \<C>_vars :: "('Lock, 'Var) Var \<Rightarrow> ('Lock, 'Var) Var set"
  and \<C> :: "('Lock, 'Var) Var set"
  and lock_val_True :: "'Val"
  and lock_val_False :: "'Val" +
  (* we need to be able to negate predicates, when we explore the ELSE branch of an IF *)
  fixes bexp_neg :: "'BExp \<Rightarrow> 'BExp"
  assumes bexp_neg_negates: "\<And>mem e. (ev\<^sub>B mem (bexp_neg e)) = (\<not> (ev\<^sub>B mem e))" 
  (* we need to be able to compute a valid postcondition after an assignment *)
  fixes assign_post :: "'BExp preds \<Rightarrow> 'Var \<Rightarrow> 'AExp \<Rightarrow> 'BExp preds"
  assumes assign_post_valid: "\<And>mem x. pred P mem \<Longrightarrow> 
    pred (assign_post P x e) (mem((Var x) := ev\<^sub>A (to_prog_mem mem) e))"
  fixes dma_type :: "('Lock, 'Var) Var \<Rightarrow> 'BExp set"
  assumes dma_correct:
    "dma mem (x :: ('Lock, 'Var) Var) = (if (\<forall>e\<in>dma_type x. ev\<^sub>B (to_prog_mem mem) e) then Low else High)"
  (* for now we consider only dma domain assignment functions that refer only to Vars *)
  assumes \<C>_vars_correct:
    "\<C>_vars (x :: ('Lock, 'Var) Var) = Var ` (\<Union>(bexp_vars ` dma_type x))"
  fixes pred_False :: 'BExp
  assumes pred_False_is_False: "\<not> ev\<^sub>B (to_prog_mem mem) pred_False"
  assumes bexp_vars_pred_False: "bexp_vars pred_False = {}"
  (* We impose this requirement so we can prove low-bisimilar memories have the same lock state *)
  assumes locks_default_dma_low: "\<forall>l. dma_type (Lock l) = {}"
  (* a variable is protected by a lock iff its control variables are. the reason is because
     when gaining access to a variable its control vars must be stable, also modifying the
     control variable modifies the variable (by changing its classification), so gaining access
     to the control variable already grants access to the variable in a sense.  *)
  assumes lock_interp_\<C>_vars\<^sub>V: 
      "\<And>c x f. f \<in> {fst,snd} \<Longrightarrow> Var c \<in> \<C>_vars (Var x) \<Longrightarrow> 
                 (c \<in> f (lock_interp l)) = (x \<in> f (lock_interp l))"

(* a more specialised form of the above locale useful for the examples that provides
   a brain-dead instantiation for the assignment postcondition transformer *)
locale sifum_types_assign =
  sifum_lang_no_dma ev\<^sub>A ev\<^sub>B ev\<^sub>L aexp_vars bexp_vars lock_interp lock_val_True lock_val_False +
  sifum_security_init dma \<C>_vars \<C> eval\<^sub>w undefined no_locks_acquired bisim_simple_ev\<^sub>B_eq
  for ev\<^sub>A :: "('Var, 'Val) Mem \<Rightarrow> 'AExp \<Rightarrow> 'Val"
  and ev\<^sub>B :: "('Var, 'Val) Mem \<Rightarrow> 'BExp \<Rightarrow> bool"
  and ev\<^sub>L :: "'Val \<Rightarrow> bool"
  and aexp_vars :: "'AExp \<Rightarrow> 'Var set"
  and bexp_vars :: "'BExp \<Rightarrow> 'Var set"
  and lock_interp :: "'Lock \<Rightarrow> ('Var set \<times> 'Var set)"
  and dma :: "(('Lock, 'Var) Var,'Val) Mem \<Rightarrow> ('Lock, 'Var) Var \<Rightarrow> Sec"
  and \<C>_vars :: "('Lock, 'Var) Var \<Rightarrow> ('Lock, 'Var) Var set"
  and \<C> :: "('Lock, 'Var) Var set"
  and lock_val_True :: "'Val"
  and lock_val_False :: "'Val" +
  (* we need to be able to negate predicates, when we explore the ELSE branch of an IF *)
  fixes bexp_neg :: "'BExp \<Rightarrow> 'BExp"
  assumes bexp_neg_negates_assign: "\<And>mem e. (ev\<^sub>B mem (bexp_neg e)) = (\<not> (ev\<^sub>B mem e))"
  fixes dma_type :: "('Lock, 'Var) Var \<Rightarrow> 'BExp set"
  assumes dma_correct_assign:
    "dma mem (x :: ('Lock, 'Var) Var) = (if (\<forall>e\<in>dma_type x. ev\<^sub>B (to_prog_mem mem) e) then Low else High)"
  (* for now we consider only dma domain assignment functions that refer only to Vars *)
  assumes \<C>_vars_correct_assign:
    "\<C>_vars (x :: ('Lock, 'Var) Var) = Var ` (\<Union>(bexp_vars ` dma_type x))"
  fixes pred_False :: 'BExp
  assumes pred_False_is_False_assign: "\<not> ev\<^sub>B (to_prog_mem mem) pred_False"
  assumes bexp_vars_pred_False_assign: "bexp_vars pred_False = {}"
  assumes locks_default_dma_low_assign: "\<forall>l. dma_type (Lock l) = {}"
  assumes lock_interp_\<C>_vars\<^sub>V_assign: 
      "\<And>c x f. f \<in> {fst,snd} \<Longrightarrow> Var c \<in> \<C>_vars (Var x) \<Longrightarrow> 
                 (c \<in> f (lock_interp l)) = (x \<in> f (lock_interp l))"
  (* we need to be able to say "variable x has value e" when we assign (the evaluation of e) to x *)
  fixes bexp_assign :: "'Var \<Rightarrow> 'AExp \<Rightarrow> 'BExp"
  assumes bexp_assign_eval: "\<And>mem e x. (ev\<^sub>B mem (bexp_assign x e)) = (mem x = (ev\<^sub>A mem e))"
  assumes bexp_assign_vars: "\<And>e x. (bexp_vars (bexp_assign x e)) = aexp_vars e \<union> {x}"


(* things needed by both sifum_types_assign and sifum_types, before we prove the former a sublocale *)
context sifum_lang_no_dma begin

definition
  stable :: "('Lock, 'Var) Var Stable \<Rightarrow> ('Lock, 'Var) Var \<Rightarrow> bool"
where
  "stable \<S> x \<equiv> x \<in> (fst \<S> \<union> snd \<S>)"

definition
  add_pred :: "'BExp preds \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> 'BExp \<Rightarrow> 'BExp preds" ("_ +\<^sub>_ _" [120, 120, 120] 1000)
where
  "P +\<^sub>\<S> e \<equiv> (if (\<forall>x\<in> Var ` bexp_vars e. stable \<S> x) then P \<union> {e} else P)"

lemma add_pred_subset:
  "P \<subseteq> P +\<^sub>\<S> p"
  apply(clarsimp simp: add_pred_def)
  done

(* TODO: overloads the syntax for partial functions --- pick something else? *)
(* this is why we can't have nice things -robs. *)
definition
  restrict_preds_to_vars :: "'BExp preds \<Rightarrow> ('Lock, 'Var) Var set \<Rightarrow> 'BExp preds" ("_ |`' _" [120, 120] 1000)
where
  "P |`' V \<equiv> {e. e \<in> P \<and> Var ` bexp_vars e \<subseteq> V}"

end

context sifum_types_assign begin

text {*
  the most simple assignment postcondition transformer
*}
definition
  assign_post :: "'BExp preds \<Rightarrow> 'Var \<Rightarrow> 'AExp \<Rightarrow> 'BExp preds"
where
  "assign_post P x e \<equiv> 
           (if x \<in> (aexp_vars e) then 
              (restrict_preds_to_vars P  (-{Var x})) 
            else 
              (restrict_preds_to_vars P  (-{Var x})) \<union> {bexp_assign x e})"
 
end

sublocale sifum_types_assign \<subseteq> sifum_types _ _ _ _ _ _ _ _ _ _ _ _ assign_post
  apply(unfold_locales)
      prefer 8
      using lock_interp_\<C>_vars\<^sub>V_assign apply blast
     using bexp_neg_negates_assign apply blast
    apply(clarsimp simp: assign_post_def pred_def to_prog_mem_def | safe)+
       apply(rename_tac p)
       unfolding restrict_preds_to_vars_def image_def
       apply clarsimp
       apply(erule_tac x=p in ballE)
        using eval_vars_det\<^sub>B
        apply -
        apply(erule_tac x=p in meta_allE)
        apply(erule_tac x="\<lambda>v. mem (Var v)" in  meta_allE)
        apply(erule_tac x="\<lambda>v. if v = x then ev\<^sub>A (\<lambda>v. mem (Var v)) e else mem (Var v)" in meta_allE)
        apply fastforce
       apply clarsimp
      unfolding bexp_assign_eval
      using eval_vars_det\<^sub>A
      apply fastforce
     apply(rename_tac p)
     apply clarsimp
     apply(erule_tac x=p in ballE)
      using eval_vars_det\<^sub>B
      apply -
      apply(erule_tac x=p in meta_allE)
      apply(erule_tac x="\<lambda>v. mem (Var v)" in meta_allE)
      apply(erule_tac x="\<lambda>v. if v = x then ev\<^sub>A (\<lambda>v. mem (Var v)) e else mem (Var v)" in meta_allE)
      apply fastforce
     apply clarsimp
    using dma_correct_assign apply blast
   using \<C>_vars_correct_assign image_def apply metis
  using \<C>_vars_correct_assign pred_False_is_False_assign bexp_vars_pred_False_assign locks_default_dma_low_assign lock_interp_\<C>_vars\<^sub>V_assign apply fast+
  done

context sifum_types
begin

lemma no_locks_in_\<C>_vars : "\<forall> v l. (Lock l) \<notin> \<C>_vars v"
  using \<C>_vars_correct
  by clarsimp

lemma no_locks_in_\<C> : "\<forall> l. (Lock l) \<notin> \<C>"
  using no_locks_in_\<C>_vars \<C>_def
  by simp

(* Redefined since Isabelle does not seem to be able to reuse the abbreviation from the old locale *)
abbreviation mm_equiv_abv2 :: "(_, _, _) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
(infix "\<approx>" 60)
  where "mm_equiv_abv2 c c' \<equiv> mm_equiv_abv c c'"

abbreviation eval_abv2 :: "(_, (_, 'Var) Var, 'Val) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
  (infixl "\<leadsto>" 70)
  where
  "x \<leadsto> y \<equiv> (x, y) \<in> eval\<^sub>w"
  
abbreviation eval_plus_abv :: "(_, (_, 'Var) Var, 'Val) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
  (infixl "\<leadsto>\<^sup>+" 70)
  where
  "x \<leadsto>\<^sup>+ y \<equiv> (x, y) \<in> eval\<^sub>w\<^sup>+"
  
abbreviation no_eval_abv :: "(_, (_, 'Var) Var, 'Val) LocalConf \<Rightarrow> bool" 
  ("_ \<leadsto> \<bottom>")
  where
  "x \<leadsto> \<bottom> \<equiv> \<forall> y. (x, y) \<notin> eval\<^sub>w"

abbreviation low_indistinguishable_abv :: "('Lock, 'Var) Var Mds \<Rightarrow> ('Var, 'Lock, 'AExp, 'BExp) Stmt \<Rightarrow> (_, _, _, _) Stmt \<Rightarrow> bool"
  ("_ \<sim>\<index> _" [100, 100] 80)
  where
  "c \<sim>\<^bsub>mds\<^esub> c' \<equiv> low_indistinguishable mds c c'"

abbreviation
  vars_of_type :: "'BExp Type \<Rightarrow> ('Lock, 'Var) Var set"
where
  "vars_of_type t \<equiv> Var ` \<Union>(bexp_vars ` t)"
  
(* this is going to appear in a -lot- of places thanks to some of the definitions floating around. *)
definition L :: "'Lock itself" where
  "L \<equiv> TYPE('Lock)"
  
definition
  type_wellformed :: "'BExp Type \<Rightarrow> bool"
where
  "type_wellformed t \<equiv> vars_of_type t \<subseteq> \<C>"
  
definition 
  update_modes :: "'Lock LockUpd \<Rightarrow> ('Lock, 'Var) Var Mds \<Rightarrow> ('Lock, 'Var) Var Mds"
where
  "update_modes upd mds \<equiv> case upd of
    Acq l \<Rightarrow> lock_acq_upd lock_interp l mds |
    Rel l \<Rightarrow> lock_rel_upd lock_interp l mds"

lemma dma_type_wellformed [simp]:
  "type_wellformed (dma_type x)"
  apply(clarsimp simp: type_wellformed_def  \<C>_def | safe)+
  using \<C>_vars_correct apply blast
  done
  
definition 
  to_total :: "(('Lock, 'Var) Var, 'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var \<Rightarrow> 'BExp Type"
where 
  "to_total \<Gamma> \<equiv> \<lambda>v. if v \<in> dom \<Gamma> then the (\<Gamma> v) else dma_type v"

definition
  types_wellformed :: "(('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> bool"
where
  "types_wellformed \<Gamma> \<equiv> \<forall>x\<in>dom \<Gamma>. type_wellformed (the (\<Gamma> x))"
  
lemma to_total_type_wellformed:
  "types_wellformed \<Gamma> \<Longrightarrow>
  type_wellformed (to_total \<Gamma> x)"
  by(auto simp: to_total_def types_wellformed_def)
  
lemma Un_type_wellformed:
  "\<forall>t\<in>ts. type_wellformed t \<Longrightarrow> type_wellformed (\<Union> ts)"
  apply(clarsimp simp: type_wellformed_def | safe)+
  using \<C>_def contra_subsetD by fastforce

inductive 
  type_aexpr :: "(('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> 'AExp \<Rightarrow> 'BExp Type \<Rightarrow> bool" ("_ \<turnstile>\<^sub>a _ \<in> _" [120, 120, 120] 1000)
where
  type_aexpr [intro!]: "\<Gamma> \<turnstile>\<^sub>a e \<in> \<Union> (image (\<lambda> x. to_total \<Gamma> x) (Var ` aexp_vars e))"

lemma type_aexprI:
  "t =  \<Union> (image (\<lambda> x. to_total \<Gamma> x) (Var ` aexp_vars e)) \<Longrightarrow> \<Gamma> \<turnstile>\<^sub>a e \<in> t"
  apply(erule ssubst)
  apply(rule type_aexpr.intros)
  done

lemma type_aexpr_type_wellformed:
  "types_wellformed \<Gamma> \<Longrightarrow> \<Gamma> \<turnstile>\<^sub>a e \<in> t \<Longrightarrow> type_wellformed t"
  apply(erule type_aexpr.cases)
  apply(erule ssubst, rule Un_type_wellformed)
  apply clarsimp
  apply(blast intro: to_total_type_wellformed)
  done
  
inductive_cases type_aexpr_elim [elim]: "\<Gamma> \<turnstile>\<^sub>a e \<in> t"

inductive
  type_bexpr :: "(('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> 'BExp \<Rightarrow> 'BExp Type \<Rightarrow> bool" ("_ \<turnstile>\<^sub>b _ \<in> _ " [120, 120, 120] 1000)
where
  type_bexpr [intro!]: "\<Gamma> \<turnstile>\<^sub>b e \<in> \<Union> (image (\<lambda> x. to_total \<Gamma> x) (Var ` bexp_vars e))"

lemma type_bexprI:
  "t =  \<Union> (image (\<lambda> x. to_total \<Gamma> x) (Var ` bexp_vars e)) \<Longrightarrow> \<Gamma> \<turnstile>\<^sub>b e \<in> t"
  apply(erule ssubst)
  apply(rule type_bexpr.intros)
  done

lemma type_bexpr_type_wellformed:
  "types_wellformed \<Gamma> \<Longrightarrow> \<Gamma> \<turnstile>\<^sub>b e \<in> t \<Longrightarrow> type_wellformed t"
  apply(erule type_bexpr.cases)
  apply(erule ssubst, rule Un_type_wellformed)
  apply clarsimp
  apply(blast intro: to_total_type_wellformed)
  done
  
inductive_cases type_bexpr_elim [elim]: "\<Gamma> \<turnstile>\<^sub>b e \<in> t"


text {*
  Define a sufficient condition for a type to be stable, assuming the type is wellformed.
  
  We need this because there is no point tracking the fact that e.g. variable @{term x}'s data has
  a classification that depends on some control variable @{term c} (where @{term c} might be
  the control variable for some other variable @{term y} whose value we've just assigned to
  @{term x}) if @{term c} can then go and be modified, since now the classification of
  the data in @{term x} no longer depends on the value of @{term c}, instead it depends on
  @{term c}'s \emph{old} value, which has now been lost.
  
  Therefore, if a type depends on @{term c}, then @{term c} had better be stable.
*}
abbreviation
  pred_stable :: "('Lock, 'Var) Var Stable \<Rightarrow> 'BExp \<Rightarrow> bool"
where
  "pred_stable \<S> p \<equiv> \<forall>x\<in> Var ` bexp_vars p. stable \<S> x"

abbreviation
  type_stable :: "('Lock, 'Var) Var Stable \<Rightarrow> 'BExp Type \<Rightarrow> bool"
where
  "type_stable \<S> t \<equiv> (\<forall>p\<in>t. pred_stable \<S> p)"
  
lemma type_stable_is_sufficient:
  "\<lbrakk>type_stable \<S> t\<rbrakk> \<Longrightarrow>
  \<forall>mem mem'. (\<forall>x. stable \<S> x \<longrightarrow> mem x = mem' x) \<longrightarrow> (ev\<^sub>B (to_prog_mem mem)) ` t = (ev\<^sub>B (to_prog_mem mem')) ` t"
  apply(clarsimp simp: type_wellformed_def image_def to_prog_mem_def)
  apply safe
   using eval_vars_det\<^sub>B
   apply (metis (mono_tags, lifting))+
done
  
definition mds_consistent :: "('Lock, 'Var) Var Mds \<Rightarrow> (('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> 'BExp preds \<Rightarrow> bool"
  where "mds_consistent mds \<Gamma> \<S> P \<equiv>
    (\<S> = (mds AsmNoWrite, mds AsmNoReadOrWrite)) \<and>
    (dom \<Gamma> = {x. x \<notin> \<C> \<and> stable \<S> x}) \<and>
    (\<forall>p \<in> P. pred_stable \<S> p)"

definition add_anno_dom :: "(('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> 'Lock LockUpd \<Rightarrow> ('Lock, 'Var) Var set"
  where
  "add_anno_dom \<Gamma> \<S> upd \<equiv> case upd of
    Acq l \<Rightarrow> case lock_interp l of (A_W, A_WR) \<Rightarrow> dom \<Gamma> \<union> { v . v \<in> Var ` (A_W \<union> A_WR) \<and> v \<notin> \<C> } |
    Rel l \<Rightarrow> case lock_interp l of (A_W, A_WR) \<Rightarrow> dom \<Gamma> - {v. v \<notin> (fst \<S> - Var ` A_W) \<union> (snd \<S> - Var ` A_WR)}"

(* I think this would be a pretty sane requirement, and it might make some troubles go away.
   Update: it didn't really fix anything (yet) but I still think it's a reasonable requirement. *)
definition
  "no_double_var_locks \<equiv> \<forall> v l. v \<notin> fst (lock_interp l) \<or> v \<notin> snd (lock_interp l)"

definition
  "one_lock_per_var \<equiv> \<forall> v l. v \<in> fst (lock_interp l) \<union> snd (lock_interp l) \<longrightarrow>
                       (\<forall> l'. l \<noteq> l' \<longrightarrow> v \<notin> fst (lock_interp l') \<union> snd (lock_interp l'))"

definition 
  add_anno :: "(('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> 
    ('Lock, 'Var) Var Stable \<Rightarrow> 
    'Lock LockUpd \<Rightarrow> 
    (('Lock, 'Var) Var,'BExp) TyEnv" ("_ \<oplus>\<^sub>_ _" [120, 120, 120] 1000)
where
  "\<Gamma> \<oplus>\<^sub>\<S> upd = restrict_map (\<lambda>x. Some (to_total \<Gamma> x)) (add_anno_dom \<Gamma> \<S> upd)"

definition vars_of_lock :: "'Lock \<Rightarrow> ('Lock, 'Var) Var set"
where
  "vars_of_lock l \<equiv> (Var ` (fst (lock_interp l) \<union> snd (lock_interp l)))"
  
definition add_anno_stable :: "('Lock, 'Var) Var Stable \<Rightarrow> 'Lock LockUpd \<Rightarrow> ('Lock, 'Var) Var Stable"
  where
  "add_anno_stable \<S> upd \<equiv> case upd of
    Acq l \<Rightarrow> case lock_interp l of (A_W, A_WR) \<Rightarrow> (fst \<S> \<union> Var ` A_W, snd \<S> \<union> Var ` A_WR) |
    Rel l \<Rightarrow> case lock_interp l of (A_W, A_WR) \<Rightarrow> (fst \<S> - Var ` A_W, snd \<S> - Var ` A_WR)"

definition pred_entailment :: "'Lock itself \<Rightarrow> 'BExp preds \<Rightarrow> 'BExp preds \<Rightarrow> bool" ("_: _ \<turnstile> _")
where
  "pred_entailment l P P' \<equiv> \<forall>mem. pred P mem \<longrightarrow> pred P' mem"

text {*
  We give a predicate interpretation of subtype and then prove it has the correct
  semantic property.
*}
definition
  subtype :: "'Lock itself \<Rightarrow> 'BExp Type \<Rightarrow> 'BExp preds \<Rightarrow> 'BExp Type \<Rightarrow> bool" ("_: _ \<le>:\<^sub>_ _" [120, 120, 120] 1000)
where
 "l: t \<le>:\<^sub>P t' \<equiv> l: (P \<union> t') \<turnstile> t"
 
definition
  type_max :: "'BExp Type \<Rightarrow> (('Lock, 'Var) Var,'Val) Mem \<Rightarrow> Sec"
where
  "type_max t mem \<equiv> if (\<forall>p\<in>t. ev\<^sub>B (to_prog_mem mem) p) then Low else High"

lemma type_stable_is_sufficient':
  "\<lbrakk>type_stable \<S> t\<rbrakk> \<Longrightarrow>
  \<forall>mem mem'. (\<forall>x. stable \<S> x \<longrightarrow> mem x = mem' x) \<longrightarrow> type_max t mem = type_max t mem'"
  using type_stable_is_sufficient
  unfolding type_max_def image_def to_prog_mem_def
  apply clarsimp
  by (metis (mono_tags, lifting) eval_vars_det\<^sub>B)

lemma subtype_sound:
  "L: t \<le>:\<^sub>P t' \<Longrightarrow> \<forall>mem. pred P mem \<longrightarrow> type_max t mem \<le> type_max t' mem"
  apply(fastforce simp: subtype_def pred_entailment_def pred_def type_max_def less_eq_Sec_def)
  done

lemma subtype_complete:
  assumes a: "\<And>mem. pred P mem \<Longrightarrow> type_max t mem \<le> type_max t' mem"
  shows "L: t \<le>:\<^sub>P t'"
unfolding subtype_def pred_entailment_def
proof (clarify)
  fix mem
  assume p: "pred (P \<union> t') (mem :: ('Lock, 'Var) Var \<Rightarrow> 'Val)"
  hence "pred P mem"
    unfolding pred_def by blast
  with a have tmax: "type_max t mem \<le> type_max t' mem" by blast
  from p have t': "pred t' mem"
    unfolding pred_def by blast
  from t' have "type_max t' mem = Low"
    unfolding type_max_def pred_def by force
  with tmax have "type_max t mem \<le> Low"
    by simp
  hence "type_max t mem = Low"
    unfolding less_eq_Sec_def by blast
  thus "pred t mem"
    unfolding type_max_def pred_def by (auto split: if_splits)
qed

lemma subtype_correct:
  "(L: t \<le>:\<^sub>P t')  = (\<forall>mem. pred P mem \<longrightarrow> type_max t mem \<le> type_max t' mem)"
  apply(rule iffI)
   apply(simp add: subtype_sound)
  apply(simp add: subtype_complete)
  done

definition
  type_equiv :: "'Lock itself \<Rightarrow> 'BExp Type \<Rightarrow> 'BExp preds \<Rightarrow> 'BExp Type \<Rightarrow> bool" ("_: _ =:\<^sub>_ _" [120, 120, 120] 1000)
where
  "l: t =:\<^sub>P t' \<equiv> (l: t \<le>:\<^sub>P t') \<and> (l: t' \<le>:\<^sub>P t)"
  

lemma subtype_refl [simp]:
  "L: t \<le>:\<^sub>P t"
  by(simp add: subtype_def pred_entailment_def pred_def)

lemma type_equiv_refl [simp]:
  "L: t =:\<^sub>P t"
  by (simp add: type_equiv_def)


definition
  anno_type_stable :: "(('Lock, 'Var) Var, 'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> 'Lock LockUpd \<Rightarrow> bool"
where
  "anno_type_stable \<Gamma> \<S> upd \<equiv> 
    case upd of 
      (* you can't let go of an assumption on a control variable if there's an assumption
         on any variable it's controlling. this could happen for instance if you rewrite a type
         to depend on a control variable protected by a lock other than the original control 
         var, something that I don't think will happen in practice. Consider instead imposing
         the restriction that all types can mention only variables under the same lock *)
      Rel l \<Rightarrow> (\<forall>v \<in> Var ` (fst (lock_interp l) \<union> snd (lock_interp l)). 
        (v \<in> \<C>) \<longrightarrow> (\<forall>x \<in> add_anno_dom \<Gamma> \<S> upd. v \<notin> vars_of_type (the (\<Gamma> x)))) |
      Acq l \<Rightarrow> True"

lemma anno_type_stable_trivial [simp]: 
  "anno_type_stable \<Gamma> \<S> (Acq l) = True"
  by (simp add: anno_type_stable_def)

definition
  types_stable :: "(('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> bool"
where
  "types_stable \<Gamma> \<S> \<equiv> \<forall>x\<in>dom \<Gamma>. type_stable \<S> (the (\<Gamma> x))"
  
definition
  tyenv_wellformed :: "('Lock, 'Var) Var Mds \<Rightarrow> (('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> 'BExp preds \<Rightarrow> bool"
where
  "tyenv_wellformed mds \<Gamma> \<S> P \<equiv> 
      mds_consistent mds \<Gamma> \<S> P \<and>
      types_wellformed \<Gamma> \<and> types_stable \<Gamma> \<S>"

lemma subset_entailment:
  "P' \<subseteq> P \<Longrightarrow> L: P \<turnstile> P'"
  apply(auto simp: pred_entailment_def pred_def)
  done

lemma pred_entailment_refl [simp]:
  "L: P \<turnstile> P"
  by(simp add: pred_entailment_def)

lemma pred_entailment_mono:
  "L: P \<turnstile> P' \<Longrightarrow> P \<subseteq> P'' \<Longrightarrow> L: P'' \<turnstile> P'"
  by(auto simp: pred_entailment_def pred_def)
  
lemma type_equiv_subset:
  "type_equiv L t P t' \<Longrightarrow> P \<subseteq> P' \<Longrightarrow> type_equiv L t P' t'"
  apply(auto simp: type_equiv_def subtype_def intro: pred_entailment_mono)
  done
  
definition
  context_equiv :: "(('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> 'BExp preds \<Rightarrow> (('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> bool" ("_ =:\<^sub>_ _" [120, 120, 120] 1000)
where
  "\<Gamma> =:\<^sub>P \<Gamma>' \<equiv> dom \<Gamma> = dom \<Gamma>' \<and>
                           (\<forall>x\<in>dom \<Gamma>'. type_equiv L (the (\<Gamma> x)) P (the (\<Gamma>' x)))"

lemma context_equiv_refl[simp]:
  "context_equiv \<Gamma> P \<Gamma>"
  by(simp add: context_equiv_def)

lemma context_equiv_subset:
  "context_equiv \<Gamma> P \<Gamma>' \<Longrightarrow> P \<subseteq> P' \<Longrightarrow> context_equiv \<Gamma> P' \<Gamma>'"
  apply(auto simp: context_equiv_def intro: type_equiv_subset)
  done

lemma pred_entailment_trans:
  "L: P \<turnstile> P' \<Longrightarrow> L: P' \<turnstile> P'' \<Longrightarrow> L: P \<turnstile> P''"
  by(auto simp: pred_entailment_def)

lemma pred_un [simp]:
  "pred (P \<union> P') mem = (pred P mem \<and> pred P' mem)"
  apply(auto simp: pred_def)
  done
  
lemma pred_entailment_un:
  "L: P \<turnstile> P' \<Longrightarrow> L: P \<turnstile> P'' \<Longrightarrow> L: P \<turnstile> (P' \<union> P'')"
  apply(subst pred_entailment_def)
  apply clarsimp
  apply(fastforce simp: pred_entailment_def)
  done

lemma pred_entailment_mono_un:
  "L: P \<turnstile> P' \<Longrightarrow> L: (P \<union> P'') \<turnstile> (P' \<union> P'')"
  apply(auto simp: pred_entailment_def pred_def)
  done
  
lemma subtype_trans:
  "L: t \<le>:\<^sub>P t' \<Longrightarrow> L: t' \<le>:\<^sub>P' t'' \<Longrightarrow> L: P \<turnstile> P' \<Longrightarrow> L: t \<le>:\<^sub>P t''"
  "L: t \<le>:\<^sub>P' t' \<Longrightarrow> L: t' \<le>:\<^sub>P t'' \<Longrightarrow> L: P \<turnstile> P' \<Longrightarrow> L: t \<le>:\<^sub>P t''"
   apply(clarsimp simp: subtype_def)
   apply(rule pred_entailment_trans)
    prefer 2
    apply assumption
   apply(rule pred_entailment_un)
    apply(blast intro: subset_entailment)
   apply(rule pred_entailment_trans)
    prefer 2
    apply assumption
   apply(blast intro: pred_entailment_mono_un)
  apply(clarsimp simp: subtype_def)
  apply(rule pred_entailment_trans)
   prefer 2
   apply assumption
  apply(rule pred_entailment_un)
   apply(blast intro: pred_entailment_mono)
  apply(blast intro: subset_entailment)
  done
  
lemma type_equiv_trans:
  "type_equiv L t P t' \<Longrightarrow> type_equiv L t' P' t'' 
    \<Longrightarrow> L: P \<turnstile> P' \<Longrightarrow> type_equiv L t P t''"
  apply(auto simp: type_equiv_def intro: subtype_trans)
  done

lemma context_equiv_trans:
  "context_equiv \<Gamma> P \<Gamma>' \<Longrightarrow> context_equiv \<Gamma>' P' \<Gamma>'' \<Longrightarrow> L: P \<turnstile> P' \<Longrightarrow> context_equiv \<Gamma> P \<Gamma>''"
  apply(force simp: context_equiv_def intro: type_equiv_trans)
  done

lemma un_pred_entailment_mono:
  "L: (P \<union> P') \<turnstile> P'' \<Longrightarrow> L: P''' \<turnstile> P \<Longrightarrow> L: (P''' \<union> P') \<turnstile> P''"
  unfolding pred_entailment_def pred_def
  apply blast
  done
  
lemma subtype_entailment:
  "L: t \<le>:\<^sub>P t' \<Longrightarrow> L: P' \<turnstile> P \<Longrightarrow> L: t \<le>:\<^sub>P' t'"
  apply(auto simp: subtype_def intro: un_pred_entailment_mono)
  done
  
lemma type_equiv_entailment:
  "type_equiv L t P t' \<Longrightarrow> L: P' \<turnstile> P \<Longrightarrow> type_equiv L t P' t'"
  apply(auto simp: type_equiv_def intro: subtype_entailment)  
  done
  
lemma context_equiv_entailment:
  "context_equiv \<Gamma> P \<Gamma>' \<Longrightarrow> L: P' \<turnstile> P \<Longrightarrow> context_equiv \<Gamma> P' \<Gamma>'"
  apply(auto simp: context_equiv_def intro: type_equiv_entailment)
  done
  
inductive has_type :: "(('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> 'BExp preds \<Rightarrow> ('Var, 'Lock, 'AExp, 'BExp) Stmt \<Rightarrow> (('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> 'BExp preds \<Rightarrow> bool"
  ("\<turnstile> _,_,_ {_} _,_,_" [120, 120, 120, 120, 120, 120, 120] 1000)
  where
  stop_type [intro]: "\<turnstile> \<Gamma>,\<S>,P {Stop} \<Gamma>,\<S>,P" |
  skip_type [intro] : "\<turnstile> \<Gamma>,\<S>,P {Skip} \<Gamma>,\<S>,P" |
  assign\<^sub>\<C> : 
  "\<lbrakk>
    x = Var x';
    x \<in> \<C>;
    \<Gamma> \<turnstile>\<^sub>a e \<in> t; 
    L: P \<turnstile> t; 
    (\<forall>v\<in>dom \<Gamma>. x \<notin> vars_of_type (the (\<Gamma> v))); 
    P' = restrict_preds_to_vars (assign_post P x' e) {v. stable \<S> v}; 
    (\<forall>v. x \<in> \<C>_vars v \<and> v \<notin> snd \<S> \<longrightarrow> 
      (L: P \<turnstile> (to_total \<Gamma> v)) \<and> 
      L: (to_total \<Gamma> v) \<le>:\<^sub>P' (dma_type v));
    \<Gamma>' = \<Gamma>
   \<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P {x' \<leftarrow> e} \<Gamma>',\<S>,P'" |
  assign\<^sub>1 : 
  "\<lbrakk> x = Var x'; x \<notin> dom \<Gamma> ; x \<notin> \<C>; \<Gamma> \<turnstile>\<^sub>a e \<in> t; L: t \<le>:\<^sub>P (dma_type x);
     P' = restrict_preds_to_vars (assign_post P x' e)  {v. stable \<S> v};
     \<Gamma>' = \<Gamma> \<rbrakk> \<Longrightarrow>
   \<turnstile> \<Gamma>,\<S>,P {x' \<leftarrow> e} \<Gamma>',\<S>,P'" |
  assign\<^sub>2 : 
  "\<lbrakk> x = Var x'; x \<in> dom \<Gamma> ; \<Gamma> \<turnstile>\<^sub>a e \<in> t; type_stable \<S> t;
     P' = restrict_preds_to_vars (assign_post P x' e) {v. stable \<S> v};
     x \<notin> snd \<S> \<longrightarrow> L: t \<le>:\<^sub>P' (dma_type x);
     \<Gamma>' = (\<Gamma> (x := Some t)) \<rbrakk> \<Longrightarrow>
   has_type \<Gamma> \<S> P (x' \<leftarrow> e) \<Gamma>' \<S> P'" |
  if_type [intro]: 
  "\<lbrakk> \<Gamma> \<turnstile>\<^sub>b e \<in> t; L: P \<turnstile> t; 
     \<turnstile> \<Gamma>,\<S>,(P +\<^sub>\<S> e) { c\<^sub>1 } \<Gamma>',\<S>',P'; \<turnstile> \<Gamma>,\<S>,(P +\<^sub>\<S> (bexp_neg e)) { c\<^sub>2 } \<Gamma>'',\<S>',P''; 
     context_equiv \<Gamma>' P' \<Gamma>'''; context_equiv \<Gamma>'' P'' \<Gamma>'''; L: P' \<turnstile> P'''; L: P'' \<turnstile> P'''; 
     \<forall>mds. tyenv_wellformed mds \<Gamma>' \<S>' P' \<longrightarrow> tyenv_wellformed mds \<Gamma>''' \<S>' P'''; 
     \<forall>mds. tyenv_wellformed mds \<Gamma>'' \<S>' P'' \<longrightarrow> tyenv_wellformed mds \<Gamma>''' \<S>' P''' \<rbrakk> \<Longrightarrow> 
   \<turnstile> \<Gamma>,\<S>,P { If e c\<^sub>1 c\<^sub>2 } \<Gamma>''',\<S>',P'''" | 
  while_type [intro]: "\<lbrakk> c \<noteq> Stop; \<Gamma> \<turnstile>\<^sub>b e \<in> t ; L: P \<turnstile> t; \<turnstile> \<Gamma>,\<S>,(P +\<^sub>\<S> e) { c } \<Gamma>,\<S>,P \<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P { While e c } \<Gamma>,\<S>,P" |
  seq_type [intro]: "\<lbrakk> c\<^sub>1 \<noteq> Stop;  \<turnstile> \<Gamma>,\<S>,P { c\<^sub>1 } \<Gamma>',\<S>',P' ; \<turnstile> \<Gamma>',\<S>',P' { c\<^sub>2 } \<Gamma>'',\<S>'',P'' \<rbrakk> \<Longrightarrow>
   \<turnstile> \<Gamma>,\<S>,P { c\<^sub>1 ;; c\<^sub>2 } \<Gamma>'',\<S>'',P''" |
  sub : "\<lbrakk> \<turnstile> \<Gamma>\<^sub>1,\<S>,P\<^sub>1 { c } \<Gamma>\<^sub>1',\<S>',P\<^sub>1' ; context_equiv \<Gamma>\<^sub>2 P\<^sub>2 \<Gamma>\<^sub>1 ; (\<forall>mds. tyenv_wellformed mds \<Gamma>\<^sub>2 \<S> P\<^sub>2 \<longrightarrow> tyenv_wellformed mds \<Gamma>\<^sub>1 \<S> P\<^sub>1);
           (\<forall>mds. tyenv_wellformed mds \<Gamma>\<^sub>1' \<S>' P\<^sub>1' \<longrightarrow> tyenv_wellformed mds \<Gamma>\<^sub>2' \<S>' P\<^sub>2'); context_equiv \<Gamma>\<^sub>1' P\<^sub>1' \<Gamma>\<^sub>2'; L: P\<^sub>2 \<turnstile> P\<^sub>1; L: P\<^sub>1' \<turnstile> P\<^sub>2' \<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>\<^sub>2,\<S>,P\<^sub>2 { c } \<Gamma>\<^sub>2',\<S>',P\<^sub>2'" |
  lock_acq_type [intro]: "\<lbrakk> upd = Acq l; \<Gamma>' = \<Gamma> \<oplus>\<^sub>\<S> upd ; \<S>' = add_anno_stable \<S> upd;
                  P' = restrict_preds_to_vars P {v. stable \<S>' v};
                 (\<And> x. L: (to_total \<Gamma> x) \<le>:\<^sub>P' (to_total \<Gamma>' x));
                 (* Reflect that locks can't manage the modes of other locks *)
                 \<forall>l. Lock l \<notin> dom \<Gamma>
                 \<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P { LockAcq l } \<Gamma>', \<S>',P'" |
  lock_rel_type [intro]: "\<lbrakk> upd = Rel l; \<Gamma>' = \<Gamma> \<oplus>\<^sub>\<S> upd ; \<S>' = add_anno_stable \<S> upd;
              P' = restrict_preds_to_vars P {v. stable \<S>' v};
             (\<And> x. L: (to_total \<Gamma> x) \<le>:\<^sub>P' (to_total \<Gamma>' x));
             anno_type_stable \<Gamma> \<S> upd;
             (* This one is here to capture a result of the one_lock_per_var assumption *)
             fst \<S> \<inter> snd \<S> = {}
             \<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P { LockRel l } \<Gamma>', \<S>',P'"

lemma sub':
  "\<lbrakk> context_equiv \<Gamma>\<^sub>2 P\<^sub>2 \<Gamma>\<^sub>1 ;
    (\<forall>mds. tyenv_wellformed mds \<Gamma>\<^sub>2 \<S> P\<^sub>2 \<longrightarrow> tyenv_wellformed mds \<Gamma>\<^sub>1 \<S> P\<^sub>1);
    (\<forall>mds. tyenv_wellformed mds \<Gamma>\<^sub>1' \<S>' P\<^sub>1' \<longrightarrow> tyenv_wellformed mds \<Gamma>\<^sub>2' \<S>' P\<^sub>2');
    context_equiv \<Gamma>\<^sub>1' P\<^sub>1' \<Gamma>\<^sub>2';
    L: P\<^sub>2 \<turnstile> P\<^sub>1;
    L: P\<^sub>1' \<turnstile> P\<^sub>2';
    \<turnstile> \<Gamma>\<^sub>1,\<S>,P\<^sub>1 { c } \<Gamma>\<^sub>1',\<S>',P\<^sub>1' \<rbrakk> \<Longrightarrow>
  \<turnstile> \<Gamma>\<^sub>2,\<S>,P\<^sub>2 { c } \<Gamma>\<^sub>2',\<S>',P\<^sub>2'"
  by(rule sub)

lemma conc':
  "\<lbrakk> \<turnstile> \<Gamma>\<^sub>1,\<S>,P { c } \<Gamma>',\<S>',P'; 
    \<Gamma>\<^sub>1 = (\<Gamma>\<^sub>2(x \<mapsto> t)); 
    x \<in> dom \<Gamma>\<^sub>2; 
    type_equiv L (the (\<Gamma>\<^sub>2 x)) P t; 
    type_wellformed t; 
    type_stable \<S> t  \<rbrakk> \<Longrightarrow> 
  \<turnstile> \<Gamma>\<^sub>2,\<S>,P { c } \<Gamma>',\<S>',P'"  
  apply(erule sub)
      apply(fastforce simp: context_equiv_def)
     apply(clarsimp simp: tyenv_wellformed_def mds_consistent_def)
     apply(rule conjI)
      apply fastforce
     apply(rule conjI)
      apply(fastforce simp: types_wellformed_def)
     apply(fastforce simp: types_stable_def)
     apply blast
    apply simp+
  done

lemma tyenv_wellformed_subset:
  "tyenv_wellformed mds \<Gamma> \<S> P \<Longrightarrow> P' \<subseteq> P \<Longrightarrow> tyenv_wellformed mds \<Gamma> \<S> P'"
  apply(auto simp: tyenv_wellformed_def mds_consistent_def)
  done

lemma if_type':
  "\<lbrakk> \<Gamma> \<turnstile>\<^sub>b e \<in> t; 
    L: P \<turnstile> t; 
    \<turnstile> \<Gamma>,\<S>,(P +\<^sub>\<S> e) { c\<^sub>1 } \<Gamma>',\<S>',P'; 
    \<turnstile> \<Gamma>,\<S>,(P +\<^sub>\<S> (bexp_neg e)) { c\<^sub>2 } \<Gamma>',\<S>',P''; 
    P''' \<subseteq> P' \<inter> P'' \<rbrakk> \<Longrightarrow> 
  \<turnstile> \<Gamma>,\<S>,P { If e c\<^sub>1 c\<^sub>2 } \<Gamma>',\<S>',P'''"
  apply(erule (3) if_type)
       apply(rule context_equiv_refl)
      apply(rule context_equiv_refl)
     apply(blast intro: subset_entailment)+
   apply(blast intro: tyenv_wellformed_subset)+
  done

lemma skip_type':
  "\<lbrakk>\<Gamma> = \<Gamma>'; \<S> = \<S>'; P = P'\<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P {Skip} \<Gamma>',\<S>',P'"
  using skip_type by simp

lemma stop_type':
  "\<lbrakk>\<Gamma> = \<Gamma>'; \<S> = \<S>'; P = P'\<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P {Stop} \<Gamma>',\<S>',P'"
  using stop_type by simp

subsection {* Typing Soundness *}

text {* The following predicate is needed to exclude some pathological
  cases, that abuse the @{term Stop} command which is not allowed to
  occur in actual programs. *}


inductive_cases has_type_elim: "\<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P'"
inductive_cases has_type_stop_elim: "\<turnstile> \<Gamma>,\<S>,P { Stop } \<Gamma>',\<S>',P'"

definition tyenv_eq :: "(('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> (('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> (('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> bool"
  (infix "=\<index>" 60)
  where "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 \<equiv> \<forall> x. (type_max (to_total \<Gamma> x) mem\<^sub>1 = Low \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x)"

lemma type_max_dma_type [simp]:
  "type_max (dma_type x) mem = dma mem x"
  using dma_correct unfolding type_max_def apply auto
  done

  
text {*
  This result followed trivially for Mantel et al., but we need to know that the
  type environment is wellformed.
*}
lemma tyenv_eq_sym': 
  "dom \<Gamma> \<inter> \<C> = {} \<Longrightarrow> types_wellformed \<Gamma> \<Longrightarrow> mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 \<Longrightarrow> mem\<^sub>2 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>1"
proof(clarsimp simp: tyenv_eq_def)
  fix x
  assume a: "\<forall>x. type_max (to_total \<Gamma> x) mem\<^sub>1 = Low \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x"
  assume b: "dom \<Gamma> \<inter> \<C> = {}"
  from a b have eq_\<C>: "\<forall>x\<in>\<C>. mem\<^sub>1 x = mem\<^sub>2 x"
    by (fastforce simp: to_total_def \<C>_Low type_max_dma_type split: if_splits)
  hence "dma mem\<^sub>1 = dma mem\<^sub>2"
    by (rule dma_\<C>)
  hence dma_type_eq: "type_max (dma_type x) mem\<^sub>1 = type_max (dma_type x) mem\<^sub>2"
    by(simp)
  assume c: "types_wellformed \<Gamma>"
  
  assume d: "type_max (to_total \<Gamma> x) mem\<^sub>2 = Low"
  show "mem\<^sub>2 x = mem\<^sub>1 x"
  proof(cases "x \<in> dom \<Gamma>")
    assume in_dom: "x \<in> dom \<Gamma>"
    from this obtain t where t: "\<Gamma> x = Some t" by blast
    from this in_dom c have "type_wellformed t" by (force simp: types_wellformed_def)
    hence "\<forall>x\<in> vars_of_type t. mem\<^sub>1 x = mem\<^sub>2 x"
      using eq_\<C> unfolding type_wellformed_def by blast
    hence t_eq: "type_max t mem\<^sub>1 = type_max t mem\<^sub>2" 
      unfolding type_max_def using eval_vars_det\<^sub>B to_prog_mem_def
      by (metis (full_types) UN_I image_eqI)
    with in_dom t have "to_total \<Gamma> x = t"
      by (auto simp: to_total_def)
    with t_eq have "type_max (to_total \<Gamma> x) mem\<^sub>2 = type_max (to_total \<Gamma> x) mem\<^sub>1" by simp
    with d have "type_max (to_total \<Gamma> x) mem\<^sub>1 = Low" by simp
    with a show ?thesis by (metis sym)
  next
    assume "x \<notin> dom \<Gamma>"
    hence "to_total \<Gamma> x = dma_type x"
      by (auto simp: to_total_def)
    with dma_type_eq have "type_max (to_total \<Gamma> x) mem\<^sub>2 = type_max (to_total \<Gamma> x) mem\<^sub>1" by simp
    with d have "type_max (to_total \<Gamma> x) mem\<^sub>1 = Low" by simp
    with a show ?thesis by (metis sym)
  qed
qed

lemma tyenv_eq_sym:
  "tyenv_wellformed mds \<Gamma> \<S> P \<Longrightarrow> mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 \<Longrightarrow> mem\<^sub>2 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>1"
  apply(rule tyenv_eq_sym')
    apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
   apply(simp add: tyenv_wellformed_def)
  by assumption
  
inductive_set \<R>\<^sub>1 :: "(('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> 'BExp preds \<Rightarrow> (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf rel"
  and \<R>\<^sub>1_abv :: "
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  (('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> 'BExp preds \<Rightarrow>
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  bool" ("_ \<R>\<^sup>1\<^bsub>_,_,_\<^esub> _" [120, 120, 120, 120, 120] 1000)
  for \<Gamma>' :: "(('Lock, 'Var) Var,'BExp) TyEnv"
  and \<S>' :: "('Lock, 'Var) Var Stable"
  and P' :: "'BExp preds"
  where
  "x \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> y \<equiv> (x, y) \<in> \<R>\<^sub>1 \<Gamma> \<S> P" |
  intro [intro!] : "\<lbrakk> \<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P' ; tyenv_wellformed mds \<Gamma> \<S> P ; mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2;
                      pred P mem\<^sub>1; pred P mem\<^sub>2; \<forall>x\<in>dom \<Gamma>. x\<notin>mds AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) mem\<^sub>1 \<le> dma mem\<^sub>1 x\<rbrakk> \<Longrightarrow> 
                    \<langle>c, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c, mds, mem\<^sub>2\<rangle>"

inductive \<R>\<^sub>3_aux :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
                 (('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> 'BExp preds \<Rightarrow> (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
                 bool" ("_ \<R>\<^sup>3\<^bsub>_,_,_\<^esub> _" [120, 120, 120, 120, 120] 1000)
  and \<R>\<^sub>3 :: "(('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> 'BExp preds \<Rightarrow> (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf rel"
  where
  "\<R>\<^sub>3 \<Gamma>' \<S>' P' \<equiv> {(lc\<^sub>1, lc\<^sub>2). \<R>\<^sub>3_aux lc\<^sub>1 \<Gamma>' \<S>' P' lc\<^sub>2}" |
  intro\<^sub>1 [intro] : "\<lbrakk> \<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>; \<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P' \<rbrakk> \<Longrightarrow>
                      \<langle>Seq c\<^sub>1 c, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>Seq c\<^sub>2 c, mds, mem\<^sub>2\<rangle>" |
  intro\<^sub>3 [intro] : "\<lbrakk> \<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>; \<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P' \<rbrakk> \<Longrightarrow>
                      \<langle>Seq c\<^sub>1 c, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>Seq c\<^sub>2 c, mds, mem\<^sub>2\<rangle>"

(* A weaker property than bisimulation to reason about the sub-relations of \<R>: *)
definition weak_bisim :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf rel \<Rightarrow>
                        (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf rel \<Rightarrow> bool"
  where "weak_bisim \<T>\<^sub>1 \<T> \<equiv> \<forall> c\<^sub>1 c\<^sub>2 mds mem\<^sub>1 mem\<^sub>2 c\<^sub>1' mds' mem\<^sub>1'.
  ((\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>) \<in> \<T>\<^sub>1 \<and>
   (\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>)) \<longrightarrow>
  (\<exists> c\<^sub>2' mem\<^sub>2'. \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle> \<and> 
                (\<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>, \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>) \<in> \<T>)"

inductive_set \<R> :: "(('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> 'BExp preds \<Rightarrow>
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf rel"
  and \<R>_abv :: "
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  (('Lock, 'Var) Var, 'BExp) TyEnv \<Rightarrow> ('Lock, 'Var) Var Stable \<Rightarrow> 'BExp preds \<Rightarrow>
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  bool" ("_ \<R>\<^sup>u\<^bsub>_,_,_\<^esub> _" [120, 120, 120, 120, 120] 1000)
  for \<Gamma> :: "(('Lock, 'Var) Var,'BExp) TyEnv"
  and \<S> :: "('Lock, 'Var) Var Stable"
  and P :: "'BExp preds"
  where
  "x \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> y \<equiv> (x, y) \<in> \<R> \<Gamma> \<S> P" |
  intro\<^sub>1: "lc \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> lc' \<Longrightarrow> (lc, lc') \<in> \<R> \<Gamma> \<S> P" |
  intro\<^sub>3: "lc \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> lc' \<Longrightarrow> (lc, lc') \<in> \<R> \<Gamma> \<S> P"

(* Some eliminators for the above relations *)
inductive_cases \<R>\<^sub>1_elim [elim]: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>"
inductive_cases \<R>\<^sub>3_elim [elim]: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>"

inductive_cases \<R>_elim [elim]: "(\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>) \<in> \<R> \<Gamma> \<S> P"
inductive_cases \<R>_elim': "(\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2\<rangle>) \<in> \<R> \<Gamma> \<S> P"
inductive_cases \<R>\<^sub>1_elim' : "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2\<rangle>"
inductive_cases \<R>\<^sub>3_elim' : "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2\<rangle>"

lemma \<R>\<^sub>1_mem_eq: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<Longrightarrow> mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>l mem\<^sub>2"
proof (erule \<R>\<^sub>1_elim)
  fix \<Gamma> \<S> P
  assume wf: "tyenv_wellformed mds \<Gamma> \<S> P"
  hence mds_consistent: "mds_consistent mds \<Gamma> \<S> P"
    unfolding tyenv_wellformed_def by blast
  assume tyenv_eq: "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assume leq: "\<forall>x\<in>dom \<Gamma>. x \<notin> mds AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) mem\<^sub>1 \<le> dma mem\<^sub>1 x"
  assume pred: "pred P mem\<^sub>1"
  
  show "mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>l mem\<^sub>2"
  unfolding low_mds_eq_def
  proof(clarify)
    fix x
    assume is_Low: "dma mem\<^sub>1 x = Low"
    assume is_readable: "x \<in> \<C> \<or> x \<notin> mds AsmNoReadOrWrite"
    show "mem\<^sub>1 x = mem\<^sub>2 x"
    proof(cases "x \<in> dom \<Gamma>")
      assume in_dom: "x \<in> dom \<Gamma>"
      with mds_consistent have "x \<notin> \<C>"
        unfolding mds_consistent_def by blast
      with is_readable have "x \<notin> mds AsmNoReadOrWrite"
        by blast
       
      with in_dom leq  have "type_max (to_total \<Gamma> x) mem\<^sub>1 \<le> dma mem\<^sub>1 x"
        unfolding to_total_def
        by auto
      with is_Low have "type_max (to_total \<Gamma> x) mem\<^sub>1 = Low"
        by(simp add: less_eq_Sec_def)
      with tyenv_eq show ?thesis
        unfolding tyenv_eq_def by blast
    next         
      assume nin_dom: "x \<notin> dom \<Gamma>"
      with is_Low have "type_max (to_total \<Gamma> x) mem\<^sub>1 = Low"
        unfolding to_total_def
        by simp
      with tyenv_eq show ?thesis
        unfolding tyenv_eq_def by blast
    qed
  qed
qed

lemma \<R>\<^sub>1_dma_eq:
  "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<Longrightarrow> dma mem\<^sub>1 = dma mem\<^sub>2"
  apply(drule \<R>\<^sub>1_mem_eq)
  apply(erule low_mds_eq_dma)
  done


(* \<R> meets the criteria of a "simple bisim"
   which can be used to simplify the establishment of a refinement relation *)
lemma bisim_simple_\<R>\<^sub>1:
  "\<langle>c, mds, mem\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c', mds', mem'\<rangle> \<Longrightarrow> c = c'"
  apply(cases rule: \<R>\<^sub>1.cases, simp+)
  done

lemma bisim_simple_\<R>\<^sub>3:
  "lc \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> lc' \<Longrightarrow> (fst (fst lc)) = (fst (fst lc'))"
  apply(induct rule: \<R>\<^sub>3_aux.induct)
  using bisim_simple_\<R>\<^sub>1 apply clarsimp
  apply simp
  done

lemma bisim_simple_\<R>\<^sub>u:
  "lc \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> lc' \<Longrightarrow> (fst (fst lc)) = (fst (fst lc'))"
  apply(induct rule: \<R>.induct)
   apply clarsimp
   apply(cases rule: \<R>\<^sub>1.cases, simp+)
  apply(cases rule: \<R>\<^sub>3_aux.cases, simp+)
   apply blast
  using bisim_simple_\<R>\<^sub>3 apply clarsimp
  done


(* To prove that \<R> is a bisimulation, we first show symmetry *)

lemma \<C>_eq_type_max_eq:
  assumes wf: "type_wellformed t"
  assumes \<C>_eq: "\<forall>x\<in>\<C>. mem\<^sub>1 x = mem\<^sub>2 x" 
  shows "type_max t mem\<^sub>1 = type_max t mem\<^sub>2"
proof -
  have "\<forall>x\<in>vars_of_type t. mem\<^sub>1 x = mem\<^sub>2 x"
    using wf \<C>_eq unfolding type_wellformed_def by blast
  thus ?thesis
    unfolding type_max_def using eval_vars_det\<^sub>B
    apply clarsimp
    by (metis (no_types, hide_lams) to_prog_mem_def)
qed

lemma vars_of_type_eq_type_max_eq:
  assumes mem_eq: "\<forall>x\<in>vars_of_type t. mem\<^sub>1 x = mem\<^sub>2 x" 
  shows "type_max t mem\<^sub>1 = type_max t mem\<^sub>2"
proof -
  from assms show ?thesis
    unfolding type_max_def using eval_vars_det\<^sub>B
    apply clarsimp
    by (metis (no_types, hide_lams) to_prog_mem_def)
qed

lemma \<R>\<^sub>1_sym: "sym (\<R>\<^sub>1 \<Gamma>' \<S>' P')"
unfolding sym_def
proof clarsimp
  fix c mds mem c' mds' mem'
  assume in_\<R>\<^sub>1: "\<langle>c, mds, mem\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c', mds', mem'\<rangle>"
  then obtain \<Gamma> \<S> P where
  stuff: "c' = c"  "mds' = mds"  "\<turnstile> \<Gamma>,\<S>,P {c} \<Gamma>',\<S>',P'"  "tyenv_wellformed mds \<Gamma> \<S> P"
  "mem =\<^bsub>\<Gamma>\<^esub> mem'" "pred P mem" "pred P mem'"
  "\<forall>x\<in>dom \<Gamma>. x \<notin> mds AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) mem \<le> dma mem x"
    using \<R>\<^sub>1_elim' by blast+
  from stuff have stuff': "mem' =\<^bsub>\<Gamma>\<^esub> mem"
    by (metis tyenv_eq_sym)
  
  have "\<forall>x\<in>dom \<Gamma>. x \<notin> mds AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) mem' \<le> dma mem' x"
  proof -
    from in_\<R>\<^sub>1 have "dma mem = dma mem'"
      using \<R>\<^sub>1_dma_eq stuff by metis
    moreover have "\<forall>x\<in>dom \<Gamma>. type_max (the (\<Gamma> x)) mem = type_max (the (\<Gamma> x)) mem'"
    proof
      fix x
      assume "x \<in> dom \<Gamma>"
      hence "type_wellformed (the (\<Gamma> x))"
        using `tyenv_wellformed mds \<Gamma> \<S> P`
        by(auto simp: tyenv_wellformed_def types_wellformed_def)
      moreover have "\<forall>x\<in>\<C>. mem x = mem' x"
        using in_\<R>\<^sub>1 \<R>\<^sub>1_mem_eq \<C>_Low stuff
        unfolding low_mds_eq_def by auto
      ultimately
      show "type_max (the (\<Gamma> x)) mem = type_max (the (\<Gamma> x)) mem'"
        using \<C>_eq_type_max_eq by blast
    qed
    ultimately show ?thesis
      using stuff(8) by fastforce
  qed
  with stuff stuff'
  show "\<langle>c', mds', mem'\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c, mds, mem\<rangle>"
  by (metis (no_types) \<R>\<^sub>1.intro)
qed

lemma \<R>\<^sub>3_sym: "sym (\<R>\<^sub>3 \<Gamma> \<S> P)"
  unfolding sym_def
proof (clarify)
  fix c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mds' mem\<^sub>2
  assume asm: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds', mem\<^sub>2\<rangle>"
  hence [simp]: "mds' = mds"
    using \<R>\<^sub>3_elim' by blast
  from asm show "\<langle>c\<^sub>2, mds', mem\<^sub>2\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>"
    apply auto
    apply (induct rule: \<R>\<^sub>3_aux.induct)
     apply (metis (lifting) \<R>\<^sub>1_sym \<R>\<^sub>3_aux.intro\<^sub>1 symD)
    by (metis (lifting) \<R>\<^sub>3_aux.intro\<^sub>3)
qed

lemma \<R>_mds [simp]: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds', mem\<^sub>2\<rangle> \<Longrightarrow> mds = mds'"
  apply (rule \<R>_elim')
     apply (auto)
   apply (metis \<R>\<^sub>1_elim')
  apply (insert \<R>\<^sub>3_elim')
  by blast

lemma \<R>_sym: "sym (\<R> \<Gamma> \<S> P)"
  unfolding sym_def
proof (clarify)
  fix c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mds\<^sub>2 mem\<^sub>2
  assume asm: "(\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2\<rangle>) \<in> \<R> \<Gamma> \<S> P"
  with \<R>_mds have [simp]: "mds\<^sub>2 = mds"
    by blast
  from asm show "(\<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2\<rangle>, \<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>) \<in> \<R> \<Gamma> \<S> P"
    using \<R>.intro\<^sub>1 [of \<Gamma> \<S> P] and \<R>.intro\<^sub>3 [of _ \<Gamma> \<S> P]
    using \<R>\<^sub>1_sym [of \<Gamma>] and \<R>\<^sub>3_sym [of \<Gamma>]
    apply simp
    apply (erule \<R>_elim)
      by (auto simp: sym_def)
qed

(* Next, we show that the relations are closed under globally consistent changes *)

lemma \<R>\<^sub>1_closed_glob_consistent: "closed_glob_consistent (\<R>\<^sub>1 \<Gamma>' \<S>' P')"
  unfolding closed_glob_consistent_def
proof (clarify)
  fix c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mem\<^sub>2 A
  assume R1: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>"
  hence [simp]: "c\<^sub>2 = c\<^sub>1" by blast
  assume A_updates_vars: "\<forall>x. case A x of None \<Rightarrow> True | Some (v, v') \<Rightarrow> mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> v' \<longrightarrow> \<not> var_asm_not_written mds x"
  assume A_updates_dma: "\<forall>x. dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x \<noteq> dma mem\<^sub>1 x \<longrightarrow> \<not> var_asm_not_written mds x"
  assume A_updates_sec: "\<forall>x. dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x = Low \<and> (x \<notin> mds AsmNoReadOrWrite \<or> x \<in> \<C>) \<longrightarrow> mem\<^sub>1 [\<parallel>\<^sub>1 A] x = mem\<^sub>2 [\<parallel>\<^sub>2 A] x"
  from R1 obtain \<Gamma> \<S> P where \<Gamma>_props: "\<turnstile> \<Gamma>,\<S>,P { c\<^sub>1 } \<Gamma>',\<S>',P'" "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2" "tyenv_wellformed mds \<Gamma> \<S> P"
                                      "pred P mem\<^sub>1" "pred P mem\<^sub>2"
                                      "\<forall>x\<in>dom \<Gamma>. x \<notin> mds AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) mem\<^sub>1 \<le> dma mem\<^sub>1 x"
    by force

  from \<Gamma>_props(3) have stable_not_written: "\<forall>x. stable \<S> x \<longrightarrow> var_asm_not_written mds x"
    by(auto simp: tyenv_wellformed_def mds_consistent_def stable_def var_asm_not_written_def)
  with A_updates_vars have stable_unchanged\<^sub>1: "\<forall>x. stable \<S> x \<longrightarrow> (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x = mem\<^sub>1 x" and
                           stable_unchanged\<^sub>2: "\<forall>x. stable \<S> x \<longrightarrow> (mem\<^sub>2 [\<parallel>\<^sub>2 A]) x = mem\<^sub>2 x"
    by(auto simp: apply_adaptation_def split: option.splits)

  from stable_not_written A_updates_dma 
  have stable_unchanged_dma\<^sub>1: "\<forall>x. stable \<S> x \<longrightarrow> dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x = dma mem\<^sub>1 x"
    by(blast)

  have tyenv_eq': "mem\<^sub>1 [\<parallel>\<^sub>1 A] =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 [\<parallel>\<^sub>2 A]"
  proof(clarsimp simp: tyenv_eq_def)
    fix x
    assume a: "type_max (to_total \<Gamma> x) mem\<^sub>1 [\<parallel>\<^sub>1 A] = Low"
    show "mem\<^sub>1 [\<parallel>\<^sub>1 A] x = mem\<^sub>2 [\<parallel>\<^sub>2 A] x"
    proof(cases "x \<in> dom \<Gamma>")
      assume in_dom: "x \<in> dom \<Gamma>"
      with \<Gamma>_props(3) have "var_asm_not_written mds x"
        by(auto simp: tyenv_wellformed_def mds_consistent_def var_asm_not_written_def stable_def)
      hence [simp]: "mem\<^sub>1 [\<parallel>\<^sub>1 A] x = mem\<^sub>1 x" and [simp]: "mem\<^sub>2 [\<parallel>\<^sub>2 A] x = mem\<^sub>2 x"
        using A_updates_vars by(auto simp: apply_adaptation_def split: option.splits)
      from in_dom a obtain t\<^sub>x where \<Gamma>\<^sub>x: "\<Gamma> x = Some t\<^sub>x" and t\<^sub>x_Low': "type_max t\<^sub>x (mem\<^sub>1 [\<parallel>\<^sub>1 A]) = Low"
        by(auto simp: to_total_def)
      have t\<^sub>x_unchanged: "type_max t\<^sub>x (mem\<^sub>1 [\<parallel>\<^sub>1 A])  = type_max t\<^sub>x mem\<^sub>1"
      proof - 
        from \<Gamma>\<^sub>x \<Gamma>_props(3) have t\<^sub>x_stable: "type_stable \<S> t\<^sub>x" and t\<^sub>x_wellformed: "type_wellformed t\<^sub>x"
          by(force simp: tyenv_wellformed_def types_stable_def types_wellformed_def)+
        from t\<^sub>x_stable t\<^sub>x_wellformed stable_unchanged\<^sub>1 show ?thesis
          using type_stable_is_sufficient'
          by blast
      qed
      with t\<^sub>x_Low' have t\<^sub>x_Low: "type_max t\<^sub>x mem\<^sub>1 = Low" by simp
      with \<Gamma>\<^sub>x \<Gamma>_props(2) have "mem\<^sub>1 x = mem\<^sub>2 x"
        by(force simp: tyenv_eq_def to_total_def split: if_splits)
      thus ?thesis by simp
    next
      assume nin_dom: "x \<notin> dom \<Gamma>"
      with a have is_Low': "dma (mem\<^sub>1[\<parallel>\<^sub>1 A]) x = Low"
        by(simp add: to_total_def)
      show ?thesis
      proof(cases "x \<notin> mds AsmNoReadOrWrite \<or> x \<in> \<C>")
        assume "x \<notin> mds AsmNoReadOrWrite \<or> x \<in> \<C>"
        with is_Low' show ?thesis
          using A_updates_sec by blast
      next
        assume "\<not> (x \<notin> mds AsmNoReadOrWrite \<or> x \<in> \<C>)"
        hence "x \<in> mds AsmNoReadOrWrite" and "x \<notin> \<C>"
          by auto
        with nin_dom \<Gamma>_props(3) have "False"
          by(auto simp: tyenv_wellformed_def mds_consistent_def stable_def)
        thus ?thesis by blast
      qed
    qed
  qed
  
  have sec': "\<forall>x\<in>dom \<Gamma>. x \<notin> mds AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) (mem\<^sub>1 [\<parallel>\<^sub>1 A]) \<le> dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x"
  proof(intro ballI impI)
    fix x
    assume readable: "x \<notin> mds AsmNoReadOrWrite"
    assume in_dom: "x \<in> dom \<Gamma>"
    with \<Gamma>_props(3) have "var_asm_not_written mds x"
      by(auto simp: tyenv_wellformed_def mds_consistent_def var_asm_not_written_def stable_def)
    hence [simp]: "dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x = dma mem\<^sub>1 x"
      using A_updates_dma by(auto simp: apply_adaptation_def split: option.splits)
    from in_dom obtain t\<^sub>x where \<Gamma>\<^sub>x: "\<Gamma> x = Some t\<^sub>x"
      by(auto simp: to_total_def)
    have t\<^sub>x_unchanged: "type_max t\<^sub>x (mem\<^sub>1 [\<parallel>\<^sub>1 A])  = type_max t\<^sub>x mem\<^sub>1"
    proof - 
      from \<Gamma>\<^sub>x \<Gamma>_props(3) have t\<^sub>x_stable: "type_stable \<S> t\<^sub>x" and t\<^sub>x_wellformed: "type_wellformed t\<^sub>x"
        by(force simp: tyenv_wellformed_def types_stable_def types_wellformed_def)+
      from t\<^sub>x_stable t\<^sub>x_wellformed stable_unchanged\<^sub>1 show ?thesis
        using type_stable_is_sufficient'
        by blast
    qed
    with \<Gamma>\<^sub>x have [simp]:"type_max (the (\<Gamma> x)) (mem\<^sub>1 [\<parallel>\<^sub>1 A]) = type_max (the (\<Gamma> x)) mem\<^sub>1"
      by simp
    show "type_max (the (\<Gamma> x)) mem\<^sub>1 [\<parallel>\<^sub>1 A] \<le> dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x "
      apply simp
      using in_dom readable \<Gamma>_props by metis
  qed
    
  from stable_unchanged\<^sub>1 stable_unchanged\<^sub>2 \<Gamma>_props(3) have "\<forall>p\<in>P. ev\<^sub>B (to_prog_mem (mem\<^sub>1 [\<parallel>\<^sub>1 A])) p = ev\<^sub>B (to_prog_mem mem\<^sub>1) p \<and> 
        ev\<^sub>B (to_prog_mem (mem\<^sub>2 [\<parallel>\<^sub>2 A])) p = ev\<^sub>B (to_prog_mem mem\<^sub>2) p"
    apply(intro ballI)
    apply(rule conjI)
    by(rule eval_vars_det\<^sub>B,force simp: tyenv_wellformed_def mds_consistent_def to_prog_mem_def stable_def)+
    
  hence "pred P (mem\<^sub>1 [\<parallel>\<^sub>1 A]) = pred P mem\<^sub>1" and
        "pred P (mem\<^sub>2 [\<parallel>\<^sub>2 A]) = pred P mem\<^sub>2"
    by(simp add: pred_def)+
  
  with \<Gamma>_props tyenv_eq' sec'
  show "\<langle>c\<^sub>1, mds, mem\<^sub>1 [\<parallel>\<^sub>1 A]\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2 [\<parallel>\<^sub>2 A]\<rangle>"
    by auto
qed


lemma \<R>\<^sub>3_closed_glob_consistent:
  "closed_glob_consistent (\<R>\<^sub>3 \<Gamma>' \<S>' P')"
unfolding closed_glob_consistent_def
proof(clarsimp)
  fix c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mem\<^sub>2 A
  assume in_\<R>\<^sub>3: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>"
  assume A_modifies_vars: "\<forall>x. case A x of None \<Rightarrow> True | Some (v, v') \<Rightarrow> mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> v' \<longrightarrow> \<not> var_asm_not_written mds x"
  assume A_modifies_dma: "\<forall>x. dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x \<noteq> dma mem\<^sub>1 x \<longrightarrow> \<not> var_asm_not_written mds x"
  assume A_modifies_sec: "\<forall>x. dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x = Low \<and> (x \<in> mds AsmNoReadOrWrite \<longrightarrow> x \<in> \<C>) \<longrightarrow> mem\<^sub>1 [\<parallel>\<^sub>1 A] x = mem\<^sub>2 [\<parallel>\<^sub>2 A] x"
  (* do a bit of massaging to get the goal state set-up nicely for the induction rule *)
  define lc\<^sub>1 where "lc\<^sub>1 \<equiv> \<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>"
  define lc\<^sub>2 where "lc\<^sub>2 \<equiv> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>"
  from lc\<^sub>1_def lc\<^sub>2_def in_\<R>\<^sub>3 have "lc\<^sub>1 \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> lc\<^sub>2" by simp
  from this lc\<^sub>1_def lc\<^sub>2_def A_modifies_vars A_modifies_dma A_modifies_sec
  show "\<langle>c\<^sub>1, mds, mem\<^sub>1 [\<parallel>\<^sub>1 A]\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2 [\<parallel>\<^sub>2 A]\<rangle>"
proof(induct arbitrary: c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mds mem\<^sub>2 rule: \<R>\<^sub>3_aux.induct)
  case (intro\<^sub>1 c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mem\<^sub>2 c \<Gamma>' \<S>' P')
  show ?case
    apply (rule \<R>\<^sub>3_aux.intro\<^sub>1[OF _ intro\<^sub>1(2)])
    using \<R>\<^sub>1_closed_glob_consistent intro\<^sub>1
    unfolding closed_glob_consistent_def by blast
next
  case (intro\<^sub>3 c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mem\<^sub>2 c \<Gamma>' \<S>' P')
  show ?case
    apply(rule \<R>\<^sub>3_aux.intro\<^sub>3)
     apply(rule intro\<^sub>3(2))
     using intro\<^sub>3 apply simp+
     done
  qed
qed


lemma \<R>_closed_glob_consistent: "closed_glob_consistent (\<R> \<Gamma>' \<S>' P')"
  unfolding closed_glob_consistent_def
proof (clarify, erule \<R>_elim, simp_all)
  fix c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mem\<^sub>2 A
  assume R1: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>"
  and A_modifies_vars: "\<forall>x. case A x of None \<Rightarrow> True | Some (v, v') \<Rightarrow> mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> v' \<longrightarrow> \<not> var_asm_not_written mds x"
  and A_modifies_dma: "\<forall>x. dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x \<noteq> dma mem\<^sub>1 x \<longrightarrow> \<not> var_asm_not_written mds x"
  and A_modifies_sec: "\<forall>x. dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x = Low \<and> (x \<in> mds AsmNoReadOrWrite \<longrightarrow> x \<in> \<C>) \<longrightarrow> mem\<^sub>1 [\<parallel>\<^sub>1 A] x = mem\<^sub>2 [\<parallel>\<^sub>2 A] x"  
  show
    "\<langle>c\<^sub>1, mds, mem\<^sub>1 [\<parallel>\<^sub>1 A]\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2 [\<parallel>\<^sub>2 A]\<rangle>"
    apply(rule intro\<^sub>1)
    apply clarify
    using \<R>\<^sub>1_closed_glob_consistent unfolding closed_glob_consistent_def
    using R1 A_modifies_vars A_modifies_dma A_modifies_sec
    by blast
next
  fix c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mem\<^sub>2 x A
  assume R3: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>"
  and A_modifies_vars: "\<forall>x. case A x of None \<Rightarrow> True | Some (v, v') \<Rightarrow> mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> v' \<longrightarrow> \<not> var_asm_not_written mds x"
  and A_modifies_dma: "\<forall>x. dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x \<noteq> dma mem\<^sub>1 x \<longrightarrow> \<not> var_asm_not_written mds x"
  and A_modifies_sec: "\<forall>x. dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x = Low \<and> (x \<in> mds AsmNoReadOrWrite \<longrightarrow> x \<in> \<C>) \<longrightarrow> mem\<^sub>1 [\<parallel>\<^sub>1 A] x = mem\<^sub>2 [\<parallel>\<^sub>2 A] x"
  show "\<langle>c\<^sub>1, mds, mem\<^sub>1 [\<parallel>\<^sub>1 A]\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2 [\<parallel>\<^sub>2 A]\<rangle> "
    apply(rule intro\<^sub>3)
    using \<R>\<^sub>3_closed_glob_consistent unfolding closed_glob_consistent_def
    using R3 A_modifies_vars A_modifies_dma A_modifies_sec
    by blast
qed

(* It now remains to show that steps of the first of two related configurations
    can be matched by the second: *)

lemma types_wellformed_mode_update:
  "types_wellformed \<Gamma> \<Longrightarrow>
   types_wellformed (\<Gamma> \<oplus>\<^sub>\<S> upd)"
  unfolding types_wellformed_def add_anno_def to_total_def restrict_map_def
  by (force split: if_splits)

(* Adaptation of technical lemma *)
(* This is saying that if the mode state is consistent with the type,
   then after updating both of them as required by "upd", they remain consistent *)
lemma mode_update_add_anno:
  "mds_consistent mds \<Gamma> \<S> P \<Longrightarrow> 
   mds_consistent (update_modes upd mds) 
                  (\<Gamma> \<oplus>\<^sub>\<S> upd) 
                  (add_anno_stable \<S> upd) 
                  (P |`' {v. stable (add_anno_stable \<S> upd) v})"
  apply(case_tac upd)
   apply(clarsimp simp:update_modes_def lock_acq_upd_def add_anno_stable_def mds_consistent_def
                       add_anno_def stable_def restrict_preds_to_vars_def add_anno_dom_def
                  split:prod.splits)
   apply blast
  apply(rename_tac l)
  (* One subgoal at a time *)
  apply(clarsimp simp:mds_consistent_def)
  apply(rule conjI)
   apply(clarsimp simp:update_modes_def lock_rel_upd_def add_anno_stable_def
                  split:prod.splits)
  apply(rule conjI)
   (* One definition at a time *)
   apply(clarsimp simp: add_anno_def)
   apply(clarsimp simp: add_anno_dom_def)
   apply(clarsimp split:prod.splits)
   apply(rename_tac NoW NoRW)
   apply(clarsimp simp: stable_def)
   apply(clarsimp simp: add_anno_stable_def)
   apply(rule set_eqI)
   apply clarsimp
   apply safe
  apply(clarsimp simp:stable_def add_anno_stable_def restrict_preds_to_vars_def Ball_Collect[symmetric]
                 split:prod.splits)
  done


lemma types_mention_only_control_vars:
  assumes a1: "p \<in> t"
  assumes a2: "types_wellformed \<Gamma>"
  assumes a3: "v \<in> bexp_vars p"
  assumes a4: "\<Gamma> x = Some t"
  shows "Var v \<in> \<C>"
proof -
  from assms have "x \<in> dom \<Gamma>"
    by blast
  then show "Var v \<in> \<C>"
    using a4 a3 a2 a1 by (metis Union_iff image_eqI option.sel subsetCE type_wellformed_def types_wellformed_def)
qed

fun
  lock_of :: "'Lock LockUpd \<Rightarrow> 'Lock"
where
  "lock_of (Acq l) = l" |
  "lock_of (Rel l) = l"

lemma types_stable_mode_update:
   "types_stable \<Gamma> \<S> \<Longrightarrow> types_wellformed \<Gamma> \<Longrightarrow> anno_type_stable \<Gamma> \<S> upd \<Longrightarrow>
    types_stable (\<Gamma> \<oplus>\<^sub>\<S> upd) (add_anno_stable \<S> upd)"
proof -
  assume "types_stable \<Gamma> \<S>"
         "types_wellformed \<Gamma>"
         "anno_type_stable \<Gamma> \<S> upd"
  define l where "l \<equiv> lock_of upd"
  obtain NoW NoRW where interp [simp]: "lock_interp l = (NoW,NoRW)" using prod.exhaust by metis
  define l_footprint where "l_footprint \<equiv> (Var ` (NoW \<union> NoRW))::('Lock, 'Var) Var set"
  with lock_interp_\<C>_vars\<^sub>V have
  "\<And>x c. Var c \<in> \<C>_vars (Var x) \<Longrightarrow>
     (c \<in> fst (lock_interp l) \<union> snd (lock_interp l)) = (x \<in> fst (lock_interp l) \<union> snd (lock_interp l))"
    by blast
  show "types_stable \<Gamma> \<oplus>\<^sub>\<S> upd (add_anno_stable \<S> upd)"
  proof(case_tac upd)
    fix l'
    assume "upd = Acq l'"
    hence upd_def [simp]: "upd = Acq l"
      by(simp add: l_def)
    show "types_stable \<Gamma> \<oplus>\<^sub>\<S> upd (add_anno_stable \<S> upd)"
    proof(clarsimp simp: types_stable_def)
      fix x p v t
      assume xt: "(\<Gamma> \<oplus>\<^sub>\<S> Acq l) x = Some t"
      assume p: "p \<in> t"
      assume v: "v \<in> bexp_vars p"
      from xt have "\<Gamma> x = Some t \<or> (x \<in> l_footprint \<and> t = dma_type x)"
        by(clarsimp simp: add_anno_def add_anno_dom_def restrict_map_def to_total_def l_footprint_def split: if_splits prod.splits)
      thus "stable (add_anno_stable \<S> (Acq l)) (Var v)"
      proof
        assume "\<Gamma> x = Some t"
        thus ?thesis
          using `types_stable \<Gamma> \<S>` v p
          apply(clarsimp simp: stable_def add_anno_stable_def types_stable_def)
          by force
      next
        assume a: "x \<in> l_footprint \<and> t = dma_type x"
        hence "\<C>_vars x \<subseteq> l_footprint"
          using lock_interp_\<C>_vars\<^sub>V unfolding l_footprint_def using interp
          using \<open>\<And>x c. Var c \<in> \<C>_vars (Var x) \<Longrightarrow> (c \<in> fst (lock_interp l) \<union> snd (lock_interp l)) = (x \<in> fst (lock_interp l) \<union> snd (lock_interp l))\<close>
          using \<C>_vars_correct fst_conv image_iff snd_conv subsetI
          by fastforce
        moreover have "Var v \<in> \<C>_vars x"
          using a p v \<C>_vars_correct by auto
        ultimately have "Var v \<in> l_footprint"
          by blast
        thus "stable (add_anno_stable \<S> (Acq l)) (Var v)"
          using l_footprint_def interp
          by(auto simp: stable_def add_anno_stable_def)
      qed
    qed
  next
    fix l'
    assume "upd = Rel l'"
    hence upd_def [simp]: "upd = Rel l"
      by(simp add: l_def)
    show "types_stable \<Gamma> \<oplus>\<^sub>\<S> upd (add_anno_stable \<S> upd)"
    proof(clarsimp simp: types_stable_def)
      fix x p v t
      assume xt: "(\<Gamma> \<oplus>\<^sub>\<S> Rel l) x = Some t"
      assume p: "p \<in> t"
      assume v: "v \<in> bexp_vars p"
      from xt have \<Gamma>x: "\<Gamma> x = Some t" and disj: "(x \<in> fst \<S> \<and> x \<notin> Var ` NoW \<or> x \<in> snd \<S> \<and> x \<notin> Var ` NoRW)"
        apply(clarsimp simp:  add_anno_def add_anno_dom_def restrict_map_def to_total_def split: if_splits prod.splits)+
        done
      from \<Gamma>x disj have "x \<in> add_anno_dom \<Gamma> \<S> upd"
        by(auto simp add: add_anno_dom_def)

      moreover from `anno_type_stable \<Gamma> \<S> upd` have
        "\<forall>v. Var v \<in> l_footprint \<and> Var v \<in> \<C> \<longrightarrow> (\<forall>x\<in>add_anno_dom \<Gamma> \<S> upd. Var v \<notin> vars_of_type (the (\<Gamma> x)))" 
        unfolding anno_type_stable_def
        apply(clarsimp simp: l_footprint_def)
        by blast

      moreover have "Var v \<in> \<C>" using v \<Gamma>x p 
        using \<open>types_wellformed \<Gamma>\<close> types_mention_only_control_vars by auto

      ultimately have vnotin: "Var v \<notin> l_footprint"
        using \<Gamma>x p v
        using UnionI image_eqI option.sel by fastforce
      
      have "stable \<S> (Var v)"
        using \<Gamma>x p v `types_stable \<Gamma> \<S>` unfolding types_stable_def
        using domI option.sel by force
      with vnotin show "stable (add_anno_stable \<S> (Rel l)) (Var v)"
      by (auto simp: stable_def add_anno_stable_def l_footprint_def)
    qed
  qed
qed

lemma tyenv_wellformed_mode_update:
  "tyenv_wellformed mds \<Gamma> \<S> P \<Longrightarrow> anno_type_stable \<Gamma> \<S> upd \<Longrightarrow>
   tyenv_wellformed (update_modes upd mds) 
                  (\<Gamma> \<oplus>\<^sub>\<S> upd) 
                  (add_anno_stable \<S> upd) 
                  (P |`' {v. stable (add_anno_stable \<S> upd) v})"
  apply(clarsimp simp: tyenv_wellformed_def)
  apply(rule conjI)
   apply(blast intro: mode_update_add_anno)
  apply(rule conjI)
   apply(blast intro: types_wellformed_mode_update)
  apply(blast intro: types_stable_mode_update)
  done

(* Strangely, using only \<turnstile> \<Gamma> { Stop } \<Gamma>' as assumption does not work *)
lemma stop_cxt :
  "\<lbrakk> \<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P' ; c = Stop \<rbrakk> \<Longrightarrow> 
  context_equiv \<Gamma> P \<Gamma>'\<and> \<S>' = \<S> \<and> L: P \<turnstile> P' \<and> (\<forall>mds. tyenv_wellformed mds \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed mds \<Gamma>' \<S> P')"
  apply (induct rule: has_type.induct)
          apply (auto intro: context_equiv_trans context_equiv_entailment pred_entailment_trans split:if_splits)
  done

lemma tyenv_wellformed_preds_update:
  "P' = P'' |`' {v. stable \<S> v} \<Longrightarrow>
  tyenv_wellformed mds \<Gamma> \<S> P \<Longrightarrow> tyenv_wellformed mds \<Gamma> \<S> P'"
  apply(clarsimp simp: tyenv_wellformed_def)
  apply(clarsimp simp: mds_consistent_def)
  apply(auto simp: restrict_preds_to_vars_def add_pred_def split: if_splits)
  done


lemma mds_consistent_preds_tyenv_update:
  "P' = P'' |`' {v. stable \<S> v} \<Longrightarrow> v \<in> dom \<Gamma> \<Longrightarrow>
  mds_consistent mds \<Gamma> \<S> P \<Longrightarrow> mds_consistent mds (\<Gamma>(v \<mapsto> t)) \<S> P'"
  apply(clarsimp simp: mds_consistent_def)
  apply(auto simp: restrict_preds_to_vars_def add_pred_def split: if_splits)
  done


lemma pred_preds_update:
  assumes mem'_def: "mem' = mem ((Var x) := ev\<^sub>A (to_prog_mem mem) e)"
  assumes P'_def: "P' = (assign_post P x e) |`' {v. stable \<S> v}"
  assumes pred_P: "pred P mem" 
  shows "pred P' mem'"
proof -
  from P'_def have P'_def': "P' \<subseteq> assign_post P x e"
    by(auto simp: restrict_preds_to_vars_def add_pred_def split: if_splits)
  have "pred (assign_post P x e) mem'"
    using assign_post_valid pred_P mem'_def by blast
  with P'_def' show ?thesis
    unfolding pred_def by blast
qed

lemma types_wellformed_update:
  "types_wellformed \<Gamma> \<Longrightarrow> type_wellformed t \<Longrightarrow> types_wellformed (\<Gamma>(x \<mapsto> t))"
  by(auto simp: types_wellformed_def)

lemma types_stable_update:
  "types_stable \<Gamma> \<S> \<Longrightarrow> type_stable \<S> t \<Longrightarrow> types_stable (\<Gamma>(x \<mapsto> t)) \<S>"
  by(auto simp: types_stable_def)
  
lemma tyenv_wellformed_sub:
  "\<lbrakk>P\<^sub>1 \<subseteq> P\<^sub>2;
  \<Gamma>\<^sub>2 = \<Gamma>\<^sub>1; tyenv_wellformed mds \<Gamma>\<^sub>2 \<S> P\<^sub>2\<rbrakk> \<Longrightarrow>
     tyenv_wellformed mds \<Gamma>\<^sub>1 \<S> P\<^sub>1"
  apply(clarsimp simp: tyenv_wellformed_def | safe)+
  apply(fastforce simp: mds_consistent_def)
  done

abbreviation
  tyenv_sec :: "('Lock, 'Var) Var Mds \<Rightarrow> (('Lock, 'Var) Var,'BExp) TyEnv \<Rightarrow> (('Lock, 'Var) Var,'Val) Mem \<Rightarrow> bool"
where
  "tyenv_sec mds \<Gamma> mem \<equiv> \<forall>x\<in>dom \<Gamma>. x\<notin>mds AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) mem \<le> dma mem x"

(* This lemma is saying that type environment security is preserved by a lock acquisition *)
lemma tyenv_sec_lock_acq:
assumes
  new_entails_old: "(\<forall>x. L: (to_total \<Gamma> x) \<le>:\<^sub>P''  (to_total \<Gamma>'' x))" and
  P''_sat: "pred P'' mem" and
  stable_def_mds: "\<S> = (mds AsmNoWrite, mds AsmNoReadOrWrite)" and
  anno_type_sec: "anno_type_sec \<Gamma> \<S> P upd" and
  stable_upd: "\<S>'' = add_anno_stable \<S> upd" and
  type_was_stable: "type_stable \<S> P" and
  P''_def: "P'' = P |`' {v. stable \<S>'' v}" and
  tyenv_upd: "\<Gamma>'' = \<Gamma> \<oplus>\<^sub>\<S> upd" and
  tyenv_sec: "tyenv_sec mds \<Gamma> mem" and
  upd_def: "upd = Acq l"
shows
  "tyenv_sec (update_modes upd mds) \<Gamma>'' mem"
proof(clarsimp)
  fix x t l
  assume
    "\<Gamma>'' x = Some t"
    "x \<notin> update_modes upd mds AsmNoReadOrWrite"
  with assms
  show "type_max t mem \<le> dma mem x"
   apply clarsimp
   apply(clarsimp simp:add_anno_def to_total_def split:if_splits)
   apply(clarsimp simp:type_max_def)
   apply(drule_tac x=x in fun_cong)
   apply(clarsimp split:if_splits) 
   apply safe
    apply(erule_tac x=x in ballE)
     apply(clarsimp split:if_splits)
     apply(clarsimp simp:update_modes_def lock_acq_upd_def to_prog_mem_def split:prod.splits if_splits)
     apply(rename_tac NoW NoRW p)
     apply(clarsimp simp:restrict_map_def split: if_splits)
     using less_eq_Sec_def apply auto[1]
    using less_eq_Sec_def apply blast
   apply(erule_tac x=x in ballE)
    apply(clarsimp split:if_splits)
    apply(clarsimp simp:update_modes_def lock_acq_upd_def to_prog_mem_def split:prod.splits if_splits)
    apply(rename_tac p NoW NoRW)
    apply(clarsimp simp:restrict_map_def split: if_splits)
    apply(clarsimp simp:less_eq_Sec_def)
    apply(clarsimp simp: to_prog_mem_def dma_correct split:if_splits)
   apply clarsimp
   apply(clarsimp simp:update_modes_def lock_acq_upd_def to_prog_mem_def split:prod.splits if_splits)
   apply(rename_tac p NoW NoRW)
   apply(clarsimp simp:restrict_map_def split: if_splits)
   apply(clarsimp simp:less_eq_Sec_def)
   apply(clarsimp simp: to_prog_mem_def dma_correct split:if_splits)
   done
qed

lemma tyenv_sec_lock_rel:
assumes
  new_entails_old: "(\<forall>x. L: (to_total \<Gamma> x) \<le>:\<^sub>P''  (to_total \<Gamma>'' x))" and
  P''_sat: "pred P'' mem'" and
  stable_def_mds: "\<S> = (mds AsmNoWrite, mds AsmNoReadOrWrite)" and
  anno_type_sec: "anno_type_sec \<Gamma> \<S> P upd" and
  stable_upd: "\<S>'' = add_anno_stable \<S> upd" and
  type_was_stable: "type_stable \<S> P" and
  P''_def: "P'' = P |`' {v. stable \<S>'' v}" and
  tyenv_upd: "\<Gamma>'' = \<Gamma> \<oplus>\<^sub>\<S> upd" and
  tyenv_sec: "tyenv_sec mds \<Gamma> mem" and
  upd_def: "upd = Rel l" and
  \<S>_no_dups: "fst \<S> \<inter> snd \<S> = {}" and
  (* from lock_rel *)
  mds_correct: "lock_held_mds_correct lock_interp mds l" and
  mem'_l: "\<not> ev\<^sub>L (mem' (Lock l))" and
  mem'_x: "\<forall>x. x \<noteq> Lock l \<longrightarrow> mem' x = mem x"
shows
  "tyenv_sec (update_modes upd mds) \<Gamma>'' mem'"
proof(clarsimp)
next
  (* The Rel case *)
  fix x t
  assume
    x_keeps_type: "\<Gamma>'' x = Some t" and
    x_NoRW_dropped: "x \<notin> update_modes upd mds AsmNoReadOrWrite"
  (* x continues to have a type, so it must have continued to be stable *)
  from x_keeps_type tyenv_upd stable_upd upd_def
  have x_stable': "stable \<S>'' x"
    unfolding stable_def add_anno_def restrict_map_def add_anno_dom_def add_anno_stable_def
    by (clarsimp split:LockUpd.splits if_splits)
  (* for it to continue to be stable,
     there must also be an Asm for x that isn't managed by the lock being released *)
  from x_keeps_type x_stable' tyenv_upd upd_def
  have l_x_not_both: "x \<notin> Var ` fst (lock_interp l) \<or> x \<notin> Var ` snd (lock_interp l)"
    unfolding add_anno_def restrict_map_def add_anno_dom_def
    by (clarsimp split:LockUpd.splits if_splits)

  (* If NoRW was dropped for x, we know that AsmNoRW is managed by the lock released *)
  from x_NoRW_dropped upd_def
  have l_NoRW_x: "x \<in> mds AsmNoReadOrWrite \<Longrightarrow> x \<in> Var ` snd (lock_interp l)"
    unfolding update_modes_def lock_rel_upd_def
    by (clarsimp split:prod.splits)
  (* We also know AsmNoW is NOT managed by the lock released *)
  with l_x_not_both
  have l_not_NoW_x: "x \<in> mds AsmNoReadOrWrite \<Longrightarrow> x \<notin> Var ` fst (lock_interp l)"
    by clarsimp

  (* x has to have been stable, because Rel only releases assumptions, and
     x was stable after the Rel *)
  from x_stable' stable_upd upd_def
  have x_stable: "stable \<S> x"
    unfolding stable_def add_anno_stable_def
    by (clarsimp split:prod.splits)
  (* That means it must have had an assumption on it *)
  with stable_def_mds
  have x_asms: "x \<in> mds AsmNoWrite \<or> x \<in> mds AsmNoReadOrWrite"
    unfolding stable_def
    by clarsimp

  (* The same reasoning for \<S>'' *)
  from x_stable' upd_def stable_def_mds x_asms stable_upd
  have "x \<in> update_modes upd mds AsmNoWrite \<or> x \<in> update_modes upd mds AsmNoReadOrWrite"
    unfolding stable_def update_modes_def lock_rel_upd_def add_anno_stable_def
    by (clarsimp split:prod.splits)
  (* That means it continues to have a NoWrite assumption on it *)
  with x_NoRW_dropped
  have x_NoW_kept: "x \<in> update_modes upd mds AsmNoWrite"
    by simp
  hence x_had_NoW: "x \<in> mds AsmNoWrite"
    unfolding upd_def update_modes_def lock_rel_upd_def
    by (clarsimp split:prod.splits)
  hence x_in_fst_\<S>: "x \<in> fst \<S>"
    using stable_def_mds
    by clarsimp
  with \<S>_no_dups
  have "x \<notin> snd \<S>"
    by auto
  hence x_RW: "x \<notin> mds AsmNoReadOrWrite"
    using stable_def_mds
    by clarsimp

  (* x's old type was also t *)
  from new_entails_old x_keeps_type P''_sat tyenv_upd upd_def
  have x_had_type: "\<Gamma> x = Some t"
    apply -
    apply(erule_tac x=x in allE)
    unfolding to_total_def
    apply(clarsimp split:if_splits)
     unfolding subtype_def
     unfolding pred_entailment_def
     apply clarsimp
     apply(erule_tac x=mem in allE)
     unfolding add_anno_def restrict_map_def
     apply(clarsimp split:if_splits)
     unfolding to_total_def
     apply clarsimp
    apply clarsimp
    apply(clarsimp split:if_splits)
    unfolding add_anno_dom_def
    by clarsimp

  have x_in_upd_dom: "x \<in> add_anno_dom \<Gamma> \<S> (Rel l)"
    using x_had_type tyenv_upd x_keeps_type upd_def
    apply -
    unfolding add_anno_def restrict_map_def
    by (clarsimp split:if_splits)

  from tyenv_sec x_had_type mds_correct mem'_l mem'_x
  have thesis_RW: "x \<notin> mds AsmNoReadOrWrite \<Longrightarrow> type_max t mem' \<le> dma mem' x"
    apply -
    apply(erule_tac x=x in ballE)
     apply simp
     apply(erule_tac x=x in allE)
     apply(case_tac "x \<noteq> Lock l")
      apply clarsimp
      apply(clarsimp simp:type_max_def to_prog_mem_def split:if_splits)
       apply(rule conjI)
        apply clarsimp
        unfolding dma_correct
        apply clarsimp
        using less_eq_Sec_def apply blast
       apply clarsimp
       unfolding less_eq_Sec_def apply clarsimp
       unfolding to_prog_mem_def
       apply (simp add: mem'_x)
      apply clarsimp
      apply (simp add: mem'_x)
     apply clarsimp
     apply(rule conjI)
      apply clarsimp
      apply(clarsimp simp:type_max_def to_prog_mem_def split:if_splits)
       apply (simp add: mem'_x)
      apply (simp add: mem'_x)
     apply clarsimp
     using Sec.exhaust apply blast
    using Sec.exhaust apply blast
    done
  with x_RW
  show "type_max t mem' \<le> dma mem' x"
    by clarsimp
qed

lemma tyenv_sec_eq:
  "\<forall>x\<in>\<C>. mem x = mem' x \<Longrightarrow> types_wellformed \<Gamma> \<Longrightarrow> tyenv_sec mds \<Gamma> mem = tyenv_sec mds \<Gamma> mem'"
  apply(rule ball_cong[OF HOL.refl])
  apply(rename_tac x)
  apply(rule imp_cong[OF HOL.refl])
  apply(subgoal_tac "type_max (the (\<Gamma> x)) mem = type_max (the (\<Gamma> x)) mem'")
   using dma_\<C> apply fastforce
  apply(force intro: \<C>_eq_type_max_eq simp: types_wellformed_def)
  done

lemma context_equiv_tyenv_sec:
  "context_equiv \<Gamma>\<^sub>2 P\<^sub>2 \<Gamma>\<^sub>1 \<Longrightarrow>
    pred P\<^sub>2 mem \<Longrightarrow> tyenv_sec mds \<Gamma>\<^sub>2 mem \<Longrightarrow> tyenv_sec mds \<Gamma>\<^sub>1 mem"
   apply(clarsimp simp: context_equiv_def type_equiv_def)
   apply(rename_tac x y)
   apply(rule_tac y="type_max (the (\<Gamma>\<^sub>2 x)) mem" in order_trans)
    apply(rule subtype_sound[rule_format])
     apply force
    apply assumption
   apply force
   done

lemma add_pred_entailment:
  "L: P +\<^sub>\<S> p \<turnstile> P"
  apply(rule subset_entailment)
  apply(rule add_pred_subset)
  done


lemma preservation: 
  "\<lbrakk>\<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P';
    \<langle>c, mds, mem\<rangle> \<leadsto>  \<langle>c', mds', mem'\<rangle>\<rbrakk> \<Longrightarrow> 
  \<exists> \<Gamma>'' \<S>'' P''. (\<turnstile> \<Gamma>'',\<S>'',P'' { c' } \<Gamma>',\<S>',P') \<and> 
         (tyenv_wellformed mds \<Gamma> \<S> P \<and> pred P mem \<and> tyenv_sec mds \<Gamma> mem \<longrightarrow> 
           tyenv_wellformed mds' \<Gamma>'' \<S>'' P'' \<and> 
           pred P'' mem' \<and> 
           tyenv_sec mds' \<Gamma>'' mem')"
proof (induct arbitrary: c' mds rule: has_type.induct)
  case stop_type
  with stop_no_eval show ?case by auto
next
  case skip_type
  hence "c' = Stop \<and> mds' = mds \<and> mem' = mem"
    by (metis skip_dest)
  thus ?case
    by (metis stop_type)
next
  case (assign\<^sub>1 x x' \<Gamma> e t P P' \<S> \<Gamma>' c' mds)
  hence upd: "c' = Stop \<and> mds' = mds \<and> mem' = mem (x := ev\<^sub>A (to_prog_mem mem) e)"
    by (metis assign_dest)
  from assign\<^sub>1 upd have \<C>_eq: "\<forall>x\<in>\<C>. mem x = mem' x"
    by auto
  from upd have " \<turnstile> \<Gamma>,\<S>,P' {c'} \<Gamma>,\<S>,P'"
    by (metis stop_type)
  moreover have "tyenv_wellformed mds \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed mds' \<Gamma> \<S> P'"
    using upd tyenv_wellformed_preds_update assign\<^sub>1 by metis
  moreover have "pred P mem \<longrightarrow> pred P' mem'"
    using pred_preds_update assign\<^sub>1 upd by metis
    
  moreover have "tyenv_wellformed mds \<Gamma> \<S> P  \<and> tyenv_sec mds \<Gamma> mem \<longrightarrow> tyenv_sec mds \<Gamma> mem'"
    using tyenv_sec_eq[OF \<C>_eq, where \<Gamma>=\<Gamma>]
    unfolding tyenv_wellformed_def by blast
  ultimately show ?case
    using assign\<^sub>1.hyps
    by (metis upd)
next
  case (assign\<^sub>2 x x' \<Gamma> e t \<S> P' P \<Gamma>' c' mds)
  hence upd: "c' = Stop \<and> mds' = mds \<and> mem' = mem (x := ev\<^sub>A (to_prog_mem mem) e)"
    by (metis assign_dest)
  let ?\<Gamma>' = "\<Gamma> (x \<mapsto> t)"
  from upd have ty: " \<turnstile> ?\<Gamma>',\<S>,P' {c'} ?\<Gamma>',\<S>,P'"
    by (metis stop_type)
  have wf: "tyenv_wellformed mds \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed mds' ?\<Gamma>' \<S> P'"
  proof
    assume tyenv_wf: "tyenv_wellformed mds \<Gamma> \<S> P"
    hence wf: "types_wellformed \<Gamma>"
      unfolding tyenv_wellformed_def by blast
    hence  "type_wellformed t"
      using assign\<^sub>2 type_aexpr_type_wellformed
      by blast
    with wf have wf': "types_wellformed ?\<Gamma>'"
      using types_wellformed_update by metis
    from tyenv_wf have stable': "types_stable ?\<Gamma>' \<S>"
      using types_stable_update
            assign\<^sub>2.hyps(4)
      unfolding tyenv_wellformed_def by blast
    have m: "mds_consistent mds \<Gamma> \<S> P"
      using tyenv_wf unfolding tyenv_wellformed_def by blast
    from assign\<^sub>2.hyps(5) assign\<^sub>2(2)
    have "mds_consistent mds' (\<Gamma>(x \<mapsto> t)) \<S> P'"
      apply(rule mds_consistent_preds_tyenv_update)
      using upd m by simp
    from wf' stable' this show "tyenv_wellformed mds' ?\<Gamma>' \<S> P'"
      unfolding tyenv_wellformed_def by blast
  qed
  have p: "pred P mem \<longrightarrow> pred P' mem'"
    using pred_preds_update assign\<^sub>2 upd by metis
  have sec: "tyenv_wellformed mds \<Gamma> \<S> P \<Longrightarrow> pred P mem \<Longrightarrow> tyenv_sec mds \<Gamma> mem \<Longrightarrow> tyenv_sec mds' ?\<Gamma>' mem'"
  proof(clarify)
    assume wf: "tyenv_wellformed mds \<Gamma> \<S> P"
    assume pred: "pred P mem"
    assume sec: "tyenv_sec mds \<Gamma> mem"
    from pred p have pred': "pred P' mem'" by blast
    fix v t'
    assume \<Gamma>v: "(\<Gamma>(x \<mapsto> t)) v = Some t'"
    assume "v \<notin> mds' AsmNoReadOrWrite"
    show "type_max (the ((\<Gamma>(x \<mapsto> t)) v)) mem' \<le> dma mem' v"
    proof(cases "v = x")
      assume [simp]: "v = x"
      hence [simp]: "(the ((\<Gamma>(x \<mapsto> t)) v)) = t" and t_def: "t = t'"
        using \<Gamma>v by auto
      from `v \<notin> mds' AsmNoReadOrWrite` upd wf have readable: "v \<notin> snd \<S>"
        by(auto simp: tyenv_wellformed_def mds_consistent_def)
      with assign\<^sub>2.hyps(6) have "L: t \<le>:\<^sub>P' (dma_type x)" by fastforce
      with pred' show ?thesis
        using type_max_dma_type subtype_correct
        by fastforce
    next
      assume neq: "v \<noteq> x"
      hence [simp]: "((\<Gamma>(x \<mapsto> t)) v) = \<Gamma> v"
        by simp
      with \<Gamma>v have \<Gamma>v: "\<Gamma> v = Some t'" by simp
      with sec upd `v \<notin> mds' AsmNoReadOrWrite` have f_leq: "type_max t' mem \<le> dma mem v"
        by auto
      have \<C>_eq: "\<forall>x\<in>\<C>. mem x = mem' x"
        using wf assign\<^sub>2 upd by(auto simp: tyenv_wellformed_def mds_consistent_def)
      hence dma_eq: "dma mem = dma mem'"
        by(rule dma_\<C>)
      have f_eq: "type_max t' mem = type_max t' mem'"
        apply(rule \<C>_eq_type_max_eq)
         using \<Gamma>v wf apply(force simp: tyenv_wellformed_def types_wellformed_def)
        by(rule \<C>_eq)     
      from neq \<Gamma>v f_leq dma_eq f_eq show ?thesis
        by simp
    qed
  qed
  from ty wf p sec assign\<^sub>2.hyps show ?case
    by blast
next
  case (assign\<^sub>\<C> x x' \<Gamma> e t P P' \<S> \<Gamma>' c' mds)
  (* this case follows from the same argument as assign\<^sub>1 *)
  hence upd: "c' = Stop \<and> mds' = mds \<and> mem' = mem (x := ev\<^sub>A (to_prog_mem mem) e)"
    by (metis assign_dest)
  hence " \<turnstile> \<Gamma>,\<S>,P' {c'} \<Gamma>,\<S>,P'"
    by (metis stop_type)
  moreover have "tyenv_wellformed mds \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed mds' \<Gamma> \<S> P'"
    using upd tyenv_wellformed_preds_update assign\<^sub>\<C> by metis
  moreover have "pred P mem \<longrightarrow> pred P' mem'"
    using pred_preds_update assign\<^sub>\<C> upd by metis
  moreover have "tyenv_wellformed mds \<Gamma> \<S> P \<and> pred P mem \<and> tyenv_sec mds \<Gamma> mem \<Longrightarrow> tyenv_sec mds' \<Gamma> mem'"
  proof(clarify)
    fix v t'
    assume wf: "tyenv_wellformed mds \<Gamma> \<S> P"
    assume pred: "pred P mem"
    hence pred': "pred P' mem'" using `pred P mem \<longrightarrow> pred P' mem'` by blast
    assume sec: "tyenv_sec mds \<Gamma> mem"
    assume \<Gamma>v: "\<Gamma> v = Some t'"
    assume readable': "v \<notin> mds' AsmNoReadOrWrite"
    with upd have readable: "v \<notin> mds AsmNoReadOrWrite" by simp
    with wf have "v \<notin> snd \<S>" by(auto simp: tyenv_wellformed_def mds_consistent_def)
    show "type_max (the (\<Gamma> v)) mem' \<le> dma mem' v"
    proof(cases "x \<in> \<C>_vars v")
      assume "x \<in> \<C>_vars v"
      with assign\<^sub>\<C>.hyps `v \<notin> snd \<S>` have "L: (to_total \<Gamma> v) \<le>:\<^sub>P' (dma_type v)" by blast
      from pred' \<Gamma>v subtype_correct this show ?thesis
        using type_max_dma_type by(auto simp: to_total_def split: if_splits)
    next
      assume "x \<notin> \<C>_vars v"
      hence "\<forall>v'\<in>\<C>_vars v. mem v' = mem' v'"
        using upd by auto
      hence dma_eq: "dma mem v = dma mem' v"
        by(rule dma_\<C>_vars)
      from \<Gamma>v assign\<^sub>\<C> have "x \<notin> vars_of_type t'"
        by (metis domIff option.sel option.simps(3))
      have "type_wellformed t'"
        using wf \<Gamma>v by(force simp: tyenv_wellformed_def types_wellformed_def)
      with `x \<notin> vars_of_type t'` upd have f_eq: "type_max t' mem = type_max t' mem'"
        using vars_of_type_eq_type_max_eq
        by (metis fun_upd_apply)
      from sec \<Gamma>v readable have "type_max t' mem \<le> dma mem v"
        by auto
      with f_eq dma_eq \<Gamma>v show ?thesis
        by simp
    qed
  qed
  ultimately show ?case
    using assign\<^sub>\<C>.hyps
    by (metis)
next
  case (if_type \<Gamma> e t P \<S> th \<Gamma>' \<S>' P' el  \<Gamma>'' P'' \<Gamma>''' P''' c' mds)
  from if_type(13)
  show ?case
  proof(rule if_elim)
    assume [simp]: "ev\<^sub>B (to_prog_mem mem) e" and [simp]: "c' = th" and [simp]: "mem' = mem" and [simp]: "mds' = mds"
    from if_type(3) have " \<turnstile> \<Gamma>,\<S>,P +\<^sub>\<S> e {c'} \<Gamma>',\<S>',P'" by simp
    hence "\<turnstile> \<Gamma>,\<S>,P +\<^sub>\<S> e {c'} \<Gamma>''',\<S>',P'''"
      apply(rule sub)
           apply simp+
         using if_type apply blast
        using if_type apply blast
       apply simp
      using if_type apply(blast intro: pred_entailment_trans)
      done
    moreover have "tyenv_wellformed mds \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed mds' \<Gamma> \<S> (P +\<^sub>\<S> e)"
      by(auto simp: tyenv_wellformed_def mds_consistent_def add_pred_def)
    moreover have "pred P mem \<longrightarrow> pred (P +\<^sub>\<S> e) mem'"
      by(auto simp: pred_def add_pred_def)
    moreover have "tyenv_sec mds \<Gamma> mem \<longrightarrow> tyenv_sec mds' \<Gamma> mem'"
      by(simp)
    ultimately show ?case by blast
  next
    assume [simp]: "\<not> ev\<^sub>B (to_prog_mem mem) e" and [simp]: "c' = el" and [simp]: "mem' = mem" and [simp]: "mds' = mds"
    from if_type(5) have " \<turnstile> \<Gamma>,\<S>,P +\<^sub>\<S> bexp_neg e {c'} \<Gamma>'',\<S>',P''" by simp
    hence "\<turnstile> \<Gamma>,\<S>,P +\<^sub>\<S> bexp_neg e {c'} \<Gamma>''',\<S>',P'''"
      apply(rule sub)
           apply simp+
         using if_type apply blast
        using if_type apply blast
       apply simp
      using if_type apply(blast intro: pred_entailment_trans)
      done
    moreover have "tyenv_wellformed mds \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed mds' \<Gamma> \<S> (P +\<^sub>\<S> bexp_neg e)"
      by(auto simp: tyenv_wellformed_def mds_consistent_def add_pred_def)
    moreover have "pred P mem \<longrightarrow> pred (P +\<^sub>\<S> bexp_neg e) mem'"
      by(auto simp: pred_def add_pred_def bexp_neg_negates)
    moreover have "tyenv_sec mds \<Gamma> mem \<longrightarrow> tyenv_sec mds' \<Gamma> mem'"
      by(simp)
    ultimately show ?case by blast
  qed
next
  case (while_type c \<Gamma> e t P \<S> c' mds)
  hence [simp]: "mds' = mds \<and> c' = If e (c ;; While e c) Stop \<and> mem' = mem"
    by (metis while_dest)
  moreover
  have "\<turnstile> \<Gamma>,\<S>,P {c'} \<Gamma>,\<S>,P"
    apply simp
    apply(rule if_type)
             apply(rule while_type(2))
            apply(rule while_type(3))
           apply(rule seq_type)
             apply(rule while_type(1))
            apply(rule while_type(4))
           apply(rule has_type.while_type)
              apply(rule while_type(1))
             apply(rule while_type(2))
            apply(rule while_type(3))
           apply(rule while_type(4))
          apply(rule stop_type)
         apply simp
        apply simp
       apply simp
      apply(rule add_pred_entailment)
      apply simp+
    by(blast intro!: tyenv_wellformed_subset add_pred_subset)
  ultimately show ?case
    by blast
next
  case (seq_type c\<^sub>1 \<Gamma> \<S> P \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 c\<^sub>2 \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 c' mds)
  then obtain c\<^sub>1' where step: "\<langle>c\<^sub>1, mds, mem\<rangle> \<leadsto> \<langle>c\<^sub>1', mds', mem'\<rangle> \<and> c' = (if c\<^sub>1' = Stop then c\<^sub>2 else c\<^sub>1' ;; c\<^sub>2)"
    by (metis seq_dest seq_type.prems)
  then obtain \<Gamma>''' \<S>''' P''' where "\<turnstile> \<Gamma>''',\<S>''',P''' {c\<^sub>1'} \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1 \<and>
    (tyenv_wellformed mds \<Gamma> \<S> P \<and> pred P mem \<and> tyenv_sec mds \<Gamma> mem \<longrightarrow> 
     tyenv_wellformed mds' \<Gamma>''' \<S>''' P''' \<and> pred P''' mem' \<and> tyenv_sec mds' \<Gamma>''' mem')"
    using seq_type(3)
    by blast
  moreover
  from seq_type have "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1 {c\<^sub>2} \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2" by auto
  ultimately show ?case
    using step
    apply(split if_splits)
     apply(rule_tac x=\<Gamma>\<^sub>1 in exI)
     apply(rule_tac x=\<S>\<^sub>1 in exI)
     apply(rule_tac x=P\<^sub>1 in exI)
     apply clarsimp
     apply(frule_tac c=Stop in stop_cxt)
      apply force
     apply clarsimp
     apply(force simp:context_equiv_tyenv_sec pred_entailment_def)
    by blast
next
  case (sub \<Gamma>\<^sub>1 \<S> P\<^sub>1 c \<Gamma>\<^sub>1' \<S>' P\<^sub>1' \<Gamma>\<^sub>2 P\<^sub>2 \<Gamma>\<^sub>2' P\<^sub>2' c' mds)
  then obtain \<Gamma>'' \<S>'' P'' where stuff: "\<turnstile> \<Gamma>'',\<S>'',P'' { c' } \<Gamma>\<^sub>1',\<S>',P\<^sub>1' \<and>
    (tyenv_wellformed mds \<Gamma>\<^sub>1 \<S> P\<^sub>1 \<and> pred P\<^sub>1 mem \<and> tyenv_sec mds \<Gamma>\<^sub>1 mem \<longrightarrow> 
     tyenv_wellformed mds' \<Gamma>'' \<S>'' P'' \<and> pred P'' mem' \<and> tyenv_sec mds' \<Gamma>'' mem')"
    by force

  have imp: "tyenv_wellformed mds \<Gamma>\<^sub>2 \<S> P\<^sub>2 \<and> pred P\<^sub>2 mem \<and> tyenv_sec mds \<Gamma>\<^sub>2 mem \<Longrightarrow> 
             tyenv_wellformed mds \<Gamma>\<^sub>1 \<S> P\<^sub>1 \<and> pred P\<^sub>1 mem \<and> tyenv_sec mds \<Gamma>\<^sub>1 mem"
    apply(rule conjI)
     using sub(5) sub(4) tyenv_wellformed_sub unfolding pred_def
     apply blast
    apply(rule conjI) 
     using local.pred_def pred_entailment_def sub.hyps(7) apply auto[1]
    using sub(3) context_equiv_tyenv_sec unfolding pred_def by blast
  show ?case
    apply (rule_tac x = \<Gamma>'' in exI, rule_tac x = "\<S>''" in exI, rule_tac x="P''" in exI)  
    apply (rule conjI)
     apply(rule has_type.sub)
           apply(rule stuff[THEN conjunct1])
          apply simp+
        apply(rule sub(5))
       apply(rule sub(6))
      apply simp
     using sub apply blast
    using imp stuff apply blast
   done

next
  case (lock_acq_type upd l \<Gamma>' \<Gamma> \<S> \<S>' P' P)
  thus ?case
  proof (cases "ev\<^sub>L (mem (Lock l))")
    assume "ev\<^sub>L (mem (Lock l))"
    with lock_acq_type show ?case
    by blast
  next
    assume "\<not> ev\<^sub>L (mem (Lock l))"
    with lock_acq_type
    have upd: "c' = Stop" and
      "ev\<^sub>L (mem' (Lock l))" and "\<forall>x. x \<noteq> Lock l \<longrightarrow> mem' x = mem x" and
      "mds' = update_modes upd mds"
      by (force simp add: lock_acq\<^sub>w_elim eval\<^sub>L_True update_modes_def)+
    moreover
    have "tyenv_wellformed mds \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (update_modes upd mds) \<Gamma>' \<S>' P'"
      using lock_acq_type
      apply auto
      using tyenv_wellformed_mode_update
      by simp
    moreover
    have pred: "pred P mem \<longrightarrow> pred P' mem"
      using lock_acq_type
      by (auto simp: pred_def restrict_preds_to_vars_def)
    moreover
    have "tyenv_wellformed mds \<Gamma> \<S> P \<and> pred P mem \<and> tyenv_sec mds \<Gamma> mem \<longrightarrow> tyenv_sec (update_modes upd mds) \<Gamma>' mem"
      apply(rule impI)
      apply(rule tyenv_sec_lock_acq)
               using lock_acq_type apply fastforce
              using lock_acq_type pred apply fastforce
             using lock_acq_type apply fastforce
            using lock_acq_type apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
           using lock_acq_type apply fastforce
          apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
         apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
        using lock_acq_type apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
       apply simp
      using lock_acq_type by simp
    ultimately show ?case
      apply(rule_tac x=\<Gamma>' in exI)
      apply(rule_tac x=\<S>' in exI)
      apply(rule_tac x=P' in exI)
      apply clarsimp
      apply(rule conjI)
       apply(rule stop_type', simp+)
      apply clarsimp
      apply(rule conjI)
       apply(simp add: pred_def to_prog_mem_def)
      apply clarsimp
      apply(rename_tac x t)
      apply(case_tac "x = Lock l")
       apply clarsimp
       apply (metis (no_types, lifting) dma_\<C> domIff no_locks_in_\<C> option.sel option.simps(3) sifum_types.tyenv_wellformed_def sifum_types_axioms subsetCE type_wellformed_def types_wellformed_def vars_of_type_eq_type_max_eq)
      by (metis (no_types, lifting) dma_\<C> domIff no_locks_in_\<C> option.sel option.simps(3) sifum_types.tyenv_wellformed_def sifum_types_axioms subsetCE type_wellformed_def types_wellformed_def vars_of_type_eq_type_max_eq)
  qed
next
  (* TODO: rel case *)
  (* fix x t *)
  case (lock_rel_type upd l \<Gamma>' \<Gamma> \<S> \<S>' P' P)
  thus ?case
  proof (cases "lock_held_mds_correct lock_interp mds l")
    assume "\<not> lock_held_mds_correct lock_interp mds l"
    with lock_rel_type show ?case
    by blast
  next
    assume "lock_held_mds_correct lock_interp mds l"
    with lock_rel_type
    have upd: "c' = Stop"
     "lock_held_mds_correct lock_interp mds l"
     "\<not> ev\<^sub>L (mem' (Lock l))" "\<forall>x. x \<noteq> Lock l \<longrightarrow> mem' x = mem x"
     "mds' = update_modes upd mds"
      by (force simp add: lock_rel\<^sub>w_elim eval\<^sub>L_False update_modes_def)+
    moreover
    have "tyenv_wellformed mds \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (update_modes upd mds) \<Gamma>' \<S>' P'"
      using lock_rel_type
      apply auto
      by (metis tyenv_wellformed_mode_update)
    moreover
    have pred: "pred P mem \<longrightarrow> pred P' mem'"
      using lock_rel_type
      unfolding pred_def restrict_preds_to_vars_def
      apply clarsimp
      using upd
      apply clarsimp
      unfolding to_prog_mem_def
      by clarsimp
    moreover
    have "tyenv_wellformed mds \<Gamma> \<S> P \<and> pred P mem \<and> tyenv_sec mds \<Gamma> mem \<longrightarrow> tyenv_sec (update_modes upd mds) \<Gamma>' mem'"
      apply(rule impI)
      apply(rule tyenv_sec_lock_rel)
                   using lock_rel_type apply fastforce
                  using lock_rel_type pred apply fastforce
                 using lock_rel_type apply fastforce
                using lock_rel_type apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
               using lock_rel_type apply fastforce
              apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
             apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
            using lock_rel_type apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
           using lock_rel_type apply fastforce
          using upd lock_rel_type apply simp
         using lock_rel_type apply (metis (no_types, lifting) mds_consistent_def sifum_types.tyenv_wellformed_def sifum_types_axioms)
        using upd lock_rel_type apply simp
       using upd lock_rel_type apply simp
      using upd lock_rel_type apply simp
      done
    ultimately
    show ?case
      apply(rule_tac x=\<Gamma>' in exI)
      apply(rule_tac x=\<S>' in exI)
      apply(rule_tac x=P' in exI)
      apply clarsimp
      by (rule stop_type', simp+)
  qed
qed


fun bisim_helper :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow> bool"
  where
  "bisim_helper \<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2\<rangle> = mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>l mem\<^sub>2"

lemma \<R>\<^sub>3_mem_eq: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<Longrightarrow> mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>l mem\<^sub>2"
  apply (subgoal_tac "bisim_helper \<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>")
   apply simp
  apply (induct rule: \<R>\<^sub>3_aux.induct)
  by (auto simp: \<R>\<^sub>1_mem_eq)


lemma ev\<^sub>A_eq:
  assumes tyenv_eq: "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assumes pred: "pred P mem\<^sub>1" 
  assumes e_type: "\<Gamma> \<turnstile>\<^sub>a e \<in> t"
  assumes subtype: "L: t \<le>:\<^sub>P (dma_type v)"
  assumes is_Low: "dma mem\<^sub>1 v = Low"
  shows "ev\<^sub>A (to_prog_mem mem\<^sub>1) e = ev\<^sub>A (to_prog_mem mem\<^sub>2) e"
proof(rule eval_vars_det\<^sub>A, clarify)
  fix x
  assume in_vars: "x \<in> aexp_vars e"
  have "type_max (to_total \<Gamma> (Var x)) mem\<^sub>1 = Low"
  proof -
    from subtype_sound[OF subtype] pred have "type_max t mem\<^sub>1 \<le> dma mem\<^sub>1 v"
      by(auto)
    with is_Low have "type_max t mem\<^sub>1 = Low" by(auto simp: less_eq_Sec_def)
    with e_type in_vars show ?thesis
      apply -
      apply(erule type_aexpr.cases)
      using Sec.exhaust by(auto simp: type_max_def split: if_splits)
  qed
  thus "(to_prog_mem mem\<^sub>1) x = (to_prog_mem mem\<^sub>2) x"
    using tyenv_eq unfolding tyenv_eq_def to_prog_mem_def by simp
qed

lemma ev\<^sub>A_eq':
  assumes tyenv_eq: "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assumes pred: "pred P mem\<^sub>1" 
  assumes e_type: "\<Gamma> \<turnstile>\<^sub>a e \<in> t"
  assumes subtype: "L: P \<turnstile> t"
  shows "ev\<^sub>A (to_prog_mem mem\<^sub>1) e = ev\<^sub>A (to_prog_mem mem\<^sub>2) e"
proof(rule eval_vars_det\<^sub>A, clarify)
  fix x
  assume in_vars: "x \<in> aexp_vars e"
  have "type_max (to_total \<Gamma> (Var x)) mem\<^sub>1 = Low"
  proof -
    from subtype pred have "type_max t mem\<^sub>1 \<le> Low"
      by(auto simp: type_max_def pred_entailment_def pred_def)
    hence "type_max t mem\<^sub>1 = Low" by(auto simp: less_eq_Sec_def)
    with e_type in_vars show ?thesis
      apply -
      apply(erule type_aexpr.cases)
      using Sec.exhaust by(auto simp: type_max_def  split: if_splits)
  qed
  thus "(to_prog_mem mem\<^sub>1) x = (to_prog_mem mem\<^sub>2) x"
    using tyenv_eq unfolding tyenv_eq_def to_prog_mem_def by simp
qed

lemma ev\<^sub>B_eq':
  assumes tyenv_eq: "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assumes pred: "pred P mem\<^sub>1" 
  assumes e_type: "\<Gamma> \<turnstile>\<^sub>b e \<in> t"
  assumes subtype: "L: P \<turnstile> t"
  shows "ev\<^sub>B (to_prog_mem mem\<^sub>1) e = ev\<^sub>B (to_prog_mem mem\<^sub>2) e"
proof(rule eval_vars_det\<^sub>B, clarify)
  fix x
  assume in_vars: "x \<in> bexp_vars e"
  have "type_max (to_total \<Gamma> (Var x)) mem\<^sub>1 = Low"
  proof -
    from subtype pred have "type_max t mem\<^sub>1 \<le> Low"
      by(auto simp: type_max_def pred_entailment_def pred_def)
    hence "type_max t mem\<^sub>1 = Low" by(auto simp: less_eq_Sec_def)
    with e_type in_vars show ?thesis
      apply -
      apply(erule type_bexpr.cases)
      using Sec.exhaust by(auto simp: type_max_def  split: if_splits)
  qed
  thus "(to_prog_mem mem\<^sub>1) x = (to_prog_mem mem\<^sub>2) x"
    using tyenv_eq unfolding tyenv_eq_def to_prog_mem_def by simp
qed

lemma R1_equiv_entailment:
  "\<langle>c, mds, mem\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c', mds', mem'\<rangle> \<Longrightarrow>  
  context_equiv \<Gamma>' P' \<Gamma>'' \<Longrightarrow> L: P' \<turnstile> P'' \<Longrightarrow>
  \<forall>mds. tyenv_wellformed mds \<Gamma>' \<S>' P' \<longrightarrow> tyenv_wellformed mds \<Gamma>'' \<S>' P'' \<Longrightarrow>
   \<langle>c, mds, mem\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>'',\<S>',P''\<^esub> \<langle>c', mds', mem'\<rangle>"
  apply(induct rule: \<R>\<^sub>1.induct)
  apply(rule \<R>\<^sub>1.intro)
       apply(blast intro: sub context_equiv_refl pred_entailment_refl)+
  done

lemma R3_equiv_entailment:
  "\<langle>c, mds, mem\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c', mds', mem'\<rangle> \<Longrightarrow> 
    context_equiv \<Gamma>' P' \<Gamma>'' \<Longrightarrow> L: P' \<turnstile> P'' \<Longrightarrow> 
  \<forall>mds. tyenv_wellformed mds \<Gamma>' \<S>' P' \<longrightarrow> tyenv_wellformed mds \<Gamma>'' \<S>' P'' \<Longrightarrow>
  \<langle>c, mds, mem\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>'',\<S>',P''\<^esub> \<langle>c', mds', mem'\<rangle>"
  apply(induct rule: \<R>\<^sub>3_aux.induct)
   apply(erule \<R>\<^sub>3_aux.intro\<^sub>1)
   apply(blast intro: sub context_equiv_refl tyenv_wellformed_subset subset_entailment)
  apply(erule \<R>\<^sub>3_aux.intro\<^sub>3)
  apply(blast intro: sub context_equiv_refl tyenv_wellformed_subset subset_entailment)
  done

lemma R_equiv_entailment:
  "lc\<^sub>1 \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> lc\<^sub>2 \<Longrightarrow> 
    context_equiv \<Gamma>' P' \<Gamma>'' \<Longrightarrow> L: P' \<turnstile> P'' \<Longrightarrow> 
  \<forall>mds. tyenv_wellformed mds \<Gamma>' \<S>' P' \<longrightarrow> tyenv_wellformed mds \<Gamma>'' \<S>' P'' \<Longrightarrow>
  lc\<^sub>1 \<R>\<^sup>u\<^bsub>\<Gamma>'',\<S>',P''\<^esub> lc\<^sub>2"
  apply(induct rule: \<R>.induct)
   apply clarsimp
   apply(rule \<R>.intro\<^sub>1)
   apply(fastforce intro: R1_equiv_entailment)
  apply(rule \<R>.intro\<^sub>3)
  apply(fastforce intro: R3_equiv_entailment)
  done

lemma context_equiv_tyenv_eq:
  "tyenv_eq \<Gamma> mem mem' \<Longrightarrow> context_equiv \<Gamma> P \<Gamma>' \<Longrightarrow> pred P mem \<Longrightarrow> tyenv_eq \<Gamma>' mem mem'"
  apply(clarsimp simp: tyenv_eq_def to_total_def context_equiv_def split: if_splits simp: type_equiv_def)
  using subtype_trans subtype_sound
  by (metis domI less_eq_Sec_def option.sel)

(* This is the main part of the proof and used in \<R>\<^sub>1_weak_bisim: *)
lemma \<R>_typed_step:
"\<lbrakk> \<turnstile> \<Gamma>,\<S>,P { c\<^sub>1 } \<Gamma>',\<S>',P' ;
  tyenv_wellformed mds \<Gamma> \<S> P; mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2; pred P mem\<^sub>1;
        pred P mem\<^sub>2; tyenv_sec mds \<Gamma> mem\<^sub>1;
     \<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>\<rbrakk> \<Longrightarrow>
   (\<exists> c\<^sub>2' mem\<^sub>2'. \<langle>c\<^sub>1, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle> \<and>
                 \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>)"
proof (induct arbitrary: mds c\<^sub>1' rule: has_type.induct)
  case (seq_type c\<^sub>1 \<Gamma> \<S> P \<Gamma>'' \<S>'' P'' c\<^sub>2 \<Gamma>' \<S>' P' mds)
  show ?case
  proof (cases "c\<^sub>1 = Stop")
    assume "c\<^sub>1 = Stop"
    with seq_type show ?case
      by fastforce
  next
    assume "c\<^sub>1 \<noteq> Stop"
    with `\<langle>c\<^sub>1 ;; c\<^sub>2, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>` obtain c\<^sub>1'' where c\<^sub>1''_props:
      "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1'', mds', mem\<^sub>1'\<rangle> \<and> c\<^sub>1' = (if c\<^sub>1'' = Stop then c\<^sub>2 else c\<^sub>1'' ;; c\<^sub>2)"
      by (metis seq_dest)
    with seq_type(3) obtain c\<^sub>2'' mem\<^sub>2' where c\<^sub>2''_props:
      "\<langle>c\<^sub>1, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2'', mds', mem\<^sub>2'\<rangle> \<and> \<langle>c\<^sub>1'', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>'',\<S>'',P''\<^esub> \<langle>c\<^sub>2'', mds', mem\<^sub>2'\<rangle>"
      using seq_type.prems(1-5) c\<^sub>1''_props
      by blast
    hence "\<langle>if c\<^sub>1'' = Stop then c\<^sub>2 else c\<^sub>1'' ;; c\<^sub>2, mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>if c\<^sub>2'' = Stop then c\<^sub>2 else c\<^sub>2'' ;; c\<^sub>2, mds', mem\<^sub>2'\<rangle>"
      apply(rule conjE)
      apply clarsimp
      apply(rule conjI)
       apply clarsimp
       apply(rule conjI)
        apply(erule \<R>_elim)
         prefer 2
         apply(erule \<R>\<^sub>3_elim)
          apply force
         apply force
        apply clarsimp
        apply(erule \<R>\<^sub>1_elim)
        using c\<^sub>1''_props seq_type(2)
        apply(frule_tac c=c\<^sub>1 and mem=mem\<^sub>1 in preservation)
         apply force
        apply clarsimp
        apply(erule impE)
         using seq_type(6,8,10)
         apply force
        apply(rule \<R>.intro\<^sub>1)
        apply clarsimp
        apply(rule \<R>\<^sub>1.intro)
              using seq_type(4) apply force
             using stop_cxt apply blast
            using context_equiv_tyenv_eq stop_cxt apply blast
           using pred_entailment_def stop_cxt apply blast
          using pred_entailment_def stop_cxt apply blast
         apply(metis (mono_tags, hide_lams) less_eq_Sec_def sifum_types.context_equiv_def sifum_types.subtype_sound sifum_types.type_equiv_def sifum_types_axioms stop_cxt)
       apply(force simp add: bisim_simple_\<R>\<^sub>1)
      apply clarsimp
      apply(rule conjI)
       apply(force simp add: bisim_simple_\<R>\<^sub>1)
      apply(erule \<R>_elim)
       apply clarsimp
       apply(metis \<R>.intro\<^sub>3 \<R>\<^sub>3_aux.intro\<^sub>1 seq_type(4))
      apply(metis \<R>.intro\<^sub>3 \<R>\<^sub>3_aux.intro\<^sub>3 seq_type(4))
      done
    moreover
    from c\<^sub>2''_props have "\<langle>c\<^sub>1 ;; c\<^sub>2, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>(if c\<^sub>2'' = Stop then c\<^sub>2 else c\<^sub>2'' ;; c\<^sub>2), mds', mem\<^sub>2'\<rangle>"
      using eval\<^sub>w.seq1 eval\<^sub>w.seq2 by force
    ultimately show ?case
      apply(rule_tac x="if c\<^sub>2'' = Stop then c\<^sub>2 else c\<^sub>2'' ;; c\<^sub>2" in exI)
      apply(rule_tac x=mem\<^sub>2' in exI)
      by (clarsimp simp:c\<^sub>1''_props split:if_splits)
  qed
next
  case stop_type
  with stop_no_eval show ?case by auto
next
  case (skip_type \<Gamma> \<S> P mds)
  moreover
  with skip_type have [simp]: "mds' = mds" "c\<^sub>1' = Stop" "mem\<^sub>1' = mem\<^sub>1"
    using skip_dest
    by (metis, metis, metis)
  with skip_type have "\<langle>Stop, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>Stop, mds, mem\<^sub>2\<rangle>"
    by auto
  thus ?case
    using \<R>.intro\<^sub>1 and simple [where c = Skip]
    apply auto
    by (metis (mono_tags, lifting) \<R>.intro\<^sub>1 old.prod.case skip_eval\<^sub>w)
next (* assign\<^sub>1 *)
  case (assign\<^sub>1 x x' \<Gamma> e t P P' \<S> mds)
  hence upd [simp]: "c\<^sub>1' = Stop" "mds' = mds" "mem\<^sub>1' = mem\<^sub>1 (x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)"
    using assign_dest
    by blast+
  from assign\<^sub>1 upd have \<C>_eq: "\<forall>x\<in>\<C>. mem\<^sub>1 x = mem\<^sub>1' x"
    by auto
  have dma_eq [simp]: "dma mem\<^sub>1 = dma mem\<^sub>1'"
    using dma_\<C> assign\<^sub>1(3) by simp
  have "mem\<^sub>1 (x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e) =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 (x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)"
  unfolding tyenv_eq_def
  proof(clarify)
    fix v
    assume is_Low': "type_max (to_total \<Gamma> v) (mem\<^sub>1(x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) = Low"
    show "(mem\<^sub>1(x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) v = (mem\<^sub>2(x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)) v"
    proof(cases "v \<in> dom \<Gamma>")
      assume [simp]: "v \<in> dom \<Gamma>"
      then obtain t' where [simp]: "\<Gamma> v = Some t'" by force
      hence [simp]: "(to_total \<Gamma> v) = t'"
        unfolding to_total_def by (auto split: if_splits)
      have "type_max t' mem\<^sub>1 = type_max t' mem\<^sub>1'"
        apply(rule \<C>_eq_type_max_eq)
         using `\<Gamma> v = Some t'` assign\<^sub>1 
         unfolding tyenv_wellformed_def types_wellformed_def
         apply (metis `v \<in> dom \<Gamma>` option.sel)
        using assign\<^sub>1 apply simp
        done
      with is_Low' have is_Low: "type_max (to_total \<Gamma> v) mem\<^sub>1 = Low"
        by simp
      from assign\<^sub>1(2) `v \<in> dom \<Gamma>` have "x \<noteq> v" by auto
      thus ?thesis
        apply simp
        using is_Low assign\<^sub>1 unfolding tyenv_eq_def by auto
    next
      assume "v \<notin> dom \<Gamma>"
      hence [simp]: "\<And>mem. type_max (to_total \<Gamma> v) mem = dma mem v"
        unfolding to_total_def by simp
      with is_Low' have "dma mem\<^sub>1' v = Low" by simp
      with dma_eq have dma_v_Low: "dma mem\<^sub>1 v = Low" by simp
      hence is_Low: "type_max (to_total \<Gamma> v) mem\<^sub>1 = Low" by simp
      show ?thesis
      proof(cases "x = v")
        assume "x \<noteq> v"
        thus ?thesis
          apply simp
          using is_Low assign\<^sub>1 unfolding tyenv_eq_def by blast
      next
        assume "x = v"
        thus ?thesis
          using ev\<^sub>A_eq assign\<^sub>1 dma_v_Low
          apply simp
          apply(rule ev\<^sub>A_eq)
              apply(rule assign\<^sub>1(9))
             apply(rule assign\<^sub>1(10))
            apply(rule assign\<^sub>1(4))
           apply(rule assign\<^sub>1(5))
          using dma_v_Low by simp
      qed
    qed
  qed

  moreover have "tyenv_wellformed mds \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed mds' \<Gamma> \<S> P'"
    using upd tyenv_wellformed_preds_update assign\<^sub>1 by metis
  moreover have "pred P mem\<^sub>1 \<longrightarrow> pred P' mem\<^sub>1'"
    using pred_preds_update assign\<^sub>1 upd by metis

  moreover have "pred P mem\<^sub>2 \<longrightarrow> pred P' (mem\<^sub>2(x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e))"
    using pred_preds_update assign\<^sub>1 upd by metis
    
  moreover have "tyenv_wellformed mds \<Gamma> \<S> P  \<and> tyenv_sec mds \<Gamma> mem\<^sub>1 \<longrightarrow> tyenv_sec mds \<Gamma> mem\<^sub>1'"
    using tyenv_sec_eq[OF \<C>_eq, where \<Gamma>=\<Gamma>]
    unfolding tyenv_wellformed_def by blast

  ultimately have \<R>':
    "\<langle>Stop, mds', mem\<^sub>1 (x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P'\<^esub> \<langle>Stop, mds', mem\<^sub>2 (x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
    apply -
    apply (rule \<R>.intro\<^sub>1, auto simp: assign\<^sub>1 simp del: dma_eq)
    done

  have a: "\<langle>x' \<leftarrow> e, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>Stop, mds', mem\<^sub>2 (x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
  using eval\<^sub>w.simple eval\<^sub>w_simple.assign
  by (simp add: assign\<^sub>1.hyps assign_eval\<^sub>w)

  from \<R>' a show ?case
    using `c\<^sub>1' = Stop` and `mem\<^sub>1' = mem\<^sub>1 (x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)` assign\<^sub>1.hyps
    by blast
next (* assign\<^sub>\<C> *)
  case (assign\<^sub>\<C> x x' \<Gamma> e t P P' \<S> mds)
  hence upd [simp]: "c\<^sub>1' = Stop" "mds' = mds" "mem\<^sub>1' = mem\<^sub>1 (x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)"
    using assign_dest
    by blast+
  have "mem\<^sub>1 (x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e) =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 (x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)"
  unfolding tyenv_eq_def
  proof(clarify)
    fix v
    assume is_Low': "type_max (to_total \<Gamma> v) (mem\<^sub>1(x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) = Low"
    show "(mem\<^sub>1(x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) v = (mem\<^sub>2(x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)) v"
    proof(cases "v \<in> dom \<Gamma>")
      assume in_dom [simp]: "v \<in> dom \<Gamma>"
      then obtain t' where \<Gamma>v [simp]: "\<Gamma> v = Some t'" by force
      hence [simp]: "(to_total \<Gamma> v) = t'"
        unfolding to_total_def by (auto split: if_splits)
      from assign\<^sub>\<C> have x_nin_C: "x \<notin> vars_of_type t'"
        using in_dom \<Gamma>v 
        by (metis option.sel snd_conv)
      have \<Gamma>v_wf: "type_wellformed t'"
        using in_dom \<Gamma>v assign\<^sub>\<C> unfolding tyenv_wellformed_def types_wellformed_def
        by (metis option.sel)
      with x_nin_C have f_eq: "type_max t' mem\<^sub>1 = type_max t' mem\<^sub>1'"
        using vars_of_type_eq_type_max_eq
        by (metis fun_upd_apply upd(3))
      with is_Low' have is_Low: "type_max (to_total \<Gamma> v) mem\<^sub>1 = Low"
        by simp  
      from assign\<^sub>\<C> `v \<in> dom \<Gamma>` have "x \<noteq> v"
        by(auto simp: tyenv_wellformed_def mds_consistent_def)
      thus ?thesis
        apply simp
        using is_Low assign\<^sub>\<C> unfolding tyenv_eq_def by auto
    next
      assume nin_dom: "v \<notin> dom \<Gamma>"
      hence [simp]: "\<And>mem. type_max (to_total \<Gamma> v) mem = dma mem v"
        unfolding to_total_def  by simp
      with is_Low' have "dma mem\<^sub>1' v = Low" by simp
      show ?thesis
      proof(cases "x = v")
        assume "x = v"
        thus ?thesis
          apply simp
          apply(rule ev\<^sub>A_eq')
             apply(rule assign\<^sub>\<C>(10))
            apply(rule assign\<^sub>\<C>(11))
           apply(rule assign\<^sub>\<C>(3))
          by (rule assign\<^sub>\<C>(4))
      next
        assume [simp]: "x \<noteq> v"
        show ?thesis
        proof(cases "x \<in> \<C>_vars v")
          assume in_\<C>_vars: "x \<in> \<C>_vars v"
          hence "v \<notin> \<C>"
            using \<C>_vars_\<C> by auto
          with nin_dom have "v \<notin> snd \<S>"
            using assign\<^sub>\<C>
            by(auto simp: tyenv_wellformed_def mds_consistent_def stable_def)
          with in_\<C>_vars have "L: P \<turnstile> (to_total \<Gamma> v)"
            using assign\<^sub>\<C>(7) by blast
          with assign\<^sub>\<C>.prems have "type_max (to_total \<Gamma> v) mem\<^sub>1 = Low"
            by(auto simp: type_max_def pred_def pred_entailment_def)
          thus ?thesis
            using not_sym[OF `x \<noteq> v`]
            apply simp
            using assign\<^sub>\<C>.prems
            unfolding tyenv_eq_def by auto
        next
          assume "x \<notin> \<C>_vars v"
          with is_Low' have "dma mem\<^sub>1 v = Low"
            using dma_\<C>_vars `\<And>mem. type_max (to_total \<Gamma> v) mem = dma mem v`
            by (metis fun_upd_other)
          thus ?thesis
            using not_sym[OF `x \<noteq> v`]
            apply simp
            using assign\<^sub>\<C>.prems
            unfolding tyenv_eq_def by auto            
        qed
      qed
    qed
  qed

  moreover have "tyenv_wellformed mds \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed mds' \<Gamma> \<S> P'"
    using upd tyenv_wellformed_preds_update assign\<^sub>\<C> by metis
  moreover have "pred P mem\<^sub>1 \<longrightarrow> pred P' mem\<^sub>1'"
    using pred_preds_update assign\<^sub>\<C> upd by metis
  moreover have "pred P mem\<^sub>2 \<longrightarrow> pred P' (mem\<^sub>2(x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e))"
    using pred_preds_update assign\<^sub>\<C> upd by metis
  moreover have "tyenv_wellformed mds \<Gamma> \<S> P \<and> pred P mem\<^sub>1 \<and> tyenv_sec mds \<Gamma> mem\<^sub>1 \<Longrightarrow> tyenv_sec mds' \<Gamma> mem\<^sub>1'"
  proof(clarify)
    fix v t'
    assume wf: "tyenv_wellformed mds \<Gamma> \<S> P"
    assume pred: "pred P mem\<^sub>1"
    hence pred': "pred P' mem\<^sub>1'" using `pred P mem\<^sub>1 \<longrightarrow> pred P' mem\<^sub>1'` by blast
    assume sec: "tyenv_sec mds \<Gamma> mem\<^sub>1"
    assume \<Gamma>v: "\<Gamma> v = Some t'"
    assume readable': "v \<notin> mds' AsmNoReadOrWrite"
    with upd have readable: "v \<notin> mds AsmNoReadOrWrite" by simp
    with wf have "v \<notin> snd \<S>" by(auto simp: tyenv_wellformed_def mds_consistent_def)
    show "type_max (the (\<Gamma> v)) mem\<^sub>1' \<le> dma mem\<^sub>1' v"
    proof(cases "x \<in> \<C>_vars v")
      assume "x \<in> \<C>_vars v"
      with assign\<^sub>\<C>(7) `v \<notin> snd \<S>` have "L: (to_total \<Gamma> v) \<le>:\<^sub>P' (dma_type v)" by blast
      from pred' \<Gamma>v subtype_correct this show ?thesis
        using type_max_dma_type by(auto simp: to_total_def split: if_splits)
    next
      assume "x \<notin> \<C>_vars v"
      hence "\<forall>v'\<in>\<C>_vars v. mem\<^sub>1 v' = mem\<^sub>1' v'"
        using upd by auto
      hence dma_eq: "dma mem\<^sub>1 v = dma mem\<^sub>1' v"
        by(rule dma_\<C>_vars)
      from \<Gamma>v assign\<^sub>\<C>(5) have "x \<notin> vars_of_type t'" by force
      have "type_wellformed t'"
        using wf \<Gamma>v by(force simp: tyenv_wellformed_def types_wellformed_def)
      with `x \<notin> vars_of_type t'` upd have f_eq: "type_max t' mem\<^sub>1 = type_max t' mem\<^sub>1'"
        using vars_of_type_eq_type_max_eq
        by (metis fun_upd_apply)
      from sec \<Gamma>v readable have "type_max t' mem\<^sub>1 \<le> dma mem\<^sub>1 v"
        by auto
      with f_eq dma_eq \<Gamma>v show ?thesis
        by simp
    qed
  qed

  ultimately have \<R>':
    "\<langle>Stop, mds', mem\<^sub>1 (x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P'\<^esub> \<langle>Stop, mds', mem\<^sub>2 (x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
    apply -
    apply (rule \<R>.intro\<^sub>1, auto simp: assign\<^sub>\<C>)
    done

  have a: "\<langle>x' \<leftarrow> e, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>Stop, mds', mem\<^sub>2 (x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
    by (simp add: assign\<^sub>\<C>.hyps assign_eval\<^sub>w)

  from \<R>' a show ?case
    using `c\<^sub>1' = Stop` and `mem\<^sub>1' = mem\<^sub>1 (x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)` assign\<^sub>\<C>.hyps
    by blast
next (* assign\<^sub>2 *)
  case (assign\<^sub>2 x x' \<Gamma> e t \<S> P' P \<Gamma>' mds)
  have upd [simp]: "c\<^sub>1' = Stop" "mds' = mds" "mem\<^sub>1' = mem\<^sub>1 (x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)"
    by (simp add: assign_dest[OF assign\<^sub>2(13)] assign\<^sub>2.hyps)+

  from `x \<in> dom \<Gamma>` `tyenv_wellformed mds \<Gamma> \<S> P`
  have x_nin_\<C>: "x \<notin> \<C>"
    by(auto simp: tyenv_wellformed_def mds_consistent_def)
  hence dma_eq [simp]: "dma mem\<^sub>1' = dma mem\<^sub>1"
    using dma_\<C> assign\<^sub>2
    by auto
    
  let ?\<Gamma>' = "\<Gamma> (x \<mapsto> t)"
  have "\<langle>x' \<leftarrow> e, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>Stop, mds, mem\<^sub>2 (x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
    using assign\<^sub>2
    by (simp add: assign_eval\<^sub>w)
    
  moreover
  have tyenv_eq': "mem\<^sub>1(x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e) =\<^bsub>\<Gamma>(x \<mapsto> t)\<^esub> mem\<^sub>2(x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)"
  unfolding tyenv_eq_def
  proof(clarify)
    fix v
    assume is_Low': "type_max (to_total (\<Gamma>(x \<mapsto> t)) v) (mem\<^sub>1(x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) = Low"
    show "(mem\<^sub>1(x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) v = (mem\<^sub>2(x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)) v"
    proof(cases "v = x")
      assume neq: "v \<noteq> x"
      hence "type_max (to_total \<Gamma> v) mem\<^sub>1 = Low"
      proof(cases "v \<in> dom \<Gamma>")
        assume "v \<in> dom \<Gamma>"
        then obtain t' where [simp]: "\<Gamma> v = Some t'" by force
        hence [simp]: "(to_total \<Gamma> v) = t'"
          unfolding to_total_def by (auto split: if_splits)
        hence [simp]: "(to_total ?\<Gamma>' v) = t'"
          using neq by(auto simp: to_total_def)
        have "type_max t' mem\<^sub>1 = type_max t' mem\<^sub>1'"
          apply(rule \<C>_eq_type_max_eq)
            using assign\<^sub>2.prems 
            apply(clarsimp simp: tyenv_wellformed_def types_wellformed_def)
            using `v \<in> dom \<Gamma>` `\<Gamma> v = Some t'` apply(metis option.sel)
           using x_nin_\<C> by simp
        from this is_Low' neq neq[THEN not_sym] show "type_max (to_total \<Gamma> v) mem\<^sub>1 = Low"
          by auto
      next
        assume "v \<notin> dom \<Gamma>"
        with is_Low' neq
        have "dma mem\<^sub>1' v = Low"
          by(auto simp: to_total_def  split: if_splits)
        with dma_eq `v \<notin> dom \<Gamma>` show ?thesis
          by(auto simp: to_total_def  split: if_splits)
      qed
      with neq assign\<^sub>2.prems show "(mem\<^sub>1(x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) v = (mem\<^sub>2(x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)) v"
        by(auto simp: tyenv_eq_def)
    next
      assume eq[simp]: "v = x"
      with is_Low' `x \<in> dom \<Gamma>` have t_Low': "type_max t mem\<^sub>1' = Low"
        by(auto simp: to_total_def split: if_splits)
      have wf_t: "type_wellformed t"
        using type_aexpr_type_wellformed assign\<^sub>2
        by(fastforce simp: tyenv_wellformed_def)
      with t_Low' `x \<notin> \<C>` have t_Low: "type_max t mem\<^sub>1 = Low"
        using \<C>_eq_type_max_eq
        by (metis (no_types, lifting) fun_upd_other upd(3))
      show ?thesis
      proof(simp, rule eval_vars_det\<^sub>A, clarify)
        fix y
        assume in_vars: "y \<in> aexp_vars e"
        have "type_max (to_total \<Gamma> (Var y)) mem\<^sub>1 = Low"
        proof -
          from t_Low in_vars assign\<^sub>2 show ?thesis
            apply -
            apply(erule type_aexpr.cases)
            using Sec.exhaust by(auto simp: type_max_def  split: if_splits)
        qed
        thus "(to_prog_mem mem\<^sub>1) y = (to_prog_mem mem\<^sub>2) y"
          using assign\<^sub>2 unfolding tyenv_eq_def to_prog_mem_def by blast
      qed
    qed
  qed

  from upd have ty: " \<turnstile> ?\<Gamma>',\<S>,P' {c\<^sub>1'} ?\<Gamma>',\<S>,P'"
    by (metis stop_type)
  have wf: "tyenv_wellformed mds \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed mds' ?\<Gamma>' \<S> P'"
  proof
    assume tyenv_wf: "tyenv_wellformed mds \<Gamma> \<S> P"
    hence wf: "types_wellformed \<Gamma>"
      unfolding tyenv_wellformed_def by blast
    hence  "type_wellformed t"
      using assign\<^sub>2 type_aexpr_type_wellformed
      by blast
    with wf have wf': "types_wellformed ?\<Gamma>'"
      using types_wellformed_update by metis
    from tyenv_wf have stable': "types_stable ?\<Gamma>' \<S>"
      using types_stable_update
            assign\<^sub>2(4)
      unfolding tyenv_wellformed_def by blast
    have m: "mds_consistent mds \<Gamma> \<S> P"
      using tyenv_wf unfolding tyenv_wellformed_def by blast
    from assign\<^sub>2(5) assign\<^sub>2(2)
    have "mds_consistent mds' (\<Gamma>(x \<mapsto> t)) \<S> P'"
      apply(rule mds_consistent_preds_tyenv_update)
      using upd m by simp
    from wf' stable' this show "tyenv_wellformed mds' ?\<Gamma>' \<S> P'"
      unfolding tyenv_wellformed_def by blast
  qed
  have p: "pred P mem\<^sub>1 \<longrightarrow> pred P' mem\<^sub>1'"
    using pred_preds_update assign\<^sub>2 upd by metis
  have p\<^sub>2: "pred P mem\<^sub>2 \<longrightarrow> pred P' (mem\<^sub>2(x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e))"
    using pred_preds_update assign\<^sub>2 upd by metis
  have sec: "tyenv_wellformed mds \<Gamma> \<S> P \<Longrightarrow> pred P mem\<^sub>1 \<Longrightarrow> tyenv_sec mds \<Gamma> mem\<^sub>1 \<Longrightarrow> tyenv_sec mds' ?\<Gamma>' mem\<^sub>1'"
  proof(clarify)
    assume wf: "tyenv_wellformed mds \<Gamma> \<S> P"
    assume pred: "pred P mem\<^sub>1"
    assume sec: "tyenv_sec mds \<Gamma> mem\<^sub>1"
    from pred p have pred': "pred P' mem\<^sub>1'" by blast
    fix v t'
    assume \<Gamma>v: "(\<Gamma>(x \<mapsto> t)) v = Some t'"
    assume "v \<notin> mds' AsmNoReadOrWrite"
    show "type_max (the ((\<Gamma>(x \<mapsto> t)) v)) mem\<^sub>1' \<le> dma mem\<^sub>1' v"
    proof(cases "v = x")
      assume [simp]: "v = x"
      hence [simp]: "(the ((\<Gamma>(x \<mapsto> t)) v)) = t" and t_def: "t = t'"
        using \<Gamma>v by auto
      from `v \<notin> mds' AsmNoReadOrWrite` upd wf have readable: "v \<notin> snd \<S>"
        by(auto simp: tyenv_wellformed_def mds_consistent_def)
      with assign\<^sub>2(6) have "L: t \<le>:\<^sub>P' (dma_type x)" by fastforce
      with pred' show ?thesis
        using type_max_dma_type subtype_correct
        by fastforce
    next
      assume neq: "v \<noteq> x"
      hence [simp]: "((\<Gamma>(x \<mapsto> t)) v) = \<Gamma> v"
        by simp
      with \<Gamma>v have \<Gamma>v: "\<Gamma> v = Some t'" by simp
      with sec upd `v \<notin> mds' AsmNoReadOrWrite` have f_leq: "type_max t' mem\<^sub>1 \<le> dma mem\<^sub>1 v"
        by auto
      have \<C>_eq: "\<forall>x\<in>\<C>. mem\<^sub>1 x = mem\<^sub>1' x"
        using wf assign\<^sub>2 upd by(auto simp: tyenv_wellformed_def mds_consistent_def)
      hence dma_eq: "dma mem\<^sub>1 = dma mem\<^sub>1'"
        by(rule dma_\<C>)
      have f_eq: "type_max t' mem\<^sub>1 = type_max t' mem\<^sub>1'"
        apply(rule \<C>_eq_type_max_eq)
         using \<Gamma>v wf apply(force simp: tyenv_wellformed_def types_wellformed_def)
        by(rule \<C>_eq)     
      from neq \<Gamma>v f_leq dma_eq f_eq show ?thesis
        by simp
    qed
  qed

  have "\<langle>Stop, mds, mem\<^sub>1 (x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)\<rangle> \<R>\<^sup>1\<^bsub>?\<Gamma>',\<S>,P'\<^esub> \<langle>Stop, mds, mem\<^sub>2 (x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
    apply(rule \<R>\<^sub>1.intro)
         apply blast
        using wf assign\<^sub>2 apply fastforce
       apply(rule tyenv_eq')
      using p assign\<^sub>2 apply fastforce
     using p\<^sub>2 assign\<^sub>2 apply fastforce
    using sec assign\<^sub>2
    using upd(2) upd(3) by blast
    
  ultimately have "\<langle>x' \<leftarrow> e, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>Stop, mds', mem\<^sub>2 (x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
    "\<langle>Stop, mds', mem\<^sub>1 (x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>(x \<mapsto> t),\<S>,P'\<^esub> \<langle>Stop, mds', mem\<^sub>2 (x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
    using \<R>.intro\<^sub>1
    by auto
  with assign\<^sub>2.hyps show ?case
    using `mds' = mds` `c\<^sub>1' = Stop` `mem\<^sub>1' = mem\<^sub>1(x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)`
    by blast
next (* if *)
  case (if_type \<Gamma> e t P \<S> th \<Gamma>' \<S>' P' el \<Gamma>'' P'' \<Gamma>''' P''')
  let ?P = "if (ev\<^sub>B (to_prog_mem mem\<^sub>1) e) then P +\<^sub>\<S> e else P +\<^sub>\<S> (bexp_neg e)"
  from `\<langle>Stmt.If e th el, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>` have ty: "\<turnstile> \<Gamma>,\<S>,?P {c\<^sub>1'} \<Gamma>''',\<S>',P'''"
  proof (rule if_elim)
    assume "c\<^sub>1' = th" "mem\<^sub>1' = mem\<^sub>1" "mds' = mds" "ev\<^sub>B (to_prog_mem mem\<^sub>1) e"
    with if_type(3)
    show ?thesis
      apply simp
      apply(erule sub)
         using if_type apply simp+
      done
  next
    assume "c\<^sub>1' = el" "mem\<^sub>1' = mem\<^sub>1" "mds' = mds" "\<not> ev\<^sub>B (to_prog_mem mem\<^sub>1) e"
    with if_type(5)
    show ?thesis
      apply simp
      apply(erule sub)
         using if_type apply simp+
      done
  qed
  have ev\<^sub>B_eq [simp]: "ev\<^sub>B (to_prog_mem mem\<^sub>1) e = ev\<^sub>B (to_prog_mem mem\<^sub>2) e"
    apply(rule ev\<^sub>B_eq')
       apply(rule `mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2`)
      apply(rule `pred P mem\<^sub>1`)
     apply(rule `\<Gamma> \<turnstile>\<^sub>b e \<in> t`)
    by(rule `L: P \<turnstile> t`)
  have "(\<langle>c\<^sub>1', mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>1', mds, mem\<^sub>2\<rangle>) \<in> \<R> \<Gamma>''' \<S>' P'''"
    apply (rule intro\<^sub>1)
    apply clarify
    apply (rule \<R>\<^sub>1.intro [where \<Gamma> = \<Gamma> and \<Gamma>' = \<Gamma>''' and \<S> = \<S> and P = ?P])
         apply(rule ty)
        using `tyenv_wellformed mds \<Gamma> \<S> P`
        apply(auto simp: tyenv_wellformed_def mds_consistent_def add_pred_def)[1]
       apply(rule `mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2`)       
      using `pred P mem\<^sub>1` apply(fastforce simp: pred_def add_pred_def bexp_neg_negates)
     using `pred P mem\<^sub>2` apply(fastforce simp: pred_def add_pred_def bexp_neg_negates)
    by(rule `tyenv_sec mds \<Gamma> mem\<^sub>1`)

  show ?case
  proof -
    from ev\<^sub>B_eq if_type(13) have "(\<langle>If e th el, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>1', mds, mem\<^sub>2\<rangle>)"
      apply (cases "ev\<^sub>B (to_prog_mem mem\<^sub>1) e")
       apply (subgoal_tac "c\<^sub>1' = th")
        apply clarify
        apply (metis eval\<^sub>w_simplep.if_true eval\<^sub>wp.simple eval\<^sub>wp_eval\<^sub>w_eq)
       using if_type.prems(6) apply blast
      apply (subgoal_tac "c\<^sub>1' = el")
       apply (metis (hide_lams, mono_tags) eval\<^sub>w.simple eval\<^sub>w_simple.if_false)
      using if_type.prems(6) by blast
    with `\<langle>c\<^sub>1', mds, mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>''',\<S>',P'''\<^esub> \<langle>c\<^sub>1', mds, mem\<^sub>2\<rangle>` show ?thesis  
    by (metis if_elim if_type.prems(6))    
  qed
next (* while *)
  case (while_type c \<Gamma> e t P \<S>)
  hence [simp]: "c\<^sub>1' = (If e (c ;; While e c) Stop)" and
    [simp]: "mds' = mds" and
    [simp]: "mem\<^sub>1' = mem\<^sub>1"
    by (auto simp: while_dest)

  with while_type have "\<langle>While e c, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>1', mds, mem\<^sub>2\<rangle>"
    by (metis eval\<^sub>w_simplep.while eval\<^sub>wp.simple eval\<^sub>wp_eval\<^sub>w_eq)
    
  moreover have ty: " \<turnstile> \<Gamma>,\<S>,P {c\<^sub>1'} \<Gamma>,\<S>,P"
    apply simp
    apply(rule if_type)
              apply(rule while_type(2))
             apply(rule while_type(3))
            apply(rule seq_type)
              apply(rule while_type(1))
             apply(rule while_type(4))
            apply(rule has_type.while_type)
               apply(rule while_type(1))
              apply(rule while_type(2))
             apply(rule while_type(3))
            apply(rule while_type(4))
           apply(rule stop_type)
          apply simp+
      apply(rule add_pred_entailment)
     apply simp
    apply(blast intro!: add_pred_subset tyenv_wellformed_subset)
    done
  moreover
  have "\<langle>c\<^sub>1', mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>1', mds, mem\<^sub>2\<rangle>"
    apply (rule \<R>\<^sub>1.intro [where \<Gamma> = \<Gamma>])
         apply(rule ty)
        using while_type apply simp+
    done
  hence "\<langle>c\<^sub>1', mds, mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>1', mds, mem\<^sub>2\<rangle>"
    using \<R>.intro\<^sub>1 by auto
  ultimately show ?case
    by fastforce
next
  case (sub \<Gamma>\<^sub>1 \<S> P\<^sub>1 c \<Gamma>\<^sub>1' \<S>' P\<^sub>1' \<Gamma>\<^sub>2 P\<^sub>2 \<Gamma>\<^sub>2' P\<^sub>2' mds c\<^sub>1')
  have imp: "tyenv_wellformed mds \<Gamma>\<^sub>2 \<S> P\<^sub>2 \<and> pred P\<^sub>2 mem\<^sub>1 \<and> pred P\<^sub>2 mem\<^sub>2 \<and> tyenv_sec mds \<Gamma>\<^sub>2 mem\<^sub>1 \<Longrightarrow> 
             tyenv_wellformed mds \<Gamma>\<^sub>1 \<S> P\<^sub>1 \<and> pred P\<^sub>1 mem\<^sub>1 \<and> pred P\<^sub>1 mem\<^sub>2 \<and> tyenv_sec mds \<Gamma>\<^sub>1 mem\<^sub>1"
    apply(rule conjI)
     using sub(5) sub(4) tyenv_wellformed_sub unfolding pred_def
     apply blast
    apply(rule conjI)
     using local.pred_def pred_entailment_def sub.hyps(7) apply auto[1]
    apply(rule conjI)
     using local.pred_def pred_entailment_def sub.hyps(7) apply auto[1]
    using sub(3) context_equiv_tyenv_sec unfolding pred_def by blast

  have tyenv_eq: "mem\<^sub>1 =\<^bsub>\<Gamma>\<^sub>1\<^esub> mem\<^sub>2"
    using context_equiv_tyenv_eq sub by blast
    
  from imp tyenv_eq obtain c\<^sub>2' mem\<^sub>2' where c\<^sub>2'_props: "\<langle>c, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>"
    "\<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>\<^sub>1',\<S>',P\<^sub>1'\<^esub> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>"
    using sub by blast 
  with R_equiv_entailment `L: P\<^sub>1' \<turnstile> P\<^sub>2'` show ?case
    using sub.hyps(6) sub.hyps(5) by blast

next
  case (lock_acq_type upd l \<Gamma>' \<Gamma> \<S> \<S>' P' P)
  hence upd: "(mds' = mds \<and> mem\<^sub>1' = mem\<^sub>1 \<and> c\<^sub>1' = (LockAcq l) \<and> ev\<^sub>L (mem\<^sub>1 (Lock l))) \<or>
    (c\<^sub>1' = Stop \<and> \<not> ev\<^sub>L (mem\<^sub>1 (Lock l)) \<and> ev\<^sub>L (mem\<^sub>1' (Lock l)) \<and> (\<forall>x. x \<noteq> Lock l \<longrightarrow> mem\<^sub>1' x = mem\<^sub>1 x) \<and>
     mds' = update_modes upd mds)"
    by (fastforce simp:eval\<^sub>L_True update_modes_def)
  thus ?case
  proof
    assume noop: "(mds' = mds \<and> mem\<^sub>1' = mem\<^sub>1 \<and> c\<^sub>1' = (LockAcq l) \<and> ev\<^sub>L (mem\<^sub>1 (Lock l)))"
    hence ty: " \<turnstile> \<Gamma>,\<S>,P {LockAcq l} \<Gamma>',\<S>',P'"
      apply(intro has_type.lock_acq_type)
      using lock_acq_type apply simp+
      done
    from noop `mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2`
    have "ev\<^sub>L (mem\<^sub>2 (Lock l))"
      unfolding tyenv_eq_def
      by (simp add: ex_in_conv lock_acq_type.hyps locks_default_dma_low to_total_def type_max_def)
    hence "\<langle>LockAcq l, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>LockAcq l, mds', mem\<^sub>2\<rangle>"
      using lock_spin noop by metis
    moreover have "\<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>LockAcq l, mds', mem\<^sub>2\<rangle>"
      apply(rule \<R>.intros)
      apply(simp add: noop)
      apply(rule \<R>\<^sub>1.intro)
           apply(rule ty)
          using lock_acq_type apply simp+
      done
    ultimately show "\<exists>c\<^sub>2' mem\<^sub>2'.
       \<langle>LockAcq l, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle> \<and>
       \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>"
      using noop by blast
  next
    assume "(c\<^sub>1' = Stop \<and> \<not> ev\<^sub>L (mem\<^sub>1 (Lock l)) \<and> ev\<^sub>L (mem\<^sub>1' (Lock l)) \<and>
      (\<forall>x. x \<noteq> Lock l \<longrightarrow> mem\<^sub>1' x = mem\<^sub>1 x) \<and> mds' = update_modes upd mds)"
    hence upd: "c\<^sub>1' = Stop"
      "\<not> ev\<^sub>L (mem\<^sub>1 (Lock l))" "ev\<^sub>L (mem\<^sub>1' (Lock l))" "\<forall>x. x \<noteq> Lock l \<longrightarrow> mem\<^sub>1' x = mem\<^sub>1 x"
      "mds' = update_modes upd mds"
      by auto
    have "mem\<^sub>1 =\<^bsub>\<Gamma>'\<^esub> mem\<^sub>2"
    proof(clarsimp simp: tyenv_eq_def)
      fix x  
      assume a: "type_max (to_total \<Gamma>' x) mem\<^sub>1 = Low"
      hence "type_max (to_total \<Gamma> x) mem\<^sub>1 = Low"
      proof -
        from `pred P mem\<^sub>1` have "pred P' mem\<^sub>1"
          using lock_acq_type.hyps
          by(auto simp: restrict_preds_to_vars_def pred_def)
        with subtype_correct lock_acq_type.hyps a 
        show ?thesis
          using less_eq_Sec_def by metis
      qed
      thus "mem\<^sub>1 x = mem\<^sub>2 x"
        using lock_acq_type.prems(2)
        unfolding tyenv_eq_def by blast
    qed
    from upd(2) lock_acq_type.prems(2) lock_acq_type.hyps
    have mem\<^sub>2_no_l: "\<not> ev\<^sub>L (mem\<^sub>2 (Lock l))"
      apply clarsimp
      unfolding tyenv_eq_def
      apply(erule_tac x="Lock l" in allE)
      unfolding type_max_def
      apply(clarsimp split:if_splits)
      unfolding to_total_def
      using locks_default_dma_low
      by clarsimp
  
    have "tyenv_wellformed mds \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (update_modes upd mds) \<Gamma>' \<S>' P'"
      using lock_acq_type
      apply auto
      using tyenv_wellformed_mode_update
      by simp
    moreover
    have pred: "pred P mem\<^sub>1 \<longrightarrow> pred P' mem\<^sub>1"
      using lock_acq_type
      by (auto simp: pred_def restrict_preds_to_vars_def)
    moreover
    have "tyenv_wellformed mds \<Gamma> \<S> P \<and> pred P mem\<^sub>1 \<and> tyenv_sec mds \<Gamma> mem\<^sub>1 \<longrightarrow> 
          tyenv_sec (update_modes upd mds) \<Gamma>' mem\<^sub>1"
      apply(rule impI)
      apply(rule tyenv_sec_lock_acq)
               using lock_acq_type apply fastforce
              using lock_acq_type pred apply fastforce
             using lock_acq_type apply fastforce
            using lock_acq_type apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
           using lock_acq_type apply fastforce
          apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
         apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
        using lock_acq_type apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
       apply simp
      using lock_acq_type by simp

    ultimately obtain c\<^sub>2' mem\<^sub>2' where "(\<langle>LockAcq l, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle> \<and>
      \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>)"
      apply (subgoal_tac "\<exists>mem\<^sub>2'. \<langle>LockAcq l, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>Stop, mds', mem\<^sub>2'\<rangle> \<and> \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>Stop, mds', mem\<^sub>2'\<rangle>")
       apply clarsimp
      apply (rule_tac x="mem\<^sub>2(Lock l := mem\<^sub>1' (Lock l))" in exI)
      apply(rule conjI)
       apply (rule lock_acq)
         using mem\<^sub>2_no_l apply clarsimp
        apply (metis (no_types, lifting) fun_upd_apply lock_acq\<^sub>w_elim lock_acq_type.prems(6) upd(2))
       apply (metis (no_types, lifting) lock_acq\<^sub>w_elim lock_acq_type.prems(6) upd(2))
      using upd(1) apply clarsimp
      apply(rule \<R>.intro\<^sub>1)
      apply clarsimp
      apply(rule_tac \<Gamma>=\<Gamma>' and \<S>=\<S>' and P=P' in \<R>\<^sub>1.intro)
           apply(rule stop_type)
          using lock_acq_type upd apply clarsimp
         using upd(4) `mem\<^sub>1 =\<^bsub>\<Gamma>'\<^esub> mem\<^sub>2`
         apply(clarsimp simp:tyenv_eq_def)
         apply(erule_tac x=x in allE)
         apply(erule_tac x=x in allE)
         apply(clarsimp simp:upd(4) type_max_def to_total_def to_prog_mem_def split:if_splits)
        apply (metis Var.distinct(1) eval_vars_det\<^sub>B lock_acq_type(9) pred_def to_prog_mem_def upd(4))
       apply(clarsimp simp:pred_def to_prog_mem_def)
       using eval_vars_det\<^sub>B
       apply (metis (mono_tags, lifting) pred_def lock_acq_type.hyps(4) lock_acq_type.prems(4) mem_Collect_eq restrict_preds_to_vars_def to_prog_mem_def)
      apply clarsimp
      by (metis (no_types, hide_lams) \<C>_eq_type_max_eq dma_\<C> domIff lock_acq_type.prems(1,3,5) no_locks_in_\<C> option.sel option.simps(3) sifum_types.tyenv_wellformed_def sifum_types_axioms types_wellformed_def upd(4,5))

    with upd
    show ?case
      apply clarsimp
      apply (rule_tac x = c\<^sub>2' in exI)
      apply (rule_tac x = mem\<^sub>2' in exI)
      by clarsimp
  qed

next
  case (lock_rel_type upd l \<Gamma>' \<Gamma> \<S> \<S>' P' P)
  hence upd: "(mds' = mds \<and> mem\<^sub>1' = mem\<^sub>1 \<and> c\<^sub>1' = (LockRel l) \<and> \<not> lock_held_mds_correct lock_interp mds l) \<or>
    (c\<^sub>1' = Stop \<and> lock_held_mds_correct lock_interp mds l \<and> \<not> ev\<^sub>L (mem\<^sub>1' (Lock l)) \<and>
     (\<forall>x. x \<noteq> Lock l \<longrightarrow> mem\<^sub>1' x = mem\<^sub>1 x) \<and> mds' = update_modes upd mds)"
    by (fastforce simp:eval\<^sub>L_False update_modes_def)
  thus ?case
  proof
    assume noop: "(mds' = mds \<and> mem\<^sub>1' = mem\<^sub>1 \<and> c\<^sub>1' = (LockRel l) \<and> \<not> lock_held_mds_correct lock_interp mds l)"
    hence ty: " \<turnstile> \<Gamma>,\<S>,P {LockRel l} \<Gamma>',\<S>',P'"
      apply(intro has_type.lock_rel_type)
      using lock_rel_type apply simp+
      done
    with noop have "\<langle>LockRel l, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>LockRel l, mds', mem\<^sub>2\<rangle>"
      using lock_invalid noop by metis
    moreover have "\<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>LockRel l, mds', mem\<^sub>2\<rangle>"
      apply(rule \<R>.intros)
      apply(simp add: noop)
      apply(rule \<R>\<^sub>1.intro)
           apply(rule ty)
          using lock_rel_type apply simp+
      done
    ultimately show "\<exists>c\<^sub>2' mem\<^sub>2'.
       \<langle>LockRel l, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle> \<and>
       \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>"
      using noop by blast
  next
    assume "(c\<^sub>1' = Stop \<and> lock_held_mds_correct lock_interp mds l \<and> \<not> ev\<^sub>L (mem\<^sub>1' (Lock l)) \<and>
      (\<forall>x. x \<noteq> Lock l \<longrightarrow> mem\<^sub>1' x = mem\<^sub>1 x) \<and> mds' = update_modes upd mds)"
    hence upd: "c\<^sub>1' = Stop" "lock_held_mds_correct lock_interp mds l"
      "\<not> ev\<^sub>L (mem\<^sub>1' (Lock l))" "\<forall>x. x \<noteq> Lock l \<longrightarrow> mem\<^sub>1' x = mem\<^sub>1 x"
      "mds' = update_modes upd mds"
      by auto
    have "mem\<^sub>1 =\<^bsub>\<Gamma>'\<^esub> mem\<^sub>2"
    proof(clarsimp simp: tyenv_eq_def)
      fix x  
      assume a: "type_max (to_total \<Gamma>' x) mem\<^sub>1 = Low"
      hence "type_max (to_total \<Gamma> x) mem\<^sub>1 = Low"
      proof -
        from `pred P mem\<^sub>1` have "pred P' mem\<^sub>1"
          using lock_rel_type.hyps
          by(auto simp: restrict_preds_to_vars_def pred_def)
        with subtype_correct lock_rel_type.hyps a 
        show ?thesis
          using less_eq_Sec_def by metis
      qed
      thus "mem\<^sub>1 x = mem\<^sub>2 x"
        using lock_rel_type.prems(2)
        unfolding tyenv_eq_def by blast
    qed
    have "tyenv_wellformed mds \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (update_modes upd mds) \<Gamma>' \<S>' P'"
      using lock_rel_type
      apply auto
      by (metis tyenv_wellformed_mode_update)
    moreover
    have pred: "pred P mem\<^sub>1 \<longrightarrow> pred P' mem\<^sub>1"
      using lock_rel_type
      by (auto simp: pred_def restrict_preds_to_vars_def)
    moreover
    have "tyenv_wellformed mds \<Gamma> \<S> P \<and> pred P mem\<^sub>1 \<and> tyenv_sec mds \<Gamma> mem\<^sub>1 \<longrightarrow> 
          tyenv_sec (update_modes upd mds) \<Gamma>' mem\<^sub>1'"
      apply(rule impI)
      apply(rule tyenv_sec_lock_rel)
                   using lock_rel_type apply fastforce
                  using lock_rel_type pred upd(4)
                  apply (simp add: eval_vars_det\<^sub>B pred_def to_prog_mem_def)
                 using lock_rel_type apply fastforce
                using lock_rel_type apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
               using lock_rel_type apply fastforce
              apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
             apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
            using lock_rel_type apply(fastforce simp: tyenv_wellformed_def mds_consistent_def)
           using lock_rel_type apply fastforce
          using upd lock_rel_type apply simp
         using lock_rel_type apply (metis (no_types, lifting) mds_consistent_def sifum_types.tyenv_wellformed_def sifum_types_axioms)
        using upd lock_rel_type apply simp
       using upd lock_rel_type apply simp
      using upd lock_rel_type apply simp
      done
    ultimately
    obtain c\<^sub>2' mem\<^sub>2' where "(\<langle>LockRel l, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle> \<and>
      \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>)"
      apply (subgoal_tac "\<exists>mem\<^sub>2'. \<langle>LockRel l, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>Stop, mds', mem\<^sub>2'\<rangle> \<and> \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>Stop, mds', mem\<^sub>2'\<rangle>")
       apply clarsimp
      apply (rule_tac x="mem\<^sub>2(Lock l := mem\<^sub>1' (Lock l))" in exI)
      apply(rule conjI)
       apply (rule lock_rel)
         using upd(2) apply clarsimp
        apply (metis (no_types, lifting) fun_upd_apply lock_rel\<^sub>w_elim lock_rel_type.prems(6) upd(2))
       apply (metis (no_types, lifting) lock_rel\<^sub>w_elim lock_rel_type.prems(6) upd(2))
      using upd(1) apply clarsimp
      apply(rule \<R>.intro\<^sub>1)
      apply clarsimp
      apply(rule_tac \<Gamma>=\<Gamma>' and \<S>=\<S>' and P=P' in \<R>\<^sub>1.intro)
           apply(rule stop_type)
          using lock_rel_type upd apply clarsimp
         using upd(4) `mem\<^sub>1 =\<^bsub>\<Gamma>'\<^esub> mem\<^sub>2`
         apply(clarsimp simp:tyenv_eq_def)
         apply(erule_tac x=x in allE)
         apply(erule_tac x=x in allE)
         apply(clarsimp simp:upd(4) type_max_def to_total_def to_prog_mem_def split:if_splits)
        apply (metis Var.distinct(1) eval_vars_det\<^sub>B lock_rel_type(10) pred_def to_prog_mem_def upd(4))
       apply(clarsimp simp:pred_def to_prog_mem_def)
       using eval_vars_det\<^sub>B
       apply (metis (mono_tags, lifting) pred_def lock_rel_type.hyps(4) lock_rel_type.prems(4) mem_Collect_eq restrict_preds_to_vars_def to_prog_mem_def)
      apply clarsimp
      by (metis (no_types, hide_lams) dma_\<C> domIff lock_rel_type.prems(1,3,5) no_locks_in_\<C> option.sel option.simps(3) upd(4,5))
    with upd
    show ?case
      apply (rule_tac x = c\<^sub>2' in exI)
      apply (rule_tac x = mem\<^sub>2' in exI)
      by clarsimp
  qed
qed

lemma is_final_\<R>\<^sub>u_is_final:
  "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<Longrightarrow> is_final c\<^sub>1 \<Longrightarrow> is_final c\<^sub>2"
  by (fastforce dest: bisim_simple_\<R>\<^sub>u)

lemma my_\<R>\<^sub>3_aux_induct [consumes 1, case_names intro\<^sub>1 intro\<^sub>3]: 
  "\<lbrakk>\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>;
   \<And>c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mem\<^sub>2 c \<Gamma>' \<S>' P'. 
     \<lbrakk>\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>; 
      \<turnstile> \<Gamma>,\<S>,P {c} \<Gamma>',\<S>',P'\<rbrakk> \<Longrightarrow> 
    Q (c\<^sub>1 ;; c) mds mem\<^sub>1 \<Gamma>' \<S>' P' (c\<^sub>2 ;; c) mds mem\<^sub>2;
   \<And>c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mem\<^sub>2 c \<Gamma>' \<S>' P'. 
     \<lbrakk>\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>; 
      Q c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mds mem\<^sub>2; 
      \<turnstile> \<Gamma>,\<S>,P {c} \<Gamma>',\<S>',P'\<rbrakk> \<Longrightarrow> 
    Q (c\<^sub>1 ;; c) mds mem\<^sub>1 \<Gamma>' \<S>' P' (c\<^sub>2 ;; c) mds mem\<^sub>2\<rbrakk> \<Longrightarrow> 
  Q c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mds mem\<^sub>2"
using \<R>\<^sub>3_aux.induct[where 
    ?x1.0 = "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>" and
    ?x2.0 = \<Gamma> and
    ?x3.0 = \<S> and
    ?x4.0 = P and
    ?x5.0 = "\<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>" and
    ?P = "\<lambda>ctx\<^sub>1 \<Gamma> \<S> P ctx\<^sub>2. Q (fst (fst ctx\<^sub>1)) (snd (fst ctx\<^sub>1)) (snd ctx\<^sub>1) \<Gamma> \<S> P (fst (fst ctx\<^sub>2)) (snd (fst ctx\<^sub>2)) (snd ctx\<^sub>2)"]
by force

(* We can now show that \<R>\<^sub>1 and \<R>\<^sub>3 are weak bisimulations of \<R>,: *)
lemma \<R>\<^sub>1_weak_bisim:
  "weak_bisim (\<R>\<^sub>1 \<Gamma>' \<S>' P') (\<R> \<Gamma>' \<S>' P')"
  unfolding weak_bisim_def
  apply clarsimp
  apply(erule \<R>\<^sub>1_elim)
  apply(blast intro: \<R>_typed_step)
  done


lemma \<R>_to_\<R>\<^sub>3: "\<lbrakk> \<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> ; \<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P' \<rbrakk> \<Longrightarrow>
  \<langle>c\<^sub>1 ;; c, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2 ;; c, mds, mem\<^sub>2\<rangle>"
  apply (erule \<R>_elim)
  by auto


lemma \<R>\<^sub>3_weak_bisim:
  "weak_bisim (\<R>\<^sub>3 \<Gamma>' \<S>' P') (\<R> \<Gamma>' \<S>' P')"
proof -
  {
    fix c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mem\<^sub>2 c\<^sub>1' mds' mem\<^sub>1'
    assume case3: "(\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>) \<in> \<R>\<^sub>3 \<Gamma>' \<S>' P'"
    assume eval: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>"
    have "\<exists> c\<^sub>2' mem\<^sub>2'. \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle> \<and> \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>"
      using case3 eval
      apply simp
      
    proof (induct arbitrary: c\<^sub>1' rule: \<R>\<^sub>3_aux.induct)
      case (intro\<^sub>1 c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mem\<^sub>2 c \<Gamma>' \<S>' P')
      hence [simp]: "c\<^sub>2 = c\<^sub>1"
        by (metis (lifting) \<R>\<^sub>1_elim)
      thus ?case
      proof (cases "c\<^sub>1 = Stop")
        assume [simp]: "c\<^sub>1 = Stop"
        thus ?thesis
        using intro\<^sub>1.prems by auto
      next
        assume "c\<^sub>1 \<noteq> Stop"
        from intro\<^sub>1
        obtain c\<^sub>1'' where c\<^sub>1''_props: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1'', mds', mem\<^sub>1'\<rangle> \<and> c\<^sub>1' = (if c\<^sub>1'' = Stop then c else (c\<^sub>1'' ;; c))"
          by (metis intro\<^sub>1.prems seq_dest)
        with intro\<^sub>1
        obtain c\<^sub>2'' mem\<^sub>2' where "\<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2'', mds', mem\<^sub>2'\<rangle>" "\<langle>c\<^sub>1'', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2'', mds', mem\<^sub>2'\<rangle>"
          using \<R>\<^sub>1_weak_bisim and weak_bisim_def
          by blast
        thus ?thesis
          using c\<^sub>1''_props
          apply (rule_tac x = "(if c\<^sub>2'' = Stop then c else (c\<^sub>2'' ;; c))" in exI)
          apply (rule_tac x = mem\<^sub>2' in exI)
          apply(clarsimp split:if_splits)
           apply(rule conjI)
            apply clarsimp
            apply(rule conjI)
             using eval\<^sub>w.seq1 eval\<^sub>w.seq2 apply force
            apply(erule \<R>_elim)
             prefer 2
             apply(erule \<R>\<^sub>3_elim)
              apply force
             apply force
            apply clarsimp
            apply(erule \<R>\<^sub>1_elim)
            apply(rule \<R>.intro\<^sub>1)
            apply clarsimp
            apply(rule \<R>\<^sub>1.intro)
                 using intro\<^sub>1(2) apply force
                using stop_cxt apply blast
               using context_equiv_tyenv_eq stop_cxt apply blast
              using pred_entailment_def stop_cxt apply blast
             using pred_entailment_def stop_cxt apply blast
            apply(metis (mono_tags, hide_lams) less_eq_Sec_def sifum_types.context_equiv_def sifum_types.subtype_sound sifum_types.type_equiv_def sifum_types_axioms stop_cxt)
           apply(force simp add: bisim_simple_\<R>\<^sub>1)
          apply(rule conjI)
           apply(force simp add: bisim_simple_\<R>\<^sub>1)
          apply clarsimp
          by (simp add: \<R>_to_\<R>\<^sub>3 \<R>.intro\<^sub>3 eval\<^sub>w.seq1 intro\<^sub>1.hyps(2))
      qed
    next
      case (intro\<^sub>3 c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mem\<^sub>2 c \<Gamma>' \<S>' P')
      thus ?case
        apply (cases "c\<^sub>1 = Stop")
         apply blast
      proof -
        assume "c\<^sub>1 \<noteq> Stop"
        then obtain c\<^sub>1'' where c\<^sub>1''_props: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1'', mds', mem\<^sub>1'\<rangle>" "c\<^sub>1' = (if c\<^sub>1'' = Stop then c else (c\<^sub>1'' ;; c))"
          by (metis intro\<^sub>3.prems seq_dest)
        then obtain c\<^sub>2'' mem\<^sub>2' where "\<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2'', mds', mem\<^sub>2'\<rangle>" "\<langle>c\<^sub>1'', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2'', mds', mem\<^sub>2'\<rangle>"
          using intro\<^sub>3(2) by metis
        thus ?thesis
          using c\<^sub>1''_props
          apply (rule_tac x = "(if c\<^sub>2'' = Stop then c else (c\<^sub>2'' ;; c))" in exI)
          apply (rule_tac x = mem\<^sub>2' in exI)
          apply(clarsimp split:if_splits)
           apply(rule conjI)
            apply clarsimp
            apply(rule conjI)
             using eval\<^sub>w.seq1 eval\<^sub>w.seq2 apply force
            apply(erule \<R>_elim)
             prefer 2
             apply(erule \<R>\<^sub>3_elim)
              apply force
             apply force
            apply clarsimp
            apply(erule \<R>\<^sub>1_elim)
            apply(rule \<R>.intro\<^sub>1)
            apply clarsimp
            apply(rule \<R>\<^sub>1.intro)
                 using intro\<^sub>3(3) apply force
                using stop_cxt apply blast
               using context_equiv_tyenv_eq stop_cxt apply blast
              using pred_entailment_def stop_cxt apply blast
             using pred_entailment_def stop_cxt apply blast
            apply(metis (mono_tags, hide_lams) less_eq_Sec_def sifum_types.context_equiv_def sifum_types.subtype_sound sifum_types.type_equiv_def sifum_types_axioms stop_cxt)
           apply(force simp add: bisim_simple_\<R>\<^sub>1)
          apply(rule conjI)
           apply(force simp add: bisim_simple_\<R>\<^sub>1)
          apply clarsimp
          by (simp add: \<R>_to_\<R>\<^sub>3 \<R>.intro\<^sub>3 eval\<^sub>w.seq1 intro\<^sub>3.hyps(3))
      qed
    qed
  }
  thus ?thesis
    unfolding weak_bisim_def
    by auto
qed

(* Hence \<R> is a bisimulation: *)
lemma \<R>_bisim: "strong_low_bisim_mm (\<R> \<Gamma>' \<S>' P')"
  unfolding strong_low_bisim_mm_def
proof (auto)
  from \<R>_sym show "sym (\<R> \<Gamma>' \<S>' P')" .
next
  from \<R>_closed_glob_consistent show "closed_glob_consistent (\<R> \<Gamma>' \<S>' P')" .
next
  fix c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mem\<^sub>2
  assume "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>"
  thus "mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>l mem\<^sub>2"
    apply (rule \<R>_elim)
    by (auto simp: \<R>\<^sub>1_mem_eq \<R>\<^sub>3_mem_eq)
next
  fix c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mem\<^sub>2 c\<^sub>1' mds' mem\<^sub>1'
  assume eval: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>"
  assume R: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>"
  from R show "\<exists> c\<^sub>2' mem\<^sub>2'. \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle> \<and>
                            \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>"
    apply (rule \<R>_elim)
      apply (insert \<R>\<^sub>1_weak_bisim \<R>\<^sub>3_weak_bisim eval weak_bisim_def)
      apply auto
    done
qed

lemma Typed_in_\<R>:
  assumes typeable: "\<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P'"
  assumes wf: "tyenv_wellformed mds \<Gamma> \<S> P"
  assumes mem_eq: "\<forall> x. type_max (to_total \<Gamma> x) mem\<^sub>1 = Low \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x"
  assumes pred\<^sub>1: "pred P mem\<^sub>1"
  assumes pred\<^sub>2: "pred P mem\<^sub>2"
  assumes tyenv_sec: "tyenv_sec mds \<Gamma> mem\<^sub>1"
  shows "\<langle>c, mds, mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c, mds, mem\<^sub>2\<rangle>"
  apply (rule \<R>.intro\<^sub>1 [of \<Gamma>'])
  apply clarify
  apply (rule \<R>\<^sub>1.intro [of \<Gamma>])
       apply(rule typeable)
      apply(rule wf)
     using mem_eq apply(fastforce simp: tyenv_eq_def)
    using assms by simp+

(* Typing enforcement of consistent branching side-condition *)

lemma has_type_ev\<^sub>B_eq:
  "\<turnstile> \<Gamma>',\<S>',P' {c} \<Gamma>,\<S>,P \<Longrightarrow>
   leftmost_cmd c = Stmt.If e a b \<Longrightarrow>
   mem\<^sub>1 =\<^bsub>\<Gamma>'\<^esub> mem\<^sub>2 \<Longrightarrow>
   pred P' mem\<^sub>1 \<Longrightarrow>
   pred P' mem\<^sub>2 \<Longrightarrow>
   ev\<^sub>B (to_prog_mem mem\<^sub>1) e = ev\<^sub>B (to_prog_mem mem\<^sub>2) e"
  apply(induct rule:has_type.induct, simp+)
       apply(metis ev\<^sub>B_eq')
      apply simp+
    apply(metis context_equiv_tyenv_eq pred_entailment_def)
   apply simp+
  done

lemma bisim_ev\<^sub>B_eq_\<R>\<^sub>1:
  "\<langle>c, mds, mem\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c', mds', mem'\<rangle> \<Longrightarrow> leftmost_cmd c = If e a b \<Longrightarrow>
   ev\<^sub>B (to_prog_mem mem) e = ev\<^sub>B (to_prog_mem mem') e"
  apply(cases rule: \<R>\<^sub>1.cases, simp+)
  using has_type_ev\<^sub>B_eq by blast

lemma bisim_ev\<^sub>B_eq_\<R>\<^sub>3:
  "lc \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> lc' \<Longrightarrow> leftmost_cmd (fst (fst lc)) = If e a b \<Longrightarrow>
   ev\<^sub>B (to_prog_mem (snd lc)) e = ev\<^sub>B (to_prog_mem (snd lc')) e"
  apply(induct rule: \<R>\<^sub>3_aux.induct)
   using bisim_ev\<^sub>B_eq_\<R>\<^sub>1 apply force
  by simp

lemma bisim_ev\<^sub>B_eq_\<R>\<^sub>u':
  "lc \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> lc' \<Longrightarrow> leftmost_cmd (fst (fst lc)) = If e a b \<Longrightarrow>
   ev\<^sub>B (to_prog_mem (snd lc)) e = ev\<^sub>B (to_prog_mem (snd lc')) e"
  apply(induct rule: \<R>.induct)
   apply clarsimp
   using bisim_ev\<^sub>B_eq_\<R>\<^sub>1 apply blast
  apply(cases rule: \<R>\<^sub>3_aux.cases, simp+)
   using bisim_ev\<^sub>B_eq_\<R>\<^sub>1 apply force
  using bisim_ev\<^sub>B_eq_\<R>\<^sub>3 by force

lemma bisim_ev\<^sub>B_eq_\<R>\<^sub>u:
  "bisim_ev\<^sub>B_eq (\<R> \<Gamma> \<S> P)"
  by (force simp:bisim_ev\<^sub>B_eq_def dest:bisim_ev\<^sub>B_eq_\<R>\<^sub>u')

lemma bisim_simple\<^sub>w_\<R>\<^sub>u:
  "bisim_simple\<^sub>w (\<R> \<Gamma> \<S> P)"
  by (force simp:bisim_simple\<^sub>w_def dest:bisim_simple_\<R>\<^sub>u)

lemma bisim_simple_ev\<^sub>B_eq_\<R>\<^sub>u:
  "bisim_simple_ev\<^sub>B_eq (\<R> \<Gamma> \<S> P)"
  by (force simp:bisim_simple_ev\<^sub>B_eq_def bisim_ev\<^sub>B_eq_\<R>\<^sub>u bisim_simple\<^sub>w_\<R>\<^sub>u)

(* We then prove the main soundness theorem using the fact that typeable
    configurations can be related using \<R>\<^sub>1 *)
theorem type_soundness:
  assumes well_typed: "\<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P'"
  assumes wf: "tyenv_wellformed mds \<Gamma> \<S> P"
  assumes mem_eq: "\<forall> x. type_max (to_total \<Gamma> x) mem\<^sub>1 = Low \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x"
  assumes pred\<^sub>1: "pred P mem\<^sub>1"
  assumes pred\<^sub>2: "pred P mem\<^sub>2"
  assumes tyenv_sec: "tyenv_sec mds \<Gamma> mem\<^sub>1"
  shows "\<langle>c, mds, mem\<^sub>1\<rangle> \<approx> \<langle>c, mds, mem\<^sub>2\<rangle>"
  using \<R>_bisim Typed_in_\<R>
  by (metis assms mem_eq mm_equiv.simps well_typed bisim_simple_ev\<^sub>B_eq_\<R>\<^sub>u)

(* FIXME: Is there a nicer way to express this so the result looks like [blah \<mapsto> a, asdf \<mapsto> b, ...]? *)
definition
  \<Gamma>_of_mds :: "('Lock, 'Var) Var Mds \<Rightarrow> (('Lock, 'Var) Var,'BExp) TyEnv"
where
  "\<Gamma>_of_mds mds \<equiv> (\<lambda>x. if x \<notin> \<C> \<and> x \<in> mds AsmNoWrite \<union> mds AsmNoReadOrWrite then
                         if x \<in> mds AsmNoReadOrWrite then 
                           Some ({pred_False}) 
                         else
                           Some (dma_type x) 
                       else None)"

definition
  \<S>_of_mds :: "('Lock, 'Var) Var Mds \<Rightarrow> ('Lock, 'Var) Var Stable"
where
  "\<S>_of_mds mds \<equiv> (mds AsmNoWrite, mds AsmNoReadOrWrite)"

definition
  mds_yields_stable_types :: "('Lock, 'Var) Var Mds \<Rightarrow> bool"
where
  "mds_yields_stable_types mds \<equiv> \<forall>x. x \<in> mds AsmNoWrite \<union> mds AsmNoReadOrWrite \<longrightarrow>
                                     (\<forall>v\<in>\<C>_vars x. v \<in> mds AsmNoWrite \<union> mds AsmNoReadOrWrite)"
  
(* The typing relation for lists of commands ("thread pools"). *)
inductive type_global :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt \<times> ('Lock, 'Var) Var Mds) list \<Rightarrow> bool"
  ("\<turnstile> _" [120] 1000)
  where
  "\<lbrakk> list_all (\<lambda> (c,m). (\<exists> \<Gamma>' \<S>' P'. \<turnstile> (\<Gamma>_of_mds m),(\<S>_of_mds m),{} { c } \<Gamma>',\<S>',P') \<and> mds_yields_stable_types m) cs ;
       \<forall> mem. sound_mode_use (cs, mem)
    \<rbrakk> \<Longrightarrow>
    type_global cs"

                                     
inductive_cases type_global_elim: "\<turnstile> cs"

lemma of_mds_tyenv_wellformed: "mds_yields_stable_types m \<Longrightarrow> tyenv_wellformed m (\<Gamma>_of_mds m) (\<S>_of_mds m) {}"
  apply(fastforce simp: tyenv_wellformed_def \<Gamma>_of_mds_def \<S>_of_mds_def mds_consistent_def stable_def
                   types_wellformed_def types_stable_def  mds_yields_stable_types_def 
                   type_wellformed_def dma_\<C>_vars \<C>_def bexp_vars_pred_False \<C>_vars_correct
             split: if_splits)
  done

lemma \<Gamma>_of_mds_tyenv_sec:
  "tyenv_sec m (\<Gamma>_of_mds m) mem\<^sub>1"
  apply(auto simp: \<Gamma>_of_mds_def)
  done

lemma type_max_pred_False [simp]:
  "type_max {pred_False} mem = High"
  apply(simp add: type_max_def pred_False_is_False)
  done
  
lemma typed_secure:
  "\<lbrakk> \<turnstile> (\<Gamma>_of_mds m),(\<S>_of_mds m),{} { c } \<Gamma>',\<S>',P'; mds_yields_stable_types m \<rbrakk> \<Longrightarrow> com_sifum_secure (c,m)"
  apply (clarsimp simp: com_sifum_secure_def low_indistinguishable_def)
  apply (erule type_soundness)
      apply(erule of_mds_tyenv_wellformed)
     apply(auto simp: to_total_def split: if_split simp: \<Gamma>_of_mds_def low_mds_eq_def)[1]
    apply(fastforce simp: pred_def type_max_def)
   apply(fastforce simp: pred_def)
  by(rule \<Gamma>_of_mds_tyenv_sec)

lemma list_all_set: "\<forall> x \<in> set xs. P x \<Longrightarrow> list_all P xs"
  by (metis (lifting) list_all_iff)

theorem type_soundness_global:
  assumes typeable: "\<turnstile> cs"
  shows "prog_sifum_secure_cont cs"
  using typeable
  apply (rule type_global_elim)
  apply (subgoal_tac "\<forall> c \<in> set cs. com_sifum_secure c")
   apply(rule sifum_compositionality_cont)
    using list_all_set apply fastforce
   apply fastforce
  apply(drule list_all_iff[THEN iffD1])
  apply clarsimp
  apply(rename_tac c m)
  apply(drule_tac x="(c,m)" in bspec)
   apply assumption
  apply clarsimp
  using typed_secure by blast

end
end