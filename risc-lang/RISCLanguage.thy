(*
Author: Robert Sison
*)

section {* RISC Language for Instantiating the Dependent SIFUM-Security Property *}

theory RISCLanguage
imports "../old-rg-sec/LockVars"
begin

subsection {* Syntax *}

type_synonym Lab = nat
(* Just to make the intended API of fresh_label a little bit more apparent. *)
type_synonym LabRec = nat

(* In the work we're referencing
  [Del Tedesco, Sands, Russo 2014 - http://arxiv.org/abs/1410.4917v2] *)
(* NB: is the "B" short for the binary encoding of the instruction? *)
datatype ('reg, 'var, 'lock, 'wrd, 'op) InstB =
    LockAcq "'lock"
  | LockRel "'lock"
  | Load "'reg" "'var"
  | Store "'var" "'reg"
  | Jmp Lab
  | Jz Lab "'reg"
  | Nop
  (* Tau is just a (possibly badly named) decoration for Nop to indicate it should not be
    associated with any step in the abstract program. Otherwise, same semantics as Nop.
    This could be viewed as a kludge if we find all compiler-introduced Nops are unnecessary. *)
  | Tau
  | MoveK "'reg" "'wrd"
  | MoveR "'reg" "'reg"
  | Op "'op" "'reg" "'reg"
  | Out "Sec" "'reg"

type_synonym ('reg, 'var, 'lock, 'wrd, 'op) Inst =
  "Lab option \<times> ('reg, 'var, 'lock, 'wrd, 'op) InstB"

(* The program counter and code memory are assumed to be fault-tolerant.
  So we don't want to include them in the memory considered subject to havoc
  by the security framework. *)

(* The parameters to this language will be:
  'wrd: word type (also the range of addressable memory)
  'reg: register set (registers are of width 'wrd)
  'var: variable identifiers
  'lock: lock identifiers *)
locale risc_lang_no_dma =
  fixes bin_op :: "'OpId \<Rightarrow> 'Wrd::zero \<Rightarrow> 'Wrd \<Rightarrow> 'Wrd"
  fixes some_reg :: "'Reg"
  fixes some_var :: "'Var"
  fixes some_lock :: "'Lock"
  fixes eval\<^sub>L :: "'Wrd \<Rightarrow> bool"
  fixes lock_interp :: "'Lock \<Rightarrow> ('Var set \<times> 'Var set)"
  fixes lock_val_True :: "'Wrd"
  fixes lock_val_False :: "'Wrd"
  assumes Var_finite : "finite {(x :: ('Lock, 'Var) Var). True}"
  assumes eval\<^sub>L_True: "eval\<^sub>L lock_val_True"
  assumes eval\<^sub>L_False: "\<not> eval\<^sub>L lock_val_False"

(* To what extent do we need this? *)
locale risc_lang = risc_lang_no_dma bin_op
  for bin_op :: "'OpId \<Rightarrow> 'Wrd::zero \<Rightarrow> 'Wrd \<Rightarrow> 'Wrd" +
  (* In [TedescoSR14] both registers and variables can be designated as High or Low containers,
     and they are both considered part of the fault-prone subset of the system state. *)
  fixes dma :: "('Lock, 'Var) Var \<Rightarrow> Sec"

context risc_lang_no_dma
begin

definition
  no_locks_acquired :: "(('Lock, 'Var) Var, 'Wrd) Mem \<Rightarrow> bool"
where
  "no_locks_acquired mem \<equiv> \<forall>l. \<not> eval\<^sub>L (mem (Lock l))"

type_synonym ('reg, 'var, 'lock, 'wrd, 'op) PMem =
  "('reg, 'var, 'lock, 'wrd, 'op) Inst list"

(* The current command can be determined from the current value of the program counter
  in combination with a list of instructions (the program memory itself)
  assuming that the program memory is fault-tolerant. *)
type_synonym ('reg, 'var, 'lock, 'wrd, 'op) PMCmd =
  "nat \<times> ('reg, 'var, 'lock, 'wrd, 'op) PMem"

type_synonym ('reg, 'var, 'lock, 'wrd, 'op) PerThread =
  "('reg, 'var, 'lock, 'wrd, 'op) PMCmd \<times> ('reg, 'wrd) Mem"

abbreviation conf\<^sub>r_abv :: "('Reg, 'Var, 'Lock, 'Wrd, 'OpId) PMCmd \<Rightarrow> ('Reg, 'Wrd) Mem \<Rightarrow>
  ('Lock, 'Var) Var Mds \<Rightarrow> (('Lock, 'Var) Var, 'Wrd) Mem \<Rightarrow>
  (('Reg, 'Var, 'Lock, 'Wrd, 'OpId) PerThread, ('Lock, 'Var) Var, 'Wrd) LocalConf"
  ("\<langle>_, _, _, _\<rangle>\<^sub>r" [0, 120, 120, 120] 100)
  where
  "\<langle> c, regs, mds, mem \<rangle>\<^sub>r \<equiv> (((c, regs), mds), mem)"

definition fresh_label :: "LabRec \<Rightarrow> Lab \<times> LabRec"
where
  "fresh_label l \<equiv> (l, Suc l)"

(* Find the index of the (first) instruction with this label, if it exists. *)
definition res\<^sub>P' :: "Lab \<Rightarrow> ('reg, 'var, 'lock, 'wrd, 'op) PMem \<Rightarrow> nat option"
where
  "res\<^sub>P' l P \<equiv> Option.bind (List.extract (\<lambda>x. fst x = Some l) P) (\<lambda>P'. Some (length (fst P')))"

(* If it doesn't exist, return the length of the program. *)
definition res\<^sub>P :: "Lab \<Rightarrow> ('reg, 'var, 'lock, 'wrd, 'op) PMem \<Rightarrow> nat"
where
  "res\<^sub>P l P \<equiv> case res\<^sub>P' l P of
    Some i \<Rightarrow> i |
    None \<Rightarrow> length P"

subsection {* Semantics *}

(* This is more like eval\<^sub>w_simple, because I intend not to have this make any changes
  to mds (mainly because we haven't even started talking about mode annotations yet),
  but I'd prefer to open that can of worms when we get there. *)
inductive_set eval\<^sub>r :: "(('Reg, 'Var, 'Lock, 'Wrd, 'OpId) PerThread, ('Lock, 'Var) Var, 'Wrd) LocalConf rel"
and eval\<^sub>r_abv :: "(('Reg, 'Var, 'Lock, 'Wrd, 'OpId) PerThread, ('Lock, 'Var) Var, 'Wrd) LocalConf \<Rightarrow>
                  (('Reg, 'Var, 'Lock, 'Wrd, 'OpId) PerThread, ('Lock, 'Var) Var, 'Wrd) LocalConf \<Rightarrow> bool"
  (infixr "\<leadsto>\<^sub>r" 60)
where
  "c \<leadsto>\<^sub>r c' \<equiv> (c, c') \<in> eval\<^sub>r" |
  lock_spin: "\<lbrakk> pc < length P; snd (P ! pc) = LockAcq l; eval\<^sub>L (mem (Lock l)) \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  lock_acq: "\<lbrakk> pc < length P; snd (P ! pc) = LockAcq l; \<not> eval\<^sub>L (mem (Lock l)) \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc + 1, P), regs, lock_acq_upd lock_interp l mds, mem((Lock l) := lock_val_True)\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  lock_invalid: "\<lbrakk> pc < length P; snd (P ! pc) = LockRel l; \<not> lock_held_mds_correct lock_interp mds l \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  lock_rel: "\<lbrakk> pc < length P; snd (P ! pc) = LockRel l; lock_held_mds_correct lock_interp mds l \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc + 1, P), regs, lock_rel_upd lock_interp l mds, mem((Lock l) := lock_val_False)\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  load: "\<lbrakk> pc < length P; snd (P ! pc) = Load r v \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc + 1, P), regs(r := mem (Var v)), mds, mem\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  store: "\<lbrakk> pc < length P; snd (P ! pc) = Store v r \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc + 1, P), regs, mds, mem(Var v := regs r)\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  jmp: "\<lbrakk> pc < length P; snd (P ! pc) = Jmp l; res\<^sub>P l P = pc' \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc', P), regs, mds, mem\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  jz_s: "\<lbrakk> pc < length P; snd (P ! pc) = Jz l r; res\<^sub>P l P = pc'; regs r = 0\<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc', P), regs, mds, mem\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  jz_f: "\<lbrakk> pc < length P; snd (P ! pc) = Jz l r; regs r \<noteq> 0 \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc + 1, P), regs, mds, mem\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  nop: "\<lbrakk> pc < length P; snd (P ! pc) = Nop \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc + 1, P), regs, mds, mem\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  tau: "\<lbrakk> pc < length P; snd (P ! pc) = Tau \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc + 1, P), regs, mds, mem\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  move_k: "\<lbrakk> pc < length P; snd (P ! pc) = MoveK r n \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc + 1, P), regs(r := n), mds, mem\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  move_r: "\<lbrakk> pc < length P; snd (P ! pc) = MoveR r r' \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc + 1, P), regs(r := regs r'), mds, mem\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  op: "\<lbrakk> pc < length P; snd (P ! pc) = Op opid r r' \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc + 1, P), regs(r := bin_op opid (regs r) (regs r')), mds, mem\<rangle>\<^sub>r) \<in> eval\<^sub>r" |
  out: "\<lbrakk> pc < length P; snd (P ! pc) = Out ch r \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc + 1, P), regs, mds, mem\<rangle>\<^sub>r) \<in> eval\<^sub>r"

(* Slightly more usable intro rules *)
lemma lock_acq':
  "\<lbrakk> pc < length P; snd (P ! pc) = LockAcq l; \<not> eval\<^sub>L (mem (Lock l));
     pc' = pc + 1; mds' = lock_acq_upd lock_interp l mds; mem' = mem((Lock l) := lock_val_True) \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc', P), regs, mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r"
  using lock_acq by blast

lemma lock_rel':
  "\<lbrakk> pc < length P; snd (P ! pc) = LockRel l; lock_held_mds_correct lock_interp mds l;
     pc' = pc + 1; mds' = lock_rel_upd lock_interp l mds; mem' = mem((Lock l) := lock_val_False) \<rbrakk> \<Longrightarrow>
    (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r,
     \<langle>(pc', P), regs, mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r"
  using lock_rel by blast

(* "invariant" that lock mds are untouched *)
lemma eval_lock_mds_untouched:
  "\<lbrakk>\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r \<rbrakk> \<Longrightarrow> \<forall>l m. Lock l \<in> mds m \<longleftrightarrow> Lock l \<in> mds' m"
  using lock_acq_upd_def[where lock_interp=lock_interp] lock_rel_upd_def[where lock_interp=lock_interp]
  apply -
  apply (induct rule:eval\<^sub>r.induct, clarsimp+)
   unfolding image_def
   apply(case_tac m, (clarsimp split:prod.split)+)
  apply(case_tac m, (clarsimp split:prod.split)+)
  done

lemma eval_lock_mds_untouched':
  "\<lbrakk>((cms, mem), (cms', mem')) \<in> eval\<^sub>r \<rbrakk> \<Longrightarrow> \<forall>l m. Lock l \<in> snd cms m \<longleftrightarrow> Lock l \<in> snd cms' m"
  apply(rule_tac pc="fst (fst (fst cms))" and pc'="fst (fst (fst cms'))" and
    P="snd (fst (fst cms))" and P'="snd (fst (fst cms'))" and
    regs="snd (fst cms)" and regs'="snd (fst cms')" and
    mds="snd cms" and mds'="snd cms'" in eval_lock_mds_untouched)
  by simp

lemma eval\<^sub>r_preserves_no_lock_mds:
  "\<langle>(pc, P), regs, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P'), regs', mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>r \<Longrightarrow>
   no_lock_mds mds\<^sub>C \<Longrightarrow>
   no_lock_mds mds\<^sub>C'"
  apply(clarsimp simp:no_lock_mds_def)
  using eval_lock_mds_untouched by blast

inductive_cases eval\<^sub>r_elim: "(\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r"

lemma lock_spin_dest [dest]:
  "snd (P ! pc) = LockAcq l \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   eval\<^sub>L (mem (Lock l)) \<Longrightarrow>
   pc = pc' \<and> P = P' \<and> regs = regs' \<and> mds = mds' \<and> mem = mem'"
  by (fastforce elim: eval\<^sub>r_elim)

lemma lock_acq_dest [dest]:
  "snd (P ! pc) = LockAcq l \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   \<not> eval\<^sub>L (mem (Lock l)) \<Longrightarrow>
   pc' = pc + 1 \<and> P = P' \<and> regs = regs' \<and> mds' = lock_acq_upd lock_interp l mds \<and> mem' = mem((Lock l) := lock_val_True)"
  by (fastforce elim: eval\<^sub>r_elim)

lemma lock_acq_elim [elim]:
  "\<And> Q.
    \<lbrakk> snd (P ! pc) = LockAcq l; (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r ;
     \<lbrakk> pc = pc'; P = P'; regs = regs'; mem = mem'; mds = mds'; eval\<^sub>L (mem (Lock l)) \<rbrakk> \<Longrightarrow> Q ;
     \<lbrakk> pc' = pc + 1; P = P'; regs = regs'; mem' = mem((Lock l) := lock_val_True);
       mds' = lock_acq_upd lock_interp l mds; \<not> eval\<^sub>L (mem (Lock l)) \<rbrakk> \<Longrightarrow> Q \<rbrakk> \<Longrightarrow> Q"
  by (blast elim:eval\<^sub>r_elim)

lemma lock_abort_dest [dest]:
  "snd (P ! pc) = LockRel l \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   \<not> lock_held_mds_correct lock_interp mds l \<Longrightarrow>
   pc' = pc \<and> P = P' \<and> regs = regs' \<and> mds = mds' \<and> mem' = mem"
  by (fastforce elim: eval\<^sub>r_elim)

lemma lock_rel_dest [dest]:
  "snd (P ! pc) = LockRel l \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   lock_held_mds_correct lock_interp mds l \<Longrightarrow>
   pc' = pc + 1 \<and> P = P' \<and> regs = regs' \<and> mds' = lock_rel_upd lock_interp l mds \<and> mem' = mem((Lock l) := lock_val_False)"
  by (fastforce elim: eval\<^sub>r_elim)

lemma lock_rel_elim [elim]:
  "\<And> Q.
    \<lbrakk> snd (P ! pc) = LockRel l; (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r ;
     \<lbrakk> pc = pc'; P = P'; regs = regs'; mem = mem'; mds = mds'; \<not> lock_held_mds_correct lock_interp mds l \<rbrakk> \<Longrightarrow> Q ;
     \<lbrakk> pc' = pc + 1; P = P'; regs = regs'; mem' = mem((Lock l) := lock_val_False);
       mds' = lock_rel_upd lock_interp l mds; lock_held_mds_correct lock_interp mds l \<rbrakk> \<Longrightarrow> Q \<rbrakk> \<Longrightarrow> Q"
  by (blast elim:eval\<^sub>r_elim)

lemma load_dest [dest]:
  "snd (P ! pc) = Load r v \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   pc' = pc + 1 \<and> P = P' \<and> regs' = regs(r := mem (Var v)) \<and> mds = mds' \<and> mem' = mem"
  by (fastforce elim: eval\<^sub>r_elim)

lemma store_dest [dest]:
  "snd (P ! pc) = Store v r \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   pc' = pc + 1 \<and> P = P' \<and> regs = regs' \<and> mds = mds' \<and> mem' = mem(Var v := regs r)"
  by (fastforce elim: eval\<^sub>r_elim)

lemma jmp_dest [dest]:
  "snd (P ! pc) = Jmp l \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   res\<^sub>P l P = pc' \<and> P = P' \<and> regs = regs' \<and> mds = mds' \<and> mem = mem'"
  by (fastforce elim: eval\<^sub>r_elim)

lemma jz_s_dest [dest]:
  "snd (P ! pc) = Jz l r \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   regs r = 0 \<Longrightarrow>
   res\<^sub>P l P = pc' \<and> P = P' \<and> regs = regs' \<and> mds = mds' \<and> mem = mem'"
  by (fastforce elim: eval\<^sub>r_elim)

lemma jz_f_dest [dest]:
  "snd (P ! pc) = Jz l r \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   regs r \<noteq> 0 \<Longrightarrow>
   pc' = pc + 1 \<and> P = P' \<and> regs = regs' \<and> mds = mds' \<and> mem = mem'"
  by (fastforce elim: eval\<^sub>r_elim)

lemma jz_elim [elim]:
  "\<And> Q.
    \<lbrakk> snd (P ! pc) = Jz l r; (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r ;
     \<lbrakk> pc' = pc + 1; P = P'; regs = regs'; mem = mem'; mds = mds'; regs r \<noteq> 0 \<rbrakk> \<Longrightarrow> Q ;
     \<lbrakk> pc' = res\<^sub>P l P; P = P'; regs = regs'; mem = mem'; mds = mds'; regs r = 0 \<rbrakk> \<Longrightarrow> Q \<rbrakk> \<Longrightarrow> Q"
  by (blast elim:eval\<^sub>r_elim)

lemma nop_dest [dest]:
  "snd (P ! pc) = Nop \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   pc' = pc + 1 \<and> P = P' \<and> regs = regs' \<and> mds = mds' \<and> mem = mem'"
  by (fastforce elim: eval\<^sub>r_elim)

lemma tau_dest [dest]:
  "snd (P ! pc) = Tau \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   pc' = pc + 1 \<and> P = P' \<and> regs = regs' \<and> mds = mds' \<and> mem = mem'"
  by (fastforce elim: eval\<^sub>r_elim)

lemma move_k_dest [dest]:
  "snd (P ! pc) = MoveK r n \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   pc' = pc + 1 \<and> P = P' \<and> regs' = regs(r := n) \<and> mds = mds' \<and> mem = mem'"
  by (fastforce elim: eval\<^sub>r_elim)

lemma move_r_dest [dest]:
  "snd (P ! pc) = MoveR r r' \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   pc' = pc + 1 \<and> P = P' \<and> regs' = regs(r := regs r') \<and> mds = mds' \<and> mem = mem'"
  by (fastforce elim: eval\<^sub>r_elim)

lemma op_dest [dest]:
  "snd (P ! pc) = Op opid r r' \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   pc' = pc + 1 \<and> P = P' \<and> regs' = regs(r := bin_op opid (regs r) (regs r')) \<and> mds = mds' \<and> mem = mem'"
  by (fastforce elim: eval\<^sub>r_elim)

lemma out_dest [dest]:
  "snd (P ! pc) = Out ch r \<Longrightarrow> (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   pc' = pc + 1 \<and> P = P' \<and> regs = regs' \<and> mds = mds' \<and> mem = mem'"
  by (fastforce elim: eval\<^sub>r_elim)

lemma eval\<^sub>r_oob_pc_halt:
  "pc \<ge> length P \<Longrightarrow> \<not> \<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r"
  by (clarsimp simp:eval\<^sub>r.simps)

lemma eval\<^sub>r_halt_oob_pc:
  "\<forall> pc' P' regs' mds' mem'. \<not> \<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   pc \<ge> length P"
  apply(erule rev_notE)
  apply clarsimp
  apply(case_tac "snd (P ! pc)")
             apply(clarsimp simp:eval\<^sub>r.simps)
             apply metis
            apply(clarsimp simp:eval\<^sub>r.simps)
            apply metis
           apply(clarsimp simp:eval\<^sub>r.simps)+
        apply metis
       apply(clarsimp simp:eval\<^sub>r.simps)+
  done

lemma eval\<^sub>r_halt_iff_oob:
  "\<forall> pc' P' regs' mds' mem'. \<not> \<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r \<equiv> pc \<ge> length P"
  apply(intro eq_reflection)
  using eval\<^sub>r_oob_pc_halt eval\<^sub>r_halt_oob_pc
  by meson

lemma eval\<^sub>r_no_modify:
  "\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow> P = P'"
  by (blast dest: eval\<^sub>r_elim)

definition present_labels :: "('reg, 'var, 'lock, 'wrd, 'op) PMem \<Rightarrow> Lab set"
where
  "present_labels P \<equiv> {l. \<exists> pc. pc < length P \<and> fst (P ! pc) = Some l}"

definition jump_labels :: "('reg, 'var, 'lock, 'wrd, 'op) PMem \<Rightarrow> Lab set"
where
  "jump_labels P \<equiv> {l. \<exists> pc r. pc < length P \<and> snd (P ! pc) \<in> {Jmp l, Jz l r}}"

definition oob_jump_labels :: "('reg, 'var, 'lock, 'wrd, 'op) PMem \<Rightarrow> Lab set"
where
  "oob_jump_labels P \<equiv> {l. l \<in> jump_labels P \<and> res\<^sub>P l P = length P}"

definition joinable_forward :: "('reg, 'var, 'lock, 'wrd, 'op) PMem \<Rightarrow> ('reg, 'var, 'lock, 'wrd, 'op) PMem \<Rightarrow> bool"
where
  (* All of P\<^sub>1's exit labels must point to the start of P\<^sub>2 *)
  "joinable_forward P\<^sub>1 P\<^sub>2 \<equiv> (\<forall>l \<in> oob_jump_labels P\<^sub>1. (fst (P\<^sub>2 ! 0)) = Some l)"

definition joinable_backward :: "('reg, 'var, 'lock, 'wrd, 'op) PMem \<Rightarrow> ('reg, 'var, 'lock, 'wrd, 'op) PMem \<Rightarrow> bool"
where
  (* None of P\<^sub>2's labels may be present in P\<^sub>1 *)
  "joinable_backward P\<^sub>1 P\<^sub>2 \<equiv> (\<forall>l \<in> jump_labels P\<^sub>2. l \<notin> present_labels P\<^sub>1)"

definition joinable :: "('reg, 'var, 'lock, 'wrd, 'op) PMem \<Rightarrow> ('reg, 'var, 'lock, 'wrd, 'op) PMem \<Rightarrow> bool"
where
  "joinable P\<^sub>1 P\<^sub>2 \<equiv> joinable_forward P\<^sub>1 P\<^sub>2 \<and> joinable_backward P\<^sub>1 P\<^sub>2"

(* Labels will always resolve to a pc that is in or right after the end of P *)
lemma res\<^sub>P_bounds: "res\<^sub>P l P = pc \<Longrightarrow> pc \<le> length P"
  unfolding res\<^sub>P_def res\<^sub>P'_def Option.bind_def
  apply(case_tac "List.extract (\<lambda>x. fst x = Some l) P")
   apply clarsimp
  by (clarsimp simp add: extract_Some_iff)

lemma res\<^sub>P_oob:
  "\<not> res\<^sub>P l P < length P \<equiv> res\<^sub>P l P = length P"
  apply(rule eq_reflection)
  apply(rule iffI)
   using res\<^sub>P_bounds le_neq_implies_less apply blast
  using res\<^sub>P_bounds by simp

(* If a label resolves to a pc in P, then the instruction at that pc is labelled by that label *)
lemma res\<^sub>P_hits: "res\<^sub>P l P = pc \<Longrightarrow> pc < length P \<Longrightarrow> fst (P ! pc) = Some l"
  unfolding res\<^sub>P_def res\<^sub>P'_def Option.bind_def
  apply(case_tac "List.extract (\<lambda>x. fst x = Some l) P")
   apply clarsimp
  apply clarsimp
  apply(clarsimp simp add: extract_Some_iff)
  done

(* If a label resolves to a pc, then no earlier locations are labelled by that label *)
lemma res\<^sub>P_misses: "res\<^sub>P l P = pc \<Longrightarrow> \<forall>n < pc. fst (P ! n) \<noteq> Some l"
  unfolding res\<^sub>P_def res\<^sub>P'_def Option.bind_def
  apply clarsimp
  apply(case_tac "List.extract (\<lambda>x. fst x = Some l) P")
   apply(simp add: extract_None_iff)
  apply(clarsimp simp add: extract_Some_iff)
  apply(erule_tac x="a ! n" in ballE)
   apply(simp add: nth_append)
  apply clarsimp
  done

lemma ib_res\<^sub>P: "pc < length P \<Longrightarrow> fst (P ! pc) = Some l \<Longrightarrow> \<forall>n < pc. fst (P ! n) \<noteq> Some l \<Longrightarrow> res\<^sub>P l P = pc"
  by (metis (full_types) nat_neq_iff res\<^sub>P_hits res\<^sub>P_misses res\<^sub>P_oob)

lemma exit_res\<^sub>P: "pc = length P \<Longrightarrow> \<forall>n < pc. fst (P ! n) \<noteq> Some l \<Longrightarrow> res\<^sub>P l P = pc"
  by (metis (full_types) res\<^sub>P_hits res\<^sub>P_oob)

(* If a label resolves to a pc in P, then it resolves to the same pc in P @ tail *)
lemma res\<^sub>P_ib_append:
  assumes "res\<^sub>P l P = pc" and
    pc_ib: "pc < length P"
  shows "res\<^sub>P l (P @ tail) = pc"
proof -
  from assms res\<^sub>P_misses
    have "\<forall>n < pc. fst (P ! n) \<noteq> Some l"
    by force
  hence no_earlier_hits: "\<forall>n < pc. fst ((P@tail) ! n) \<noteq> Some l"
    by (metis pc_ib less_trans nth_append)
  from assms
  have "fst (P ! pc) = Some l"
    using res\<^sub>P_hits by blast
  hence "fst ((P@tail) ! pc) = Some l"
    by (simp add: pc_ib nth_append)
  with no_earlier_hits pc_ib
  show ?thesis
    unfolding res\<^sub>P_def res\<^sub>P'_def Option.bind_def
    apply(case_tac "List.extract (\<lambda>x. fst x = Some l) (P@tail)")
     apply(metis (mono_tags, lifting) extract_None_iff length_append nth_mem trans_less_add1)
    apply(clarsimp simp add: extract_Some_iff)
    by (metis fst_conv linorder_neqE_nat nth_append nth_append_length nth_mem)
qed

(* If a label resolves to pc = length P, then:
  - If tail has l, it resolves to (length P) + res\<^sub>P l tail
  - If tail doesn't have l, it resolves to (length P) + (length tail)
  But that's just saying it resolves to pc + res\<^sub>P l tail *)
lemma res\<^sub>P_exit_append:
  assumes "res\<^sub>P l P = pc" and
    pc_exit: "pc = length P"
  shows "res\<^sub>P l (P @ tail) = pc + res\<^sub>P l tail"
proof(case_tac "tail = []")
  show "tail = [] \<Longrightarrow> ?thesis"
    by (simp add: assms(1) exit_res\<^sub>P)
next
  assume has_tail: "tail \<noteq> []"
  from assms res\<^sub>P_misses
  have no_earlier_hits: "\<forall>n < pc. fst ((P@tail) ! n) \<noteq> Some l"
    by (metis pc_exit nth_append)
  from has_tail pc_exit
  have pc_ib_tail: "pc < length (P @ tail)"
    by simp
  obtain pc' where pc'_def: "res\<^sub>P l tail = pc'"
    by simp
  with res\<^sub>P_misses
  have "\<forall>n < pc'. fst (tail ! n) \<noteq> Some l"
    by blast
  with pc'_def pc_exit no_earlier_hits
  have pc'_misses: "\<forall>n < length P + pc'. fst ((P @ tail) ! n) \<noteq> Some l"
    by (simp add: add.commute less_diff_conv2 not_le nth_append)
  show ?thesis
    proof(case_tac "pc' < length tail")
      assume pc'_ib: "pc' < length tail"
      with pc'_def res\<^sub>P_hits
      have "fst (tail ! pc') = Some l"
        by blast
      then have "fst ((P @ tail) ! (length P + pc')) = Some l"
        by simp
      with pc'_ib pc'_misses
      show ?thesis
        using ib_res\<^sub>P
        by (metis length_append nat_add_left_cancel_less pc'_def pc_exit)
    next
      assume pc'_exit: "\<not> pc' < length tail"
      hence "pc' = length tail"
        using res\<^sub>P_bounds pc'_def le_neq_implies_less
        by blast
      with pc'_misses
      show ?thesis
        using exit_res\<^sub>P
        by (metis length_append pc'_def pc_exit)
    qed
qed

lemma oob_labels_res\<^sub>P: "\<forall>l \<in> oob_jump_labels P. res\<^sub>P l P = length P"
  unfolding oob_jump_labels_def res\<^sub>P_def
  by simp

lemma
  "joinable_forward P tail \<Longrightarrow> l \<in> jump_labels P \<Longrightarrow> res\<^sub>P l P = pc \<Longrightarrow> res\<^sub>P l (P @ tail) = pc"
  unfolding joinable_forward_def
  apply clarsimp
  apply(erule_tac x=l in ballE)
   using res\<^sub>P_exit_append
   apply (metis add.right_neutral nat_less_le not_gr0 res\<^sub>P_bounds res\<^sub>P_ib_append res\<^sub>P_misses)

  unfolding oob_jump_labels_def res\<^sub>P_def res\<^sub>P_ib_append
  apply(clarsimp split:option.splits)

  using  res\<^sub>P_ib_append res\<^sub>P_bounds
  (* l might not be in oob_labels but still resolve to exit,
    because oob_labels are only the exit labels P jumps to *)
  oops

lemma res\<^sub>P_ib_head:
  assumes
    "pc < length P"
    "res\<^sub>P l (P @ tail) = pc"
  shows "res\<^sub>P l P = pc"
proof -
  from assms res\<^sub>P_misses
    have "\<forall>n < pc. fst ((P@tail) ! n) \<noteq> Some l"
    using length_append trans_less_add1 by force
  with assms have no_earlier_hits: "\<forall>n < pc. fst (P ! n) \<noteq> Some l"
    using nth_append
    by (metis less_trans)
  from assms
  have "fst ((P@tail) ! pc) = Some l"
    by (simp add: res\<^sub>P_hits)
  with assms
  have "fst (P ! pc) = Some l"
    using nth_append
    by metis
  with no_earlier_hits assms
  show ?thesis
    unfolding res\<^sub>P_def res\<^sub>P'_def Option.bind_def
    apply(case_tac "List.extract (\<lambda>x. fst x = Some l) P")
     apply(metis (mono_tags, lifting) extract_None_iff nth_mem)
    apply(clarsimp simp add: extract_Some_iff)
    by (metis fst_conv linorder_neqE_nat nth_append nth_append_length nth_mem)
qed

lemma res\<^sub>P_exit_head:
  assumes
    "pc = length P"
    "res\<^sub>P l (P @ tail) = pc"
  shows "res\<^sub>P l P = pc"
proof(case_tac "tail = []")
  show "tail = [] \<Longrightarrow> ?thesis"
  using assms by auto
next
  assume has_tail: "tail \<noteq> []"
  from assms res\<^sub>P_misses
    have "\<forall>n < pc. fst ((P@tail) ! n) \<noteq> Some l"
    using length_append trans_less_add1 by force
  with assms have no_earlier_hits: "\<forall>n < pc. fst (P ! n) \<noteq> Some l"
    using nth_append
    by metis
  from assms
  have "fst ((P@tail) ! pc) = Some l"
    by (simp add: has_tail res\<^sub>P_hits)
  with no_earlier_hits
  show ?thesis
    unfolding res\<^sub>P_def res\<^sub>P'_def Option.bind_def
    apply(case_tac "List.extract (\<lambda>x. fst x = Some l) P")
     apply (simp add: assms(1))
    apply(clarsimp simp add: extract_Some_iff)
    by (metis fst_conv linorder_neqE_nat nth_append nth_append_length nth_mem)
qed

lemma res\<^sub>P_heads_miss:
  "\<forall>n < length P. (res\<^sub>P l (map fst P) = length P \<and> P' = take n P \<longrightarrow> res\<^sub>P l (map fst P') = length P')"
  apply clarsimp
  apply(rule exit_res\<^sub>P)
   apply clarsimp
  apply clarsimp
  apply(drule res\<^sub>P_misses)
  by clarsimp

lemma "\<exists>pc. pc < length P \<and> snd (P ! pc) = Jmp l \<Longrightarrow> l \<in> jump_labels P"
  unfolding jump_labels_def
  by blast

lemma "joinable_forward P tail \<Longrightarrow> pc < length P \<Longrightarrow> snd (P ! pc) = Jmp l \<Longrightarrow> res\<^sub>P l P = length P \<Longrightarrow> fst (tail ! 0) = Some l"
  unfolding joinable_forward_def oob_jump_labels_def
  apply(subgoal_tac "l \<in> jump_labels P")
   apply clarsimp
  unfolding jump_labels_def
  by blast

lemma "fst (tail ! 0) = Some l \<Longrightarrow> res\<^sub>P l tail = 0"
  using ib_res\<^sub>P
  by (meson not_gr0 res\<^sub>P_misses)

lemma join_exit:
  "joinable_forward P tail \<Longrightarrow>
   pc < length P \<Longrightarrow>
   snd (P ! pc) = Jmp l \<or> snd (P ! pc) = Jz l r \<Longrightarrow>
   res\<^sub>P l P = length P \<Longrightarrow>
   res\<^sub>P l tail = 0"
  unfolding joinable_forward_def oob_jump_labels_def
  apply(subgoal_tac "l \<in> jump_labels P")
   apply clarsimp
   apply(meson not_gr0 res\<^sub>P_misses)
  unfolding jump_labels_def
  by blast

lemma eval\<^sub>r_bounds:
  "\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   pc < length P \<and> pc' \<le> length P"
  apply(erule eval\<^sub>r_elim, simp+)
         using res\<^sub>P_bounds apply blast
        using res\<^sub>P_bounds apply blast
       by simp+

lemma eval\<^sub>r_head_bounds:
  "joinable_forward P tail \<Longrightarrow> pc < length P \<Longrightarrow>
   \<langle>(pc, P@tail), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P@tail), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   pc' \<le> length P"
  unfolding joinable_forward_def oob_jump_labels_def
  apply(erule eval\<^sub>r_elim, simp+)
         apply(rename_tac l)
         apply(subgoal_tac "snd (P ! pc) = Jmp l")
          apply(clarsimp simp:jump_labels_def)
          apply(metis add.right_neutral linorder_neqE_nat not_less nth_append_length_plus res\<^sub>P_bounds res\<^sub>P_ib_append res\<^sub>P_misses)
         apply(simp add: nth_append)
        apply(rename_tac l r)
        apply(subgoal_tac "snd (P ! pc) = Jz l r")
        apply(clarsimp simp:jump_labels_def)
         apply(metis add.right_neutral linorder_neqE_nat not_less nth_append_length_plus res\<^sub>P_bounds res\<^sub>P_ib_append res\<^sub>P_misses)
        apply(simp add: nth_append)
       using Suc_leI apply blast+
  done

lemma eval\<^sub>r_append:
  "joinable_forward P tail \<Longrightarrow> pc < length P \<Longrightarrow>
   \<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   \<langle>(pc, P@tail), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P@tail), regs', mds', mem'\<rangle>\<^sub>r"
  apply(subgoal_tac "pc' \<le> length P")
   prefer 2
   apply(simp add:eval\<^sub>r_bounds)
  apply(erule eval\<^sub>r_elim)
                apply(force simp:eval\<^sub>r.lock_spin nth_append)
               apply(metis (no_types, lifting) Suc_eq_plus1 length_append nth_append trans_less_add1 eval\<^sub>r.lock_acq)
              apply(force simp:eval\<^sub>r.lock_invalid nth_append)
             apply(metis (no_types, lifting) Suc_eq_plus1 length_append nth_append trans_less_add1 eval\<^sub>r.lock_rel)
            apply(metis (no_types, lifting) Suc_eq_plus1 length_append nth_append trans_less_add1 eval\<^sub>r.load)
           apply(metis (no_types, lifting) Suc_eq_plus1 length_append nth_append trans_less_add1 eval\<^sub>r.store)
          apply(subgoal_tac "res\<^sub>P l (P@tail) = pc'")
           apply(metis (no_types, lifting) length_append nth_append trans_less_add1 eval\<^sub>r.jmp)
          apply(case_tac "pc' < length P")
           apply(simp add:res\<^sub>P_ib_append)
          apply(simp add:res\<^sub>P_exit_append join_exit)
         apply(subgoal_tac "res\<^sub>P l (P@tail) = pc'")
          apply(metis (no_types, lifting) length_append nth_append trans_less_add1 eval\<^sub>r.jz_s)
         apply(case_tac "pc' < length P")
          apply(simp add:res\<^sub>P_ib_append)
         apply(simp add:res\<^sub>P_exit_append join_exit)
        apply(clarsimp simp only:Nat.Suc_eq_plus1)
        apply(fastforce intro:eval\<^sub>r.jz_f simp:nth_append)
       apply(metis (no_types, lifting) Suc_eq_plus1 length_append nth_append trans_less_add1 eval\<^sub>r.nop)
      apply(metis (no_types, lifting) Suc_eq_plus1 length_append nth_append trans_less_add1 eval\<^sub>r.tau)
     apply(metis (no_types, lifting) Suc_eq_plus1 length_append nth_append trans_less_add1 eval\<^sub>r.move_k)
    apply(metis (no_types, lifting) Suc_eq_plus1 length_append nth_append trans_less_add1 eval\<^sub>r.move_r)
   apply(metis (no_types, lifting) Suc_eq_plus1 length_append nth_append trans_less_add1 eval\<^sub>r.op)
  apply(metis (no_types, lifting) Suc_eq_plus1 length_append nth_append trans_less_add1 eval\<^sub>r.out)
  done

lemma eval\<^sub>r_head:
  "pc < length P \<Longrightarrow> pc' \<le> length P \<Longrightarrow>
   \<langle>(pc, P@tail), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P@tail), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   \<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P), regs', mds', mem'\<rangle>\<^sub>r"
  apply(erule eval\<^sub>r_elim)
                apply(force simp:eval\<^sub>r.lock_spin nth_append)
               apply(metis (no_types, lifting) Suc_eq_plus1 nth_append eval\<^sub>r.lock_acq)
              apply(force simp:eval\<^sub>r.lock_invalid nth_append)
             apply(metis (no_types, lifting) Suc_eq_plus1 nth_append eval\<^sub>r.lock_rel)
            apply(metis (no_types, lifting) Suc_eq_plus1 nth_append eval\<^sub>r.load)
           apply(metis (no_types, lifting) Suc_eq_plus1 nth_append eval\<^sub>r.store)
          apply(subgoal_tac "res\<^sub>P l P = pc'")
           apply(simp add: eval\<^sub>r.jmp nth_append)
          apply(case_tac "pc' < length P")
           apply(metis (no_types, lifting) res\<^sub>P_ib_head)
          apply clarsimp
          apply(metis (no_types, lifting) res\<^sub>P_exit_head)
         apply(subgoal_tac "res\<^sub>P l P = pc'")
          apply(simp add: eval\<^sub>r.jz_s nth_append)
         apply(case_tac "pc' < length P")
          apply(metis (no_types, lifting) res\<^sub>P_ib_head)
         apply clarsimp
         apply(metis (no_types, lifting) res\<^sub>P_exit_head)
        apply(metis (no_types, lifting) Suc_eq_plus1 nth_append eval\<^sub>r.jz_f)
       apply(metis (no_types, lifting) Suc_eq_plus1 nth_append eval\<^sub>r.nop)
      apply(metis (no_types, lifting) Suc_eq_plus1 nth_append eval\<^sub>r.tau)
     apply(metis (no_types, lifting) Suc_eq_plus1 nth_append eval\<^sub>r.move_k)
    apply(metis (no_types, lifting) Suc_eq_plus1 nth_append eval\<^sub>r.move_r)
   apply(metis (no_types, lifting) Suc_eq_plus1 nth_append eval\<^sub>r.op)
  apply(metis (no_types, lifting) Suc_eq_plus1 nth_append eval\<^sub>r.out)
  done

lemma eval\<^sub>r_join_left:
  "joinable_forward P tail \<Longrightarrow> pc < length P \<Longrightarrow>
   \<langle>(pc, P@tail), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P@tail), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   \<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P), regs', mds', mem'\<rangle>\<^sub>r"
  using eval\<^sub>r_head_bounds eval\<^sub>r_head
  by force

lemma helper_eval\<^sub>r_mid_bounds:
  "\<And>n na nb. (n::nat) < na \<or> \<not> n + nb < nb + na"
  by force

lemma eval\<^sub>r_mid_bounds:
  "joinable_forward P tail \<Longrightarrow> pc < length (P\<^sub>0 @ P) \<Longrightarrow> pc \<ge> length P\<^sub>0 \<Longrightarrow>
   \<langle>(pc, P\<^sub>0@P@tail), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P\<^sub>0@P@tail), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   pc' \<le> length (P\<^sub>0@P)"
  apply clarsimp
  apply(subgoal_tac "pc - length P\<^sub>0 < length P")
   prefer 2
   apply linarith
  unfolding joinable_forward_def oob_jump_labels_def jump_labels_def
  apply(erule eval\<^sub>r_elim, simp+)
         apply(rename_tac l)
         apply(subgoal_tac "snd (P ! (pc - length P\<^sub>0)) = Jmp l")
          prefer 2
          apply(force simp:nth_append split:if_splits)
         apply(subgoal_tac "res\<^sub>P l (P\<^sub>0 @ P @ tail) \<le> length P + length P\<^sub>0")
          prefer 2
          using helper_eval\<^sub>r_mid_bounds
          apply(metis (no_types) cancel_comm_monoid_add_class.diff_cancel leD less_or_eq_imp_le not_le_imp_less nth_append res\<^sub>P_exit_append res\<^sub>P_ib_append res\<^sub>P_misses res\<^sub>P_oob trans_less_add2)
         apply force
        apply(rename_tac l r)
        apply(subgoal_tac "snd (P ! (pc - length P\<^sub>0)) = Jz l r")
         prefer 2
         apply(force simp:nth_append split:if_splits)
        apply clarsimp
        apply(subgoal_tac "res\<^sub>P l (P\<^sub>0 @ P @ tail) \<le> length P + length P\<^sub>0")
         prefer 2
         using helper_eval\<^sub>r_mid_bounds
         apply(metis (no_types) cancel_comm_monoid_add_class.diff_cancel leD less_or_eq_imp_le not_le_imp_less nth_append res\<^sub>P_exit_append res\<^sub>P_ib_append res\<^sub>P_misses res\<^sub>P_oob trans_less_add2)
        apply force
       using Suc_leI apply blast+
  done

lemma eval\<^sub>r_join_right:
  "pc = length P\<^sub>1 + pc\<^sub>2 \<Longrightarrow> pc' = length P\<^sub>1 + pc\<^sub>2' \<Longrightarrow>
   pc\<^sub>2 < length P\<^sub>2 \<Longrightarrow>
   \<langle>(pc, P\<^sub>1@P\<^sub>2), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P\<^sub>1@P\<^sub>2), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   \<langle>(pc\<^sub>2, P\<^sub>2), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc\<^sub>2', P\<^sub>2), regs', mds', mem'\<rangle>\<^sub>r"
  apply(erule eval\<^sub>r_elim)
                apply(clarsimp, metis eval\<^sub>r.lock_spin)
               apply(clarsimp, metis eval\<^sub>r.lock_acq Suc_eq_plus1)
              apply(clarsimp, metis eval\<^sub>r.lock_invalid)
             apply(clarsimp, metis eval\<^sub>r.lock_rel Suc_eq_plus1)
            apply(clarsimp, metis eval\<^sub>r.load Suc_eq_plus1)
           apply(clarsimp, metis eval\<^sub>r.store Suc_eq_plus1)
          apply(clarsimp, metis (no_types, lifting) eval\<^sub>r.jmp add_diff_cancel_left' le_add1 le_antisym le_neq_implies_less res\<^sub>P_bounds res\<^sub>P_exit_append res\<^sub>P_ib_append)
         apply(clarsimp, metis (no_types, lifting) eval\<^sub>r.jz_s add_diff_cancel_left' le_add1 le_antisym le_neq_implies_less res\<^sub>P_bounds res\<^sub>P_exit_append res\<^sub>P_ib_append)
        apply(clarsimp, metis eval\<^sub>r.jz_f Suc_eq_plus1)
       apply(clarsimp, metis eval\<^sub>r.nop Suc_eq_plus1)
      apply(clarsimp, metis eval\<^sub>r.tau Suc_eq_plus1)
     apply(clarsimp, metis eval\<^sub>r.move_k Suc_eq_plus1)
    apply(clarsimp, metis eval\<^sub>r.move_r Suc_eq_plus1)
   apply(clarsimp, metis eval\<^sub>r.op Suc_eq_plus1)
  by (clarsimp, metis eval\<^sub>r.out Suc_eq_plus1)

lemma present_labels_def2:
  "present_labels P \<equiv> {l. res\<^sub>P l P < length P}"
  apply(unfold present_labels_def)
  apply(rule eq_reflection)
  apply(rule Set.equalityI)
   apply clarsimp
   apply(metis leD nat_neq_iff res\<^sub>P_bounds res\<^sub>P_misses)
  apply clarsimp
  using res\<^sub>P_hits by blast

lemma present_labels_append:
  "present_labels (P\<^sub>1 @ P\<^sub>2) = present_labels P\<^sub>1 \<union> present_labels P\<^sub>2"
  unfolding present_labels_def2
  apply clarsimp
  apply(rule Set.equalityI)
   apply clarsimp
   apply(metis leD nat_neq_iff res\<^sub>P_bounds res\<^sub>P_exit_append)
  apply clarsimp
  apply(rule conjI)
   apply(clarsimp simp add: res\<^sub>P_ib_append)
  apply clarsimp
  by (metis (no_types, lifting) add_less_cancel_left le_add1 le_neq_implies_less length_append not_less res\<^sub>P_bounds res\<^sub>P_exit_append res\<^sub>P_ib_append)

lemma oob_jump_labels_def2:
  "oob_jump_labels P = {l. l \<in> jump_labels P \<and> l \<notin> present_labels P}"
  apply(unfold oob_jump_labels_def)
  apply(clarsimp simp:present_labels_def2)
  using res\<^sub>P_bounds leD nat_neq_iff by blast

lemma jump_labels_append:
  "jump_labels (P\<^sub>1 @ P\<^sub>2) = jump_labels P\<^sub>1 \<union> jump_labels P\<^sub>2"
  unfolding jump_labels_def
  apply clarsimp
  apply(rule Set.equalityI)
   apply clarsimp
   apply(metis add.commute less_diff_conv2 not_less nth_append)
  apply clarsimp
  apply(rule conjI)
   apply clarsimp
   apply(metis nth_append trans_less_add1)
  apply clarsimp
  by (metis add.commute add_less_cancel_right nth_append_length_plus)

lemma eval\<^sub>r_no_backtrack_over_join:
  "joinable_backward P\<^sub>1 P\<^sub>2 \<Longrightarrow>
   pc \<ge> length P\<^sub>1 \<Longrightarrow>
   \<langle>(pc, P\<^sub>1 @ P\<^sub>2), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P\<^sub>1 @ P\<^sub>2), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   pc' \<ge> length P\<^sub>1"
  apply(clarsimp simp:joinable_backward_def present_labels_def2 res\<^sub>P_oob)
  apply(erule eval\<^sub>r_elim, simp_all)
   apply(clarsimp simp:jump_labels_def)
   apply(erule_tac x=l in allE)
   apply(erule impE)
    apply(metis add_diff_cancel_left' add_less_cancel_left leD le_Suc_ex nth_append)
   apply(simp add: res\<^sub>P_exit_append)
  apply(clarsimp simp:jump_labels_def)
  apply(erule_tac x=l in allE)
  apply(erule impE)
   apply(metis add_diff_cancel_left' add_less_cancel_left leD le_Suc_ex nth_append)
  apply(simp add: res\<^sub>P_exit_append)
  done

lemma no_jumps_eval\<^sub>r_only_stays_or_increments:
  "jump_labels P = {} \<Longrightarrow>
   \<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   pc' = pc \<or> pc' = Suc pc"
  apply(clarsimp simp:jump_labels_def)
  by (erule eval\<^sub>r_elim, (simp add:nth_append)+)

lemma eval\<^sub>r_det:
  "(\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   (\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r, \<langle>(pc'', P''), regs'', mds'', mem''\<rangle>\<^sub>r) \<in> eval\<^sub>r \<Longrightarrow>
   \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r = \<langle>(pc'', P''), regs'', mds'', mem''\<rangle>\<^sub>r"
  by (induct rule:eval\<^sub>r.induct, force+)

lemma eval\<^sub>r_enabled_mem_diff:
  "\<langle>(pc, P), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P'), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   \<exists> pc'' P'' regs'' mds'' mem''.
    \<langle>(pc, P), regs, mds, mem'''\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc'', P''), regs'', mds'', mem''\<rangle>\<^sub>r"
  by (induct rule:eval\<^sub>r.induct, (fastforce dest:eval\<^sub>r_halt_oob_pc)+)

lemma eval\<^sub>r_prepend_join:
  "joinable_backward P\<^sub>1 P\<^sub>2 \<Longrightarrow>
   pc = length P\<^sub>1 + pc\<^sub>2 \<Longrightarrow> pc' = length P\<^sub>1 + pc\<^sub>2' \<Longrightarrow>
   pc\<^sub>2 < length P\<^sub>2 \<Longrightarrow>
   \<langle>(pc\<^sub>2, P\<^sub>2), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc\<^sub>2', P\<^sub>2), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   \<langle>(pc, P\<^sub>1@P\<^sub>2), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P\<^sub>1@P\<^sub>2), regs', mds', mem'\<rangle>\<^sub>r"
  apply(erule eval\<^sub>r_elim)
                apply(force simp:eval\<^sub>r.lock_spin nth_append)
               apply(force simp:lock_acq' nth_append)
              apply(force simp:eval\<^sub>r.lock_invalid nth_append)
             apply(force simp:lock_rel' nth_append)
            using eval\<^sub>r.load apply force
           using eval\<^sub>r.store apply force
          apply(clarsimp simp:joinable_backward_def)
          apply(rule eval\<^sub>r.jmp)
            apply force
           apply force
          apply(rename_tac l)
          apply(erule_tac x=l in ballE)
           apply(clarsimp simp:present_labels_def2)
           using res\<^sub>P_exit_append res\<^sub>P_oob apply blast
          apply(force simp:jump_labels_def)
         apply(clarsimp simp:joinable_backward_def)
         apply(rule eval\<^sub>r.jz_s)
            apply force
           apply force
          apply(rename_tac l r)
          apply(erule_tac x=l in ballE)
           apply(clarsimp simp:present_labels_def2)
           using res\<^sub>P_exit_append res\<^sub>P_oob apply blast
          apply(force simp:jump_labels_def)
         apply force
        using eval\<^sub>r.intros by force+

lemma eval\<^sub>r_context_joins:
  "joinable_backward P\<^sub>1 P\<^sub>2 \<Longrightarrow>
   joinable_forward P\<^sub>2 tail \<Longrightarrow>
   pc = length P\<^sub>1 + pc\<^sub>2 \<Longrightarrow> pc' = length P\<^sub>1 + pc\<^sub>2' \<Longrightarrow>
   pc\<^sub>2 < length P\<^sub>2 \<Longrightarrow>
   \<langle>(pc\<^sub>2, P\<^sub>2), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc\<^sub>2', P\<^sub>2), regs', mds', mem'\<rangle>\<^sub>r \<Longrightarrow>
   \<langle>(pc, P\<^sub>1@P\<^sub>2@tail), regs, mds, mem\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P\<^sub>1@P\<^sub>2@tail), regs', mds', mem'\<rangle>\<^sub>r"
  apply(erule eval\<^sub>r_elim)
                apply(force simp:eval\<^sub>r.lock_spin nth_append)
               apply(force simp:lock_acq' nth_append)
              apply(force simp:eval\<^sub>r.lock_invalid nth_append)
             apply(force simp:lock_rel' nth_append)
            using eval\<^sub>r.load
            apply(metis (no_types, lifting) Suc_eq_plus1 add_Suc_right add_diff_cancel_left' length_append nat_add_left_cancel_less not_add_less1 nth_append trans_less_add1)
           using eval\<^sub>r.store
           apply(metis (no_types, lifting) Suc_eq_plus1 add_Suc_right add_diff_cancel_left' length_append nat_add_left_cancel_less not_add_less1 nth_append trans_less_add1)
          apply(clarsimp simp:joinable_backward_def joinable_forward_def)
          apply(rule eval\<^sub>r.jmp)
            apply force
           apply(force simp:nth_append)
          apply(rename_tac l)
          apply(subgoal_tac "l \<in> jump_labels P\<^sub>2")
           prefer 2
           apply(force simp:jump_labels_def)
          apply(erule_tac x=l in ballE)
           prefer 2
           apply force
          apply(case_tac "l \<in> oob_jump_labels P\<^sub>2")
           apply(erule_tac x=l in ballE)
            apply(clarsimp simp:oob_jump_labels_def present_labels_def2)
            apply(subgoal_tac "res\<^sub>P l tail = 0")
             prefer 2
             apply(metis (no_types) add.commute add.right_neutral nat_neq_iff not_add_less1 res\<^sub>P_misses)
            apply(force simp:res\<^sub>P_exit_append res\<^sub>P_oob)
           apply force
          apply(clarsimp simp:oob_jump_labels_def2 present_labels_def2)
          apply(force simp: res\<^sub>P_exit_append res\<^sub>P_ib_append res\<^sub>P_oob)
         apply(clarsimp simp:joinable_backward_def joinable_forward_def)
         apply(rule eval\<^sub>r.jz_s)
            apply force
           apply(force simp:nth_append)
          apply(rename_tac l r)
          apply(subgoal_tac "l \<in> jump_labels P\<^sub>2")
           prefer 2
           apply(force simp:jump_labels_def)
          apply(erule_tac x=l in ballE)
           prefer 2
           apply force
          apply(case_tac "l \<in> oob_jump_labels P\<^sub>2")
           apply(erule_tac x=l in ballE)
            apply(clarsimp simp:oob_jump_labels_def present_labels_def2)
            apply(subgoal_tac "res\<^sub>P l tail = 0")
             prefer 2
             apply(metis (no_types) add.commute add.right_neutral nat_neq_iff not_add_less1 res\<^sub>P_misses)
            apply(force simp:res\<^sub>P_exit_append res\<^sub>P_oob)
           apply force
          apply(clarsimp simp:oob_jump_labels_def2 present_labels_def2)
          apply(force simp: res\<^sub>P_exit_append res\<^sub>P_ib_append res\<^sub>P_oob)
         apply force
        using eval\<^sub>r.intros
        by (metis (no_types, lifting) Suc_eq_plus1 add_Suc_right add_diff_cancel_left' length_append nat_add_left_cancel_less not_add_less1 nth_append trans_less_add1)+

(* Thanks to Matthew Brecknell *)
lemma disj_allE: "(\<forall>x. P x) \<or> (\<forall>x. Q x) \<Longrightarrow> (P x \<Longrightarrow> R) \<Longrightarrow> (Q x \<Longrightarrow> R) \<Longrightarrow> R"
  by blast

lemma eval\<^sub>r_preserves_lock_consistent_mds:
  "\<langle>(pc, P), regs, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>r \<leadsto>\<^sub>r \<langle>(pc', P'), regs', mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>r \<Longrightarrow>
   lone_lock_per_var lock_interp \<Longrightarrow>
   lock_interp_no_overlap lock_interp \<Longrightarrow>
   lock_consistent_mds lock_interp mds\<^sub>C \<Longrightarrow>
   lock_consistent_mds lock_interp mds\<^sub>C'"
  apply(clarsimp simp:lock_consistent_mds_def)
  apply(rule conjI)
   apply(force simp:no_lock_mds_def eval_lock_mds_untouched)
  apply(erule eval\<^sub>r_elim)
                apply force
               apply(rule context_conjI)
                apply(clarsimp simp:only_lock_governed_mds_def lock_acq_upd_def image_def split:prod.splits Mode.splits)
                apply(metis (mono_tags, hide_lams) Mode.exhaust fst_conv snd_conv)
               apply(rule context_conjI)
                apply(clarsimp simp:lock_not_held_mds_correct_def lock_held_mds_correct_def lock_acq_upd_def image_def split:prod.splits Mode.splits)
                apply(rule conjI)
                 apply(metis (no_types, hide_lams) Un_iff fst_conv snd_conv lone_lock_per_var_def)
                apply(metis (no_types, hide_lams) Un_iff fst_conv snd_conv lone_lock_per_var_def)
               apply(clarsimp simp:one_mode_per_var_def)
               apply(clarsimp simp:lock_acq_upd_def split:prod.splits)
               apply(clarsimp simp:lock_interp_no_overlap_def image_def)
               apply(clarsimp simp:lock_not_held_mds_correct_def lock_held_mds_correct_def image_def)
               apply(erule_tac x=l in allE)
               apply(erule_tac x=l in allE)
               apply(erule_tac x=l in allE)
               apply clarsimp
               apply(rename_tac NoW NoRW)
               apply(case_tac "v \<in> NoW")
                apply(erule_tac x=v in disj_allE)
                 apply(force simp:image_def split:Mode.splits)
                apply(force simp:image_def split:Mode.splits)
               apply(erule_tac x=v in disj_allE)
                apply(force simp:image_def split:Mode.splits)
               apply(force simp:image_def split:Mode.splits)
              apply force
             (* Repeat of the above, but for the rel case *)
             apply(rule context_conjI)
              apply(clarsimp simp:only_lock_governed_mds_def lock_rel_upd_def image_def split:prod.splits Mode.splits)
              apply(metis Mode.exhaust fst_conv snd_conv)
             apply(rule context_conjI)
              apply(clarsimp simp:lock_not_held_mds_correct_def lock_held_mds_correct_def lock_rel_upd_def image_def split:prod.splits Mode.splits)
              apply(rule conjI)
               apply(metis (no_types, lifting) Un_iff fst_conv snd_conv lone_lock_per_var_def)
              apply(metis (no_types, lifting) Un_iff fst_conv snd_conv lone_lock_per_var_def)
             apply(clarsimp simp:one_mode_per_var_def)
             apply(clarsimp simp:lock_rel_upd_def split:prod.splits)
             apply(clarsimp simp:lock_interp_no_overlap_def image_def)
             apply(clarsimp simp:lock_not_held_mds_correct_def lock_held_mds_correct_def image_def)
             apply(erule_tac x=l in allE)
             apply(erule_tac x=l in allE)
             apply(erule_tac x=l in allE)
             apply clarsimp
             apply(rename_tac NoW NoRW)
             apply(erule_tac x=v in allE)
             apply(fastforce simp:image_def split:Mode.splits) (* long-running, takes about 15 s *)
            apply force+
  done

end

end