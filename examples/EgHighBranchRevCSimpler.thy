(*
Author: Robert Sison
Based on the Dependent_SIFUM_Refinement AFP entry, whose authors are:
  Toby Murray, Robert Sison, Edward Pierzchalski, Christine Rizkallah
*)
theory EgHighBranchRevCSimpler
imports "../old-rg-sec/Compositionality"
        "../while-lang/Language"
        "~~/src/HOL/Eisbach/Eisbach_Tools"
        "../refinement/CompositionalRefinement"
begin

text {*
  Theory for exploring refinement of a program that branches on a high variable.

  The purpose of this particular example is to demonstrate that in order to prove any branching
  on a High-classified variable remains safe when refined to a concrete implementation, it is
  necessary to ensure that the two concrete branches of execution take the same number of steps,
  possibly by skip-padding the one with less steps in it, and provide an appropriate coupling
  invariant to link the corresponding concrete steps of the two branches of the if-statement.
*}

type_synonym 'var addr = 'var
type_synonym val = nat
type_synonym 'var mem = "'var addr \<Rightarrow> val"

(* Arithmetic expression evaluation *)
datatype 'var aexp = Const "val" |
                     Load "'var addr" |
                     Add "'var aexp" "'var aexp" |
                     Sub "'var aexp" "'var aexp"

fun
  ev\<^sub>A :: "'var mem \<Rightarrow> 'var aexp \<Rightarrow> val"
where
  "ev\<^sub>A mem (Const v) = v" |
  "ev\<^sub>A mem (Load x) = mem x" |
  "ev\<^sub>A mem (Add x y) = ev\<^sub>A mem x + ev\<^sub>A mem y" |
  "ev\<^sub>A mem (Sub x y) = ev\<^sub>A mem x - ev\<^sub>A mem y"

(* Boolean expression evaluation *)
datatype 'var bexp = Eq "'var aexp" "'var aexp" |
                     Neq "'var aexp" "'var aexp" |
                     Lte "'var aexp" "'var aexp" |
                     Gt "'var aexp" "'var aexp"

fun
  ev\<^sub>B :: "'var mem \<Rightarrow> 'var bexp \<Rightarrow> bool"
where
  "ev\<^sub>B mem (Eq x y) = (ev\<^sub>A mem x = ev\<^sub>A mem y)" |
  "ev\<^sub>B mem (Neq x y) = (ev\<^sub>A mem x \<noteq> ev\<^sub>A mem y)" |
  "ev\<^sub>B mem (Lte x y) = (ev\<^sub>A mem x \<le> ev\<^sub>A mem y)" |
  "ev\<^sub>B mem (Gt x y) = (ev\<^sub>A mem x > ev\<^sub>A mem y)"

(* Function that gives the control variables of a given variable.
 * None for this example. *)
definition
  \<C>_vars :: "'var addr \<Rightarrow> 'var addr set"
where
  "\<C>_vars x \<equiv> {}"

(* NB: mds ~ "mode state"
 * mds\<^sub>s is the initial mode state *)
definition
  mds\<^sub>s :: "Mode \<Rightarrow> 'var set"
where
  "mds\<^sub>s \<equiv> \<lambda>m. case m of AsmNoReadOrWrite \<Rightarrow> {} | _ \<Rightarrow> {}"

primrec
  aexp_vars :: "'var aexp \<Rightarrow> 'var set"
where
  "aexp_vars (Const _) = {}" |
  "aexp_vars (Load v) = {v}" |
  "aexp_vars (Add x y) = aexp_vars x \<union> aexp_vars y" |
  "aexp_vars (Sub x y) = aexp_vars x \<union> aexp_vars y"

fun
  bexp_vars :: "'var bexp \<Rightarrow> 'var addr set"
where
  "bexp_vars (Neq x y) = aexp_vars x \<union> aexp_vars y" |
  "bexp_vars (Eq x y) = aexp_vars x \<union> aexp_vars y" |
  "bexp_vars (Lte x y) = aexp_vars x \<union> aexp_vars y" |
  "bexp_vars (Gt x y) = aexp_vars x \<union> aexp_vars y"

fun
  bexp_neg :: "'var bexp \<Rightarrow> 'var bexp"
where
  "bexp_neg (Neq x y) = (Eq x y)" |
  "bexp_neg (Eq x y) = (Neq x y)" |
  "bexp_neg (Lte x y) = (Gt x y)" |
  "bexp_neg (Gt x y) = (Lte x y)"

fun
  bexp_assign :: "'var addr \<Rightarrow> 'var aexp \<Rightarrow> 'var bexp"
where
  "bexp_assign x a = (Eq (Load x) a)"

datatype var = h_var | x_var | y_var | z_var

definition
  dma :: "var mem \<Rightarrow> var addr \<Rightarrow> Sec"
where
  "dma m x \<equiv> if x = h_var then High else Low"

type_synonym t_ev\<^sub>A = "var mem \<Rightarrow> var aexp \<Rightarrow> val"
type_synonym t_ev\<^sub>B = "var mem \<Rightarrow> var bexp \<Rightarrow> bool"
type_synonym t_aexp_vars = "var aexp \<Rightarrow> var set"
type_synonym t_bexp_vars = "var bexp \<Rightarrow> var addr set"

locale sifum_example =
  sifum_lang_no_dma "ev\<^sub>A::t_ev\<^sub>A" "ev\<^sub>B::t_ev\<^sub>B" "aexp_vars::t_aexp_vars" "bexp_vars::t_bexp_vars" +
  assumes eval_det: "(lc, lc') \<in> eval\<^sub>w \<Longrightarrow> (lc, lc'') \<in> eval\<^sub>w \<Longrightarrow> lc' = lc''"

definition
  \<C> :: "var addr set"
where
  "\<C> \<equiv> \<Union>x. \<C>_vars x"

sublocale sifum_example \<subseteq>  sifum_security dma \<C>_vars \<C> eval\<^sub>w undefined
  apply(unfold_locales)
       apply(rule Var_finite)
      apply(auto simp: \<C>_vars_def dma_def \<C>_def split: if_splits)[3]
   apply(blast intro: eval_det)
  apply(auto simp: \<C>_def)
  done

context sifum_example begin
(*
  y := 0; z := 0;
  x := y;
  if h then x := y
       else x := y + z

  Must have NoWrite z to prevent an agent getting something observable by spiking the value of z.
  Also take NoWrite y & h to simplify reasoning about reg values staying consistent with the mem.
 *)
definition
  prog_high_branch :: "(var addr, var aexp, var bexp) Stmt"
where
  "prog_high_branch \<equiv>
     ModeDecl Skip (Acq h_var AsmNoWrite) ;;
     ModeDecl Skip (Acq y_var AsmNoWrite) ;;
     ModeDecl Skip (Acq z_var AsmNoWrite) ;;
     Assign y_var (Const 0) ;;
     Assign z_var (Const 0) ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     )"
end

datatype var\<^sub>C = h_mem | x_mem | y_mem | z_mem | reg0 | reg1 | reg2 | reg3

definition
  dma\<^sub>C :: "var\<^sub>C mem \<Rightarrow> var\<^sub>C addr \<Rightarrow> Sec"
where
  "dma\<^sub>C m x \<equiv> if x = h_mem then High else Low"

type_synonym t_ev\<^sub>A\<^sub>C = "var\<^sub>C mem \<Rightarrow> var\<^sub>C aexp \<Rightarrow> val"
type_synonym t_ev\<^sub>B\<^sub>C = "var\<^sub>C mem \<Rightarrow> var\<^sub>C bexp \<Rightarrow> bool"
type_synonym t_aexp_vars\<^sub>C = "var\<^sub>C aexp \<Rightarrow> var\<^sub>C set"
type_synonym t_bexp_vars\<^sub>C = "var\<^sub>C bexp \<Rightarrow> var\<^sub>C addr set"

locale sifum_example\<^sub>C =
  sifum_lang_no_dma "ev\<^sub>A::t_ev\<^sub>A\<^sub>C" "ev\<^sub>B::t_ev\<^sub>B\<^sub>C" "aexp_vars::t_aexp_vars\<^sub>C" "bexp_vars::t_bexp_vars\<^sub>C" +
  assumes eval_det: "(lc, lc') \<in> eval\<^sub>w \<Longrightarrow> (lc, lc'') \<in> eval\<^sub>w \<Longrightarrow> lc' = lc''"

definition
  \<C>\<^sub>C :: "var\<^sub>C addr set"
where
  "\<C>\<^sub>C \<equiv> \<Union>x. \<C>_vars x"

sublocale sifum_example\<^sub>C \<subseteq>  sifum_security dma\<^sub>C \<C>_vars \<C>\<^sub>C eval\<^sub>w undefined
  apply(unfold_locales)
       apply(rule Var_finite)
      apply(auto simp: \<C>_vars_def dma\<^sub>C_def \<C>_def split: if_splits)[3]
   apply(blast intro: eval_det)
  apply(auto simp: \<C>\<^sub>C_def)
  done

context sifum_example\<^sub>C begin
(*
  y := 0; z := 0;
  x := y;
  if h
    then
      // "then"-branch: x := y
      NOP ;
      NOP ;
      LOAD r0 y ;
      STORE x r0 ;
    else
      // "else"-branch: x := y + z
      LOAD r1 y ;
      LOAD r2 z ;
      ADD r0 r1 r2 ;
      STORE x r0 ;
 *)
definition
  prog_high_branch\<^sub>C :: "(var\<^sub>C addr, var\<^sub>C aexp, var\<^sub>C bexp) Stmt"
where
  "prog_high_branch\<^sub>C \<equiv>
     ModeDecl Skip (Acq h_mem AsmNoWrite) ;;
     ModeDecl Skip (Acq y_mem AsmNoWrite) ;;
     ModeDecl Skip (Acq z_mem AsmNoWrite) ;;
     (* Just set up the memory to match our assumptions *)
     Assign y_mem (Const 0) ;;
     Assign z_mem (Const 0) ;;
     Assign x_mem (Load y_mem) ;;
     (* From this point onwards we model the if-statement using registers.
        Note that we can just as well (re-)use reg0 instead of reg3 - verify with a search-replace.
        Leaving it this way to make the reg usages easier to distinguish for the reader, though. *)
     Assign reg3 (Load h_mem) ;;
     If (Neq (Load reg3) (Const 0)) (
       Skip ;;
       Skip ;;
       Assign reg0 (Load y_mem) ;;
       Assign x_mem (Load reg0)
     ) (
       Assign reg1 (Load y_mem) ;;
       Assign reg2 (Load z_mem) ;;
       Assign reg0 (Add (Load reg1) (Load reg2)) ;;
       Assign x_mem (Load reg0)
     )"

(* A function that returns all program locations reachable from a given program location.
  We just bother to make it work for this example program. *)
primrec adhoc_unpeel
where
  (* These are the only cases that matter for prog_high_branch\<^sub>C *)
  "adhoc_unpeel (Seq a b) = {Seq a b} \<union> {Seq Stop b} \<union> adhoc_unpeel b" |
  "adhoc_unpeel (If c a b) = {(If c a b)} \<union> adhoc_unpeel a \<union> adhoc_unpeel b" |
  (* Because the very last instruction of prog_high_branch\<^sub>C is always an Assign *)
  "adhoc_unpeel (Assign v e) = {Assign v e} \<union> {Stop}" |
  (* The rest we don't care about *)
  "adhoc_unpeel (ModeDecl c m) = {}" |
  "adhoc_unpeel (While b c) = {}" |
  "adhoc_unpeel (Await b c) = {}" |
  "adhoc_unpeel Skip = {}" |
  "adhoc_unpeel Stop = {}"

end

context sifum_example begin
lemma \<C>_simp[simp]:
  "\<C> = {}"
  by(auto simp: \<C>_def \<C>_vars_def split: if_splits)

declare \<C>_vars_def [simp]

(* Manual proof of bisimulation because we can't use the type system *)
inductive inv :: "(((var addr, var aexp, var bexp) Stmt, var addr, val) LocalConf) \<Rightarrow> bool"
where
  inv\<^sub>1h: "\<lbrakk>c = prog_high_branch; mds AsmNoReadOrWrite = {}; mds AsmNoWrite = {}\<rbrakk> \<Longrightarrow> inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>1h': "\<lbrakk>c =
     Stop ;;
     ModeDecl Skip (Acq y_var AsmNoWrite) ;;
     ModeDecl Skip (Acq z_var AsmNoWrite) ;;
     Assign y_var (Const 0) ;;
     Assign z_var (Const 0) ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ); mds AsmNoWrite = {h_var}; mds AsmNoReadOrWrite = {}\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>1y: "\<lbrakk>c =
     ModeDecl Skip (Acq y_var AsmNoWrite) ;;
     ModeDecl Skip (Acq z_var AsmNoWrite) ;;
     Assign y_var (Const 0) ;;
     Assign z_var (Const 0) ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ); mds AsmNoWrite = {h_var}; mds AsmNoReadOrWrite = {}\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>1y': "\<lbrakk>c =
     Stop ;;
     ModeDecl Skip (Acq z_var AsmNoWrite) ;;
     Assign y_var (Const 0) ;;
     Assign z_var (Const 0) ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ); mds AsmNoWrite = {y_var, h_var}; mds AsmNoReadOrWrite = {}\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>1z: "\<lbrakk>c =
     ModeDecl Skip (Acq z_var AsmNoWrite) ;;
     Assign y_var (Const 0) ;;
     Assign z_var (Const 0) ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ); mds AsmNoWrite = {y_var, h_var}; mds AsmNoReadOrWrite = {}\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>1z': "\<lbrakk>c =
     Stop ;;
     Assign y_var (Const 0) ;;
     Assign z_var (Const 0) ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ); mds AsmNoWrite = {z_var, y_var, h_var}; mds AsmNoReadOrWrite = {}\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>2: "\<lbrakk>c =
     Assign y_var (Const 0) ;;
     Assign z_var (Const 0) ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ); mds AsmNoWrite = {z_var, y_var, h_var}; mds AsmNoReadOrWrite = {}\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>2': "\<lbrakk>c =
     Stop ;;
     Assign z_var (Const 0) ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ); mds AsmNoWrite = {z_var, y_var, h_var}; mds AsmNoReadOrWrite = {}\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>3: "\<lbrakk>c =
     Assign z_var (Const 0) ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ); mds AsmNoWrite = {z_var, y_var, h_var}; mds AsmNoReadOrWrite = {}\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>3': "\<lbrakk>c =
     Stop ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ); mds AsmNoWrite = {z_var, y_var, h_var}; mds AsmNoReadOrWrite = {}; mem z_var = 0\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>4: "\<lbrakk>c =
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ); mds AsmNoWrite = {z_var, y_var, h_var}; mds AsmNoReadOrWrite = {}; mem z_var = 0\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>4': "\<lbrakk>c =
     Stop ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ); mds AsmNoWrite = {z_var, y_var, h_var}; mds AsmNoReadOrWrite = {}; mem z_var = 0\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>5: "\<lbrakk>c =
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ); mds AsmNoWrite = {z_var, y_var, h_var}; mds AsmNoReadOrWrite = {}; mem z_var = 0\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>5_then: "\<lbrakk>c = Assign x_var (Load y_var);
     mds AsmNoWrite = {z_var, y_var, h_var}; mds AsmNoReadOrWrite = {}; mem z_var = 0\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>5_else: "\<lbrakk>c = Assign x_var (Add (Load y_var) (Load z_var));
     mds AsmNoWrite = {z_var, y_var, h_var}; mds AsmNoReadOrWrite = {}; mem z_var = 0\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>" |
  inv\<^sub>6: "\<lbrakk>c = Stop\<rbrakk> \<Longrightarrow>
   inv \<langle>c, mds, mem\<rangle>"

definition z_locked_steps
where "z_locked_steps \<equiv> {
     Stop ;;
     Assign y_var (Const 0) ;;
     Assign z_var (Const 0) ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ),
     Assign y_var (Const 0) ;;
     Assign z_var (Const 0) ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ),
     Stop ;;
     Assign z_var (Const 0) ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     ),
     Assign z_var (Const 0) ;;
     Assign x_var (Load y_var) ;;
     If (Neq (Load h_var) (Const 0)) (
       Assign x_var (Load y_var)
     ) (
       Assign x_var (Add (Load y_var) (Load z_var))
     )}"

inductive_set rel_inv :: "(((var addr, var aexp, var bexp) Stmt, var addr, val) LocalConf) rel"
where
  rel_inv_if:
  "\<lbrakk>c = Assign x_var (Load y_var);
    c' = Assign x_var (Add (Load y_var) (Load z_var));
    z_var \<in> mds AsmNoWrite; mds AsmNoReadOrWrite = {}; mem z_var = mem' z_var\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv" |
  rel_inv_if':
  "\<lbrakk>c' = Assign x_var (Load y_var);
    c = Assign x_var (Add (Load y_var) (Load z_var));
    z_var \<in> mds AsmNoWrite; mds AsmNoReadOrWrite = {}; mem z_var = mem' z_var\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv" |
  rel_inv_start:
  "\<lbrakk>c = c'; z_var \<notin> mds AsmNoWrite; mds AsmNoReadOrWrite = {}\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv" |
  rel_inv_z_locked:
  "\<lbrakk>c \<in> z_locked_steps;
    c = c'; z_var \<in> mds AsmNoWrite; mds AsmNoReadOrWrite = {}\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv" |
  rel_inv_z_initted:
  "\<lbrakk>c \<notin> z_locked_steps;
    c = c'; mem z_var = mem' z_var; z_var \<in> mds AsmNoWrite; mds AsmNoReadOrWrite = {}\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv"

declare z_locked_steps_def[simp add]

(* the bisimulation is phrased as a conjunction of a global invariant and a
   relational invariant, defined above, plus the background requirement
   of low-equivalence modulo modes *)
inductive_set  R :: "(((var addr, var aexp, var bexp) Stmt, var addr, val) LocalConf) rel"
where
(* Having a c = c' requirement here is actually too restrictive *)
  "\<lbrakk>mds = mds'; low_mds_eq mds mem mem'; inv \<langle>c,mds,mem\<rangle>; inv \<langle>c',mds',mem'\<rangle>;
    (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv\<rbrakk> \<Longrightarrow>
 (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> R"

lemma rel_inv_sym:
  "low_mds_eq mds mem mem' \<Longrightarrow> (\<langle>c, mds, mem\<rangle>, \<langle>c, mds, mem'\<rangle>) \<in> rel_inv \<Longrightarrow>
    (\<langle>c, mds, mem'\<rangle>, \<langle>c, mds, mem\<rangle>) \<in> rel_inv"
  apply(auto elim!: rel_inv.cases intro: rel_inv.intros)
  done

lemma R_sym':
  "(\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> R \<Longrightarrow>
   (\<langle>c', mds', mem'\<rangle>, \<langle>c, mds, mem\<rangle>) \<in> R"
  apply(rule R.intros)
      apply(blast elim: R.cases dest: low_mds_eq_sym dest: rel_inv_sym)+
  apply(erule R.cases)
  apply(erule rel_inv.cases)
      apply clarsimp
      apply(rule rel_inv_if', simp+)
     apply(rule rel_inv_if, simp+)
    apply(rule rel_inv_start, simp+)
   apply(rule rel_inv_z_locked, simp+)
  apply(rule rel_inv_z_initted, simp+)
  done

lemma R_sym:
  "sym R"
  by(rule symI, clarify, erule R_sym')

lemma inv_closed_glob_consistent:
  "inv \<langle>c', mds', mem\<rangle> \<Longrightarrow>
       \<forall>x. case A x of None \<Rightarrow> True | Some (v, v') \<Rightarrow> mem x \<noteq> v \<longrightarrow> \<not> var_asm_not_written mds' x \<Longrightarrow>
       \<forall>x. dma mem [\<parallel>\<^sub>1 A] x \<noteq> dma mem x \<longrightarrow> \<not> var_asm_not_written mds' x  \<Longrightarrow>
       inv \<langle>c', mds', mem [\<parallel>\<^sub>1 A]\<rangle>"
  apply(erule inv.cases)
             using inv.intros apply (force, force, force, force, force, force, force, force, force)
        apply(rule inv\<^sub>3', clarsimp+)
        apply(erule_tac x=z_var in allE)
        apply(erule_tac x=z_var in allE)
        apply(cases "A z_var")
         apply(simp add:apply_adaptation_def)
        apply(clarsimp simp add:var_asm_not_written_def dma_def apply_adaptation_def)
       apply(rule inv\<^sub>4, clarsimp+)
       apply(erule_tac x=z_var in allE)
       apply(erule_tac x=z_var in allE)
       apply(cases "A z_var")
        apply(simp add:apply_adaptation_def)
       apply(clarsimp simp add:var_asm_not_written_def dma_def apply_adaptation_def)
      apply(rule inv\<^sub>4', clarsimp+)
      apply(erule_tac x=z_var in allE)
      apply(erule_tac x=z_var in allE)
      apply(cases "A z_var")
       apply(simp add:apply_adaptation_def)
      apply(clarsimp simp add:var_asm_not_written_def dma_def apply_adaptation_def)
     apply(rule inv\<^sub>5, clarsimp+)
     apply(erule_tac x=z_var in allE)
     apply(erule_tac x=z_var in allE)
     apply(cases "A z_var")
      apply(simp add:apply_adaptation_def)
     apply(clarsimp simp add:var_asm_not_written_def dma_def apply_adaptation_def)
    apply(rule inv\<^sub>5_then, clarsimp+)
    apply(erule_tac x=z_var in allE)
    apply(erule_tac x=z_var in allE)
    apply(cases "A z_var")
     apply(simp add:apply_adaptation_def)
    apply(clarsimp simp add:var_asm_not_written_def dma_def apply_adaptation_def)
   apply(rule inv\<^sub>5_else, clarsimp+)
   apply(erule_tac x=z_var in allE)
   apply(erule_tac x=z_var in allE)
   apply(cases "A z_var")
    apply(simp add:apply_adaptation_def)
   apply(clarsimp simp add:var_asm_not_written_def dma_def apply_adaptation_def)
  apply(rule inv\<^sub>6, clarsimp+)
  done

(* The "right-hand" side of inv_closed_glob_consistent *)
lemma inv_closed_glob_consistent_r:
  "inv \<langle>c', mds', mem\<rangle> \<Longrightarrow>
       \<forall>x. case A x of None \<Rightarrow> True | Some (v, v') \<Rightarrow> mem x \<noteq> v' \<longrightarrow> \<not> var_asm_not_written mds' x \<Longrightarrow>
       \<forall>x. dma mem [\<parallel>\<^sub>2 A] x \<noteq> dma mem x \<longrightarrow> \<not> var_asm_not_written mds' x  \<Longrightarrow>
       inv \<langle>c', mds', mem [\<parallel>\<^sub>2 A]\<rangle>"
  apply(erule inv.cases)
               using inv.intros apply (force, force, force, force, force, force, force, force, force)
        apply(rule inv\<^sub>3', clarsimp+)
        apply(erule_tac x=z_var in allE)
        apply(erule_tac x=z_var in allE)
        apply(cases "A z_var")
         apply(simp add:apply_adaptation_def)
        apply(clarsimp simp add:var_asm_not_written_def dma_def apply_adaptation_def)
       apply(rule inv\<^sub>4, clarsimp+)
       apply(erule_tac x=z_var in allE)
       apply(erule_tac x=z_var in allE)
       apply(cases "A z_var")
        apply(simp add:apply_adaptation_def)
       apply(clarsimp simp add:var_asm_not_written_def dma_def apply_adaptation_def)
      apply(rule inv\<^sub>4', clarsimp+)
      apply(erule_tac x=z_var in allE)
      apply(erule_tac x=z_var in allE)
      apply(cases "A z_var")
       apply(simp add:apply_adaptation_def)
      apply(clarsimp simp add:var_asm_not_written_def dma_def apply_adaptation_def)
     apply(rule inv\<^sub>5, clarsimp+)
     apply(erule_tac x=z_var in allE)
     apply(erule_tac x=z_var in allE)
     apply(cases "A z_var")
      apply(simp add:apply_adaptation_def)
     apply(clarsimp simp add:var_asm_not_written_def dma_def apply_adaptation_def)
    apply(rule inv\<^sub>5_then, clarsimp+)
    apply(erule_tac x=z_var in allE)
    apply(erule_tac x=z_var in allE)
    apply(cases "A z_var")
     apply(simp add:apply_adaptation_def)
    apply(clarsimp simp add:var_asm_not_written_def dma_def apply_adaptation_def)
   apply(rule inv\<^sub>5_else, clarsimp+)
   apply(erule_tac x=z_var in allE)
   apply(erule_tac x=z_var in allE)
   apply(cases "A z_var")
    apply(simp add:apply_adaptation_def)
   apply(clarsimp simp add:var_asm_not_written_def dma_def apply_adaptation_def)
  apply(rule inv\<^sub>6, clarsimp+)
  done

lemma rel_inv_closed_glob_consistent:
  "(\<langle>c, mds', mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv \<Longrightarrow>
       \<forall>x a b.
          A x = Some (a, b) \<longrightarrow>
          (mem x = a \<longrightarrow> mem' x \<noteq> b) \<longrightarrow> \<not> var_asm_not_written mds' x \<Longrightarrow>
       \<forall>x. dma mem [\<parallel>\<^sub>1 A] x \<noteq> dma mem x \<longrightarrow> \<not> var_asm_not_written mds' x \<Longrightarrow>
       \<forall>x. dma mem [\<parallel>\<^sub>1 A] x = Low \<and> (x \<in> mds' AsmNoReadOrWrite \<longrightarrow> x \<in> \<C>) \<longrightarrow>
           mem [\<parallel>\<^sub>1 A] x = mem' [\<parallel>\<^sub>2 A] x \<Longrightarrow>
       (\<langle>c, mds', mem [\<parallel>\<^sub>1 A]\<rangle>, \<langle>c', mds', mem' [\<parallel>\<^sub>2 A]\<rangle>) \<in> rel_inv"
  apply(safe elim!: rel_inv.cases)
  using rel_inv.intros dma_def by simp+

lemma R_closed_glob_consistent:
  "closed_glob_consistent R"
  unfolding closed_glob_consistent_def
  apply clarsimp
  apply(erule R.cases)
  apply clarsimp
  apply(rule R.intros)
      apply simp
     apply(fastforce simp: low_mds_eq_def)
    apply(rule inv_closed_glob_consistent)
      apply simp
     apply(fastforce split: option.splits)
    apply assumption
   apply(rule inv_closed_glob_consistent_r)
     apply simp
    apply(fastforce split: option.splits)
   apply(subgoal_tac "dma mem = dma mem' \<and> dma mem [\<parallel>\<^sub>1 A] = dma mem' [\<parallel>\<^sub>2 A]")
    apply simp
   apply(rule conjI)
    apply(rule low_mds_eq_dma)
    apply assumption
   apply(rule dma_\<C>)
   apply(simp add: \<C>_Low)
  apply(rule rel_inv_closed_glob_consistent)
  apply(simp_all)
  apply(fastforce split: option.splits)
  done

lemma R_low_mds_eq:
  "(\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>) \<in> R \<Longrightarrow>
        low_mds_eq mds mem\<^sub>1 mem\<^sub>2"
  apply(blast elim: R.cases)
  done

inductive_cases rel_invE: "(\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv"

method seq_decl_inv_method uses inv_rule rel_inv_rule =
 (erule rel_invE, simp+,
   drule seq_elim, simp,
   clarsimp,
   drule upd_elim, drule skip_elim, simp,
   clarsimp,
   rule conjI,
    rule eval\<^sub>w.seq,
    rule decl_eval\<^sub>w', simp+,
   rule R.intros, simp+,
      fastforce simp: low_mds_eq_def,
     rule inv_rule, simp+,
    rule inv_rule, simp+,
   rule rel_inv_rule, simp+)

method seq_stop_inv_method uses inv_rule rel_inv_rule =
 (erule rel_invE, simp+,
   drule seq_stop_elim, clarsimp,
   (rule exI)+,
   intro conjI,
    rule seq_stop_eval\<^sub>w,
   rule R.intros, (simp|clarsimp simp: low_mds_eq_def dma_def ev\<^sub>A.simps)+,
     (rule inv_rule; simp|blast?),
    (rule inv_rule; simp|blast?),
   rule rel_inv_rule, simp+)

method assign_inv\<^sub>6_method =
 (rule_tac x=c\<^sub>1' in exI,
  rule_tac x="mem\<^sub>2(x_var := ev\<^sub>A mem\<^sub>2 (Load y_var))" in exI,
  erule rel_invE, simp+,
  (drule assign_elim, simp,
   rule conjI,
    clarsimp simp: ev\<^sub>A.simps,
    erule_tac x=z_var in allE,
    clarsimp,
    rule assign_eval\<^sub>w', simp+,
    clarsimp simp: ev\<^sub>A.simps,
   rule R.intros, simp+,
      unfold low_mds_eq_def dma_def, clarsimp simp: ev\<^sub>A.simps,
     (rule inv\<^sub>6, simp+)+,
   rule rel_inv_z_initted, simp+)+)

lemma R_inv:
  notes ev\<^sub>A.simps[simp del]
  shows  "(\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>) \<in> R \<Longrightarrow>
       (\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>) \<in> eval\<^sub>w \<Longrightarrow>
       \<exists>c\<^sub>2' mem\<^sub>2'.
          (\<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>, \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>) \<in> eval\<^sub>w \<and>
          (\<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>, \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>) \<in> R"
  apply(erule R.cases)
  apply clarsimp
  apply(erule inv.cases)
                 apply(unfold prog_high_branch_def)
                 apply clarsimp
                 apply(rule_tac x=c\<^sub>1' in exI)
                 apply(rule_tac x=mem\<^sub>2 in exI)
                 apply(seq_decl_inv_method inv_rule:inv\<^sub>1h' rel_inv_rule:rel_inv_start)
                apply(seq_stop_inv_method inv_rule:inv\<^sub>1y rel_inv_rule:rel_inv_start)
               apply(rule_tac x=c\<^sub>1' in exI)
               apply(rule_tac x=mem\<^sub>2 in exI)
               apply(seq_decl_inv_method inv_rule:inv\<^sub>1y' rel_inv_rule:rel_inv_start)
              apply(seq_stop_inv_method inv_rule:inv\<^sub>1z rel_inv_rule:rel_inv_start)
             apply(rule_tac x=c\<^sub>1' in exI)
             apply(rule_tac x=mem\<^sub>2 in exI)
             apply(seq_decl_inv_method inv_rule:inv\<^sub>1z' rel_inv_rule:rel_inv_z_locked)
            apply(seq_stop_inv_method inv_rule:inv\<^sub>2 rel_inv_rule:rel_inv_z_locked)

           (* Assign ;; *)
           apply clarsimp
           apply(rule_tac x=c\<^sub>1' in exI)
           apply(rule_tac x="mem\<^sub>2(y_var := ev\<^sub>A mem\<^sub>2 (Const 0))" in exI)
           apply(erule rel_invE, simp+)
            apply(drule seq_elim, simp)
            apply clarsimp
            apply(drule assign_elim, simp)
            apply(rule conjI)
             apply(rule eval\<^sub>w.seq)
             apply(rule assign_eval\<^sub>w)
            apply(rule R.intros, simp+)
               apply(unfold low_mds_eq_def dma_def, clarsimp simp: ev\<^sub>A.simps)
              apply(rule inv\<^sub>2', fast+)+
            apply(rule rel_inv_z_locked, simp+)
          apply(seq_stop_inv_method inv_rule:inv\<^sub>3 rel_inv_rule:rel_inv_z_locked)

         (* Assign ;; *)
         apply clarsimp
         apply(rule_tac x=c\<^sub>1' in exI)
         apply(rule_tac x="mem\<^sub>2(z_var := ev\<^sub>A mem\<^sub>2 (Const 0))" in exI)
         apply(erule rel_invE, simp+)
          apply(drule seq_elim, simp)
          apply clarsimp
          apply(drule assign_elim, simp)
          apply(rule conjI)
           apply(rule eval\<^sub>w.seq)
           apply(rule assign_eval\<^sub>w)
          apply(rule R.intros, simp+)
             apply(unfold low_mds_eq_def dma_def, clarsimp simp: ev\<^sub>A.simps)
            apply(rule inv\<^sub>3', simp+)
            apply(simp add: ev\<^sub>A.simps)
           apply(rule inv\<^sub>3', simp+)
           apply(simp add: ev\<^sub>A.simps)
          apply(rule rel_inv_z_initted, simp+)
            apply(simp add: ev\<^sub>A.simps) (* do it separately so it doesn't mess everything else up *)
           apply simp+
        apply(seq_stop_inv_method inv_rule:inv\<^sub>4 rel_inv_rule:rel_inv_z_initted)

       (* Assign ;; *)
       apply clarsimp
       apply(rule_tac x=c\<^sub>1' in exI)
       apply(rule_tac x="mem\<^sub>2(x_var := ev\<^sub>A mem\<^sub>2 (Load y_var))" in exI)
       apply(erule rel_invE, simp+)
       apply(drule seq_elim, simp)
       apply clarsimp
       apply(drule assign_elim, simp)
       apply(rule conjI)
        apply(rule eval\<^sub>w.seq)
        apply(rule assign_eval\<^sub>w)
       apply(rule R.intros, simp+)
          apply(unfold low_mds_eq_def dma_def, clarsimp simp: ev\<^sub>A.simps)
         apply(rule inv\<^sub>4', simp+)+
       apply(rule rel_inv_z_initted, simp+)
      apply(seq_stop_inv_method inv_rule:inv\<^sub>5 rel_inv_rule:rel_inv_z_initted)

     (* (If (cond) Then Else) *)
     apply clarsimp
     apply(erule rel_invE, simp+)
     apply(rule_tac x="(if ev\<^sub>B mem\<^sub>2 (Neq (Load h_var) (Const 0))
                        then (x_var \<leftarrow> Load y_var)
                        else (x_var \<leftarrow> Add (Load y_var) (Load z_var)))" in exI)
     apply(rule_tac x="mem\<^sub>2" in exI)
     apply(rule conjI)
       apply(erule if_elim)
        apply clarify
        apply(rule if_eval\<^sub>w)
       apply clarify
       apply(rule if_eval\<^sub>w)
      apply(erule if_elim)
       apply(rule R.intros, simp+)
          apply(unfold low_mds_eq_def dma_def, clarsimp)
         apply(rule inv\<^sub>5_then, simp+)
        apply safe
         apply(rule inv\<^sub>5_then, simp+)
        apply(rule inv\<^sub>5_else, simp+)
       apply safe
        apply(rule rel_inv_z_initted, simp+)
       apply(rule rel_inv_if, simp, simp, simp, simp, simp)
 
     apply(rule R.intros, simp+)
        apply(unfold low_mds_eq_def dma_def, clarsimp)
       apply(rule inv\<^sub>5_else, simp+)
      apply safe
       apply(rule inv\<^sub>5_then, simp+)
      apply(rule inv\<^sub>5_else, simp+)
     apply safe
      apply(rule rel_inv_if', simp+)
     apply(rule rel_inv_z_initted, simp+)
 
    (* Assign - then | else *)
    apply assign_inv\<^sub>6_method
   apply assign_inv\<^sub>6_method
  using stop_no_eval by fastforce

lemma strong_low_bisim_mm_R:
  "strong_low_bisim_mm R"
  unfolding strong_low_bisim_mm_def
  proof(safe)
    show "sym R" by(rule R_sym)
  next
    show "closed_glob_consistent R" by(rule R_closed_glob_consistent)
  next
    fix c\<^sub>1 mds mem\<^sub>1 c\<^sub>2  mem\<^sub>2
    assume "(\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>) \<in> R"
    thus "low_mds_eq mds mem\<^sub>1 mem\<^sub>2"
    by(rule R_low_mds_eq)
  next
    fix c\<^sub>1 mds mem\<^sub>1 c\<^sub>2  mem\<^sub>2 c\<^sub>1' mds' mem\<^sub>1'
    assume "(\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>) \<in> R"
       and "(\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>) \<in> eval\<^sub>w"
    thus "\<exists>c\<^sub>2' mem\<^sub>2'.
          (\<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>, \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>) \<in> eval\<^sub>w \<and>
          (\<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>, \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>) \<in> R"
       by(rule R_inv)
  qed

lemma prog_high_branch_secure':
  "low_mds_eq mds\<^sub>s mem\<^sub>1 mem\<^sub>2 \<Longrightarrow>
       (\<langle>prog_high_branch, mds\<^sub>s, mem\<^sub>1\<rangle>, \<langle>prog_high_branch, mds\<^sub>s, mem\<^sub>2\<rangle>) \<in> R"
  apply(rule R.intros)
  apply simp_all
   apply(rule inv\<^sub>1h)
     apply (simp_all add: mds\<^sub>s_def)
   apply(rule inv\<^sub>1h)
     apply (simp_all add: mds\<^sub>s_def)
  unfolding prog_high_branch_def by(fastforce intro: rel_inv.intros)

lemma "com_sifum_secure (prog_high_branch, mds\<^sub>s)"
  unfolding com_sifum_secure_def low_indistinguishable_def
  apply clarify
  apply(rule mm_equiv_intro)
    apply(rule strong_low_bisim_mm_R)
   apply force
  by (rule prog_high_branch_secure')

end

locale sifum_refinement_high_branch =
  A: sifum_example +
  C: sifum_example\<^sub>C

primrec var\<^sub>C_of :: "var \<Rightarrow> var\<^sub>C"
where
  "var\<^sub>C_of h_var = h_mem" |
  "var\<^sub>C_of x_var = x_mem" |
  "var\<^sub>C_of y_var = y_mem" |
  "var\<^sub>C_of z_var = z_mem"

sublocale sifum_refinement_high_branch \<subseteq>
  sifum_refinement dma dma\<^sub>C \<C>_vars \<C>_vars \<C> \<C>\<^sub>C A.eval\<^sub>w C.eval\<^sub>w undefined var\<^sub>C_of
  apply(unfold_locales)
     apply(rule inj_onI, simp)
     apply(case_tac x)
         apply(case_tac y, simp+)
        apply(case_tac y, simp+)
       apply(case_tac y, simp+)
      apply(case_tac y, simp+)
    apply(case_tac x\<^sub>A)
       apply(clarsimp simp:dma_def dma\<^sub>C_def)+
  done

context sifum_refinement_high_branch begin

(* Adapted these helpers from Eg1Eg2 *)
lemma conc_only_vars_not_visible_abs:
  "(\<forall>v\<^sub>C. v\<^sub>C \<in> range var\<^sub>C_of \<longrightarrow> mem\<^sub>C v\<^sub>C = mem\<^sub>C' v\<^sub>C) \<Longrightarrow> mem\<^sub>A_of mem\<^sub>C = mem\<^sub>A_of mem\<^sub>C'"
  by (simp add: mem\<^sub>A_of_def)

lemma conc_only_var_assign_not_visible_abs:
  "\<forall>v\<^sub>C e. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> mem\<^sub>A_of mem\<^sub>C = mem\<^sub>A_of (mem\<^sub>C(v\<^sub>C := e))"
  using conc_only_vars_not_visible_abs
  by simp

lemma reg_is_not_the_var\<^sub>C_of_anything:
  "reg \<in> {reg0, reg1, reg2, reg3} \<Longrightarrow> reg = var\<^sub>C_of x \<Longrightarrow> False"
  by (induct x, clarsimp+)

lemma reg_not_visible_abs:
  "reg \<in> {reg0, reg1, reg2, reg3} \<Longrightarrow> reg \<notin> range var\<^sub>C_of"
  using reg_is_not_the_var\<^sub>C_of_anything
  by blast

(* Helpers carried over from Eg1Eg2 *)
lemma assign_eval\<^sub>w_load\<^sub>A:
  shows "(\<langle>x \<leftarrow> Load y, mds, mem\<rangle>\<^sub>A, \<langle>Stop, mds, mem (x := mem y)\<rangle>\<^sub>A) \<in> A.eval\<^sub>w"
  by (metis A.assign_eval\<^sub>w ev\<^sub>A.simps(2))

lemma assign_eval\<^sub>w_load\<^sub>C:
  shows "(\<langle>x \<leftarrow> Load y, mds, mem\<rangle>\<^sub>C, \<langle>Stop, mds, mem (x := mem y)\<rangle>\<^sub>C) \<in> C.eval\<^sub>w"
  using C.unannotated[OF C.assign, where E="[]", simplified]
  apply(drule_tac x=x in meta_spec)
  apply(drule_tac x="Load y" in meta_spec)
  apply(drule_tac x=mds in meta_spec)
  apply(drule_tac x=mem in meta_spec)
  apply clarsimp
  done

lemma assign_eval\<^sub>w_const\<^sub>A:
  shows "(\<langle>x \<leftarrow> Const c, mds, mem\<rangle>, \<langle>Stop, mds, mem (x := c)\<rangle>) \<in> A.eval\<^sub>w"
  by (metis A.assign_eval\<^sub>w ev\<^sub>A.simps(1))

lemma assign_eval\<^sub>w_const\<^sub>C:
  shows "(\<langle>x \<leftarrow> Const c, mds, mem\<rangle>, \<langle>Stop, mds, mem (x := c)\<rangle>) \<in> C.eval\<^sub>w"
  using C.unannotated[OF C.assign, where E="[]", simplified]
  apply(drule_tac x=x in meta_spec)
  apply(drule_tac x="Const c" in meta_spec)
  apply(drule_tac x=mds in meta_spec)
  apply(drule_tac x=mem in meta_spec)
  apply clarsimp
  done

lemma mem_assign_refinement_helper_var:
  "mem\<^sub>A_of (mem\<^sub>C (var\<^sub>C_of x := mem\<^sub>C (var\<^sub>C_of y)))
       = (mem\<^sub>A_of mem\<^sub>C) (x := (mem\<^sub>A_of mem\<^sub>C) y)"
  apply(clarsimp simp: mem\<^sub>A_of_def)
  apply(rule ext, clarsimp)
  apply(cases x)
   apply(case_tac x\<^sub>A, clarsimp+)+
  done

lemma mem_assign_refinement_helper_const:
  "mem\<^sub>A_of (mem\<^sub>C (var\<^sub>C_of x := c))
       = (mem\<^sub>A_of mem\<^sub>C) (x := c)"
  apply(clarsimp simp: mem\<^sub>A_of_def)
  apply(rule ext, clarsimp)
  apply(cases x)
      apply(case_tac x\<^sub>A, clarsimp+)+
  done

lemma NoRW\<^sub>A_implies_NoRW\<^sub>C:
  "x \<in> mds\<^sub>A_of mds\<^sub>C AsmNoReadOrWrite \<Longrightarrow>
   var\<^sub>C_of x \<in> mds\<^sub>C AsmNoReadOrWrite"
  unfolding mds\<^sub>A_of_def
  apply clarsimp
  apply (simp only: var\<^sub>C_of_def)
  apply clarsimp
  apply (simp add: f_inv_into_f)
  done

lemma NoWrite\<^sub>A_implies_NoWrite\<^sub>C:
  "x \<in> mds\<^sub>A_of mds\<^sub>C AsmNoWrite \<Longrightarrow>
   var\<^sub>C_of x \<in> mds\<^sub>C AsmNoWrite"
  unfolding mds\<^sub>A_of_def
  apply clarsimp
  apply (simp only: var\<^sub>C_of_def)
  apply clarsimp
  apply (simp add: f_inv_into_f)
  done

lemma if_seq_eval\<^sub>w_helper\<^sub>A:
  "(\<langle>If B T E, mds, mem\<rangle>,
    \<langle>if ev\<^sub>B mem B then T else E, mds, mem\<rangle>\<^sub>A) \<in> A.eval\<^sub>w
    \<Longrightarrow>
   (\<langle>If B T E ;; TAIL, mds, mem\<rangle>,
    \<langle>if ev\<^sub>B mem B then T ;; TAIL else E ;; TAIL, mds, mem\<rangle>\<^sub>A) \<in> A.eval\<^sub>w"
  using A.eval\<^sub>w.seq
  by auto

lemma if_seq_eval\<^sub>w_helper\<^sub>C:
  "(\<langle>If B T E, mds, mem\<rangle>,
    \<langle>if ev\<^sub>B\<^sub>C mem B then T else E, mds, mem\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
    \<Longrightarrow>
   (\<langle>If B T E ;; TAIL, mds, mem\<rangle>,
    \<langle>if ev\<^sub>B\<^sub>C mem B then T ;; TAIL else E ;; TAIL, mds, mem\<rangle>\<^sub>C) \<in> C.eval\<^sub>w"
  using C.eval\<^sub>w.seq
  by auto

fun aexp\<^sub>C_of
where
  "aexp\<^sub>C_of (Const c) = (Const c)" |
  "aexp\<^sub>C_of (Load v) = (Load (var\<^sub>C_of v))" |
  "aexp\<^sub>C_of (Add x y) = Add (aexp\<^sub>C_of x) (aexp\<^sub>C_of y)" |
  "aexp\<^sub>C_of (Sub x y) = Sub (aexp\<^sub>C_of x) (aexp\<^sub>C_of y)"

lemma mem_assign_refinement_helper_aexp:
  "mem\<^sub>A_of (mem\<^sub>C (var\<^sub>C_of x := ev\<^sub>A mem\<^sub>C (aexp\<^sub>C_of a)))
       = (mem\<^sub>A_of mem\<^sub>C) (x := ev\<^sub>A (mem\<^sub>A_of mem\<^sub>C) a)"
  apply(clarsimp simp: mem\<^sub>A_of_def)
  apply(rule ext, clarsimp)
  apply(rename_tac x\<^sub>A)
  apply(cases x)
     apply(case_tac x\<^sub>A, induct rule:aexp\<^sub>C_of.induct, clarsimp+)
    apply(case_tac x\<^sub>A, induct rule:aexp\<^sub>C_of.induct, clarsimp+)
      apply(induct rule:aexp\<^sub>C_of.induct, clarsimp+)
   apply(case_tac x\<^sub>A, induct rule:aexp\<^sub>C_of.induct, clarsimp+)
    apply(induct rule:aexp\<^sub>C_of.induct, clarsimp+)
  apply(case_tac x\<^sub>A, induct rule:aexp\<^sub>C_of.induct, clarsimp+)
  apply(induct rule:aexp\<^sub>C_of.induct, clarsimp+)
  done

inductive_set rel_inv\<^sub>C :: "(((var\<^sub>C addr, var\<^sub>C aexp, var\<^sub>C bexp) Stmt, var\<^sub>C addr, val) LocalConf) rel"
where
  rel_inv\<^sub>C_1:
  "\<lbrakk>c =
     Skip ;;
     Skip ;;
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    c' =
     Assign reg1 (Load y_mem) ;;
     Assign reg2 (Load z_mem) ;;
     Assign reg0 (Add (Load reg1) (Load reg2)) ;;
     Assign x_mem (Load reg0)\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv\<^sub>C" |
  rel_inv\<^sub>C_1':
  "\<lbrakk>c' =
     Skip ;;
     Skip ;;
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    c =
     Assign reg1 (Load y_mem) ;;
     Assign reg2 (Load z_mem) ;;
     Assign reg0 (Add (Load reg1) (Load reg2)) ;;
     Assign x_mem (Load reg0)\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv\<^sub>C" |
  rel_inv\<^sub>C_2:
  "\<lbrakk>c =
     Stop ;;
     Skip ;;
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    c' =
     Stop ;;
     Assign reg2 (Load z_mem) ;;
     Assign reg0 (Add (Load reg1) (Load reg2)) ;;
     Assign x_mem (Load reg0)\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv\<^sub>C" |
  rel_inv\<^sub>C_2':
  "\<lbrakk>c' =
     Stop ;;
     Skip ;;
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    c =
     Stop ;;
     Assign reg2 (Load z_mem) ;;
     Assign reg0 (Add (Load reg1) (Load reg2)) ;;
     Assign x_mem (Load reg0)\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv\<^sub>C" |
  rel_inv\<^sub>C_3:
  "\<lbrakk>c =
     Skip ;;
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    c' =
     Assign reg2 (Load z_mem) ;;
     Assign reg0 (Add (Load reg1) (Load reg2)) ;;
     Assign x_mem (Load reg0)\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv\<^sub>C" |
  rel_inv\<^sub>C_3':
  "\<lbrakk>c' =
     Skip ;;
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    c =
     Assign reg2 (Load z_mem) ;;
     Assign reg0 (Add (Load reg1) (Load reg2)) ;;
     Assign x_mem (Load reg0)\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv\<^sub>C" |
  rel_inv\<^sub>C_4:
  "\<lbrakk>c =
     Stop ;;
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    c' =
     Stop ;;
     Assign reg0 (Add (Load reg1) (Load reg2)) ;;
     Assign x_mem (Load reg0)\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv\<^sub>C" |
  rel_inv\<^sub>C_4':
  "\<lbrakk>c' =
     Stop ;;
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    c =
     Stop ;;
     Assign reg0 (Add (Load reg1) (Load reg2)) ;;
     Assign x_mem (Load reg0)\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv\<^sub>C" |
  rel_inv\<^sub>C_5:
  "\<lbrakk>c =
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    c' =
     Assign reg0 (Add (Load reg1) (Load reg2)) ;;
     Assign x_mem (Load reg0)\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv\<^sub>C" |
  rel_inv\<^sub>C_5':
  "\<lbrakk>c' =
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    c =
     Assign reg0 (Add (Load reg1) (Load reg2)) ;;
     Assign x_mem (Load reg0)\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv\<^sub>C" |
  rel_inv\<^sub>C_default:
  "\<lbrakk>c = c'; C.no_await c; c \<in> C.adhoc_unpeel C.prog_high_branch\<^sub>C\<rbrakk> \<Longrightarrow>
      (\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> rel_inv\<^sub>C"

inductive_cases rel_inv\<^sub>CE: "(\<langle>c, mds, mem\<rangle>\<^sub>C, \<langle>c', mds', mem'\<rangle>\<^sub>C) \<in> rel_inv\<^sub>C"

lemma rel_inv\<^sub>C_sym:
  "sym rel_inv\<^sub>C"
  unfolding sym_def
  apply(auto elim!: rel_inv\<^sub>C.cases intro: rel_inv\<^sub>C.intros)
  done

lemma rel_inv\<^sub>C_closed_glob_consistent:
  "C.closed_glob_consistent rel_inv\<^sub>C"
  unfolding C.closed_glob_consistent_def
  apply(safe elim!: rel_inv\<^sub>C.cases)
  using rel_inv\<^sub>C.intros by simp+

inductive_set RefRel_HighBranch :: "(
    (((var, var aexp, var bexp) Stmt \<times> (Mode \<Rightarrow> var set)) \<times> (var \<Rightarrow> val)) \<times>
     ((var\<^sub>C, var\<^sub>C aexp, var\<^sub>C bexp) Stmt \<times> (Mode \<Rightarrow> var\<^sub>C set)) \<times> (var\<^sub>C \<Rightarrow> val)
    ) set"
where
  acq_mode_rel: "\<lbrakk>c\<^sub>A = ModeDecl Skip (Acq x SomeMode) ;; c\<^sub>A_tail;
    c\<^sub>C = ModeDecl Skip (Acq (var\<^sub>C_of x) SomeMode) ;; c\<^sub>C_tail;
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A''\<rangle>\<^sub>A, \<langle>Stop ;; c\<^sub>A_tail, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A) \<in> A.eval\<^sub>w \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>Stop ;; c\<^sub>A_tail, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  assign_load_rel: "\<lbrakk>c\<^sub>A = (x \<leftarrow> aexp.Load y) ;; c\<^sub>A_tail;
    c\<^sub>C = ((var\<^sub>C_of x) \<leftarrow> aexp.Load (var\<^sub>C_of y)) ;; c\<^sub>C_tail;
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    x \<notin> mds\<^sub>A GuarNoReadOrWrite;
    x \<notin> mds\<^sub>A GuarNoWrite;
    y \<notin> mds\<^sub>A GuarNoReadOrWrite;
    x \<notin> \<C>;
    y \<notin> \<C>;
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A''\<rangle>\<^sub>A, \<langle>Stop ;; c\<^sub>A_tail, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A) \<in> A.eval\<^sub>w \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>Stop ;; c\<^sub>A_tail, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  assign_const_rel: "\<lbrakk>c\<^sub>A = (x \<leftarrow> Const z) ;; c\<^sub>A_tail;
    c\<^sub>C = ((var\<^sub>C_of x) \<leftarrow> Const z) ;; c\<^sub>C_tail;
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    x \<notin> mds\<^sub>A GuarNoReadOrWrite;
    x \<notin> mds\<^sub>A GuarNoWrite;
    x \<notin> \<C>;
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A''\<rangle>\<^sub>A, \<langle>Stop ;; c\<^sub>A_tail, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A) \<in> A.eval\<^sub>w \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>Stop ;; c\<^sub>A_tail, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_reg_load_rel: "\<lbrakk>
    c\<^sub>A = (If (Neq (Load x) (Const 0)) c\<^sub>A_then c\<^sub>A_else);
    c\<^sub>C = (reg3 \<leftarrow> (Load (var\<^sub>C_of x))) ;; ((If (Neq (Load reg3) (Const 0)) c\<^sub>C_then c\<^sub>C_else));
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    x \<in> mds\<^sub>A AsmNoWrite \<and> x \<notin> mds\<^sub>A AsmNoReadOrWrite;
    reg3 \<notin> mds\<^sub>C GuarNoReadOrWrite \<and> reg3 \<notin> mds\<^sub>C GuarNoWrite;
    x \<notin> mds\<^sub>A GuarNoReadOrWrite;
    \<forall>x'. x \<in> \<C>_vars x' \<longrightarrow> x' \<notin> mds\<^sub>A GuarNoReadOrWrite;
    (\<forall>mem\<^sub>A' mem\<^sub>C' mem\<^sub>A'' mem\<^sub>C''.
        (
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (* The change in mem\<^sub>C does not affect mem\<^sub>A *)
         mem\<^sub>A'' = mem\<^sub>A' \<and>
         (* The concrete and abstract versions of the bool condition must become consistent *)
         ev\<^sub>B mem\<^sub>C' (Neq (Load reg3) (Const 0)) = ev\<^sub>B mem\<^sub>A' (Neq (Load x) (Const 0)) \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C,
          \<langle>Stop ;; (If (Neq (Load reg3) (Const 0)) c\<^sub>C_then c\<^sub>C_else), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A''\<rangle>\<^sub>A,
          \<langle>Stop ;; (If (Neq (Load reg3) (Const 0)) c\<^sub>C_then c\<^sub>C_else), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch)
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_reg_stop_rel: "\<lbrakk>
    c\<^sub>A = (If (Neq (Load x) (Const 0)) c\<^sub>A_then c\<^sub>A_else);
    c\<^sub>C = Stop ;; ((If (Neq (Load reg3) (Const 0)) c\<^sub>C_then c\<^sub>C_else));
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    x \<in> mds\<^sub>A AsmNoWrite \<and> x \<notin> mds\<^sub>A AsmNoReadOrWrite;
    (* We will need to carry this through to the if_reg_rel case. *)
    ev\<^sub>B mem\<^sub>C (Neq (Load reg3) (Const 0)) = ev\<^sub>B mem\<^sub>A (Neq (Load x) (Const 0));
    (\<forall>mem\<^sub>A' mem\<^sub>C' mem\<^sub>A'' mem\<^sub>C''.
        (
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (* The change in mem\<^sub>C does not affect mem\<^sub>A *)
         mem\<^sub>A'' = mem\<^sub>A' \<and>
         (* The concrete and abstract versions of the bool condition must remain consistent *)
         ev\<^sub>B mem\<^sub>C' (Neq (Load reg3) (Const 0)) = ev\<^sub>B mem\<^sub>A' (Neq (Load x) (Const 0)) \<and>
         ev\<^sub>B mem\<^sub>C'' (Neq (Load reg3) (Const 0)) = ev\<^sub>B mem\<^sub>A'' (Neq (Load x) (Const 0)) \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C,
          \<langle>(If (Neq (Load reg3) (Const 0)) c\<^sub>C_then c\<^sub>C_else), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A''\<rangle>\<^sub>A,
          \<langle>(If (Neq (Load reg3) (Const 0)) c\<^sub>C_then c\<^sub>C_else), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch)
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_reg_rel: "\<lbrakk>
(* Is a more generic version possible for an arbitrary b\<^sub>A?
    c\<^sub>A = (If b\<^sub>A c\<^sub>A_then c\<^sub>A_else);
    b\<^sub>A = Eq (Load x) (Const 0); (* ev\<^sub>B mem\<^sub>A b\<^sub>A *)
*)
    c\<^sub>A = (If (Neq (Load x) (Const 0)) c\<^sub>A_then c\<^sub>A_else);
    c\<^sub>C = (If (Neq (Load reg3) (Const 0)) c\<^sub>C_then c\<^sub>C_else);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    x \<in> mds\<^sub>A AsmNoWrite \<and> x \<notin> mds\<^sub>A AsmNoReadOrWrite;
    (* The concrete and abstract versions of the bool condition must have remained consistent *)
    ev\<^sub>B mem\<^sub>C (Neq (Load reg3) (Const 0)) = ev\<^sub>B mem\<^sub>A (Neq (Load x) (Const 0));
    (* As for if_reg_load_rel *)
    reg3 \<notin> mds\<^sub>C GuarNoReadOrWrite \<and> reg3 \<notin> mds\<^sub>C GuarNoWrite;
    x \<notin> mds\<^sub>A GuarNoReadOrWrite;
    \<forall>x'. x \<in> \<C>_vars x' \<longrightarrow> x' \<notin> mds\<^sub>A GuarNoReadOrWrite;
    (* As we would expect, the two branches must be related by the coupling invariant. *)
    \<forall>mem\<^sub>C' mem\<^sub>C''. (\<langle>c\<^sub>C_then, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C, \<langle>c\<^sub>C_else, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C) \<in> rel_inv\<^sub>C;
    (\<forall>mem\<^sub>A' mem\<^sub>C' mem\<^sub>A'' mem\<^sub>C''.
       mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
       mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
       ev\<^sub>B mem\<^sub>C'' (Neq (Load reg3) (Const 0)) = ev\<^sub>B mem\<^sub>A'' (Neq (Load x) (Const 0)) \<and>
       ev\<^sub>B mem\<^sub>C' (Neq (Load reg3) (Const 0)) = ev\<^sub>B mem\<^sub>A' (Neq (Load x) (Const 0)) \<and>
       (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A''\<rangle>\<^sub>A,
        \<langle>(if (ev\<^sub>B mem\<^sub>A'' (Neq (Load x) (Const 0))) then c\<^sub>A_then else c\<^sub>A_else), mds\<^sub>A, mem\<^sub>A'\<rangle>\<^sub>A) \<in> A.eval\<^sub>w \<and>
       (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C,
        \<langle>(if (ev\<^sub>B mem\<^sub>C'' (Neq (Load reg3) (Const 0))) then c\<^sub>C_then else c\<^sub>C_else), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
       \<longrightarrow>
       (\<langle>(if (ev\<^sub>B mem\<^sub>A'' (Neq (Load x) (Const 0))) then c\<^sub>A_then else c\<^sub>A_else), mds\<^sub>A, mem\<^sub>A'\<rangle>\<^sub>A,
        \<langle>(if (ev\<^sub>B mem\<^sub>C'' (Neq (Load reg3) (Const 0))) then c\<^sub>C_then else c\<^sub>C_else), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch)
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

(*
  Assign x_var (Load y_var)
    - to -
  Skip ;;
  Skip ;;
  Assign reg0 (Load y_mem) ;;
  Assign x_mem (Load reg0);
*)
  if_then_rel_1: "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Load y_var);
    c\<^sub>C = Skip ;; c\<^sub>C_tail;
    c\<^sub>C_tail =
     Skip ;;
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_then_rel_1': "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Load y_var);
    c\<^sub>C = Stop ;; c\<^sub>C_tail;
    c\<^sub>C_tail =
     Skip ;;
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_then_rel_2: "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Load y_var);
    c\<^sub>C = Skip ;; c\<^sub>C_tail;
    c\<^sub>C_tail =
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_then_rel_2': "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Load y_var);
    c\<^sub>C = Stop ;; c\<^sub>C_tail;
    c\<^sub>C_tail =
     Assign reg0 (Load y_mem) ;;
     Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_then_rel_3: "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Load y_var);
    c\<^sub>C = Assign reg0 (Load y_mem) ;; c\<^sub>C_tail;
    c\<^sub>C_tail = Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    y_var \<in> mds\<^sub>A AsmNoWrite \<and> y_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (* The value of reg0 and y_var must now become consistent *)
         ev\<^sub>A mem\<^sub>C' (Load reg0) = ev\<^sub>A mem\<^sub>A' (Load y_var) \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_then_rel_3': "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Load y_var);
    c\<^sub>C = Stop ;; c\<^sub>C_tail;
    c\<^sub>C_tail = Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    y_var \<in> mds\<^sub>A AsmNoWrite \<and> y_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    (* The value of reg0 and y_var at this point must be consistent *)
    ev\<^sub>A mem\<^sub>C (Load reg0) = ev\<^sub>A mem\<^sub>A (Load y_var);
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (* and must remain consistent *)
         ev\<^sub>A mem\<^sub>C' (Load reg0) = ev\<^sub>A mem\<^sub>A' (Load y_var) \<and>
         ev\<^sub>A mem\<^sub>C'' (Load reg0) = ev\<^sub>A mem\<^sub>A'' (Load y_var) \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_then_rel_4: "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Load y_var);
    c\<^sub>C = Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    y_var \<in> mds\<^sub>A AsmNoWrite \<and> y_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    (* The value of reg0 and y_var at this point must be consistent *)
    ev\<^sub>A mem\<^sub>C (Load reg0) = ev\<^sub>A mem\<^sub>A (Load y_var);
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A''\<rangle>\<^sub>A, \<langle>Stop, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A) \<in> A.eval\<^sub>w \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>Stop, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>Stop, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>Stop, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

(*
  Assign x_var (Add (Load y_var) (Load z_var))
    - to -
  Assign reg1 (Load y_mem) ;;
  Assign reg2 (Load z_mem) ;;
  Assign reg0 (Add (Load reg1) (Load reg2)) ;;
  Assign x_mem (Load reg0)
*)
  if_else_rel_1: "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Add (Load y_var) (Load z_var));
    c\<^sub>C = (reg1 \<leftarrow> Load y_mem) ;; c\<^sub>C_tail;
    c\<^sub>C_tail =
      Assign reg2 (Load z_mem) ;;
      Assign reg0 (Add (Load reg1) (Load reg2)) ;;
      Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    y_var \<in> mds\<^sub>A AsmNoWrite \<and> y_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (* Now reg1 takes on y_var *)
         ev\<^sub>A mem\<^sub>C' (Load reg1) = ev\<^sub>A mem\<^sub>A' (Load y_var) \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_else_rel_1': "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Add (Load y_var) (Load z_var));
    c\<^sub>C = Stop ;; c\<^sub>C_tail;
    c\<^sub>C_tail =
      Assign reg2 (Load z_mem) ;;
      Assign reg0 (Add (Load reg1) (Load reg2)) ;;
      Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    y_var \<in> mds\<^sub>A AsmNoWrite \<and> y_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    (* At this point reg1 contains y_var *)
    ev\<^sub>A mem\<^sub>C (Load reg1) = ev\<^sub>A mem\<^sub>A (Load y_var);
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (* maintain it *)
         ev\<^sub>A mem\<^sub>C' (Load reg1) = ev\<^sub>A mem\<^sub>A' (Load y_var) \<and>
         ev\<^sub>A mem\<^sub>C'' (Load reg1) = ev\<^sub>A mem\<^sub>A'' (Load y_var) \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_else_rel_2: "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Add (Load y_var) (Load z_var));
    c\<^sub>C = Assign reg2 (Load z_mem) ;; c\<^sub>C_tail;
    c\<^sub>C_tail =
      Assign reg0 (Add (Load reg1) (Load reg2)) ;;
      Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    y_var \<in> mds\<^sub>A AsmNoWrite \<and> y_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    (* At this point reg1 contains y_var *)
    ev\<^sub>A mem\<^sub>C (Load reg1) = ev\<^sub>A mem\<^sub>A (Load y_var);
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (* maintain it *)
         ev\<^sub>A mem\<^sub>C'' (Load reg1) = ev\<^sub>A mem\<^sub>A'' (Load y_var) \<and>
         ev\<^sub>A mem\<^sub>C' (Load reg1) = ev\<^sub>A mem\<^sub>A' (Load y_var) \<and>
         (* Now reg2 takes on z_var *)
         ev\<^sub>A mem\<^sub>C' (Load reg2) = ev\<^sub>A mem\<^sub>A' (Load z_var) \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_else_rel_2': "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Add (Load y_var) (Load z_var));
    c\<^sub>C = Stop ;; c\<^sub>C_tail;
    c\<^sub>C_tail =
      Assign reg0 (Add (Load reg1) (Load reg2)) ;;
      Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    y_var \<in> mds\<^sub>A AsmNoWrite \<and> y_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    z_var \<in> mds\<^sub>A AsmNoWrite \<and> z_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    (* At this point reg1 contains y_var and reg2 contains z_var *)
    ev\<^sub>A mem\<^sub>C (Load reg1) = ev\<^sub>A mem\<^sub>A (Load y_var);
    ev\<^sub>A mem\<^sub>C (Load reg2) = ev\<^sub>A mem\<^sub>A (Load z_var);
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (* maintain it *)
         ev\<^sub>A mem\<^sub>C'' (Load reg1) = ev\<^sub>A mem\<^sub>A'' (Load y_var) \<and>
         ev\<^sub>A mem\<^sub>C'' (Load reg2) = ev\<^sub>A mem\<^sub>A'' (Load z_var) \<and>
         ev\<^sub>A mem\<^sub>C' (Load reg1) = ev\<^sub>A mem\<^sub>A' (Load y_var) \<and>
         ev\<^sub>A mem\<^sub>C' (Load reg2) = ev\<^sub>A mem\<^sub>A' (Load z_var) \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_else_rel_3: "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Add (Load y_var) (Load z_var));
    c\<^sub>C = Assign reg0 (Add (Load reg1) (Load reg2)) ;; c\<^sub>C_tail;
    c\<^sub>C_tail = Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    y_var \<in> mds\<^sub>A AsmNoWrite \<and> y_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    z_var \<in> mds\<^sub>A AsmNoWrite \<and> z_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    (* At this point reg1 contains y_var and reg2 contains z_var *)
    ev\<^sub>A mem\<^sub>C (Load reg1) = ev\<^sub>A mem\<^sub>A (Load y_var);
    ev\<^sub>A mem\<^sub>C (Load reg2) = ev\<^sub>A mem\<^sub>A (Load z_var);
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         ev\<^sub>A mem\<^sub>C'' (Load reg1) = ev\<^sub>A mem\<^sub>A'' (Load y_var) \<and>
         ev\<^sub>A mem\<^sub>C'' (Load reg2) = ev\<^sub>A mem\<^sub>A'' (Load z_var) \<and>
         (* Thus the value of reg0 becomes consistent with y_var + z_var *)
         ev\<^sub>A mem\<^sub>C' (Load reg0) = ev\<^sub>A mem\<^sub>A' (Add (Load y_var) (Load z_var)) \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_else_rel_3': "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Add (Load y_var) (Load z_var));
    c\<^sub>C = Stop ;; c\<^sub>C_tail;
    c\<^sub>C_tail = Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    y_var \<in> mds\<^sub>A AsmNoWrite \<and> y_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    z_var \<in> mds\<^sub>A AsmNoWrite \<and> z_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    (* The value of reg0 and y_var + z_var at this point must be consistent *)
    ev\<^sub>A mem\<^sub>C (Load reg0) = ev\<^sub>A mem\<^sub>A (Add (Load y_var) (Load z_var));
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (* and must remain consistent *)
         ev\<^sub>A mem\<^sub>C' (Load reg0) = ev\<^sub>A mem\<^sub>A' (Add (Load y_var) (Load z_var)) \<and>
         ev\<^sub>A mem\<^sub>C'' (Load reg0) = ev\<^sub>A mem\<^sub>A'' (Add (Load y_var) (Load z_var)) \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>c\<^sub>A, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>c\<^sub>C_tail, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  if_else_rel_4: "\<lbrakk>c\<^sub>A = (x_var \<leftarrow> Add (Load y_var) (Load z_var));
    c\<^sub>C = Assign x_mem (Load reg0);
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C AsmNoReadOrWrite);
    y_var \<in> mds\<^sub>A AsmNoWrite \<and> y_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    z_var \<in> mds\<^sub>A AsmNoWrite \<and> z_var \<notin> mds\<^sub>A AsmNoReadOrWrite;
    (* The value of reg0 and y_var + z_var at this point must be consistent *)
    ev\<^sub>A mem\<^sub>C (Load reg0) = ev\<^sub>A mem\<^sub>A (Add (Load y_var) (Load z_var));
    \<forall>mds\<^sub>A' mem\<^sub>A' mem\<^sub>A'' mds\<^sub>C' mem\<^sub>C' mem\<^sub>C''.
        (
         mds\<^sub>A' = mds\<^sub>A_of mds\<^sub>C' \<and>
         mem\<^sub>A' = mem\<^sub>A_of mem\<^sub>C' \<and>
         mem\<^sub>A'' = mem\<^sub>A_of mem\<^sub>C'' \<and>
         (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A''\<rangle>\<^sub>A, \<langle>Stop, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A) \<in> A.eval\<^sub>w \<and>
         (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C''\<rangle>\<^sub>C, \<langle>Stop, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w
        )
         \<longrightarrow>
         (\<langle>Stop, mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>A, \<langle>Stop, mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch
    \<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  stop_seq_rel: "\<lbrakk>c\<^sub>A = Stop ;; c\<^sub>A_tail;
    c\<^sub>C = Stop ;; c\<^sub>C_tail;
    mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C;
    mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C;
    (\<langle>c\<^sub>A_tail, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C_tail, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch\<rbrakk>
    \<Longrightarrow>
    (\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" |

  stop_rel:
    "(\<langle>Stop, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A, \<langle>Stop, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch"

inductive_cases RefRelE: "(\<langle>c, mds, mem\<rangle>\<^sub>A, \<langle>c', mds', mem'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"

definition abs_steps' :: "(_,_,_) Stmt \<Rightarrow> (_,_,_) Stmt \<Rightarrow> nat" where
  "abs_steps' c\<^sub>A c\<^sub>C \<equiv>
    (if (\<exists> x then\<^sub>A else\<^sub>A then\<^sub>C else\<^sub>C.
        c\<^sub>A = (Stmt.If (Neq (Load x) (Const 0)) then\<^sub>A else\<^sub>A) \<and>
        (c\<^sub>C = ((reg3 \<leftarrow> Load (var\<^sub>C_of x)) ;; Stmt.If (Neq (Load reg3) (Const 0)) then\<^sub>C else\<^sub>C) \<or>
         c\<^sub>C = (Stop ;; Stmt.If (Neq (Load reg3) (Const 0)) then\<^sub>C else\<^sub>C)))
        then 0
     else if (c\<^sub>A = (x_var \<leftarrow> Load y_var) \<and>
        c\<^sub>C \<in> {Skip ;; Skip ;; Assign reg0 (Load y_mem) ;; Assign x_mem (Load reg0),
              Stop ;; Skip ;; Assign reg0 (Load y_mem) ;; Assign x_mem (Load reg0),
              Skip ;; Assign reg0 (Load y_mem) ;; Assign x_mem (Load reg0),
              Stop ;; Assign reg0 (Load y_mem) ;; Assign x_mem (Load reg0),
              Assign reg0 (Load y_mem) ;; Assign x_mem (Load reg0),
              Stop ;; Assign x_mem (Load reg0)})
        then 0
     else if (c\<^sub>A = (x_var \<leftarrow> Add (Load y_var) (Load z_var)) \<and>
        c\<^sub>C \<in> {(reg1 \<leftarrow> Load y_mem) ;; Assign reg2 (Load z_mem) ;;
                Assign reg0 (Add (Load reg1) (Load reg2)) ;; Assign x_mem (Load reg0),
              Stop ;; Assign reg2 (Load z_mem) ;;
                Assign reg0 (Add (Load reg1) (Load reg2)) ;; Assign x_mem (Load reg0),
              Assign reg2 (Load z_mem) ;;
                Assign reg0 (Add (Load reg1) (Load reg2)) ;; Assign x_mem (Load reg0),
              Stop ;;
                Assign reg0 (Add (Load reg1) (Load reg2)) ;; Assign x_mem (Load reg0),
              Assign reg0 (Add (Load reg1) (Load reg2)) ;; Assign x_mem (Load reg0),
              Stop ;; Assign x_mem (Load reg0)})
        then 0
     else (Suc 0))"

fun abs_steps
where
"abs_steps \<langle>c\<^sub>A, _, _\<rangle>\<^sub>A \<langle>c\<^sub>C, _, _\<rangle>\<^sub>C = abs_steps' c\<^sub>A c\<^sub>C"

lemma closed_others_RefRel_HighBranch:
  "closed_others RefRel_HighBranch"
unfolding closed_others_def
proof(clarify)
fix c\<^sub>A c\<^sub>C mds\<^sub>C mem\<^sub>C mem\<^sub>C'
assume
  in_rel: "(\<langle>c\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A,
    \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" and
  vars: " \<forall>x. mem\<^sub>C x \<noteq> mem\<^sub>C' x \<longrightarrow> \<not> var_asm_not_written mds\<^sub>C x"
and  dmas: "\<forall>x. dma\<^sub>C mem\<^sub>C x \<noteq> dma\<^sub>C mem\<^sub>C' x \<longrightarrow> \<not> var_asm_not_written mds\<^sub>C x"
from in_rel vars dmas
show "(\<langle>c\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
  proof (induct rule: RefRel_HighBranch.induct)
  case (acq_mode_rel c\<^sub>A x SomeMode tail\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using acq_mode_rel
    by (simp add:RefRel_HighBranch.acq_mode_rel)
  next
  case (assign_load_rel c\<^sub>A x y tail\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using assign_load_rel
    by (simp add:RefRel_HighBranch.assign_load_rel)
  next
  case (assign_const_rel c\<^sub>A x z tail\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using assign_const_rel
    by (simp add:RefRel_HighBranch.assign_const_rel)
  next
  case (if_reg_load_rel c\<^sub>A x then\<^sub>A else\<^sub>A c\<^sub>C then\<^sub>C else\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_reg_load_rel
    by (simp add:RefRel_HighBranch.if_reg_load_rel)
  next
  case (if_reg_stop_rel c\<^sub>A x then\<^sub>A else\<^sub>A c\<^sub>C then\<^sub>C else\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_reg_stop_rel
    proof (clarsimp)
      from if_reg_stop_rel.hyps(3,5,6) if_reg_stop_rel.prems(1)
      have reg_untouched: "mem\<^sub>C reg3 = mem\<^sub>C' reg3" and
        x\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of x) = mem\<^sub>C' (var\<^sub>C_of x)" and
        x\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C x = mem\<^sub>A_of mem\<^sub>C' x"
        using reg_is_not_the_var\<^sub>C_of_anything mem\<^sub>A_of_def NoWrite\<^sub>A_implies_NoWrite\<^sub>C
        by (clarsimp simp:var_asm_not_written_def, blast)+
      with if_reg_stop_rel.hyps
      have conds_still_match: "(mem\<^sub>C' reg3 = 0) = (mem\<^sub>A_of mem\<^sub>C' x = 0)"
        by fastforce
      with if_reg_stop_rel RefRel_HighBranch.if_reg_stop_rel
      show "(\<langle>Stmt.If (Neq (Load x) (Const 0)) then\<^sub>A else\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A,
             \<langle>Stop ;; Stmt.If (Neq (Load reg3) (Const 0)) then\<^sub>C else\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C)
             \<in> RefRel_HighBranch"
        by auto
    qed
  next
  case (if_reg_rel c\<^sub>A x then\<^sub>A else\<^sub>A c\<^sub>C then\<^sub>C else\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_reg_rel(1-11)
    proof (clarsimp)
      from if_reg_rel.hyps(3,5,6) if_reg_rel.prems(1)
      have reg_untouched: "mem\<^sub>C reg3 = mem\<^sub>C' reg3" and
        x\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of x) = mem\<^sub>C' (var\<^sub>C_of x)" and
        x\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C x = mem\<^sub>A_of mem\<^sub>C' x"
        using reg_is_not_the_var\<^sub>C_of_anything mem\<^sub>A_of_def NoWrite\<^sub>A_implies_NoWrite\<^sub>C
        by (clarsimp simp:var_asm_not_written_def, blast)+
      with if_reg_rel.hyps
      have conds_still_match: "(mem\<^sub>C' reg3 = 0) = (mem\<^sub>A_of mem\<^sub>C' x = 0)"
        by auto
      show "(\<langle>Stmt.If (Neq (Load x) (Const 0)) then\<^sub>A else\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A,
             \<langle>Stmt.If (Neq (Load reg3) (Const 0)) then\<^sub>C else\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C)
             \<in> RefRel_HighBranch"
        apply (rule RefRel_HighBranch.if_reg_rel, simp+)
            using if_reg_rel apply simp
           using if_reg_rel apply simp
          using conds_still_match apply simp
         using if_reg_rel apply blast+
        done
    qed
  next
  case (if_then_rel_1 c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_then_rel_1
    by (simp add:RefRel_HighBranch.if_then_rel_1)
  next
  case (if_then_rel_1' c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_then_rel_1'
    by (simp add:RefRel_HighBranch.if_then_rel_1')
  next
  case (if_then_rel_2 c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_then_rel_2
    by (simp add:RefRel_HighBranch.if_then_rel_2)
  next
  case (if_then_rel_2' c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_then_rel_2'
    by (simp add:RefRel_HighBranch.if_then_rel_2')
  next
  case (if_then_rel_3 c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_then_rel_3
    by (simp add:RefRel_HighBranch.if_then_rel_3)
  next
  case (if_then_rel_3' c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_then_rel_3'
    proof (clarsimp)
      from if_then_rel_3'.hyps(4,5,6,7) if_then_rel_3'.prems(1)
      have reg_untouched: "mem\<^sub>C reg0 = mem\<^sub>C' reg0" and
        y\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of y_var) = mem\<^sub>C' (var\<^sub>C_of y_var)" and
        y\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C y_var = mem\<^sub>A_of mem\<^sub>C' y_var"
        using reg_is_not_the_var\<^sub>C_of_anything mem\<^sub>A_of_def NoWrite\<^sub>A_implies_NoWrite\<^sub>C var_asm_not_written_def
        by fastforce+
      with if_then_rel_3'.hyps
      have conds_still_match: "mem\<^sub>C' reg0 = mem\<^sub>A_of mem\<^sub>C' y_var"
        by simp
      show "(\<langle>x_var \<leftarrow> Load y_var, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A,
             \<langle>Stop ;; (x_mem \<leftarrow> Load reg0), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C)
             \<in> RefRel_HighBranch"
        using RefRel_HighBranch.if_then_rel_3' if_then_rel_3' conds_still_match by simp
    qed
  next
  case (if_then_rel_4 c\<^sub>A c\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_then_rel_4
    proof (clarsimp)
      from if_then_rel_4.hyps(3-7) if_then_rel_4.prems(1)
      have reg_untouched: "mem\<^sub>C reg0 = mem\<^sub>C' reg0" and
        y\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of y_var) = mem\<^sub>C' (var\<^sub>C_of y_var)" and
        y\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C y_var = mem\<^sub>A_of mem\<^sub>C' y_var"
        using reg_is_not_the_var\<^sub>C_of_anything mem\<^sub>A_of_def NoWrite\<^sub>A_implies_NoWrite\<^sub>C var_asm_not_written_def
        by (fast, metis+)
      with if_then_rel_4.hyps
      have conds_still_match: "mem\<^sub>C' reg0 = mem\<^sub>A_of mem\<^sub>C' y_var"
        by simp
      show "(\<langle>x_var \<leftarrow> Load y_var, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A,
             \<langle>x_mem \<leftarrow> Load reg0, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C)
             \<in> RefRel_HighBranch"
        using RefRel_HighBranch.if_then_rel_4 if_then_rel_4 conds_still_match by simp
    qed
  next
  case (if_else_rel_1 c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_else_rel_1
    by (simp add:RefRel_HighBranch.if_else_rel_1)
  next
  case (if_else_rel_1' c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_else_rel_1'
    proof (clarsimp)
      from if_else_rel_1'.hyps(4-7) if_else_rel_1'.prems(1)
      have reg_untouched: "mem\<^sub>C reg1 = mem\<^sub>C' reg1" and
        y\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of y_var) = mem\<^sub>C' (var\<^sub>C_of y_var)" and
        y\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C y_var = mem\<^sub>A_of mem\<^sub>C' y_var"
        using reg_is_not_the_var\<^sub>C_of_anything mem\<^sub>A_of_def NoWrite\<^sub>A_implies_NoWrite\<^sub>C var_asm_not_written_def
        by (metis (no_types) insertCI reg_not_visible_abs, metis+)
      with if_else_rel_1'.hyps
      have conds_still_match: "mem\<^sub>C' reg1 = mem\<^sub>A_of mem\<^sub>C' y_var"
        by simp
      show "(\<langle>x_var \<leftarrow> Add (Load y_var) (Load z_var), mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A,
             \<langle>Stop ;; (reg2 \<leftarrow> Load z_mem) ;; (reg0 \<leftarrow> Add (Load reg1) (Load reg2)) ;; (x_mem \<leftarrow> Load reg0), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C)
             \<in> RefRel_HighBranch"
        using RefRel_HighBranch.if_else_rel_1' if_else_rel_1' conds_still_match by simp
    qed
  next
  case (if_else_rel_2 c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_else_rel_2
    proof (clarsimp)
      from if_else_rel_2.hyps(4-7) if_else_rel_2.prems(1)
      have reg_untouched: "mem\<^sub>C reg1 = mem\<^sub>C' reg1" and
        y\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of y_var) = mem\<^sub>C' (var\<^sub>C_of y_var)" and
        y\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C y_var = mem\<^sub>A_of mem\<^sub>C' y_var"
        using reg_is_not_the_var\<^sub>C_of_anything mem\<^sub>A_of_def NoWrite\<^sub>A_implies_NoWrite\<^sub>C var_asm_not_written_def
        by (metis (no_types) insertCI reg_not_visible_abs, metis+)
      with if_else_rel_2.hyps
      have conds_still_match: "mem\<^sub>C' reg1 = mem\<^sub>A_of mem\<^sub>C' y_var"
        by simp
      show "(\<langle>x_var \<leftarrow> Add (Load y_var) (Load z_var), mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A,
             \<langle>(reg2 \<leftarrow> Load z_mem) ;; (reg0 \<leftarrow> Add (Load reg1) (Load reg2)) ;; (x_mem \<leftarrow> Load reg0), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C)
             \<in> RefRel_HighBranch"
        using RefRel_HighBranch.if_else_rel_2 if_else_rel_2 conds_still_match by simp
    qed
  next
  case (if_else_rel_2' c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_else_rel_2'
    proof (clarsimp)
      from if_else_rel_2'.hyps(4-8) if_else_rel_2'.prems(1)
      have regs_untouched: "mem\<^sub>C reg1 = mem\<^sub>C' reg1 \<and> mem\<^sub>C reg2 = mem\<^sub>C' reg2" and
        y\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of y_var) = mem\<^sub>C' (var\<^sub>C_of y_var)" and
        y\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C y_var = mem\<^sub>A_of mem\<^sub>C' y_var" and
        z\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of z_var) = mem\<^sub>C' (var\<^sub>C_of z_var)" and
        z\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C z_var = mem\<^sub>A_of mem\<^sub>C' z_var"
        using reg_is_not_the_var\<^sub>C_of_anything mem\<^sub>A_of_def NoWrite\<^sub>A_implies_NoWrite\<^sub>C var_asm_not_written_def
        by (metis (no_types) insertCI reg_not_visible_abs, metis+)
      with if_else_rel_2'.hyps(5,9,10)
      have conds_still_match: "mem\<^sub>C' reg1 = mem\<^sub>A_of mem\<^sub>C' y_var \<and> mem\<^sub>C' reg2 = mem\<^sub>A_of mem\<^sub>C' z_var"
        by simp
      show "(\<langle>x_var \<leftarrow> Add (Load y_var) (Load z_var), mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A,
             \<langle>Stop ;; (reg0 \<leftarrow> Add (Load reg1) (Load reg2)) ;; (x_mem \<leftarrow> Load reg0), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C)
             \<in> RefRel_HighBranch"
        using RefRel_HighBranch.if_else_rel_2' if_else_rel_2' conds_still_match by simp
    qed
  next
  case (if_else_rel_3 c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_else_rel_3
    proof (clarsimp)
      from if_else_rel_3.hyps(4-8) if_else_rel_3.prems(1)
      have regs_untouched: "mem\<^sub>C reg1 = mem\<^sub>C' reg1 \<and> mem\<^sub>C reg2 = mem\<^sub>C' reg2" and
        y\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of y_var) = mem\<^sub>C' (var\<^sub>C_of y_var)" and
        y\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C y_var = mem\<^sub>A_of mem\<^sub>C' y_var" and
        z\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of z_var) = mem\<^sub>C' (var\<^sub>C_of z_var)" and
        z\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C z_var = mem\<^sub>A_of mem\<^sub>C' z_var"
        using reg_is_not_the_var\<^sub>C_of_anything mem\<^sub>A_of_def NoWrite\<^sub>A_implies_NoWrite\<^sub>C var_asm_not_written_def
        by (metis (no_types) insertCI reg_not_visible_abs, metis+)
      with if_else_rel_3.hyps(5,9,10)
      have conds_still_match: "mem\<^sub>C' reg1 = mem\<^sub>A_of mem\<^sub>C' y_var \<and> mem\<^sub>C' reg2 = mem\<^sub>A_of mem\<^sub>C' z_var"
        by simp
      show "(\<langle>x_var \<leftarrow> Add (Load y_var) (Load z_var), mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A,
             \<langle>(reg0 \<leftarrow> Add (Load reg1) (Load reg2)) ;; (x_mem \<leftarrow> Load reg0), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C)
             \<in> RefRel_HighBranch"
        using RefRel_HighBranch.if_else_rel_3 if_else_rel_3 conds_still_match by simp
    qed
  next
  case (if_else_rel_3' c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_else_rel_3'
    proof (clarsimp)
      from if_else_rel_3'.hyps(4-8) if_else_rel_3'.prems(1)
      have reg_untouched: "mem\<^sub>C reg0 = mem\<^sub>C' reg0" and
        y\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of y_var) = mem\<^sub>C' (var\<^sub>C_of y_var)" and
        y\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C y_var = mem\<^sub>A_of mem\<^sub>C' y_var" and
        z\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of z_var) = mem\<^sub>C' (var\<^sub>C_of z_var)" and
        z\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C z_var = mem\<^sub>A_of mem\<^sub>C' z_var"
        using reg_is_not_the_var\<^sub>C_of_anything mem\<^sub>A_of_def NoWrite\<^sub>A_implies_NoWrite\<^sub>C var_asm_not_written_def
        by (metis (no_types) insertCI reg_not_visible_abs, metis+)
      with if_else_rel_3'.hyps(5,9)
      have conds_still_match:
        "ev\<^sub>A mem\<^sub>C' (Load reg0) = ev\<^sub>A (mem\<^sub>A_of mem\<^sub>C') (Add (Load y_var) (Load z_var))"
        by simp
      show "(\<langle>x_var \<leftarrow> Add (Load y_var) (Load z_var), mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A,
             \<langle>Stop ;; (x_mem \<leftarrow> Load reg0), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C)
             \<in> RefRel_HighBranch"
        using RefRel_HighBranch.if_else_rel_3' if_else_rel_3' conds_still_match by simp
    qed
  next
  case (if_else_rel_4 c\<^sub>A c\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using if_else_rel_4
    proof (clarsimp)
      from if_else_rel_4.hyps(3-7) if_else_rel_4.prems(1)
      have reg_untouched: "mem\<^sub>C reg0 = mem\<^sub>C' reg0" and
        y\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of y_var) = mem\<^sub>C' (var\<^sub>C_of y_var)" and
        y\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C y_var = mem\<^sub>A_of mem\<^sub>C' y_var" and
        z\<^sub>C_untouched: "mem\<^sub>C (var\<^sub>C_of z_var) = mem\<^sub>C' (var\<^sub>C_of z_var)" and
        z\<^sub>A_untouched: "mem\<^sub>A_of mem\<^sub>C z_var = mem\<^sub>A_of mem\<^sub>C' z_var"
        using reg_is_not_the_var\<^sub>C_of_anything mem\<^sub>A_of_def NoWrite\<^sub>A_implies_NoWrite\<^sub>C var_asm_not_written_def
        by (metis (no_types) insertCI reg_not_visible_abs, metis+)
      with if_else_rel_4.hyps(4,8)
      have conds_still_match:
        "ev\<^sub>A mem\<^sub>C' (Load reg0) = ev\<^sub>A (mem\<^sub>A_of mem\<^sub>C') (Add (Load y_var) (Load z_var))"
        by simp
      show "(\<langle>x_var \<leftarrow> Add (Load y_var) (Load z_var), mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A,
             \<langle>(x_mem \<leftarrow> Load reg0), mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C)
             \<in> RefRel_HighBranch"
        using RefRel_HighBranch.if_else_rel_4 if_else_rel_4 conds_still_match by simp
    qed
  next
  case (stop_seq_rel c\<^sub>A tail\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  show "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A_of mem\<^sub>C'\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using stop_seq_rel
    by (simp add:RefRel_HighBranch.stop_seq_rel)
  next
  case (stop_rel mds\<^sub>C mem\<^sub>C)
  show ?case by (simp add:RefRel_HighBranch.stop_rel)
  qed
qed

lemma preserves_modes_mem_RefRel_HighBranch:
  "preserves_modes_mem RefRel_HighBranch"
unfolding preserves_modes_mem_def2
proof(clarsimp)
  fix c\<^sub>A mds\<^sub>A mem\<^sub>A c\<^sub>C mds\<^sub>C mem\<^sub>C
  assume in_rel: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
  thus "mem\<^sub>A = mem\<^sub>A_of mem\<^sub>C \<and> mds\<^sub>A = mds\<^sub>A_of mds\<^sub>C"
    by (cases rule:RefRel_HighBranch.cases, blast+)
qed

lemma new_vars_private_RefRel_HighBranch:
  "new_vars_private RefRel_HighBranch"
unfolding new_vars_private_def
(* Slightly more intuitive goals without simp converting \<or> to \<longrightarrow> *)
proof(clarify)
  fix c\<^sub>A mds\<^sub>A mem\<^sub>A c\<^sub>C mds\<^sub>C mem\<^sub>C c\<^sub>C' mds\<^sub>C' mem\<^sub>C'
  assume in_rel: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch" and
      does_eval: "(\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C, \<langle>c\<^sub>C', mds\<^sub>C', mem\<^sub>C'\<rangle>\<^sub>C) \<in> C.eval\<^sub>w"
  let "?diff_mem v\<^sub>C" = "mem\<^sub>C' v\<^sub>C \<noteq> mem\<^sub>C v\<^sub>C"
  let "?diff_dma v\<^sub>C" = "dma\<^sub>C mem\<^sub>C' v\<^sub>C < dma\<^sub>C mem\<^sub>C v\<^sub>C"
  let "?conc_only v\<^sub>C" = "v\<^sub>C \<notin> range var\<^sub>C_of"
  show "(\<forall>v\<^sub>C. (?diff_mem v\<^sub>C \<or> ?diff_dma v\<^sub>C) \<and> ?conc_only v\<^sub>C \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C' AsmNoReadOrWrite) \<and>
         mds\<^sub>C AsmNoReadOrWrite - range var\<^sub>C_of \<subseteq> mds\<^sub>C' AsmNoReadOrWrite - range var\<^sub>C_of"
  using in_rel does_eval
  proof(cases rule:RefRel_HighBranch.cases)
  case (acq_mode_rel x SomeMode tail\<^sub>A tail\<^sub>C)
    moreover with does_eval
    have "mds\<^sub>C' = mds\<^sub>C (SomeMode := insert (var\<^sub>C_of x) (mds\<^sub>C SomeMode))"
      by (metis (mono_tags) C.seq_decl_elim C.update_modes.simps(1))
    ultimately show ?thesis
      by simp
  next
  case (assign_load_rel x y tail\<^sub>A tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis C.assign_elim C.seq_elim Stmt.distinct(13) set_eq_subset)
  next
  case (assign_const_rel x z tail\<^sub>A tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis C.assign_elim C.seq_elim Stmt.distinct(13) set_eq_subset)
  next
  case (if_reg_load_rel x then\<^sub>A else\<^sub>A then\<^sub>C else\<^sub>C)
    with does_eval
    show ?thesis
      by (metis C.assign_elim C.seq_elim Stmt.distinct(13) set_eq_subset)
  next
  case (if_reg_stop_rel x then\<^sub>A else\<^sub>A then\<^sub>C else\<^sub>C)
    with does_eval
    show ?thesis
      by (metis C.seq_stop_elim subset_refl)
  next
  case (if_reg_rel x then\<^sub>A else\<^sub>A then\<^sub>C else\<^sub>C)
    with does_eval
    have mds\<^sub>C_unchanged: "mds\<^sub>C = mds\<^sub>C'"
      by (metis C.if_elim C.seq_elim Stmt.distinct(39))
    moreover with if_reg_rel(5)
    have "\<forall>v\<^sub>C. (?diff_mem v\<^sub>C \<or> ?diff_dma v\<^sub>C) \<and> ?conc_only v\<^sub>C \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C' AsmNoReadOrWrite"
      by simp
    moreover from mds\<^sub>C_unchanged
    have "mds\<^sub>C AsmNoReadOrWrite - range var\<^sub>C_of \<subseteq> mds\<^sub>C' AsmNoReadOrWrite - range var\<^sub>C_of"
      by clarify
    ultimately show ?thesis
      by simp
  next
  case (if_then_rel_1 tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis (no_types, hide_lams) C.eval\<^sub>w.seq C.eval_det C.seq_stop_elim C.seq_stop_eval\<^sub>w C.skip_eval\<^sub>w subsetI)
  next
  case (if_then_rel_1' tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis (no_types, hide_lams) C.seq_stop_elim subsetI)
  next
  case (if_then_rel_2 tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis (no_types, hide_lams) C.eval\<^sub>w.seq C.eval_det C.seq_stop_elim C.seq_stop_eval\<^sub>w C.skip_eval\<^sub>w subsetI)
  next
  case (if_then_rel_2' tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis (no_types, hide_lams) C.seq_stop_elim subsetI)
  next
  case (if_then_rel_3 tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis (full_types) C.seq_assign_elim subset_refl)
  next
  case (if_then_rel_3' tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis (no_types, hide_lams) C.seq_stop_elim subsetI)
  next
  case (if_then_rel_4)
    with does_eval
    show ?thesis
      by (metis C.assign_elim order_refl)
  next
  case (if_else_rel_1 tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis (full_types) C.seq_assign_elim subset_refl)
  next
  case (if_else_rel_1' tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis (no_types, hide_lams) C.seq_stop_elim subsetI)
  next
  case (if_else_rel_2 tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis (full_types) C.seq_assign_elim subset_refl)
  next
  case (if_else_rel_2' tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis (no_types, hide_lams) C.seq_stop_elim subsetI)
  next
  case (if_else_rel_3 tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis (full_types) C.seq_assign_elim subset_refl)
  next
  case (if_else_rel_3' tail\<^sub>C)
    with does_eval
    show ?thesis
      by (metis (no_types, hide_lams) C.seq_stop_elim subsetI)
  next
  case (if_else_rel_4)
    with does_eval
    show ?thesis
      by (metis C.assign_elim order_refl)
  next
  case (stop_seq_rel tail\<^sub>A tail\<^sub>C)
    with does_eval C.seq_stop_elim
    show ?thesis by blast
  next
  case (stop_rel)
    with does_eval C.stop_no_eval have False by simp
    thus ?thesis by simp
  qed
qed

inductive_cases inR_E:
"(\<langle>c, mds, mem\<rangle>\<^sub>A, \<langle>c', mds', mem'\<rangle>\<^sub>A) \<in> A.R"

lemma refrel_helper_Acq_Rel_ex1I:
"
  (\<langle>(Skip@[x +=\<^sub>m SomeMode]) ;; c\<^sub>A_tail, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>2\<^sub>C\<rangle>\<^sub>A,
      \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C)
     \<in> RefRel_HighBranch
  \<Longrightarrow>
  \<exists>!c\<^sub>C_tail.
  c\<^sub>2\<^sub>C = (Skip@[var\<^sub>C_of x +=\<^sub>m SomeMode]) ;; c\<^sub>C_tail \<and>
        ((\<langle>(Skip@[var\<^sub>C_of x +=\<^sub>m SomeMode]) ;; c\<^sub>C_tail, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C,
          \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C(SomeMode := insert (var\<^sub>C_of x) (mds\<^sub>C SomeMode)), mem\<^sub>2\<^sub>C\<rangle>\<^sub>C)
          \<in> C.eval\<^sub>w)"
  apply(cases rule:RefRel_HighBranch.cases)
       apply simp
      apply clarsimp
      apply(erule_tac x="mem\<^sub>A_of mem\<^sub>2\<^sub>C" in allE)
      apply(erule_tac x="mds\<^sub>C(SomeMode := insert (var\<^sub>C_of x) (mds\<^sub>C SomeMode))" in allE)
      apply(erule_tac x="mem\<^sub>2\<^sub>C" in allE)
      apply(erule impE)
       apply(rule_tac x="mem\<^sub>2\<^sub>C" in exI)
       apply(rule conjI)
        apply(rule HOL.refl)
       apply(rule conjI)
        apply(rule A.eval\<^sub>w.seq)
        apply(rule A.decl_eval\<^sub>w', rule HOL.refl, clarsimp)
        apply(rule mode_acquire_refinement_helper)
       apply(rule C.eval\<^sub>w.seq)
       apply(rule C.decl_eval\<^sub>w', rule HOL.refl, clarsimp)
      apply(rule_tac a=c\<^sub>C_tail in ex1I)
       apply clarsimp
        apply(rule C.eval\<^sub>w.seq)
        apply(rule C.decl_eval\<^sub>w', rule HOL.refl, clarsimp)
       apply blast+
  done

lemma refrel_helper_Assign_Load_ex1I:
"
  (\<langle>(x \<leftarrow> Load y) ;; tail\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>2\<^sub>C\<rangle>\<^sub>A,
      \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C)
     \<in> RefRel_HighBranch
  \<Longrightarrow>
  \<exists>!tail\<^sub>C.
  c\<^sub>2\<^sub>C = (var\<^sub>C_of x \<leftarrow> Load (var\<^sub>C_of y)) ;; tail\<^sub>C \<and>
        ((\<langle>(var\<^sub>C_of x \<leftarrow> Load (var\<^sub>C_of y)) ;; tail\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C,
          \<langle>Stop ;; tail\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C((var\<^sub>C_of x) := mem\<^sub>2\<^sub>C (var\<^sub>C_of y))\<rangle>\<^sub>C)
          \<in> C.eval\<^sub>w)"
  apply(cases rule:RefRel_HighBranch.cases, simp+)
          apply clarsimp
          apply(erule_tac x="mem\<^sub>A_of mem\<^sub>2\<^sub>C" in allE)
          apply(erule_tac x="mds\<^sub>C" in allE)
          apply(erule_tac x="mem\<^sub>2\<^sub>C((var\<^sub>C_of x) := mem\<^sub>2\<^sub>C (var\<^sub>C_of y))" in allE)
          apply(erule impE)
           apply(rule_tac x="mem\<^sub>2\<^sub>C" in exI)
           apply(rule conjI)
            apply(rule HOL.refl)
           apply(rule conjI)
            apply(rule A.eval\<^sub>w.seq)
            apply(simp add: assign_eval\<^sub>w_load\<^sub>A mem_assign_refinement_helper_var)
           apply(rule C.eval\<^sub>w.seq)
           apply(simp add: assign_eval\<^sub>w_load\<^sub>C)
          apply(rule_tac a=c\<^sub>C_tail in ex1I)
           apply clarsimp
           apply(rule C.eval\<^sub>w.seq)
           apply(simp add:assign_eval\<^sub>w_load\<^sub>C)
          apply blast
         apply clarsimp
        apply(simp add: assign_eval\<^sub>w_load\<^sub>A mem_assign_refinement_helper_var)
       apply(simp add:assign_eval\<^sub>w_load\<^sub>C)
      apply(rule_tac a=c\<^sub>C_tail in ex1I)
       apply clarsimp
      apply(simp add:assign_eval\<^sub>w_load\<^sub>C)
     apply blast
    apply(rule ex1I, clarsimp+)
  done


lemma refrel_helper_Assign_Const_ex1I:
"
  (\<langle>(x \<leftarrow> Const z) ;; tail\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>2\<^sub>C\<rangle>\<^sub>A,
      \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C)
     \<in> RefRel_HighBranch
  \<Longrightarrow>
  \<exists>!tail\<^sub>C.
  c\<^sub>2\<^sub>C = (var\<^sub>C_of x \<leftarrow> Const z) ;; tail\<^sub>C \<and>
        ((\<langle>(var\<^sub>C_of x \<leftarrow> Const z) ;; tail\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C,
          \<langle>Stop ;; tail\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C((var\<^sub>C_of x) := z)\<rangle>\<^sub>C)
          \<in> C.eval\<^sub>w)"
  apply(cases rule:RefRel_HighBranch.cases, simp+)
          apply clarsimp
          apply(erule_tac x="mem\<^sub>A_of mem\<^sub>2\<^sub>C" in allE)
          apply(erule_tac x="mds\<^sub>C" in allE)
          apply(erule_tac x="mem\<^sub>2\<^sub>C((var\<^sub>C_of x) := z)" in allE)
          apply(erule impE)
           apply(rule_tac x="mem\<^sub>2\<^sub>C" in exI)
           apply(rule conjI)
            apply(rule HOL.refl)
           apply(rule conjI)
            apply(rule A.eval\<^sub>w.seq)
            apply(simp add: assign_eval\<^sub>w_const\<^sub>A mem_assign_refinement_helper_const)
           apply(rule C.eval\<^sub>w.seq)
           apply(simp add:assign_eval\<^sub>w_const\<^sub>C)
          apply(rule_tac a=c\<^sub>C_tail in ex1I)
           apply clarsimp
           apply(rule C.eval\<^sub>w.seq)
           apply(simp add:assign_eval\<^sub>w_const\<^sub>C)
          apply blast
         apply clarsimp
        apply(simp add: assign_eval\<^sub>w_load\<^sub>A mem_assign_refinement_helper_var)
       apply(simp add:assign_eval\<^sub>w_const\<^sub>C)
      apply(rule_tac a=c\<^sub>C_tail in ex1I)
       apply clarsimp
      apply(simp add:assign_eval\<^sub>w_const\<^sub>C)
     apply blast
    apply(rule ex1I, clarsimp+)
  done

(* Could possibly be useful if we have one that just does seq *)
lemma refrel_helper_Assign_Const:
"
  (\<langle>(x \<leftarrow> Const z), mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>2\<^sub>C\<rangle>\<^sub>A,
      \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C)
     \<in> RefRel_HighBranch
  \<Longrightarrow>
  c\<^sub>2\<^sub>C = (var\<^sub>C_of x \<leftarrow> Const z) \<and>
        ((\<langle>(var\<^sub>C_of x \<leftarrow> Const z), mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C,
          \<langle>Stop, mds\<^sub>C, mem\<^sub>2\<^sub>C((var\<^sub>C_of x) := z)\<rangle>\<^sub>C)
          \<in> C.eval\<^sub>w)"
  by (cases rule:RefRel_HighBranch.cases, (simp add:assign_eval\<^sub>w_const\<^sub>C)+)

type_synonym t_Neq_C = "(var\<^sub>C aexp \<Rightarrow> var\<^sub>C aexp \<Rightarrow> var\<^sub>C bexp)"

lemma refrel_helper_If_Reg_Load_exI:
  "
  c\<^sub>C = (reg3 \<leftarrow> Load (var\<^sub>C_of x)) ;; Stmt.If ((Neq::t_Neq_C) (Load reg3) (Const 0)) then\<^sub>C else\<^sub>C
  \<Longrightarrow>
  c\<^sub>2\<^sub>A = If (Neq (Load x) (Const 0)) then\<^sub>A else\<^sub>A
  \<Longrightarrow>
  (\<langle>c\<^sub>2\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>2\<^sub>C\<rangle>\<^sub>A,
    \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C)
   \<in> RefRel_HighBranch
  \<Longrightarrow>
  A.neval
     \<langle>c\<^sub>2\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>2\<^sub>C\<rangle>\<^sub>A 0
     \<langle>c\<^sub>2\<^sub>A, mds\<^sub>A_of mds\<^sub>C', mem\<^sub>2\<^sub>A'\<rangle>\<^sub>A
  \<Longrightarrow>
  (* Distinguish between the different concrete cases
     for which the abstract program remains the same. *)
  (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C, \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C) \<in> rel_inv\<^sub>C
  \<Longrightarrow>
  \<exists>then\<^sub>C' else\<^sub>C'.
  ((c\<^sub>2\<^sub>C = (reg3 \<leftarrow> Load (var\<^sub>C_of x)) ;; Stmt.If ((Neq::t_Neq_C) (Load reg3) (Const 0)) then\<^sub>C' else\<^sub>C')
 \<and>
   (\<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C,
    \<langle>Stop ;; Stmt.If (Neq (Load reg3) (Const 0)) then\<^sub>C' else\<^sub>C', mds\<^sub>C, mem\<^sub>2\<^sub>C(reg3 := mem\<^sub>2\<^sub>C (var\<^sub>C_of x))\<rangle>\<^sub>C)
     \<in> C.eval\<^sub>w
)"
  apply(cases rule:RefRel_HighBranch.cases, simp+)
       apply clarsimp
       apply(erule_tac x="mem\<^sub>2\<^sub>C(reg3 := mem\<^sub>2\<^sub>C (var\<^sub>C_of x))" in allE)
       apply(erule impE)
        apply(rule_tac x="mem\<^sub>2\<^sub>C" in exI)
        apply(rule conjI)
         apply(simp add: conc_only_var_assign_not_visible_abs reg_not_visible_abs)
        apply(rule conjI)
         apply clarsimp
         apply(simp add: mem\<^sub>A_of_def)
        apply(rule C.eval\<^sub>w.seq)
        apply(simp add:assign_eval\<^sub>w_load\<^sub>C)
       apply(rule C.eval\<^sub>w.seq)
       apply(simp add:assign_eval\<^sub>w_load\<^sub>C)
      apply(erule rel_inv\<^sub>CE, simp+)+
  done

lemma refrel_helper_If_Reg_Stop_exI:
  "
  c\<^sub>C = Stop ;; Stmt.If ((Neq::t_Neq_C) (Load reg3) (Const 0)) then\<^sub>C else\<^sub>C
  \<Longrightarrow>
  c\<^sub>2\<^sub>A = If (Neq (Load x) (Const 0)) then\<^sub>A else\<^sub>A
  \<Longrightarrow>
  (\<langle>c\<^sub>2\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>2\<^sub>C\<rangle>\<^sub>A,
    \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C)
   \<in> RefRel_HighBranch
  \<Longrightarrow>
  A.neval
     \<langle>c\<^sub>2\<^sub>A, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>2\<^sub>C\<rangle>\<^sub>A 0
     \<langle>c\<^sub>2\<^sub>A, mds\<^sub>A_of mds\<^sub>C', mem\<^sub>2\<^sub>A'\<rangle>\<^sub>A
  \<Longrightarrow>
  (* Distinguish between concrete cases for which abstract program remains the same. *)
  (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C, \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C) \<in> rel_inv\<^sub>C
  \<Longrightarrow>
  \<exists>then\<^sub>C' else\<^sub>C'.
  ((c\<^sub>2\<^sub>C = Stop ;; Stmt.If ((Neq::t_Neq_C) (Load reg3) (Const 0)) then\<^sub>C' else\<^sub>C')
 \<and>
   (\<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C,
    \<langle>Stmt.If ((Neq::t_Neq_C) (Load reg3) (Const 0)) then\<^sub>C' else\<^sub>C', mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C)
     \<in> C.eval\<^sub>w
)"
  apply(cases rule:RefRel_HighBranch.cases, simp+)
      apply(erule rel_inv\<^sub>CE, simp+)+
     apply(rule C.seq_stop_eval\<^sub>w)+
    apply(erule rel_inv\<^sub>CE, simp+)+
  done

lemma refrel_helper_If_Reg_exI:
assumes
  conds_match: "ev\<^sub>B (mem\<^sub>A_of mem\<^sub>2\<^sub>C) (Neq (Load x) (Const 0)) =
    ev\<^sub>B mem\<^sub>2\<^sub>C ((Neq::t_Neq_C) (Load reg3) (Const 0))" and
  rest_assms: "c\<^sub>C = Stmt.If ((Neq::t_Neq_C) (Load reg3) (Const 0)) then\<^sub>C else\<^sub>C \<and>
  (* Distinguish between concrete cases for which abstract program remains the same. *)
  (\<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C, \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C) \<in> rel_inv\<^sub>C"
shows
"
  \<exists>then\<^sub>C' else\<^sub>C'.
  ((c\<^sub>2\<^sub>C = Stmt.If ((Neq::t_Neq_C) (Load reg3) (Const 0)) then\<^sub>C' else\<^sub>C')
 \<and>
   (\<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C,
    \<langle>if (ev\<^sub>B mem\<^sub>2\<^sub>C ((Neq::t_Neq_C) (Load reg3) (Const 0))) then (then\<^sub>C') else (else\<^sub>C'),
                  mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C)
     \<in> C.eval\<^sub>w
)"
  using rest_assms
  apply(clarsimp simp del:ev\<^sub>B.simps split del:if_splits)
  apply(erule rel_inv\<^sub>CE, (clarsimp simp del:ev\<^sub>B.simps split del:if_splits)+)
  apply(rule C.if_eval\<^sub>w)
  done

inductive_cases Skip_tail_RefRel_HighBranchE: "(\<langle>Skip ;; c\<^sub>A_tail, mds\<^sub>A, mem\<^sub>A\<rangle>, \<langle>Skip ;; c\<^sub>C_tail, mds\<^sub>C, mem\<^sub>C\<rangle>) \<in> RefRel_HighBranch"
thm Skip_tail_RefRel_HighBranchE

inductive_cases Stop_tail_RefRel_HighBranchE: "(\<langle>Stop ;; c\<^sub>A_tail, mds\<^sub>A, mem\<^sub>A\<rangle>, \<langle>Stop ;; c\<^sub>C_tail, mds\<^sub>C, mem\<^sub>C\<rangle>) \<in> RefRel_HighBranch"
thm Stop_tail_RefRel_HighBranchE

inductive_cases Acq_Mode_RefRel_HighBranchE:
"(\<langle>(Skip@[x +=\<^sub>m SomeMode]) ;; c\<^sub>A_tail, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A,
  \<langle>(Skip@[var\<^sub>C_of x +=\<^sub>m SomeMode]) ;; c\<^sub>C_tail, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
thm Acq_Mode_RefRel_HighBranchE

inductive_cases Assign_Load_RefRel_HighBranchE:
"(\<langle>(x \<leftarrow> aexp.Load y) ;; c\<^sub>A_tail, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A,
  \<langle>((var\<^sub>C_of x) \<leftarrow> Load (var\<^sub>C_of y)) ;; c\<^sub>C_tail, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
thm Assign_Load_RefRel_HighBranchE

inductive_cases Assign_Const_RefRel_HighBranchE:
"(\<langle>(x \<leftarrow> Const z) ;; c\<^sub>A_tail, mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A,
  \<langle>((var\<^sub>C_of x) \<leftarrow> Const z) ;; c\<^sub>C_tail, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
thm Assign_Const_RefRel_HighBranchE

inductive_cases If_Reg_Load_RefRel_HighBranchE:
"(\<langle>(If (Neq (Load x) (Const 0)) c\<^sub>A_then c\<^sub>A_else), mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A,
  \<langle>(reg3 \<leftarrow> Load (var\<^sub>C_of y)) ;; ((If (Neq (Load reg3) (Const 0)) c\<^sub>C_then c\<^sub>C_else)), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
thm If_Reg_Load_RefRel_HighBranchE

inductive_cases If_Reg_Stop_RefRel_HighBranchE:
"(\<langle>(If (Neq (Load x) (Const 0)) c\<^sub>A_then c\<^sub>A_else), mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A,
  \<langle>Stop ;; ((If (Neq (Load reg3) (Const 0)) c\<^sub>C_then c\<^sub>C_else)), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
thm If_Reg_Stop_RefRel_HighBranchE

inductive_cases If_Reg_RefRel_HighBranchE:
"(\<langle>(If (Neq (Load x) (Const 0)) c\<^sub>A_then c\<^sub>A_else), mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A,
  \<langle>((If (Neq (Load reg3) (Const 0)) c\<^sub>C_then c\<^sub>C_else)), mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
thm If_Reg_RefRel_HighBranchE

inductive_cases If_Then_RefRel_HighBranchE:
"(\<langle>(x_var \<leftarrow> Load y_var), mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C)
  \<in> RefRel_HighBranch"

inductive_cases If_Else_RefRel_HighBranchE:
"(\<langle>(x_var \<leftarrow> Add (Load y_var) (Load z_var)), mds\<^sub>A_of mds\<^sub>C, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A, \<langle>c\<^sub>C, mds\<^sub>C, mem\<^sub>C\<rangle>\<^sub>C)
  \<in> RefRel_HighBranch"

inductive_cases inv\<^sub>5E:
"A.inv \<langle>Assign x_var (Load y_var), mds, mem\<rangle>"

inductive_cases inv\<^sub>6E:
"A.inv \<langle>Assign x_var (Add (Load y_var) (Load z_var)), mds, mem\<rangle>"

lemma induction_simulation_RefRel_HighBranch:
  notes neq0_conv[simp del] neq0_conv[symmetric, simp add]
  shows
  "(\<langle>c\<^sub>1\<^sub>A, mds\<^sub>A, mem\<^sub>1\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>1\<^sub>C, mds\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch \<Longrightarrow>
   eval_abv\<^sub>C \<langle>c\<^sub>1\<^sub>C, mds\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C \<langle>c\<^sub>1\<^sub>C', mds\<^sub>C', mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C \<Longrightarrow>
   \<exists>c\<^sub>1\<^sub>A' mds\<^sub>A' mem\<^sub>1\<^sub>A'.
      A.neval \<langle>c\<^sub>1\<^sub>A, mds\<^sub>A, mem\<^sub>1\<^sub>A\<rangle>\<^sub>A (abs_steps' c\<^sub>1\<^sub>A c\<^sub>1\<^sub>C)
       \<langle>c\<^sub>1\<^sub>A', mds\<^sub>A', mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A \<and>
      (\<langle>c\<^sub>1\<^sub>A', mds\<^sub>A', mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A, \<langle>c\<^sub>1\<^sub>C', mds\<^sub>C', mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
proof(induct rule: RefRel_HighBranch.induct)
case (acq_mode_rel c\<^sub>A x m tail\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  let ?c\<^sub>1\<^sub>A' = "Stop ;; tail\<^sub>A"
  from acq_mode_rel(1,2)
  have abs_steps_acq: "(abs_steps' c\<^sub>A c\<^sub>C) = 1"
    by (simp add:abs_steps'_def)
  moreover from acq_mode_rel.prems acq_mode_rel(2)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = (Stop ;; tail\<^sub>C)" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C(m := insert (var\<^sub>C_of x) (mds\<^sub>C m))" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C"
    by (metis (mono_tags) C.seq_decl_elim C.update_modes.simps(1))+
  moreover from mds\<^sub>C'_def
  have mds\<^sub>A'_def: "?mds\<^sub>A' = A.update_modes (x +=\<^sub>m m) (mds\<^sub>A_of mds\<^sub>C)"
    unfolding A.update_modes.simps
    by(blast intro: mode_acquire_refinement_helper)
  moreover with mem\<^sub>1\<^sub>C'_def acq_mode_rel A.eval\<^sub>w.seq A.decl_eval\<^sub>w
  have eval\<^sub>A: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A) \<in> A.eval\<^sub>w"
    by presburger
  hence neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 1 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A"
    by clarsimp
  moreover from acq_mode_rel(5)
  have mds\<^sub>C'_concrete_vars_unwritable:
    "(\<forall>v\<^sub>C. v\<^sub>C \<notin> range var\<^sub>C_of \<longrightarrow> v\<^sub>C \<in> mds\<^sub>C' AsmNoReadOrWrite)"
    by(auto simp: mds\<^sub>C'_def)
  moreover with acq_mode_rel(6)[simplified] acq_mode_rel.hyps(4) neval\<^sub>A c\<^sub>1\<^sub>C'_def
  have in_Rel': "(\<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A, \<langle>c\<^sub>1\<^sub>C', mds\<^sub>C', mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    by (metis A.neval_Suc_simp One_nat_def acq_mode_rel.prems)
  ultimately show ?case by metis
next
case (assign_load_rel c\<^sub>A x y tail\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "Stop ;; tail\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from assign_load_rel(1) assign_load_rel(2)
  have abs_steps_load: "(abs_steps' c\<^sub>A c\<^sub>C) = 1"
    by (simp add:abs_steps'_def)
  moreover from assign_load_rel.prems assign_load_rel(2)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = (Stop ;; tail\<^sub>C)" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C((var\<^sub>C_of x) := mem\<^sub>C (var\<^sub>C_of y))"
    using C.seq_elim C.assign_elim assign_eval\<^sub>w_load\<^sub>C
    by (metis (no_types, lifting) Stmt.distinct(13))+
  moreover from assign_load_rel(1,3,4) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have eval\<^sub>A: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A,\<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A) \<in> A.eval\<^sub>w" and
    neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 1 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A"
    using A.eval\<^sub>w.seq assign_eval\<^sub>w_load\<^sub>A mem_assign_refinement_helper_var abs_steps_load by simp+
  moreover with assign_load_rel(11)[simplified] assign_load_rel.prems c\<^sub>1\<^sub>C'_def
  have in_Rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using assign_load_rel.hyps(4-5) mds\<^sub>C'_def by blast
  ultimately show ?case by metis
next
case (assign_const_rel c\<^sub>A x z tail\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "Stop ;; tail\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from assign_const_rel(1,2)
  have abs_steps_const: "(abs_steps' c\<^sub>A c\<^sub>C) = 1"
    by (simp add:abs_steps'_def)
  moreover from assign_const_rel.prems assign_const_rel(2)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = Stop ;; tail\<^sub>C" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C((var\<^sub>C_of x) := z)"
    using C.seq_elim C.assign_elim assign_eval\<^sub>w_const\<^sub>C
    by (metis (no_types, lifting) Stmt.distinct(13))+
  moreover from assign_const_rel(1,3,4) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have eval\<^sub>A: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A) \<in> A.eval\<^sub>w" and
    neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 1 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A"
    using A.eval\<^sub>w.seq assign_eval\<^sub>w_const\<^sub>A mem_assign_refinement_helper_const abs_steps_const by simp+
  moreover with assign_const_rel c\<^sub>1\<^sub>C'_def stop_rel mds\<^sub>C'_def
  have in_Rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch" by blast
  ultimately show ?case by metis
next
case (if_reg_load_rel c\<^sub>A x then\<^sub>A else\<^sub>A c\<^sub>C then\<^sub>C else\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_reg_load_rel.hyps(1,2)
  have abs_steps_if_reg_load: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_reg_load_rel.prems if_reg_load_rel(2)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = (Stop ;; If (Neq (Load reg3) (Const 0)) then\<^sub>C else\<^sub>C)" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C(reg3 := mem\<^sub>C (var\<^sub>C_of x))"
    using C.seq_elim C.assign_elim C.skip_elim assign_eval\<^sub>w_load\<^sub>C
    by (metis (no_types, lifting) Stmt.distinct(13))+
  moreover from if_reg_load_rel(1-4) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using reg_not_visible_abs conc_only_var_assign_not_visible_abs A.neval.intros(1) by simp+
  moreover with if_reg_load_rel c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    apply clarsimp
    apply(erule_tac x=mem\<^sub>1\<^sub>C' in allE)
    apply(erule_tac impE)
     apply(rule_tac x=mem\<^sub>C in exI)
     apply(rule conjI)
      apply(clarsimp)
     apply(rule conjI)
      apply clarsimp
      apply (simp add: mem\<^sub>A_of_def)
     using if_reg_load_rel.prems mem\<^sub>A_of_def apply blast
    apply clarsimp
    done
  ultimately show ?case by metis
next
case (if_reg_stop_rel c\<^sub>A x then\<^sub>A else\<^sub>A c\<^sub>C then\<^sub>C else\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_reg_stop_rel.hyps(1,2)
  have abs_steps_if_reg_stop: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_reg_stop_rel.prems if_reg_stop_rel(2)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = (If (Neq (Load reg3) (Const 0)) then\<^sub>C else\<^sub>C)" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C"
    using C.seq_stop_elim by simp+
  moreover from if_reg_stop_rel(1-4) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using reg_not_visible_abs conc_only_var_assign_not_visible_abs A.neval.intros(1) by simp+
  moreover with if_reg_stop_rel c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have in_Rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch" by blast
  ultimately show ?case by metis
next
case (if_reg_rel c\<^sub>A x then\<^sub>A else\<^sub>A c\<^sub>C then\<^sub>C else\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "if (ev\<^sub>B mem\<^sub>A (Neq (Load x) (Const 0))) then (then\<^sub>A) else (else\<^sub>A)"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_reg_rel(1) if_reg_rel(2)
  have abs_steps_if_reg: "(abs_steps' c\<^sub>A c\<^sub>C) = 1"
    by (simp add:abs_steps'_def)
  moreover from if_reg_rel.prems if_reg_rel(2)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = (if (ev\<^sub>B mem\<^sub>C (Neq (Load reg3) (Const 0))) then (then\<^sub>C) else (else\<^sub>C))" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C"
    apply simp_all
    by (erule C.if_elim, clarsimp+)+
  moreover from if_reg_rel(1,3,4) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have eval\<^sub>A: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A, \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A) \<in> A.eval\<^sub>w"
    using if_seq_eval\<^sub>w_helper\<^sub>A A.if_eval\<^sub>w by presburger
  hence neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 1 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A"
    using abs_steps_if_reg by simp
  moreover from if_reg_rel(3,4,7,12) if_reg_rel.prems c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def eval\<^sub>A
  have in_Rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A, \<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    by clarsimp
  ultimately show ?case by metis
next
case (if_then_rel_1 c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_then_rel_1.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_then_rel_1.prems if_then_rel_1(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = (Stop ;; tail\<^sub>C)" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C"
    using C.seq_elim C.skip_elim by blast+
  moreover from if_then_rel_1(1-5) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using A.neval.intros(1) by auto
  moreover with if_then_rel_1 c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    by blast
  ultimately show ?case by metis
next
case (if_else_rel_1 c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_else_rel_1.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_else_rel_1.prems if_else_rel_1(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = (Stop ;; tail\<^sub>C)" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C(reg1 := mem\<^sub>C y_mem)"
    using C.seq_elim C.assign_elim assign_eval\<^sub>w_load\<^sub>C
    by (metis (no_types, lifting) Stmt.distinct(13))+
  moreover from if_else_rel_1(1-5) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using A.neval.neval_0 conc_only_var_assign_not_visible_abs reg_not_visible_abs by simp+
  moreover with if_else_rel_1 c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    apply clarsimp
    apply(erule_tac x=mem\<^sub>A in allE)
    apply(erule_tac x=mds\<^sub>C in allE)
    apply(erule_tac x=mem\<^sub>1\<^sub>C' in allE)
    apply(erule_tac impE)
     apply(rule_tac x=mem\<^sub>C in exI)
     apply(rule conjI)
      apply(clarsimp)
     apply(rule conjI)
      apply clarsimp
      apply (simp add: mem\<^sub>A_of_def)
     using if_else_rel_1.prems mem\<^sub>A_of_def apply blast
    apply clarsimp
    done
  ultimately show ?case by metis
next
case (if_then_rel_1' c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_then_rel_1'.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_then_rel_1'.prems if_then_rel_1'(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = tail\<^sub>C" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C"
    using C.seq_stop_elim by blast+
  moreover from if_then_rel_1'(1-5) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using A.neval.intros(1) by auto
  moreover with if_then_rel_1' c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    by blast
  ultimately show ?case by metis
next
case (if_else_rel_1' c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_else_rel_1'.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_else_rel_1'.prems if_else_rel_1'(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = tail\<^sub>C" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C"
    using C.seq_stop_elim by blast+
  moreover from if_else_rel_1'(1-5) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using A.neval.neval_0 by simp+
  moreover with if_else_rel_1' c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    by blast
  ultimately show ?case by metis
next
case (if_then_rel_2 c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_then_rel_2.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_then_rel_2.prems if_then_rel_2(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = (Stop ;; tail\<^sub>C)" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C"
    using C.seq_elim C.skip_elim by blast+
  moreover from if_then_rel_2(1-5) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using A.neval.intros(1) by auto
  moreover with if_then_rel_2 c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    by blast
  ultimately show ?case by metis
next
case (if_else_rel_2 c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_else_rel_2.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_else_rel_2.prems if_else_rel_2(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = (Stop ;; tail\<^sub>C)" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C(reg2 := mem\<^sub>C z_mem)"
    using C.seq_elim C.assign_elim assign_eval\<^sub>w_load\<^sub>C
    by (metis (no_types, lifting) Stmt.distinct(13))+
  moreover from if_else_rel_2(1-5) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using A.neval.neval_0 conc_only_var_assign_not_visible_abs reg_not_visible_abs by simp+
  moreover with if_else_rel_2 c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    apply clarsimp
    apply(erule_tac x=mem\<^sub>A in allE)
    apply(erule_tac x=mds\<^sub>C in allE)
    apply(erule_tac x=mem\<^sub>1\<^sub>C' in allE)
    apply(erule_tac impE)
     apply(rule_tac x=mem\<^sub>C in exI)
     apply(rule conjI)
      apply(clarsimp)
     apply(rule conjI)
      apply clarsimp
      apply (simp add: mem\<^sub>A_of_def)
     using if_else_rel_2.prems mem\<^sub>A_of_def apply blast
    apply clarsimp
    done
  ultimately show ?case by metis
next
case (if_then_rel_2' c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_then_rel_2'.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_then_rel_2'.prems if_then_rel_2'(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = tail\<^sub>C" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C"
    using C.seq_stop_elim by blast+
  moreover from if_then_rel_2'(1-5) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using A.neval.intros(1) by auto
  moreover with if_then_rel_2' c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    by blast
  ultimately show ?case by metis
next
case (if_else_rel_2' c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_else_rel_2'.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_else_rel_2'.prems if_else_rel_2'(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = tail\<^sub>C" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C"
    using C.seq_stop_elim by blast+
  moreover from if_else_rel_2'(1-5) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using A.neval.neval_0 by simp+
  moreover with if_else_rel_2' c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    by blast
  ultimately show ?case by metis
next
case (if_then_rel_3 c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_then_rel_3.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_then_rel_3.prems if_then_rel_3(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = (Stop ;; tail\<^sub>C)" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C(reg0 := mem\<^sub>C y_mem)"
    using C.seq_elim C.assign_elim assign_eval\<^sub>w_load\<^sub>C
    by (metis (no_types, lifting) Stmt.distinct(13))+
  moreover from if_then_rel_3(1-5) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using A.neval.neval_0 conc_only_var_assign_not_visible_abs reg_not_visible_abs by simp+
  moreover with if_then_rel_3 c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    apply clarsimp
    apply(erule_tac x=mem\<^sub>A in allE)
    apply(erule_tac x=mds\<^sub>C in allE)
    apply(erule_tac x=mem\<^sub>1\<^sub>C' in allE)
    apply(erule_tac impE)
     apply(rule_tac x=mem\<^sub>C in exI)
     apply(rule conjI)
      apply(clarsimp)
     apply(rule conjI)
      apply clarsimp
      apply (simp add: mem\<^sub>A_of_def)
     using if_then_rel_3.prems mem\<^sub>A_of_def apply blast
    apply clarsimp
    done
  ultimately show ?case by metis
next
case (if_else_rel_3 c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_else_rel_3.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_else_rel_3.prems if_else_rel_3(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = (Stop ;; tail\<^sub>C)" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C(reg0 := ev\<^sub>A mem\<^sub>C (Add (Load reg1) (Load reg2)))"
    using C.seq_elim C.assign_elim assign_eval\<^sub>w_load\<^sub>C
    by (metis (no_types, lifting) Stmt.distinct(13))+
  moreover from if_else_rel_3(1-5) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using A.neval.neval_0 conc_only_var_assign_not_visible_abs reg_not_visible_abs by simp+
  moreover with if_else_rel_3 c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    apply clarsimp
    apply(erule_tac x=mem\<^sub>A in allE)
    apply(erule_tac x=mds\<^sub>C in allE)
    apply(erule_tac x=mem\<^sub>1\<^sub>C' in allE)
    apply(erule_tac impE)
     apply(rule_tac x=mem\<^sub>C in exI)
     apply(rule conjI)
      apply(clarsimp)
     apply(rule conjI)
      apply clarsimp
      apply (simp add: mem\<^sub>A_of_def)
     using if_else_rel_3.prems mem\<^sub>A_of_def apply blast
    apply clarsimp
    done
  ultimately show ?case by metis
next
case (if_then_rel_3' c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_then_rel_3'.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_then_rel_3'.prems if_then_rel_3'(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = tail\<^sub>C" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C"
    using C.seq_stop_elim by blast+
  moreover from if_then_rel_3'(1-5) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using A.neval.intros(1) by auto
  moreover with if_then_rel_3' c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    by blast
  ultimately show ?case by metis
next
case (if_else_rel_3' c\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "c\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_else_rel_3'.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 0"
    by (simp add:abs_steps'_def)
  moreover from if_else_rel_3'.prems if_else_rel_3'(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = tail\<^sub>C" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C"
    using C.seq_stop_elim by blast+
  moreover from if_else_rel_3'(1-5) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 0 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    mem\<^sub>A_unchanged: "?mem\<^sub>1\<^sub>A' = mem\<^sub>A"
    using A.neval.neval_0 by simp+
  moreover with if_else_rel_3' c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    by blast
  ultimately show ?case by metis
next
case (if_then_rel_4 c\<^sub>A c\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "Stop"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_then_rel_4.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 1"
    by (simp add:abs_steps'_def)
  moreover from if_then_rel_4.prems if_then_rel_4(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = Stop" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C(x_mem := mem\<^sub>C reg0)"
    using C.assign_elim assign_eval\<^sub>w_load\<^sub>C
    by metis+
  moreover from if_then_rel_4(1-7) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have eval\<^sub>A: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A,\<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A) \<in> A.eval\<^sub>w" and
    neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 1 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A"
    using A.assign_eval\<^sub>w' ev\<^sub>A.simps(2) mem_assign_refinement_helper_const var\<^sub>C_of.simps(2) abs_steps_this
    by (metis, simp, metis)
  moreover with if_then_rel_4 c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    by blast
  ultimately show ?case by metis
next
case (if_else_rel_4 c\<^sub>A c\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "Stop"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from if_else_rel_4.hyps(1-3)
  have abs_steps_this: "(abs_steps' c\<^sub>A c\<^sub>C) = 1"
    by (simp add:abs_steps'_def)
  moreover from if_else_rel_4.prems if_else_rel_4(2,3)
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = Stop" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C(x_mem := mem\<^sub>C reg0)"
    using C.assign_elim assign_eval\<^sub>w_load\<^sub>C
    by metis+
  moreover from if_else_rel_4(1-8) mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have eval\<^sub>A: "(\<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A,\<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A) \<in> A.eval\<^sub>w" and
    neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 1 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A"
    using A.assign_eval\<^sub>w' ev\<^sub>A.simps(2) mem_assign_refinement_helper_const var\<^sub>C_of.simps(2) abs_steps_this
    by (metis, simp, metis)
  moreover with if_else_rel_4 c\<^sub>1\<^sub>C'_def mds\<^sub>C'_def mem\<^sub>1\<^sub>C'_def
  have rel': "(\<langle>?c\<^sub>1\<^sub>A',?mds\<^sub>A',?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A,\<langle>c\<^sub>1\<^sub>C',mds\<^sub>C',mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    by blast
  ultimately show ?case by metis
next
case (stop_seq_rel c\<^sub>A tail\<^sub>A c\<^sub>C tail\<^sub>C mds\<^sub>A mds\<^sub>C mem\<^sub>A mem\<^sub>C)
  let ?c\<^sub>1\<^sub>A' = "tail\<^sub>A"
  let ?mds\<^sub>A' = "mds\<^sub>A_of mds\<^sub>C'"
  let ?mem\<^sub>1\<^sub>A' = "mem\<^sub>A_of mem\<^sub>1\<^sub>C'"
  from stop_seq_rel(1,2)
  have abs_steps_stop: "(abs_steps' c\<^sub>A c\<^sub>C) = 1"
    by (simp add:abs_steps'_def)
  moreover from stop_seq_rel
  have c\<^sub>1\<^sub>C'_def: "c\<^sub>1\<^sub>C' = tail\<^sub>C" and
    mds\<^sub>C'_def: "mds\<^sub>C' = mds\<^sub>C" and
    mem\<^sub>1\<^sub>C'_def: "mem\<^sub>1\<^sub>C' = mem\<^sub>C"
    using C.seq_stop_elim by auto
  hence neval\<^sub>A: "A.neval \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>A 1 \<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A" and
    rel': "(\<langle>?c\<^sub>1\<^sub>A', ?mds\<^sub>A', ?mem\<^sub>1\<^sub>A'\<rangle>\<^sub>A, \<langle>c\<^sub>1\<^sub>C', mds\<^sub>C', mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
    using stop_seq_rel A.seq_stop_eval\<^sub>w by auto
  ultimately show ?case by metis
next
case stop_rel
  with C.stop_no_eval have False by simp
  thus ?case by simp
qed

(* New obligation 1.2: Consistent stutter introduction/elimination rate. *)
inductive_cases bisimE: "(\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> A.R"
inductive_cases invE: "A.inv \<langle>c, mds, mem\<rangle>"
inductive_cases rel_invE: "(\<langle>c, mds, mem\<rangle>, \<langle>c', mds', mem'\<rangle>) \<in> A.rel_inv"

lemma consistent_abs_steps:
  "(\<langle>c\<^sub>1\<^sub>A, mds\<^sub>A, mem\<^sub>1\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>2\<^sub>A, mds\<^sub>A, mem\<^sub>2\<^sub>A\<rangle>\<^sub>A) \<in> A.R \<Longrightarrow>
   (\<langle>c\<^sub>1\<^sub>A, mds\<^sub>A, mem\<^sub>1\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>1\<^sub>C, mds\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch \<Longrightarrow>
   (\<langle>c\<^sub>2\<^sub>A, mds\<^sub>A, mem\<^sub>2\<^sub>A\<rangle>\<^sub>A, \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch \<Longrightarrow>
   (\<langle>c\<^sub>1\<^sub>C, mds\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C, \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C) \<in> rel_inv\<^sub>C \<Longrightarrow>
   abs_steps' c\<^sub>1\<^sub>A c\<^sub>1\<^sub>C = abs_steps' c\<^sub>2\<^sub>A c\<^sub>2\<^sub>C"
  (* We'll do it by the cases of abs_steps, splitting manually to avoid
    clogging up the automation. This is mostly just a brute-force job,
    taking advantage of all the information in the relations available to us
    -robs. *)
  apply(clarsimp simp:abs_steps'_def split del:if_splits)
  apply(split if_splits)
  apply(rule conjI)
   (* Case: Abstract Program 2 is at If (x) then else. *)
   apply(clarsimp split del:if_splits)
   apply(fastforce elim:bisimE rel_invE rel_inv\<^sub>CE split del:if_splits)
  apply(clarsimp split del:if_splits)
  apply(split if_splits)
  apply(rule conjI)
   (* Case: Abstract Program 2 is at the assignment x := y *)
   apply(clarsimp split del:if_splits)
   apply(fastforce elim:bisimE rel_invE rel_inv\<^sub>CE RefRelE split del:if_splits)
  apply(clarsimp split del:if_splits)
  apply(split if_splits)
  apply(rule conjI)
   (* Case: Abstract Program 2 is at x := y + z *)
   apply(clarsimp split del:if_splits)
   apply(fastforce elim:bisimE rel_invE rel_inv\<^sub>CE RefRelE split del:if_splits)
  apply(clarsimp split del:if_splits)
  (* Default: Abstract Program 2 is at none of the above cases,
              so its abs_steps defaults to 1. *)
  apply(split if_splits)
  apply(rule conjI)
   (* Case: Abstract Program 1 is at the If (x) then else,
            so its abs_steps is 0.
     But 1 != 0, so we need to derive a contradiction.*)
   apply(clarsimp split del:if_splits)
   apply(erule_tac x=x in allE)
   apply(rotate_tac -1)
   apply(erule disjE)
    (* Sub-case: Abstract Program 2 is not at the If (x) then else.
      This is contradictory to Abstract Program 1 being there.
      We should be able to obtain this from the bisimulation relation. *)
    apply(fastforce elim:bisimE rel_invE)
   (* Sub-case: Concrete Program 2 is not at the (reg3 := x ;; If reg3 then else)
                nor the (Stop ;; If reg3 then else).
     This is contradictory to Concrete Program 1 having to be in one of them.
     We obtain this from the concrete coupling invariant. *)
   apply(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)
  apply(split if_splits)
  apply(rule conjI)
   (* Case: Abstract Program 1 is at the x := y,
            so its abs_steps is 1. *)
   apply(clarsimp split del:if_splits)
   apply(rule conjI)
    apply(erule bisimE)
    apply clarsimp
    (* Find the cheat-code and push the rest through the spaghetti-maker *)
    apply(erule rel_invE, simp+)
        apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
      apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
    apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
   apply(rule conjI)
    apply(erule bisimE)
    apply clarsimp
    apply(erule rel_invE, simp+)
        apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
      apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
    apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
   apply(rule conjI)
    apply(erule bisimE)
    apply clarsimp
    apply(erule rel_invE, simp+)
        apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
      apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
    apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
   apply(rule conjI)
    apply(erule bisimE)
    apply clarsimp
    apply(erule rel_invE, simp+)
        apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
      apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
    apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
   apply(rule conjI)
    apply(erule bisimE)
    apply clarsimp
    apply(erule rel_invE, simp+)
        apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
      apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
    apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
   apply(erule bisimE)
   apply clarsimp
   apply(erule rel_invE, simp+)
       apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
  apply(split if_splits)
  apply(rule conjI)
   (* Case: Abstract Program 1 is at the x := y + z,
            so its abs_steps is 1. *)
   apply(clarsimp split del:if_splits)
   apply(rule conjI)
    apply(erule bisimE)
    apply clarsimp
    apply(erule rel_invE, simp+)
       apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
      apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
   apply(rule conjI)
    apply(erule bisimE)
    apply clarsimp
    apply(erule rel_invE, simp+)
       apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
      apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
   apply(rule conjI)
    apply(erule bisimE)
    apply clarsimp
    apply(erule rel_invE, simp+)
       apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
      apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
   apply(rule conjI)
    apply(erule bisimE)
    apply clarsimp
    apply(erule rel_invE, simp+)
       apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
      apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
   apply(rule conjI)
    apply(erule bisimE)
    apply clarsimp
    apply(erule rel_invE, simp+)
       apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
      apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
   apply(erule bisimE)
   apply clarsimp
   apply(erule rel_invE, simp+)
      apply((erule RefRelE, simp+)+,(solves\<open>erule rel_inv\<^sub>CE, force+\<close>)+)
  done

(* New obligation 1.3: Closedness of the coupling invariant and preservation of mds-equality
                       under lockstep execution. *)
lemma same_cmd_preserves_consistent_mds:
  "eval_abv\<^sub>C \<langle>c, mds, mem\<^sub>1\<rangle>\<^sub>C \<langle>c\<^sub>1, mds\<^sub>1, mem\<^sub>1'\<rangle>\<^sub>C \<Longrightarrow>
   eval_abv\<^sub>C \<langle>c, mds, mem\<^sub>2\<rangle>\<^sub>C \<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2'\<rangle>\<^sub>C \<Longrightarrow>
   C.no_await c \<Longrightarrow>
   mds\<^sub>1 = mds\<^sub>2"
  apply(induct c arbitrary:mds c\<^sub>1 c\<^sub>2 mds\<^sub>1 mds\<^sub>2 mem\<^sub>1 mem\<^sub>2 mem\<^sub>1' mem\<^sub>2')
         using C.assign_elim apply blast
        using C.skip_elim apply blast
       using C.upd_elim C.no_await.simps apply blast
      apply(case_tac "c1 = Stop")
       using C.seq_stop_elim apply metis
      apply(subgoal_tac "C.no_await c1")
       prefer 2
       using C.no_await.simps apply blast
      using C.seq_elim apply meson
     apply auto[1]
    apply(metis C.while_elim)
   using C.no_await.simps apply blast
  apply(force simp:C.stop_no_eval)
  done

lemma rel_inv\<^sub>C_closed_lockstep_keeps_mds_consistent:
  "(\<langle>c\<^sub>1\<^sub>C, mds\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C, \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C) \<in> rel_inv\<^sub>C \<Longrightarrow>
   \<forall>mds\<^sub>1\<^sub>C' mds\<^sub>2\<^sub>C' mem\<^sub>1\<^sub>C' mem\<^sub>2\<^sub>C' c\<^sub>1\<^sub>C' c\<^sub>2\<^sub>C'.
      eval_abv\<^sub>C \<langle>c\<^sub>1\<^sub>C, mds\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C \<langle>c\<^sub>1\<^sub>C', mds\<^sub>1\<^sub>C', mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C \<and>
      eval_abv\<^sub>C \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C \<langle>c\<^sub>2\<^sub>C', mds\<^sub>2\<^sub>C', mem\<^sub>2\<^sub>C'\<rangle>\<^sub>C \<longrightarrow>
      (\<langle>c\<^sub>1\<^sub>C', mds\<^sub>1\<^sub>C', mem\<^sub>1\<^sub>C'\<rangle>\<^sub>C, \<langle>c\<^sub>2\<^sub>C', mds\<^sub>2\<^sub>C', mem\<^sub>2\<^sub>C'\<rangle>\<^sub>C)
      \<in> rel_inv\<^sub>C \<and>
      mds\<^sub>1\<^sub>C' = mds\<^sub>2\<^sub>C'"
  (* Eliminating using rel_inv\<^sub>CE may have also been sufficient, because
    rel_inv\<^sub>C doesn't really have any inductive cases anyway... *)
  apply(induct rule:rel_inv\<^sub>C.induct)
            apply clarsimp
            apply(metis (no_types, lifting) C.seq_assign_elim C.seq_elim C.skip_elim Stmt.distinct(25) rel_inv\<^sub>C.rel_inv\<^sub>C_2)
           apply clarsimp
           apply(metis (no_types, lifting) C.seq_assign_elim C.seq_elim C.skip_elim Stmt.distinct(25) rel_inv\<^sub>C.rel_inv\<^sub>C_2')
          apply clarsimp
          apply(metis C.seq_stop_elim rel_inv\<^sub>C.rel_inv\<^sub>C_3)
         apply clarsimp
         apply(metis C.seq_stop_elim rel_inv\<^sub>C.rel_inv\<^sub>C_3')
        apply clarsimp
        apply (metis (no_types, lifting) C.seq_assign_elim C.seq_elim C.skip_elim Stmt.distinct(25) rel_inv\<^sub>C.rel_inv\<^sub>C_4)
       apply clarsimp
       apply(metis (no_types, lifting) C.seq_assign_elim C.seq_elim C.skip_elim Stmt.distinct(25) rel_inv\<^sub>C.rel_inv\<^sub>C_4')
      apply clarsimp
      apply(metis C.seq_stop_elim rel_inv\<^sub>C.rel_inv\<^sub>C_5)
     apply clarsimp
     apply(metis C.seq_stop_elim rel_inv\<^sub>C.rel_inv\<^sub>C_5')
    apply clarsimp
    apply(rule conjI)
     apply(rule rel_inv\<^sub>C.rel_inv\<^sub>C_default)
       apply(metis C.seq_assign_elim)
      apply(metis C.no_await.intros(1,2) C.no_await_trans)
     apply(clarsimp simp:C.prog_high_branch\<^sub>C_def)
     apply(metis C.seq_assign_elim)
    apply(metis C.seq_assign_elim)
   apply clarsimp
   apply(rule conjI)
    apply(rule rel_inv\<^sub>C.rel_inv\<^sub>C_default)
      apply(metis C.seq_assign_elim)
     apply(metis C.no_await.intros(1,2) C.no_await_trans)
    apply(clarsimp simp:C.prog_high_branch\<^sub>C_def)
    apply(metis C.seq_assign_elim)
   apply(metis C.seq_assign_elim)
  apply clarsimp
  apply(rule conjI)
   apply(clarsimp simp:C.prog_high_branch\<^sub>C_def)
   apply(case_tac "\<exists> e a b. C.leftmost_cmd c' = If e a b")
    apply(fastforce intro:rel_inv\<^sub>C_default rel_inv\<^sub>C_1 rel_inv\<^sub>C_1' simp:C.no_await.intros C.prog_high_branch\<^sub>C_def)
   apply(rule rel_inv\<^sub>C.rel_inv\<^sub>C_default)
     apply(force simp:C.stop_no_eval dest:C.seq_stop_elim C.seq_assign_elim C.seq_elim C.skip_elim C.seq_decl_elim C.assign_elim)
    apply(metis C.no_await_trans)
   apply(fastforce simp:C.prog_high_branch\<^sub>C_def C.stop_no_eval dest:C.seq_skip_elim C.seq_stop_elim C.seq_assign_elim C.seq_elim C.skip_elim C.seq_decl_elim C.assign_elim)
  using same_cmd_preserves_consistent_mds by blast

(* New obligation 1.1: Consistent stopping behaviour for coupling invariant-related
                       concrete configurations *)
fun stops_together :: "('var,'aexp,'bexp) Stmt \<Rightarrow> (_,_,_) Stmt \<Rightarrow> bool" where
  "stops_together Stop Stop = True" |
  "stops_together Skip Skip = True" |
  "stops_together (Assign _ _) (Assign _ _) = True" |
  "stops_together (Assign _ _) Skip = True" |
  "stops_together Skip (Assign _ _) = True" |
  "stops_together (Seq s1 s2) (Seq s1' s2') = ((stops_together s1 s1') \<and> (stops_together s2 s2'))" |
  "stops_together (If c a b) (If c' a' b') = ((stops_together a a') \<and> (stops_together b b'))" |
  "stops_together a b = (a = b)"

lemma stops_together_Stop_right [simp]:
  "stops_together s Stop = (s = Stop)"
  apply(induction s, auto)
  done

lemma stops_together_Stop_left [simp]:
  "stops_together Stop s = (s = Stop)"
  apply(induction s, auto)
  done

abbreviation
  stops_simple where
  "stops_simple c \<equiv> (\<forall>c'. (c,c') \<notin> C.eval\<^sub>w_simple)"

lemma decide_stops_simple:
  "stops_together c c' \<Longrightarrow> (stops_simple (c,mem)) = (stops_simple (c',mem'))"
  apply(induction arbitrary: mem mem' rule: stops_together.induct, auto  intro: C.eval\<^sub>w_simple.intros C.eval\<^sub>w_simple.seq_stop intro!: C.cond elim: C.eval\<^sub>w_simple.cases)
  using C.eval\<^sub>w_simple.seq_stop by fastforce+

inductive_cases no_await_seqE [elim!]: "C.no_await (c1 ;; c2)"
inductive_cases no_await_ifE [elim!]: "C.no_await (If b c d)"
inductive_cases no_await_ModeUpdE [elim!]: "C.no_await (c@[m])"


lemma decide_stops:
  "stops_together c c' \<Longrightarrow> C.no_await c \<Longrightarrow> C.no_await c' \<Longrightarrow> (stops\<^sub>C \<langle>c,mds,mem\<rangle>\<^sub>C) = (stops\<^sub>C \<langle>c',mds,mem'\<rangle>\<^sub>C)"
  apply(induction arbitrary: mds mem mem' rule: stops_together.induct, auto simp: stops\<^sub>C_def  simp: C.stop_no_eval)

                      apply(auto elim!: C.skip_elim  intro!: C.skip_eval\<^sub>w C.assign_eval\<^sub>w C.seq_stop_eval\<^sub>w  dest!: C.seq_stop_elim)
  using C.seq_elim C.eval\<^sub>w.seq C.seq_stop_elim C.seq_stop_eval\<^sub>w stops_together_Stop_right stops_together_Stop_left 
  using C.seq_elim C.eval\<^sub>w.seq C.seq_stop_elim C.seq_stop_eval\<^sub>w stops_together_Stop_right stops_together_Stop_left no_await_seqE apply metis
  using C.seq_elim C.eval\<^sub>w.seq C.seq_stop_elim C.seq_stop_eval\<^sub>w stops_together_Stop_right stops_together_Stop_left no_await_seqE apply metis
  using C.if_eval\<^sub>w C.if_elim no_await_ifE
  apply meson
  using C.if_eval\<^sub>w C.if_elim no_await_ifE
  apply meson
  using C.eval\<^sub>w_mem_independent no_await_ModeUpdE
                     apply (meson C.no_await.intros(7))
  using C.eval\<^sub>w_mem_independent no_await_ModeUpdE
                     apply (meson C.no_await.intros(7))
  using C.eval\<^sub>w_mem_independent apply blast+
                 apply (metis C.no_await_no_await)+
  using C.eval\<^sub>w_mem_independent no_await_ModeUpdE
                     apply (meson C.no_await.intros(7))
  using C.eval\<^sub>w_mem_independent no_await_ModeUpdE
                     apply (meson C.no_await.intros(7))
  using C.eval\<^sub>w_mem_independent apply blast+
                 apply (metis C.no_await_no_await)+
  done

lemma stops_together_refl [simp]: "stops_together c c"
  apply(induction c, auto)
  done

lemma same_stopping_behaviour:
  "(\<langle>c\<^sub>1\<^sub>C, mds\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C, \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C) \<in> rel_inv\<^sub>C \<Longrightarrow> 
       stops\<^sub>C \<langle>c\<^sub>1\<^sub>C, mds\<^sub>C, mem\<^sub>1\<^sub>C\<rangle>\<^sub>C = stops\<^sub>C \<langle>c\<^sub>2\<^sub>C, mds\<^sub>C, mem\<^sub>2\<^sub>C\<rangle>\<^sub>C"
  apply(induction rule: rel_inv\<^sub>C.induct)
            apply(rule decide_stops,fastforce,simp add: C.no_await.intros,simp add: C.no_await.intros)+
  done

lemma RefRel_HighBranch_simpler_refinement_safe:
  "simpler_refinement_safe A.R RefRel_HighBranch rel_inv\<^sub>C abs_steps"
  apply(unfold simpler_refinement_safe_def)
  apply clarsimp
  apply(rule conjI)
   (* New obligation 1.1: Consistent stopping behaviour for coupling invariant-related
                          concrete configurations *)
   using same_stopping_behaviour apply blast
  apply(rule conjI)
   (* New obligation 1.2: Consistent step-introduction/elimination rate. *)
   using consistent_abs_steps apply blast
  (* New obligation 1.3: Closedness of the coupling invariant
                         (and preservation of mds-equality)
                         under lockstep execution. *)
  using rel_inv\<^sub>C_closed_lockstep_keeps_mds_consistent apply blast
  done

lemma RefRel_HighBranch_secure_refinement_simpler:
  "secure_refinement_simpler A.R RefRel_HighBranch rel_inv\<^sub>C abs_steps"
  apply(unfold secure_refinement_simpler_def)
  apply(rule conjI)
   apply(rule closed_others_RefRel_HighBranch)
  apply(rule conjI)
   apply(rule preserves_modes_mem_RefRel_HighBranch)
  apply(rule conjI)
   apply(rule new_vars_private_RefRel_HighBranch)
  apply(rule conjI)
   (* New obligation 1: simpler_refinement_safe *)
   using RefRel_HighBranch_simpler_refinement_safe apply blast
  apply(rule conjI)
   apply(rule rel_inv\<^sub>C_closed_glob_consistent)
  apply clarsimp
  (* New obligation 2: square-shaped diagram *)
  apply(rule induction_simulation_RefRel_HighBranch, simp+)
  done

lemma RefRel_HighBranch_secure_refinement:
  "secure_refinement A.R RefRel_HighBranch rel_inv\<^sub>C"
  using RefRel_HighBranch_secure_refinement_simpler secure_refinement_simpler
  by blast

lemma strong_low_bisim_mm_R\<^sub>C_of_R:
  "C.strong_low_bisim_mm (R\<^sub>C_of A.R RefRel_HighBranch rel_inv\<^sub>C)"
  apply(rule R\<^sub>C_of_strong_low_bisim_mm)
    apply(rule A.strong_low_bisim_mm_R)
   apply(rule RefRel_HighBranch_secure_refinement)
  apply(rule rel_inv\<^sub>C_sym)
  done

definition
  mds\<^sub>0 :: "Mode \<Rightarrow> var\<^sub>C set"
where
  "mds\<^sub>0 \<equiv> \<lambda>m. case m of AsmNoReadOrWrite \<Rightarrow> {reg0, reg1, reg2, reg3} |
                        AsmNoWrite \<Rightarrow> {} |
                        _ \<Rightarrow> {}"

lemma regs_the_only_concrete_only_vars:
  "v\<^sub>C \<notin> range var\<^sub>C_of \<Longrightarrow> v\<^sub>C \<in> {reg0, reg1, reg2, reg3}"
  by (case_tac v\<^sub>C, (clarsimp|metis rangeI var\<^sub>C_of.simps)+)

lemma prog_high_branch_RefRel:
  "(\<langle>A.prog_high_branch, mds\<^sub>A_of mds\<^sub>0, mem\<^sub>A_of mem\<^sub>C\<rangle>\<^sub>A, \<langle>C.prog_high_branch\<^sub>C, mds\<^sub>0, mem\<^sub>C\<rangle>\<^sub>C) \<in> RefRel_HighBranch"
  unfolding A.prog_high_branch_def C.prog_high_branch\<^sub>C_def mds\<^sub>0_def
  using regs_the_only_concrete_only_vars doesnt_have_mode has_mode\<^sub>A NoRW\<^sub>A_implies_NoRW\<^sub>C
  apply clarsimp

  apply(rule acq_mode_rel, (simp|clarsimp)+)
  apply(drule C.seq_decl_elim, clarsimp)
  apply(rule stop_seq_rel, simp+)

  apply(rule acq_mode_rel, (simp|clarsimp)+)
  apply(drule C.seq_decl_elim, clarsimp)
  apply(rule stop_seq_rel, simp+)

  apply(rule acq_mode_rel, (simp|clarsimp)+)
  apply(drule C.seq_decl_elim, clarsimp)
  apply(rule stop_seq_rel, simp+)

  apply(rule assign_const_rel, (simp|clarsimp)+)
  apply(drule C.seq_assign_elim, clarsimp)
  apply(rule stop_seq_rel, simp+)
  
  apply(rule assign_const_rel, (simp|clarsimp)+)
  apply(drule C.seq_assign_elim, clarsimp)
  apply(rule stop_seq_rel, simp+)

  apply(rule assign_load_rel, (simp|clarsimp)+)
  apply(drule C.seq_assign_elim, clarsimp)
  apply(rule stop_seq_rel, simp+)

  apply(rule if_reg_load_rel, (simp|clarsimp)+)
  apply(drule C.seq_assign_elim, clarsimp)
  apply(rule if_reg_stop_rel, (simp|clarsimp)+)

  apply(drule C.seq_stop_elim, clarsimp)

  apply(rule if_reg_rel, (simp|clarsimp)+)
   apply(rule rel_inv\<^sub>C_1, clarsimp+)
  apply(rule conjI)

   (* Then *)
   apply clarsimp
   apply(rule if_then_rel_1, (simp|clarsimp)+)
   apply(drule C.if_elim, simp+)
   apply(drule C.seq_elim, simp+)
   apply(drule C.skip_elim, simp+)
   apply(rule if_then_rel_1', (simp|clarsimp)+)
   apply(drule C.seq_stop_elim, simp+)
   apply(rule if_then_rel_2, (simp|clarsimp)+)
   apply(drule C.seq_elim, simp+)
   apply(drule C.skip_elim, simp+)
   apply(rule if_then_rel_2', (simp|clarsimp)+)
   apply(drule C.seq_stop_elim, simp+)
   apply(rule if_then_rel_3, (simp|clarsimp)+)
   apply(drule C.seq_assign_elim, simp+)
   apply(rule if_then_rel_3', (simp|clarsimp)+)
   apply(drule C.seq_stop_elim, simp+)
   apply(rule if_then_rel_4, (simp|clarsimp)+)
   apply(drule C.assign_elim, simp+)
   apply(rule stop_rel)

  (* Else *)
  apply clarsimp
  apply(rule if_else_rel_1, (simp|clarsimp)+)
  apply(drule C.if_elim, simp+)
  apply(drule C.seq_assign_elim, simp+)
  apply(rule if_else_rel_1', (simp|clarsimp)+)
  apply(drule C.seq_stop_elim, simp+)
  apply(rule if_else_rel_2, (simp|clarsimp)+)
  apply(drule C.seq_assign_elim, simp+)
  apply(rule if_else_rel_2', (simp|clarsimp)+)
  apply(drule C.seq_stop_elim, simp+)
  apply(rule if_else_rel_3, (simp|clarsimp)+)
  apply(drule C.seq_assign_elim, simp+)
  apply(rule if_else_rel_3', (simp|clarsimp)+)
  apply(drule C.seq_stop_elim, simp+)
  apply(rule if_else_rel_4, (simp|clarsimp)+)
  apply(drule C.assign_elim, simp+)
  apply(rule stop_rel)
  done

lemma mds\<^sub>s_A_of_mds\<^sub>0:
  "mds\<^sub>s = mds\<^sub>A_of mds\<^sub>0"
  apply(rule preserves_modes_mem_mds\<^sub>A_simp)
  apply clarsimp
  apply(case_tac m)
     unfolding mds\<^sub>s_def mds\<^sub>0_def mds\<^sub>A_of_def
     apply(clarsimp simp: reg_not_visible_abs)+
  done

lemma A_of_mds\<^sub>0_is_mds\<^sub>s:
  "mds\<^sub>A_of mds\<^sub>0 = mds\<^sub>s"
  by (simp add: mds\<^sub>s_A_of_mds\<^sub>0)

lemma prog_high_branch\<^sub>C_in_R\<^sub>C_of_R:
  "C.low_mds_eq mds\<^sub>0 mem\<^sub>1 mem\<^sub>2 \<Longrightarrow>
       (\<langle>C.prog_high_branch\<^sub>C, mds\<^sub>0, mem\<^sub>1\<rangle>\<^sub>C, \<langle>C.prog_high_branch\<^sub>C, mds\<^sub>0, mem\<^sub>2\<rangle>\<^sub>C)
        \<in> (R\<^sub>C_of A.R RefRel_HighBranch rel_inv\<^sub>C)"
  apply(clarsimp simp: R\<^sub>C_of_def)
  apply(rule_tac x=A.prog_high_branch in exI)
  apply(rule_tac x="mds\<^sub>A_of mds\<^sub>0" in exI)
  apply(rule_tac x="mem\<^sub>A_of mem\<^sub>1" in exI)
  apply(rule conjI)
   apply(rule prog_high_branch_RefRel)
  apply(rule_tac x=A.prog_high_branch in exI)
  apply(rule_tac x="mds\<^sub>A_of mds\<^sub>0" in exI)
  apply(rule_tac x="mem\<^sub>A_of mem\<^sub>2" in exI)
  apply(rule conjI)
   apply(rule prog_high_branch_RefRel)
  apply(simp add: A_of_mds\<^sub>0_is_mds\<^sub>s)
  apply(rule conjI)
   apply(rule A.prog_high_branch_secure')
   apply(clarsimp simp: A.low_mds_eq_def)
   apply(case_tac x)
   unfolding C.low_mds_eq_def mem\<^sub>A_of_def dma_def dma\<^sub>C_def \<C>\<^sub>C_def mds\<^sub>0_def
   apply clarsimp+
  by (force intro:rel_inv\<^sub>C_default simp:C.prog_high_branch\<^sub>C_def C.no_await.intros)

lemma "C.com_sifum_secure (C.prog_high_branch\<^sub>C, mds\<^sub>0)"
  unfolding C.com_sifum_secure_def C.low_indistinguishable_def
  apply clarify
  apply(rule C.mm_equiv_intro)
    apply(rule strong_low_bisim_mm_R\<^sub>C_of_R)
   apply force
  by (rule prog_high_branch\<^sub>C_in_R\<^sub>C_of_R)

end

end
