(*
Author: Toby Murray
*)

theory Example_CopyInv
imports "../logic/LockTypeSystemSimpl"
begin

datatype addr = input_var | output_var | input_clas | output_clas
datatype lock_addr = lock
type_synonym mem = "((lock_addr, addr) Var, nat) Mem"
type_synonym prog_mem = "(addr, nat) Mem"
type_synonym lock_mem = "((addr, lock_addr) Var, nat) Mem"

datatype aexp = Load "addr" | Const "nat"

fun
  ev\<^sub>A :: "prog_mem \<Rightarrow> aexp \<Rightarrow> nat"
where
  "ev\<^sub>A mem (Load x) = mem x" |
  "ev\<^sub>A mem (Const c) = c"

fun
  aexp_vars :: "aexp \<Rightarrow> addr set"
where
  "aexp_vars (Load x) = {x}" |
  "aexp_vars _ = {}"

primrec
  aexp_to_lexp :: "aexp \<Rightarrow> (addr,nat) lexp"
where
  "aexp_to_lexp (Load x) = (Var x)" |
  "aexp_to_lexp (Const x) = (lexp.Const x)"

datatype bexp = Same "addr" "addr" |
                NotSame "addr" "addr" |
                Eq "addr" "nat" |
                Neq "addr" "nat" |
                Or bexp bexp |
                And bexp bexp |
                TT | FF

primrec
  bexp_to_lpred :: "bexp \<Rightarrow> (addr,nat) lpred"
where
  "bexp_to_lpred (Same x y) = PCmp (=) (Var x) (Var y)" |
  "bexp_to_lpred (NotSame x y) = PNeg (PCmp (=) (Var x) (Var y))" |
  "bexp_to_lpred (Eq x a) = PCmp (=) (Var x) (lexp.Const a)" |
  "bexp_to_lpred (Neq x a) = PNeg (PCmp (=) (Var x) (lexp.Const a))" |
  "bexp_to_lpred (Or a b) = (PDisj (bexp_to_lpred a) (bexp_to_lpred b))" |
  "bexp_to_lpred (And a b) = (PConj (bexp_to_lpred a) (bexp_to_lpred b))" |
  "bexp_to_lpred TT = PTrue" |
  "bexp_to_lpred FF = PFalse"

lemma aexp_to_lexp_correct':
  "leval mem (aexp_to_lexp e) = ev\<^sub>A mem e"
  apply(induct e, auto)
  done


fun
  ev\<^sub>B :: "prog_mem \<Rightarrow> bexp \<Rightarrow> bool"
where
  "ev\<^sub>B mem (Same x y) = ((mem x) = (mem y))" |
  "ev\<^sub>B mem (NotSame x y) = ((mem x) \<noteq> (mem y))" |
  "ev\<^sub>B mem (Eq x c) = ((mem x) = c)" |
  "ev\<^sub>B mem (Neq x c) = ((mem x) \<noteq> c)" |
  "ev\<^sub>B mem (Or x y) = (ev\<^sub>B mem x \<or> ev\<^sub>B mem y)" |
  "ev\<^sub>B mem (And x y) = (ev\<^sub>B mem x \<and> ev\<^sub>B mem y)" |
  "ev\<^sub>B mem TT = True" |
  "ev\<^sub>B mem FF = False"

fun
  bexp_vars :: "bexp \<Rightarrow> addr set"
where
  "bexp_vars (Neq x c) = {x}" |
  "bexp_vars (Eq x c) = {x}" |
  "bexp_vars (Same x y) = {x,y}" |
  "bexp_vars (NotSame x y) = {x,y}" |
  "bexp_vars (Or x y) = bexp_vars x \<union> bexp_vars y" |
  "bexp_vars (And x y) = bexp_vars x \<union> bexp_vars y" |
  "bexp_vars _ = {}"

fun
  bexp_neg :: "bexp \<Rightarrow> bexp"
where
  "bexp_neg (Neq x y) = (Eq x y)" |
  "bexp_neg (Eq x y) = (Neq x y)" |
  "bexp_neg (Same x y) = (NotSame x y)" |
  "bexp_neg (NotSame x y) = (Same x y)" |
  "bexp_neg (Or x y) = And (bexp_neg x) (bexp_neg y)" |
  "bexp_neg (And x y) = Or (bexp_neg x) (bexp_neg y)" |
  "bexp_neg TT = FF" |
  "bexp_neg FF = TT"

fun
   bexp_assign :: "addr \<Rightarrow> aexp \<Rightarrow> bexp"
where
  "bexp_assign x (Load y) = (Same x y)" |
  "bexp_assign x (Const c) = (Eq x c)"


definition
  dma :: "mem \<Rightarrow> (lock_addr, addr) Var \<Rightarrow> Sec"
where
  "dma m v \<equiv> case v of
               ProgVar x \<Rightarrow>
                 (if x = input_var then (if (to_prog_mem m) input_clas = 0 then Low else High)
                  else if x = output_var then (if (to_prog_mem m) output_clas = 0 then Low else High)
                  else Low) |
               Lock l \<Rightarrow> Low"

definition
  \<C>_vars :: "(lock_addr, addr) Var \<Rightarrow> (lock_addr, addr) Var set"
where
  "\<C>_vars v \<equiv> case v of
                ProgVar x \<Rightarrow>
                  (if x = input_var then {ProgVar input_clas}
                   else if x = output_var then {ProgVar output_clas}
                   else {}) |
                Lock l \<Rightarrow> {}"

fun
  ev\<^sub>L :: "mem \<Rightarrow> lock_addr \<Rightarrow> bool"
where
  "ev\<^sub>L mem l = ((to_lock_mem mem) l \<noteq> 0)"

definition
  lock_acq_sem :: "lock_addr \<Rightarrow> nat \<Rightarrow> nat"
where
  "lock_acq_sem l v \<equiv> 1"

definition
  lock_rel_sem :: "lock_addr \<Rightarrow> nat \<Rightarrow> nat"
where
  "lock_rel_sem l v \<equiv> 0"

definition
  mds\<^sub>s :: "Mode \<Rightarrow> 'Var set"
where
  "mds\<^sub>s \<equiv> \<lambda>_. {}"

definition
  lock_interp :: "lock_addr \<Rightarrow> (addr set \<times> addr set)"
where
  "lock_interp l \<equiv> case l of
       lock \<Rightarrow> ({},UNIV)"

definition
  lock_inv :: "lock_addr \<Rightarrow> (addr, nat) lpred"
where
  "lock_inv l \<equiv> case l of lock \<Rightarrow> PCmp (=) (Var input_clas) (Var output_clas)"

lemma lock_inv_sat: "\<exists>mem. lpred_eval mem (lock_inv l)"
  apply(case_tac l)
   apply(clarsimp simp: lock_inv_def)+
   apply(rule_tac x="\<lambda>_. undefined" in exI, auto)[1]
  done


lemma UNIV[simp]:
  "UNIV = {ProgVar input_var, ProgVar output_var, ProgVar input_clas, ProgVar output_clas, Lock lock}"
  apply(clarsimp | safe)+
   apply (metis Var.exhaust addr.exhaust lock_addr.exhaust)
  by auto

lemma finite_Vars:
  "finite {x::(lock_addr,addr) Var. True}"
  by simp

lemma ev\<^sub>A_det:
  " \<forall>x\<in>aexp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<Longrightarrow> ev\<^sub>A mem\<^sub>1 e = ev\<^sub>A mem\<^sub>2 e"
  apply(induct e, auto)
  done

lemma ev\<^sub>B_det:
  " \<forall>x\<in>bexp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<Longrightarrow> ev\<^sub>B mem\<^sub>1 e = ev\<^sub>B mem\<^sub>2 e"
  apply(induct e, auto)
  done


abbreviation
  \<C> :: "(lock_addr, addr) Var set"
where
  "\<C> \<equiv> \<Union>x. \<C>_vars x"

fun
  dma_type :: "addr \<Rightarrow> bexp"
where
  "dma_type input_var = (Eq input_clas 0)" |
  "dma_type output_var = (Eq output_clas 0)" |
  "dma_type _ = TT"

definition
  dma_type' :: "(lock_addr, addr) Var \<Rightarrow> bexp"
where
  "dma_type' v = (case v of Lock l \<Rightarrow> TT |
                            ProgVar x \<Rightarrow> dma_type x)"

lemma bexp_neg_negates':
  "ev\<^sub>B mem (bexp_neg e) = (\<not> ev\<^sub>B mem e)"
  by (induct e rule:ev\<^sub>B.induct, simp+)

lemma bexp_neg_vars':
  "bexp_vars (bexp_neg e) = bexp_vars e"
  by (induct e, simp+)

definition 
  addr_ORDERING :: "addr list"
where
  "addr_ORDERING \<equiv> [ input_var, output_var, input_clas, output_clas ]"


primrec 
  index_of :: "'a \<Rightarrow> 'a list \<Rightarrow> nat option"
where
  "index_of x [] = None" |
  "index_of x (y#ys) = (if x = y then Some 0 else map_option Suc (index_of x ys))"

lemma addr_ORDERING_complete:
  "\<exists>i. index_of a addr_ORDERING = Some i"
  apply(case_tac a, (auto simp: addr_ORDERING_def))
  done


lemma nat_less_cases:
  "(a::nat) < b \<or> b < a \<or> a = b"
  using nat_neq_iff by blast

lemma index_of_inject:
  "index_of a xs = Some i \<Longrightarrow> index_of b xs = Some i \<Longrightarrow> a = b"
  apply(induct xs arbitrary: i, auto split: if_splits)
  done


instantiation addr :: linorder
begin

definition
  less_eq_addr :: "addr \<Rightarrow> addr \<Rightarrow> bool"
where
  "less_eq_addr a b \<equiv> a = b \<or> the (index_of a addr_ORDERING) < the (index_of b addr_ORDERING)"

definition
  less_addr :: "addr \<Rightarrow> addr \<Rightarrow> bool"
where
  "less_addr a b \<equiv> the (index_of a addr_ORDERING) < the (index_of b addr_ORDERING)"


instance 
  apply(intro_classes)
      apply(clarsimp simp: less_addr_def less_eq_addr_def | safe)+
  using addr_ORDERING_complete index_of_inject
  by (metis option.sel)

lemma ev\<^sub>L_det:
  "mem (Lock l) = mem' (Lock l) \<Longrightarrow> ev\<^sub>L mem l  = ev\<^sub>L mem' l"
  apply(auto simp: to_lock_mem_def)
  done

lemma bexp_to_lpred_correct':
  "lpred_eval mem (bexp_to_lpred e) = ev\<^sub>B mem e"
  apply(induct e, auto)
  done

lemma aexp_to_lexp_vars':
  "lexp_vars (aexp_to_lexp e) = aexp_vars e"
  apply(induct e, auto)
  done

lemma bexp_to_lpred_vars':
  "lpred_vars (bexp_to_lpred e) = bexp_vars e"
  apply(induct e, auto)
  done

end

interpretation sifum_types undefined ev\<^sub>A ev\<^sub>B ev\<^sub>L aexp_vars bexp_vars 
                            lock_interp lock_inv lock_acq_sem lock_rel_sem 
                            aexp_to_lexp bexp_to_lpred dma \<C>_vars \<C> dma_type
  apply(unfold_locales)
  apply(rule lock_inv_sat)
  using ev\<^sub>A_det ev\<^sub>B_det apply auto[3]
  using ev\<^sub>L_det apply blast
  apply (auto  simp: lock_inv_def  split: Var.splits simp: dma_def to_prog_mem_def  \<C>_vars_def to_lock_mem_def lock_acq_sem_def lock_rel_sem_def lock_interp_def aexp_to_lexp_correct' bexp_to_lpred_correct' aexp_to_lexp_vars' bexp_to_lpred_vars' split: lock_addr.splits)[9]
          apply(auto simp: \<C>_vars_def split: Var.splits simp: dma_def)[1]  
         apply(auto simp: \<C>_vars_def split: Var.splits simp: dma_def to_prog_mem_def)[2]  
       apply(case_tac x, auto simp:dma_def to_prog_mem_def dma_type'_def)[1]
      apply(case_tac x, auto simp: \<C>_vars_def to_prog_mem_def dma_type'_def)[1]
     apply(force simp: \<C>_vars_def to_prog_mem_def dma_type'_def)
    apply(force simp:dma_def)
   apply(force simp:\<C>_vars_def lock_interp_def split:if_splits lock_addr.splits)
  by auto


declare Collect_conj_eq [simp]
declare Collect_disj_eq [simp]
declare subtype_def [simp]
declare dma_type'_def [simp]
declare pred_entailment_def [simp]
declare pred_def [simp]
declare to_prog_mem_def [simp]
declare add_anno_def [simp]
declare restrict_map_def [simp]
declare add_anno_dom_def [simp]
declare lock_interp_def [simp]
declare add_anno_stable_def [simp]
declare context_equiv_def [simp]
declare type_equiv_def [simp]
declare tyenv_wellformed_def [simp]
declare env_consistent_def [simp]
declare types_wellformed_def [simp]
declare types_stable_def [simp]
declare type_wellformed_def [simp]

declare add_pred_def [simp]
declare stable_def [simp]
declare to_total_def [simp]

declare anno_type_stable_def [simp]
declare assign_post_def [simp]

declare \<C>_vars_def [simp]
declare \<C>_vars\<^sub>V_def [simp]
declare \<C>\<^sub>V_def [simp]

declare stable_NoW_def [simp]
declare stable_NoRW_def [simp]
declare lock_inv_def [simp]

declare aexp_readable_def [simp]
declare bexp_readable_def [simp]
declare var_readable_def [simp]

(* The following lemmas, if_fun_(disj|not_conj) make it possible for the simplifier
  to reduce the typing environments to the user-friendly maplet form armed only with
  the definitions for restrict_preds_to_vars, etc. *)

lemma if_fun_disj[simp]:
  "(\<lambda>x. if x = a | P x then (f x) else g x)
    = ((\<lambda>x. if P x then (f x) else g x)(a := f a))"
  "(\<lambda>x. if x = a then (f x) else g x)
    = (g(a := f a))"
  by auto

lemma if_fun_not_conj[simp]:
  "(\<lambda>x. if x \<noteq> a \<and> P x then (f x) else g x)
    = ((\<lambda>x. if x = a \<or> \<not> P x then (g x) else f x))"
  "(\<lambda>x. if x \<noteq> a then (f x) else g x)
    = ((\<lambda>x. if x = a then (g x) else f x))"
  by auto

lemma if_fun_conj[simp]:
  "(\<lambda>x. if x = a \<and> P x then (f x) else g x)
    = ((\<lambda>x. if x \<noteq> a \<or> \<not> P x then (g x) else f x))"
  "(\<lambda>x. if x = a then (f x) else g x)
    = ((\<lambda>x. if x \<noteq> a then (g x) else f x))"
  by auto

lemma type_aexpr_Load:
  "v \<notin> dom \<Gamma> \<Longrightarrow> type_aexpr \<Gamma> (Load v) (PConj (bexp_to_lpred (dma_type v)) PTrue)"
  apply(insert type_aexpr[of \<Gamma> "Load v", simplified])
  by auto

lemma type_aexpr_Load':
  "v \<in> dom \<Gamma> \<Longrightarrow> t = (the (\<Gamma> v)) \<Longrightarrow> type_aexpr \<Gamma> (Load v) (PConj t PTrue)"
  apply(insert type_aexpr[of \<Gamma> "Load v", simplified])
  by simp

lemma type_aexpr_Const:
  "type_aexpr \<Gamma> (Const c) PTrue"
  apply(insert type_aexpr[of \<Gamma> "Const c", simplified])
  by simp

definition
  prog_Copy :: "(addr, lock_addr, aexp, bexp) Stmt"
where
  "prog_Copy \<equiv>
     LockAcq lock ;;
     Assign output_var (Load input_var) ;;
     LockRel lock"

lemma [simp]:
  "var_modifiable {lock} output_var"
  by(auto simp: var_modifiable_def split: lock_addr.splits)

lemma [simp]:
  "var_modifiable {lock} input_var"
  by(auto simp: var_modifiable_def split: lock_addr.splits)

lemma [simp]:
  "var_readable {lock} output_var"
  by(auto simp: var_modifiable_def split: lock_addr.splits)

lemma [simp]:
  "var_readable {lock} input_var"
  by(auto simp: var_modifiable_def split: lock_addr.splits)

lemma [simp]: "UNIV = {output_clas, input_clas, output_var, input_var}"
  using addr.exhaust apply(safe | clarsimp | blast)+
  done

(* FIXME: why aren't these defined in LockTypeSystemSimpl ? *)
method has_type_simpl_pre = (rule has_type_simpl_pre, solves \<open>(rule pred_simpl.intros)+\<close>)+

method has_type_context_simpl_pre = (rule has_type_context_simpl_pre, solves \<open>(rule context_simpl.intros, (blast intro: pred_simpl.intros)+)+\<close>)+

method has_type_context_simpl_post = (rule has_type_context_simpl_post, solves \<open>(rule context_simpl.intros)+\<close>)+

lemma prog_Copy_typed:
  "\<exists>\<Gamma>' \<S>' P'. has_type Map.empty {} PTrue prog_Copy \<Gamma>' \<S>' P'"
  apply(intro exI)
  apply(simp add: prog_Copy_def)
  apply(rule seq_type)
   apply(rule lock_acq_type, simp+)[1]
  apply(has_type_simpl_pre)
  apply(rule seq_type)
   apply(rule assign\<^sub>2, simp+)
        apply(rule type_aexpr_Load',simp+)
   apply(clarsimp split: lock_addr.splits)
  apply(has_type_context_simpl_pre)
  apply(has_type_simpl_pre)
  (* need to do some rewriting of output_var back to its original type since we have to
     show it is consistent with its original type when we release the lock *)
  apply(rule_tac \<Gamma>\<^sub>1="[input_var  \<mapsto> PCmp (=) (Var input_clas) (lexp.Const 0),
                      output_var \<mapsto> PCmp (=) (Var output_clas) (lexp.Const 0)]" in sub)
         prefer 6
         apply(rule pred_entailment_refl)
        apply(rule lock_rel_type,simp+)
      apply blast
     apply blast
    apply simp+
  done


end
