(*
Author: Robert Sison
*)

theory Example_Worker
imports "../old-type-system/WhileLockTypeSystem"
  "~~/src/HOL/Word/Word" "~~/src/HOL/Eisbach/Eisbach"
  "../old-type-system/LocallySoundWhileLockUse"
  "../wr-compiler/ExprsMemWithBinOps"
begin

text {*
  An input processing worker thread.
*}

datatype addr =
    source | domain | workspace | suspended | low_sink | high_sink

instantiation addr :: "{enum}"
begin

abbreviation
  "addr_enum \<equiv> [
    source, domain, workspace, suspended, low_sink, high_sink
    ]"

definition enum_addr_def:
  "enum_class.enum \<equiv> addr_enum"

definition enum_addr_all_def: "enum_class.enum_all P \<equiv> \<forall>r \<in> set addr_enum. P r"
definition enum_addr_ex_def: "enum_class.enum_ex P \<equiv> \<exists>r \<in> set addr_enum. P r"

instance proof
  show "(UNIV::addr set) = set enum_class.enum"
  unfolding enum_addr_def
  apply(clarsimp | safe)+
  by (rename_tac x, case_tac x, auto)
next
  show "distinct (enum_class.enum::addr list)"
  unfolding enum_addr_def by simp
next
  show "\<And>P::addr \<Rightarrow> bool. enum_class.enum_all P = Ball UNIV P"
  unfolding enum_addr_all_def
  apply(clarsimp | safe)+
  by (rename_tac x, case_tac x, auto)
next
  show "\<And>P::addr \<Rightarrow> bool. enum_class.enum_ex P = Bex UNIV P"
  unfolding enum_addr_ex_def
  by (auto, rename_tac x, case_tac x, auto)
qed
end

datatype lock_addr =
    workspace_lock | source_lock

type_synonym mem = "((lock_addr, addr) Var, 8 word) Mem"
type_synonym prog_mem = "(addr, 8 word) Mem"
type_synonym lock_mem = "((addr, lock_addr) Var, 8 word) Mem"

(* Chosen arbitrarily, but to be distinct *)
definition DOMAIN_INVALID :: "8 word" where "DOMAIN_INVALID = -1"
definition DOMAIN_LOW :: "8 word" where "DOMAIN_LOW = 0"
definition DOMAIN_HIGH :: "8 word" where "DOMAIN_HIGH = 1"
(* Chosen arbitrarily *)
definition LOW_BITMASK :: "8 word" where "LOW_BITMASK = 42"

definition
  dma :: "mem \<Rightarrow> (lock_addr, addr) Var \<Rightarrow> Sec"
where
  "dma m v \<equiv> case v of
               Var x \<Rightarrow>
                 if x = source
                   then if (to_prog_mem m) domain = DOMAIN_LOW then Low else High
                   else if x = high_sink then High else Low |
               Lock l \<Rightarrow> Low"

definition
  \<C>_vars :: "(lock_addr, addr) Var \<Rightarrow> (lock_addr, addr) Var set"
where
  "\<C>_vars v \<equiv> case v of
                Var x \<Rightarrow>
                  if x = source
                    then {Var domain}
                    else {} |
                Lock l \<Rightarrow> {}"

definition
  mds\<^sub>s :: "Mode \<Rightarrow> 'Var set"
where
  "mds\<^sub>s \<equiv> \<lambda>_. {}"

fun
  ev\<^sub>L :: "8 word \<Rightarrow> bool"
where
  "ev\<^sub>L w = (w \<noteq> 0)"

definition
  val\<^sub>L :: "bool \<Rightarrow> 8 word"
where
  "val\<^sub>L b \<equiv> if b then 1 else 0 "

datatype bin_op_id = BEq | BNeq | BOr | BAnd | BitAnd

fun bin_op_ev :: "bin_op_id \<Rightarrow> 8 word \<Rightarrow> 8 word \<Rightarrow> 8 word"
where
  "bin_op_ev BEq x y = val\<^sub>L (x = y)" |
  "bin_op_ev BNeq x y = val\<^sub>L (x \<noteq> y)" |
  "bin_op_ev BOr x y = val\<^sub>L (ev\<^sub>L x \<or> ev\<^sub>L y)" |
  "bin_op_ev BAnd x y = val\<^sub>L (ev\<^sub>L x \<and> ev\<^sub>L y)" |
  "bin_op_ev BitAnd x y = x AND y"

definition
  lock_interp :: "lock_addr \<Rightarrow> (addr set \<times> addr set)"
where
  "lock_interp l \<equiv> case l of
       workspace_lock \<Rightarrow> ({}, {workspace}) |
       source_lock \<Rightarrow> ({source, domain}, {})"

definition
  \<C> :: "(lock_addr, addr) Var set"
where
  "\<C> \<equiv> \<Union>x. \<C>_vars x"

(* This way of proving finite_Vars is Toby's from cddc-buf development head *)
lemma UNIV[simp]:
  "UNIV = Var ` {source, domain, workspace, suspended, low_sink, high_sink} \<union>
    Lock ` {workspace_lock, source_lock}"
  apply (clarsimp | safe)+
  apply(rename_tac x, case_tac x, auto)+
  done

lemma UNIV_rewrite:
  "UNIV = (Var ` UNIV \<union> Lock ` UNIV)"
  apply (clarsimp | safe)+
  apply(case_tac x, auto)
  done

lemma finite_Vars:
  "finite {x::(lock_addr,addr) Var. True}"
  apply simp
  done

lemma bin_op_ev_det:
  "\<forall>x\<in>op_set_param_lang.exp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<Longrightarrow>
   op_set_param_lang.exp_ev bin_op_ev mem\<^sub>1 e = op_set_param_lang.exp_ev bin_op_ev mem\<^sub>2 e"
  apply(induct e)
    apply(force simp:op_set_param_lang.exp_ev.simps(1))
   apply(simp add:op_set_param_lang.exp_ev.simps(2) op_set_param_lang.exp_vars.simps(2))
  by (simp add:op_set_param_lang.exp_vars.simps(3) op_set_param_lang.exp_ev.simps(3))

global_interpretation
  OSPL: op_set_param_lang bin_op_ev
  defines OSPL_exp_vars = OSPL.exp_vars
  done

interpretation
  WL: sifum_lang_no_dma OSPL.exp_ev OSPL.exp_ev\<^sub>B ev\<^sub>L OSPL.exp_vars OSPL.exp_vars lock_interp 1 0
  apply(unfold_locales)
      using finite_Vars apply blast
     unfolding OSPL_exp_vars_def using bin_op_ev_det apply blast
    apply(clarsimp simp:op_set_param_lang.exp_ev\<^sub>B_def)
    using bin_op_ev_det apply force
   apply force
  by force

fun
  exp_neg :: "(8 word, addr, bin_op_id) exp \<Rightarrow> (8 word, addr, bin_op_id) exp"
where
  "exp_neg (EConst c) = EOp BEq (EConst c) (EConst 0)" |
  "exp_neg (ELoad v) = EOp BEq (ELoad v) (EConst 0)" |
  "exp_neg (EOp BEq x y) = EOp BNeq x y" |
  "exp_neg (EOp BNeq x y) = EOp BEq x y" |
  "exp_neg (EOp BOr x y) = EOp BAnd (exp_neg x) (exp_neg y)" |
  "exp_neg (EOp BAnd x y) = EOp BOr (exp_neg x) (exp_neg y)" |
  "exp_neg (EOp BitAnd x y) = EOp BEq (EOp BitAnd x y) (EConst 0)"

declare val\<^sub>L_def [simp]

lemma exp_neg_negates:
  "OSPL.exp_ev\<^sub>B mem (exp_neg e) = (\<not> OSPL.exp_ev\<^sub>B mem e)"
  unfolding OSPL.exp_ev\<^sub>B_def
  apply(induct e, simp_all)
  apply(rename_tac opid e1 e2)
  by (case_tac opid, simp+)

fun
  dma_type :: "addr \<Rightarrow> (8 word, addr, bin_op_id) exp set"
where
  "dma_type high_sink = {EConst 0}" |
  "dma_type source = {EOp BEq (ELoad domain) (EConst DOMAIN_LOW)}" |
  "dma_type _ = {}"

definition
  dma_type' :: "(lock_addr, addr) Var \<Rightarrow> (8 word, addr, bin_op_id) exp set"
where
  "dma_type' v = (case v of Lock l \<Rightarrow> {} |
                            Var x \<Rightarrow> dma_type x)"

fun
  exp_assign :: "addr \<Rightarrow> (8 word, addr, bin_op_id) exp \<Rightarrow> (8 word, addr, bin_op_id) exp"
where
  "exp_assign x (EConst c) = EOp BEq (ELoad x) (EConst c)" |
  "exp_assign x (ELoad y) = EOp BEq (ELoad x) (ELoad y)" |
  "exp_assign x (EOp opid y z) = EOp BEq (ELoad x) (EOp opid y z)"

interpretation
  TS: sifum_types_assign OSPL.exp_ev OSPL.exp_ev\<^sub>B ev\<^sub>L OSPL.exp_vars OSPL.exp_vars lock_interp dma \<C>_vars \<C> 1 0 exp_neg dma_type' "EConst 0" exp_assign
  apply(unfold_locales)
                using finite_Vars apply blast
               apply(force simp:\<C>_vars_def split:Var.splits)
              apply(force simp:\<C>_vars_def dma_def to_prog_mem_def split:Var.splits)
             apply(force simp:\<C>_vars_def dma_def)
            using WL.eval\<^sub>w_det apply force
           using exp_neg_negates apply blast
          apply(case_tac x, clarsimp simp:dma_def to_prog_mem_def dma_type'_def)
          apply(rename_tac x')
          apply(case_tac x', auto simp:dma_def to_prog_mem_def dma_type'_def OSPL.exp_ev\<^sub>B_def)[1]
         apply(case_tac x, clarsimp simp:\<C>_vars_def to_prog_mem_def dma_type'_def)
         apply(rename_tac x')
         apply(case_tac x', auto simp:\<C>_vars_def to_prog_mem_def dma_type'_def OSPL.exp_ev\<^sub>B_def)[1]
        apply(force simp:OSPL.exp_ev\<^sub>B_def)
       apply force
      apply(force simp:\<C>_vars_def to_prog_mem_def dma_type'_def)
     apply(force simp:\<C>_vars_def lock_interp_def split:lock_addr.splits if_splits)
    apply(clarsimp simp:OSPL.exp_ev\<^sub>B_def)
    apply(case_tac e, simp_all)
   apply(case_tac e, force+)
  by (force simp:\<C>_vars_def \<C>_def)

interpretation TM: sifum_modes OSPL.exp_ev OSPL.exp_ev\<^sub>B ev\<^sub>L OSPL.exp_vars OSPL.exp_vars lock_interp dma \<C>_vars \<C> 1 0
  by (unfold_locales, auto simp: \<C>_vars_def split:Var.splits)

declare WL.restrict_preds_to_vars_def [simp]
declare Collect_conj_eq [simp]
declare Collect_disj_eq [simp]
declare TS.subtype_def [simp]
declare dma_type'_def [simp]
declare TS.pred_entailment_def [simp]
declare pred_def [simp]
declare to_prog_mem_def [simp]
declare TS.add_anno_def [simp]
declare restrict_map_def [simp]
declare TS.add_anno_dom_def [simp]
declare lock_interp_def [simp]
declare TS.add_anno_stable_def [simp]
declare TS.context_equiv_def [simp]
declare TS.type_equiv_def [simp]
declare TS.tyenv_wellformed_def [simp]
declare TS.mds_consistent_def [simp]
declare TS.types_wellformed_def [simp]
declare TS.types_stable_def [simp]
declare TS.type_wellformed_def [simp]

declare WL.pred_def [simp]
declare WL.add_pred_def [simp]
declare WL.stable_def [simp]
declare TS.to_total_def [simp]

declare OSPL.exp_ev\<^sub>B_def [simp]

declare TS.anno_type_stable_def [simp]
declare TS.assign_post_def [simp]

declare \<C>_vars_def [simp]
declare insert_Diff_if [simp]

(* The following lemmas, if_fun_(disj|not_conj) make it possible for the simplifier
  to reduce the typing environments to the user-friendly maplet form armed only with
  the definitions for restrict_preds_to_vars, etc. *)

lemma if_fun_disj[simp]:
  "(\<lambda>x. if x = a | P x then (f x) else g x)
    = ((\<lambda>x. if P x then (f x) else g x)(a := f a))"
  "(\<lambda>x. if x = a then (f x) else g x)
    = (g(a := f a))"
  by auto

lemma if_fun_not_conj[simp]:
  "(\<lambda>x. if x \<noteq> a \<and> P x then (f x) else g x)
    = ((\<lambda>x. if x = a \<or> \<not> P x then (g x) else f x))"
  "(\<lambda>x. if x \<noteq> a then (f x) else g x)
    = ((\<lambda>x. if x = a then (g x) else f x))"
  by auto

(* An experimental one I committed to cddc-buf branch *)
lemma if_fun_conj[simp]:
  "(\<lambda>x. if x = a \<and> P x then (f x) else g x)
    = ((\<lambda>x. if x \<noteq> a \<or> \<not> P x then (g x) else f x))"
  "(\<lambda>x. if x = a then (f x) else g x)
    = ((\<lambda>x. if x \<noteq> a then (g x) else f x))"
  by auto

(* New one again thanks to Thomas Sewell - useful for slightly more complex case *)
lemma if_fun_conj_disj[simp]:
  "Q a \<Longrightarrow> (\<lambda>x. if (x = a | P x) \<and> Q x then (f x) else g x)
    = ((\<lambda>x. if P x \<and> Q x then f x else g x) (a := f a))"
  by auto

(* Variation on Tom's, seems to be the missing case we need to bust leading false disjunctions *)
lemma if_fun_conj_not_disj[simp]:
  "\<not> Q a \<Longrightarrow> (\<lambda>x. if (x = a | P x) \<and> Q x then (f x) else g x)
    = ((\<lambda>x. if P x \<and> Q x then f x else g x))"
  "\<not> Q a \<Longrightarrow> (\<lambda>x. if x = a \<and> Q x then (f x) else g x) = g"
  by auto

lemma \<C>_simp[simp]:
  "\<C> = {Var domain}"
  by (auto simp: \<C>_def split: if_splits Var.splits)

lemma type_aexpr_Load:
  "Var v \<notin> dom \<Gamma> \<Longrightarrow> TS.type_aexpr \<Gamma> (ELoad v) (dma_type v)"
  apply(insert TS.type_aexpr[of \<Gamma> "ELoad v", simplified])
  by simp

lemma type_aexpr_Load':
  "Var v \<in> dom \<Gamma> \<Longrightarrow> t = (the (\<Gamma> (Var v))) \<Longrightarrow> TS.type_aexpr \<Gamma> (ELoad v) t"
  apply(insert TS.type_aexpr[of \<Gamma> "ELoad v", simplified])
  by simp

lemma type_aexpr_Const:
  "TS.type_aexpr \<Gamma> (EConst c) {}"
  apply(insert TS.type_aexpr[of \<Gamma> "EConst c", simplified])
  by simp

definition
  prog_Worker :: "(addr, lock_addr, (8 word, addr, bin_op_id) exp, (8 word, addr, bin_op_id) exp) Stmt"
where
  "prog_Worker \<equiv>
     While (EConst 1) (
       Stmt.LockAcq workspace_lock ;;
       While (EOp BEq (ELoad suspended) (EConst 0)) (
         Stmt.LockAcq source_lock ;;
         (workspace \<leftarrow> ELoad source) ;;
         \<comment> \<open> ... operations on workspace ... \<close>
         (workspace \<leftarrow> (EOp BitAnd (ELoad workspace) (EConst LOW_BITMASK))) ;;
         If (EOp BEq (ELoad domain) (EConst DOMAIN_LOW)) (
           (low_sink \<leftarrow> ELoad workspace)
         ) (
           (high_sink \<leftarrow> ELoad workspace) ;;
           (workspace \<leftarrow> EConst 0)
         ) ;;
         Stmt.LockRel source_lock
       ) ;;
       Stmt.LockRel workspace_lock ;;
       While (EOp BNeq (ELoad suspended) (EConst 0)) Skip
     )"

definition
  mds\<^sub>0
where
  "mds\<^sub>0 \<equiv> empty_mds lock_interp"

abbreviation \<Gamma>\<^sub>0
where
  "\<Gamma>\<^sub>0 \<equiv> TS.\<Gamma>_of_mds mds\<^sub>0"

abbreviation \<S>\<^sub>0
where
  "\<S>\<^sub>0 \<equiv> TS.\<S>_of_mds mds\<^sub>0"

definition P\<^sub>0 :: "(8 word, addr, bin_op_id) exp set"
where
  "P\<^sub>0 \<equiv> {}"

declare P\<^sub>0_def[simp]
declare empty_mds_def[simp]

(* Automation *)
method assign_tac =
  solves \<open>rule type_aexpr_Const\<close> |
  solves \<open>rule type_aexpr_Load, (rule order_refl|simp|clarsimp split:Var.splits)+\<close> |
  solves \<open>rule type_aexpr_Load', (rule order_refl|simp|clarsimp split:Var.splits)+\<close> |
  solves \<open>rule TS.type_aexpr\<close>

(* This tactic stops at each If-conditional branch *)
method security_type_tac' =
  rule order_refl |
  (rule TS.seq_type, blast) |
  (rule TS.while_type, blast, blast, simp) |
  solves \<open>(rule TS.skip_type', (force|simp|blast)+)\<close> |
  solves \<open>(rule TS.skip_type)\<close> |
  solves \<open>(rule TS.assign\<^sub>1, simp, clarsimp, simp, assign_tac, (rule order_refl|simp|clarsimp split:Var.splits|fast|force)+)\<close> |
  solves \<open>(rule TS.assign\<^sub>2, simp, clarsimp, assign_tac, (rule order_refl|simp|clarsimp split:Var.splits|fast|force)+)\<close> |
  solves \<open>(rule TS.assign\<^sub>\<C>, simp, clarsimp, assign_tac, (rule order_refl|simp|clarsimp split:Var.splits|fast|force)+)\<close> |
  solves \<open>(rule TS.lock_acq_type, simp+)\<close> |
  solves \<open>(rule TS.lock_rel_type, force, force, force, force, clarsimp+)\<close> |
  simp | clarsimp | solves \<open>auto\<close>

(* This tactic naively tries to blunder through If-conditional branches *)
method security_type_tac =
  (rule TS.skip_type', (force|simp|blast)+) |
  (rule TS.if_type', rule TS.type_bexprI, simp, clarsimp+) |
  security_type_tac'

lemma prog_Worker_typed:
  "TS.has_type \<Gamma>\<^sub>0 \<S>\<^sub>0 P\<^sub>0 prog_Worker \<Gamma>\<^sub>0 \<S>\<^sub>0 P\<^sub>0"
  unfolding TS.\<Gamma>_of_mds_def TS.\<S>_of_mds_def mds\<^sub>0_def
  apply(simp add: prog_Worker_def)
  apply security_type_tac'+
    (* Note importantly we are also throwing away predicates that we don't need
      nor want to be forced to prove hold after the conditionals. *)
    apply(rule_tac \<Gamma>'''="[Var source \<mapsto> {EOp BEq (ELoad domain) (EConst DOMAIN_LOW)},
                          Var workspace \<mapsto> {}]" and
                   P'''= "{EConst 1}" in TS.if_type, rule TS.type_bexprI, simp+)
           apply security_type_tac'+
  apply(rule_tac \<Gamma>\<^sub>1=Map.empty and P\<^sub>1="{}" in TS.sub', force+)
  done

lemma prog_Worker_com_sifum_secure:
  "TS.com_sifum_secure (prog_Worker, mds\<^sub>0)"
  apply(insert prog_Worker_typed)
  apply(rule TS.typed_secure)
   apply(fastforce simp: TS.\<Gamma>_of_mds_def TS.\<S>_of_mds_def)
  unfolding mds\<^sub>0_def
  apply(auto simp: TS.mds_yields_stable_types_def)
  done

lemma no_lock_mds\<^sub>0:
  "no_lock_mds mds\<^sub>0"
  by (clarsimp simp:no_lock_mds_def mds\<^sub>0_def image_def split:Mode.splits)

declare no_lock_mds\<^sub>0[simp]

lemma [simp]:
  "no_lock_mds (lock_acq_upd lock_interp lock mds\<^sub>0)"
  by (clarsimp simp:no_lock_mds_def lock_acq_upd_def mds\<^sub>0_def image_def split:Mode.splits lock_addr.splits)

lemma [simp]:
  "no_lock_mds (lock_rel_upd lock_interp lock (lock_acq_upd lock_interp lock' mds\<^sub>0))"
  by (clarsimp simp:no_lock_mds_def lock_rel_upd_def  lock_acq_upd_def mds\<^sub>0_def image_def split:Mode.splits lock_addr.splits)

lemma mds\<^sub>0_acq_rel:
  "mds\<^sub>0 =
    lock_rel_upd lock_interp lock
     (lock_acq_upd lock_interp lock mds\<^sub>0)"
  unfolding mds\<^sub>0_def
  apply(rule ext)
  apply(clarsimp simp:lock_acq_upd_def lock_rel_upd_def image_def split:Mode.splits lock_addr.splits)
  apply(case_tac x, blast+)
  done

lemma mds\<^sub>0_nested_acq_rel:
  "lock \<noteq> lock' \<Longrightarrow> (lock_acq_upd lock_interp lock' mds\<^sub>0) =
    lock_rel_upd lock_interp lock
     (lock_acq_upd lock_interp lock (lock_acq_upd lock_interp lock' mds\<^sub>0))"
  unfolding mds\<^sub>0_def
  apply(rule ext)
  apply(clarsimp simp:lock_acq_upd_def lock_rel_upd_def image_def split:Mode.splits lock_addr.splits)
  apply(case_tac x, blast+)
  done

method mode_type_tac =
  match conclusion in "TM.mode_type mds\<^sub>0
     (Stmt.LockAcq ?some_lock ;; ?something)
     mds\<^sub>0" \<Rightarrow> "rule TM.seq" |
  rule TM.seq |
  rule TM.while |
  rule TM.skip |
  rule TM.if_ |
  rule TM.assign |
  simp |
  solves \<open>clarsimp simp:to_prog_var_set_def lock_acq_upd_def lock_rel_upd_def\<close> |
  solves \<open>clarsimp simp:to_prog_var_set_def lock_rel_upd_def split:Var.splits\<close> |
  solves \<open>clarsimp simp:to_prog_var_set_def lock_acq_upd_def lock_rel_upd_def mds\<^sub>0_def split:Var.splits lock_addr.splits\<close> |
  solves \<open>force simp:lock_held_mds_correct_def to_prog_var_set_def lock_acq_upd_def lock_rel_upd_def image_def mds\<^sub>0_def split:lock_addr.splits\<close> |
  solves \<open>force simp:lock_acq_upd_def lock_rel_upd_def image_def mds\<^sub>0_def split:lock_addr.splits\<close> |
  solves \<open>(clarsimp simp:no_lock_mds_def lock_rel_upd_def lock_acq_upd_def mds\<^sub>0_def image_def split:Mode.splits lock_addr.splits)\<close> |
  solves \<open>force intro:mds\<^sub>0_acq_rel mds\<^sub>0_nested_acq_rel\<close>

lemma prog_Worker_mode_typed:
  "\<exists>mds. TM.mode_type mds\<^sub>0 prog_Worker mds"
  apply(intro exI)
  apply(simp add: prog_Worker_def)
  apply mode_type_tac+
   apply(rule TM.lock_acq, mode_type_tac+)
    apply(rule TM.lock_acq, mode_type_tac+)
   apply(rule TM.lock_rel, mode_type_tac+)
   apply(rule TM.lock_rel, mode_type_tac+)
  done

lemma prog_Worker_locally_sound_mode_use:
  "TM.locally_sound_mode_use (TM.conf_abv prog_Worker mds\<^sub>0 mem)"
  using prog_Worker_mode_typed TM.mode_type_sound
  by blast

end
