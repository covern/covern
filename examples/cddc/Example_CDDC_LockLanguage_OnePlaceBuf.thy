(*
Author: Robert Sison
*)

theory Example_CDDC_LockLanguage_OnePlaceBuf
imports "../../logic/GloballySoundLockUse" "../../logic/LockTypeSystemSimpl"
  "~~/src/HOL/Word/Word" "~~/src/HOL/Eisbach/Eisbach"
begin

text {*
  Theory for modelling three-component Input, Switch, and Overlay architecture for the CDDC,
  with a one-place buffer between Input and Switch.

  The model in this theory is an *abstraction* of the implementation of the CDDC system prototype
  in the CDDC development repository, in the following respects:

  * Use of hardware APIs is modelled by reads or writes to variables with no assumptions on them
    (i.e. reads from them will return arbitrary values).

  * We model the user's (trusted) decision to enter high or low input based on the UI-indicated
    security domain by using two separate (resp. statically High, Low-rated) input sources.

  * The RPC mechanism is implemented using the locking primitives provided by the LockLanguage,
    and is merely an abstraction of the RPC mechanisms provided by CAmkES on top of seL4.
    As such, it does not leverage any aspects of the seL4 information-flow proofs, but we would
    expect that future, higher-fidelity models of the CDDC implementation may do so.

  * Some of the more mundane (and more security-irrelevant) Switch component functionality is
    pared away in this model, with comment stubs indicating where they would belong if we were
    to model down to that level of detail:
    - compositor cursor shape
    - application of mouse acceleration
    - hardware button events

  The following abstractions are primarily due to the limitations of our verification framework:

  * Due to the limitations of the existing verification framework, it features only 1 High + 1 Low
    security classification domain, as opposed to 4 mutually distrusting domains.

  * Each of the components is flattened into a single function, because of current lack of support
    by the modelling language(s) for function calls.

  * No domain-switching via hotkeys.
    Domain-switching only via mouse-clicks on the overlay buttons or on windows in the UI.

  * Vastly simplified state machine.
    The state machine on the development head uses a table full of function pointers,
    due to current lack of support for function pointers (or functions for that matter).

  The following is the pseudocode for the RPC abstraction we are using in this model of the system,
  implemented solely using LockLanguage's locking primitives:

  Lock-based RPC server model (valid only for one client)
    SERVER
    while (True) {
         lock(L)
             if (L_call) {
                 L_ret := fun(L_arg)
                 L_call := 0
             }
         unlock(L)
    }

  Lock-based RPC client call model (valid only with no other clients)
    CLIENT
    done := 0
    while (!done) {
         lock(L)
             L_arg := my_arg
             L_call := 1
             done := 1
         unlock(L)
    }
    done := 0
    while (!done) {
         lock(L)
             if (!L_call) {
                 my_ret := L_ret
                 done := 1
             }
         unlock(L)
    }

  This theory also includes some experimentation with proof automation support provided by Eisbach.
*}

datatype addr =
    (* HidInputDriver [] HidInputDevice
       These variables model HidInputDevice hardware interfaces, but the distinction
       between the 'high' and 'low' keyboard sources is artificial for the purpose of
       modelling a user basing their input decisions on the currently indicated domain. *)
    hid_mouse_available | hid_keyboard_available |
    hid_mouse_source |
    hid_high_keyboard_source | hid_low_keyboard_source |
    (* HidInputDriver [] Switch *)
    input_event_data | input_event_type |
    (* Switch *)
    active_domain |
    current_domain | current_event_data | current_hid_event_type | cursor_domain |
    switch_state_mouse_down | overlay_result | done_rpc |
    (* Switch [] CompositorDevice
       This variable models the current state of the top-of-screen title/color bar,
       controlled via a CompositorDevice hardware interface, which we will consider
       to be the authoritative domain indicator. *)
    indicated_domain | compositor_cursor_position | compositor_domain_under_cursor |
    (* Switch |> OverlayDriver *)
    rpc_overlay_mouse_click_arg | rpc_overlay_mouse_click_call | rpc_overlay_mouse_click_ret |
    (* Switch [] OutputX *)
    output_event_buffer0 | output_event_buffer1

datatype lock_addr =
    rpc_overlay_mouse_click_lock | input_event_lock | switch_private_lock

type_synonym mem = "((lock_addr, addr) Var, 8 word) Mem"
type_synonym prog_mem = "(addr, 8 word) Mem"
type_synonym lock_mem = "((addr, lock_addr) Var, 8 word) Mem"

datatype aexp = Load "addr" | Const "8 word"

fun
  ev\<^sub>A :: "prog_mem \<Rightarrow> aexp \<Rightarrow> 8 word"
where
  "ev\<^sub>A mem (Load x) = mem x" |
  "ev\<^sub>A mem (Const c) = c"

fun
  aexp_vars :: "aexp \<Rightarrow> addr set"
where
  "aexp_vars (Load x) = {x}" |
  "aexp_vars _ = {}"

primrec
  aexp_to_lexp :: "aexp \<Rightarrow> (addr,8 word) lexp"
where
  "aexp_to_lexp (Load x) = (Var x)" |
  "aexp_to_lexp (Const x) = (lexp.Const x)"

datatype bexp = Same "addr" "addr" |
                NotSame "addr" "addr" |
                Eq "addr" "8 word" |
                Neq "addr" "8 word" |
                Or bexp bexp |
                And bexp bexp |
                TT | FF

primrec
  bexp_to_lpred :: "bexp \<Rightarrow> (addr,8 word) lpred"
where
  "bexp_to_lpred (Same x y) = PCmp (=) (Var x) (Var y)" |
  "bexp_to_lpred (NotSame x y) = PNeg (PCmp (=) (Var x) (Var y))" |
  "bexp_to_lpred (Eq x a) = PCmp (=) (Var x) (lexp.Const a)" |
  "bexp_to_lpred (Neq x a) = PNeg (PCmp (=) (Var x) (lexp.Const a))" |
  "bexp_to_lpred (Or a b) = (PDisj (bexp_to_lpred a) (bexp_to_lpred b))" |
  "bexp_to_lpred (And a b) = (PConj (bexp_to_lpred a) (bexp_to_lpred b))" |
  "bexp_to_lpred TT = PTrue" |
  "bexp_to_lpred FF = PFalse"

lemma aexp_to_lexp_correct':
  "leval mem (aexp_to_lexp e) = ev\<^sub>A mem e"
  apply(induct e, auto)
  done



fun
  ev\<^sub>B :: "prog_mem \<Rightarrow> bexp \<Rightarrow> bool"
where
  "ev\<^sub>B mem (Same x y) = ((mem x) = (mem y))" |
  "ev\<^sub>B mem (NotSame x y) = ((mem x) \<noteq> (mem y))" |
  "ev\<^sub>B mem (Eq x c) = ((mem x) = c)" |
  "ev\<^sub>B mem (Neq x c) = ((mem x) \<noteq> c)" |
  "ev\<^sub>B mem (Or x y) = (ev\<^sub>B mem x \<or> ev\<^sub>B mem y)" |
  "ev\<^sub>B mem (And x y) = (ev\<^sub>B mem x \<and> ev\<^sub>B mem y)" |
  "ev\<^sub>B mem TT = True" |
  "ev\<^sub>B mem FF = False"

lemma bexp_to_lpred_correct':
  "lpred_eval mem (bexp_to_lpred e) = ev\<^sub>B mem e"
  apply(induct e, auto)
  done

lemma aexp_to_lexp_vars':
  "lexp_vars (aexp_to_lexp e) = aexp_vars e"
  apply(induct e, auto)
  done


fun
  bexp_vars :: "bexp \<Rightarrow> addr set"
where
  "bexp_vars (Neq x c) = {x}" |
  "bexp_vars (Eq x c) = {x}" |
  "bexp_vars (Same x y) = {x,y}" |
  "bexp_vars (NotSame x y) = {x,y}" |
  "bexp_vars (Or x y) = bexp_vars x \<union> bexp_vars y" |
  "bexp_vars (And x y) = bexp_vars x \<union> bexp_vars y" |
  "bexp_vars _ = {}"

lemma bexp_to_lpred_vars':
  "lpred_vars (bexp_to_lpred e) = bexp_vars e"
  apply(induct e, auto)
  done


definition NULL_EVENT :: "8 word" where "NULL_EVENT = 0"
definition CDDC_HID_MOUSE_EVENT :: "8 word" where "CDDC_HID_MOUSE_EVENT = 1"
definition CDDC_HID_KEYBOARD_EVENT :: "8 word" where "CDDC_HID_KEYBOARD_EVENT = 2"
definition CDDC_HID_BUTTON_EVENT :: "8 word" where "CDDC_HID_BUTTON_EVENT = 3"

definition EVENT_TYPE_MOUSE_DOWN :: "8 word" where "EVENT_TYPE_MOUSE_DOWN = 42"

definition OVERLAY_POSITION_BUTTON_HIGH :: "8 word" where "OVERLAY_POSITION_BUTTON_HIGH = 121"
definition OVERLAY_POSITION_BUTTON_LOW :: "8 word" where "OVERLAY_POSITION_BUTTON_LOW = 212"
definition OVERLAY_POSITION_REST_OF_OVERLAY :: "8 word" where "OVERLAY_POSITION_REST_OF_OVERLAY = 50"

definition DOMAIN_INVALID :: "8 word" where "DOMAIN_INVALID = -1"
definition DOMAIN_LOW :: "8 word" where "DOMAIN_LOW = 0"
definition DOMAIN_HIGH :: "8 word" where "DOMAIN_HIGH = 1"
definition DOMAIN_OVERLAY :: "8 word" where "DOMAIN_OVERLAY = 2"

definition
  dma_event_data :: "prog_mem \<Rightarrow> addr \<Rightarrow> addr \<Rightarrow> Sec"
where
  (* The only potentially sensitive data is keyboard data when the indicated domain is high *)
  "dma_event_data m type domain \<equiv> if m type = CDDC_HID_KEYBOARD_EVENT \<and> m domain = DOMAIN_HIGH then High
                                   else Low"

definition
  dma :: "mem \<Rightarrow> (lock_addr, addr) Var \<Rightarrow> Sec"
where
  "dma m v \<equiv> case v of
               ProgVar x \<Rightarrow>
                 (if x = current_event_data then dma_event_data (to_prog_mem m) current_hid_event_type current_domain
                  else if x = input_event_data then dma_event_data (to_prog_mem m) input_event_type indicated_domain
                  else if x \<in> {output_event_buffer1, hid_high_keyboard_source} then High
                  else Low) |
               Lock l \<Rightarrow> Low"

definition
  \<C>_vars :: "(lock_addr, addr) Var \<Rightarrow> (lock_addr, addr) Var set"
where
  "\<C>_vars v \<equiv> case v of
                ProgVar x \<Rightarrow>
                  (if x = current_event_data then {ProgVar current_hid_event_type, ProgVar current_domain}
                   else if x = input_event_data then {ProgVar input_event_type, ProgVar indicated_domain}
                   else {}) |
                Lock l \<Rightarrow> {}"

definition
  mds\<^sub>s :: "Mode \<Rightarrow> 'Var set"
where
  "mds\<^sub>s \<equiv> \<lambda>_. {}"

fun
  ev\<^sub>L :: "mem \<Rightarrow> lock_addr \<Rightarrow> bool"
where
  "ev\<^sub>L mem l = ((to_lock_mem mem) l \<noteq> 0)"

definition
  lock_interp :: "lock_addr \<Rightarrow> (addr set \<times> addr set)"
where
  "lock_interp l \<equiv> case l of
       rpc_overlay_mouse_click_lock \<Rightarrow> ({}, {rpc_overlay_mouse_click_arg,
                                            rpc_overlay_mouse_click_call,
                                            rpc_overlay_mouse_click_ret}) |
       input_event_lock \<Rightarrow> ({}, {input_event_type, indicated_domain, active_domain, input_event_data}) |
       (* Dummy lock for Switch component's private variables *)
       switch_private_lock \<Rightarrow> ({}, {current_domain, current_hid_event_type, current_event_data,
           (* Note these Asms on private vars are not essential to the proof, because even if
              corrupted they only affects where mouse events go, and those are always typed Low.
              But include them for now because in the real system, they're private variables. *)
           cursor_domain, switch_state_mouse_down, overlay_result})"


definition
  lock_inv :: "lock_addr \<Rightarrow> (addr, 8 word) lpred"
where
  "lock_inv l \<equiv> case l of input_event_lock \<Rightarrow> PCmp (=) (Var indicated_domain) (Var active_domain) | _ \<Rightarrow> PTrue"


abbreviation
  \<C> :: "(lock_addr, addr) Var set"
where
  "\<C> \<equiv> \<Union>x. \<C>_vars x"

fun
  dma_type :: "addr \<Rightarrow> bexp"
where
  "dma_type output_event_buffer1 = FF" |
  "dma_type hid_high_keyboard_source = FF" |
  "dma_type current_event_data = (Or (Neq current_hid_event_type CDDC_HID_KEYBOARD_EVENT) (Neq current_domain DOMAIN_HIGH))" |
  "dma_type input_event_data = (Or (Neq input_event_type CDDC_HID_KEYBOARD_EVENT) (Neq indicated_domain DOMAIN_HIGH))" |
  "dma_type _ = TT"

definition
  dma_type' :: "(lock_addr, addr) Var \<Rightarrow> bexp"
where
  "dma_type' v = (case v of Lock l \<Rightarrow> TT |
                            ProgVar x \<Rightarrow> dma_type x)"


lemma lock_inv_sat: "\<exists>mem. lpred_eval mem (lock_inv l)"
  apply(case_tac l)
   apply(clarsimp simp: lock_inv_def)+
   apply(rule_tac x="\<lambda>_. undefined" in exI, auto)[1]
  apply(clarsimp simp: lock_inv_def)+
  done

lemma UNIV[simp]:
  "UNIV = ProgVar ` {hid_mouse_available , hid_keyboard_available ,
    hid_mouse_source ,
    hid_high_keyboard_source , hid_low_keyboard_source ,
    input_event_data , input_event_type ,
    active_domain ,
    current_domain , current_event_data , current_hid_event_type , cursor_domain ,
    switch_state_mouse_down , overlay_result , done_rpc ,
    indicated_domain , compositor_cursor_position , compositor_domain_under_cursor ,
    rpc_overlay_mouse_click_arg , rpc_overlay_mouse_click_call , rpc_overlay_mouse_click_ret ,
    output_event_buffer0 , output_event_buffer1} \<union> Lock ` {
    rpc_overlay_mouse_click_lock , input_event_lock , switch_private_lock}"
  apply (clarsimp | safe)+
  apply(rename_tac x, case_tac x, auto)+
  done

lemma UNIV_rewrite:
  "UNIV = (ProgVar ` UNIV \<union> Lock ` UNIV)"
  apply (clarsimp | safe)+
  apply(case_tac x, auto)
  done


lemma finite_Vars:
  "finite {x::(lock_addr,addr) Var. True}"
  apply simp
  done

lemma ev\<^sub>A_det:
  " \<forall>x\<in>aexp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<Longrightarrow> ev\<^sub>A mem\<^sub>1 e = ev\<^sub>A mem\<^sub>2 e"
  apply(induct e, auto)
  done

lemma ev\<^sub>B_det:
  " \<forall>x\<in>bexp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<Longrightarrow> ev\<^sub>B mem\<^sub>1 e = ev\<^sub>B mem\<^sub>2 e"
  apply(induct e, auto)
  done


definition
  lock_acq_sem :: "lock_addr \<Rightarrow> 8 word \<Rightarrow> 8 word"
where
  "lock_acq_sem l v \<equiv> 1"

definition
  lock_rel_sem :: "lock_addr \<Rightarrow> 8 word \<Rightarrow> 8 word"
where
  "lock_rel_sem l v \<equiv> 0"


definition 
  addr_ORDERING :: "addr list"
where
  "addr_ORDERING \<equiv> 
    [hid_mouse_available , hid_keyboard_available ,
    hid_mouse_source ,
    hid_high_keyboard_source , hid_low_keyboard_source ,
    input_event_data , input_event_type ,
    active_domain ,
    current_domain , current_event_data , current_hid_event_type , cursor_domain ,
    switch_state_mouse_down , overlay_result , done_rpc ,
    indicated_domain , compositor_cursor_position , compositor_domain_under_cursor ,
    rpc_overlay_mouse_click_arg , rpc_overlay_mouse_click_call , rpc_overlay_mouse_click_ret ,
    output_event_buffer0 , output_event_buffer1]"


primrec 
  index_of :: "'a \<Rightarrow> 'a list \<Rightarrow> nat option"
where
  "index_of x [] = None" |
  "index_of x (y#ys) = (if x = y then Some 0 else map_option Suc (index_of x ys))"

lemma addr_ORDERING_complete:
  "\<exists>i. index_of a addr_ORDERING = Some i"
  apply(case_tac a, (auto simp: addr_ORDERING_def))
  done


lemma nat_less_cases:
  "(a::nat) < b \<or> b < a \<or> a = b"
  using nat_neq_iff by blast

lemma index_of_inject:
  "index_of a xs = Some i \<Longrightarrow> index_of b xs = Some i \<Longrightarrow> a = b"
  by (induct xs arbitrary: i, auto split: if_splits)


instantiation addr :: linorder
begin

definition
  less_eq_addr :: "addr \<Rightarrow> addr \<Rightarrow> bool"
where
  "less_eq_addr a b \<equiv> a = b \<or> the (index_of a addr_ORDERING) < the (index_of b addr_ORDERING)"

definition
  less_addr :: "addr \<Rightarrow> addr \<Rightarrow> bool"
where
  "less_addr a b \<equiv> the (index_of a addr_ORDERING) < the (index_of b addr_ORDERING)"


instance 
  apply(intro_classes)
      apply(clarsimp simp: less_addr_def less_eq_addr_def | safe)+
  using addr_ORDERING_complete index_of_inject
  by (metis option.sel)

end

lemma ev\<^sub>L_det:
  "mem (Lock l) = mem' (Lock l) \<Longrightarrow> ev\<^sub>L mem l  = ev\<^sub>L mem' l"
  apply(auto simp: to_lock_mem_def)
  done

definition
  myINIT :: "_ \<Rightarrow> bool"
where
  "myINIT mem \<equiv> (\<forall>l. \<not> ev\<^sub>L mem l) \<and> (mem (ProgVar indicated_domain) = mem (ProgVar active_domain))"


interpretation sifum_types myINIT ev\<^sub>A ev\<^sub>B ev\<^sub>L aexp_vars bexp_vars 
                            lock_interp lock_inv lock_acq_sem lock_rel_sem 
                            aexp_to_lexp bexp_to_lpred dma \<C>_vars \<C> dma_type
  apply(unfold_locales)
  apply(rule lock_inv_sat)
  using ev\<^sub>A_det ev\<^sub>B_det apply auto[3]
  using ev\<^sub>L_det apply blast
  apply (auto  simp: lock_inv_def dma_event_data_def  split: Var.splits simp: dma_def to_prog_mem_def  \<C>_vars_def to_lock_mem_def lock_acq_sem_def lock_rel_sem_def lock_interp_def aexp_to_lexp_correct' bexp_to_lpred_correct' aexp_to_lexp_vars' bexp_to_lpred_vars' split: lock_addr.splits)[9]
          apply(auto simp: \<C>_vars_def split: Var.splits simp: dma_def dma_event_data_def)[1]  
         apply(auto simp: \<C>_vars_def split: Var.splits simp: dma_def dma_event_data_def to_prog_mem_def)[2]  
       apply(case_tac x, auto simp:dma_def to_prog_mem_def dma_type'_def dma_event_data_def)[1]
      apply(case_tac x, auto simp: \<C>_vars_def to_prog_mem_def dma_type'_def)[1]
     apply(force simp: \<C>_vars_def to_prog_mem_def dma_type'_def)
    apply(force simp:dma_def)
   apply(force simp:\<C>_vars_def lock_interp_def split:if_splits lock_addr.splits)
  by auto

declare Collect_conj_eq [simp]
declare Collect_disj_eq [simp]
declare subtype_def [simp]
declare dma_type'_def [simp]
declare pred_entailment_def [simp]
declare pred_def [simp]
declare to_prog_mem_def [simp]
declare add_anno_def [simp]
declare restrict_map_def [simp]
declare add_anno_dom_def [simp]
declare lock_interp_def [simp]
declare add_anno_stable_def [simp]
declare context_equiv_def [simp]
declare type_equiv_def [simp]
declare tyenv_wellformed_def [simp]
declare env_consistent_def [simp]
declare types_wellformed_def [simp]
declare types_stable_def [simp]
declare type_wellformed_def [simp]

declare add_pred_def [simp]
declare stable_def [simp]
declare to_total_def [simp]

declare anno_type_stable_def [simp]
declare assign_post_def [simp]

declare \<C>_vars_def [simp]
declare \<C>_vars\<^sub>V_def [simp]
declare \<C>\<^sub>V_def [simp]

declare stable_NoW_def [simp]
declare stable_NoRW_def [simp]
declare lock_inv_def [simp]

declare aexp_readable_def [simp]
declare bexp_readable_def [simp]
declare var_readable_def [simp]

(* The following lemmas, if_fun_(disj|not_conj) make it possible for the simplifier
  to reduce the typing environments to the user-friendly maplet form armed only with
  the definitions for restrict_preds_to_vars, etc. *)

lemma if_fun_disj[simp]:
  "(\<lambda>x. if x = a | P x then (f x) else g x)
    = ((\<lambda>x. if P x then (f x) else g x)(a := f a))"
  "(\<lambda>x. if x = a then (f x) else g x)
    = (g(a := f a))"
  by auto

lemma if_fun_not_conj[simp]:
  "(\<lambda>x. if x \<noteq> a \<and> P x then (f x) else g x)
    = ((\<lambda>x. if x = a \<or> \<not> P x then (g x) else f x))"
  "(\<lambda>x. if x \<noteq> a then (f x) else g x)
    = ((\<lambda>x. if x = a then (g x) else f x))"
  by auto

lemma if_fun_conj[simp]:
  "(\<lambda>x. if x = a \<and> P x then (f x) else g x)
    = ((\<lambda>x. if x \<noteq> a \<or> \<not> P x then (g x) else f x))"
  "(\<lambda>x. if x = a then (f x) else g x)
    = ((\<lambda>x. if x \<noteq> a then (g x) else f x))"
  by auto

lemma \<C>_simp[simp]:
  "\<C> = {ProgVar current_hid_event_type, ProgVar input_event_type, ProgVar indicated_domain, ProgVar current_domain}"
  by (auto simp: \<C>_def split: if_splits Var.splits)

lemma type_aexpr_Load:
  "v \<notin> dom \<Gamma> \<Longrightarrow> type_aexpr \<Gamma> (Load v) (PConj (bexp_to_lpred (dma_type v)) PTrue)"
  apply(insert type_aexpr[of \<Gamma> "Load v", simplified])
  by auto

lemma type_aexpr_Load':
  "v \<in> dom \<Gamma> \<Longrightarrow> t = (the (\<Gamma> v)) \<Longrightarrow> type_aexpr \<Gamma> (Load v) (PConj t PTrue)"
  apply(insert type_aexpr[of \<Gamma> "Load v", simplified])
  by simp

lemma type_aexpr_Const:
  "type_aexpr \<Gamma> (Const c) PTrue"
  apply(insert type_aexpr[of \<Gamma> "Const c", simplified])
  by simp

definition
  prog_OverlayDriver :: "(addr, lock_addr, aexp, bexp) Stmt"
where
  "prog_OverlayDriver \<equiv>
     (* Lock-based RPC server model, for one client - see top of theory. *)
     While (TT) (
       LockAcq rpc_overlay_mouse_click_lock ;;
       If (Neq rpc_overlay_mouse_click_call 0) (
         (* if (point_in_button(&buttons[0], click_x, click_y)) *)
         If (Eq rpc_overlay_mouse_click_arg OVERLAY_POSITION_BUTTON_LOW) (
           rpc_overlay_mouse_click_ret \<leftarrow> Const DOMAIN_LOW
         ) (
           (* if (point_in_button(&buttons[1], click_x, click_y)) *)
           If (Eq rpc_overlay_mouse_click_arg OVERLAY_POSITION_BUTTON_HIGH) (
             rpc_overlay_mouse_click_ret \<leftarrow> Const DOMAIN_HIGH
           ) (
             (* if (click_y < 50) *)
             If (Eq rpc_overlay_mouse_click_arg OVERLAY_POSITION_REST_OF_OVERLAY) (
               rpc_overlay_mouse_click_ret \<leftarrow> Const DOMAIN_OVERLAY
             ) (
               (* Otherwise, the mouse is not in the overlay at all,
                 i.e. it's in the composited window-populated region of the display *)
               rpc_overlay_mouse_click_ret \<leftarrow> Const DOMAIN_INVALID
             )
           )
         ) ;;
         (rpc_overlay_mouse_click_call \<leftarrow> Const 0)
       ) Skip ;;
       LockRel rpc_overlay_mouse_click_lock
     )"

lemma rpc_overlay_mouse_click_ret_mod [simp]:
  "var_modifiable {rpc_overlay_mouse_click_lock} rpc_overlay_mouse_click_ret"
  apply(clarsimp simp:var_modifiable_def)
  apply(rule conjI)
   using lock_addr.exhaust apply force
  using lock_addr.exhaust by force

lemma rpc_overlay_mouse_click_call_mod [simp]:
  "rpc_overlay_mouse_click_lock \<in> ls \<Longrightarrow> var_modifiable ls rpc_overlay_mouse_click_call"
  apply(clarsimp simp:var_modifiable_def)
  apply(subgoal_tac "l = rpc_overlay_mouse_click_lock \<or> l = input_event_lock \<or> l = switch_private_lock")
   prefer 2
   apply(meson lock_addr.exhaust)
  by force

lemma var_modifiableI [intro]:
  "v \<in> fst (lock_interp l) \<union> snd (lock_interp l) \<Longrightarrow> l \<in> ls \<Longrightarrow> var_modifiable ls v"
  apply(clarsimp simp:var_modifiable_def split:lock_addr.splits)
    apply blast
   apply blast
  apply blast
  done

method has_type_simpl_pre = (rule has_type_simpl_pre, solves \<open>(rule pred_simpl.intros)+\<close>)+

method has_type_context_simpl_pre = (rule has_type_context_simpl_pre, solves \<open>(rule context_simpl.intros)+\<close>)+

method has_type_context_simpl_post = (rule has_type_context_simpl_post, solves \<open>(rule context_simpl.intros)+\<close>)+


lemma prog_OverlayDriver_typed:
  "\<exists>\<Gamma>' \<S>' P'. has_type Map.empty {} PTrue prog_OverlayDriver \<Gamma>' \<S>' P'"
  apply(intro exI)
  apply(simp add: prog_OverlayDriver_def)
  apply(rule while_type', blast, simp+)
  apply(rule seq_type)
   apply(rule lock_acq_type, simp+)
  apply(rule seq_type)
   apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
     apply(has_type_simpl_pre)
     apply(rule seq_type)
      apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
        apply(rule assign\<^sub>2, simp, clarsimp, rule type_aexpr_Const, simp+)
       apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
         apply(rule assign\<^sub>2, simp, clarsimp, rule type_aexpr_Const, simp+)
        apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
          apply(rule assign\<^sub>2, simp, clarsimp, rule type_aexpr_Const, simp+)
         apply(rule assign\<^sub>2, simp, clarsimp, rule type_aexpr_Const, simp+)
        apply(clarsimp split: lock_addr.splits)
       apply(clarsimp split: lock_addr.splits)
      apply(clarsimp split: lock_addr.splits)
     apply(rule assign\<^sub>2, simp, clarsimp, rule type_aexpr_Const, simp+)
    apply(rule skip_type')
      apply force
     apply simp
    apply simp
   apply(clarsimp split: lock_addr.splits)  
  apply(rule has_type_strengthen_post)
    apply(rule lock_rel_type, simp+)
  done

(* Automation *)
method assign_tac =
  solves \<open>rule type_aexpr_Const\<close> |
  solves \<open>rule type_aexpr_Load, (rule order_refl|simp|clarsimp split:Var.splits)+\<close> |
  solves \<open>rule type_aexpr_Load', (rule order_refl|simp|clarsimp split:Var.splits)+\<close>

(* Note: This inner tactic's use of order_refl means that when choosing how to modify
  the typing environment's predicate set, it will default to retaining as much as possible.
  Also, this tactic refrains from attempting to type If-conditional commands, and should be
  used directly when we know we will need to intervene by using the full if_type rule. *)
method security_type_tac' =
  rule order_refl |
  rule seq_type |
  (rule while_type', blast, (solves \<open>(simp split:lock_addr.splits)\<close>)+) |
  solves \<open>(rule skip_type', (force|simp split:lock_addr.splits|blast)+)\<close> |
  solves \<open>(rule skip_type)\<close> |
  solves \<open>(rule assign\<^sub>1, simp, force, simp, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close> |
  solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close> |
  solves \<open>(rule assign\<^sub>2', simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close> |
  solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close> |
  solves \<open>(rule lock_acq_type, (simp split:lock_addr.splits|clarsimp)+)\<close> |
  (* The "force" make this tactic a little more robust to changes in ordering and duplication of
    type environment mappings & predicate set elements when fine-tuning using the subtyping rule,
    which is something we've found ourselves doing typically right before a lock release. *)
  solves \<open>(rule lock_rel_type, (simp split:lock_addr.splits|clarsimp|force)+)\<close> |
  solves \<open>simp split:lock_addr.splits\<close> | clarsimp

(* Note: This outer tactic's use of empty_subsetI means that when choosing how to modify
  the typing environment's predicate set, it will default to dropping everything.
  This takes precedence over the order_refl in the inner tactic. *)
method security_type_tac =
  rule empty_subsetI |
  (rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+) |
  security_type_tac' |
  (rule has_type_strengthen_post, security_type_tac')

lemma prog_OverlayDriver_typed':
  "\<exists>\<Gamma>' \<S>' P'. has_type Map.empty {} PTrue prog_OverlayDriver \<Gamma>' \<S>' P'"
  apply(intro exI)
  unfolding prog_OverlayDriver_def
  apply security_type_tac+
  done

lemma prog_OverlayDriver_com_sifum_secure:
  "com_sifum_secure (prog_OverlayDriver, env\<^sub>0)"
  apply(insert prog_OverlayDriver_typed)
  apply clarify
  apply(rule typed_secure)
   apply fastforce
  by (clarsimp simp: myINIT_def global_invariant_def split: lock_addr.splits)

definition
  prog_HidInputDriver :: "(addr, lock_addr, aexp, bexp) Stmt"
where
  "prog_HidInputDriver \<equiv>
     While (TT) (
       (* Supposing the input_event_type is just the LSB of one actual input_event variable,
          we consider each of the pairs of assignments to input_event_* as one atomic block,
          both with respect to each other and to any changes to the value of indicated_domain. *)
       If (Neq hid_mouse_available 0) (
         (* Not modelled: Application of mouse acceleration would happen here. *)
         LockAcq input_event_lock ;;
         (* Hacky clearance so the type assignment is safe. *)
         (input_event_data \<leftarrow> Const 0) ;;
         (input_event_type \<leftarrow> Const CDDC_HID_MOUSE_EVENT) ;;
         (input_event_data \<leftarrow> Load hid_mouse_source) ;;
         LockRel input_event_lock
       ) Skip ;;
       If (Neq hid_keyboard_available 0) (
         LockAcq input_event_lock ;;
         (* Hacky clearance so the type assignment is safe. *)
         (input_event_data \<leftarrow> Const 0) ;;
         (input_event_type \<leftarrow> Const CDDC_HID_KEYBOARD_EVENT) ;;
         (* This conditional models the user's decision to type high or low input
            based on the domain currently indicated by the compositor *)
         If (Eq indicated_domain DOMAIN_HIGH) (
           input_event_data \<leftarrow> Load hid_high_keyboard_source
         ) (
           input_event_data \<leftarrow> Load hid_low_keyboard_source
         ) ;;
         LockRel input_event_lock
       ) Skip
       (* Not modelled: Handling of hardware button presses would go here. *)
     )"

lemma prog_HidInputDriver_typed:
  "\<exists>\<Gamma>' \<S>' P'. has_type Map.empty {} PTrue prog_HidInputDriver \<Gamma>' \<S>' P'"
  apply(intro exI)
  unfolding prog_HidInputDriver_def
  apply(rule while_type', blast, simp+)
  apply(rule seq_type)
   apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
     apply(rule seq_type, rule lock_acq_type, simp+)
     apply(rule seq_type)
      apply(rule assign\<^sub>2, simp, force, rule type_aexpr_Const, simp+)
     apply(rule seq_type)
      apply(rule assign\<^sub>\<C>, simp, force, rule type_aexpr_Const, (simp|clarsimp split:Var.splits)+)
     apply(rule seq_type)
      apply(rule assign\<^sub>2, simp, force, rule type_aexpr_Load, (simp split:lock_addr.splits)+)
     apply(rule lock_rel_type, simp+)
    apply(force intro:skip_type')
   apply(clarsimp simp add: bexp_readable_def split: lock_addr.splits)
  apply(rule has_type_strengthen_post)
    apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
      apply(rule seq_type, rule lock_acq_type, simp+)
      apply(rule seq_type)
       apply(rule assign\<^sub>2, simp, force, rule type_aexpr_Const, simp+)
      apply(rule seq_type)
       apply(rule assign\<^sub>\<C>, simp, force, rule type_aexpr_Const, (simp|clarsimp split:Var.splits)+)
      apply(rule seq_type)
       apply(rule_tac \<Gamma>'''="[active_domain \<mapsto> PTrue,
          input_event_data \<mapsto> (PDisj (PNeg (PCmp (=) (Var indicated_domain) (lexp.Const DOMAIN_HIGH))) (PNeg (PCmp (=) (Var input_event_type) (lexp.Const CDDC_HID_KEYBOARD_EVENT))))]" and
          P'''= "PConj (PCmp (=) (Var input_event_type) (lexp.Const CDDC_HID_KEYBOARD_EVENT)) (PCmp (=) (Var indicated_domain) (Var active_domain))" and
          \<S>'="{input_event_lock}"
          in if_type)
                  apply(rule type_bexprI, simp, clarsimp, rule HOL.refl)
               apply(rule assign\<^sub>2', simp, force, rule type_aexpr_Load, (simp split:lock_addr.splits)+)
              apply(rule assign\<^sub>2', simp, force, rule type_aexpr_Load, (simp split:lock_addr.splits|clarsimp)+)
      apply(rule lock_rel_type, (simp|clarsimp)+)
     apply(rule skip_type', simp+)
    apply(clarsimp split: lock_addr.splits)+
  done

lemma prog_HidInputDriver_typed':
  "\<exists>\<Gamma>' \<S>' P'. has_type Map.empty {} PTrue prog_HidInputDriver \<Gamma>' \<S>' P'"
  apply(intro exI)
  unfolding prog_HidInputDriver_def
  apply security_type_tac'
  apply security_type_tac'
   apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
     apply security_type_tac'+
  apply(rule has_type_strengthen_post)
    apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
      apply security_type_tac'+
       apply(rule_tac \<Gamma>'''="[active_domain \<mapsto> PTrue,
          input_event_data \<mapsto> (PDisj (PNeg (PCmp (=) (Var indicated_domain) (lexp.Const DOMAIN_HIGH))) (PNeg (PCmp (=) (Var input_event_type) (lexp.Const CDDC_HID_KEYBOARD_EVENT))))]" and
          P'''= "PConj (PCmp (=) (Var input_event_type) (lexp.Const CDDC_HID_KEYBOARD_EVENT)) (PCmp (=) (Var indicated_domain) (Var active_domain))" and
          \<S>'="{input_event_lock}"
          in if_type, fastforce)
              apply security_type_tac'+
  done

lemma prog_HidInputDriver_com_sifum_secure:
  "com_sifum_secure (prog_HidInputDriver, env\<^sub>0)"
  apply(insert prog_HidInputDriver_typed)
  apply clarify
  apply(rule typed_secure)
   apply fastforce
  apply(clarsimp simp: myINIT_def global_invariant_def split: lock_addr.splits)
  done

definition
  prog_Switch :: "(addr, lock_addr, aexp, bexp) Stmt"
where
  "prog_Switch \<equiv>
     (* Permanently grab this dummy lock *)
     LockAcq switch_private_lock ;;

     (* Initialisation *)
     LockAcq input_event_lock ;;
     (* Temporarily zero input_event_data so init-syncing indicated_domain with active_domain
        cannot inadvertently leak whatever High data might be in it. *)
     (input_event_data \<leftarrow> Const 0) ;;
     (indicated_domain \<leftarrow> Load active_domain) ;;
     LockRel input_event_lock ;;
     (current_event_data \<leftarrow> Const 0) ;;
     (current_domain \<leftarrow> Const DOMAIN_INVALID) ;;
     (current_hid_event_type \<leftarrow> Const NULL_EVENT) ;;

     While (TT) (
       (* Not modelled: Updating the compositor cursor shape would happen here. *)

       LockAcq input_event_lock ;;
       (current_domain \<leftarrow> Load active_domain) ;;
       (current_hid_event_type \<leftarrow> Load input_event_type) ;;
       (current_event_data \<leftarrow> Load input_event_data) ;;
       LockRel input_event_lock ;;

       (* Pass through one mouse event, if available.
          If it's a click on the overlay button or window of a non-top domain, switch to it. *)
       If (Eq current_hid_event_type CDDC_HID_MOUSE_EVENT) (

         (* A model of the decision based on the mouse event data on whether we should
            switch domain. In this model, only mouse-clicks can cause a domain switch.
            Furthermore, mouse events are considered a Low-only rated control channel. *)

         (* Lock-based RPC client call model with no other clients - see top of theory. *)
         (done_rpc \<leftarrow> Const 0) ;;
         While (Neq done_rpc 0) (
           LockAcq rpc_overlay_mouse_click_lock ;;
           (rpc_overlay_mouse_click_arg \<leftarrow> Load current_event_data) ;;
           (rpc_overlay_mouse_click_call \<leftarrow> Const 1) ;;
           (done_rpc \<leftarrow> Const 1) ;;
           LockRel rpc_overlay_mouse_click_lock
         ) ;;
         (done_rpc \<leftarrow> Const 0) ;;
         While (Neq done_rpc 0) (
           LockAcq rpc_overlay_mouse_click_lock ;;
           If (Neq rpc_overlay_mouse_click_call 0) (
             (overlay_result \<leftarrow> Load rpc_overlay_mouse_click_ret) ;;
             (done_rpc \<leftarrow> Const 1)
           ) Skip ;;
           LockRel rpc_overlay_mouse_click_lock
         ) ;;

         If (Neq overlay_result DOMAIN_INVALID) (
           cursor_domain \<leftarrow> Const DOMAIN_OVERLAY
         ) (
           (* Approximate use of these compositor hardware library APIs:
           COMPOSITOR_SET_CURSOR_POSITION(mouse_event_get_position(current_event_data)) ;;
           cursor_domain = COMPOSITOR_GET_TOP_DOMAIN_UNDER_CURSOR() ;; *)
           (compositor_cursor_position \<leftarrow> Load current_event_data) ;;
           (cursor_domain \<leftarrow> Load compositor_domain_under_cursor) ;;
           If (Eq cursor_domain DOMAIN_INVALID) (
             cursor_domain \<leftarrow> Load current_domain
           ) Skip
         ) ;;

         If (Eq cursor_domain DOMAIN_OVERLAY) (
           (* If we're hovering above an overlay button, and we click on it, switch to that domain *)
           If (And (Neq overlay_result DOMAIN_OVERLAY)
                   (And (Eq switch_state_mouse_down 0)
                        (And (Eq current_event_data EVENT_TYPE_MOUSE_DOWN)
                             (And (Neq overlay_result DOMAIN_INVALID)
                                  (NotSame overlay_result current_domain))))) (
             LockAcq input_event_lock ;;
             (current_domain \<leftarrow> Load overlay_result) ;;
             (active_domain \<leftarrow> Load current_domain) ;;
             (* To ensure safe update, any pending event in the buffer is flushed *)
             (input_event_data \<leftarrow> Const 0) ;;
             (input_event_type \<leftarrow> Const NULL_EVENT) ;;
             (* Approximate use of cddc_compositor_bubble_domain_to_top API *)
             (indicated_domain \<leftarrow> Load active_domain) ;;
             LockRel input_event_lock
           ) Skip
         ) (
           (* If we're hovering above a window, and we click on it, switch to that domain *)
           If (And (Eq switch_state_mouse_down 0)
                   (And (Eq current_event_data EVENT_TYPE_MOUSE_DOWN)
                        (NotSame current_domain cursor_domain))) (
             LockAcq input_event_lock ;;
             (current_domain \<leftarrow> Load cursor_domain) ;;
             (active_domain \<leftarrow> Load current_domain) ;;
             (* To ensure safe update, any pending event in the buffer is flushed *)
             (input_event_data \<leftarrow> Const 0) ;;
             (input_event_type \<leftarrow> Const NULL_EVENT) ;;
             (* Approximate use of cddc_compositor_bubble_domain_to_top API *)
             (indicated_domain \<leftarrow> Load active_domain) ;;
             LockRel input_event_lock
           ) Skip ;;

           (* Send hovering mouse events to cursor_domain (not active_domain).
              Note that in the current CDDC, the cursor_domain is non-persistent state. *)
           If (Or (Neq switch_state_mouse_down 0) (Eq current_event_data EVENT_TYPE_MOUSE_DOWN)) (
             If (Eq current_domain DOMAIN_LOW) (
               output_event_buffer0 \<leftarrow> Load current_event_data
             ) (
               output_event_buffer1 \<leftarrow> Load current_event_data
             )
           ) (
             If (Eq cursor_domain DOMAIN_LOW) (
               output_event_buffer0 \<leftarrow> Load current_event_data
             ) (
               output_event_buffer1 \<leftarrow> Load current_event_data
             )
           )
         ) ;;

         If (Eq current_event_data EVENT_TYPE_MOUSE_DOWN) (
           switch_state_mouse_down \<leftarrow> Const 1
         ) (
           switch_state_mouse_down \<leftarrow> Const 0
         )
       ) Skip ;;

       (* Pass through one keyboard event, if available *)
       If (Eq current_hid_event_type CDDC_HID_KEYBOARD_EVENT) (
         (* Send keyboard events to the current_domain *)
         If (Eq current_domain DOMAIN_LOW) (
           output_event_buffer0 \<leftarrow> Load current_event_data
         ) (
           output_event_buffer1 \<leftarrow> Load current_event_data
         )
       ) Skip ;;

       (current_event_data \<leftarrow> Const 0) ;;
       (current_domain \<leftarrow> Const DOMAIN_INVALID) ;;
       (current_hid_event_type \<leftarrow> Const NULL_EVENT)
     )"

(* Note "{a, b, c}" is syntax for "insert a (insert b (insert c {}))" *)
thm "Set.insert_Diff_if"
(* ^ This is the rule to use for simplifying the messed-up \<Gamma>s! *)

declare Set.insert_Diff_if[simp]

lemma done_rpc_unprotected [simp]:
  "unprotected done_rpc"
  by (clarsimp simp:unprotected_def split:lock_addr.splits)

lemma compositor_cursor_position_unprotected [simp]:
  "unprotected compositor_cursor_position"
  by (clarsimp simp:unprotected_def split:lock_addr.splits)

lemma output_event_buffer0_unprotected [simp]:
  "unprotected output_event_buffer0"
  by (clarsimp simp:unprotected_def split:lock_addr.splits)

lemma output_event_buffer1_unprotected [simp]:
  "unprotected output_event_buffer1"
  by (clarsimp simp:unprotected_def split:lock_addr.splits)

lemma skip_type'':
  assumes wf: "\<forall>env. tyenv_wellformed env \<Gamma> \<S>' P' = tyenv_wellformed env \<Gamma>' \<S>' P'"
  assumes equiv: "context_equiv \<Gamma> P \<Gamma>'"
  shows "\<lbrakk>\<S> = \<S>'; P = P'\<rbrakk> \<Longrightarrow> has_type \<Gamma> \<S> P Skip \<Gamma>' \<S>' P'"
  apply(simp, rule_tac \<Gamma>\<^sub>1=\<Gamma> and P\<^sub>1=P' in sub, rule skip_type, simp)
      using wf equiv apply (blast intro: wf pred_entailment_refl)+
  done     

declare combine_preds_def [simp]
(* Workspace for type system automation.
  Note that the inner tactic stops at each If-statement to prompt the user to confirm
  the pre and post-environment coming out of both branches.
  The user can use the if_type' helper if they merely want the intersection of each branch's
  predicates, and must use the full if_type rule if they want to customise the post-set.*)
lemma prog_Switch_typed':
  "\<exists>\<Gamma>' \<S>' P'. has_type Map.empty {} PTrue prog_Switch \<Gamma>' \<S>' P'"
  using NULL_EVENT_def CDDC_HID_MOUSE_EVENT_def CDDC_HID_KEYBOARD_EVENT_def CDDC_HID_BUTTON_EVENT_def
        DOMAIN_LOW_def DOMAIN_HIGH_def EVENT_TYPE_MOUSE_DOWN_def
  apply(intro exI)
  unfolding prog_Switch_def
  (* We have to count these off manually so we can apply the subtype rule at the right moment *)
  apply security_type_tac'
   apply security_type_tac'
  apply security_type_tac'
   apply security_type_tac'
  apply security_type_tac'
   apply security_type_tac'
  apply security_type_tac'
   apply security_type_tac'
  apply security_type_tac'
   (* We want "Same indicated_domain active_domain" as an invariant of input_event_type_lock.
      This reflects that we *know* that nobody else is writing to (indicated|active)_domain. *)
   apply security_type_tac'
  apply security_type_tac'
   apply security_type_tac'
  apply security_type_tac'
   apply security_type_tac'
  apply security_type_tac'
   apply security_type_tac'
  apply security_type_tac'
  apply security_type_tac'
   apply security_type_tac'
  apply security_type_tac'
   apply security_type_tac'
  apply security_type_tac'
   apply security_type_tac'
  apply security_type_tac'
   apply security_type_tac'
  (* Before we release the locks, we have to re-establish the type of the data
    in the local vars in terms of the local copies of the control vars *)
  apply(rule_tac
    \<Gamma>\<^sub>1="[input_event_data \<mapsto>
    (PDisj (PNeg (PCmp (=) (Var input_event_type) (lexp.Const CDDC_HID_KEYBOARD_EVENT))) 
           (PNeg (PCmp (=) (Var indicated_domain) (lexp.Const 1)))),
    active_domain \<mapsto> PTrue, overlay_result \<mapsto> PTrue, switch_state_mouse_down \<mapsto> PTrue,
    cursor_domain \<mapsto> PTrue, current_event_data \<mapsto>
    (PDisj (PNeg (PCmp (=) (Var current_hid_event_type) (lexp.Const CDDC_HID_KEYBOARD_EVENT))) (PNeg (PCmp (=) (Var current_domain) (lexp.Const 1))))]" and
    P\<^sub>1="(PConj (PConj (PConj (PConj (PCmp (=) (Var indicated_domain) (Var active_domain)) 
                             (PCmp (=) (Var current_domain) (Var active_domain)))
                             (PCmp (=) (Var indicated_domain) (Var current_domain)))
                             (PCmp (=) (Var current_hid_event_type) (Var input_event_type)))
                             (PCmp (=) (Var current_event_data) (Var input_event_data)))" and
    (* Leave the current post-type untouched *)
    \<Gamma>\<^sub>1'="[overlay_result \<mapsto> PTrue, switch_state_mouse_down \<mapsto> PTrue, cursor_domain \<mapsto> PTrue,
    current_event_data \<mapsto> PTrue]" and
    P\<^sub>1'="(PConj (PConj (PCmp (=) (Var current_event_data) (lexp.Const 0))
                       (PCmp (=) (Var current_domain) (lexp.Const DOMAIN_INVALID)))
                       (PCmp (=) (Var current_hid_event_type) (lexp.Const 0)))"
    in sub')
        apply force
       apply force
      apply force
     apply force
    apply force
   apply force
  apply security_type_tac'+
   apply(rule_tac \<Gamma>'''="[overlay_result \<mapsto> PTrue, switch_state_mouse_down \<mapsto> PTrue, 
                         cursor_domain \<mapsto> PTrue,
          current_event_data \<mapsto>
          (PDisj (PNeg (PCmp (=) (Var current_hid_event_type) (lexp.Const CDDC_HID_KEYBOARD_EVENT))) 
                 (PNeg (PCmp (=) (Var current_domain) (lexp.Const 1))))]" and
          P'''= "PTrue"
          in if_type)
             apply(rule type_bexprI, simp+)
           (* Infer that current_event_data\<mapsto>{} because current_hid_event_type=CDDC_HID_MOUSE_EVENT.
              This is necessary as we will soon be assigning to its CV current_domain.
              Doing this as early as possible in case it can be useful as soon as possible. *)
           apply(rule_tac
             \<Gamma>\<^sub>1="[overlay_result \<mapsto> PTrue, switch_state_mouse_down \<mapsto> PTrue, cursor_domain \<mapsto> PTrue,
               current_event_data \<mapsto> PTrue]" and
             P\<^sub>1="(PCmp (=) (Var current_hid_event_type) (lexp.Const 1))"
             in sub')
                 apply force
                apply force
               apply force
              apply force
             apply force
            apply force
           apply security_type_tac'+
            apply (has_type_simpl_pre) (* works nicely! *)
            (* method has_type_context_simpl_pre doesn't work here for some reason so do manually *)
            apply (rule has_type_context_simpl_pre)
             apply (rule context_simpl.intros(2))
              apply(rule HOL.refl)
             apply(rule context_simpl.intros(1))
              apply(rule HOL.refl)
             apply(solves\<open>(rule pred_simpl.intros)+\<close>)
            (* security_type_tac' getting stuck on lockrel *)
            apply(rule has_type_strengthen_post)
            apply(rule has_type.lock_rel_type)
                   apply simp+
           apply security_type_tac'+
             apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
               apply security_type_tac'+
            (* method has_type_context_simpl_post doesn't work so do manually *)
            apply (rule has_type_context_simpl_post)
            apply (solves\<open>rule context_simpl.intros, (simp | (rule pred_simpl.intros))+\<close>)
               apply security_type_tac'+ 
            apply (rule has_type_context_simpl_pre)
            apply (solves\<open>rule context_simpl.intros, (simp | (rule pred_simpl.intros))+\<close>)
            apply(rule has_type_strengthen_post)
              apply(rule has_type.lock_rel_type)
                     apply simp
                    (* why do we need such nonsense? *)
                    apply(rule ext, rename_tac x, case_tac x, auto)[1]
                   apply simp+
           apply security_type_tac'+
            apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
              apply security_type_tac'+
             apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
               apply(has_type_simpl_pre)
               apply (rule has_type_context_simpl_pre)
               apply (solves\<open>rule context_simpl.intros, (simp | (rule pred_simpl.intros))+\<close>)
               apply (rule assign\<^sub>2'_simpl, simp)
                     apply(simp add: var_modifiable_def split: lock_addr.splits)
                    apply(assign_tac)
                   apply simp+
                 apply(solves\<open>rule pred_simpl.intros\<close>)
                apply simp
               apply(clarsimp split: lock_addr.splits)
              apply (rule has_type_context_simpl_pre)
              apply (solves\<open>rule context_simpl.intros, (simp | (rule pred_simpl.intros))+\<close>)
             apply simp
             apply(has_type_simpl_pre)

              apply security_type_tac'+
            apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
              apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
                apply security_type_tac'+
              apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
                apply security_type_tac'+
             apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
               apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
                 apply security_type_tac'+
              apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
                apply security_type_tac'+
           
           apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
             apply security_type_tac'+  (* SLOW *)
         apply (fastforce) (* SLOW -- lots slow from here on *)
        apply security_type_tac'+
     apply fastforce
    apply fastforce
   apply(clarsimp split: lock_addr.splits)
  apply security_type_tac'+
   apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
     apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
       apply security_type_tac'+
  (* simpl_pre seems to work well here *)
  apply(has_type_simpl_pre)
  apply(rule has_type_strengthen_post)
    apply security_type_tac'+
  done


(* NB: This time, we solved it using the automation first, and then I figured out
  the manual proof by filling in the details of the automated proof. *)
lemma prog_Switch_typed:
  "\<exists>\<Gamma>' \<S>' P'. has_type Map.empty {} PTrue prog_Switch \<Gamma>' \<S>' P'"
  using NULL_EVENT_def CDDC_HID_MOUSE_EVENT_def CDDC_HID_KEYBOARD_EVENT_def CDDC_HID_BUTTON_EVENT_def
        DOMAIN_LOW_def DOMAIN_HIGH_def EVENT_TYPE_MOUSE_DOWN_def
  apply(intro exI)
  unfolding prog_Switch_def
  apply(rule seq_type, rule lock_acq_type, (simp split:lock_addr.splits|clarsimp)+)
  apply(rule seq_type, rule lock_acq_type, (simp split:lock_addr.splits|clarsimp)+)
  apply(rule seq_type, solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
  apply(rule seq_type, solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
  apply(rule seq_type, rule lock_rel_type, (simp split:lock_addr.splits|clarsimp)+)
  apply(rule seq_type, solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
  apply(rule seq_type, solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
  apply(rule seq_type, solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
  apply(rule while_type', blast, (solves \<open>(simp split:lock_addr.splits)\<close>)+)
  apply(rule seq_type, rule lock_acq_type, (simp split:lock_addr.splits|clarsimp)+)
  apply(rule seq_type, solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
  apply(rule seq_type, solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
  apply(rule seq_type, solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
  (* Before we release the locks, we have to re-establish the type of the data
    in the local vars in terms of the local copies of the control vars *)
  apply(rule_tac
    \<Gamma>\<^sub>1="[input_event_data \<mapsto>
    (PDisj (PNeg (PCmp (=) (Var input_event_type) (lexp.Const CDDC_HID_KEYBOARD_EVENT))) 
           (PNeg (PCmp (=) (Var indicated_domain) (lexp.Const 1)))),
    active_domain \<mapsto> PTrue, overlay_result \<mapsto> PTrue, switch_state_mouse_down \<mapsto> PTrue,
    cursor_domain \<mapsto> PTrue, current_event_data \<mapsto>
    (PDisj (PNeg (PCmp (=) (Var current_hid_event_type) (lexp.Const CDDC_HID_KEYBOARD_EVENT))) (PNeg (PCmp (=) (Var current_domain) (lexp.Const 1))))]" and
    P\<^sub>1="(PConj (PConj (PConj (PConj (PCmp (=) (Var indicated_domain) (Var active_domain)) 
                             (PCmp (=) (Var current_domain) (Var active_domain)))
                             (PCmp (=) (Var indicated_domain) (Var current_domain)))
                             (PCmp (=) (Var current_hid_event_type) (Var input_event_type)))
                             (PCmp (=) (Var current_event_data) (Var input_event_data)))" and
    (* Leave the current post-type untouched *)
    \<Gamma>\<^sub>1'="[overlay_result \<mapsto> PTrue, switch_state_mouse_down \<mapsto> PTrue, cursor_domain \<mapsto> PTrue,
    current_event_data \<mapsto> PTrue]" and
    P\<^sub>1'="(PConj (PConj (PCmp (=) (Var current_event_data) (lexp.Const 0))
                       (PCmp (=) (Var current_domain) (lexp.Const DOMAIN_INVALID)))
                       (PCmp (=) (Var current_hid_event_type) (lexp.Const 0)))"
    in sub')
        apply force
       apply force
      apply force
     apply force
    apply force
   apply force
  apply(rule seq_type, rule lock_rel_type, (simp split:lock_addr.splits|clarsimp)+)
  (* Pass through one mouse event, if available *)
  apply(rule seq_type)
   apply(rule_tac \<Gamma>'''="[overlay_result \<mapsto> PTrue, switch_state_mouse_down \<mapsto> PTrue, 
                         cursor_domain \<mapsto> PTrue,
          current_event_data \<mapsto>
          (PDisj (PNeg (PCmp (=) (Var current_hid_event_type) (lexp.Const CDDC_HID_KEYBOARD_EVENT))) 
                 (PNeg (PCmp (=) (Var current_domain) (lexp.Const 1))))]" and
          P'''= "PTrue"
          in if_type)
             apply(rule type_bexprI, simp+)
           (* Infer that current_event_data\<mapsto>{} because current_hid_event_type=CDDC_HID_MOUSE_EVENT.
              This is necessary as we will soon be assigning to its CV current_domain.
              Doing this as early as possible in case it can be useful as soon as possible. *)
           apply(rule_tac
             \<Gamma>\<^sub>1="[overlay_result \<mapsto> PTrue, switch_state_mouse_down \<mapsto> PTrue, cursor_domain \<mapsto> PTrue,
               current_event_data \<mapsto> PTrue]" and
             P\<^sub>1="(PCmp (=) (Var current_hid_event_type) (lexp.Const 1))"
             in sub')
                 apply force
                apply force
               apply force
              apply force
             apply force
            apply force
           (* RPC call *)
           apply(rule seq_type, solves \<open>(rule assign\<^sub>1, simp, force, simp, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
           apply(rule seq_type)
            apply(rule while_type', blast, (solves \<open>(simp split:lock_addr.splits)\<close>)+)
            apply(rule seq_type, rule lock_acq_type, (simp split:lock_addr.splits|clarsimp)+)
            apply(rule seq_type, solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
            apply(rule seq_type, solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
            apply(rule seq_type, solves \<open>(rule assign\<^sub>1, simp, force, simp, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
            apply(rule has_type_strengthen_post)
              apply(rule lock_rel_type, (simp split:lock_addr.splits|clarsimp)+)
           apply(rule seq_type, solves \<open>(rule assign\<^sub>1, simp, force, simp, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
           apply(rule seq_type, rule while_type', blast, (solves \<open>(simp split:lock_addr.splits)\<close>)+)
            apply(rule seq_type, rule lock_acq_type, (simp split:lock_addr.splits|clarsimp)+)
            apply(rule seq_type)
             apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
                apply(rule seq_type, solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
                apply(solves \<open>(rule assign\<^sub>1, simp, force, simp, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
               apply(solves \<open>(rule skip_type'', (force|simp split:lock_addr.splits|blast)+)\<close>)
              apply(clarsimp split: lock_addr.splits)
             apply(rule has_type_context_simpl_pre)
              apply(solves\<open>(rule context_simpl.intros | rule pred_simpl.intros | simp)+\<close>)+
             apply(rule has_type_strengthen_post)
             apply(rule lock_rel_type, (fastforce|(simp split:lock_addr.splits|clarsimp))+)
            apply(rule seq_type)
            apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
              apply(solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
             apply(rule seq_type, solves \<open>(rule assign\<^sub>1, simp, force, simp, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
             apply(rule seq_type, solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
             apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
             apply(rule has_type_context_simpl_pre)
              apply(solves\<open>(rule context_simpl.intros | rule pred_simpl.intros | simp)+\<close>)+
             apply(has_type_simpl_pre)
               apply(rule assign\<^sub>2'_simpl, simp, force, assign_tac, clarsimp, (fastforce intro: pred_simpl.intros split: lock_addr.splits)+)
              apply(rule has_type_context_simpl_pre)
               apply(solves\<open>(rule context_simpl.intros | rule pred_simpl.intros | simp)+\<close>)+
              apply(rule skip_type)
             apply(fastforce split: lock_addr.splits)
            apply(fastforce split: lock_addr.splits)
           apply(rule seq_type)
            apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
              apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
                apply(rule seq_type, rule lock_acq_type, (simp split:lock_addr.splits|clarsimp)+)
                apply(rule seq_type, solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
                apply(rule seq_type, solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
                apply(rule seq_type, solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
                apply(rule seq_type, solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
                apply(rule seq_type, solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
                apply(rule lock_rel_type, (simp split:lock_addr.splits|clarsimp)+)
               apply(solves \<open>(rule skip_type', (force|simp split:lock_addr.splits|blast)+)\<close>)
              apply(fastforce split: lock_addr.splits)
            apply(rule seq_type)
              apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
                apply(rule seq_type, rule lock_acq_type, (simp split:lock_addr.splits|clarsimp)+)
                apply(rule seq_type, solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
                apply(rule seq_type, solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
                apply(rule seq_type, solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
                apply(rule seq_type, solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
                apply(rule seq_type, solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
                apply(rule lock_rel_type, (simp split:lock_addr.splits|clarsimp)+)
               apply(solves \<open>(rule skip_type', (force|simp split:lock_addr.splits|blast)+)\<close>)
              apply(fastforce split: lock_addr.splits)
             apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
               apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
                 apply(solves \<open>(rule assign\<^sub>1, simp, force, simp, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
                apply(solves \<open>(rule assign\<^sub>1, simp, force, simp, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
               apply(fastforce split: lock_addr.splits)
            apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
                apply(solves \<open>(rule assign\<^sub>1, simp, force, simp, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
               apply(solves \<open>(rule assign\<^sub>1, simp, force, simp, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
              apply(fastforce split: lock_addr.splits)
             apply(fastforce split: lock_addr.splits)
            apply(fastforce split: lock_addr.splits)
           apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
             apply(solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
            apply(solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
           apply(fastforce split: lock_addr.splits)
         apply(rule skip_type)
         apply fastforce
        apply force
       apply force
      apply force
     apply force
    apply fastforce
   apply(force split:lock_addr.splits)
  (* Pass through one keyboard event, if available *)
  apply(rule seq_type)
   apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
     apply(rule if_type', rule type_bexprI, (simp split:lock_addr.splits)+)
       apply(solves \<open>(rule assign\<^sub>1, simp, force, simp, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
      apply(solves \<open>(rule assign\<^sub>1, simp, force, simp, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
     apply(fastforce split: lock_addr.splits)
    apply(rule skip_type)
   apply(fastforce split: lock_addr.splits)
  apply(rule seq_type, solves \<open>(rule assign\<^sub>2, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
  apply(rule seq_type, solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
  apply(rule has_type_strengthen_post)
    apply(solves \<open>(rule assign\<^sub>\<C>, simp, force, assign_tac, (rule order_refl|simp split:lock_addr.splits|clarsimp split:Var.splits|fast|force)+)\<close>)
   by fastforce+


lemma prog_Switch_com_sifum_secure:
  "com_sifum_secure (prog_Switch, env\<^sub>0)"
  apply(insert prog_Switch_typed')
  apply clarify
  apply(rule typed_secure)
   apply fastforce
  by (clarsimp simp:myINIT_def global_invariant_def split: lock_addr.splits)

(* Parallel composition of the CDDC components *)

theorem CDDC_sifum_secure_cont:
  "prog_sifum_secure_cont [(prog_OverlayDriver, env\<^sub>0), (prog_HidInputDriver, env\<^sub>0), (prog_Switch, env\<^sub>0)]"
  apply(rule typed_prog_sifum_secure_cont)
  apply(rule typed.intros)
   using prog_OverlayDriver_typed prog_HidInputDriver_typed prog_Switch_typed env\<^sub>0_def
   apply (metis (mono_tags, lifting) case_prodI list_all_simps(1) list_all_simps(2))
  by (clarsimp simp:myINIT_def global_invariant_def split: lock_addr.splits)

end
