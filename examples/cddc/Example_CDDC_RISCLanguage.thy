(*
Author: Robert Sison
*)

theory Example_CDDC_RISCLanguage
imports Example_CDDC_WhileLockLanguage
begin

(* ('Reg, 'Wrd) Mem *)
definition regs\<^sub>0 :: "(reg, 8 word) Mem"
where
  "regs\<^sub>0 \<equiv> \<lambda>_. 0"

abbreviation comp_outputs_OverlayDriver
where
  "comp_outputs_OverlayDriver \<equiv>
   let (PCs, l', nl', C', fail) = RC_compile_cmd RC_C\<^sub>0 None 0 prog_OverlayDriver
      in (PCs, l', nl', C', regs\<^sub>0)"

abbreviation comp_outputs_InputSwitch
where
  "comp_outputs_InputSwitch \<equiv>
   let (PCs, l', nl', C', fail) = RC_compile_cmd RC_C\<^sub>0 None 0 prog_InputSwitch
      in (PCs, l', nl', C', regs\<^sub>0)"

definition prog_OverlayDriver_RISC
where
  "prog_OverlayDriver_RISC \<equiv> map fst (fst comp_outputs_OverlayDriver)"

definition prog_OverlayDriver_Cs
where
  "prog_OverlayDriver_Cs \<equiv> map snd (fst comp_outputs_OverlayDriver)"

definition prog_InputSwitch_RISC
where
  "prog_InputSwitch_RISC \<equiv> map fst (fst comp_outputs_InputSwitch)"

definition prog_InputSwitch_Cs
where
  "prog_InputSwitch_Cs \<equiv> map snd (fst comp_outputs_InputSwitch)"

abbreviation comp_outputs
where
  "comp_outputs \<equiv> [comp_outputs_OverlayDriver, comp_outputs_InputSwitch]"

definition init_cms_CDDC_RISC
where
  "init_cms_CDDC_RISC \<equiv> [(((0, prog_OverlayDriver_RISC), regs\<^sub>0), RC.mds\<^sub>0),
                          (((0, prog_InputSwitch_RISC), regs\<^sub>0), RC.mds\<^sub>0)]"

abbreviation comp_inputs
where
   "comp_inputs \<equiv> [(RC_C\<^sub>0, None, 0), (RC_C\<^sub>0, None, 0)]"

thm RC.whole_system_compilation_secure
  RC.whole_system_compilation_secure[simplified]
  RC.compile_cmd_simple_ev\<^sub>B_eq_secure

lemma prog_OverlayDriver_input_reqs:
  "RC.compile_cmd_input_reqs RC_C\<^sub>0 None 0 prog_OverlayDriver"
  unfolding RC.compile_cmd_input_reqs_def RC.C\<^sub>0_def prog_OverlayDriver_def RC.no_unstable_exprs_def
  by (force simp:RC.regrec_stable_def RC.asmrec_expr_stable'_def RC.asmrec_var_stable'_def)

lemma prog_OverlayDriver_compile_succeeded:
  "let (PCs, l', nl', C', fail) = RC_compile_cmd RC_C\<^sub>0 None 0 prog_OverlayDriver
   in \<not> fail"
  by eval

method unstable_exprs_debug_tac =
  rule conjI |
  solves \<open>clarsimp simp:RC.asmrec_var_stable'_def\<close>

lemma prog_InputSwitch_input_reqs:
  "RC.compile_cmd_input_reqs RC.C\<^sub>0 None 0 prog_InputSwitch"
  unfolding RC.compile_cmd_input_reqs_def
  apply clarsimp
  apply(rule conjI)
   apply(force simp:prog_InputSwitch_def)
  apply(rule conjI)
   unfolding prog_InputSwitch_def RC.C\<^sub>0_def RC.no_unstable_exprs_def
   (* Long-running *)
   apply clarsimp
   apply(clarsimp simp:RC.lock_governed_def RC.asmrec_var_stable'_def split:lock_addr.splits)
   apply(clarsimp simp:RC.asmrec_expr_stable'_def)
   apply unstable_exprs_debug_tac+
  by (force simp:RC.regrec_stable_def)

lemma prog_InputSwitch_compile_succeeded:
  "let (PCs, l', nl', C', fail) = RC_compile_cmd RC_C\<^sub>0 None 0 prog_InputSwitch
   in \<not> fail"
  by eval

(* Whole-system secure compilation theorem for this CDDC model, using Rob's
  whole_system_compilation_secure lemma proved in RISCRefinementSafe *)

theorem
  "RC.RL.prog_sifum_secure_cont init_cms_CDDC_RISC"
  unfolding init_cms_CDDC_RISC_def
  apply(rule_tac comp_inputs=comp_inputs and comp_outputs=comp_outputs and cms=init_cms_CDDC
        in RC.whole_system_compilation_secure[simplified, rotated -1])
      unfolding prog_OverlayDriver_RISC_def prog_InputSwitch_RISC_def init_cms_CDDC_def
      apply(force simp:Let_def split:prod.splits)
     apply(force simp:RL.no_locks_acquired_def WL.no_locks_acquired_def)
    apply force
   apply force
  apply(clarsimp simp:Let_def split:prod.splits)
  apply(rule conjI)
   using CDDC_sound_mode_use unfolding RC.WL.sound_mode_use_def init_cms_CDDC_def
   apply force
  apply(rule conjI)
   using prog_OverlayDriver_com_sifum_secure apply force
  apply(rule conjI)
   using prog_OverlayDriver_input_reqs apply blast
  apply(rule conjI)
   using prog_OverlayDriver_compile_succeeded apply force
  apply(rule conjI)
   apply clarsimp
   using CDDC_sound_mode_use unfolding RC.WL.sound_mode_use_def init_cms_CDDC_def
   apply force
  apply(rule conjI)
   using prog_InputSwitch_com_sifum_secure apply force
  apply(rule conjI)
   using prog_InputSwitch_input_reqs apply blast
  using prog_InputSwitch_compile_succeeded by force

(* Per-thread secure compilation theorems for this CDDC model's components *)

theorem
  "RC.RL.com_sifum_secure (((0, prog_OverlayDriver_RISC), regs\<^sub>0), RC.mds\<^sub>0)"
  apply(rule_tac l=None and nl=0 and comp_output=comp_outputs_OverlayDriver
        in RC.per_thread_compilation_secure[simplified])
     apply(force simp:RL.no_locks_acquired_def WL.no_locks_acquired_def)
    using prog_OverlayDriver_com_sifum_secure apply blast
   unfolding prog_OverlayDriver_RISC_def
   using prog_OverlayDriver_compile_succeeded apply force
  using prog_OverlayDriver_input_reqs by blast

theorem
  "RC.RL.com_sifum_secure (((0, prog_InputSwitch_RISC), regs\<^sub>0), RC.mds\<^sub>0)"
  apply(rule_tac l=None and nl=0 and comp_output=comp_outputs_InputSwitch
        in RC.per_thread_compilation_secure[simplified])
     apply(force simp:RL.no_locks_acquired_def WL.no_locks_acquired_def)
    using prog_InputSwitch_com_sifum_secure apply blast
   unfolding prog_InputSwitch_RISC_def
   using prog_InputSwitch_compile_succeeded apply force
  using prog_InputSwitch_input_reqs by blast

(* Compiled program texts obtained using Isabelle codegen execution of the compiler *)

(* For pretty-printing
declare One_nat_def [simp del]
declare One_nat_def [symmetric, simp add]
*)

value [code] "prog_OverlayDriver_RISC"

lemma
  "prog_OverlayDriver_RISC =
   [(Some 0, MoveK r0 1),
    (None, Jz 1 r0),
    (None, InstB.LockAcq rpc_overlay_mouse_click_lock),
    (None, Load r1 rpc_overlay_mouse_click_call),
    (None, MoveK r2 0),
    (None, Op BNeq r1 r2),
    (None, Jz 2 r1),
    (None, Load r3 rpc_overlay_mouse_click_arg),
    (None, MoveK r4 121),
    (None, Op BEq r3 r4),
    (None, Jz 4 r3),
    (None, MoveK r5 0),
    (None, Store rpc_overlay_mouse_click_ret r5),
    (None, Jmp 5),
    (Some 4, Load r5 rpc_overlay_mouse_click_arg),
    (None, MoveK r6 212),
    (None, Op BEq r5 r6),
    (None, Jz 6 r5),
    (None, MoveK r7 1),
    (None, Store rpc_overlay_mouse_click_ret r7),
    (None, Jmp 7),
    (Some 6, Load r7 rpc_overlay_mouse_click_arg),
    (None, MoveK r8 50),
    (None, Op BEq r7 r8),
    (None, Jz 8 r7),
    (None, MoveK r9 2),
    (None, Store rpc_overlay_mouse_click_ret r9),
    (None, Jmp 9),
    (Some 8, MoveK r9 255),
    (None, Store rpc_overlay_mouse_click_ret r9),
    (None, Tau),
    (Some 9, Tau),
    (Some 7, Tau),
    (Some 5, MoveK r5 0),
    (None, Store rpc_overlay_mouse_click_call r5),
    (None, Jmp 3),
    (Some 2, Nop),
    (None, Tau),
    (Some 3, InstB.LockRel rpc_overlay_mouse_click_lock),
    (None, Jmp 0)]"
  by eval

value [code] "prog_InputSwitch_RISC"

lemma
  "prog_InputSwitch_RISC =
   [(None, InstB.LockAcq switch_private_lock),
    (None, MoveK r0 0),
    (None, Store current_event_data r0),
    (None, Load r1 active_domain),
    (None, Store indicated_domain r1),
    (None, MoveK r2 0),
    (None, Store current_hid_event_type r2),
    (Some 0, MoveK r0 1),
    (None, Jz 1 r0),
    (None, InstB.LockAcq hid_read_atomicity_lock),
    (None, Load r1 hid_mouse_available),
    (None, Store temp r1),
    (None, InstB.LockRel hid_read_atomicity_lock),
    (None, MoveK r2 0),
    (None, Op BNeq r1 r2),
    (None, Jz 2 r1),
    (None, MoveK r3 1),
    (None, Store current_hid_event_type r3),
    (None, InstB.LockAcq hid_read_atomicity_lock),
    (None, Load r4 hid_mouse_source),
    (None, Store current_event_data r4),
    (None, InstB.LockRel hid_read_atomicity_lock),
    (None, InstB.LockAcq rpc_overlay_mouse_click_lock),
    (None, Store rpc_overlay_mouse_click_arg r4),
    (None, MoveK r5 1),
    (None, Store rpc_overlay_mouse_click_call r5),
    (None, InstB.LockRel rpc_overlay_mouse_click_lock),
    (None, MoveK r4 0),
    (None, Store done_rpc r4),
    (Some 4, Load r0 done_rpc),
    (None, MoveK r1 0),
    (None, Op BEq r0 r1),
    (None, Jz 5 r0),
    (None, InstB.LockAcq rpc_overlay_mouse_click_lock),
    (None, Load r2 rpc_overlay_mouse_click_call),
    (None, MoveK r3 0),
    (None, Op BNeq r2 r3),
    (None, Jz 6 r2),
    (None, Load r4 rpc_overlay_mouse_click_ret),
    (None, Store overlay_result r4),
    (None, MoveK r5 1),
    (None, Store done_rpc r5),
    (None, Jmp 7),
    (Some 6, Nop),
    (None, Tau),
    (Some 7, InstB.LockRel rpc_overlay_mouse_click_lock),
    (None, Jmp 4),
    (Some 5, Load r0 overlay_result),
    (None, MoveK r1 255),
    (None, Op BNeq r0 r1),
    (None, Jz 8 r0),
    (None, MoveK r2 2),
    (None, Store cursor_domain r2),
    (None, Jmp 9),
    (Some 8, Load r2 current_event_data),
    (None, Store compositor_cursor_position r2),
    (None, InstB.LockAcq compositor_read_atomicity_lock),
    (None, Load r3 compositor_domain_under_cursor),
    (None, Store cursor_domain r3),
    (None, InstB.LockRel compositor_read_atomicity_lock),
    (None, MoveK r4 255),
    (None, Op BEq r3 r4),
    (None, Jz 10 r3),
    (None, Load r5 active_domain),
    (None, Store cursor_domain r5),
    (None, Jmp 11),
    (Some 10, Nop),
    (None, Tau),
    (Some 11, Tau),
    (Some 9, Load r2 cursor_domain),
    (None, MoveK r3 2),
    (None, Op BEq r2 r3),
    (None, Jz 12 r2),
    (None, Load r4 overlay_result),
    (None, MoveK r5 2),
    (None, Op BNeq r4 r5),
    (None, Load r6 switch_state_mouse_down),
    (None, MoveK r7 0),
    (None, Op BEq r6 r7),
    (None, Load r8 current_event_data),
    (None, MoveK r9 42),
    (None, Op BEq r8 r9),
    (None, Load r10 overlay_result),
    (None, MoveK r11 255),
    (None, Op BNeq r10 r11),
    (None, Load r12 overlay_result),
    (None, Load r13 active_domain),
    (None, Op BNeq r12 r13),
    (None, Op BAnd r10 r12),
    (None, Op BAnd r8 r10),
    (None, Op BAnd r6 r8),
    (None, Op BAnd r4 r6),
    (None, Jz 14 r4),
    (None, Load r14 overlay_result),
    (None, Store active_domain r14),
    (None, Store indicated_domain r14),
    (None, Jmp 15),
    (Some 14, Nop),
    (None, Tau),
    (Some 15, Jmp 13),
    (Some 12, Load r4 switch_state_mouse_down),
    (None, MoveK r5 0),
    (None, Op BEq r4 r5),
    (None, Load r6 current_event_data),
    (None, MoveK r7 42),
    (None, Op BEq r6 r7),
    (None, Load r8 active_domain),
    (None, Load r9 cursor_domain),
    (None, Op BNeq r8 r9),
    (None, Op BAnd r6 r8),
    (None, Op BAnd r4 r6),
    (None, Jz 16 r4),
    (None, Store active_domain r9),
    (None, Store indicated_domain r9),
    (None, Jmp 17),
    (Some 16, Nop),
    (None, Tau),
    (Some 17, Load r4 switch_state_mouse_down),
    (None, MoveK r6 0),
    (None, Op BNeq r4 r6),
    (None, Load r8 current_event_data),
    (None, MoveK r9 42),
    (None, Op BEq r8 r9),
    (None, Op BOr r4 r8),
    (None, Jz 18 r4),
    (None, Load r10 active_domain),
    (None, MoveK r11 0),
    (None, Op BEq r10 r11),
    (None, Jz 20 r10),
    (None, Load r12 current_event_data),
    (None, Store output_event_buffer0 r12),
    (None, Jmp 21),
    (Some 20, Load r12 current_event_data),
    (None, Store output_event_buffer1 r12),
    (None, Tau),
    (Some 21, Jmp 19),
    (Some 18, Load r10 cursor_domain),
    (None, MoveK r11 0),
    (None, Op BEq r10 r11),
    (None, Jz 22 r10),
    (None, Load r12 current_event_data),
    (None, Store output_event_buffer0 r12),
    (None, Jmp 23),
    (Some 22, Load r12 current_event_data),
    (None, Store output_event_buffer1 r12),
    (None, Tau),
    (Some 23, Tau),
    (Some 19, Tau),
    (Some 13, Load r4 current_event_data),
    (None, MoveK r5 42),
    (None, Op BEq r4 r5),
    (None, Jz 24 r4),
    (None, MoveK r6 1),
    (None, Store switch_state_mouse_down r6),
    (None, Jmp 25),
    (Some 24, MoveK r6 0),
    (None, Store switch_state_mouse_down r6),
    (None, Tau),
    (Some 25, Jmp 3),
    (Some 2, Nop),
    (None, Tau),
    (Some 3, InstB.LockAcq hid_read_atomicity_lock),
    (None, Load r0 hid_keyboard_available),
    (None, Store temp r0),
    (None, InstB.LockRel hid_read_atomicity_lock),
    (None, MoveK r1 0),
    (None, Op BNeq r0 r1),
    (None, Jz 26 r0),
    (None, MoveK r2 0),
    (None, Store current_event_data r2),
    (None, MoveK r3 2),
    (None, Store current_hid_event_type r3),
    (None, Load r4 indicated_domain),
    (None, MoveK r5 1),
    (None, Op BEq r4 r5),
    (None, Jz 28 r4),
    (None, InstB.LockAcq hid_read_atomicity_lock),
    (None, Load r6 hid_high_keyboard_source),
    (None, Store current_event_data r6),
    (None, InstB.LockRel hid_read_atomicity_lock),
    (None, Jmp 29),
    (Some 28, InstB.LockAcq hid_read_atomicity_lock),
    (None, Load r6 hid_low_keyboard_source),
    (None, Store current_event_data r6),
    (None, InstB.LockRel hid_read_atomicity_lock),
    (None, Tau),
    (Some 29, Load r2 active_domain),
    (None, MoveK r7 0),
    (None, Op BEq r2 r7),
    (None, Jz 30 r2),
    (None, Store output_event_buffer0 r6),
    (None, Jmp 31),
    (Some 30, Store output_event_buffer1 r6),
    (None, Tau),
    (Some 31, Jmp 27),
    (Some 26, Nop),
    (None, Tau),
    (Some 27, MoveK r2 0),
    (None, Store current_event_data r2),
    (None, MoveK r3 0),
    (None, Store current_hid_event_type r3),
    (None, Jmp 0)]"
  by eval

(* Explore alternative way to prove whole-system secure compilation theorem for this CDDC model
  using Toby's system refinement locale *)

(* A "cms" according to sifum_refinement_sys is a list of these:
  record ('a, 'Val, 'Var\<^sub>C, 'Com\<^sub>C, 'Var\<^sub>A, 'Com\<^sub>A) componentwise_refinement =
    priv_mem :: "'Var\<^sub>C set" (* private variables *)
    \<R>\<^sub>A_rel :: "('Com\<^sub>A, 'Var\<^sub>A, 'Val) LocalConf rel" (* abstract bisimulation *)
    \<R>_rel :: "('Com\<^sub>A, 'Var\<^sub>A, 'Val, 'Com\<^sub>C, 'Var\<^sub>C) state_relation" (* refinement relation *)
    P_rel :: "('Com\<^sub>C, 'Var\<^sub>C, 'Val) LocalConf rel"
  To disambiguate from the "cms" I refer to in RISCRefinementSafe, I will use "crs" here.
*)
definition my_crs
where
  "my_crs \<equiv> [
     \<lparr> priv_mem = {},
       \<R>\<^sub>A_rel = TS.\<R> \<Gamma>\<^sub>0 \<S>\<^sub>0 P\<^sub>0,
       \<R>_rel = RC.\<R>,
       P_rel = RC.\<I> \<rparr>,
     \<lparr> priv_mem = {},
       \<R>\<^sub>A_rel = TS.\<R> \<Gamma>'_InputSwitch \<S>'_InputSwitch P'_InputSwitch,
       \<R>_rel = RC.\<R>,
       P_rel = RC.\<I> \<rparr>
     ]"

(* Wishlist: a function that returns the \<B> of a typecheck, and these facts about that \<B>.
    TM.strong_low_bisim_mm ?\<B> \<Longrightarrow>
    RC.WL.bisim_simple_ev\<^sub>B_eq ?\<B>
  The typecheck currently requires user intervention, so I'm not sure we can just write
  a function that does this.
  But that might be nice to think about for future work. *)

(* TODO: push these up? *)
lemma \<I>_sym':
  "(\<langle>(c, regs), mds, mem\<rangle>\<^sub>C, \<langle>(c', regs'), mds', mem'\<rangle>\<^sub>C) \<in> RC.\<I> \<Longrightarrow>
   (\<langle>(c', regs'), mds', mem'\<rangle>\<^sub>C, \<langle>(c, regs), mds, mem\<rangle>\<^sub>C) \<in> RC.\<I>"
  apply(induct rule:RC.\<I>.induct)
  using RC.\<I>.\<I>_default by blast

lemma \<I>_sym: "sym RC.\<I>"
  unfolding sym_def
  using \<I>_sym' by force

interpretation
  SRSI: sifum_refinement_sys_init dma dma \<C>_vars \<C>_vars \<C> \<C> WL.eval\<^sub>w RL.eval\<^sub>r undefined WL.no_locks_acquired RL.no_locks_acquired WL.bisim_simple_ev\<^sub>B_eq id my_crs "[{}, {}]"
  apply(unfold_locales)
          apply(force simp:my_crs_def nth_Cons')
         apply(clarsimp simp:my_crs_def)
         apply(rule set_eqI)
         apply(solves \<open>(rename_tac x, case_tac x, clarsimp, rename_tac y, case_tac y, force+)+\<close>)
        apply fastforce
       apply(clarsimp simp:my_crs_def)
       apply(case_tac i)
        apply(force simp:RC.secure_refinement_\<R>_simple_ev\<^sub>B_eq_\<B> TS.\<R>_bisim TS.bisim_simple_ev\<^sub>B_eq_\<R>\<^sub>u)
       apply(force simp:RC.secure_refinement_\<R>_simple_ev\<^sub>B_eq_\<B> TS.\<R>_bisim TS.bisim_simple_ev\<^sub>B_eq_\<R>\<^sub>u)
      apply(clarsimp simp:my_crs_def)
      apply(case_tac i)
       apply(force simp:RC.preserves_local_guarantee_compliance_\<R>)
      apply(force simp:RC.preserves_local_guarantee_compliance_\<R>)
     apply(clarsimp simp:my_crs_def)
     apply(case_tac i)
      apply(force simp:TS.\<R>_bisim)
     apply(force simp:TS.\<R>_bisim)
    apply(clarsimp simp:my_crs_def)
    apply(case_tac i)
     apply(force simp:\<I>_sym)
    apply(force simp:\<I>_sym)
   apply(clarsimp simp:my_crs_def)
   apply(case_tac i)
    apply(force simp:RC.\<I>.\<I>_default)
   apply(force simp:RC.\<I>.\<I>_default)
  by (force simp:my_crs_def)

(* Whole-system secure compilation theorem for this CDDC model, using Toby's
  whole-system locale and refined_prog_secure lemma *)

theorem
  "RC.RL.prog_sifum_secure_cont init_cms_CDDC_RISC"
  apply(rule SRSI.refined_prog_secure_init[where cms\<^sub>A=init_cms_CDDC])
        apply(force simp:RL.no_locks_acquired_def RC.WL.no_locks_acquired_def RC.mem\<^sub>A_of_def)
       apply(force simp:init_cms_CDDC_RISC_def my_crs_def)
      apply(force simp:init_cms_CDDC_def my_crs_def)
     apply(clarsimp simp:init_cms_CDDC_def init_cms_CDDC_RISC_def my_crs_def)
     apply(case_tac i)
      (* OverlayDriver *)
      apply(clarsimp simp:prog_OverlayDriver_RISC_def RC.mem\<^sub>A_of_def split:prod.splits)
      apply(rule RC.compile_cmd_correctness_\<R>'[where C=RC_C\<^sub>0 and l=None and nl=0 and Cs=prog_OverlayDriver_Cs])
         apply clarsimp
         apply(rule conjI)
          apply(force simp:prog_OverlayDriver_Cs_def zip_map_fst_snd)
         apply(rule conjI)
          apply force
         apply(rule conjI)
          apply force
         apply(rule conjI)
          apply force
         using prog_OverlayDriver_compile_succeeded
         apply force
        apply(force simp:prog_OverlayDriver_input_reqs)
       apply(force simp:prog_OverlayDriver_Cs_def)
      apply(force intro:RC.no_locks_acquired_C\<^sub>0_mds\<^sub>0_init_reqs)
     (* InputSwitch *)
     apply(clarsimp simp:prog_InputSwitch_RISC_def RC.mem\<^sub>A_of_def split:prod.splits)
     apply(rule RC.compile_cmd_correctness_\<R>'[where C=RC_C\<^sub>0 and l=None and nl=0 and Cs=prog_InputSwitch_Cs])
         apply clarsimp
         apply(rule conjI)
          apply(force simp:prog_InputSwitch_Cs_def zip_map_fst_snd)
         apply(rule conjI)
          apply force
         apply(rule conjI)
          apply force
         apply(rule conjI)
          apply force
         using prog_InputSwitch_compile_succeeded
         apply force
        apply(force simp:prog_InputSwitch_input_reqs)
       apply(force simp:prog_InputSwitch_Cs_def)
      apply(force intro:RC.no_locks_acquired_C\<^sub>0_mds\<^sub>0_init_reqs)
     apply(clarsimp simp:init_cms_CDDC_def init_cms_CDDC_RISC_def my_crs_def)
     apply(case_tac i)
      (* OverlayDriver *)
      apply clarsimp
      using prog_OverlayDriver_typed
      apply(clarsimp simp:RC.mem\<^sub>A_of_def)
      apply(rule TS.Typed_in_\<R>)
           apply force
          apply(force simp:TS.\<Gamma>_of_mds_def TS.\<S>_of_mds_def RC.mds\<^sub>0_def)
         apply(clarsimp simp:TM.low_mds_eq_def TS.\<Gamma>_of_mds_def TS.\<S>_of_mds_def RC.mds\<^sub>0_def)
         using TS.type_max_dma_type apply force
        apply force
       apply force
      apply(force simp:TS.\<Gamma>_of_mds_def RC.mds\<^sub>0_def)
     (* InputSwitch *)
     apply clarsimp
     using prog_InputSwitch_typed
     apply(clarsimp simp:RC.mem\<^sub>A_of_def)
     apply(rule TS.Typed_in_\<R>)
          apply force
         apply(force simp:TS.\<Gamma>_of_mds_def TS.\<S>_of_mds_def RC.mds\<^sub>0_def)
        apply(clarsimp simp:TM.low_mds_eq_def TS.\<Gamma>_of_mds_def TS.\<S>_of_mds_def RC.mds\<^sub>0_def)
        using TS.type_max_dma_type apply force
       apply force
      apply force
     apply(force simp:TS.\<Gamma>_of_mds_def RC.mds\<^sub>0_def)
    apply(rule CDDC_sound_mode_use)
    apply force
   apply(clarsimp simp:SRSI.modes_respect_priv_def init_cms_CDDC_RISC_def
           SRSI.priv_is_asm_priv_def SRSI.priv_is_guar_priv_def
           SRSI.new_asms_only_for_priv_def SRSI.new_asms_NoReadOrWrite_only_def)
   apply(rule conjI)
    apply(clarsimp simp:my_crs_def)
    apply(metis empty_iff less_Suc0 less_antisym nth_Cons_0 nth_Cons_Suc)
   apply(rule conjI)
    apply(clarsimp simp:my_crs_def)
    apply(metis empty_iff less_Suc0 less_antisym nth_Cons_0 nth_Cons_Suc)
   apply(rule conjI)
    apply(clarsimp simp:my_crs_def)
    apply(metis RC.mds\<^sub>0_no_asms less_Suc0 less_antisym nth_Cons_0 nth_Cons_Suc)
   apply(clarsimp simp:my_crs_def)
  by (simp add:RC.mds\<^sub>0_no_asms nth_Cons')

end