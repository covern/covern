(*
Author: Robert Sison
*)

theory Example_CDDC_WhileLockLanguage
imports "../../old-type-system/WhileLockTypeSystem"
  "~~/src/HOL/Word/Word" "~~/src/HOL/Eisbach/Eisbach"
  "../../old-type-system/LocallySoundWhileLockUse"
  "../../wr-compiler/RISCRefinementSafe"
begin

text {*
  Theory for the verification of a model of the 'model-restricted' branch of the CDDC system
  using the LockLanguage extension of the Dependent_SIFUM_Type_System framework.

  This theory serves as a proof-of-concept for the LockLanguage extension, whose purpose is to
  enforce the globally sound use of modes at the level of the language's locking semantics.
  In doing so, it is also the first case study for which we satisfy all proof obligations for the
  (assume-guarantee based) compositional verification of an entire (in this case, two-component)
  shared-variable concurrent system using the Dependent_SIFUM_Type_System framework.

  The model in this theory is an *abstraction* of the implementation of the CDDC system committed
  to the 'model-restricted' branch of the CDDC development repository, in the following respects:

  * Use of hardware APIs is modelled by reads or writes to variables with no assumptions on them
    (i.e. reads from them will return arbitrary values).

  * We model the user's (correct) decision to enter high or low input based on the UI-indicated
    security domain by using two separate (resp. statically High, Low-rated) input sources.

  * The RPC mechanism is implemented using the locking primitives provided by the LockLanguage,
    and is merely an abstraction of the RPC mechanisms provided by CAmkES on top of seL4.
    As such, it does not leverage any aspects of the seL4 information-flow proofs, but we would
    expect that future, higher-fidelity models of the CDDC implementation may do so.

  * Some of the more mundane (and more security-irrelevant) Switch component functionality is
    pared away in this model, with comment stubs indicating where they would belong if we were
    to model down to that level of detail:
    - compositor cursor shape
    - application of mouse acceleration
    - hardware button events

  The following abstractions are primarily due to the limitations of our verification framework:

  * Due to the limitations of the existing verification framework, it features only 1 High + 1 Low
    security classification domain, as opposed to 4 mutually distrusting domains.

  * Each of the components is flattened into a single function, because of current lack of support
    by the modelling language(s) for function calls.

  In turn, the 'model-restricted' version differs from the current CDDC development head
  in the following respects:

  * The 'Input' and 'Switch' components are combined into a single 'InputSwitch' component.
    In order to verify information-flow security for data passed between Input and Switch whose
    classification is value-dependent, we would need to add more general-purpose assume-guarantee
    support to the existing verification framework.

  * No domain-switching via hotkeys.
    Domain-switching only via mouse-clicks on the overlay buttons or on windows in the UI.

  * Vastly simplified state machine.
    The state machine on the development head uses a table full of function pointers,
    due to current lack of support for function pointers (or functions for that matter).

  The following is the pseudocode for the RPC abstraction we are using in this model of the system,
  implemented solely using LockLanguage's locking primitives:

  (Tentative) Lock-based RPC server model (valid only for one client)
    SERVER
     while (True) {
         lock(L)
             if (L_call) {
                 L_ret := fun(L_arg)
                 L_call := 0
             }
         unlock(L)
     }

  (Tentative) Lock-based RPC client call model (valid only with no other clients)
    CLIENT
     lock(L)
         L_arg := my_arg
         L_call := 1
     unlock(L)
     done := 0
     while (!done) {
         lock(L)
             if (!L_call) {
                 my_ret := L_ret
                 done := 1
             }
         unlock(L)
     }

  This theory also includes some experimentation with proof automation support provided by Eisbach.
*}

datatype addr =
    (* InputSwitch [] HidInputDevice
       These variables model HidInputDevice hardware interfaces, but the distinction
       between the 'high' and 'low' keyboard sources is artificial for the purpose of
       modelling a user basing their input decisions on the currently indicated domain. *)
    hid_mouse_available | hid_keyboard_available |
    hid_mouse_source |
    hid_high_keyboard_source | hid_low_keyboard_source |
    (* InputSwitch *)
    active_domain | current_event_data | current_hid_event_type | cursor_domain |
    switch_state_mouse_down | overlay_result | done_rpc | temp |
    (* InputSwitch [] CompositorDevice
       This variable models the current state of the top-of-screen title/color bar,
       controlled via a CompositorDevice hardware interface, which we will consider
       to be the authoritative domain indicator. *)
    indicated_domain | compositor_cursor_position | compositor_domain_under_cursor |
    (* InputSwitch |> OverlayDriver *)
    rpc_overlay_mouse_click_arg | rpc_overlay_mouse_click_call | rpc_overlay_mouse_click_ret |
    (* InputSwitch [] OutputX *)
    output_event_buffer0 | output_event_buffer1

instantiation addr :: "{enum}"
begin

abbreviation
  "addr_enum \<equiv> [
    hid_mouse_available, hid_keyboard_available,
    hid_mouse_source,
    hid_high_keyboard_source, hid_low_keyboard_source,
    (* InputSwitch *)
    active_domain, current_event_data, current_hid_event_type, cursor_domain,
    switch_state_mouse_down, overlay_result, done_rpc, temp,
    (* InputSwitch [] CompositorDevice
       This variable models the current state of the top-of-screen title/color bar,
       controlled via a CompositorDevice hardware interface, which we will consider
       to be the authoritative domain indicator. *)
    indicated_domain, compositor_cursor_position, compositor_domain_under_cursor,
    (* InputSwitch,> OverlayDriver *)
    rpc_overlay_mouse_click_arg, rpc_overlay_mouse_click_call, rpc_overlay_mouse_click_ret,
    (* InputSwitch [] OutputX *)
    output_event_buffer0, output_event_buffer1
    ]"

definition enum_addr_def:
  "enum_class.enum \<equiv> addr_enum"

definition enum_addr_all_def: "enum_class.enum_all P \<equiv> \<forall>r \<in> set addr_enum. P r"
definition enum_addr_ex_def: "enum_class.enum_ex P \<equiv> \<exists>r \<in> set addr_enum. P r"

instance proof
  show "(UNIV::addr set) = set enum_class.enum"
  unfolding enum_addr_def
  apply(clarsimp | safe)+
  by (rename_tac x, case_tac x, auto)
next
  show "distinct (enum_class.enum::addr list)"
  unfolding enum_addr_def by simp
next
  show "\<And>P::addr \<Rightarrow> bool. enum_class.enum_all P = Ball UNIV P"
  unfolding enum_addr_all_def
  apply(clarsimp | safe)+
  by (rename_tac x, case_tac x, auto)
next
  show "\<And>P::addr \<Rightarrow> bool. enum_class.enum_ex P = Bex UNIV P"
  unfolding enum_addr_ex_def
  by (auto, rename_tac x, case_tac x, auto)
qed
end

datatype lock_addr =
    rpc_overlay_mouse_click_lock | switch_private_lock |
    hid_read_atomicity_lock | compositor_read_atomicity_lock

type_synonym mem = "((lock_addr, addr) Var, 8 word) Mem"
type_synonym prog_mem = "(addr, 8 word) Mem"
type_synonym lock_mem = "((addr, lock_addr) Var, 8 word) Mem"

definition NULL_EVENT :: "8 word" where "NULL_EVENT = 0"
definition CDDC_HID_MOUSE_EVENT :: "8 word" where "CDDC_HID_MOUSE_EVENT = 1"
definition CDDC_HID_KEYBOARD_EVENT :: "8 word" where "CDDC_HID_KEYBOARD_EVENT = 2"

definition EVENT_TYPE_MOUSE_DOWN :: "8 word" where "EVENT_TYPE_MOUSE_DOWN = 42"

definition DOMAIN_INVALID :: "8 word" where "DOMAIN_INVALID = -1"
definition DOMAIN_LOW :: "8 word" where "DOMAIN_LOW = 0"
definition DOMAIN_HIGH :: "8 word" where "DOMAIN_HIGH = 1"
definition DOMAIN_OVERLAY :: "8 word" where "DOMAIN_OVERLAY = 2"

definition
  dma_event_data :: "prog_mem \<Rightarrow> addr \<Rightarrow> addr \<Rightarrow> Sec"
where
  (* The only potentially sensitive data is keyboard data when the indicated domain is high *)
  "dma_event_data m type domain \<equiv> if m type = CDDC_HID_KEYBOARD_EVENT \<and> m domain = DOMAIN_HIGH then High
                                   else Low"

definition
  dma :: "mem \<Rightarrow> (lock_addr, addr) Var \<Rightarrow> Sec"
where
  "dma m v \<equiv> case v of
               Var x \<Rightarrow>
                 if x = current_event_data
                   then dma_event_data (to_prog_mem m) current_hid_event_type indicated_domain
                   else if x = output_event_buffer1 \<or> x = hid_high_keyboard_source
                          then High
                          else Low |
               Lock l \<Rightarrow> Low"

definition
  \<C>_vars :: "(lock_addr, addr) Var \<Rightarrow> (lock_addr, addr) Var set"
where
  "\<C>_vars v \<equiv> case v of
                Var x \<Rightarrow>
                  if x = current_event_data
                    then {Var current_hid_event_type, Var indicated_domain}
                    else {} |
                Lock l \<Rightarrow> {}"

definition
  mds\<^sub>s :: "Mode \<Rightarrow> 'Var set"
where
  "mds\<^sub>s \<equiv> \<lambda>_. {}"

fun
  ev\<^sub>L :: "8 word \<Rightarrow> bool"
where
  "ev\<^sub>L w = (w \<noteq> 0)"

definition
  val\<^sub>L :: "bool \<Rightarrow> 8 word"
where
  "val\<^sub>L b \<equiv> if b then 1 else 0 "

datatype bin_op_id = BEq | BNeq | BOr | BAnd

fun bin_op_ev :: "bin_op_id \<Rightarrow> 8 word \<Rightarrow> 8 word \<Rightarrow> 8 word"
where
  "bin_op_ev BEq x y = val\<^sub>L (x = y)" |
  "bin_op_ev BNeq x y = val\<^sub>L (x \<noteq> y)" |
  "bin_op_ev BOr x y = val\<^sub>L (ev\<^sub>L x \<or> ev\<^sub>L y)" |
  "bin_op_ev BAnd x y = val\<^sub>L (ev\<^sub>L x \<and> ev\<^sub>L y)"

definition
  lock_interp :: "lock_addr \<Rightarrow> (addr set \<times> addr set)"
where
  "lock_interp l \<equiv> case l of rpc_overlay_mouse_click_lock \<Rightarrow> ({},
       {rpc_overlay_mouse_click_arg, rpc_overlay_mouse_click_call, rpc_overlay_mouse_click_ret}) |
       (* Dummy lock for Switch component's private variables *)
       switch_private_lock \<Rightarrow> ({
           (* Hardware state and APIs that can only be touched by this component *)
           indicated_domain,
           (* Variables private to this component *)
           active_domain, current_hid_event_type, current_event_data, done_rpc, temp,
           (* Note these Asms on private vars are not essential to the proof, because even if
              corrupted they only affects where mouse events go, and those are always typed Low.
              But include them for now because in the real system, they're private variables. *)
           cursor_domain, switch_state_mouse_down, overlay_result}, {}) |
       (* The idea of this lock is to represent the program's reliance on the HID
         hardware interface to provide values that remain stable and meaningful
         at least for the duration of a call to such read-interfaces. *)
       hid_read_atomicity_lock \<Rightarrow> ({hid_mouse_available, hid_keyboard_available,
           hid_mouse_source, hid_high_keyboard_source, hid_low_keyboard_source}, {}) |
       (* And similarly for the compositor device *)
       compositor_read_atomicity_lock \<Rightarrow> ({compositor_domain_under_cursor}, {})"

definition
  \<C> :: "(lock_addr, addr) Var set"
where
  "\<C> \<equiv> \<Union>x. \<C>_vars x"

(* Naive for initial instantiation: 32 GPRs *)
datatype reg =
  r0 |
  r1 |
  r2 |
  r3 |
  r4 |
  r5 |
  r6 |
  r7 |
  r8 |
  r9 |
  r10 |
  r11 |
  r12 |
  r13 |
  r14 |
  r15 |
  r16 |
  r17 |
  r18 |
  r19 |
  r20 |
  r21 |
  r22 |
  r23 |
  r24 |
  r25 |
  r26 |
  r27 |
  r28 |
  r29 |
  r30 |
  r31

instantiation reg :: "{enum, linorder}"
begin

abbreviation
  "reg_enum \<equiv> [
    r0,
    r1,
    r2,
    r3,
    r4,
    r5,
    r6,
    r7,
    r8,
    r9,
    r10,
    r11,
    r12,
    r13,
    r14,
    r15,
    r16,
    r17,
    r18,
    r19,
    r20,
    r21,
    r22,
    r23,
    r24,
    r25,
    r26,
    r27,
    r28,
    r29,
    r30,
    r31
    ]"

definition enum_reg_def:
  "enum_class.enum \<equiv> reg_enum"

primrec index_of_reg :: "reg \<Rightarrow> nat"
where
  "index_of_reg r0 = 0" |
  "index_of_reg r1 = 1" |
  "index_of_reg r2 = 2" |
  "index_of_reg r3 = 3" |
  "index_of_reg r4 = 4" |
  "index_of_reg r5 = 5" |
  "index_of_reg r6 = 6" |
  "index_of_reg r7 = 7" |
  "index_of_reg r8 = 8" |
  "index_of_reg r9 = 9" |
  "index_of_reg r10 = 10" |
  "index_of_reg r11 = 11" |
  "index_of_reg r12 = 12" |
  "index_of_reg r13 = 13" |
  "index_of_reg r14 = 14" |
  "index_of_reg r15 = 15" |
  "index_of_reg r16 = 16" |
  "index_of_reg r17 = 17" |
  "index_of_reg r18 = 18" |
  "index_of_reg r19 = 19" |
  "index_of_reg r20 = 20" |
  "index_of_reg r21 = 21" |
  "index_of_reg r22 = 22" |
  "index_of_reg r23 = 23" |
  "index_of_reg r24 = 24" |
  "index_of_reg r25 = 25" |
  "index_of_reg r26 = 26" |
  "index_of_reg r27 = 27" |
  "index_of_reg r28 = 28" |
  "index_of_reg r29 = 29" |
  "index_of_reg r30 = 30" |
  "index_of_reg r31 = 31"

definition enum_reg_all_def: "enum_class.enum_all P \<equiv> \<forall>r \<in> set reg_enum. P r"
definition enum_reg_ex_def: "enum_class.enum_ex P \<equiv> \<exists>r \<in> set reg_enum. P r"

definition linorder_reg_less_eq_def: "ord_class.less_eq a b \<equiv> index_of_reg a \<le> index_of_reg b"
definition linorder_reg_less_def: "ord_class.less a b \<equiv> index_of_reg a < index_of_reg b"

instance proof
  show "(UNIV::reg set) = set enum_class.enum"
  unfolding enum_reg_def
  apply(clarsimp | safe)+
  by (rename_tac x, case_tac x, auto)
next
  show "distinct (enum_class.enum::reg list)"
  unfolding enum_reg_def by simp
next
  show "\<And>P::reg \<Rightarrow> bool. enum_class.enum_all P = Ball UNIV P"
  unfolding enum_reg_all_def
  apply(clarsimp | safe)+
  by (rename_tac x, case_tac x, auto)
next
  show "\<And>P::reg \<Rightarrow> bool. enum_class.enum_ex P = Bex UNIV P"
  unfolding enum_reg_ex_def
  by (auto, rename_tac x, case_tac x, auto)
next
  show "\<And>(x::reg) y::reg. (x < y) = (x \<le> y \<and> \<not> y \<le> x)"
  unfolding linorder_reg_less_eq_def linorder_reg_less_def by linarith
next
  show "\<And>x::reg. x \<le> x"
  unfolding linorder_reg_less_eq_def by linarith
next
  show "\<And>(x::reg) (y::reg) z::reg. x \<le> y \<Longrightarrow> y \<le> z \<Longrightarrow> x \<le> z"
  unfolding linorder_reg_less_eq_def by linarith
next
  show "\<And>(x::reg) y::reg. x \<le> y \<Longrightarrow> y \<le> x \<Longrightarrow> x = y"
  unfolding linorder_reg_less_eq_def
  by (case_tac x, (case_tac y, auto)+)
next
  show "\<And>(x::reg) y::reg. x \<le> y \<or> y \<le> x"
  unfolding linorder_reg_less_eq_def by linarith
qed

end

primrec reg_alloc_cached' :: "reg list \<Rightarrow> (reg, 8 word, addr, bin_op_id) RegRec \<Rightarrow> addr \<Rightarrow> reg option"
where
  "reg_alloc_cached' [] \<Phi> v = None" |
  "reg_alloc_cached' (r#rs) \<Phi> v = (if \<Phi> r = Some (ELoad v) then Some r else reg_alloc_cached' rs \<Phi> v)"

lemma reg_alloc_cached'_correct1:
  "reg_alloc_cached' rs \<Phi> v = Some r \<Longrightarrow> \<Phi> r = Some (ELoad v) \<and> r \<in> set rs"
  apply(induct rs)
   apply clarsimp
  by (clarsimp split:if_splits)

lemma reg_alloc_cached'_correct2:
  "reg_alloc_cached' rs \<Phi> v = None \<Longrightarrow> \<Phi> r = Some (ELoad v) \<Longrightarrow> r \<notin> set rs"
  apply(induct rs)
   apply clarsimp
  by (clarsimp split:if_splits)

definition reg_alloc_cached :: "(reg, 8 word, addr, bin_op_id) RegRec \<Rightarrow> reg set \<Rightarrow> addr \<Rightarrow> reg option"
where
  "reg_alloc_cached \<Phi> A v = reg_alloc_cached' (sorted_list_of_set (dom \<Phi> - A)) \<Phi> v"

lemma reg_alloc_cached_correct1:
  "reg_alloc_cached \<Phi> A v = Some r \<Longrightarrow> \<Phi> r = Some (ELoad v) \<and> r \<notin> A"
  apply(clarsimp simp:reg_alloc_cached_def)
  apply(frule reg_alloc_cached'_correct1)
  by (metis DiffD2 equals0D set_empty sorted_list_of_set sorted_list_of_set.infinite)

lemma reg_alloc_cached_correct2:
  "finite (dom \<Phi>) \<Longrightarrow> reg_alloc_cached \<Phi> A v = None \<Longrightarrow> \<not> (\<exists>r. \<Phi> r = Some (ELoad v) \<and> r \<notin> A)"
  by (force dest:reg_alloc_cached'_correct2 simp:reg_alloc_cached_def)

function (domintros) reg_alloc' :: "(reg, 8 word, addr, bin_op_id) RegRec \<Rightarrow> reg set \<Rightarrow> nat \<Rightarrow> reg option"
where
  "reg_alloc' \<Phi> A n = (if n \<ge> (length reg_enum) then None
                       else (if reg_enum ! n \<notin> dom \<Phi> \<union> A then Some (reg_enum ! n)
                             else reg_alloc' \<Phi> A (Suc n)))"
   using prod_cases3 apply blast
  by clarsimp

lemma reg_alloc'_dom_correctness:
  "reg_alloc'_dom (\<Phi>, A, n) \<Longrightarrow> reg_alloc' \<Phi> A n = Some r \<Longrightarrow> r \<notin> (dom \<Phi> \<union> A)"
  apply(induct \<Phi> A n rule:reg_alloc'.pinduct)
  by (simp add:reg_alloc'.psimps split:if_splits)

lemma reg_alloc'_termination1:
  assumes max_above: "(length reg_enum) \<ge> n"
  shows "reg_alloc'_dom (\<Phi>, A, n)"
  using max_above
  by (induct rule:inc_induct) (auto intro:reg_alloc'.domintros)

lemma reg_alloc'_termination2:
  assumes max_below: "(length reg_enum) < n"
  shows "reg_alloc'_dom (\<Phi>, A, n)"
  using max_below
  by (auto intro:reg_alloc'.domintros)

termination
  apply clarsimp
  apply(rename_tac \<Phi> A n)
  apply(case_tac "(length reg_enum) \<ge> n")
   using reg_alloc'_termination1 apply blast
  using reg_alloc'_termination2 by force

lemma reg_alloc'_correctness:
  "finite (dom \<Phi>) \<Longrightarrow> finite (A::reg set) \<Longrightarrow> reg_alloc' \<Phi> A 0 = Some r \<Longrightarrow> r \<notin> (dom \<Phi> \<union> A)"
  apply(rule reg_alloc'_dom_correctness)
   apply(rule reg_alloc'_termination1)
   apply force
  by force

value [code] reg_alloc'
(*
lemma [code]:
  "finite (dom \<Phi>) \<Longrightarrow> finite (A::reg set) \<Longrightarrow>
   reg_alloc' \<Phi> A n = (if n \<ge> (length reg_enum) then None
                       else (if reg_enum ! n \<notin> dom \<Phi> \<union> A then Some (reg_enum ! n)
                             else reg_alloc' \<Phi> A (Suc n)))"
  by clarsimp
*)

definition reg_alloc :: "(reg, 8 word, addr, bin_op_id) RegRec \<Rightarrow> reg set \<Rightarrow> reg option"
where
  "reg_alloc \<Phi> A = reg_alloc' \<Phi> A 0"

(* This way of proving finite_Vars is Toby's from cddc-buf development head *)
lemma UNIV[simp]:
  "UNIV = Var ` {hid_mouse_available , hid_keyboard_available ,
    hid_mouse_source ,
    hid_high_keyboard_source , hid_low_keyboard_source ,
    active_domain ,
    current_event_data , current_hid_event_type , cursor_domain ,
    switch_state_mouse_down , overlay_result , done_rpc , temp,
    indicated_domain , compositor_cursor_position , compositor_domain_under_cursor ,
    rpc_overlay_mouse_click_arg , rpc_overlay_mouse_click_call , rpc_overlay_mouse_click_ret ,
    output_event_buffer0 , output_event_buffer1} \<union> Lock ` {
    rpc_overlay_mouse_click_lock, switch_private_lock,
    hid_read_atomicity_lock, compositor_read_atomicity_lock}"
  apply (clarsimp | safe)+
  apply(rename_tac x, case_tac x, auto)+
  done

lemma UNIV_rewrite:
  "UNIV = (Var ` UNIV \<union> Lock ` UNIV)"
  apply (clarsimp | safe)+
  apply(case_tac x, auto)
  done

lemma finite_Vars:
  "finite {x::(lock_addr,addr) Var. True}"
  apply simp
  done

lemma bin_op_ev_det:
  "\<forall>x\<in>op_set_param_lang.exp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<Longrightarrow>
   op_set_param_lang.exp_ev bin_op_ev mem\<^sub>1 e = op_set_param_lang.exp_ev bin_op_ev mem\<^sub>2 e"
  apply(induct e)
    apply(force simp:op_set_param_lang.exp_ev.simps(1))
   apply(simp add:op_set_param_lang.exp_ev.simps(2) op_set_param_lang.exp_vars.simps(2))
  by (simp add:op_set_param_lang.exp_vars.simps(3) op_set_param_lang.exp_ev.simps(3))

global_interpretation
  OSPL: op_set_param_lang bin_op_ev
  defines OSPL_exp_vars = OSPL.exp_vars
  done

interpretation
  WL: sifum_lang_no_dma OSPL.exp_ev OSPL.exp_ev\<^sub>B ev\<^sub>L OSPL.exp_vars OSPL.exp_vars lock_interp 1 0
  apply(unfold_locales)
      using finite_Vars apply blast
     unfolding OSPL_exp_vars_def using bin_op_ev_det apply blast
    apply(clarsimp simp:op_set_param_lang.exp_ev\<^sub>B_def)
    using bin_op_ev_det apply force
   apply force
  by force

global_interpretation
  RL: risc_lang_no_dma bin_op_ev r31 output_event_buffer1 rpc_overlay_mouse_click_lock ev\<^sub>L lock_interp 1 0
  defines RL_fresh_label = RL.fresh_label
  by (unfold_locales, force+)

global_interpretation
  RC: risc_compilation bin_op_ev dma \<C>_vars \<C> bin_op_ev r31 output_event_buffer1 rpc_overlay_mouse_click_lock ev\<^sub>L lock_interp 1 0 reg_alloc_cached reg_alloc
      WL.no_locks_acquired RL.no_locks_acquired WL.bisim_simple_ev\<^sub>B_eq
  defines RC_compile_cmd = RC.compile_cmd
      and RC_compile_expr = RC.compile_expr
      and RC_C\<^sub>0 = RC.C\<^sub>0
      and RC_asmrec_var_stable' = RC.asmrec_var_stable'
      and RC_regrec_purge_vars = RC.regrec_purge_vars
      and RC_regrec_purge_var = RC.regrec_purge_var
      and RC_exp_vars_and_\<C>s = RC.exp_vars_and_\<C>s
  apply(unfold_locales)
                       using finite_Vars apply blast
                      using bin_op_ev_det apply blast
                     apply(clarsimp simp:op_set_param_lang.exp_ev\<^sub>B_def)
                     using bin_op_ev_det apply force
                    apply force
                   apply force
                  apply(force simp:\<C>_vars_def split:Var.splits)
                 apply(force simp:\<C>_vars_def dma_def dma_event_data_def to_prog_mem_def split:Var.splits)
                apply(force simp:\<C>_vars_def dma_def)
               apply(auto simp:\<C>_vars_def split:Var.splits)[2]
             apply(force simp:lone_lock_per_var_def lock_interp_def split:lock_addr.splits)
            apply(force simp:no_vacuous_locks_def lock_interp_def split:lock_addr.splits)
           apply blast
          apply blast
         apply(force simp:\<C>_vars_def dma_def)
        apply(force simp:lock_interp_no_overlap_def lock_interp_def split:lock_addr.splits)
       apply(force simp:\<C>_vars_def lock_interp_def split:lock_addr.splits if_splits)
      apply(unfold reg_alloc_def)
      using reg_alloc'_correctness apply blast
     using reg_alloc_cached_correct1 apply blast
    using reg_alloc_cached_correct2 apply blast
   apply force
  by (force simp:\<C>_vars_def \<C>_def)

fun
  exp_neg :: "(8 word, addr, bin_op_id) exp \<Rightarrow> (8 word, addr, bin_op_id) exp"
where
  "exp_neg (EConst c) = EOp BEq (EConst c) (EConst 0)" |
  "exp_neg (ELoad v) = EOp BEq (ELoad v) (EConst 0)" |
  "exp_neg (EOp BEq x y) = EOp BNeq x y" |
  "exp_neg (EOp BNeq x y) = EOp BEq x y" |
  "exp_neg (EOp BOr x y) = EOp BAnd (exp_neg x) (exp_neg y)" |
  "exp_neg (EOp BAnd x y) = EOp BOr (exp_neg x) (exp_neg y)"

declare val\<^sub>L_def [simp]

lemma exp_neg_negates:
  "OSPL.exp_ev\<^sub>B mem (exp_neg e) = (\<not> OSPL.exp_ev\<^sub>B mem e)"
  unfolding OSPL.exp_ev\<^sub>B_def
  apply(induct e, simp_all)
  apply(rename_tac opid e1 e2)
  by (case_tac opid, simp+)

fun
  dma_type :: "addr \<Rightarrow> (8 word, addr, bin_op_id) exp set"
where
  "dma_type output_event_buffer1 = {EConst 0}" |
  "dma_type hid_high_keyboard_source = {EConst 0}" |
  "dma_type current_event_data = {EOp BOr (EOp BNeq (ELoad current_hid_event_type) (EConst CDDC_HID_KEYBOARD_EVENT)) (EOp BNeq (ELoad indicated_domain) (EConst DOMAIN_HIGH))}" |
  "dma_type _ = {}"

definition
  dma_type' :: "(lock_addr, addr) Var \<Rightarrow> (8 word, addr, bin_op_id) exp set"
where
  "dma_type' v = (case v of Lock l \<Rightarrow> {} |
                            Var x \<Rightarrow> dma_type x)"

fun
  exp_assign :: "addr \<Rightarrow> (8 word, addr, bin_op_id) exp \<Rightarrow> (8 word, addr, bin_op_id) exp"
where
  "exp_assign x (EConst c) = EOp BEq (ELoad x) (EConst c)" |
  "exp_assign x (ELoad y) = EOp BEq (ELoad x) (ELoad y)" |
  "exp_assign x (EOp opid y z) = EOp BEq (ELoad x) (EOp opid y z)"

interpretation
  TS: sifum_types_assign OSPL.exp_ev OSPL.exp_ev\<^sub>B ev\<^sub>L OSPL.exp_vars OSPL.exp_vars lock_interp dma \<C>_vars \<C> 1 0 exp_neg dma_type' "EConst 0" exp_assign
  apply(unfold_locales)
          using exp_neg_negates apply blast
         apply(case_tac x, clarsimp simp:dma_def dma_event_data_def to_prog_mem_def dma_type'_def)
         apply(rename_tac x')
         apply(case_tac x', auto simp:dma_def dma_event_data_def to_prog_mem_def dma_type'_def OSPL.exp_ev\<^sub>B_def)[1]
        apply(case_tac x, clarsimp simp:\<C>_vars_def to_prog_mem_def dma_type'_def)
        apply(rename_tac x')
        apply(case_tac x', auto simp:\<C>_vars_def to_prog_mem_def dma_type'_def OSPL.exp_ev\<^sub>B_def)[1]
       apply(force simp:OSPL.exp_ev\<^sub>B_def)
      apply force
     apply(force simp:\<C>_vars_def to_prog_mem_def dma_type'_def)
    apply(force simp:\<C>_vars_def lock_interp_def split:lock_addr.splits if_splits)
   apply(clarsimp simp:OSPL.exp_ev\<^sub>B_def)
   apply(case_tac e, simp_all)
  by (case_tac e, force+)

interpretation TM: sifum_modes OSPL.exp_ev OSPL.exp_ev\<^sub>B ev\<^sub>L OSPL.exp_vars OSPL.exp_vars lock_interp dma \<C>_vars \<C> 1 0
  by (unfold_locales, auto simp: \<C>_vars_def split:Var.splits)

declare WL.restrict_preds_to_vars_def [simp]
declare Collect_conj_eq [simp]
declare Collect_disj_eq [simp]
declare TS.subtype_def [simp]
declare dma_type'_def [simp]
declare TS.pred_entailment_def [simp]
declare pred_def [simp]
declare to_prog_mem_def [simp]
declare TS.add_anno_def [simp]
declare restrict_map_def [simp]
declare TS.add_anno_dom_def [simp]
declare lock_interp_def [simp]
declare TS.add_anno_stable_def [simp]
declare TS.context_equiv_def [simp]
declare TS.type_equiv_def [simp]
declare TS.tyenv_wellformed_def [simp]
declare TS.mds_consistent_def [simp]
declare TS.types_wellformed_def [simp]
declare TS.types_stable_def [simp]
declare TS.type_wellformed_def [simp]

declare WL.pred_def [simp]
declare WL.add_pred_def [simp]
declare WL.stable_def [simp]
declare TS.to_total_def [simp]

declare OSPL.exp_ev\<^sub>B_def [simp]

declare TS.anno_type_stable_def [simp]
declare TS.assign_post_def [simp]

declare \<C>_vars_def [simp]
declare insert_Diff_if [simp]

(* The following lemmas, if_fun_(disj|not_conj) make it possible for the simplifier
  to reduce the typing environments to the user-friendly maplet form armed only with
  the definitions for restrict_preds_to_vars, etc. *)

lemma if_fun_disj[simp]:
  "(\<lambda>x. if x = a | P x then (f x) else g x)
    = ((\<lambda>x. if P x then (f x) else g x)(a := f a))"
  "(\<lambda>x. if x = a then (f x) else g x)
    = (g(a := f a))"
  by auto

lemma if_fun_not_conj[simp]:
  "(\<lambda>x. if x \<noteq> a \<and> P x then (f x) else g x)
    = ((\<lambda>x. if x = a \<or> \<not> P x then (g x) else f x))"
  "(\<lambda>x. if x \<noteq> a then (f x) else g x)
    = ((\<lambda>x. if x = a then (g x) else f x))"
  by auto

(* An experimental one I committed to cddc-buf branch *)
lemma if_fun_conj[simp]:
  "(\<lambda>x. if x = a \<and> P x then (f x) else g x)
    = ((\<lambda>x. if x \<noteq> a \<or> \<not> P x then (g x) else f x))"
  "(\<lambda>x. if x = a then (f x) else g x)
    = ((\<lambda>x. if x \<noteq> a then (g x) else f x))"
  by auto

(* New one again thanks to Thomas Sewell - useful for slightly more complex case *)
lemma if_fun_conj_disj[simp]:
  "Q a \<Longrightarrow> (\<lambda>x. if (x = a | P x) \<and> Q x then (f x) else g x)
    = ((\<lambda>x. if P x \<and> Q x then f x else g x) (a := f a))"
  by auto

(* Variation on Tom's, seems to be the missing case we need to bust leading false disjunctions *)
lemma if_fun_conj_not_disj[simp]:
  "\<not> Q a \<Longrightarrow> (\<lambda>x. if (x = a | P x) \<and> Q x then (f x) else g x)
    = ((\<lambda>x. if P x \<and> Q x then f x else g x))"
  "\<not> Q a \<Longrightarrow> (\<lambda>x. if x = a \<and> Q x then (f x) else g x) = g"
  by auto

lemma \<C>_simp[simp]:
  "\<C> = {Var current_hid_event_type, Var indicated_domain}"
  by (auto simp: \<C>_def split: if_splits Var.splits)

lemma type_aexpr_Load:
  "Var v \<notin> dom \<Gamma> \<Longrightarrow> TS.type_aexpr \<Gamma> (ELoad v) (dma_type v)"
  apply(insert TS.type_aexpr[of \<Gamma> "ELoad v", simplified])
  by simp

lemma type_aexpr_Load':
  "Var v \<in> dom \<Gamma> \<Longrightarrow> t = (the (\<Gamma> (Var v))) \<Longrightarrow> TS.type_aexpr \<Gamma> (ELoad v) t"
  apply(insert TS.type_aexpr[of \<Gamma> "ELoad v", simplified])
  by simp

lemma type_aexpr_Const:
  "TS.type_aexpr \<Gamma> (EConst c) {}"
  apply(insert TS.type_aexpr[of \<Gamma> "EConst c", simplified])
  by simp

(* Components of the CDDC remodelled to have Switch interface with the HidInputDevice directly  *)
definition
  prog_OverlayDriver :: "(addr, lock_addr, (8 word, addr, bin_op_id) exp, (8 word, addr, bin_op_id) exp) Stmt"
where
  "prog_OverlayDriver \<equiv>
     (* Lock-based RPC server model, for one client - see top of theory. *)
     While (EConst 1) (
       Stmt.LockAcq rpc_overlay_mouse_click_lock ;;
       If (EOp BNeq (ELoad rpc_overlay_mouse_click_call) (EConst 0)) (
         (* if (point_in_button(&buttons[0], click_x, click_y)) *)
         If (EOp BEq (ELoad rpc_overlay_mouse_click_arg) (EConst 121)) (
           rpc_overlay_mouse_click_ret \<leftarrow> EConst DOMAIN_LOW
         ) (
           (* if (point_in_button(&buttons[1], click_x, click_y)) *)
           If (EOp BEq (ELoad rpc_overlay_mouse_click_arg) (EConst 212)) (
             rpc_overlay_mouse_click_ret \<leftarrow> EConst DOMAIN_HIGH
           ) (
             (* if (click_y < 50) *)
             If (EOp BEq (ELoad rpc_overlay_mouse_click_arg) (EConst 50)) (
               rpc_overlay_mouse_click_ret \<leftarrow> EConst DOMAIN_OVERLAY
             ) (
               rpc_overlay_mouse_click_ret \<leftarrow> EConst DOMAIN_INVALID
             )
           )
         ) ;;
         (rpc_overlay_mouse_click_call \<leftarrow> EConst 0)
       ) Skip ;;
       Stmt.LockRel rpc_overlay_mouse_click_lock
     )"

abbreviation \<Gamma>\<^sub>0
where
  "\<Gamma>\<^sub>0 \<equiv> TS.\<Gamma>_of_mds RC.mds\<^sub>0"

abbreviation \<S>\<^sub>0
where
  "\<S>\<^sub>0 \<equiv> TS.\<S>_of_mds RC.mds\<^sub>0"

definition P\<^sub>0 :: "(8 word, addr, bin_op_id) exp set"
where
  "P\<^sub>0 \<equiv> {}"

declare P\<^sub>0_def[simp]

lemma prog_OverlayDriver_typed:
  "TS.has_type \<Gamma>\<^sub>0 \<S>\<^sub>0 P\<^sub>0 prog_OverlayDriver \<Gamma>\<^sub>0 \<S>\<^sub>0 P\<^sub>0"
  unfolding TS.\<Gamma>_of_mds_def TS.\<S>_of_mds_def RC.mds\<^sub>0_def
  apply(simp add: prog_OverlayDriver_def)
  apply(rule TS.while_type, blast, blast, simp)
  apply(rule TS.seq_type, blast)
   apply(rule TS.lock_acq_type, simp+)
  apply(rule TS.seq_type, blast)
   apply(rule TS.if_type', rule TS.type_bexprI, simp+)
     apply(rule TS.seq_type, blast)
      apply(rule TS.if_type', rule TS.type_bexprI, simp+)
        apply(rule TS.assign\<^sub>2, simp, clarsimp, rule type_aexpr_Const, simp+)
       apply(rule TS.if_type', rule TS.type_bexprI, simp+)
         apply(rule TS.assign\<^sub>2, simp, clarsimp, rule type_aexpr_Const, simp+)
        apply(rule TS.if_type', rule TS.type_bexprI, simp+)
          apply(rule TS.assign\<^sub>2, simp, clarsimp, rule type_aexpr_Const, simp+)
         apply(rule TS.assign\<^sub>2, simp, clarsimp, rule type_aexpr_Const, simp+)
        apply blast
       apply blast
      apply blast
     apply(rule TS.assign\<^sub>2, simp, clarsimp, rule type_aexpr_Const, simp+)
    apply(rule TS.skip_type')
      apply force
     apply simp
    apply simp
   apply blast
  apply(rule TS.lock_rel_type, simp+)
  done

(* Automation *)
method assign_tac =
  solves \<open>rule type_aexpr_Const\<close> |
  solves \<open>rule type_aexpr_Load, (rule order_refl|simp|clarsimp split:Var.splits)+\<close> |
  solves \<open>rule type_aexpr_Load', (rule order_refl|simp|clarsimp split:Var.splits)+\<close>

method security_type_tac' =
  rule order_refl |
  (rule TS.seq_type, blast) |
  (rule TS.while_type, blast, blast, simp) |
  solves \<open>(rule TS.skip_type', (force|simp|blast)+)\<close> |
  solves \<open>(rule TS.skip_type)\<close> |
  solves \<open>(rule TS.assign\<^sub>1, simp, clarsimp, simp, assign_tac, (rule order_refl|simp|clarsimp split:Var.splits|fast|force)+)\<close> |
  solves \<open>(rule TS.assign\<^sub>2, simp, clarsimp, assign_tac, (rule order_refl|simp|clarsimp split:Var.splits|fast|force)+)\<close> |
  solves \<open>(rule TS.assign\<^sub>\<C>, simp, clarsimp, assign_tac, (rule order_refl|simp|clarsimp split:Var.splits|fast|force)+)\<close> |
  solves \<open>(rule TS.lock_acq_type, simp+)\<close> |
  solves \<open>(rule TS.lock_rel_type, force, force, force, force, clarsimp+)\<close> |
  simp | clarsimp | solves \<open>auto\<close>

method security_type_tac =
  (rule TS.skip_type', (force|simp|blast)+) |
  (rule TS.if_type', rule TS.type_bexprI, simp, clarsimp+) |
  security_type_tac'

lemma prog_OverlayDriver_typed':
  "\<Gamma> = TS.\<Gamma>_of_mds RC.mds\<^sub>0 \<Longrightarrow>
   \<S> = TS.\<S>_of_mds RC.mds\<^sub>0 \<Longrightarrow> P = {} \<Longrightarrow>
  \<exists>\<Gamma>' \<S>' P'. TS.has_type \<Gamma> \<S> P prog_OverlayDriver \<Gamma>' \<S>' P'"
  apply(intro exI)
  unfolding TS.\<Gamma>_of_mds_def TS.\<S>_of_mds_def RC.mds\<^sub>0_def
  apply(simp add: prog_OverlayDriver_def)
  apply security_type_tac+
  done

lemma prog_OverlayDriver_com_sifum_secure:
  "RC.WL.com_sifum_secure (prog_OverlayDriver, RC.mds\<^sub>0)"
  apply(insert prog_OverlayDriver_typed)
  apply(rule TS.typed_secure)
   unfolding RC.mds\<^sub>0_def
   apply(fastforce simp: TS.\<Gamma>_of_mds_def TS.\<S>_of_mds_def)
  apply(auto simp: TS.mds_yields_stable_types_def)
  done

declare RC.no_lock_mds\<^sub>0[simp]

lemma [simp]:
  "no_lock_mds (lock_acq_upd lock_interp rpc_overlay_mouse_click_lock RC.mds\<^sub>0)"
  by (clarsimp simp:no_lock_mds_def lock_acq_upd_def RC.mds\<^sub>0_def image_def split:Mode.splits lock_addr.splits)

lemma mds\<^sub>0_acq_rel:
  "RC.mds\<^sub>0 =
    lock_rel_upd lock_interp rpc_overlay_mouse_click_lock
     (lock_acq_upd lock_interp rpc_overlay_mouse_click_lock RC.mds\<^sub>0)"
  unfolding RC.mds\<^sub>0_def
  apply(rule ext)
  apply(clarsimp simp:lock_acq_upd_def lock_rel_upd_def image_def split:Mode.splits lock_addr.splits)
  apply(rule set_eqI)
  apply clarsimp
  apply(case_tac x)
   apply blast
  by blast
  
lemma prog_OverlayDriver_mode_typed:
  "\<exists>mds. TM.mode_type RC.mds\<^sub>0 prog_OverlayDriver mds"
  apply(intro exI)
  apply(simp add: prog_OverlayDriver_def)
  apply(rule TM.while, simp+)
  apply(rule_tac TM.seq, simp)
   apply(rule_tac TM.lock_acq, simp+)
  apply(rule TM.seq, simp)
   apply(rule TM.if_)
       apply(force simp:to_prog_var_set_def lock_acq_upd_def)
      apply(force simp:to_prog_var_set_def split:Var.splits)
     apply force
    apply(rule TM.seq, simp)
     apply(rule TM.if_)
         apply(force simp:to_prog_var_set_def lock_acq_upd_def)
        apply(force simp:to_prog_var_set_def split:Var.splits)
       apply force
      apply(rule TM.assign)
         apply(force simp:to_prog_var_set_def lock_acq_upd_def RC.mds\<^sub>0_def split:lock_addr.splits)
        apply force
       using RC.WL.\<C>_vars_subset_\<C> apply auto[1]
      apply force
     apply(rule TM.if_)
         apply(force simp:to_prog_var_set_def lock_acq_upd_def)
        apply(force simp:to_prog_var_set_def split:Var.splits)
       apply force
      apply(rule TM.assign)
         apply(clarsimp simp:to_prog_var_set_def lock_acq_upd_def RC.mds\<^sub>0_def split:lock_addr.splits)
        apply force
       using RC.WL.\<C>_vars_subset_\<C> apply auto[1]
      apply force
     apply(rule TM.if_)
         apply(force simp:to_prog_var_set_def lock_acq_upd_def)
        apply(force simp:to_prog_var_set_def split:Var.splits)
       apply force
      apply(rule TM.assign)
        apply(clarsimp simp:to_prog_var_set_def lock_acq_upd_def RC.mds\<^sub>0_def split:lock_addr.splits)
       apply force
      using RC.WL.\<C>_vars_subset_\<C> apply auto[1]
     apply force
    apply(rule TM.assign)
       apply(clarsimp simp:to_prog_var_set_def lock_acq_upd_def RC.mds\<^sub>0_def split:lock_addr.splits)
      apply force
     using RC.WL.\<C>_vars_subset_\<C> apply auto[1]
    apply force
   apply(rule TM.assign)
      apply(clarsimp simp:to_prog_var_set_def lock_acq_upd_def RC.mds\<^sub>0_def split:lock_addr.splits)
     apply force
    using RC.WL.\<C>_vars_subset_\<C> apply auto[1]
   apply force
  apply(rule TM.skip, simp)
  apply(rule TM.lock_rel)
     apply(force simp:lock_held_mds_correct_def to_prog_var_set_def lock_acq_upd_def RC.mds\<^sub>0_def split:lock_addr.splits)
    apply force
   apply(force intro:mds\<^sub>0_acq_rel)
  by force

lemma prog_OverlayDriver_locally_sound_mode_use:
  "RC.WL.locally_sound_mode_use \<langle>prog_OverlayDriver, RC.mds\<^sub>0, mem\<rangle>\<^sub>A"
  using prog_OverlayDriver_mode_typed TM.mode_type_sound
  by blast

method mode_type_tac =
  match conclusion in "TM.mode_type RC.mds\<^sub>0
     (Stmt.LockAcq rpc_overlay_mouse_click_lock ;; ?something)
     RC.mds\<^sub>0" \<Rightarrow> "rule TM.seq" |
  rule TM.seq |
  rule TM.while |
  rule TM.skip |
  rule TM.if_ |
  rule TM.assign |
  simp |
  solves \<open>clarsimp simp:to_prog_var_set_def lock_acq_upd_def\<close> |
  solves \<open>clarsimp simp:to_prog_var_set_def split:Var.splits\<close> |
  solves \<open>clarsimp simp:lock_held_mds_correct_def to_prog_var_set_def lock_acq_upd_def RC.mds\<^sub>0_def split:lock_addr.splits\<close> |
  solves \<open>force intro:mds\<^sub>0_acq_rel\<close>

lemma prog_OverlayDriver_mode_typed':
  "\<exists>mds. TM.mode_type RC.mds\<^sub>0 prog_OverlayDriver mds"
  apply(intro exI)
  apply(simp add: prog_OverlayDriver_def)
  apply mode_type_tac+
   apply(rule_tac NoW_vars="fst (lock_interp rpc_overlay_mouse_click_lock)" and
                  NoRW_vars="snd (lock_interp rpc_overlay_mouse_click_lock)" in TM.lock_acq, simp+)
  apply mode_type_tac+
  apply(rule_tac NoW_vars="fst (lock_interp rpc_overlay_mouse_click_lock)" and
                 NoRW_vars="snd (lock_interp rpc_overlay_mouse_click_lock)" in TM.lock_rel)
  by mode_type_tac+

definition
  prog_InputSwitch :: "(addr, lock_addr, (8 word, addr, bin_op_id) exp, (8 word, addr, bin_op_id) exp) Stmt"
where
  "prog_InputSwitch \<equiv>
     (* Permanently grab this dummy lock *)
     Stmt.LockAcq switch_private_lock ;;

     (current_event_data \<leftarrow> EConst 0) ;;
     (indicated_domain \<leftarrow> ELoad active_domain) ;;
     (current_hid_event_type \<leftarrow> EConst NULL_EVENT) ;;

     While (EConst 1) (
       (* Pass through one mouse event, if available.
          If it's a click on the overlay button or window of a non-top domain, switch to it. *)
       Stmt.LockAcq hid_read_atomicity_lock ;;
       (temp \<leftarrow> ELoad hid_mouse_available) ;;
       Stmt.LockRel hid_read_atomicity_lock ;;
       If (EOp BNeq (ELoad temp) (EConst 0)) (
         (current_hid_event_type \<leftarrow> EConst CDDC_HID_MOUSE_EVENT) ;;
         Stmt.LockAcq hid_read_atomicity_lock ;;
         (current_event_data \<leftarrow> ELoad hid_mouse_source) ;;
         Stmt.LockRel hid_read_atomicity_lock ;;

         (* A model of the decision based on the mouse event data on whether we should
            switch domain. In this model, only mouse-clicks can cause a domain switch.
            Furthermore, mouse events are considered a Low-only rated control channel. *)

         (* Lock-based RPC client call model with no other clients - see top of theory. *)
         Stmt.LockAcq rpc_overlay_mouse_click_lock ;;
         (rpc_overlay_mouse_click_arg \<leftarrow> ELoad current_event_data) ;;
         (rpc_overlay_mouse_click_call \<leftarrow> EConst 1) ;;
         Stmt.LockRel rpc_overlay_mouse_click_lock ;;
         (done_rpc \<leftarrow> EConst 0) ;;
         While (EOp BEq (ELoad done_rpc) (EConst 0)) (
           Stmt.LockAcq rpc_overlay_mouse_click_lock ;;
           If (EOp BNeq (ELoad rpc_overlay_mouse_click_call) (EConst 0)) (
             (overlay_result \<leftarrow> ELoad rpc_overlay_mouse_click_ret) ;;
             (done_rpc \<leftarrow> EConst 1)
           ) Skip ;;
           Stmt.LockRel rpc_overlay_mouse_click_lock
         ) ;;

         If (EOp BNeq (ELoad overlay_result) (EConst DOMAIN_INVALID)) (
           cursor_domain \<leftarrow> EConst DOMAIN_OVERLAY
         ) (
           (* Approximate use of these compositor hardware library APIs:
           COMPOSITOR_SET_CURSOR_POSITION(mouse_event_get_position(current_event_data)) ;;
           cursor_domain = COMPOSITOR_GET_TOP_DOMAIN_UNDER_CURSOR() ;; *)
           (compositor_cursor_position \<leftarrow> ELoad current_event_data) ;;
           Stmt.LockAcq compositor_read_atomicity_lock ;;
           (cursor_domain \<leftarrow> ELoad compositor_domain_under_cursor) ;;
           Stmt.LockRel compositor_read_atomicity_lock ;;
           If (EOp BEq (ELoad cursor_domain) (EConst DOMAIN_INVALID)) (
             cursor_domain \<leftarrow> ELoad active_domain
           ) Skip
         ) ;;

         If (EOp BEq (ELoad cursor_domain) (EConst DOMAIN_OVERLAY)) (
           If (EOp BAnd (EOp BNeq (ELoad overlay_result) (EConst DOMAIN_OVERLAY))
                   (EOp BAnd (EOp BEq (ELoad switch_state_mouse_down) (EConst 0))
                        (EOp BAnd (EOp BEq (ELoad current_event_data) (EConst EVENT_TYPE_MOUSE_DOWN))
                             (EOp BAnd (EOp BNeq (ELoad overlay_result) (EConst DOMAIN_INVALID))
                                  (EOp BNeq (ELoad overlay_result) (ELoad active_domain)))))) (
             (active_domain \<leftarrow> ELoad overlay_result) ;;
             (* Approximate use of cddc_compositor_bubble_domain_to_top API *)
             (indicated_domain \<leftarrow> ELoad active_domain)
           ) Skip
         ) (
           If (EOp BAnd (EOp BEq (ELoad switch_state_mouse_down) (EConst 0))
                   (EOp BAnd (EOp BEq (ELoad current_event_data) (EConst EVENT_TYPE_MOUSE_DOWN))
                        (EOp BNeq (ELoad active_domain) (ELoad cursor_domain)))) (
             (active_domain \<leftarrow> ELoad cursor_domain) ;;
             (* Approximate use of cddc_compositor_bubble_domain_to_top API *)
             (indicated_domain \<leftarrow> ELoad active_domain)
           ) Skip ;;

           (* Send hovering mouse events to cursor_domain (not active_domain).
              Note that in the current CDDC, the cursor_domain is non-persistent state. *)
           If (EOp BOr (EOp BNeq (ELoad switch_state_mouse_down) (EConst 0)) (EOp BEq (ELoad current_event_data) (EConst EVENT_TYPE_MOUSE_DOWN))) (
             If (EOp BEq (ELoad active_domain) (EConst DOMAIN_LOW)) (
               output_event_buffer0 \<leftarrow> ELoad current_event_data
             ) (
               output_event_buffer1 \<leftarrow> ELoad current_event_data
             )
           ) (
             If (EOp BEq (ELoad cursor_domain) (EConst DOMAIN_LOW)) (
               output_event_buffer0 \<leftarrow> ELoad current_event_data
             ) (
               output_event_buffer1 \<leftarrow> ELoad current_event_data
             )
           )
         ) ;;

         If (EOp BEq (ELoad current_event_data) (EConst EVENT_TYPE_MOUSE_DOWN)) (
           switch_state_mouse_down \<leftarrow> EConst 1
         ) (
           switch_state_mouse_down \<leftarrow> EConst 0
         )
       ) Skip ;;

       (* Pass through one keyboard event, if available *)
       Stmt.LockAcq hid_read_atomicity_lock ;;
       (temp \<leftarrow> ELoad hid_keyboard_available) ;;
       Stmt.LockRel hid_read_atomicity_lock ;;
       If (EOp BNeq (ELoad temp) (EConst 0)) (
         (* Hacky clearance so the type assignment is safe. *)
         (current_event_data \<leftarrow> EConst 0) ;;
         (current_hid_event_type \<leftarrow> EConst CDDC_HID_KEYBOARD_EVENT) ;;
         (* This rather artificially models the user's decision to type high or low input
            based on the domain currently indicated by the compositor *)
         If (EOp BEq (ELoad indicated_domain) (EConst DOMAIN_HIGH)) (
           Stmt.LockAcq hid_read_atomicity_lock ;;
           (current_event_data \<leftarrow> ELoad hid_high_keyboard_source) ;;
           Stmt.LockRel hid_read_atomicity_lock
         ) (
           Stmt.LockAcq hid_read_atomicity_lock ;;
           (current_event_data \<leftarrow> ELoad hid_low_keyboard_source) ;;
           Stmt.LockRel hid_read_atomicity_lock
         ) ;;

         (* Send keyboard events to the active_domain *)
         If (EOp BEq (ELoad active_domain) (EConst DOMAIN_LOW)) (
           output_event_buffer0 \<leftarrow> ELoad current_event_data
         ) (
           output_event_buffer1 \<leftarrow> ELoad current_event_data
         )
       ) Skip ;;

       (current_event_data \<leftarrow> EConst 0) ;;
       (current_hid_event_type \<leftarrow> EConst NULL_EVENT)
     )"

abbreviation
  "\<Gamma>'_InputSwitch \<equiv> [Var overlay_result \<mapsto> {}, Var switch_state_mouse_down \<mapsto> {}, Var cursor_domain \<mapsto> {},
      Var temp \<mapsto> {}, Var done_rpc \<mapsto> {}, Var active_domain \<mapsto> {}, Var current_event_data \<mapsto> {}]"

abbreviation
  "\<S>'_InputSwitch \<equiv> ({Var indicated_domain, Var active_domain, Var current_hid_event_type,
       Var current_event_data, Var done_rpc, Var temp, Var cursor_domain,
       Var switch_state_mouse_down, Var overlay_result},
      {})"

abbreviation
  "P'_InputSwitch \<equiv> {EOp BEq (ELoad current_event_data) (EConst 0),
      EOp BEq (ELoad indicated_domain) (ELoad active_domain),
      EOp BEq (ELoad current_hid_event_type) (EConst 0)}"

(* Workspace for type system automation.
  Note that the inner tactic stops at each If-statement to prompt the user to confirm
  the pre and post-environment coming out of both branches.
  The user can use the TS.if_type' helper if they merely want the intersection of each branch's
  predicates, and must use the full TS.if_type rule if they want to customise the post-set.*)
lemma prog_InputSwitch_typed:
  (* Initially, we try proving a lemma that looks like this:
  \<exists>\<Gamma>' \<S>' P'. TS.has_type \<Gamma>\<^sub>0 \<S>\<^sub>0 P\<^sub>0 prog_InputSwitch \<Gamma>' \<S>' P'
     Once we have done this successfully, we can manually extract the typing context
     from the Isabelle output. *)
  "TS.has_type \<Gamma>\<^sub>0 \<S>\<^sub>0 P\<^sub>0 prog_InputSwitch \<Gamma>'_InputSwitch \<S>'_InputSwitch P'_InputSwitch"
  using NULL_EVENT_def CDDC_HID_MOUSE_EVENT_def CDDC_HID_KEYBOARD_EVENT_def
        DOMAIN_LOW_def DOMAIN_HIGH_def EVENT_TYPE_MOUSE_DOWN_def
  unfolding TS.\<Gamma>_of_mds_def TS.\<S>_of_mds_def RC.mds\<^sub>0_def
  apply(clarsimp simp:prog_InputSwitch_def)
  apply security_type_tac'+
   apply(rule_tac \<Gamma>'''="[Var overlay_result \<mapsto> {}, Var switch_state_mouse_down \<mapsto> {},
          Var cursor_domain \<mapsto> {}, Var done_rpc \<mapsto> {}, Var active_domain \<mapsto> {}, Var temp \<mapsto> {},
          (* This is the one that really matters *)
          Var current_event_data \<mapsto> {EOp BOr (EOp BNeq (ELoad current_hid_event_type) (EConst CDDC_HID_KEYBOARD_EVENT)) (EOp BNeq (ELoad indicated_domain) (EConst DOMAIN_HIGH))}]" and
          (* Note importantly we are also throwing away predicates that we don't need
            nor want to be forced to prove hold after the conditionals. *)
          P'''= "{EOp BEq (ELoad indicated_domain) (ELoad active_domain)}"
          in TS.if_type, rule TS.type_bexprI, simp+)
          (* Have to count off the number of steps before doing a manual subtype rewrite...
            Not quite sure of a nice way to deal with this yet. *)
          apply security_type_tac'
           apply security_type_tac'
          apply security_type_tac'
           apply security_type_tac'
          apply security_type_tac'
           apply security_type_tac'
          apply security_type_tac'
           apply security_type_tac'
          apply security_type_tac'
           apply security_type_tac'
          apply security_type_tac'
           apply security_type_tac'
          apply security_type_tac'
           apply security_type_tac'
          apply security_type_tac'
           apply security_type_tac'
          apply security_type_tac'
           apply security_type_tac'
          apply security_type_tac'
           (* Drop the predicate that done_rpc = 0 because we don't actually need it
             and we know the loop will be exiting with done_rpc = 1 *)
           apply(rule_tac \<Gamma>\<^sub>1="[Var overlay_result \<mapsto> {}, Var switch_state_mouse_down \<mapsto> {},
                  Var cursor_domain \<mapsto> {}, Var active_domain \<mapsto> {}, Var temp \<mapsto> {},
                  Var current_event_data \<mapsto> {}, Var done_rpc \<mapsto> {}]" and
                 P\<^sub>1="{EOp BNeq (ELoad temp) (EConst 0), EConst 1, EOp BEq (ELoad indicated_domain) (ELoad active_domain),
                      EOp BEq (ELoad current_hid_event_type) (EConst 1)}"
             in TS.sub', simp+, blast, blast, simp+)
           apply security_type_tac'+
            apply(rule TS.if_type', rule TS.type_bexprI, simp, clarsimp, security_type_tac'+)
          apply(rule TS.if_type', rule TS.type_bexprI, simp, clarsimp, security_type_tac'+)
           apply(rule TS.if_type', rule TS.type_bexprI, simp, clarsimp, security_type_tac'+)
          apply(rule TS.if_type', rule TS.type_bexprI, simp, clarsimp, security_type_tac'+)
            apply(rule TS.if_type', rule TS.type_bexprI, simp, clarsimp, security_type_tac'+)
            apply(rule TS.if_type', rule TS.type_bexprI, simp, clarsimp, security_type_tac'+)
           apply(rule TS.if_type', rule TS.type_bexprI, simp, clarsimp, security_type_tac'+)
             apply(rule TS.if_type', rule TS.type_bexprI, simp, clarsimp, security_type_tac'+)
            apply(rule TS.if_type', rule TS.type_bexprI, simp, clarsimp, security_type_tac'+)
          apply(rule TS.if_type', rule TS.type_bexprI, simp, clarsimp, security_type_tac'+)
   apply(rule_tac \<Gamma>'''="[Var overlay_result \<mapsto> {}, Var switch_state_mouse_down \<mapsto> {},
          Var cursor_domain \<mapsto> {}, Var active_domain \<mapsto> {},
          Var done_rpc \<mapsto> {}, Var temp \<mapsto> {},
          Var current_event_data \<mapsto> {EOp BOr (EOp BNeq (ELoad current_hid_event_type) (EConst CDDC_HID_KEYBOARD_EVENT)) (EOp BNeq (ELoad indicated_domain) (EConst 1))}]" and
         (* Note again we are aggressively dropping the rest of the predicates *)
         P'''= "{EOp BEq (ELoad indicated_domain) (ELoad active_domain)}"
         in TS.if_type, rule TS.type_bexprI, simp+)
          apply security_type_tac'+
           (* Recall that this branch is on indicated_domain to model the user's decision *)
           apply(rule_tac \<Gamma>'''="[Var overlay_result \<mapsto> {}, Var switch_state_mouse_down \<mapsto> {},
                  Var cursor_domain \<mapsto> {}, Var active_domain \<mapsto> {},
                  Var done_rpc \<mapsto> {}, Var temp \<mapsto> {},
                  Var current_event_data \<mapsto> {EOp BOr (EOp BNeq (ELoad current_hid_event_type) (EConst CDDC_HID_KEYBOARD_EVENT)) (EOp BNeq (ELoad indicated_domain) (EConst 1))}]" and
                 (* Drop just the predicate on the value of current_event_data that differs between branches *)
                 P'''= "{EOp BNeq (ELoad temp) (EConst 0),
                         EOp BEq (ELoad indicated_domain) (ELoad active_domain),
                         EOp BEq (ELoad current_hid_event_type) (EConst CDDC_HID_KEYBOARD_EVENT)}"
                 in TS.if_type, rule TS.type_bexprI, simp+)
                  apply security_type_tac'+
          apply(rule TS.if_type', rule TS.type_bexprI, simp, clarsimp, security_type_tac'+)
  done

lemma prog_InputSwitch_com_sifum_secure:
  "RC.WL.com_sifum_secure (prog_InputSwitch, RC.mds\<^sub>0)"
  apply(insert prog_InputSwitch_typed)
  apply(rule TS.typed_secure)
   apply(fastforce simp: TS.\<Gamma>_of_mds_def TS.\<S>_of_mds_def)
  unfolding RC.mds\<^sub>0_def
  apply(auto simp: TS.mds_yields_stable_types_def)
  done

lemma [simp]:
  "no_lock_mds (lock_acq_upd lock_interp switch_private_lock RC.mds\<^sub>0)"
  by (clarsimp simp:no_lock_mds_def lock_acq_upd_def RC.mds\<^sub>0_def image_def split:Mode.splits lock_addr.splits)

lemma [simp]:
  "no_lock_mds
     (lock_acq_upd lock_interp rpc_overlay_mouse_click_lock
       (lock_acq_upd lock_interp switch_private_lock RC.mds\<^sub>0))"
  by (clarsimp simp:no_lock_mds_def lock_acq_upd_def RC.mds\<^sub>0_def image_def split:Mode.splits lock_addr.splits)

lemma [simp]:
  "(lock_rel_upd lock_interp rpc_overlay_mouse_click_lock
     (lock_acq_upd lock_interp rpc_overlay_mouse_click_lock
       (lock_acq_upd lock_interp switch_private_lock RC.mds\<^sub>0))) =
   (lock_acq_upd lock_interp switch_private_lock RC.mds\<^sub>0)"
  unfolding RC.mds\<^sub>0_def
  apply(rule ext)
  apply(clarsimp simp:lock_acq_upd_def lock_rel_upd_def image_def split:Mode.splits lock_addr.splits)
  apply(rule set_eqI)
  apply clarsimp
  apply(case_tac x)
   apply blast
  by blast

lemma [simp]:
  "(lock_rel_upd lock_interp hid_read_atomicity_lock
     (lock_acq_upd lock_interp hid_read_atomicity_lock
       (lock_acq_upd lock_interp switch_private_lock RC.mds\<^sub>0))) =
   (lock_acq_upd lock_interp switch_private_lock RC.mds\<^sub>0)"
  unfolding RC.mds\<^sub>0_def
  apply(rule ext)
  apply(clarsimp simp:lock_acq_upd_def lock_rel_upd_def image_def split:Mode.splits lock_addr.splits)
  apply(rule set_eqI)
  apply clarsimp
  apply(case_tac x)
   apply blast
  by blast

lemma [simp]:
  "(lock_rel_upd lock_interp compositor_read_atomicity_lock
     (lock_acq_upd lock_interp compositor_read_atomicity_lock
       (lock_acq_upd lock_interp switch_private_lock RC.mds\<^sub>0))) =
   (lock_acq_upd lock_interp switch_private_lock RC.mds\<^sub>0)"
  unfolding RC.mds\<^sub>0_def
  apply(rule ext)
  apply(clarsimp simp:lock_acq_upd_def lock_rel_upd_def image_def split:Mode.splits lock_addr.splits)
  apply(rule set_eqI)
  apply clarsimp
  apply(case_tac x)
   apply blast
  by blast

(* Because the mode type system will be going away after merge with agrels, I don't even really
  care about this - just want to make it work. *)
method mode_type_tac' =
  match conclusion in "TM.mode_type RC.mds\<^sub>0
     (Stmt.LockAcq rpc_overlay_mouse_click_lock ;; ?something)
     RC.mds\<^sub>0" \<Rightarrow> "rule TM.seq" |
  rule TM.seq |
  rule TM.while |
  rule TM.skip |
  rule TM.if_ |
  rule TM.assign |
  simp |
  solves \<open>clarsimp simp:to_prog_var_set_def lock_acq_upd_def lock_rel_upd_def\<close> |
  solves \<open>clarsimp simp:to_prog_var_set_def lock_rel_upd_def split:Var.splits\<close> |
  solves \<open>clarsimp simp:to_prog_var_set_def lock_acq_upd_def lock_rel_upd_def RC.mds\<^sub>0_def split:Var.splits lock_addr.splits\<close> |
  solves \<open>force simp:lock_held_mds_correct_def to_prog_var_set_def lock_acq_upd_def lock_rel_upd_def image_def RC.mds\<^sub>0_def split:lock_addr.splits\<close> |
  solves \<open>force simp:lock_acq_upd_def lock_rel_upd_def image_def RC.mds\<^sub>0_def split:lock_addr.splits\<close> |
  solves \<open>(clarsimp simp:no_lock_mds_def lock_rel_upd_def lock_acq_upd_def RC.mds\<^sub>0_def image_def split:Mode.splits lock_addr.splits)\<close>

lemma prog_InputSwitch_mode_typed:
  "\<exists>mds. TM.mode_type RC.mds\<^sub>0 prog_InputSwitch mds"
  apply(intro exI)
  apply(simp add: prog_InputSwitch_def)
  apply mode_type_tac'+
   apply(rule TM.lock_acq, mode_type_tac'+)
   apply(rule TM.lock_acq, mode_type_tac'+)
   apply(rule TM.lock_rel, mode_type_tac'+)
     apply(rule TM.lock_acq, mode_type_tac'+)
     apply(rule TM.lock_rel, mode_type_tac'+)
     apply(rule TM.lock_acq, mode_type_tac'+)
     apply(rule TM.lock_rel, mode_type_tac'+)
      apply(rule TM.lock_acq, mode_type_tac'+)
     apply(rule TM.lock_rel, mode_type_tac'+)
      apply(rule TM.lock_acq, mode_type_tac'+)
      apply(rule TM.lock_rel, mode_type_tac'+)
   apply(rule TM.lock_acq, mode_type_tac'+)
   apply(rule TM.lock_rel, mode_type_tac'+)
       apply(rule TM.lock_acq, mode_type_tac'+)
      apply(rule TM.lock_rel, mode_type_tac'+)
      apply(rule TM.lock_acq, mode_type_tac'+)
     apply(rule TM.lock_rel, mode_type_tac'+)
  done

lemma prog_InputSwitch_locally_sound_mode_use:
  "RC.WL.locally_sound_mode_use \<langle>prog_InputSwitch, RC.mds\<^sub>0, mem\<rangle>\<^sub>A"
  using prog_InputSwitch_mode_typed TM.mode_type_sound
  by blast

(* Parallel composition of the CDDC components *)

(* Proof of globally sound mode use *)
thm RC.WL.management_requirements_ensure_global_soundness

definition init_cms_CDDC
where
"init_cms_CDDC \<equiv> [(prog_OverlayDriver, RC.mds\<^sub>0),
                   (prog_InputSwitch, RC.mds\<^sub>0)]"

lemma CDDC_no_lock_mds_gc:
  "RC.WL.no_lock_mds_gc (init_cms_CDDC, mem)"
  unfolding init_cms_CDDC_def RC.WL.no_lock_mds_gc_def RC.WL.modes_of_gc_def no_lock_mds_def
    RC.mds\<^sub>0_def
  by (clarsimp simp:image_def split:Mode.splits)

(* Fine provided the initial memory has no locks held in it *)
lemma CDDC_lock_managed_vars_mds_mem_correct:
  "WL.no_locks_acquired mem \<Longrightarrow> RC.WL.lock_managed_vars_mds_mem_correct (init_cms_CDDC, mem)"
  unfolding RC.WL.lock_managed_vars_mds_mem_correct_def WL.no_locks_acquired_def
  apply clarsimp
  unfolding lock_not_held_mds_correct_def RC.WL.modes_of_gc_def init_cms_CDDC_def
    RC.mds\<^sub>0_def lock_interp_def
  apply safe
     apply(case_tac i)
      apply(clarsimp simp:image_def split:lock_addr.splits)
        apply blast
       apply blast
      apply blast
     apply(clarsimp simp:image_def split:lock_addr.splits)
       apply blast
      apply blast
     apply blast
    apply(case_tac i)
     apply force
    apply force
   apply(case_tac i)
    apply(clarsimp simp:image_def split:lock_addr.splits)
    apply blast
   apply(clarsimp simp:image_def split:lock_addr.splits)
   apply blast
  by (case_tac i, simp+)

lemma CDDC_unmanaged_var_modes_compatible:
  "RC.WL.unmanaged_var_modes_compatible (init_cms_CDDC, mem)"
  unfolding RC.WL.unmanaged_var_modes_compatible_def
  apply clarsimp
  unfolding lock_not_held_mds_correct_def RC.WL.modes_of_gc_def init_cms_CDDC_def
    RC.mds\<^sub>0_def lock_interp_def
    RC.WL.NoRW_lock_managed_vars_def
  apply clarsimp
  by (metis Mode.simps(13,14) empty_iff less_Suc0 less_SucE nth_Cons_0 nth_Cons_Suc)

(* Thus we know, provided that no locks are held in the initial memory, that
   the CDDC system exhibits globally sound mode use *)
lemma CDDC_globally_sound_mode_use:
  "WL.no_locks_acquired mem \<Longrightarrow> RC.WL.globally_sound_mode_use (init_cms_CDDC, mem)"
  using RC.WL.management_requirements_ensure_global_soundness
    CDDC_no_lock_mds_gc CDDC_lock_managed_vars_mds_mem_correct CDDC_unmanaged_var_modes_compatible
  by simp

lemma CDDC_sound_mode_use:
  "WL.no_locks_acquired mem \<Longrightarrow> RC.WL.sound_mode_use (init_cms_CDDC, mem)"
  using CDDC_globally_sound_mode_use
  using prog_InputSwitch_locally_sound_mode_use prog_OverlayDriver_locally_sound_mode_use
  by (simp add: init_cms_CDDC_def)

lemma CDDC_sifum_compositionality_cont:
  "RC.WL.prog_sifum_secure_cont init_cms_CDDC"
  apply(rule RC.WL.sifum_compositionality_cont)
   apply(force simp: init_cms_CDDC_def prog_OverlayDriver_com_sifum_secure prog_InputSwitch_com_sifum_secure)
  apply(rule allI)
  using CDDC_sound_mode_use by blast

end
