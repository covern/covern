(*
Authors: Robert Sison, Toby Murray, Edward Pierzchalski
Based on the Dependent_SIFUM_Type_Systems AFP entry, whose authors are:
 Toby Murray, Robert Sison, Edward Pierzchalski, Christine Rizkallah
in turn based on the SIFUM_Type_Systems AFP entry, whose authors are:
 Sylvia Grewe, Heiko Mantel, Daniel Schoepe
*)
section {* Language for Instantiating the Dependent SIFUM-Security Property *}

theory LockLanguage
imports Main "../rg-sec/Security"
begin

subsection {* Syntax *}

type_synonym 'var Lock = "('var set) \<times> ('var set)"

datatype ('var, 'lock, 'aexp, 'bexp) Stmt = Assign "'var" "'aexp" (infix "\<leftarrow>" 130)
  | Skip
  | LockAcq "'lock"
  | LockRel "'lock"
  | Seq "('var, 'lock, 'aexp, 'bexp) Stmt" "('var, 'lock, 'aexp, 'bexp) Stmt" (infixr ";;" 150)
  | If "'bexp" "('var, 'lock, 'aexp, 'bexp) Stmt" "('var, 'lock, 'aexp, 'bexp) Stmt"
  | While "'bexp" "('var, 'lock, 'aexp, 'bexp) Stmt"
  | Stop


text {*
  Variables are now either locks or ordinary program variables.
*}
datatype ('lock, 'var) Var = Lock 'lock | ProgVar 'var

text {*
  Convert a full memory with locks into one with just program variables. We use this
  simpler type of memory to evaluate the simple program semantics.
*}
definition
  to_prog_mem :: "(('lock,'var) Var,'val) Mem \<Rightarrow> ('var,'val) Mem"
where
  "to_prog_mem m \<equiv> \<lambda>v. m (ProgVar v)"

definition
  to_lock_mem :: "(('lock,'var) Var,'val) Mem \<Rightarrow> ('lock,'val) Mem"
where
  "to_lock_mem m \<equiv> \<lambda>l. m (Lock l)"

definition
  to_full_mem :: "('var,'val) Mem \<Rightarrow> ('lock,'val) Mem \<Rightarrow> (('lock,'var) Var, 'val) Mem"
where
  "to_full_mem vm lm \<equiv> \<lambda>v. case v of (ProgVar p) \<Rightarrow> vm p | (Lock l) \<Rightarrow> lm l"

lemma fullmemI:
  "\<lbrakk>to_prog_mem m = to_prog_mem m'; to_lock_mem m = to_lock_mem m'\<rbrakk> \<Longrightarrow> m = m'"
  using to_prog_mem_def to_lock_mem_def
  by (metis Var.exhaust ext)

lemma fullmemD:
  "m = m' \<Longrightarrow> to_prog_mem m = to_prog_mem m' \<and> to_lock_mem m = to_lock_mem m'"
  by metis

definition
  to_prog_var_set :: "('lock,'var) Var set \<Rightarrow> 'var set"
where
  "to_prog_var_set vs \<equiv> {v. ProgVar v \<in> vs}"

definition
  to_prog_mds :: "('lock,'var) Var Mds \<Rightarrow> 'var Mds"
where
  "to_prog_mds mds \<equiv> \<lambda> m. {v. ProgVar v \<in> mds m}"

text {* Logical Predicates *}
(* NOTE: this stuff is here in LockLanguage so we can associate these to locks *)

(* Logical expressions: we will need transformers from arithmetic
   expressions into these. For instance we will have to be able to take an arithmetic
   expresssion and replace all occurrences of some program variable with a fresh logical
   variable. That means being able to turn the arith expression into one of these. Hence
   we provide arbitrary pluggable binary operations. Might add unary ones in future too.
*)
datatype ('var,'val) lexp = Var 'var |  Const 'val | 
                            LBinOp "('val \<Rightarrow> 'val \<Rightarrow> 'val)" "('var,'val) lexp" "('var,'val) lexp"

primrec
  leval :: "('var,'val) Mem \<Rightarrow> ('var,'val) lexp \<Rightarrow> 'val"
where
  "leval mem (Var var) = (mem var)" |
  "leval mem (Const c) = c" |
  "leval mem (LBinOp bop e f) = (bop (leval mem e) (leval mem f))"

primrec
  lexp_vars :: "('var,'val) lexp \<Rightarrow> 'var set"
where
  "lexp_vars (Var v) = {v}" |
  "lexp_vars (Const c) = {}" |
  "lexp_vars (LBinOp bop e f) = (lexp_vars e \<union> lexp_vars f)"

(* logical predicates: we will need transformers from boolean expressions to these for the same
   reasons as above for arithmetic expressions and logical ones *)
datatype ('var,'val) lpred = 
   PTrue | PFalse | 
   PDisj "('var,'val) lpred" "('var,'val) lpred" | 
   PConj "('var,'val) lpred" "('var,'val) lpred" | 
   PNeg "('var,'val) lpred" | 
   PImp "('var,'val) lpred" "('var,'val) lpred" |
   PCmp "('val \<Rightarrow> 'val \<Rightarrow> bool)" "('var,'val) lexp" "('var,'val) lexp" |
   (* for now try using HOAS to represent existentials *)
   PEx "('val \<Rightarrow> ('var,'val) lpred)" |
   (* not sure if we actually need universal quantification but why not? *)
   PAll "('val \<Rightarrow> ('var,'val) lpred)"

text {* How to evaluate logical predicates. 
*}
primrec 
  lpred_eval :: "('var,'val) Mem \<Rightarrow> ('var,'val) lpred  \<Rightarrow> bool"
where
  "lpred_eval mem PTrue = True" |
  "lpred_eval mem PFalse = False" |
  "lpred_eval mem (PDisj p q) = ((lpred_eval mem p) \<or> (lpred_eval mem q))" |
  "lpred_eval mem (PConj p q) = ((lpred_eval mem p) \<and> (lpred_eval mem q))" |
  "lpred_eval mem (PNeg p) = (\<not> (lpred_eval mem p))" |
  "lpred_eval mem (PImp p q) = ((lpred_eval mem p) \<longrightarrow> (lpred_eval mem q))" |
  "lpred_eval mem (PCmp cmp e f) = (cmp (leval mem e) (leval mem f))" |
  "lpred_eval mem (PEx fe) = (\<exists>v. lpred_eval mem (fe v))" |
  "lpred_eval mem (PAll fe) = (\<forall>v. lpred_eval mem (fe v))"

primrec
  lpred_vars :: "('var,'val) lpred \<Rightarrow> 'var set"
where
  "lpred_vars PTrue = {}" |
  "lpred_vars PFalse = {}"  |
  "lpred_vars (PNeg p) = lpred_vars p" |
  "lpred_vars (PImp p q) = (lpred_vars p \<union> lpred_vars q)" |
  "lpred_vars (PConj p q) = (lpred_vars p \<union> lpred_vars q)" |
  "lpred_vars (PDisj p q) = (lpred_vars p \<union> lpred_vars q)" |
  "lpred_vars (PCmp cmp e f) = (lexp_vars e \<union> lexp_vars f)" |
  "lpred_vars (PEx fe) = (\<Union>v. lpred_vars (fe v))" | (* will this be a nightmare? to reason about *)
  "lpred_vars (PAll fe) = (\<Union>v. lpred_vars (fe v))" 

text {* 
  A few quick tests.
*}
notepad begin
  fix v :: 'var
  fix mem :: "('var,'val) Mem"

have blah:
  "\<And>P. (\<Union>x. P) = P"
  apply(subst Complete_Lattices.UN_constant)
  by simp

have
  "lpred_vars (PEx (\<lambda>x. (PCmp (=) (Var v) (Const x)))) = {v}"
  apply (subst lpred_vars.simps)
  apply (subst lpred_vars.simps)
  apply (subst lexp_vars.simps)
  apply (subst lexp_vars.simps)
  (* for well behaved PEx expressions any potential harm caused by the \<Union> seems not to arise *)
  by simp

have
  "lpred_eval mem (PEx (\<lambda>x. (PCmp (=) (Var v) (Const x))))"
  by simp

have
  "lpred_eval mem (PAll (\<lambda>x. (PDisj (PCmp (=) (Var v) (Const x)) (PNeg (PCmp (=) (Var v) (Const x))))))"
  by simp

end

locale sifum_lang_no_dma =
  fixes eval\<^sub>A :: "('Var, 'Val) Mem \<Rightarrow> 'AExp \<Rightarrow> 'Val"
  fixes eval\<^sub>B :: "('Var, 'Val) Mem \<Rightarrow> 'BExp \<Rightarrow> bool"
  fixes eval\<^sub>L :: "(('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> 'Lock \<Rightarrow> bool"
  fixes aexp_vars :: "'AExp \<Rightarrow> 'Var set"
  fixes bexp_vars :: "'BExp \<Rightarrow> 'Var set"
  fixes lock_interp :: "'Lock \<Rightarrow> ('Var set \<times> 'Var set)" (* (noW,noRW) = lock_interp l *)
  fixes lock_inv :: "'Lock \<Rightarrow> ('Var,'Val) lpred"
  fixes lock_acq_sem :: "'Lock \<Rightarrow> 'Val \<Rightarrow> 'Val"
  fixes lock_rel_sem :: "'Lock \<Rightarrow> 'Val \<Rightarrow> 'Val"
  fixes aexp_to_lexp :: "'AExp \<Rightarrow> ('Var,'Val) lexp"
  fixes bexp_to_lpred :: "'BExp \<Rightarrow> ('Var,'Val) lpred"
  assumes lock_invs_sat: "\<And>l. \<exists> (mem::('Var,'Val) Mem). lpred_eval mem (lock_inv l)"
  assumes Var_finite : "finite {(x :: ('Lock, 'Var) Var). True}"
  assumes eval_vars_det\<^sub>A : "\<lbrakk> \<forall> x \<in> aexp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<rbrakk> \<Longrightarrow> eval\<^sub>A mem\<^sub>1 e = eval\<^sub>A mem\<^sub>2 e"
  assumes eval_vars_det\<^sub>B : "\<lbrakk> \<forall> x \<in> bexp_vars b. mem\<^sub>1 x = mem\<^sub>2 x \<rbrakk> \<Longrightarrow> eval\<^sub>B mem\<^sub>1 b = eval\<^sub>B mem\<^sub>2 b"
  assumes eval_det\<^sub>L : "\<lbrakk> mem (Lock l) = mem' (Lock l) \<rbrakk> \<Longrightarrow> eval\<^sub>L mem l = eval\<^sub>L mem' l"
  assumes locks_can_be_taken : "\<forall> mem l. eval\<^sub>L (mem(Lock l := lock_acq_sem l (mem (Lock l)))) l"
  assumes locks_can_be_released : "\<forall> mem l. \<not> eval\<^sub>L (mem(Lock l := lock_rel_sem l (mem (Lock l)))) l"
  (* invariants associated with locks can only mention variables controlled by the lock *)
  assumes lock_invs: "lock_interp l = (noW,noRW) \<Longrightarrow> lpred_vars (lock_inv l) \<subseteq> noW \<union> noRW" 
  assumes lone_lock_per_var: "\<forall> v l. v \<in> fst (lock_interp l) \<union> snd (lock_interp l) \<longrightarrow>
                       (\<forall> l'.  v \<in> fst (lock_interp l') \<union> snd (lock_interp l') \<longrightarrow> l' = l)"
  assumes lock_interp_no_overlap: "\<forall>l. fst (lock_interp l) \<inter> snd (lock_interp l) = {}"
  assumes aexp_to_lexp_correct: "\<And>e mem. leval mem (aexp_to_lexp e) = eval\<^sub>A mem e"
  assumes bexp_to_lpred_correct: "\<And>e mem. lpred_eval mem (bexp_to_lpred e) = eval\<^sub>B mem e"
  assumes aexp_to_lexp_vars: "\<And>e. lexp_vars (aexp_to_lexp e) = aexp_vars e"
  assumes bexp_to_lpred_vars: "\<And>e. lpred_vars (bexp_to_lpred e) = bexp_vars e"

context sifum_lang_no_dma
begin

definition
  no_locks_acquired :: "(('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> bool"
where
  "no_locks_acquired mem \<equiv> \<forall>l. \<not> eval\<^sub>L mem l"

(* To make the examples look a bit nicer in the PDF. *)

notation (latex output)
  Seq ("_; _" 60)

notation (Rule output)
  Seq ("_ ; _" 60)

notation (Rule output)
  If ("if _ then _ else _ fi" 50)

notation (Rule output)
  While ("while _ do _ done")

abbreviation conf\<^sub>w_abv :: "('Var, 'Lock, 'AExp, 'BExp) Stmt \<Rightarrow>
  ((('Lock, 'Var) Var,'Val) Env) \<Rightarrow> (('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> (_,_,_) LocalConf"
  ("\<langle>_, _, _\<rangle>\<^sub>w" [0, 120, 120] 100)
  where
  "\<langle> c, mds, mem \<rangle>\<^sub>w \<equiv> ((c, mds), mem)"

subsection {* Semantics *}

inductive_set eval\<^sub>w_simple :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt \<times> ('Var, 'Val) Mem) rel"
and eval\<^sub>w_simple_abv :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt \<times> ('Var, 'Val) Mem) \<Rightarrow>
  ('Var, 'Lock, 'AExp, 'BExp) Stmt \<times> ('Var, 'Val) Mem \<Rightarrow> bool"
  (infixr "\<leadsto>\<^sub>s" 60)
  where
  "c \<leadsto>\<^sub>s c' \<equiv> (c, c') \<in> eval\<^sub>w_simple" |
  assign: "((x \<leftarrow> e, mem), (Stop, mem (x := eval\<^sub>A mem e))) \<in> eval\<^sub>w_simple" |
  seq_stop: "((Seq Stop c,mem), (c,mem)) \<in> eval\<^sub>w_simple" |
  skip: "((Skip, mem), (Stop, mem)) \<in> eval\<^sub>w_simple" |
  if_true: "\<lbrakk> eval\<^sub>B mem b \<rbrakk> \<Longrightarrow> ((If b t e, mem), (t, mem)) \<in> eval\<^sub>w_simple" |
  if_false: "\<lbrakk> \<not> eval\<^sub>B mem b \<rbrakk> \<Longrightarrow> ((If b t e, mem), (e, mem)) \<in> eval\<^sub>w_simple" |
  while: "((While b c, mem), (If b (c ;; While b c) Stop, mem)) \<in> eval\<^sub>w_simple"

lemma eval\<^sub>w_simple_det:
  "(c,mem) \<leadsto>\<^sub>s (c',mem') \<Longrightarrow> (c,mem) \<leadsto>\<^sub>s (c'',mem'') \<Longrightarrow> c' = c'' \<and> mem' = mem''"
  by(induct arbitrary: c'' mem'' rule: eval\<^sub>w_simple.induct, auto elim: eval\<^sub>w_simple.cases)

lemma cond:
  "((If b t e, mem), (if eval\<^sub>B mem b then t else e, mem)) \<in> eval\<^sub>w_simple"
  apply(case_tac "eval\<^sub>B mem b")
   apply(auto intro: eval\<^sub>w_simple.intros)
  done



definition lock_acq_mds_upd :: "'Lock \<Rightarrow> ('Lock, 'Var) Var Mds \<Rightarrow> ('Lock, 'Var) Var Mds" where
  "lock_acq_mds_upd l mds \<equiv> case (lock_interp l) of (NoW_vars, NoRW_vars) \<Rightarrow> 
    \<lambda> m. case m of 
      GuarNoWrite \<Rightarrow> mds GuarNoWrite - (ProgVar ` NoW_vars) |
      AsmNoWrite \<Rightarrow> mds AsmNoWrite \<union> ProgVar ` NoW_vars |
      GuarNoReadOrWrite \<Rightarrow> mds GuarNoReadOrWrite - ProgVar ` NoRW_vars |
      AsmNoReadOrWrite \<Rightarrow> mds AsmNoReadOrWrite \<union> ProgVar ` NoRW_vars "

definition lock_rel_mds_upd :: "'Lock \<Rightarrow> ('Lock, 'Var) Var Mds \<Rightarrow> ('Lock, 'Var) Var Mds" where
  "lock_rel_mds_upd l mds \<equiv> case (lock_interp l) of (NoW_vars, NoRW_vars) \<Rightarrow> 
    \<lambda> m. case m of 
      GuarNoWrite \<Rightarrow> mds GuarNoWrite \<union> ProgVar ` NoW_vars |
      AsmNoWrite \<Rightarrow> mds AsmNoWrite - (ProgVar ` NoW_vars) |
      GuarNoReadOrWrite \<Rightarrow> mds GuarNoReadOrWrite \<union> ProgVar ` NoRW_vars |
      AsmNoReadOrWrite \<Rightarrow> mds AsmNoReadOrWrite - ProgVar ` NoRW_vars "

(* Note: design decision for now. Have the language keep the set of rely-guarantee conditions
   almost-constant. In particular, we instantiate with preservation of a global invariant that 
   simply says that all the lock_interp invariants hold for all locks currently not-held, as per
   concurrent separation logic. We also need a dymanic part saying how we guarantee not to
   release locks we don't currently hold and how we assume nobody will release a lock that we
   are holding. Thus lock acquisition acquires assumptions and releases guarantees of this form,
   and lock releasing releases and acquires the same assumptions and guarantees respectively. *)
definition
  lock_no_release_rel :: "'Lock \<Rightarrow> (('Lock,'Var) Var,'Val) Mem rel"
where
  "lock_no_release_rel l \<equiv> {(mem,mem'). eval\<^sub>L mem l \<longrightarrow> eval\<^sub>L mem' l}"

definition
  lock_acq_agrels_upd :: "'Lock \<Rightarrow> (('Lock, 'Var) Var, 'Val) AGRels \<Rightarrow> (('Lock, 'Var) Var, 'Val) AGRels"
where
  "lock_acq_agrels_upd l agrels \<equiv> 
   \<lambda>m. case m of AsmRel \<Rightarrow> insert (lock_no_release_rel l) (agrels AsmRel) |
                 GuarRel \<Rightarrow> (agrels GuarRel) - {lock_no_release_rel l}"

definition
  lock_rel_agrels_upd :: "'Lock \<Rightarrow> (('Lock, 'Var) Var, 'Val) AGRels \<Rightarrow> (('Lock, 'Var) Var, 'Val) AGRels"
where
  "lock_rel_agrels_upd l agrels \<equiv> 
   \<lambda>m. case m of AsmRel \<Rightarrow> (agrels AsmRel) - {lock_no_release_rel l} |
                 GuarRel \<Rightarrow> insert (lock_no_release_rel l) (agrels GuarRel)"




inductive_set eval\<^sub>w :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf rel"
and eval\<^sub>w_abv :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
                  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow> bool"
  (infixr "\<leadsto>\<^sub>w" 60)
where
  "c \<leadsto>\<^sub>w c' \<equiv> (c, c') \<in> eval\<^sub>w" |
  unannotated: "\<lbrakk> (c, pmem) \<leadsto>\<^sub>s (c', pmem') ; pmem = to_prog_mem mem;
                  pmem' = to_prog_mem mem'; to_lock_mem mem = to_lock_mem mem'\<rbrakk>
    \<Longrightarrow> (\<langle>c, env, mem\<rangle>\<^sub>w, \<langle>c', env, mem'\<rangle>\<^sub>w) \<in> eval\<^sub>w" |
  seq: "\<lbrakk> \<langle>c\<^sub>1, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c\<^sub>1', env', mem'\<rangle>\<^sub>w \<rbrakk> \<Longrightarrow> (\<langle>(c\<^sub>1 ;; c\<^sub>2), env, mem\<rangle>\<^sub>w, \<langle>(c\<^sub>1' ;; c\<^sub>2), env', mem'\<rangle>\<^sub>w) \<in> eval\<^sub>w" 
 |
 lock_wait:
    "\<lbrakk> eval\<^sub>L mem l; env' = env; mem' = mem \<rbrakk> \<Longrightarrow>
     (\<langle>(LockAcq l), env, mem\<rangle>\<^sub>w, \<langle>(LockAcq l), env', mem'\<rangle>\<^sub>w) \<in> eval\<^sub>w"
 |
 lock_acq: 
    "\<lbrakk> \<not> eval\<^sub>L mem l; 
      mem' = mem(Lock l := lock_acq_sem l (mem(Lock l)));
      mds' = lock_acq_mds_upd l (fst env); agrels' = lock_acq_agrels_upd l (snd env)\<rbrakk> \<Longrightarrow>
     (\<langle>(LockAcq l), env, mem\<rangle>\<^sub>w, \<langle>Stop, (mds',agrels'), mem'\<rangle>\<^sub>w) \<in> eval\<^sub>w"
 |
 lock_rel: 
   "\<lbrakk> mem' = mem(Lock l := lock_rel_sem l (mem(Lock l)));
      mds' = lock_rel_mds_upd l (fst env); agrels' = lock_rel_agrels_upd l (snd env)\<rbrakk> \<Longrightarrow>
      (\<langle>(LockRel l), env, mem\<rangle>\<^sub>w, \<langle>Stop, (mds',agrels'), mem'\<rangle>\<^sub>w) \<in> eval\<^sub>w"

inductive_cases stop_no_eval' [dest]: "((Stop, mem), (c', mem')) \<in> eval\<^sub>w_simple"

lemma stop_no_eval: "(\<langle>Stop, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w) \<Longrightarrow> False"
  apply(erule eval\<^sub>w.cases)
  by auto

lemma eval\<^sub>w_det:
  "\<langle>c,env,mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c',env',mem'\<rangle>\<^sub>w \<Longrightarrow> \<langle>c,env,mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c'',env'',mem''\<rangle>\<^sub>w \<Longrightarrow>
   c' = c'' \<and> env' = env'' \<and> mem' = mem''"
proof(induct arbitrary: c'' env'' mem'' rule: eval\<^sub>w.induct)
  case (unannotated c pmem c' pmem' mem mem' env)
  from `\<langle>c, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c'', env'', mem''\<rangle>\<^sub>w`
       `(c, pmem) \<leadsto>\<^sub>s (c', pmem')` obtain pmem'' where
    "(c, pmem) \<leadsto>\<^sub>s (c'', pmem'')" "to_lock_mem mem'' = to_lock_mem mem"
    "to_prog_mem mem'' = pmem''" "env'' = env"
      apply -
      apply(erule eval\<^sub>w.cases)    
          apply (clarsimp simp: unannotated eval\<^sub>w_simple_det)
         apply(auto elim!: eval\<^sub>w_simple.cases elim: eval\<^sub>w.cases)
      done
    with unannotated show ?case 
      apply -
      apply(drule (1) eval\<^sub>w_simple_det)
      using unannotated by (simp add: fullmemI)
next
  case (seq c1 env mem c1' env' mem' c2)
  from `\<langle>c1 ;; c2, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c'', env'', mem''\<rangle>\<^sub>w`
        ` \<langle>c1, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c1', env', mem'\<rangle>\<^sub>w` show ?case
    apply -
    apply(erule eval\<^sub>w.cases)
        apply(erule eval\<^sub>w_simple.cases)
             apply simp
            apply clarsimp
            apply(auto elim: eval\<^sub>w_simple.cases dest: stop_no_eval)[5]
        apply(blast dest: seq(2))
       by auto
next
  case (lock_acq) thus ?case
  by (auto elim!: eval\<^sub>w.cases eval\<^sub>w_simple.cases)
next
  case (lock_wait) thus ?case
  by (auto elim!: eval\<^sub>w.cases eval\<^sub>w_simple.cases)
next
  case (lock_rel) thus ?case
  by (auto elim!: eval\<^sub>w.cases eval\<^sub>w_simple.cases)
qed


text {*
  The trivial language semantics above and associations of invariants to locks induces the
  following global invariant. 
*}
definition
  global_invariant :: "(('Lock,'Var) Var,'Val) Mem \<Rightarrow> bool"
where
  "global_invariant mem \<equiv> \<forall> l. \<not> eval\<^sub>L mem l \<longrightarrow> lpred_eval  (to_prog_mem mem) (lock_inv l)"

definition
  INIT :: "(('Lock,'Var) Var,'Val) Mem \<Rightarrow> bool"
where
  "INIT mem \<equiv> no_locks_acquired mem \<and> global_invariant mem"

text {*
  This is the global rely and guarantee condition of all threads on all others, all the time.
*}
definition
  agrels\<^sub>0 :: "(('Lock,'Var) Var,'Val) AGRels"
where
  "agrels\<^sub>0 \<equiv> \<lambda>x. case x of AsmRel \<Rightarrow> {{(m,m'). global_invariant m \<longrightarrow> global_invariant m'}} |
                           GuarRel \<Rightarrow> {{(m,m'). global_invariant m \<longrightarrow> global_invariant m'}} \<union>
                                      (\<Union>l. {lock_no_release_rel l})"

text {*
  If we use this global condition everywhere, then we get compatibility straightaway.
*}
lemma agrels\<^sub>0_compatible:
  "agrels = replicate n agrels\<^sub>0 \<Longrightarrow>
  \<forall>i<length agrels. \<forall>R\<in>(agrels ! i) AsmRel. \<forall>j<length agrels. j \<noteq> i \<longrightarrow> \<Inter>(agrels ! j) GuarRel \<subseteq> R"
  by(simp add: agrels\<^sub>0_def)


abbreviation eval\<^sub>w_plus :: "
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow> bool" ("_ \<leadsto>\<^sub>w\<^sup>+ _") where
"ctx \<leadsto>\<^sub>w\<^sup>+ ctx' \<equiv> (ctx, ctx') \<in> eval\<^sub>w\<^sup>+"

  
subsection {* Semantic Properties *}

text {* The following lemmas simplify working with evaluation contexts
  in the soundness proofs for the type system(s). *}

inductive_cases eval_elim: "(((c, mds), mem), ((c', mds'), mem')) \<in> eval\<^sub>w"
inductive_cases assign_elim' [elim]: "((x \<leftarrow> e, mem), (c', mem')) \<in> eval\<^sub>w_simple"
inductive_cases skip_elim' [elim]: "(Skip, mem) \<leadsto>\<^sub>s (c', mem')"

lemma skipD [dest]:
  "\<langle>Skip, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w \<Longrightarrow> c' = Stop \<and> mds = mds' \<and> mem = mem'"
  apply (erule eval_elim)
     apply (metis (lifting)   skip_elim' fullmemI)
    apply (metis Stmt.simps)
   apply (metis Stmt.simps)
  by (metis Stmt.simps)+

lemma assignD [dest]:
  "\<langle>x \<leftarrow> e, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w \<Longrightarrow> c' = Stop \<and> mds = mds' \<and> mem' = mem (ProgVar x := eval\<^sub>A (to_prog_mem mem) e)"
  apply (erule eval_elim)
      apply safe[1]
       apply force
      apply(rule fullmemI)
       apply(fastforce simp: to_prog_mem_def to_lock_mem_def)+
  done

inductive_cases if_elim' [elim!]: "(If b p q, mem) \<leadsto>\<^sub>s (c', mem')"

lemma ifD [dest]:
  "\<And> P.
    \<lbrakk> \<langle>If b p q, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w ;
     \<lbrakk> c' = p; mem' = mem ; mds' = mds ; eval\<^sub>B (to_prog_mem mem) b \<rbrakk> \<Longrightarrow> P ;
     \<lbrakk> c' = q; mem' = mem ; mds' = mds ; \<not> eval\<^sub>B (to_prog_mem mem) b \<rbrakk> \<Longrightarrow> P \<rbrakk> \<Longrightarrow> P"
  apply (erule eval_elim)
     apply (metis (no_types) if_elim' fullmemI)
    apply (metis Stmt.simps)
   apply (metis Stmt.simps)+
  done

inductive_cases while_elim' [elim!]: "(While e c, mem) \<leadsto>\<^sub>s (c', mem')"

lemma whileD:
  "\<lbrakk> \<langle>While e c, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w \<rbrakk> \<Longrightarrow> c' = If e (c ;; While e c) Stop \<and> mds' = mds \<and> mem' = mem"
  apply (erule eval_elim)
     apply (metis (no_types) while_elim' fullmemI)
    apply (metis Stmt.simps)
   apply (metis (lifting) Stmt.simps)+
  done

inductive_cases lock_acq_elim' [elim]: "(LockAcq l, mem) \<leadsto>\<^sub>s (c', mem')"

lemma lock_acqD [dest]:
  "\<langle>LockAcq l, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', env', mem'\<rangle>\<^sub>w \<Longrightarrow> (env' = env \<and> mem' = mem \<and> c' = LockAcq l \<and> eval\<^sub>L mem l) \<or> (
   c' = Stop \<and>
   \<not> eval\<^sub>L mem l \<and> eval\<^sub>L mem' l \<and> (mem' = mem(Lock l := lock_acq_sem l (mem (Lock l)))) \<and>
   env' = (lock_acq_mds_upd l (fst env),lock_acq_agrels_upd l (snd env)))"
  apply (erule eval_elim)
      apply (metis (no_types) Stmt.simps lock_acq_elim')
     apply(metis Stmt.simps)
    apply clarsimp
   using locks_can_be_taken
   using lock_acq_mds_upd_def 
   apply (metis (mono_tags, lifting) Stmt.inject(2))
  using lock_rel_mds_upd_def
  by (metis Stmt.simps(34))

inductive_cases lock_rel_elim' [elim]: "(LockRel l, mem) \<leadsto>\<^sub>s (c', mem')"

lemma lock_relD [dest]:
  "\<langle>LockRel l, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', env', mem'\<rangle>\<^sub>w \<Longrightarrow>
   c' = Stop \<and>
   \<not> eval\<^sub>L mem' l \<and> mem' = (mem(Lock l := lock_rel_sem l (mem (Lock l)))) \<and>
   env' = (lock_rel_mds_upd l (fst env), lock_rel_agrels_upd l (snd env))"
  apply (erule eval_elim)
      apply (metis (no_types) Stmt.simps lock_rel_elim')
     apply(metis Stmt.simps)
    apply (metis Stmt.distinct(27))
   apply (metis Stmt.distinct(27))
  using locks_can_be_released
  by (metis Stmt.inject(3)) (* slow *)
   
inductive_cases seq_elim' [elim]: "(c\<^sub>1 ;; c\<^sub>2, mem) \<leadsto>\<^sub>s (c', mem')"

lemma seq_stopD[dest]:
  "\<langle>Stop ;; c, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w \<Longrightarrow> c' = c \<and> mds' = mds \<and> mem' = mem"
  apply (erule eval_elim)
     apply clarify
     apply (metis (no_types) seq_elim' stop_no_eval' fullmemI)
    apply (metis Stmt.inject stop_no_eval)
   apply (metis Stmt.distinct)+
  done

(* kept for backwards compatibility *)
lemmas lock_acq_eval\<^sub>w = lock_acq
lemmas lock_wait_eval\<^sub>w = lock_wait
lemmas lock_rel_eval\<^sub>w = lock_rel

lemmas seq_stop_eval\<^sub>w = unannotated[OF seq_stop]
lemmas assign_eval\<^sub>w = unannotated[OF assign]
lemmas if_eval\<^sub>w = unannotated[OF cond]
lemmas if_false_eval\<^sub>w = unannotated[OF if_false]
lemmas skip_eval\<^sub>w = unannotated[OF skip]
lemmas while_eval\<^sub>w = unannotated[OF while]

lemma seqD [dest]:
  "\<lbrakk> \<langle>c\<^sub>1 ;; c\<^sub>2, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w ; c\<^sub>1 \<noteq> Stop \<rbrakk> \<Longrightarrow>
  (\<exists> c\<^sub>1'. \<langle>c\<^sub>1, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c\<^sub>1', mds', mem'\<rangle>\<^sub>w \<and> c' = c\<^sub>1' ;; c\<^sub>2)"
  by (erule eval_elim, auto)


lemma prog_mem_assign_helper:
  "to_prog_mem (mem(ProgVar x := eval\<^sub>A (to_prog_mem mem) e)) = (to_prog_mem mem)(x := eval\<^sub>A (to_prog_mem mem) e)"
  unfolding to_prog_mem_def
  by auto

lemma prog_mem_assign_helper':
  "to_prog_mem (mem(v := eval\<^sub>A (to_prog_mem mem) e)) = (case v of
      ProgVar x \<Rightarrow> (to_prog_mem mem)(x := eval\<^sub>A (to_prog_mem mem) e) |
      Lock l \<Rightarrow> to_prog_mem mem)"
  unfolding to_prog_mem_def
  by (case_tac v, auto)

lemma assign_eval\<^sub>w':
  "\<lbrakk>mds = mds'; mem' = mem(ProgVar x := eval\<^sub>A (to_prog_mem mem) e)\<rbrakk> \<Longrightarrow>
  \<langle>x \<leftarrow> e, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>Stop, mds', mem'\<rangle>\<^sub>w"
  apply(drule fullmemD, clarsimp)
  apply(rule assign_eval\<^sub>w)
   apply(simp add:prog_mem_assign_helper)+
  unfolding to_lock_mem_def
  by simp

lemma seq_assignD:
  "\<langle>(x \<leftarrow> e) ;; c, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w \<Longrightarrow>
   c' = Stop ;; c \<and> mds' = mds \<and> mem' = mem(ProgVar x := eval\<^sub>A (to_prog_mem mem) e)"
  apply(drule seqD, simp)
  apply(erule exE, clarsimp)
  apply(drule assignD, clarsimp)
  done

lemma only_Stop_stops:
  "c \<noteq> Stop \<Longrightarrow> \<exists> c' mds' mem'. \<langle>c, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w"
proof(induct c arbitrary: mds mem)
  case (Assign x e)
  show ?case
    using assign_eval\<^sub>w' by blast
next
  case Skip
  show ?case
    using skip_eval\<^sub>w by blast
next
  case (LockAcq l)
  show ?case
  proof(cases "eval\<^sub>L mem l")
    assume "eval\<^sub>L mem l"
    thus ?case
      using lock_wait by blast
  next
    assume a: "\<not> eval\<^sub>L mem l"
    from locks_can_be_taken obtain v where
      "eval\<^sub>L (mem(Lock l := v)) l" by blast
    with a lock_acq_eval\<^sub>w show ?case
      by (meson fun_upd_apply)
  qed
next
  case (LockRel l)
  show ?case
    using lock_rel_eval\<^sub>w locks_can_be_released 
    by (metis (mono_tags, hide_lams) fun_upd_apply)
next
  case (Seq c1 c2)
  show ?case
  proof(cases "c1 = Stop")
    assume "c1 = Stop"
    thus ?case using seq_stop_eval\<^sub>w by blast
  next
    assume "c1 \<noteq> Stop"
    thus ?case using Seq.hyps seq by blast
  qed
next
  case (If e c1 c2)
  show ?case using if_eval\<^sub>w by blast
next
  case (While e c)
  show ?case using while_eval\<^sub>w by blast
next
  case Stop
  thus ?case by blast
qed

end

end
