(*
Authors: Robert Sison, Edward Pierzchalski
Based on the Dependent_SIFUM_Type_Systems AFP entry, whose authors are:
 Toby Murray, Robert Sison, Edward Pierzchalski, Christine Rizkallah
in turn based on the SIFUM_Type_Systems AFP entry, whose authors are:
 Sylvia Grewe, Heiko Mantel, Daniel Schoepe
*)

section {* Language with Lock Primitives for Instantiating the Dependent SIFUM-Security Property *}

theory WhileLockLanguage
imports "../old-rg-sec/LockVars"
begin

subsection {* Syntax *}

datatype ('var, 'lock, 'aexp, 'bexp) Stmt = Assign "'var" "'aexp" (infix "\<leftarrow>" 130)
  | Skip
  | LockAcq "'lock"
  | LockRel "'lock"
  | Seq "('var, 'lock, 'aexp, 'bexp) Stmt" "('var, 'lock, 'aexp, 'bexp) Stmt" (infixr ";;" 150)
  | If "'bexp" "('var, 'lock, 'aexp, 'bexp) Stmt" "('var, 'lock, 'aexp, 'bexp) Stmt"
  | While "'bexp" "('var, 'lock, 'aexp, 'bexp) Stmt"
  | Stop

locale sifum_lang_no_dma =
  fixes eval\<^sub>A :: "('Var, 'Val) Mem \<Rightarrow> 'AExp \<Rightarrow> 'Val"
  fixes eval\<^sub>B :: "('Var, 'Val) Mem \<Rightarrow> 'BExp \<Rightarrow> bool"
  fixes eval\<^sub>L :: "'Val \<Rightarrow> bool"
  fixes aexp_vars :: "'AExp \<Rightarrow> 'Var set"
  fixes bexp_vars :: "'BExp \<Rightarrow> 'Var set"
  fixes lock_interp :: "'Lock \<Rightarrow> ('Var set \<times> 'Var set)"
  assumes Var_finite : "finite {(x :: ('Lock, 'Var) Var). True}"
  assumes eval_vars_det\<^sub>A : "\<lbrakk> \<forall> x \<in> aexp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<rbrakk> \<Longrightarrow> eval\<^sub>A mem\<^sub>1 e = eval\<^sub>A mem\<^sub>2 e"
  assumes eval_vars_det\<^sub>B : "\<lbrakk> \<forall> x \<in> bexp_vars b. mem\<^sub>1 x = mem\<^sub>2 x \<rbrakk> \<Longrightarrow> eval\<^sub>B mem\<^sub>1 b = eval\<^sub>B mem\<^sub>2 b"
  fixes lock_val_True :: "'Val"
  fixes lock_val_False :: "'Val"
  assumes eval\<^sub>L_True: "eval\<^sub>L lock_val_True"
  assumes eval\<^sub>L_False: "\<not> eval\<^sub>L lock_val_False"

locale sifum_lang = sifum_lang_no_dma eval\<^sub>A eval\<^sub>B eval\<^sub>L aexp_vars bexp_vars lock_interp
                      lock_val_True lock_val_False
  for eval\<^sub>A :: "('Var, 'Val) Mem \<Rightarrow> 'AExp \<Rightarrow> 'Val"
  and eval\<^sub>B :: "('Var, 'Val) Mem \<Rightarrow> 'BExp \<Rightarrow> bool"
  and eval\<^sub>L :: "'Val \<Rightarrow> bool"
  and aexp_vars :: "'AExp \<Rightarrow> 'Var set"
  and bexp_vars :: "'BExp \<Rightarrow> 'Var set"
  and lock_interp :: "'Lock \<Rightarrow> ('Var set \<times> 'Var set)"
  and lock_val_True :: "'Val"
  and lock_val_False :: "'Val" +
  fixes dma :: "('Lock, 'Var) Var \<Rightarrow> Sec"

context sifum_lang_no_dma
begin

definition
  no_locks_acquired :: "(('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> bool"
where
  "no_locks_acquired mem \<equiv> \<forall>l. \<not> eval\<^sub>L (mem (Lock l))"

(* To make the examples look a bit nicer in the PDF. *)

notation (latex output)
  Seq ("_; _" 60)

notation (Rule output)
  Seq ("_ ; _" 60)

notation (Rule output)
  If ("if _ then _ else _ fi" 50)

notation (Rule output)
  While ("while _ do _ done")

abbreviation conf\<^sub>w_abv :: "('Var, 'Lock, 'AExp, 'BExp) Stmt \<Rightarrow>
  (('Lock, 'Var) Var Mds) \<Rightarrow> (('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> (_,_,_) LocalConf"
  ("\<langle>_, _, _\<rangle>\<^sub>w" [0, 120, 120] 100)
  where
  "\<langle> c, mds, mem \<rangle>\<^sub>w \<equiv> ((c, mds), mem)"

subsection {* Semantics *}

inductive_set eval\<^sub>w_simple :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt \<times> ('Var, 'Val) Mem) rel"
and eval\<^sub>w_simple_abv :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt \<times> ('Var, 'Val) Mem) \<Rightarrow>
  ('Var, 'Lock, 'AExp, 'BExp) Stmt \<times> ('Var, 'Val) Mem \<Rightarrow> bool"
  (infixr "\<leadsto>\<^sub>s" 60)
  where
  "c \<leadsto>\<^sub>s c' \<equiv> (c, c') \<in> eval\<^sub>w_simple" |
  assign: "((x \<leftarrow> e, mem), (Stop, mem (x := eval\<^sub>A mem e))) \<in> eval\<^sub>w_simple" |
  skip: "((Skip, mem), (Stop, mem)) \<in> eval\<^sub>w_simple" |
  if_true: "\<lbrakk> eval\<^sub>B mem b \<rbrakk> \<Longrightarrow> ((If b t e, mem), (t, mem)) \<in> eval\<^sub>w_simple" |
  if_false: "\<lbrakk> \<not> eval\<^sub>B mem b \<rbrakk> \<Longrightarrow> ((If b t e, mem), (e, mem)) \<in> eval\<^sub>w_simple" |
  while: "((While b c, mem), (If b (c ;; While b c) Stop, mem)) \<in> eval\<^sub>w_simple"

inductive_cases eval\<^sub>s_elim: "((c, mem), (c', mem')) \<in> eval\<^sub>w_simple"

lemma eval\<^sub>s_det: "((c, mem), (c', mem')) \<in> eval\<^sub>w_simple \<Longrightarrow>
  ((c, mem), (c'', mem'')) \<in> eval\<^sub>w_simple \<Longrightarrow>
  (c', mem') = (c'', mem'')"
  by (blast elim:eval\<^sub>s_elim)

lemma cond:
  "((If b t e, mem), (if eval\<^sub>B mem b then t else e, mem)) \<in> eval\<^sub>w_simple"
  apply(case_tac "eval\<^sub>B mem b")
   apply(auto intro: eval\<^sub>w_simple.intros)
  done

inductive_set eval\<^sub>w :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf rel"
and eval\<^sub>w_abv :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
                  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow> bool"
  (infixr "\<leadsto>\<^sub>w" 60)
where
  "c \<leadsto>\<^sub>w c' \<equiv> (c, c') \<in> eval\<^sub>w" |
  simple: "\<lbrakk> (c, pmem) \<leadsto>\<^sub>s (c', pmem'); pmem = to_prog_mem mem; pmem' = to_prog_mem mem';
             to_lock_mem mem = to_lock_mem mem' \<rbrakk> \<Longrightarrow> (\<langle>c, mds, mem\<rangle>\<^sub>w, \<langle>c', mds, mem'\<rangle>\<^sub>w) \<in> eval\<^sub>w" |
  (* These correspond to the [c-1] and [c-2] rules of the (i-)while-language of [TedescoSR14] *)
  seq1: "\<lbrakk> \<langle>c\<^sub>1, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c\<^sub>1', mds', mem'\<rangle>\<^sub>w; c\<^sub>1' \<noteq> Stop \<rbrakk> \<Longrightarrow> (\<langle>c\<^sub>1 ;; c\<^sub>2, mds, mem\<rangle>\<^sub>w, \<langle>c\<^sub>1' ;; c\<^sub>2, mds', mem'\<rangle>\<^sub>w) \<in> eval\<^sub>w" |
  seq2: "\<lbrakk> \<langle>c\<^sub>1, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>Stop, mds', mem'\<rangle>\<^sub>w \<rbrakk> \<Longrightarrow> (\<langle>c\<^sub>1 ;; c\<^sub>2, mds, mem\<rangle>\<^sub>w, \<langle>c\<^sub>2, mds', mem'\<rangle>\<^sub>w) \<in> eval\<^sub>w" |
  lock_spin: "\<lbrakk> eval\<^sub>L (mem (Lock l)); mem' = mem; mds' = mds\<rbrakk> \<Longrightarrow>
             (\<langle>LockAcq l, mds, mem\<rangle>\<^sub>w, \<langle>LockAcq l, mds', mem'\<rangle>\<^sub>w) \<in> eval\<^sub>w" |
  lock_acq: "\<lbrakk> \<not> eval\<^sub>L (mem (Lock l));
               mem' = mem((Lock l) := lock_val_True);
               mds' = lock_acq_upd lock_interp l mds\<rbrakk> \<Longrightarrow>
             (\<langle>LockAcq l, mds, mem\<rangle>\<^sub>w, \<langle>Stop, mds', mem'\<rangle>\<^sub>w) \<in> eval\<^sub>w" |
  lock_rel: "\<lbrakk> lock_held_mds_correct lock_interp mds l;
               mem' = mem((Lock l) := lock_val_False);
               mds' = lock_rel_upd lock_interp l mds\<rbrakk> \<Longrightarrow>
             (\<langle>LockRel l, mds, mem\<rangle>\<^sub>w, \<langle>Stop, mds', mem'\<rangle>\<^sub>w) \<in> eval\<^sub>w" |
  lock_invalid: "\<lbrakk> \<not> lock_held_mds_correct lock_interp mds l; mem' = mem; mds' = mds\<rbrakk> \<Longrightarrow>
             (\<langle>LockRel l, mds, mem\<rangle>\<^sub>w, \<langle>LockRel l, mds', mem'\<rangle>\<^sub>w) \<in> eval\<^sub>w"

(* "invariant" that lock mds are untouched *)
lemma eval_lock_mds_untouched:
  "\<lbrakk>\<langle>c, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w \<rbrakk> \<Longrightarrow> \<forall>l m. Lock l \<in> mds m \<longleftrightarrow> Lock l \<in> mds' m"
  using lock_acq_upd_def[where lock_interp=lock_interp] lock_rel_upd_def[where lock_interp=lock_interp]
  apply -
  apply (induct rule:eval\<^sub>w.induct, clarsimp+)
   unfolding image_def
   apply(case_tac m, (clarsimp split:prod.split)+)
  apply(case_tac m, (clarsimp split:prod.split)+)
  done

lemma eval_lock_mds_untouched':
  "\<lbrakk>((cms, mem), (cms', mem')) \<in> eval\<^sub>w \<rbrakk> \<Longrightarrow> \<forall>l m. Lock l \<in> snd cms m \<longleftrightarrow> Lock l \<in> snd cms' m"
  apply(rule_tac c="fst cms" and c'="fst cms'" and mds="snd cms" and mds'="snd cms'" in eval_lock_mds_untouched)
  apply simp
  done


subsection {* Semantic Properties *}

text {* The following lemmas simplify working with evaluation contexts
  in the soundness proofs for the type system(s). *}

inductive_cases eval_elim: "(((c, mds), mem), ((c', mds'), mem')) \<in> eval\<^sub>w"
inductive_cases stop_no_eval' [elim]: "((Stop, mem), (c', mem')) \<in> eval\<^sub>w_simple"
inductive_cases assign_elim' [elim]: "((x \<leftarrow> e, mem), (c', mem')) \<in> eval\<^sub>w_simple"
inductive_cases skip_elim' [elim]: "(Skip, mem) \<leadsto>\<^sub>s (c', mem')"

lemma skip_dest [dest]:
  "\<langle>Skip, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w \<Longrightarrow> c' = Stop \<and> mds = mds' \<and> mem = mem'"
  by (force elim:eval_elim intro:fullmemI)

lemma assign_dest [dest]:
  "\<langle>x \<leftarrow> e, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w \<Longrightarrow> c' = Stop \<and> mds = mds' \<and> mem' = mem (Var x := eval\<^sub>A (to_prog_mem mem) e)"
  by (force elim:eval_elim intro:fullmemI simp:to_prog_mem_def to_lock_mem_def)

inductive_cases if_elim' [elim!]: "(If b p q, mem) \<leadsto>\<^sub>s (c', mem')"

lemma if_elim [elim]:
  "\<And> P.
    \<lbrakk> \<langle>If b p q, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w ;
     \<lbrakk> c' = p; mem' = mem ; mds' = mds ; eval\<^sub>B (to_prog_mem mem) b \<rbrakk> \<Longrightarrow> P ;
     \<lbrakk> c' = q; mem' = mem ; mds' = mds ; \<not> eval\<^sub>B (to_prog_mem mem) b \<rbrakk> \<Longrightarrow> P \<rbrakk> \<Longrightarrow> P"
  by (force elim:eval_elim intro:fullmemI)

inductive_cases while_elim' [elim!]: "(While e c, mem) \<leadsto>\<^sub>s (c', mem')"

lemma while_dest [dest]:
  "\<lbrakk> \<langle>While e c, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w \<rbrakk> \<Longrightarrow> c' = If e (c ;; While e c) Stop \<and> mds' = mds \<and> mem' = mem"
  by (force elim:eval_elim intro:fullmemI)

inductive_cases lock_acq_elim' [elim]: "(LockAcq l, mem) \<leadsto>\<^sub>s (c', mem')"

lemma lock_acq\<^sub>w_elim [elim]:
  "\<And> P.
    \<lbrakk> \<langle>LockAcq l, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w ;
     \<lbrakk> c' = LockAcq l; mem' = mem ; mds' = mds ; eval\<^sub>L (mem (Lock l)) \<rbrakk> \<Longrightarrow> P ;
     \<lbrakk> c' = Stop; mem' = mem((Lock l) := lock_val_True); mds' = lock_acq_upd lock_interp l mds;
       \<not> eval\<^sub>L (mem (Lock l)) \<rbrakk> \<Longrightarrow> P \<rbrakk> \<Longrightarrow> P"
  by (force elim:eval_elim)

inductive_cases lock_rel_elim' [elim]: "(LockRel l, mem) \<leadsto>\<^sub>s (c', mem')"

lemma lock_rel\<^sub>w_elim [elim]:
  "\<And> P.
    \<lbrakk> \<langle>LockRel l, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w ;
     \<lbrakk> c' = Stop ; mem' = mem((Lock l) := lock_val_False); mds' = lock_rel_upd lock_interp l mds;
       lock_held_mds_correct lock_interp mds l \<rbrakk> \<Longrightarrow> P ;
     \<lbrakk> c' = LockRel l ; mem = mem' ; mds = mds';
       \<not> lock_held_mds_correct lock_interp mds l \<rbrakk> \<Longrightarrow> P \<rbrakk> \<Longrightarrow> P"
  by (force elim:eval_elim)

lemma stop_no_eval [simp]: "\<not> (\<langle>Stop, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w)"
  by (blast elim:eval_elim eval\<^sub>s_elim)

inductive_cases seq_elim' [elim]: "(c\<^sub>1 ;; c\<^sub>2, mem) \<leadsto>\<^sub>s (c', mem')"

lemmas assign_eval\<^sub>w' = simple[OF assign, simplified]
lemmas if_eval\<^sub>w' = simple[OF cond, simplified]
lemmas if_false_eval\<^sub>w' = simple[OF if_false, simplified]
lemmas skip_eval\<^sub>w' = simple[OF skip, simplified]
lemmas while_eval\<^sub>w' = simple[OF while, simplified]

lemma seq_dest [dest]:
  "\<lbrakk> \<langle>c\<^sub>1 ;; c\<^sub>2, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w \<rbrakk> \<Longrightarrow>
  (\<exists> c\<^sub>1'. \<langle>c\<^sub>1, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c\<^sub>1', mds', mem'\<rangle>\<^sub>w \<and> c' = (if c\<^sub>1' = Stop then c\<^sub>2 else c\<^sub>1' ;; c\<^sub>2))"
  by (force elim:eval_elim intro:fullmemI)

lemma eval\<^sub>w_det: "(\<langle>c, mds, mem\<rangle>\<^sub>w, \<langle>c', mds', mem'\<rangle>\<^sub>w) \<in> eval\<^sub>w \<Longrightarrow>
  (\<langle>c, mds, mem\<rangle>\<^sub>w, \<langle>c'', mds'', mem''\<rangle>\<^sub>w) \<in> eval\<^sub>w \<Longrightarrow>
  \<langle>c', mds', mem'\<rangle>\<^sub>w = \<langle>c'', mds'', mem''\<rangle>\<^sub>w"
  apply(induct arbitrary:c'' rule:eval\<^sub>w.induct)
        apply(erule eval\<^sub>s_elim)
            apply(force intro:fullmemI simp:to_prog_mem_def to_lock_mem_def)
           apply(force intro:fullmemI simp:to_prog_mem_def to_lock_mem_def)
          apply(force intro:fullmemI)
         apply(force intro:fullmemI)
        apply(force intro:fullmemI)
       apply force
      apply force
     apply force
    apply force
   apply force
  by force

lemma prog_mem_assign_helper:
  "to_prog_mem (mem(Var x := eval\<^sub>A (to_prog_mem mem) e)) = (to_prog_mem mem)(x := eval\<^sub>A (to_prog_mem mem) e)"
  unfolding to_prog_mem_def
  by auto

lemma prog_mem_assign_helper':
  "to_prog_mem (mem(v := eval\<^sub>A (to_prog_mem mem) e)) = (case v of
      Var x \<Rightarrow> (to_prog_mem mem)(x := eval\<^sub>A (to_prog_mem mem) e) |
      Lock l \<Rightarrow> to_prog_mem mem)"
  unfolding to_prog_mem_def
  by (case_tac v, auto)

lemma assign_eval\<^sub>w:
  "\<lbrakk>mds = mds'; mem' = mem(Var x := eval\<^sub>A (to_prog_mem mem) e)\<rbrakk> \<Longrightarrow>
  \<langle>x \<leftarrow> e, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>Stop, mds', mem'\<rangle>\<^sub>w"
  apply(drule fullmemD, clarsimp)
  apply(rule assign_eval\<^sub>w')
   apply(simp add:prog_mem_assign_helper)+
  unfolding to_lock_mem_def
  by simp

lemma if_true_eval\<^sub>w:
  "\<lbrakk>mds = mds'; mem = mem'; eval\<^sub>B (to_prog_mem mem) b\<rbrakk> \<Longrightarrow>
  \<langle>If b t e, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>t, mds', mem'\<rangle>\<^sub>w"
  using if_eval\<^sub>w' by presburger

lemma if_false_eval\<^sub>w:
  "\<lbrakk>mds = mds'; mem = mem'; \<not> eval\<^sub>B (to_prog_mem mem) b\<rbrakk> \<Longrightarrow>
  \<langle>If b t e, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>e, mds', mem'\<rangle>\<^sub>w"
  using if_eval\<^sub>w' by presburger

lemma if_eval\<^sub>w:
  "\<lbrakk>mds = mds'; mem = mem'\<rbrakk> \<Longrightarrow>
  \<langle>If b t e, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>if eval\<^sub>B (to_prog_mem mem) b then t else e, mds', mem'\<rangle>\<^sub>w"
  using if_eval\<^sub>w' by presburger

lemma skip_eval\<^sub>w:
  "\<lbrakk>mds = mds'; mem = mem'\<rbrakk> \<Longrightarrow>
  \<langle>Skip, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>Stop, mds', mem'\<rangle>\<^sub>w"
  using skip_eval\<^sub>w' by presburger

lemma while_eval\<^sub>w:
  "\<lbrakk>mds = mds'; mem = mem'\<rbrakk> \<Longrightarrow>
  \<langle>While b c, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>If b (c ;; While b c) Stop, mds', mem'\<rangle>\<^sub>w"
  using while_eval\<^sub>w' by presburger

lemma seq_assign_dest [dest]:
  "\<langle>(x \<leftarrow> e) ;; c, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w \<Longrightarrow>
   c' = c \<and> mds' = mds \<and> mem' = mem(Var x := eval\<^sub>A (to_prog_mem mem) e)"
  by force

lemma eval\<^sub>w_enabled_mem_diff:
  "\<langle>c, mds, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', mds', mem'\<rangle>\<^sub>w \<Longrightarrow>
   \<exists> c'' mds'' mem''.
    \<langle>c, mds, mem'''\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c'', mds'', mem''\<rangle>\<^sub>w"
  apply(induct rule:eval\<^sub>w.induct)
        apply(erule eval\<^sub>s_elim)
            using assign_eval\<^sub>w apply blast
           using skip_eval\<^sub>w apply blast
          using if_eval\<^sub>w apply blast
         using if_eval\<^sub>w apply blast
        using while_eval\<^sub>w apply blast
       using eval\<^sub>w.seq1 eval\<^sub>w.seq2 apply blast
      using eval\<^sub>w.seq1 eval\<^sub>w.seq2 apply blast
     apply(case_tac "eval\<^sub>L (mem''' (Lock l))")
      using eval\<^sub>w.lock_spin apply blast
     using eval\<^sub>w.lock_acq apply blast
    apply(case_tac "eval\<^sub>L (mem''' (Lock l))")
     using eval\<^sub>w.lock_spin apply blast
    using eval\<^sub>w.lock_acq apply blast
   using eval\<^sub>w.lock_rel apply blast
  using eval\<^sub>w.lock_invalid by blast


subsection {* Other Helpers *}

primrec leftmost_cmd
where
  "leftmost_cmd (Assign v e) = Assign v e" |
  "leftmost_cmd Skip = Skip" |
  "leftmost_cmd (LockAcq l) = LockAcq l" |
  "leftmost_cmd (LockRel l) = LockRel l" |
  "leftmost_cmd (Seq a b) = leftmost_cmd a" |
  "leftmost_cmd (If c a b) = If c a b" |
  "leftmost_cmd (While b c) = While b c" |
  "leftmost_cmd Stop = Stop"

lemma leftmost_while_enabled:
  "leftmost_cmd c\<^sub>A = While b c \<Longrightarrow>
   \<exists>c\<^sub>A' mds\<^sub>A' mem\<^sub>A'.
     \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c\<^sub>A', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>w \<and>
     leftmost_cmd c\<^sub>A' = If b (c ;; While b c) Stop"
  apply(induct c\<^sub>A arbitrary: b c)
        apply force
       apply force
      apply force
      apply force
     apply(metis seq1 if_eval\<^sub>w leftmost_cmd.simps(5,8) stop_no_eval)
    apply force
   using while_eval\<^sub>w leftmost_cmd.simps(4,5)
   apply fastforce
  by force

lemma leftmost_if_enabled:
  "leftmost_cmd c\<^sub>A = If b c\<^sub>1 c\<^sub>2 \<Longrightarrow>
   \<exists>c\<^sub>A' mds\<^sub>A' mem\<^sub>A'. \<langle>c\<^sub>A, mds\<^sub>A, mem\<^sub>A\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c\<^sub>A', mds\<^sub>A', mem\<^sub>A'\<rangle>\<^sub>w"
  apply(induct c\<^sub>A arbitrary: b c\<^sub>1 c\<^sub>2)
         apply force
        apply force
       apply force
      apply force
     using seq1 seq2 apply fastforce
    using if_eval\<^sub>w apply blast
   apply force
  by force

(* This requirement is intended to correspond to ev\<^sub>B_eq' as established/used by TypeSystem.thy.
  Note that this sort of only really makes sense assuming the presence of both bisim_simple \<B>
  and strong_low_bisim_mm \<B>, so that mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>l mem\<^sub>2 holds. What this says however is a bit
  stronger, because it furthermore imposes equality on H and NoRW conditional expressions too. *)
definition bisim_ev\<^sub>B_eq
where
  "bisim_ev\<^sub>B_eq \<B> \<equiv> \<forall>c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mem\<^sub>2 e a b.
       (\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>\<^sub>w, \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>\<^sub>w) \<in> \<B> \<and> leftmost_cmd c\<^sub>1 = If e a b \<longrightarrow>
       eval\<^sub>B (to_prog_mem mem\<^sub>1) e = eval\<^sub>B (to_prog_mem mem\<^sub>2) e"

(* Annoyingly easiest just to duplicate this here to make it accessible as a locale parameter *)
definition bisim_simple\<^sub>w
where
  "bisim_simple\<^sub>w \<B> \<equiv> \<forall>c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mem\<^sub>2. (\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>\<^sub>w, \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>\<^sub>w) \<in> \<B> \<longrightarrow> c\<^sub>1 = c\<^sub>2"

definition bisim_simple_ev\<^sub>B_eq
where
  "bisim_simple_ev\<^sub>B_eq \<equiv> \<lambda>\<B>. bisim_ev\<^sub>B_eq \<B> \<and> bisim_simple\<^sub>w \<B>"

end

end