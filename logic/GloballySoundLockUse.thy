(*
Author: Toby Murray
*)

section {* A Method for Establishing Globally Sound Use of Modes with Locking Primitives *}

theory GloballySoundLockUse
imports LocallySoundLockUse
begin

record ('Lock,'Var,'Val) cxt =
  \<Gamma>_of :: "('Var \<Rightarrow> ('Var,'Val) lpred preds option)"
  \<S>_of :: "'Lock set"
  P_of :: "('Var,'Val) lpred preds"

context sifum_types begin

text {* 
  We establish globally sound environments via the type system and a global invariant on what
  it tracks.
*}

definition
  lock_safe :: "'Lock set list \<Rightarrow> bool"
where
  "lock_safe \<S>s \<equiv> \<forall>l i j. i < length \<S>s \<and> j < length \<S>s \<and> l \<in> (\<S>s ! i) \<and> l \<in> (\<S>s ! j) \<longrightarrow> i = j" 

definition
  global_inv :: "('Lock,'Var,'Val) cxt list \<Rightarrow> (_,('Lock,'Var) Var,'Val) GlobalConf \<Rightarrow> bool"
where
  "global_inv cxts' gc \<equiv>  case gc of (lcs,mem) \<Rightarrow> 
   length cxts' = length lcs \<and> 
   (\<exists> (cxts::('Lock,'Var,'Val) cxt list). length cxts = length lcs \<and>
    (\<forall>i<length lcs.
     has_type (\<Gamma>_of (cxts ! i)) (\<S>_of (cxts ! i)) (P_of (cxts ! i)) (fst (lcs ! i)) 
              (\<Gamma>_of (cxts' ! i)) (\<S>_of (cxts' ! i)) (P_of (cxts' ! i)) \<and> 
     preservation_props (snd (lcs ! i)) (\<Gamma>_of (cxts ! i)) (\<S>_of (cxts ! i)) (P_of (cxts ! i)) mem) \<and>
    lock_safe (map \<S>_of cxts))"

lemma compatible_envs_from_env_consistent_lock_safe:
  assumes len [simp]: "length cxts = length envs"
  assumes con: "(\<forall>i<length cxts. 
     env_consistent (envs ! i) (\<Gamma>_of (cxts ! i)) (\<S>_of (cxts ! i)) (P_of (cxts ! i)))"
  assumes safe: "lock_safe (map \<S>_of cxts)"
  shows "compatible_envs envs"
  apply(clarsimp simp: compatible_envs_def, safe)
   apply(clarsimp simp: compatible_modes_def, safe)
    proof -
      fix i x j
      assume ile: "i < length envs"
         and xin: "x \<in> fst (envs ! i) AsmNoReadOrWrite"
         and jle: "j < length envs"
         and neq: "j \<noteq> i"
      from con ile have coni: "env_consistent (envs ! i) (\<Gamma>_of (cxts ! i)) (\<S>_of (cxts ! i)) (P_of (cxts ! i))" by auto 
      from coni xin obtain y l where [simp]: "x = ProgVar y" and li: "y \<in> snd (lock_interp l)" and
                                             "l \<in> (\<S>_of (cxts ! i))"
        by(auto simp: env_consistent_def stable_NoRW_def)
      with safe jle have "l \<notin> (\<S>_of (cxts ! j))" unfolding lock_safe_def
      using ile len length_map neq nth_map by metis
      moreover with jle con have 
        "env_consistent (envs ! j) (\<Gamma>_of (cxts ! j)) (\<S>_of (cxts ! j)) (P_of (cxts ! j))"
        by auto
      ultimately show "x \<in> fst (envs ! j) GuarNoReadOrWrite" using li
        by(auto simp: env_consistent_def)
    next
      fix i x j
      assume ile: "i < length envs"
         and xin: "x \<in> fst (envs ! i) AsmNoWrite"
         and jle: "j < length envs"
         and neq: "j \<noteq> i"
      from con ile have coni: "env_consistent (envs ! i) (\<Gamma>_of (cxts ! i)) (\<S>_of (cxts ! i)) (P_of (cxts ! i))" by auto 
      from coni xin obtain y l where [simp]: "x = ProgVar y" and li: "y \<in> fst (lock_interp l)" and
                                             "l \<in> (\<S>_of (cxts ! i))"
        by(auto simp: env_consistent_def stable_NoW_def)
      with safe jle have "l \<notin> (\<S>_of (cxts ! j))" unfolding lock_safe_def
      using ile len length_map neq nth_map by metis
      moreover with jle con have 
        "env_consistent (envs ! j) (\<Gamma>_of (cxts ! j)) (\<S>_of (cxts ! j)) (P_of (cxts ! j))"
        by auto
      ultimately show "x \<in> fst (envs ! j) GuarNoWrite" using li
        by(auto simp: env_consistent_def)
    next
      show "compatible_agrels (map snd envs)"
      proof(rule compatible_agrels_legacy_imp, clarsimp)
        fix i R j 
        assume ile: "i < length envs"
           and Rasm: "R \<in> snd (envs ! i) AsmRel"
           and jle: "j < length envs"
           and neq: "j \<noteq> i"
        from con ile have coni: "env_consistent (envs ! i) (\<Gamma>_of (cxts ! i)) (\<S>_of (cxts ! i)) (P_of (cxts ! i))" by auto 
        from jle con have 
          jcon: "env_consistent (envs ! j) (\<Gamma>_of (cxts ! j)) (\<S>_of (cxts ! j)) (P_of (cxts ! j))"
          by auto
        from coni Rasm have "R = global_invariant_rel \<or> (\<exists>l. l \<in> \<S>_of (cxts ! i) \<and> R = lock_no_release_rel l)"
          by(auto simp: env_consistent_def)
        thus "R \<in> snd (envs ! j) GuarRel"
        proof
          assume [simp]: "R = global_invariant_rel"
          from jcon show ?thesis by(auto simp: env_consistent_def)
        next
          assume "\<exists>l. l \<in> \<S>_of (cxts ! i) \<and> R = lock_no_release_rel l "
          from this obtain l where lin: "l \<in> \<S>_of (cxts ! i)" and [simp]: " R = lock_no_release_rel l"
            by blast
          with safe jle have nin: "l \<notin> (\<S>_of (cxts ! j))" unfolding lock_safe_def
          using ile len length_map neq nth_map by metis
          from jcon nin show ?thesis by(auto simp: image_def env_consistent_def)
        qed
      qed
    qed

lemma global_inv_compatible_envs:
  "global_inv cxts' gc \<Longrightarrow> compatible_envs (map snd (fst gc))"
  apply(clarsimp simp: global_inv_def)
  apply(rename_tac cxts)
  apply(rule_tac cxts=cxts in compatible_envs_from_env_consistent_lock_safe)
  apply(auto simp: tyenv_wellformed_def)
  done


lemma others_steps_are_step_other:
  "meval_abv (cms,mem) k (cms',mem') \<Longrightarrow> global_inv cxts' (cms,mem) \<Longrightarrow> 
   j \<noteq> k \<Longrightarrow> j < length cms \<Longrightarrow> step_other (cms ! j,mem) (cms ! j,mem')"
proof(induct rule: meval.induct)
  fix cms n mem cm' mem'
  assume step: "(cms ! n, mem) \<leadsto> (cm', mem')"
  assume nle: "n < length cms"
  assume ginv: "global_inv cxts' (cms, mem)"
  assume jle: "j < length cms"
  assume neq: "j \<noteq> n"
  from ginv obtain \<Gamma> \<S> P where
    "has_type \<Gamma> \<S> P (fst (cms ! n)) (\<Gamma>_of (cxts' ! n)) (\<S>_of (cxts' ! n)) (P_of (cxts' ! n))"
    "preservation_props (snd (cms ! n)) \<Gamma> \<S> P mem"
    unfolding global_inv_def
    using nle by blast
  hence loc: "locally_sound_env_use \<langle>fst (cms ! n),snd (cms ! n),mem\<rangle>"
    by(rule has_type_locally_sound_env_use)

  show "step_other (cms ! j, mem) (cms ! j, mem')"   
  proof(subst surjective_pairing, subst surjective_pairing, rule step_other.intros, intro allI impI)
    fix x
    assume nw: "var_asm_not_written (fst (snd (cms ! j))) x"
    have "x \<in> fst (snd (cms ! n)) GuarNoWrite \<or> x \<in> fst (snd (cms ! n)) GuarNoReadOrWrite"
    proof -
      from ginv have "compatible_envs (map snd cms)" using global_inv_compatible_envs by auto
      with neq nw nle jle show ?thesis
        by(auto simp: compatible_envs_def compatible_modes_def var_asm_not_written_def)
    qed
    with loc have "doesnt_modify (fst (cms ! n)) mem {x}"
      using doesnt_read_or_modify_doesnt_modify doesnt_modify_subset loc_reach_refl'
      unfolding locally_sound_env_use_def
      by (metis (no_types, lifting) singletonD subsetI)
    with step show "mem x = mem' x \<and> dma mem x = dma mem' x"
      unfolding doesnt_modify_def using surjective_pairing
      by (metis singletonI)
  next
    show "\<forall>R\<in>snd (snd (cms ! j)) AsmRel. (mem, mem') \<in> R"
    proof
      fix R
      assume AsmR: "R \<in> snd (snd (cms ! j)) AsmRel"
      have "\<Inter>snd (snd (cms ! n)) GuarRel \<subseteq> R"
      proof -
        from ginv have "compatible_envs (map snd cms)" using global_inv_compatible_envs by auto
        with neq AsmR nle jle show ?thesis
          by(auto simp: compatible_envs_def compatible_agrels_def)
      qed
      moreover with loc have "(mem,mem') \<in> \<Inter>snd (snd (cms ! n)) GuarRel"
      using  loc_reach_refl' step
      unfolding locally_sound_env_use_def
      using respects_guarantee_rel_def
      proof -
        assume "\<forall>c' env' mem'. \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>fst (cms ! n), snd (cms ! n), mem\<rangle> \<longrightarrow> doesnt_read_or_modify c' mem' (fst env' GuarNoReadOrWrite) \<and> doesnt_modify c' mem' (fst env' GuarNoWrite) \<and> Ball (snd env' GuarRel) (respects_guarantee_rel c' env' mem')"
        then have f1: "\<And>r. r \<notin> snd (snd (cms ! n)) GuarRel \<or> respects_guarantee_rel (fst (cms ! n)) (snd (cms ! n)) mem r"
          using loc_reach_refl' by blast
        have "\<forall>R s p f. \<exists>r. (r \<in> R \<or> respects_guarantee_rel s p f (\<Inter>R)) \<and> (\<not> respects_guarantee_rel s p f r \<or> respects_guarantee_rel s p f (\<Inter>R))"
          using respects_guarantee_rel_Inter by blast
        then show ?thesis
          using f1 by (metis (no_types) local.step prod.collapse respects_guarantee_rel_def)
      qed
      ultimately show "(mem,mem') \<in> R"
        by blast
    qed
  qed
qed

lemma lock_safe_upd:
  assumes safe: "lock_safe (map \<S>_of cxts)"
  assumes nle: "n < length cxts"
  assumes new_safe: "\<And>l. l\<in>\<S>_of cxt - \<S>_of (cxts ! n) \<Longrightarrow> (\<forall>i. i < length cxts \<and> i \<noteq> n \<longrightarrow> l \<notin> \<S>_of (cxts ! i))"
  shows "lock_safe (map \<S>_of (cxts[n := cxt]))"
  using assms apply(clarsimp simp: lock_safe_def)
  by (metis nth_list_update nth_map)

lemma global_inv_preservation:
  "meval_abv (cms,mem) k (cms',mem') \<Longrightarrow> global_inv cxts' (cms,mem) \<Longrightarrow> global_inv cxts' (cms',mem')"
proof(induct rule: meval.induct)
  fix cms n mem cm' mem'
  assume step: "(cms ! n, mem) \<leadsto> (cm', mem')"
  assume nle: "n < length cms"
  assume ginv: "global_inv cxts' (cms, mem)"
  from ginv have [simp]: "length cxts' = length cms" unfolding global_inv_def by blast
  from ginv obtain cxts where
  [simp]: "length (cxts::('Lock,'Var,'Val) cxt list) = length cms" and
  cxts: "(\<forall>i<length cms.
     has_type (\<Gamma>_of (cxts ! i)) (\<S>_of (cxts ! i)) (P_of (cxts ! i)) (fst (cms ! i)) 
              (\<Gamma>_of (cxts' ! i)) (\<S>_of (cxts' ! i)) (P_of (cxts' ! i)) \<and> 
     preservation_props (snd (cms ! i)) (\<Gamma>_of (cxts ! i)) (\<S>_of (cxts ! i)) (P_of (cxts ! i)) mem)" and
  safe: "lock_safe (map \<S>_of cxts)" unfolding global_inv_def by blast
  let ?\<Gamma>n = "(\<Gamma>_of (cxts ! n))"
  let ?\<S>n = "(\<S>_of (cxts ! n))"
  let ?Pn = "(P_of (cxts ! n))"
  let ?cn = "fst (cms ! n)"
  let ?cn' = "fst cm'"
  let ?envn' = "snd cm'"
  let ?envn = "snd (cms ! n)"
  let ?\<Gamma>n' = "(\<Gamma>_of (cxts' ! n))"
  let ?\<S>n' = "(\<S>_of (cxts' ! n))"
  let ?Pn' = "(P_of (cxts' ! n))"
  from nle cxts have t: "has_type ?\<Gamma>n ?\<S>n ?Pn ?cn ?\<Gamma>n' ?\<S>n' ?Pn'" and
                     pp: "preservation_props ?envn ?\<Gamma>n ?\<S>n ?Pn mem"
    by blast+

  have step_rewritten: "\<langle>fst (cms ! n),(fst (snd (cms ! n)),snd (snd (cms ! n))),mem\<rangle> \<leadsto> \<langle>fst cm',(fst (snd cm'),snd (snd cm')),mem'\<rangle>"
    using step by simp
  from preservation[OF t step_rewritten] obtain \<Gamma>n'' \<S>n'' Pn'' where
    has_type': "has_type \<Gamma>n'' \<S>n'' Pn'' ?cn' ?\<Gamma>n' ?\<S>n' ?Pn'" and
          pp': "preservation_props ?envn' \<Gamma>n'' \<S>n'' Pn'' mem'" and
          new_locks_werent_held: "\<And>l. l \<in> \<S>n'' \<and> l \<notin> ?\<S>n \<Longrightarrow> \<not> ev\<^sub>L mem l" 
    using pp by auto
  let ?cxts'' = "cxts[n := \<lparr>\<Gamma>_of = \<Gamma>n'', \<S>_of = \<S>n'', P_of = Pn''\<rparr>]"
  show "global_inv cxts' (cms[n := cm'], mem')"
  proof(simp add: global_inv_def, rule_tac x="?cxts''" in exI, clarsimp, intro conjI, intro allI impI, rule conjI)
    fix i
    assume ile: "i < length cms"
    show "\<turnstile> \<Gamma>_of (?cxts'' ! i),\<S>_of (?cxts'' ! i),P_of (?cxts'' ! i) 
            {fst (cms[n := cm'] ! i)}
            \<Gamma>_of (cxts' ! i),\<S>_of (cxts' ! i),P_of (cxts' ! i)"
    proof(cases "i = n")
      assume [simp]: "i = n"
      thus ?thesis
        using has_type' nle by simp
    next
      assume "i \<noteq> n"
      hence [simp]: "n \<noteq> i" by simp
      thus ?thesis using cxts ile by auto
    qed
  next
    fix i
    assume ile: "i < length cms"
    show "preservation_props (snd (cms[n := cm'] ! i))
          (\<Gamma>_of (cxts[n := \<lparr>\<Gamma>_of = \<Gamma>n'', \<S>_of = \<S>n'', P_of = Pn''\<rparr>] ! i))
          (\<S>_of (cxts[n := \<lparr>\<Gamma>_of = \<Gamma>n'', \<S>_of = \<S>n'', P_of = Pn''\<rparr>] ! i))
          (P_of (cxts[n := \<lparr>\<Gamma>_of = \<Gamma>n'', \<S>_of = \<S>n'', P_of = Pn''\<rparr>] ! i)) mem'"
    proof(cases "i = n")
      assume [simp]: "i = n"
      thus ?thesis
        using pp' nle by simp
    next
      assume "i \<noteq> n"
      hence [simp]: "n \<noteq> i" by simp
      thus ?thesis using cxts ile 
        apply simp
        apply(rule step_other_preservation)
            apply(subst surjective_pairing[symmetric], subst surjective_pairing[symmetric],rule others_steps_are_step_other[where j=i and k=n])
               apply(rule meval.intros)
                apply(rule step)
               apply(rule nle)
              apply(rule ginv)
             apply(rule `i \<noteq> n`)
            apply(rule ile)
           apply fast+
        done
    qed
  next
    show "lock_safe (map \<S>_of (cxts[n := \<lparr>\<Gamma>_of = \<Gamma>n'', \<S>_of = \<S>n'', P_of = Pn''\<rparr>]))"
    proof(rule lock_safe_upd[OF safe], (simp add: nle)+, clarify)
      fix l i
      assume lnew: "l \<in> \<S>n''" "l \<notin> \<S>_of (cxts ! n)" and
        ile: "i < length cms" and neq: "i \<noteq> n" and lold: "l \<in> \<S>_of (cxts ! i)"
      from lnew new_locks_werent_held have wasnt_held: "\<not> ev\<^sub>L mem l" by blast
      with ile cxts have "stable_consistent (\<S>_of (cxts ! i)) mem" by fast
      with lold wasnt_held show "False"
        by(auto simp: stable_consistent_def)
    qed
  qed
qed
   
lemma global_invariant_ignores_not_readable_vars:
  assumes ginv: "global_inv cxts' (cms,mem)"
  assumes diff: "\<forall>x. x \<notin> not_readable_vars (map (fst \<circ> snd) cms) \<longrightarrow> mem x = mem' x"
  assumes inv: "global_invariant mem"
  shows "global_invariant mem'"
proof(clarsimp simp: global_invariant_def)
  from ginv have [simp]: "length cxts' = length cms" unfolding global_inv_def by blast
  from ginv obtain cxts where
  [simp]: "length (cxts::('Lock,'Var,'Val) cxt list) = length cms" and
  cxts: "(\<forall>i<length cms.
     has_type (\<Gamma>_of (cxts ! i)) (\<S>_of (cxts ! i)) (P_of (cxts ! i)) (fst (cms ! i)) 
              (\<Gamma>_of (cxts' ! i)) (\<S>_of (cxts' ! i)) (P_of (cxts' ! i)) \<and> 
     preservation_props (snd (cms ! i)) (\<Gamma>_of (cxts ! i)) (\<S>_of (cxts ! i)) (P_of (cxts ! i)) mem)" and
  safe: "lock_safe (map \<S>_of cxts)" unfolding global_inv_def by blast

  fix l
  assume not_locked: "\<not> ev\<^sub>L mem' l"
  have "mem (Lock l) = mem' (Lock l)"
  proof -
    have "Lock l \<notin> not_readable_vars (map (fst \<circ> snd) cms)"
    proof(clarsimp simp: not_readable_vars_def)
      fix c mds agrels
      assume inset: "(c, mds, agrels) \<in> set cms"
      assume nrw: "Lock l \<in> mds AsmNoReadOrWrite"
      from inset obtain i where ile: "i < length cms" and cmsi_def: "cms ! i = (c, mds, agrels)"
        by (meson in_set_conv_nth)
      from ile cmsi_def ginv obtain \<Gamma> \<S> P where
        "preservation_props (mds,agrels) \<Gamma> \<S> P mem"
        using case_prodD global_inv_def snd_conv by auto
      with nrw show "False"
        by(auto simp: env_consistent_def tyenv_wellformed_def)
    qed
    with diff show ?thesis by blast
  qed
  with not_locked have not_locked': "\<not> ev\<^sub>L mem l" using eval_det\<^sub>L by metis
  have "\<forall>v\<in>lpred_vars (lock_inv l). ProgVar v \<notin> not_readable_vars (map (fst \<circ> snd) cms)"
  proof
    fix v
    assume "v \<in> lpred_vars (lock_inv l)"
    hence "v \<in> fst (lock_interp l) \<union> snd (lock_interp l)" using lock_invs[OF surjective_pairing] by blast
    hence owned_by_l: "\<forall>l'. v \<in> fst (lock_interp l') \<union> snd (lock_interp l') \<longrightarrow> l' = l"
      using lone_lock_per_var by blast
    moreover from cxts not_locked' have "\<forall>i<length cxts. l \<notin> \<S>_of (cxts ! i)"
      by(auto simp: stable_consistent_def)
    hence "\<forall>i<length cms. ProgVar v \<notin> (fst (snd (cms ! i))) AsmNoReadOrWrite"
      using cxts owned_by_l by(clarsimp simp: tyenv_wellformed_def env_consistent_def stable_NoW_def stable_NoRW_def)
    thus "ProgVar v \<notin> not_readable_vars (map (fst \<circ> snd) cms)"
      apply (clarsimp simp: not_readable_vars_def in_set_conv_nth) by force
  qed  
  moreover from not_locked' inv have "lpred_eval (to_prog_mem mem) (lock_inv l)"
    unfolding global_invariant_def by metis
  ultimately show "lpred_eval (to_prog_mem mem') (lock_inv l)"
    using diff lpred_eval_vars_det
    by (metis to_prog_mem_def)
qed

lemma lock_held_ignores_not_readable_vars:
  assumes ginv: "global_inv cxts' (cms,mem)"
  assumes diff: "\<forall>x. x \<notin> not_readable_vars (map (fst \<circ> snd) cms) \<longrightarrow> mem x = mem' x"
  shows "ev\<^sub>L mem l = ev\<^sub>L mem' l "
proof -
  have "mem (Lock l) = mem' (Lock l)"
  proof -
    have "Lock l \<notin> not_readable_vars (map (fst \<circ> snd) cms)"
    proof(clarsimp simp: not_readable_vars_def)
      fix c mds agrels
      assume inset: "(c, mds, agrels) \<in> set cms"
      assume nrw: "Lock l \<in> mds AsmNoReadOrWrite"
      from inset obtain i where ile: "i < length cms" and cmsi_def: "cms ! i = (c, mds, agrels)"
        by (meson in_set_conv_nth)
      from ile cmsi_def ginv obtain \<Gamma> \<S> P where
        "preservation_props (mds,agrels) \<Gamma> \<S> P mem"
        using case_prodD global_inv_def snd_conv by auto
      with nrw show "False"
        by(auto simp: env_consistent_def tyenv_wellformed_def)
    qed
    with diff show ?thesis by blast
  qed
  thus ?thesis using eval_det\<^sub>L by metis
qed

text {*
   a phrasing of globally_sound_env_use in the form of an invariant over reachable global
   configurations
*}
definition 
  globally_sound_envs :: "(_,_,_) GlobalConf \<Rightarrow> bool"
where
  "globally_sound_envs gc \<equiv> \<forall>sched cms mem. (meval_sched sched gc (cms,mem)) \<longrightarrow>
          compatible_envs (map snd cms) \<and>
          (\<forall>k cms' mem'. meval_abv (cms,mem) k (cms',mem') \<longrightarrow>
          (\<forall>i<length cms. (\<forall>R\<in>snd (snd (cms ! i)) AsmRel. rel_ignores_vars R (not_readable_vars (map (fst \<circ> snd) cms)) (not_readable_vars (map (fst \<circ> snd) cms')) mem mem')))"

lemma globally_sound_env_use_by_globally_sound_envs:
  "globally_sound_envs gc \<Longrightarrow> globally_sound_env_use gc"
proof -
  assume ge: "globally_sound_envs gc"
  show "globally_sound_env_use gc"
  unfolding globally_sound_env_use_def
  proof(safe)
    fix envs
    assume "envs \<in> reachable_envs gc"
    from this obtain sched cms mem where
      "meval_sched sched gc (cms,mem)" "envs = map snd cms"
      unfolding reachable_envs_def by blast+
    thus "compatible_envs envs"
      using ge unfolding globally_sound_envs_def by blast
  next
    show "side_condition gc"
    unfolding side_condition_def
    using ge unfolding globally_sound_envs_def by blast
  qed
qed

lemma global_inv_AsmRel_rel_ignores_vars:
  assumes ginv: "global_inv cxts' (cms,mem)"
  assumes step: "meval_abv (cms, mem) k (cms', mem')"
  assumes ile: "i < length cms"
  assumes RasmRel: "R\<in>snd (snd (cms ! i)) AsmRel"
  shows "rel_ignores_vars R (not_readable_vars (map (fst \<circ> snd) cms))
                (not_readable_vars (map (fst \<circ> snd) cms')) mem mem'"
proof -
  from ginv step have ginv': "global_inv cxts' (cms',mem')" using global_inv_preservation by blast
  from ginv ile obtain \<Gamma> \<S> P where pp: "preservation_props (snd (cms ! i)) \<Gamma> \<S> P mem"
    by(auto simp: global_inv_def)
  with RasmRel have "R = global_invariant_rel \<or> (\<exists>l. R = lock_no_release_rel l)"
    by(auto simp: tyenv_wellformed_def env_consistent_def)
  thus ?thesis
  proof
    assume "R = global_invariant_rel"
    with ginv' pp show ?thesis
      by(auto simp: rel_ignores_vars_def intro: global_invariant_ignores_not_readable_vars)
  next
    assume "\<exists>l. R = lock_no_release_rel l"
    from this obtain l where [simp]: "R = lock_no_release_rel l" by blast
    thus ?thesis
      apply(clarsimp simp: rel_ignores_vars_def lock_no_release_rel_def)
      apply(drule lock_held_ignores_not_readable_vars[OF ginv])
      apply(drule lock_held_ignores_not_readable_vars[OF ginv'])
      by auto
  qed
qed
    
  
lemma global_soudness':
  assumes ginv: "global_inv cxts' gc"
  assumes sched: "meval_sched_abv gc sched (cms, mem)"
  shows "global_inv cxts' (cms, mem) \<and>
         compatible_envs (map snd cms) \<and>
         (\<forall>k cms' mem'.
           meval_abv (cms, mem) k (cms', mem') \<longrightarrow>
           (\<forall>i<length cms.
               \<forall>R\<in>snd (snd (cms ! i)) AsmRel.
                  rel_ignores_vars R (not_readable_vars (map (fst \<circ> snd) cms))
                   (not_readable_vars (map (fst \<circ> snd) cms')) mem mem'))"
using sched ginv proof(induct sched arbitrary: gc cms mem cxts' rule: rev_induct)
  case Nil
  from `meval_sched_abv gc [] (cms,mem)` have [simp]: "gc = (cms,mem)"
    by(auto elim: meval_sched.cases)
  thus ?case
    using Nil(2) global_inv_compatible_envs global_inv_AsmRel_rel_ignores_vars 
    using fst_conv by fastforce
next
  case (snoc n ns)
  from `meval_sched_abv gc (ns @ [n]) (cms, mem)` obtain cms' mem' where
  sched_ns: "meval_sched_abv gc ns (cms',mem')" and step: "meval_abv (cms',mem') n (cms,mem)"
    by (metis meval_sched_snocD prod.collapse)
  from snoc(1)[OF sched_ns snoc(3)] have "global_inv cxts' (cms', mem')" by blast
  with step show ?case
    using global_inv_preservation global_inv_compatible_envs global_inv_AsmRel_rel_ignores_vars
  by (metis fst_conv)
qed

lemma global_inv_globally_sound_env_use:
  "global_inv cxts' gc \<Longrightarrow> globally_sound_env_use gc"
  apply(rule globally_sound_env_use_by_globally_sound_envs)
  unfolding globally_sound_envs_def
  by(clarsimp simp: global_soudness')

lemma global_inv_sound_env_use:
  "global_inv cxts' gc \<Longrightarrow> sound_env_use gc"
  apply(clarsimp simp: sound_env_use_def)
  apply(rename_tac cms mem)
  apply(rule conjI)
   apply(clarsimp simp: list_all_length)
   apply(clarsimp simp: global_inv_def)
   using has_type_locally_sound_env_use
   apply (metis prod.collapse)
  by(erule global_inv_globally_sound_env_use)

inductive typed :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt \<times> (('Lock, 'Var) Var,'Val) Env) list \<Rightarrow> bool"
  ("\<turnstile> _" [120] 1000)
  where
  "\<lbrakk> list_all (\<lambda> (c,(m,a)). (\<exists> \<Gamma>' \<S>' P'. \<turnstile> (Map.empty),{},PTrue { c } \<Gamma>',\<S>',P') \<and> m = (mds\<^sub>0) \<and> a = agrels\<^sub>0) cs;
       \<forall> mem. INIT mem \<longrightarrow>  global_invariant mem
    \<rbrakk> \<Longrightarrow>
    typed cs"


lemma typed_global_inv:
  "typed cs \<Longrightarrow> \<exists> cxts'. \<forall>mem. INIT mem \<longrightarrow> global_inv cxts' (cs,mem)"
proof(induct rule: typed.induct)
  fix cs 
  assume a: "list_all
           (\<lambda>(c, m, a). (\<exists>\<Gamma>' \<S>' P'. \<turnstile> Map.empty,{},PTrue {c} \<Gamma>',\<S>',P') \<and> m = mds\<^sub>0 \<and> a = agrels\<^sub>0)
           (cs:: (('Var, 'Lock, 'AExp, 'BExp) Stmt \<times>
    (Mode \<Rightarrow> ('Lock, 'Var) Var set) \<times>
    (AG
     \<Rightarrow> ((('Lock, 'Var) Var \<Rightarrow> 'Val) \<times>
         (('Lock, 'Var) Var \<Rightarrow> 'Val)) set set)) list)" 
          "\<forall>mem. INIT mem \<longrightarrow> global_invariant mem"
  hence all_typed: "\<forall>i<length cs. (\<exists>\<Gamma>' \<S>' P'. \<turnstile> Map.empty,{},PTrue {fst (cs ! i)} \<Gamma>',\<S>',P')" and
        all_env: "\<forall>i<length cs. snd (cs ! i) = (mds\<^sub>0,agrels\<^sub>0)"
    apply(clarsimp simp: list_all_length split: prod.splits)
    apply(rename_tac i)
    apply(case_tac "cs ! i", auto)[1]
    using a
    apply(clarsimp simp: list_all_length split: prod.splits)
    apply(rename_tac i)
    apply(case_tac "cs ! i", auto)[1]
    done
  from all_typed obtain f\<Gamma> f\<S> fP where ftyped: "(\<forall>i<length cs. has_type Map.empty {} PTrue (fst (cs ! i)) (f\<Gamma> i) (f\<S> i) (fP i))"
    apply -
    apply(subst (asm) all_nat_less_eq)+
    apply(clarsimp dest!: bchoice)
    done
  define cxts' where "cxts' \<equiv> map (\<lambda>n. \<lparr>\<Gamma>_of = f\<Gamma> n, \<S>_of = f\<S> n, P_of = fP n\<rparr>) [0..<length cs]"
  have "\<forall>mem. INIT mem \<longrightarrow> global_inv cxts' (cs, mem)"
  proof(clarify)
    fix mem
    assume INIT: "INIT mem"
    define cxts where "cxts \<equiv> replicate (length cs) \<lparr>\<Gamma>_of = (Map.empty::('Var \<Rightarrow> ('Var,'Val) lpred preds option)), \<S>_of = ({}::'Lock set), P_of = (PTrue)\<rparr>"
    show "global_inv cxts' (cs, mem)"
    proof(clarsimp simp: global_inv_def, intro conjI)
      show "length cxts' = length cs" by(simp add: cxts'_def)
    next
      show "\<exists>(cxts::('Lock,'Var,'Val) cxt list). length cxts = length cs \<and>
           (\<forall>i<length cs.
               \<turnstile> \<Gamma>_of (cxts ! i),\<S>_of (cxts ! i),P_of (cxts ! i) 
                 {fst (cs ! i)} 
                 \<Gamma>_of (cxts' ! i),\<S>_of (cxts' ! i),P_of (cxts' ! i) \<and>
               preservation_props (snd (cs ! i)) (\<Gamma>_of (cxts ! i)) (\<S>_of (cxts ! i))
                (P_of (cxts ! i)) mem) \<and>
           lock_safe (map \<S>_of cxts)"
      apply(rule_tac x=cxts in exI)
      by(auto simp: a INIT cxts_def cxts'_def ftyped all_env  mds\<^sub>0_def agrels\<^sub>0_def stable_NoRW_def stable_def types_wellformed_def types_stable_def pred_def stable_consistent_def lock_safe_def tyenv_wellformed_def env_consistent_def stable_NoW_def)
    qed
  qed
  thus "\<exists>cxts'. \<forall>mem. INIT mem \<longrightarrow> global_inv cxts' (cs, mem)" by blast
qed

lemma typed_type_global:
  "typed cs \<Longrightarrow> type_global cs"
  apply(rule type_global.intros)
   apply(blast elim: typed.cases)
  apply clarify
  apply(rule conjI)
   apply(drule typed_global_inv)
   apply clarify
   apply(rule global_inv_sound_env_use)
   apply blast
  apply(blast elim: typed.cases)
  done

lemma typed_prog_sifum_secure_cont:
  "typed cs \<Longrightarrow> prog_sifum_secure_cont cs"
  apply(rule type_soundness_global)
  apply(erule typed_type_global)
  done


end

end