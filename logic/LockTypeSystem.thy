(*
Author: Toby Murray
Based on the Dependent_SIFUM_Type_Systems AFP entry, whose authors are:
 Toby Murray, Robert Sison, Edward Pierzchalski, Christine Rizkallah
in turn based on the SIFUM_Type_Systems AFP entry, whose authors are:
 Sylvia Grewe, Heiko Mantel, Daniel Schoepe
*)

section {* Type System for Ensuring Dependent SIFUM-Security of Commands for the Lock Language *}

theory LockTypeSystem
imports Main  "../rg-sec/Compositionality" "../while-lang/LockLanguage"
begin

subsection {* Typing Rules *}

text {*
  Types now depend on memories. To see why, consider an assignment in which some variable
  @{term x} for which we have a @{term AsmNoReadOrWrite} assumption is assigned the value in 
  variable @{term input}, but where @{term input}'s classification depends on some control
  variable. Then the new type of @{term x} depends on memory. If we were to just take the 
  upper bound of @{term input}'s classification, this would likely give us @{term High} as
  @{term x}'s type, but that would prevent us from treating @{term x} as @{term Low}
  if we later learn @{term input}'s original classification. 
  
  Instead we need to make @{term x}'s type explicitly depend on memory so later on, once we
  learn @{term input}'s classification, we can resolve @{term x}'s type to a concrete
  security level.
  
  We choose to deeply embed types as predicates. If the predicate
  evaluates to @{term True}, the type is @{term Low}; otherwise it is @{term High}.
*}
type_synonym ('Var,'Val) Type = "('Var,'Val) lpred"

text {*
  We require @{term \<Gamma>} to track all stable (i.e. @{term AsmNoWrite} or @{term AsmNoReadOrWrite}), 
  non-@{term \<C>} variables.
  
  This differs from Mantel a bit. Mantel would exclude from @{term \<Gamma>}, variables
  whose classification (according to @{term dma}) is @{term Low} for which we have only
  an @{term AsmNoWrite} assumption.
  
  We decouple the requirement for inclusion in @{term \<Gamma>} from a variable's classification
  so that we don't need to be updating @{term \<Gamma>} each time we alter a control variable.
  Even if we tried to keep @{term \<Gamma>} up-to-date in that case, we may not be able to 
  precisely compute the new classification of each variable after the modification anyway.
*}
type_synonym ('Var,'Val) TyEnv = "'Var \<rightharpoonup> ('Var,'Val) Type"


text {*
  We track a predicate on memories as we analyse over the program. 
*}
type_synonym 'pred preds = "'pred"

datatype 'Lock LockUpd = Acq 'Lock | Rel 'Lock

context sifum_lang_no_dma begin

abbreviation
  pred :: "(('Var,'Val) lpred) preds \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> bool"
where
  "pred P \<equiv> \<lambda>mem. (lpred_eval mem P)"

end

(* Note: we quietly require that variables are linearly ordered so we can sort them
         when we need to. *)
locale sifum_types =
  sifum_lang_no_dma ev\<^sub>A ev\<^sub>B ev\<^sub>L aexp_vars bexp_vars lock_interp lock_inv lock_acq_sem lock_rel_sem
                    aexp_to_lexp bexp_to_lpred + 
  sifum_security_init_no_det dma \<C>_vars \<C> eval\<^sub>w undefined
  for ev\<^sub>A :: "('Var::linorder, 'Val) Mem \<Rightarrow> 'AExp \<Rightarrow> 'Val"
  and ev\<^sub>B :: "('Var, 'Val) Mem \<Rightarrow> 'BExp \<Rightarrow> bool"
  and ev\<^sub>L :: "(('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> 'Lock \<Rightarrow> bool"
  and aexp_vars :: "'AExp \<Rightarrow> 'Var set"
  and bexp_vars :: "'BExp \<Rightarrow> 'Var set"
  and lock_interp :: "'Lock \<Rightarrow> ('Var set \<times> 'Var set)"
  and lock_inv :: "'Lock \<Rightarrow> ('Var,'Val) lpred"
  and lock_acq_sem :: "'Lock \<Rightarrow> 'Val \<Rightarrow> 'Val"
  and lock_rel_sem :: "'Lock \<Rightarrow> 'Val \<Rightarrow> 'Val"
  and aexp_to_lexp :: "'AExp \<Rightarrow> ('Var,'Val) lexp"
  and bexp_to_lpred :: "'BExp \<Rightarrow> ('Var,'Val) lpred"
  and dma :: "(('Lock, 'Var) Var,'Val) Mem \<Rightarrow> ('Lock, 'Var) Var \<Rightarrow> Sec"
  and \<C>_vars :: "('Lock, 'Var) Var \<Rightarrow> ('Lock, 'Var) Var set"
  and \<C> :: "('Lock, 'Var) Var set" +
  fixes dma_type :: "'Var \<Rightarrow> 'BExp"
  assumes dma_correct:
    "\<And>x. dma mem (ProgVar x) = (if (ev\<^sub>B (to_prog_mem mem) (dma_type x)) then Low else High)"
  (* for now we consider only dma domain assignment functions that refer only to ProgVars *)
  assumes \<C>_vars_correct:
    "\<C>_vars (ProgVar x) = ProgVar ` (bexp_vars (dma_type x))"
    "\<C>_vars (Lock l) = {}"
  (* We impose this requirement so we can prove low-bisimilar memories have the same lock state *)
  assumes locks_are_Low: "dma mem (Lock l) = Low"
  (* a variable is protected by a lock iff its control variables are. the reason is because
     when gaining access to a variable its control vars must be stable, also modifying the
     control variable modifies the variable (by changing its classification), so gaining access
     to the control variable already grants access to the variable in a sense.  *)
  assumes lock_interp_\<C>_vars\<^sub>V: 
      "\<And>c x f. f \<in> {fst,snd} \<Longrightarrow> ProgVar c \<in> \<C>_vars (ProgVar x) \<Longrightarrow> 
                 (c \<in> f (lock_interp l)) = (x \<in> f (lock_interp l))"

sublocale sifum_types \<subseteq> sifum_security_init dma \<C>_vars \<C> eval\<^sub>w undefined
  apply(unfold_locales)
  apply(rename_tac lc lc' lc'')
  apply(case_tac lc, rename_tac cms mem, case_tac lc', rename_tac cms' mem', case_tac lc'', rename_tac cms'' mem'')
  apply(case_tac cms, case_tac cms', case_tac cms'')  
  using eval\<^sub>w_det by blast


context sifum_lang_no_dma begin

definition
  stable_NoW :: "'Lock set \<Rightarrow> 'Var set"
where
  "stable_NoW \<S> \<equiv> \<Union>(fst ` (lock_interp ` \<S>))"

definition
  stable_NoRW :: "'Lock set \<Rightarrow> 'Var set"
where
  "stable_NoRW \<S> \<equiv> \<Union>(snd ` (lock_interp ` \<S>))"

lemma stable_NoWI[intro!]:
  "l \<in> \<S> \<Longrightarrow> x \<in> fst (lock_interp l) \<Longrightarrow> x \<in> stable_NoW \<S>"
  apply(simp add: stable_NoW_def) by blast

lemma stable_NoWRI[intro!]:
  "l \<in> \<S> \<Longrightarrow> x \<in> snd (lock_interp l) \<Longrightarrow> x \<in> stable_NoRW \<S>"
  apply(simp add: stable_NoRW_def) by blast



definition
  stable :: "'Lock set \<Rightarrow> 'Var \<Rightarrow> bool"
where
  "stable \<S> x \<equiv> x \<in> stable_NoW \<S> \<union> stable_NoRW \<S>"

definition
  add_pred :: "('Var,'Val) lpred preds \<Rightarrow> 'Lock set \<Rightarrow> ('Var,'Val) lpred \<Rightarrow> ('Var,'Val) lpred preds" ("_ +\<^sub>_ _" [120, 120, 120] 1000)
where
  "P +\<^sub>\<S> e \<equiv> (if (\<forall>x\<in> lpred_vars e. stable \<S> x) then (PConj P e) else P)"


primrec
  restrict_preds_to_vars :: "('Var,'Val) lpred preds \<Rightarrow> 'Var set \<Rightarrow> ('Var,'Val) lpred preds" ("_ |`' _" [120, 120] 1000)
where
  "restrict_preds_to_vars PTrue V = PTrue" |
  "restrict_preds_to_vars PFalse V = PFalse" |
  "restrict_preds_to_vars (PDisj P Q) V = (PDisj (restrict_preds_to_vars P V) (restrict_preds_to_vars Q V))" |
  "restrict_preds_to_vars (PConj P Q) V = (PConj (restrict_preds_to_vars P V) (restrict_preds_to_vars Q V))" |
  "restrict_preds_to_vars (PImp P Q) V = (if (restrict_preds_to_vars P V) = P then (PImp P (restrict_preds_to_vars Q V)) else PTrue)" |
  "restrict_preds_to_vars (PNeg P) V = (if (restrict_preds_to_vars P V) = P then (PNeg P) else PTrue)" |
  "restrict_preds_to_vars (PCmp cmp e f) V = (if (lexp_vars e \<union> lexp_vars f) \<subseteq> V then (PCmp cmp e f) else PTrue)" |
  "restrict_preds_to_vars (PAll fP) V = (PAll (\<lambda>x. (restrict_preds_to_vars (fP x) V)))" |
  "restrict_preds_to_vars (PEx fP) V = (PEx (\<lambda>x. (restrict_preds_to_vars (fP x) V)))"

lemma restrict_preds_to_vars_weakens':
  "lpred_eval mem P \<Longrightarrow> lpred_eval mem (restrict_preds_to_vars P V)"
  apply(induct P, auto)
  done

lemma restrict_preds_to_vars_restricts:
  "lpred_vars (restrict_preds_to_vars P V) \<subseteq> V"
  apply(induct P, auto)
  done

lemma restrict_preds_to_vars_noop:
  "lpred_vars P \<subseteq> V \<Longrightarrow> restrict_preds_to_vars P V = P"
  apply(induct P, (auto | blast)+)
  done

lemma restrict_preds_to_vars_noopD:
  "restrict_preds_to_vars P V = P \<Longrightarrow> lpred_vars P \<subseteq> V"
  apply(induct P, auto split: if_splits dest: fun_cong)
  done

lemma restrict_preds_to_vars_idempotent:
  "restrict_preds_to_vars (restrict_preds_to_vars P V) V = (restrict_preds_to_vars P V)"
  apply(rule restrict_preds_to_vars_noop)
  apply(rule restrict_preds_to_vars_restricts)
  done


primrec
  lexp_subst :: "('Var,'Val) lexp \<Rightarrow> ('Var,'Val) lexp \<Rightarrow> 'Var \<Rightarrow> ('Var,'Val) lexp"
where
  "lexp_subst (Var y) e x = (if x = y then e else (Var y))" |
  "lexp_subst (Const c) e x = Const c" |
  "lexp_subst (LBinOp opf f f') e x = (LBinOp opf (lexp_subst f e x) (lexp_subst f' e x))"

lemma lexp_subst_mem_upd:
  "(\<And>y. y \<noteq> x \<Longrightarrow> mem' y = mem y) \<Longrightarrow> leval mem' (lexp_subst f (Const (mem x)) x) = leval mem f"
  apply(induct f, auto)
  done

primrec
  lpred_subst :: "('Var,'Val) lpred preds \<Rightarrow> ('Var,'Val) lexp \<Rightarrow> 'Var \<Rightarrow> ('Var,'Val) lpred preds"
where
  "lpred_subst PTrue e x = PTrue" |
  "lpred_subst PFalse e x = PFalse" |
  "lpred_subst (PConj P Q) e x = (PConj (lpred_subst P e x) (lpred_subst Q e x))" |
  "lpred_subst (PDisj P Q) e x = (PDisj (lpred_subst P e x) (lpred_subst Q e x))" |
  "lpred_subst (PImp P Q) e x = (PImp (lpred_subst P e x) (lpred_subst Q e x))" |
  "lpred_subst (PNeg P) e x = (PNeg (lpred_subst P e x))" |
  "lpred_subst (PCmp cmp f f') e x = (PCmp cmp (lexp_subst f e x) (lexp_subst f' e x))" |
  "lpred_subst (PAll fP) e x = (PAll (\<lambda>x'. lpred_subst (fP x') e x))" |
  "lpred_subst (PEx fP) e x = (PEx (\<lambda>x'. lpred_subst (fP x') e x))"

lemma lpred_subst_mem_upd:
  "(\<And>y. y \<noteq> x \<Longrightarrow> mem' y = mem y) \<Longrightarrow> 
     lpred_eval mem' (lpred_subst P (Const (mem x)) x) = lpred_eval mem P"
  apply(induct P, auto simp: lexp_subst_mem_upd)
  done

text {*
  This encodes Floyd's rule for reasoning forwards over assignment statements.
*}
definition
  assign_post :: "('Var,'Val) lpred preds \<Rightarrow> 'Var \<Rightarrow> 'AExp \<Rightarrow> ('Var,'Val) lpred preds"
where
  "assign_post P x e = (PEx (\<lambda>x'. (PConj (lpred_subst P (Const x') x) (PCmp (=) (Var x) (lexp_subst (aexp_to_lexp e) (Const x') x)))))"

lemma assign_post_sound:
  assumes P: "lpred_eval (to_prog_mem mem) P"
  assumes step: "\<langle>x \<leftarrow> e, env, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', env', mem'\<rangle>\<^sub>w"
  shows "lpred_eval (to_prog_mem mem') (assign_post P x e)"
proof(clarsimp simp: assign_post_def, rule_tac x="mem (ProgVar x)" in exI, safe)
  from assignD step have upd [simp]:
  "c' = Stop \<and> env = env'" "mem' = mem(ProgVar x := eval\<^sub>A (to_prog_mem mem) e)" by blast+

  show "to_prog_mem mem' x =
    leval (to_prog_mem mem') (lexp_subst (aexp_to_lexp e) (Const (mem (ProgVar x))) x)"
   apply(simp add: to_prog_mem_def)
   apply(subst lexp_subst_mem_upd)
    apply auto[1]
   apply(simp add: aexp_to_lexp_correct)
   done
next
  from assignD step have upd [simp]:
  "c' = Stop \<and> env = env'" "mem' = mem(ProgVar x := eval\<^sub>A (to_prog_mem mem) e)" by blast+

  have "pred (lpred_subst P (Const (mem (ProgVar x))) x) (to_prog_mem mem') = pred P (to_prog_mem mem)"
    apply(subst lpred_subst_mem_upd)
     apply(simp add: to_prog_mem_def)
    by(simp add: to_prog_mem_def)
  with P show 
  " pred (lpred_subst P (Const (mem (ProgVar x))) x) (to_prog_mem mem')"
  by simp
qed


lemma lexp_vars_lexp_subst[simp]:
  "lexp_vars (lexp_subst f e x) = (if x \<in> lexp_vars f then lexp_vars e \<union> (lexp_vars f - {x}) else lexp_vars f)"
  apply(induct f, auto)
  done

lemma lpred_vars_lpred_subst[simp]:
  "lpred_vars (lpred_subst P e x) = (if x \<in> lpred_vars P then lexp_vars e \<union> (lpred_vars P - {x}) else lpred_vars P)"
  apply(induct P, auto split: if_splits)
  done


lemma leval_vars_det:
  "\<forall>v\<in>lexp_vars e. mem v = mem' v \<Longrightarrow> leval mem e = leval mem' e"
  apply(induct e, auto)
  done

lemma lpred_eval_vars_det:
  "\<forall>v\<in>lpred_vars P. mem v = mem' v \<Longrightarrow> lpred_eval mem P = lpred_eval mem' P"
  apply(induct P, (auto | metis leval_vars_det arg_cong UnCI)+)
  done
 
end


context sifum_types
begin

lemma no_locks_in_\<C>_vars [simp]: "(Lock l) \<notin> \<C>_vars v"
  using \<C>_vars_correct
  apply(case_tac v)
  by clarsimp+

lemma no_locks_in_\<C> [simp]: "(Lock l) \<notin> \<C>"
  using no_locks_in_\<C>_vars \<C>_def
  by simp

(* Redefined since Isabelle does not seem to be able to reuse the abbreviation from the old locale *)
abbreviation mm_equiv_abv2 :: "(_, _, _) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
(infix "\<approx>" 60)
  where "mm_equiv_abv2 c c' \<equiv> mm_equiv_abv c c'"

abbreviation eval_abv2 :: "(_, (_, 'Var) Var, 'Val) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
  (infixl "\<leadsto>" 70)
  where
  "x \<leadsto> y \<equiv> (x, y) \<in> eval\<^sub>w"
  
abbreviation eval_plus_abv :: "(_, (_, 'Var) Var, 'Val) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
  (infixl "\<leadsto>\<^sup>+" 70)
  where
  "x \<leadsto>\<^sup>+ y \<equiv> (x, y) \<in> eval\<^sub>w\<^sup>+"
  
abbreviation no_eval_abv :: "(_, (_, 'Var) Var, 'Val) LocalConf \<Rightarrow> bool" 
  ("_ \<leadsto> \<bottom>")
  where
  "x \<leadsto> \<bottom> \<equiv> \<forall> y. (x, y) \<notin> eval\<^sub>w"

abbreviation low_indistinguishable_abv :: "(('Lock, 'Var) Var,'Val) Env \<Rightarrow> ('Var, 'Lock, 'AExp, 'BExp) Stmt \<Rightarrow> (_, _, _, _) Stmt \<Rightarrow> bool"
  ("_ \<sim>\<index> _" [100, 100] 80)
  where
  "c \<sim>\<^bsub>env\<^esub> c' \<equiv> low_indistinguishable env c c'"

definition
  type_wellformed :: "('Var,'Val) Type \<Rightarrow> bool"
where
  "type_wellformed t \<equiv> ProgVar ` lpred_vars t \<subseteq> \<C>"
  
definition 
  update_env :: "'Lock LockUpd \<Rightarrow> (('Lock, 'Var) Var, 'Val) Env \<Rightarrow> (('Lock, 'Var) Var, 'Val) Env "
where
  "update_env upd env \<equiv> case upd of 
    Acq l \<Rightarrow> (lock_acq_mds_upd l (fst env),lock_acq_agrels_upd l (snd env)) |
    Rel l \<Rightarrow> (lock_rel_mds_upd l (fst env),lock_rel_agrels_upd l (snd env))"

lemma dma_type_wellformed [simp]:
  "type_wellformed (bexp_to_lpred (dma_type x))"
  apply(clarsimp simp: type_wellformed_def  \<C>_def bexp_to_lpred_vars | safe)+
  using \<C>_vars_correct by blast
  
definition 
  to_total :: "('Var, 'Val) TyEnv \<Rightarrow> 'Var \<Rightarrow> ('Var,'Val) Type"
where 
  "to_total \<Gamma> \<equiv> \<lambda>v. if v \<in> dom \<Gamma> then the (\<Gamma> v) else bexp_to_lpred (dma_type v)"

definition
  types_wellformed :: "('Var,'Val) TyEnv \<Rightarrow> bool"
where
  "types_wellformed \<Gamma> \<equiv> \<forall>x\<in>dom \<Gamma>. type_wellformed (the (\<Gamma> x))"
  
lemma to_total_type_wellformed:
  "types_wellformed \<Gamma> \<Longrightarrow>
  type_wellformed (to_total \<Gamma> x)"
  by(auto simp: to_total_def types_wellformed_def)
  
(* Note that here we have to sort the variables that come from the expression to produce
   a list of predicates that we then take the conjunction over. Hopefully this will lead to
   types having somewhat canonical form. *)
inductive 
  type_aexpr :: "('Var,'Val) TyEnv \<Rightarrow> 'AExp \<Rightarrow> ('Var,'Val) Type \<Rightarrow> bool" ("_ \<turnstile>\<^sub>a _ \<in> _" [120, 120, 120] 1000)
where
  type_aexpr [intro!]: "\<Gamma> \<turnstile>\<^sub>a e \<in> fold PConj (map (\<lambda> x. to_total \<Gamma> x) (sorted_list_of_set (aexp_vars e))) PTrue"



lemma type_aexprI:
  "t = fold PConj (map (\<lambda> x. to_total \<Gamma> x) (sorted_list_of_set (aexp_vars e))) PTrue \<Longrightarrow> \<Gamma> \<turnstile>\<^sub>a e \<in> t"
  apply(erule ssubst)
  apply(rule type_aexpr.intros)
  done

lemma type_wellformed_PConj[intro!]:
  "type_wellformed a \<Longrightarrow> type_wellformed b \<Longrightarrow> type_wellformed (PConj a b)"
  apply(auto simp: type_wellformed_def)
  done

lemma type_wellformed_fold_PConj:
  "\<forall>x \<in> set xs. type_wellformed x \<Longrightarrow> type_wellformed y \<Longrightarrow> type_wellformed (fold PConj xs y)"
  apply(induct xs arbitrary: y, auto)
  done

lemma [simp, intro!]:
  "type_wellformed PTrue"
  by(auto simp: type_wellformed_def)

lemma type_aexpr_type_wellformed:
  "types_wellformed \<Gamma> \<Longrightarrow> \<Gamma> \<turnstile>\<^sub>a e \<in> t \<Longrightarrow> type_wellformed t"
  apply(erule type_aexpr.cases)
  apply(erule ssubst, rule type_wellformed_fold_PConj)
   apply clarsimp
   apply(blast intro: to_total_type_wellformed)
  by simp
  
inductive_cases type_aexpr_elim [elim]: "\<Gamma> \<turnstile>\<^sub>a e \<in> t"

inductive
  type_bexpr :: "('Var,'Val) TyEnv \<Rightarrow> 'BExp \<Rightarrow> ('Var,'Val) Type \<Rightarrow> bool" ("_ \<turnstile>\<^sub>b _ \<in> _ " [120, 120, 120] 1000)
where
  type_bexpr [intro!]: "\<Gamma> \<turnstile>\<^sub>b e \<in> fold PConj (map (\<lambda> x. to_total \<Gamma> x) (sorted_list_of_set (bexp_vars e))) PTrue"

lemma type_bexprI:
  "t = fold PConj (map (\<lambda> x. to_total \<Gamma> x) (sorted_list_of_set (bexp_vars e))) PTrue \<Longrightarrow> \<Gamma> \<turnstile>\<^sub>b e \<in> t"
  apply(erule ssubst)
  apply(rule type_bexpr.intros)
  done

lemma type_bexpr_type_wellformed:
  "types_wellformed \<Gamma> \<Longrightarrow> \<Gamma> \<turnstile>\<^sub>b e \<in> t \<Longrightarrow> type_wellformed t"
  apply(erule type_bexpr.cases)
  apply(erule ssubst, rule type_wellformed_fold_PConj)
   apply clarsimp
   apply(blast intro: to_total_type_wellformed)
  by simp
  
inductive_cases type_bexpr_elim [elim]: "\<Gamma> \<turnstile>\<^sub>b e \<in> t"


text {*
  Define a sufficient condition for a type to be stable, assuming the type is wellformed.
  
  We need this because there is no point tracking the fact that e.g. variable @{term x}'s data has
  a classification that depends on some control variable @{term c} (where @{term c} might be
  the control variable for some other variable @{term y} whose value we've just assigned to
  @{term x}) if @{term c} can then go and be modified, since now the classification of
  the data in @{term x} no longer depends on the value of @{term c}, instead it depends on
  @{term c}'s \emph{old} value, which has now been lost.
  
  Therefore, if a type depends on @{term c}, then @{term c} had better be stable.
*}
abbreviation
  type_stable :: "'Lock set \<Rightarrow> ('Var,'Val) Type \<Rightarrow> bool"
where
  "type_stable \<S> p \<equiv> \<forall>x\<in> lpred_vars p. stable \<S> x"
  
lemma type_stable_is_sufficient:
  "\<lbrakk>type_stable \<S> t\<rbrakk> \<Longrightarrow>
  \<forall>mem mem'. (\<forall>x. stable \<S> x \<longrightarrow> mem x = mem' x) \<longrightarrow> (lpred_eval mem) t = (lpred_eval mem') t"
  apply(intro allI impI)
  apply(rule lpred_eval_vars_det)
  apply auto
  done

definition
  \<C>_vars\<^sub>V :: "'Var \<Rightarrow> 'Var set"
where
  "\<C>_vars\<^sub>V y \<equiv> {x. ProgVar x \<in> \<C>_vars (ProgVar y)}"

definition
  \<C>\<^sub>V :: "'Var set"
where
  "\<C>\<^sub>V \<equiv> {x. ProgVar x \<in> \<C>}"

lemma type_wellformed_def2:
  "type_wellformed t = (lpred_vars t \<subseteq> \<C>\<^sub>V)"
  apply(auto simp: type_wellformed_def \<C>\<^sub>V_def image_def)
  done

declare \<C>_vars_correct(2)[simp add]

lemma \<C>_vars_def2:
  "\<C>_vars (ProgVar x) = ProgVar ` \<C>_vars\<^sub>V x"
  apply(rule equalityI)
   apply(rule subsetI, rename_tac y)
   apply(case_tac y)
   using \<C>_vars_correct \<C>\<^sub>V_def
   apply (auto simp: \<C>_vars\<^sub>V_def)
  done

lemma \<C>_def2:
  "\<C> = ProgVar ` \<C>\<^sub>V"
  apply(rule equalityI)
   apply(rule subsetI, rename_tac x)
   apply(case_tac x)
   using \<C>_def \<C>_vars_correct \<C>\<^sub>V_def
   apply (auto simp: \<C>\<^sub>V_def)
  done

abbreviation
  global_invariant_rel :: "(('Lock,'Var) Var,'Val) Mem rel"
where
  "global_invariant_rel \<equiv> {(mem,mem'). global_invariant mem \<longrightarrow> global_invariant mem'}"

definition env_consistent :: "(('Lock, 'Var) Var,'Val) Env \<Rightarrow> ('Var,'Val) TyEnv \<Rightarrow> 'Lock set \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> bool"
  where "env_consistent env \<Gamma> \<S> P \<equiv>
    ((fst env) AsmNoWrite, (fst env) AsmNoReadOrWrite) = ((ProgVar ` stable_NoW \<S>, ProgVar ` stable_NoRW \<S>)) \<and>
    ((fst env) GuarNoWrite, (fst env) GuarNoReadOrWrite) = ((ProgVar ` stable_NoW (-\<S>), ProgVar ` stable_NoRW (-\<S>))) \<and>
    (snd env) AsmRel = (insert global_invariant_rel (lock_no_release_rel ` \<S>)) \<and>
    (snd env) GuarRel = (insert global_invariant_rel (lock_no_release_rel ` (-\<S>))) \<and>
    (dom \<Gamma> = {x. ProgVar x \<notin> \<C> \<and> stable \<S> x}) \<and>
    (type_stable \<S> P)"

definition add_anno_dom :: "('Var,'Val) TyEnv \<Rightarrow> 'Lock set \<Rightarrow> 'Lock LockUpd \<Rightarrow> 'Var set"
  where
  "add_anno_dom \<Gamma> \<S> upd \<equiv> case upd of
    Acq l \<Rightarrow> case lock_interp l of (A_W, A_WR) \<Rightarrow> dom \<Gamma> \<union> { v . v \<in> (A_W \<union> A_WR) \<and> ProgVar v \<notin> \<C> } |
    Rel l \<Rightarrow> case lock_interp l of (A_W, A_WR) \<Rightarrow> dom \<Gamma> - {v. v \<notin> (stable_NoW \<S> - A_W) \<union> (stable_NoRW \<S> - A_WR)}"


definition 
  add_anno :: "('Var,'Val) TyEnv \<Rightarrow> 
    'Lock set \<Rightarrow> 
    'Lock LockUpd \<Rightarrow> 
    ('Var,'Val) TyEnv" ("_ \<oplus>\<^sub>_ _" [120, 120, 120] 1000)
where
  "\<Gamma> \<oplus>\<^sub>\<S> upd = restrict_map (\<lambda>x. Some (to_total \<Gamma> x)) (add_anno_dom \<Gamma> \<S> upd)"



definition vars_of_lock :: "'Lock \<Rightarrow> ('Lock, 'Var) Var set"
where
  "vars_of_lock l \<equiv> (ProgVar ` (fst (lock_interp l) \<union> snd (lock_interp l)))"
  
definition add_anno_stable :: "'Lock set \<Rightarrow> 'Lock LockUpd \<Rightarrow> 'Lock set"
  where
  "add_anno_stable \<S> upd \<equiv> case upd of
    Acq l \<Rightarrow> insert l \<S> |
    Rel l \<Rightarrow> \<S> - {l}"

definition pred_entailment :: "('Var,'Val) lpred preds \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> bool" ("_ \<turnstile> _")
where
  "pred_entailment P P' \<equiv> \<forall>mem. pred P mem \<longrightarrow> pred P' mem"

text {*
  We give a predicate interpretation of subtype and then prove it has the correct
  semantic property.
*}
definition
  subtype :: "('Var,'Val) Type \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> ('Var,'Val) Type \<Rightarrow> bool" ("_ \<le>:\<^sub>_ _" [120, 120, 120] 1000)
where
 "t \<le>:\<^sub>P t' \<equiv> (PConj P t') \<turnstile> t"
 
definition
  type_max :: "('Var,'Val) Type \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> Sec"
where
  "type_max t mem \<equiv> if (lpred_eval mem t) then Low else High"

lemma type_stable_is_sufficient':
  "\<lbrakk>type_stable \<S> t\<rbrakk> \<Longrightarrow>
  \<forall>mem mem'. (\<forall>x. stable \<S> x \<longrightarrow> mem x = mem' x) \<longrightarrow> type_max t mem = type_max t mem'"
  using type_stable_is_sufficient
  unfolding type_max_def image_def to_prog_mem_def
  by (metis (no_types, lifting) eval_vars_det\<^sub>B)

lemma subtype_sound:
  "t \<le>:\<^sub>P t' \<Longrightarrow> \<forall>mem. pred P mem \<longrightarrow> type_max t mem \<le> type_max t' mem"
  apply(fastforce simp: subtype_def pred_entailment_def pred_def type_max_def less_eq_Sec_def)
  done

lemma subtype_complete:
  assumes a: "\<And>mem. pred P mem \<Longrightarrow> type_max t mem \<le> type_max t' mem"
  shows "t \<le>:\<^sub>P t'"
unfolding subtype_def pred_entailment_def
proof (clarify)
  fix mem
  assume p: "pred (PConj P t') (mem :: ('Var \<Rightarrow> 'Val))"
  hence "pred P mem"
    by simp
  with a have tmax: "type_max t mem \<le> type_max t' mem" by blast
  from p have t': "pred t' mem"
    by simp
  from t' have "type_max t' mem = Low"
    unfolding type_max_def pred_def by force
  with tmax have "type_max t mem \<le> Low"
    by simp
  hence "type_max t mem = Low"
    unfolding less_eq_Sec_def by blast
  thus "pred t mem"
    unfolding type_max_def pred_def by (auto split: if_splits)
qed

lemma subtype_correct:
  "(t \<le>:\<^sub>P t')  = (\<forall>mem. pred P mem \<longrightarrow> type_max t mem \<le> type_max t' mem)"
  apply(rule iffI)
   apply(simp add: subtype_sound)
  apply(simp add: subtype_complete)
  done

definition
  type_equiv :: "('Var,'Val) Type \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> ('Var,'Val) Type \<Rightarrow> bool" ("_ =:\<^sub>_ _" [120, 120, 120] 1000)
where
  "t =:\<^sub>P t' \<equiv> (t \<le>:\<^sub>P t') \<and> (t' \<le>:\<^sub>P t)"
  

lemma subtype_refl [simp]:
  "t \<le>:\<^sub>P t"
  by(simp add: subtype_def pred_entailment_def pred_def)

lemma type_equiv_refl [simp]:
  "t =:\<^sub>P t"
  by (simp add: type_equiv_def)

lemma subtype_entailment:
  "t \<le>:\<^sub>P t' \<Longrightarrow> P' \<turnstile> P \<Longrightarrow> t \<le>:\<^sub>P' t'"
  apply(auto simp: subtype_def pred_entailment_def)
  done

lemma type_equiv_entailment:
  "t =:\<^sub>P t' \<Longrightarrow> P' \<turnstile> P \<Longrightarrow> t =:\<^sub>P' t'"
  apply(auto simp: type_equiv_def subtype_entailment)
  done

definition
  types_stable :: "('Var,'Val) TyEnv \<Rightarrow> 'Lock set \<Rightarrow> bool"
where
  "types_stable \<Gamma> \<S> \<equiv> \<forall>x\<in>dom \<Gamma>. type_stable \<S> (the (\<Gamma> x))"
  
definition
  tyenv_wellformed :: "(('Lock, 'Var) Var, 'Val) Env \<Rightarrow> ('Var,'Val) TyEnv \<Rightarrow> 'Lock set \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> bool"
where
  "tyenv_wellformed env \<Gamma> \<S> P \<equiv> 
      env_consistent env \<Gamma> \<S> P \<and>
      types_wellformed \<Gamma> \<and> types_stable \<Gamma> \<S>"


lemma pred_entailment_refl [simp]:
  "P \<turnstile> P"
  by(simp add: pred_entailment_def)

definition
  context_equiv :: "('Var,'Val) TyEnv \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> ('Var,'Val) TyEnv \<Rightarrow> bool" ("_ =:\<^sub>_ _" [120, 120, 120] 1000)
where
  "\<Gamma> =:\<^sub>P \<Gamma>' \<equiv> dom \<Gamma> = dom \<Gamma>' \<and>
                           (\<forall>x\<in>dom \<Gamma>'. type_equiv (the (\<Gamma> x)) P (the (\<Gamma>' x)))"

lemma context_equiv_refl[simp]:
  "context_equiv \<Gamma> P \<Gamma>"
  by(simp add: context_equiv_def)

lemma context_equiv_entailment:
  "context_equiv \<Gamma> P \<Gamma>' \<Longrightarrow> P' \<turnstile> P \<Longrightarrow> context_equiv \<Gamma> P' \<Gamma>'"
  apply(auto simp: context_equiv_def dest: type_equiv_entailment)
  done

lemma pred_entailment_trans:
  "P \<turnstile> P' \<Longrightarrow> P' \<turnstile> P'' \<Longrightarrow> P \<turnstile> P''"
  by(auto simp: pred_entailment_def)

  
lemma pred_entailment_PConj:
  "P \<turnstile> P' \<Longrightarrow> P \<turnstile> P'' \<Longrightarrow> P \<turnstile> (PConj P' P'')"
  apply(subst pred_entailment_def)
  apply clarsimp
  apply(fastforce simp: pred_entailment_def)
  done

lemma pred_entailment_mono_PConj:
  "P \<turnstile> P' \<Longrightarrow> (PConj P P'') \<turnstile> (PConj P' P'')"
  apply(auto simp: pred_entailment_def pred_def)
  done
  
lemma subtype_trans:
  "t \<le>:\<^sub>P t' \<Longrightarrow> t' \<le>:\<^sub>P' t'' \<Longrightarrow> P \<turnstile> P' \<Longrightarrow> t \<le>:\<^sub>P t''"
  "t \<le>:\<^sub>P' t' \<Longrightarrow> t' \<le>:\<^sub>P t'' \<Longrightarrow> P \<turnstile> P' \<Longrightarrow> t \<le>:\<^sub>P t''"
   apply(clarsimp simp: subtype_def)
   apply(fastforce simp: pred_entailment_def)
  apply(clarsimp simp: subtype_def)
  apply(fastforce simp: pred_entailment_def)
  done
  
lemma type_equiv_trans:
  "type_equiv t P t' \<Longrightarrow> type_equiv t' P' t'' 
    \<Longrightarrow> P \<turnstile> P' \<Longrightarrow> type_equiv t P t''"
  apply(auto simp: type_equiv_def intro: subtype_trans)
  done

lemma context_equiv_trans:
  "context_equiv \<Gamma> P \<Gamma>' \<Longrightarrow> context_equiv \<Gamma>' P' \<Gamma>'' \<Longrightarrow> P \<turnstile> P' \<Longrightarrow> context_equiv \<Gamma> P \<Gamma>''"
  apply(force simp: context_equiv_def intro: type_equiv_trans)
  done

lemma PConj_entailment_mono:
  "(PConj P P') \<turnstile> P'' \<Longrightarrow> P''' \<turnstile> P \<Longrightarrow> (PConj P''' P') \<turnstile> P''"
  unfolding pred_entailment_def pred_def
  apply force
  done
  
  
  

definition
  var_modifiable :: "'Lock set \<Rightarrow> 'Var \<Rightarrow> bool"
where
  "var_modifiable \<S> x \<equiv> \<forall>l. x \<in> fst (lock_interp l) \<union> snd (lock_interp l) \<longrightarrow> l \<in> \<S>"

definition
  var_readable :: "'Lock set \<Rightarrow> 'Var \<Rightarrow> bool"
where
  "var_readable \<S> x \<equiv> \<forall>l. x \<in>  snd (lock_interp l) \<longrightarrow> l \<in> \<S>"

definition
  aexp_readable :: "'Lock set \<Rightarrow> 'AExp \<Rightarrow> bool"
where
  "aexp_readable \<S> e \<equiv> \<forall>v\<in>aexp_vars e. var_readable \<S> v"

definition
  bexp_readable :: "'Lock set \<Rightarrow> 'BExp \<Rightarrow> bool"
where
  "bexp_readable \<S> e \<equiv> \<forall>v\<in>bexp_vars e. var_readable \<S> v"


(* Note: this can't be an equality on sets and still be preserved by globally consistent
         changes, since somebody else might acquire a lock *)
definition
  stable_consistent :: "'Lock set \<Rightarrow> (('Lock,'Var) Var,'Val) Mem \<Rightarrow> bool"
where
  "stable_consistent \<S> mem \<equiv> \<S> \<subseteq> {l. ev\<^sub>L mem l}"

lemma var_modifiable_global_invariant_preserved:
  assumes modifiable: "var_modifiable \<S> x"
  assumes modified: "mem' = (mem(ProgVar x := blah))"
  assumes ginv: "global_invariant mem"
  assumes consistent: "stable_consistent \<S> mem"
  shows "global_invariant mem'"
proof -
  have locks_unchanged: "\<And>l. ev\<^sub>L mem' l = ev\<^sub>L mem l"
    using modified eval_det\<^sub>L
    by (metis Var.distinct(1) fun_upd_def)
  show ?thesis
  unfolding global_invariant_def
  proof(clarify)
    fix l
    assume not_locked': "\<not> ev\<^sub>L mem' l"
    from not_locked' have not_locked: "\<not> ev\<^sub>L mem l" using locks_unchanged by simp
    with consistent have "l \<notin> \<S>" unfolding stable_consistent_def by blast
    with modifiable have unseen: "x \<notin> fst (lock_interp l) \<union> snd (lock_interp l)"
      unfolding var_modifiable_def by blast
    hence "x \<notin> lpred_vars (lock_inv l)"
      by (metis in_mono lock_invs prod.collapse)
    moreover from ginv not_locked have lock_inv: "lpred_eval (to_prog_mem mem) (lock_inv l)"
      unfolding global_invariant_def by blast
    ultimately show "lpred_eval (to_prog_mem mem') (lock_inv l)"
      using modified 
      by (metis (mono_tags, lifting) Var.inject(2) fun_upd_apply lpred_eval_vars_det to_prog_mem_def)
  qed
qed



definition
  unprotected :: "'Var \<Rightarrow> bool"
where
  "unprotected x \<equiv> \<forall>l. x \<notin> fst (lock_interp l) \<union> snd (lock_interp l)"

definition
  anno_type_stable :: "('Var,'Val) TyEnv \<Rightarrow> 'Lock set \<Rightarrow> 'Lock LockUpd \<Rightarrow> bool"
where
  "anno_type_stable \<Gamma> \<S> upd \<equiv> 
    case upd of 
      (* you can't let go of an assumption on a control variable if there's an assumption
         on any variable it's controlling. this could happen for instance if you rewrite a type
         to depend on a control variable protected by a lock other than the original control 
         var, something that I don't think will happen in practice. Consider instead imposing
         the restriction that all types can mention only variables under the same lock *)
      Rel l \<Rightarrow> (\<forall>v \<in> (fst (lock_interp l) \<union> snd (lock_interp l)). 
        (ProgVar v \<in> \<C>) \<longrightarrow> (\<forall>x \<in> add_anno_dom \<Gamma> \<S> upd. v \<notin> lpred_vars (the (\<Gamma> x)))) |
      Acq l \<Rightarrow> True"

lemma anno_type_stable_trivial [simp]: 
  "anno_type_stable \<Gamma> \<S> (Acq l) = True"
  by (simp add: anno_type_stable_def)

inductive has_type :: "('Var,'Val) TyEnv \<Rightarrow> 'Lock set \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 
                       ('Var, 'Lock, 'AExp, 'BExp) Stmt \<Rightarrow> 
                       ('Var,'Val) TyEnv \<Rightarrow> 'Lock set \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> bool"
  ("\<turnstile> _,_,_ {_} _,_,_" [120, 120, 120, 120, 120, 120, 120] 1000)
  where
  stop_type [intro]: "\<turnstile> \<Gamma>,\<S>,P {Stop} \<Gamma>,\<S>,P" |
  skip_type [intro] : "\<turnstile> \<Gamma>,\<S>,P {Skip} \<Gamma>,\<S>,P" |
  assign\<^sub>\<C> : 
  "\<lbrakk>
    x \<in> \<C>\<^sub>V; var_modifiable \<S> x;
    \<Gamma> \<turnstile>\<^sub>a e \<in> t; 
    P \<turnstile> t; 
    (\<forall>v\<in>dom \<Gamma>. x \<notin> lpred_vars (the (\<Gamma> v))); 
    P' = restrict_preds_to_vars (assign_post P x e)  {v. stable \<S> v}; 
    (\<forall>v. x \<in> \<C>_vars\<^sub>V v \<and> v \<notin> stable_NoRW \<S> \<longrightarrow> 
      (P \<turnstile> (to_total \<Gamma> v)) \<and> 
      (to_total \<Gamma> v) \<le>:\<^sub>P' bexp_to_lpred (dma_type v));
    \<Gamma>' = \<Gamma>; aexp_readable \<S> e
   \<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P {x \<leftarrow> e} \<Gamma>',\<S>,P'" |
  assign\<^sub>1 : 
  "\<lbrakk> x \<notin> dom \<Gamma> ; x \<notin> \<C>\<^sub>V; unprotected x; \<Gamma> \<turnstile>\<^sub>a e \<in> t; t \<le>:\<^sub>P bexp_to_lpred (dma_type x);
     P' = restrict_preds_to_vars (assign_post P x e)  {v. stable \<S> v};
     \<Gamma>' = \<Gamma>; aexp_readable \<S> e \<rbrakk> \<Longrightarrow>
   \<turnstile> \<Gamma>,\<S>,P {x \<leftarrow> e} \<Gamma>',\<S>,P'" |
  assign\<^sub>2 : 
  "\<lbrakk> x \<in> dom \<Gamma> ; var_modifiable \<S> x;  \<Gamma> \<turnstile>\<^sub>a e \<in> t; type_stable \<S> t;
     P' = restrict_preds_to_vars (assign_post P x e)  {v. stable \<S> v};
     x \<notin> stable_NoRW \<S> \<longrightarrow> t \<le>:\<^sub>P' bexp_to_lpred (dma_type x);
     \<Gamma>' = (\<Gamma> (x := Some t)); aexp_readable \<S> e \<rbrakk> \<Longrightarrow>
   has_type \<Gamma> \<S> P (x \<leftarrow> e) \<Gamma>' \<S> P'" |
  if_type [intro]: 
  "\<lbrakk> \<Gamma> \<turnstile>\<^sub>b e \<in> t; P \<turnstile> t; p = bexp_to_lpred e;
     \<turnstile> \<Gamma>,\<S>,(P +\<^sub>\<S> p) { c\<^sub>1 } \<Gamma>',\<S>',P'; \<turnstile> \<Gamma>,\<S>,(P +\<^sub>\<S> (PNeg p)) { c\<^sub>2 } \<Gamma>'',\<S>',P''; 
     context_equiv \<Gamma>' P' \<Gamma>'''; context_equiv \<Gamma>'' P'' \<Gamma>'''; P' \<turnstile> P'''; P'' \<turnstile> P'''; 
     \<forall>env. tyenv_wellformed env \<Gamma>' \<S>' P' \<longrightarrow> tyenv_wellformed env \<Gamma>''' \<S>' P'''; 
     \<forall>env. tyenv_wellformed env \<Gamma>'' \<S>' P'' \<longrightarrow> tyenv_wellformed env \<Gamma>''' \<S>' P''';
     bexp_readable \<S> e \<rbrakk> \<Longrightarrow> 
   \<turnstile> \<Gamma>,\<S>,P { If e c\<^sub>1 c\<^sub>2 } \<Gamma>''',\<S>',P'''" | 
  while_type [intro]: "\<lbrakk> \<Gamma> \<turnstile>\<^sub>b e \<in> t ; P \<turnstile> t; \<turnstile> \<Gamma>,\<S>,(P +\<^sub>\<S> (bexp_to_lpred e)) { c } \<Gamma>,\<S>,P; bexp_readable \<S> e \<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P { While e c } \<Gamma>,\<S>,P" |
  seq_type [intro]: "\<lbrakk> \<turnstile> \<Gamma>,\<S>,P { c\<^sub>1 } \<Gamma>',\<S>',P' ; \<turnstile> \<Gamma>',\<S>',P' { c\<^sub>2 } \<Gamma>'',\<S>'',P'' \<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P { c\<^sub>1 ;; c\<^sub>2 } \<Gamma>'',\<S>'',P''" |
  sub : "\<lbrakk> \<turnstile> \<Gamma>\<^sub>1,\<S>,P\<^sub>1 { c } \<Gamma>\<^sub>1',\<S>',P\<^sub>1' ; context_equiv \<Gamma>\<^sub>2 P\<^sub>2 \<Gamma>\<^sub>1 ; (\<forall>env. tyenv_wellformed env \<Gamma>\<^sub>2 \<S> P\<^sub>2 \<longrightarrow> tyenv_wellformed env \<Gamma>\<^sub>1 \<S> P\<^sub>1);
           (\<forall>env. tyenv_wellformed env \<Gamma>\<^sub>1' \<S>' P\<^sub>1' \<longrightarrow> tyenv_wellformed env \<Gamma>\<^sub>2' \<S>' P\<^sub>2'); context_equiv \<Gamma>\<^sub>1' P\<^sub>1' \<Gamma>\<^sub>2'; P\<^sub>2 \<turnstile> P\<^sub>1; P\<^sub>1' \<turnstile> P\<^sub>2' \<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>\<^sub>2,\<S>,P\<^sub>2 { c } \<Gamma>\<^sub>2',\<S>',P\<^sub>2'" |
  lock_acq_type [intro]: "\<lbrakk> upd = Acq l; \<Gamma>' = \<Gamma> \<oplus>\<^sub>\<S> upd ; \<S>' = add_anno_stable \<S> upd;
                  P' = (PConj P (lock_inv l))
                 \<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P { LockAcq l } \<Gamma>', \<S>',P'" |
  lock_rel_type [intro]: "\<lbrakk> upd = Rel l; \<Gamma>' = \<Gamma> \<oplus>\<^sub>\<S> upd ; \<S>' = add_anno_stable \<S> upd;
              pred_entailment P (lock_inv l);
              P' = restrict_preds_to_vars P {v. stable \<S>' v};
             (\<forall>x\<in>dom \<Gamma>. (the (\<Gamma> x)) \<le>:\<^sub>P' (to_total \<Gamma>' x));
             anno_type_stable \<Gamma> \<S> upd;
             l \<in> \<S>
             \<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P { LockRel l } \<Gamma>', \<S>',P'"



lemma sub':
  "\<lbrakk> context_equiv \<Gamma>\<^sub>2 P\<^sub>2 \<Gamma>\<^sub>1 ;
    (\<forall>env. tyenv_wellformed env \<Gamma>\<^sub>2 \<S> P\<^sub>2 \<longrightarrow> tyenv_wellformed env \<Gamma>\<^sub>1 \<S> P\<^sub>1);
    (\<forall>env. tyenv_wellformed env \<Gamma>\<^sub>1' \<S>' P\<^sub>1' \<longrightarrow> tyenv_wellformed env \<Gamma>\<^sub>2' \<S>' P\<^sub>2');
    context_equiv \<Gamma>\<^sub>1' P\<^sub>1' \<Gamma>\<^sub>2';
    P\<^sub>2 \<turnstile> P\<^sub>1;
    P\<^sub>1' \<turnstile> P\<^sub>2';
    \<turnstile> \<Gamma>\<^sub>1,\<S>,P\<^sub>1 { c } \<Gamma>\<^sub>1',\<S>',P\<^sub>1' \<rbrakk> \<Longrightarrow>
  \<turnstile> \<Gamma>\<^sub>2,\<S>,P\<^sub>2 { c } \<Gamma>\<^sub>2',\<S>',P\<^sub>2'"
  by(rule sub)

lemma conc':
  "\<lbrakk> \<turnstile> \<Gamma>\<^sub>1,\<S>,P { c } \<Gamma>',\<S>',P'; 
    \<Gamma>\<^sub>1 = (\<Gamma>\<^sub>2(x \<mapsto> t)); 
    x \<in> dom \<Gamma>\<^sub>2; 
    type_equiv (the (\<Gamma>\<^sub>2 x)) P t; 
    type_wellformed t; 
    type_stable \<S> t  \<rbrakk> \<Longrightarrow> 
  \<turnstile> \<Gamma>\<^sub>2,\<S>,P { c } \<Gamma>',\<S>',P'"  
  apply(erule sub)
      apply(fastforce simp: context_equiv_def)
     apply(clarsimp simp: tyenv_wellformed_def env_consistent_def)
     apply(rule conjI)
      apply (metis insert_dom)
     apply(rule conjI)
      apply(fastforce simp: types_wellformed_def)
     apply(fastforce simp: types_stable_def)
     apply blast
    apply simp+
  done


lemma tyenv_wellformed_add_pred:
  "tyenv_wellformed env \<Gamma> \<S> P +\<^sub>\<S> p \<Longrightarrow> tyenv_wellformed env \<Gamma> \<S> P"
  apply(clarsimp simp: tyenv_wellformed_def env_consistent_def)
  apply(case_tac "type_stable \<S> p")
   apply(simp add: add_pred_def)+
  done
  
(* This is used all over the place. Not sure what it's new formulation is suposed to be. 
lemma tyenv_wellformed_subset:
  "tyenv_wellformed env \<Gamma> \<S> P \<Longrightarrow> P' \<subseteq> P \<Longrightarrow> tyenv_wellformed env \<Gamma> \<S> P'"
  apply(auto simp: tyenv_wellformed_def env_consistent_def)
  done
*)

(* No easy way to have this now with Exists in the logic since we no longer talk about
   sets of predicates
lemma if_type':
  "\<lbrakk> \<Gamma> \<turnstile>\<^sub>b e \<in> t; bexp_readable \<S> e;
    P \<turnstile> t; 
    \<turnstile> \<Gamma>,\<S>,(P +\<^sub>\<S> e) { c\<^sub>1 } \<Gamma>',\<S>',P'; 
    \<turnstile> \<Gamma>,\<S>,(P +\<^sub>\<S> (bexp_neg e)) { c\<^sub>2 } \<Gamma>',\<S>',P''; 
    P''' \<subseteq> P' \<inter> P'' \<rbrakk> \<Longrightarrow> 
  \<turnstile> \<Gamma>,\<S>,P { If e c\<^sub>1 c\<^sub>2 } \<Gamma>',\<S>',P'''"
  apply(erule (3) if_type)
       apply(rule context_equiv_refl)
      apply(rule context_equiv_refl)
     apply(blast intro: subset_entailment)+
   apply(blast intro: tyenv_wellformed_subset)+
  done
*)

lemma while_type':
  "\<lbrakk> \<Gamma> \<turnstile>\<^sub>b e \<in> t ; bexp_readable \<S> e;
    P \<turnstile> t;
    \<turnstile> \<Gamma>,\<S>,(P +\<^sub>\<S> (bexp_to_lpred e)) { c } \<Gamma>,\<S>,P \<rbrakk> \<Longrightarrow>
  \<turnstile> \<Gamma>,\<S>,P { While e c } \<Gamma>,\<S>,P"
  using while_type by simp

lemma skip_type':
  "\<lbrakk>\<Gamma> = \<Gamma>'; \<S> = \<S>'; P = P'\<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P {Skip} \<Gamma>',\<S>',P'"
  using skip_type by simp


lemma stop_type':
  "\<lbrakk>\<Gamma> = \<Gamma>'; \<S> = \<S>'; P = P'\<rbrakk> \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P {Stop} \<Gamma>',\<S>',P'"
  using stop_type by simp

lemma assign\<^sub>2':
  "\<lbrakk> x \<in> dom \<Gamma> ; var_modifiable \<S> x; \<Gamma> \<turnstile>\<^sub>a e \<in> t; type_stable \<S> t;
     P' = restrict_preds_to_vars (assign_post P x e)  {v. stable \<S> v};
     x \<notin> stable_NoRW \<S> \<longrightarrow> t \<le>:\<^sub>P' bexp_to_lpred (dma_type x);
     \<Gamma>' = (\<Gamma> (x := Some t)); aexp_readable \<S> e;
     \<S>' = \<S> \<rbrakk> \<Longrightarrow>
   has_type \<Gamma> \<S> P (x \<leftarrow> e) \<Gamma>' \<S>' P'"
  using assign\<^sub>2 by simp

subsection {* Typing Soundness *}

text {* The following predicate is needed to exclude some pathological
  cases, that abuse the @{term Stop} command which is not allowed to
  occur in actual programs. *}


inductive_cases has_type_elim: "\<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P'"
inductive_cases has_type_stop_elim: "\<turnstile> \<Gamma>,\<S>,P { Stop } \<Gamma>',\<S>',P'"

definition tyenv_eq :: "('Var,'Val) TyEnv \<Rightarrow> (('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> (('Lock, 'Var) Var, 'Val) Mem \<Rightarrow> bool"
  (infix "=\<index>" 60)
  where "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 \<equiv> (\<forall> x. (type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) = Low \<longrightarrow> mem\<^sub>1 (ProgVar x) = mem\<^sub>2 (ProgVar x))) \<and>
                          (\<forall>l. mem\<^sub>1 (Lock l) = mem\<^sub>2 (Lock l))"


lemma to_prog_mem_dma_eq:
  "to_prog_mem mem = to_prog_mem mem' \<Longrightarrow> dma mem = dma mem'"
  apply(rule ext, rename_tac x)
  apply(case_tac x)
   using dma_correct locks_are_Low apply auto
  done

definition
  dma\<^sub>V :: "('Var,'Val) Mem \<Rightarrow> 'Var \<Rightarrow> Sec"
where
  "dma\<^sub>V mem v \<equiv> dma (\<lambda>x. case x of ProgVar y \<Rightarrow> mem y | _ \<Rightarrow> undefined) (ProgVar v)"

lemma to_prog_mem_default_simp [simp]:
  "to_prog_mem (\<lambda>x. case x of ProgVar y \<Rightarrow> mem y | _ \<Rightarrow> undefined) = mem"
  apply(simp add: to_prog_mem_def)
  done

lemma dma\<^sub>V_dma [simp]:
  "dma\<^sub>V (to_prog_mem mem) x = dma mem (ProgVar x)"
  apply(simp add: dma\<^sub>V_def)
  apply(rule_tac x="ProgVar x" in fun_cong)
  apply(rule to_prog_mem_dma_eq)
  apply(simp add: to_prog_mem_def)
  done

lemma type_max_dma_type [simp]:
  "type_max (bexp_to_lpred (dma_type x)) mem = dma\<^sub>V mem x"
  using dma_correct unfolding type_max_def apply (auto simp: dma\<^sub>V_def bexp_to_lpred_correct)
  done

lemma \<C>\<^sub>V_Low [simp]:
  "x \<in> \<C>\<^sub>V \<Longrightarrow> dma\<^sub>V mem x = Low"
  apply(simp add: \<C>\<^sub>V_def dma\<^sub>V_def)
  using \<C>_Low  by blast

lemma \<C>_ProgVar:
  "x \<in> \<C> \<Longrightarrow> \<exists>y. x = ProgVar y \<and> y \<in> \<C>\<^sub>V"
  apply(case_tac x) using \<C>_def \<C>_vars_correct \<C>\<^sub>V_def apply auto
  done
  
text {*
  This result followed trivially for Mantel et al., but we need to know that the
  type environment is wellformed.
*}
lemma tyenv_eq_sym': 
  "dom \<Gamma> \<inter> \<C>\<^sub>V = {} \<Longrightarrow> types_wellformed \<Gamma> \<Longrightarrow> mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 \<Longrightarrow> mem\<^sub>2 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>1"
proof(clarsimp simp: tyenv_eq_def)
  fix x
  assume a: "\<forall>x. type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) = Low \<longrightarrow> mem\<^sub>1 (ProgVar x) = mem\<^sub>2 (ProgVar x)"
  assume b: "dom \<Gamma> \<inter> \<C>\<^sub>V = {}"
  have eq_\<C>: "\<forall>x\<in>\<C>. mem\<^sub>1 x = mem\<^sub>2 x"
  proof
    fix x
    assume in\<C>: "x \<in> \<C>" from this obtain y where x_def: "x = ProgVar y" and in\<C>\<^sub>V: "y \<in> \<C>\<^sub>V"
      using \<C>_ProgVar by blast
    have "to_prog_mem mem\<^sub>1 y = to_prog_mem mem\<^sub>2 y"
      using a to_total_def \<C>\<^sub>V_Low type_max_dma_type
      by (metis IntI b empty_iff in\<C>\<^sub>V to_prog_mem_def)
    thus "mem\<^sub>1 x = mem\<^sub>2 x"
      using x_def
      by (simp add: to_prog_mem_def)
  qed
  hence "dma mem\<^sub>1 = dma mem\<^sub>2"
    by (rule dma_\<C>)
  hence dma_type_eq: "type_max (bexp_to_lpred (dma_type x)) (to_prog_mem mem\<^sub>1) = type_max (bexp_to_lpred (dma_type x)) (to_prog_mem mem\<^sub>2)"
    by(simp)
  assume c: "types_wellformed \<Gamma>"
  
  assume d: "type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>2) = Low"
  show "mem\<^sub>2 (ProgVar x) = mem\<^sub>1 (ProgVar x)"
  proof(cases "x \<in> dom \<Gamma>")
    assume in_dom: "x \<in> dom \<Gamma>"
    from this obtain t where t: "\<Gamma> x = Some t" by blast
    from this in_dom c have "type_wellformed t" by (force simp: types_wellformed_def)
    hence "\<forall>x\<in> lpred_vars t. (to_prog_mem mem\<^sub>1) x = (to_prog_mem mem\<^sub>2) x"
      using eq_\<C> unfolding type_wellformed_def to_prog_mem_def by blast
    hence t_eq: "type_max t (to_prog_mem mem\<^sub>1) = type_max t (to_prog_mem mem\<^sub>2)" 
      unfolding type_max_def using lpred_eval_vars_det by metis
    with in_dom t have "to_total \<Gamma> x = t"
      by (auto simp: to_total_def)
    with t_eq have "type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>2) = type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1)" by simp
    with d have "type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) = Low" by simp
    with a show ?thesis by (metis sym)
  next
    assume "x \<notin> dom \<Gamma>"
    hence "to_total \<Gamma> x =  bexp_to_lpred (dma_type x)"
      by (auto simp: to_total_def)
    with dma_type_eq have "type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>2) = type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1)" by simp
    with d have "type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) = Low" by simp
    with a show ?thesis by (metis sym)
  qed
qed

lemma tyenv_eq_sym:
  "tyenv_wellformed env \<Gamma> \<S> P \<Longrightarrow> mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 \<Longrightarrow> mem\<^sub>2 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>1"
  apply(rule tyenv_eq_sym')
    apply(clarsimp simp: \<C>\<^sub>V_def tyenv_wellformed_def env_consistent_def)
    apply fastforce
   apply(simp add: tyenv_wellformed_def)
  by assumption

lemma stable_consistent_tyenv_eq:
  assumes con: "stable_consistent \<S> mem\<^sub>1"
  assumes eq: "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  shows "stable_consistent \<S> mem\<^sub>2"
  using con eq apply(clarsimp simp: stable_consistent_def tyenv_eq_def) using eval_det\<^sub>L
  using Collect_cong by blast

inductive_set \<R>\<^sub>1 :: "('Var,'Val) TyEnv \<Rightarrow> 'Lock set \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> 
                     (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf rel"
  and \<R>\<^sub>1_abv :: "
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  ('Var,'Val) TyEnv \<Rightarrow> 'Lock set \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow>
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  bool" ("_ \<R>\<^sup>1\<^bsub>_,_,_\<^esub> _" [120, 120, 120, 120, 120] 1000)
  for \<Gamma>' :: "('Var,'Val) TyEnv"
  and \<S>' :: "'Lock set"
  and P' :: "('Var,'Val) lpred preds"
  where
  "x \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> y \<equiv> (x, y) \<in> \<R>\<^sub>1 \<Gamma> \<S> P" |
  intro [intro!] : "\<lbrakk> \<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P' ; tyenv_wellformed env \<Gamma> \<S> P ; mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2;
                      pred P (to_prog_mem mem\<^sub>1); pred P (to_prog_mem mem\<^sub>2); 
                      global_invariant mem\<^sub>1; global_invariant mem\<^sub>2;
                      stable_consistent \<S> mem\<^sub>1;
                      \<forall>x\<in>dom \<Gamma>. ProgVar x\<notin>(fst env) AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) (to_prog_mem mem\<^sub>1) \<le> dma mem\<^sub>1 (ProgVar x)
                     \<rbrakk> \<Longrightarrow> 
                    \<langle>c, env, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c, env, mem\<^sub>2\<rangle>"

inductive \<R>\<^sub>3_aux :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
                 ('Var,'Val) TyEnv \<Rightarrow> 'Lock set \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
                 bool" ("_ \<R>\<^sup>3\<^bsub>_,_,_\<^esub> _" [120, 120, 120, 120, 120] 1000)
  and \<R>\<^sub>3 :: "('Var,'Val) TyEnv \<Rightarrow> 'Lock set \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf rel"
  where
  "\<R>\<^sub>3 \<Gamma>' \<S>' P' \<equiv> {(lc\<^sub>1, lc\<^sub>2). \<R>\<^sub>3_aux lc\<^sub>1 \<Gamma>' \<S>' P' lc\<^sub>2}" |
  intro\<^sub>1 [intro] : "\<lbrakk> \<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>; \<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P' \<rbrakk> \<Longrightarrow>
                      \<langle>Seq c\<^sub>1 c, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>Seq c\<^sub>2 c, mds, mem\<^sub>2\<rangle>" |
  intro\<^sub>3 [intro] : "\<lbrakk> \<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>; \<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P' \<rbrakk> \<Longrightarrow>
                      \<langle>Seq c\<^sub>1 c, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>Seq c\<^sub>2 c, mds, mem\<^sub>2\<rangle>"

(* A weaker property than bisimulation to reason about the sub-relations of \<R>: *)
definition weak_bisim :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf rel \<Rightarrow>
                        (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf rel \<Rightarrow> bool"
  where "weak_bisim \<T>\<^sub>1 \<T> \<equiv> \<forall> c\<^sub>1 c\<^sub>2 mds mem\<^sub>1 mem\<^sub>2 c\<^sub>1' mds' mem\<^sub>1'.
  ((\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>) \<in> \<T>\<^sub>1 \<and>
   (\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>)) \<longrightarrow>
  (\<exists> c\<^sub>2' mem\<^sub>2'. \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle> \<and> 
                (\<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>, \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>) \<in> \<T>)"

inductive_set \<R> :: "('Var,'Val) TyEnv \<Rightarrow> 'Lock set \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow>
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf rel"
  and \<R>_abv :: "
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  ('Var, 'Val) TyEnv \<Rightarrow> 'Lock set \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow>
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  bool" ("_ \<R>\<^sup>u\<^bsub>_,_,_\<^esub> _" [120, 120, 120, 120, 120] 1000)
  for \<Gamma> :: "('Var,'Val) TyEnv"
  and \<S> :: "'Lock set"
  and P :: "('Var,'Val) lpred preds"
  where
  "x \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> y \<equiv> (x, y) \<in> \<R> \<Gamma> \<S> P" |
  intro\<^sub>1: "lc \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> lc' \<Longrightarrow> (lc, lc') \<in> \<R> \<Gamma> \<S> P" |
  intro\<^sub>3: "lc \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> lc' \<Longrightarrow> (lc, lc') \<in> \<R> \<Gamma> \<S> P"

(* Some eliminators for the above relations *)
inductive_cases \<R>\<^sub>1_elim [elim]: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>"
inductive_cases \<R>\<^sub>3_elim [elim]: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>"

inductive_cases \<R>_elim [elim]: "(\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>) \<in> \<R> \<Gamma> \<S> P"
inductive_cases \<R>_elim': "(\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2\<rangle>) \<in> \<R> \<Gamma> \<S> P"
inductive_cases \<R>\<^sub>1_elim' : "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2\<rangle>"
inductive_cases \<R>\<^sub>3_elim' : "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds\<^sub>2, mem\<^sub>2\<rangle>"


lemma \<R>\<^sub>1_mem_eq: "\<langle>c\<^sub>1, env, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, env, mem\<^sub>2\<rangle> \<Longrightarrow> mem\<^sub>1 =\<^bsub>(fst env)\<^esub>\<^sup>l mem\<^sub>2"
proof (erule \<R>\<^sub>1_elim)
  fix \<Gamma> \<S> P
  assume wf: "tyenv_wellformed env \<Gamma> \<S> P"
  hence env_consistent: "env_consistent env \<Gamma> \<S> P"
    unfolding tyenv_wellformed_def by blast
  assume tyenv_eq: "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assume leq: "\<forall>x\<in>dom \<Gamma>. ProgVar x \<notin> (fst env) AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) (to_prog_mem mem\<^sub>1) \<le> dma mem\<^sub>1 (ProgVar x)"
  assume pred: "pred P (to_prog_mem mem\<^sub>1)"
  
  show "mem\<^sub>1 =\<^bsub>(fst env)\<^esub>\<^sup>l mem\<^sub>2"
  unfolding low_mds_eq_def
  proof(clarify)
    fix x
    assume is_Low: "dma mem\<^sub>1 x = Low"
    assume is_readable: "x \<in> \<C> \<or> x \<notin> (fst env) AsmNoReadOrWrite"
    show "mem\<^sub>1 x = mem\<^sub>2 x"
    proof(cases x)
      fix l
      assume x_def: "x = Lock l"
      with tyenv_eq show ?thesis
        unfolding tyenv_eq_def by blast
    next
      fix v
      assume x_def: "x = ProgVar v"
      show ?thesis
      proof(cases "v \<in> dom \<Gamma>")
        assume in_dom: "v \<in> dom \<Gamma>"
        with x_def env_consistent have "x \<notin> \<C>"
          unfolding env_consistent_def by blast
        with is_readable have "x \<notin> (fst env) AsmNoReadOrWrite"
          by blast
       
        with in_dom leq  have "type_max (to_total \<Gamma> v) (to_prog_mem mem\<^sub>1) \<le> dma mem\<^sub>1 x"
          unfolding to_total_def
          by (auto simp: x_def)
        with is_Low have "type_max (to_total \<Gamma> v) (to_prog_mem mem\<^sub>1) = Low"
          by(simp add: less_eq_Sec_def)
        with tyenv_eq show ?thesis
          unfolding tyenv_eq_def using x_def by blast
      next         
        assume nin_dom: "v \<notin> dom \<Gamma>"
        with is_Low have "type_max (to_total \<Gamma> v) (to_prog_mem mem\<^sub>1) = Low"
          unfolding to_total_def
          by (simp add: x_def)
        with tyenv_eq x_def show ?thesis
          unfolding tyenv_eq_def by blast
      qed
    qed
  qed
qed

lemma \<R>\<^sub>1_dma_eq:
  "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<Longrightarrow> dma mem\<^sub>1 = dma mem\<^sub>2"
  apply(drule \<R>\<^sub>1_mem_eq)
  apply(erule low_mds_eq_dma)
  done


(* \<R> meets the criteria of a "simple bisim"
   which can be used to simplify the establishment of a refinement relation *)
lemma bisim_simple_\<R>\<^sub>1:
  "\<langle>c, mds, mem\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c', mds', mem'\<rangle> \<Longrightarrow> c = c'"
  apply(cases rule: \<R>\<^sub>1.cases, simp+)
  done

lemma bisim_simple_\<R>\<^sub>3:
  "lc \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> lc' \<Longrightarrow> (fst (fst lc)) = (fst (fst lc'))"
  apply(induct rule: \<R>\<^sub>3_aux.induct)
  using bisim_simple_\<R>\<^sub>1 apply clarsimp
  apply simp
  done

lemma bisim_simple_\<R>\<^sub>u:
  "lc \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> lc' \<Longrightarrow> (fst (fst lc)) = (fst (fst lc'))"
  apply(induct rule: \<R>.induct)
   apply clarsimp
   apply(cases rule: \<R>\<^sub>1.cases, simp+)
  apply(cases rule: \<R>\<^sub>3_aux.cases, simp+)
   apply blast
  using bisim_simple_\<R>\<^sub>3 apply clarsimp
  done


(* To prove that \<R> is a bisimulation, we first show symmetry *)

lemma \<C>_eq_type_max_eq:
  assumes wf: "type_wellformed t"
  assumes \<C>_eq: "\<forall>x\<in>\<C>\<^sub>V. mem\<^sub>1 x = mem\<^sub>2 x" 
  shows "type_max t mem\<^sub>1 = type_max t mem\<^sub>2"
proof -
  have "\<forall>x\<in>lpred_vars t. mem\<^sub>1 x = mem\<^sub>2 x"
    using wf \<C>_eq unfolding type_wellformed_def2 by blast
  thus ?thesis
    unfolding type_max_def using lpred_eval_vars_det by metis
qed

lemma vars_of_type_eq_type_max_eq:
  assumes mem_eq: "\<forall>x\<in>lpred_vars t. mem\<^sub>1 x = mem\<^sub>2 x" 
  shows "type_max t mem\<^sub>1 = type_max t mem\<^sub>2"
proof -
  from assms show ?thesis
    unfolding type_max_def using lpred_eval_vars_det by metis
qed

lemma \<R>\<^sub>1_sym: "sym (\<R>\<^sub>1 \<Gamma>' \<S>' P')"
unfolding sym_def
proof clarsimp
  fix c env mem c' env' mem'
  assume in_\<R>\<^sub>1: "\<langle>c, env, mem\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c', env', mem'\<rangle>"
  let ?pmem = "to_prog_mem mem"
  let ?pmem' = "to_prog_mem mem'"
  let ?mds = "fst env"
  let ?mds' = "fst env'"
  let ?agrels = "snd env"
  let ?agrels' = "snd env"
  from in_\<R>\<^sub>1 obtain \<Gamma> \<S> P where
  stuff: "c' = c"  "env' = env"  "\<turnstile> \<Gamma>,\<S>,P {c} \<Gamma>',\<S>',P'"  "tyenv_wellformed env \<Gamma> \<S> P"
  "mem =\<^bsub>\<Gamma>\<^esub> mem'" "pred P ?pmem" "pred P ?pmem'" "global_invariant mem" "global_invariant mem'"
  "\<forall>x\<in>dom \<Gamma>. ProgVar x \<notin> ?mds AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) ?pmem \<le> dma mem (ProgVar x)"
  "stable_consistent \<S> mem"
    using \<R>\<^sub>1_elim' by blast+  (* slowish *)
  from stuff have stuff': "mem' =\<^bsub>\<Gamma>\<^esub> mem"
    by (metis tyenv_eq_sym)
  
  have sec': "\<forall>x\<in>dom \<Gamma>. (ProgVar x) \<notin> ?mds AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) ?pmem' \<le> dma mem' (ProgVar x)"
  proof -
    from in_\<R>\<^sub>1 have "dma mem = dma mem'"
      using \<R>\<^sub>1_dma_eq stuff by metis
    moreover have "\<forall>x\<in>dom \<Gamma>. type_max (the (\<Gamma> x)) ?pmem = type_max (the (\<Gamma> x)) ?pmem'"
    proof
      fix x
      assume "x \<in> dom \<Gamma>"
      hence "type_wellformed (the (\<Gamma> x))"
        using `tyenv_wellformed env \<Gamma> \<S> P`
        by(auto simp: tyenv_wellformed_def types_wellformed_def)
      moreover have "\<forall>x\<in>\<C>. mem x = mem' x"
        using in_\<R>\<^sub>1 \<R>\<^sub>1_mem_eq \<C>_Low stuff
        unfolding low_mds_eq_def by auto
      hence "\<forall>x\<in>\<C>\<^sub>V. ?pmem x = ?pmem' x"
        by(auto simp: \<C>\<^sub>V_def to_prog_mem_def)
      ultimately
      show "type_max (the (\<Gamma> x)) ?pmem = type_max (the (\<Gamma> x)) ?pmem'"
        using \<C>_eq_type_max_eq by blast
    qed
    ultimately show ?thesis
      using stuff(10) by fastforce
  qed

  from `stable_consistent \<S> mem` `tyenv_eq \<Gamma> mem mem'` have "stable_consistent \<S> mem'"
    by(blast intro: stable_consistent_tyenv_eq)

  with sec' stuff stuff'
  show "\<langle>c', env', mem'\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c, env, mem\<rangle>"
  by (metis (no_types) \<R>\<^sub>1.intro)
qed

lemma \<R>\<^sub>3_sym: "sym (\<R>\<^sub>3 \<Gamma> \<S> P)"
  unfolding sym_def
proof (clarify)
  fix c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mds' mem\<^sub>2
  assume asm: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds', mem\<^sub>2\<rangle>"
  hence [simp]: "mds' = mds"
    using \<R>\<^sub>3_elim' by blast
  from asm show "\<langle>c\<^sub>2, mds', mem\<^sub>2\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>"
    apply auto
    apply (induct rule: \<R>\<^sub>3_aux.induct)
     apply (metis (lifting) \<R>\<^sub>1_sym \<R>\<^sub>3_aux.intro\<^sub>1 symD)
    by (metis (lifting) \<R>\<^sub>3_aux.intro\<^sub>3)
qed

lemma \<R>_env [simp]: "\<langle>c\<^sub>1, env, mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, env', mem\<^sub>2\<rangle> \<Longrightarrow> env = env'"
  apply (rule \<R>_elim')
     apply (auto)
   apply (metis \<R>\<^sub>1_elim')
  apply (insert \<R>\<^sub>3_elim')
  by blast

lemma \<R>_sym: "sym (\<R> \<Gamma> \<S> P)"
  unfolding sym_def
proof (clarify)
  fix c\<^sub>1 env mem\<^sub>1 c\<^sub>2 env\<^sub>2 mem\<^sub>2
  assume asm: "(\<langle>c\<^sub>1, env, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, env\<^sub>2, mem\<^sub>2\<rangle>) \<in> \<R> \<Gamma> \<S> P"
  with \<R>_env have [simp]: "env\<^sub>2 = env"
    by blast
  from asm show "(\<langle>c\<^sub>2, env\<^sub>2, mem\<^sub>2\<rangle>, \<langle>c\<^sub>1, env, mem\<^sub>1\<rangle>) \<in> \<R> \<Gamma> \<S> P"
    using \<R>.intro\<^sub>1 [of \<Gamma> \<S> P] and \<R>.intro\<^sub>3 [of _ \<Gamma> \<S> P]
    using \<R>\<^sub>1_sym [of \<Gamma>, THEN symD] and \<R>\<^sub>3_sym [of \<Gamma>, THEN symD]
    apply simp
    apply (erule \<R>_elim)
    by auto
qed

(* Next, we show that the relations are closed under globally consistent changes *)

lemma \<R>\<^sub>1_closed_glob_consistent: "closed_glob_consistent (\<R>\<^sub>1 \<Gamma>' \<S>' P')"
  unfolding closed_glob_consistent_def
proof (clarify)
  fix c\<^sub>1 env mem\<^sub>1 c\<^sub>2 mem\<^sub>2 A
  assume R1: "\<langle>c\<^sub>1, env, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, env, mem\<^sub>2\<rangle>"
  hence [simp]: "c\<^sub>2 = c\<^sub>1" by blast
  let ?mds = "fst env"
  let ?agrels = "snd env"
  let ?pmem\<^sub>1 = "to_prog_mem mem\<^sub>1"
  let ?pmem\<^sub>2 = "to_prog_mem mem\<^sub>2"

  assume A_updates_vars: "\<forall>x. case A x of None \<Rightarrow> True | Some (v, v') \<Rightarrow> mem\<^sub>1 x \<noteq> v \<or> mem\<^sub>2 x \<noteq> v' \<longrightarrow> \<not> var_asm_not_written ?mds x"
  assume A_updates_dma: "\<forall>x. dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x \<noteq> dma mem\<^sub>1 x \<longrightarrow> \<not> var_asm_not_written ?mds x"
  assume A_updates_sec: "\<forall>x. dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x = Low \<and> (x \<notin> ?mds AsmNoReadOrWrite \<or> x \<in> \<C>) \<longrightarrow> mem\<^sub>1 [\<parallel>\<^sub>1 A] x = mem\<^sub>2 [\<parallel>\<^sub>2 A] x"
  assume A_updates_rel: " \<forall>R\<in>?agrels AsmRel. (mem\<^sub>1, mem\<^sub>1 [\<parallel>\<^sub>1 A]) \<in> R \<and> (mem\<^sub>2, mem\<^sub>2 [\<parallel>\<^sub>2 A]) \<in> R"
  from R1 obtain \<Gamma> \<S> P where \<Gamma>_props: "\<turnstile> \<Gamma>,\<S>,P { c\<^sub>1 } \<Gamma>',\<S>',P'" "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2" "tyenv_wellformed env \<Gamma> \<S> P"
                                      "pred P ?pmem\<^sub>1" "pred P ?pmem\<^sub>2"
                                      "\<forall>x\<in>dom \<Gamma>. ProgVar x \<notin> ?mds AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) ?pmem\<^sub>1 \<le> dma mem\<^sub>1 (ProgVar x)"
                                      "global_invariant mem\<^sub>1" "global_invariant mem\<^sub>2"
                                      "stable_consistent \<S> mem\<^sub>1"
    by force

  from \<Gamma>_props(3) have stable_not_written: "\<forall>x. stable \<S> x \<longrightarrow> var_asm_not_written ?mds (ProgVar x)"
    by(auto simp: tyenv_wellformed_def env_consistent_def stable_def var_asm_not_written_def)
  with A_updates_vars have stable_unchanged\<^sub>1: "\<forall>x. stable \<S> x \<longrightarrow> (mem\<^sub>1 [\<parallel>\<^sub>1 A]) (ProgVar x) = mem\<^sub>1 (ProgVar x)" and
                           stable_unchanged\<^sub>2: "\<forall>x. stable \<S> x \<longrightarrow> (mem\<^sub>2 [\<parallel>\<^sub>2 A]) (ProgVar x) = mem\<^sub>2 (ProgVar x)"
    by(auto simp: apply_adaptation_def split: option.splits)

  from stable_not_written A_updates_dma 
  have stable_unchanged_dma\<^sub>1: "\<forall>x. stable \<S> x \<longrightarrow> dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) (ProgVar x) = dma mem\<^sub>1 (ProgVar x)"
    by(blast)

  let ?pmem\<^sub>1' = "to_prog_mem (mem\<^sub>1 [\<parallel>\<^sub>1 A])"
  let ?pmem\<^sub>2' = "to_prog_mem (mem\<^sub>2 [\<parallel>\<^sub>2 A])"

  have tyenv_eq': "mem\<^sub>1 [\<parallel>\<^sub>1 A] =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 [\<parallel>\<^sub>2 A]"
  proof(clarsimp simp: tyenv_eq_def, safe)
    fix x
    assume a: "type_max (to_total \<Gamma> x) ?pmem\<^sub>1' = Low"
    show "mem\<^sub>1 [\<parallel>\<^sub>1 A] (ProgVar x) = mem\<^sub>2 [\<parallel>\<^sub>2 A] (ProgVar x)"
    proof(cases "x \<in> dom \<Gamma>")
      assume in_dom: "x \<in> dom \<Gamma>"
      with \<Gamma>_props(3) have "var_asm_not_written ?mds (ProgVar x)"
        by(auto simp: tyenv_wellformed_def env_consistent_def var_asm_not_written_def stable_def)
      hence [simp]: "mem\<^sub>1 [\<parallel>\<^sub>1 A] (ProgVar x) = mem\<^sub>1 (ProgVar x)" and [simp]: "mem\<^sub>2 [\<parallel>\<^sub>2 A] (ProgVar x) = mem\<^sub>2 (ProgVar x)"
        using A_updates_vars by(auto simp: apply_adaptation_def split: option.splits)
      from in_dom a obtain t\<^sub>x where \<Gamma>\<^sub>x: "\<Gamma> x = Some t\<^sub>x" and t\<^sub>x_Low': "type_max t\<^sub>x ?pmem\<^sub>1' = Low"
        by(auto simp: to_total_def)
      have t\<^sub>x_unchanged: "type_max t\<^sub>x ?pmem\<^sub>1'  = type_max t\<^sub>x ?pmem\<^sub>1"
      proof - 
        from \<Gamma>\<^sub>x \<Gamma>_props(3) have t\<^sub>x_stable: "type_stable \<S> t\<^sub>x" and t\<^sub>x_wellformed: "type_wellformed t\<^sub>x"
          by(force simp: tyenv_wellformed_def types_stable_def types_wellformed_def)+
        from t\<^sub>x_stable t\<^sub>x_wellformed stable_unchanged\<^sub>1 show ?thesis
          using type_stable_is_sufficient'
          by(auto simp: to_prog_mem_def)
      qed
      with t\<^sub>x_Low' have t\<^sub>x_Low: "type_max t\<^sub>x ?pmem\<^sub>1 = Low" by simp
      with \<Gamma>\<^sub>x \<Gamma>_props(2) have "mem\<^sub>1 (ProgVar x) = mem\<^sub>2 (ProgVar x)"
        by(force simp: tyenv_eq_def to_total_def split: if_splits)
      thus ?thesis by simp
    next
      assume nin_dom: "x \<notin> dom \<Gamma>"
      with a have is_Low': "dma (mem\<^sub>1[\<parallel>\<^sub>1 A]) (ProgVar x) = Low"
        by(simp add: to_total_def)
      show ?thesis
      proof(cases "ProgVar x \<notin> ?mds AsmNoReadOrWrite \<or> ProgVar x \<in> \<C>")
        assume "ProgVar x \<notin> ?mds AsmNoReadOrWrite \<or> ProgVar x \<in> \<C>"
        with is_Low' show ?thesis
          using A_updates_sec by blast
      next
        assume "\<not> (ProgVar x \<notin> ?mds AsmNoReadOrWrite \<or> ProgVar x \<in> \<C>)"
        hence stable_NoRW': "ProgVar x \<in> ?mds AsmNoReadOrWrite" and nin_\<C>: "ProgVar x \<notin> \<C>"
          by auto
        have "dom \<Gamma> = {x. ProgVar x \<notin> \<C> \<and> stable \<S> x}"
          using \<Gamma>_props(3) unfolding tyenv_wellformed_def env_consistent_def by blast
        with nin_dom nin_\<C> have unstable: "\<not> stable \<S> x" by blast
        from stable_NoRW' have stable_NoRW: "x \<in> stable_NoRW \<S>"
          using \<Gamma>_props(3) unfolding tyenv_wellformed_def env_consistent_def 
          by (metis (no_types, lifting) Var.inject(2) imageE prod.inject)
        with unstable have "False"
          unfolding stable_def by blast
        thus ?thesis by blast
      qed
    qed
  next
    fix l
    have "dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) (Lock l) = Low"
      by(rule locks_are_Low)
    moreover from \<Gamma>_props(3) have "Lock l \<notin> ?mds AsmNoReadOrWrite"
      unfolding tyenv_wellformed_def env_consistent_def by auto
    ultimately show "mem\<^sub>1 [\<parallel>\<^sub>1 A] (Lock l) = mem\<^sub>2 [\<parallel>\<^sub>2 A] (Lock l)"
      using A_updates_sec by auto
  qed
  
  have sec': "\<forall>x\<in>dom \<Gamma>. (ProgVar x) \<notin> ?mds AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) ?pmem\<^sub>1' \<le> dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) (ProgVar x)"
  proof(intro ballI impI)
    fix x
    assume readable: "ProgVar x \<notin> ?mds AsmNoReadOrWrite"
    assume in_dom: "x \<in> dom \<Gamma>"
    with \<Gamma>_props(3) have "var_asm_not_written ?mds (ProgVar x)"
      by(auto simp: tyenv_wellformed_def env_consistent_def var_asm_not_written_def stable_def)
    hence [simp]: "dma mem\<^sub>1 [\<parallel>\<^sub>1 A] (ProgVar x) = dma mem\<^sub>1 (ProgVar x)"
      using A_updates_dma by(auto simp: apply_adaptation_def split: option.splits)
    from in_dom obtain t\<^sub>x where \<Gamma>\<^sub>x: "\<Gamma> x = Some t\<^sub>x"
      by(auto simp: to_total_def)
    have t\<^sub>x_unchanged: "type_max t\<^sub>x ?pmem\<^sub>1'  = type_max t\<^sub>x ?pmem\<^sub>1"
    proof - 
      from \<Gamma>\<^sub>x \<Gamma>_props(3) have t\<^sub>x_stable: "type_stable \<S> t\<^sub>x" and t\<^sub>x_wellformed: "type_wellformed t\<^sub>x"
        by(force simp: tyenv_wellformed_def types_stable_def types_wellformed_def)+
      from t\<^sub>x_stable t\<^sub>x_wellformed stable_unchanged\<^sub>1 show ?thesis
        using type_stable_is_sufficient'
        by (auto simp: to_prog_mem_def)
    qed
    with \<Gamma>\<^sub>x have [simp]:"type_max (the (\<Gamma> x)) ?pmem\<^sub>1' = type_max (the (\<Gamma> x)) ?pmem\<^sub>1"
      by simp
    show "type_max (the (\<Gamma> x)) ?pmem\<^sub>1' \<le> dma mem\<^sub>1 [\<parallel>\<^sub>1 A] (ProgVar x) "
      apply simp
      using in_dom readable \<Gamma>_props by metis
  qed
    
  from stable_unchanged\<^sub>1 stable_unchanged\<^sub>2 \<Gamma>_props(3) have "lpred_eval (to_prog_mem (mem\<^sub>1 [\<parallel>\<^sub>1 A])) P = lpred_eval (to_prog_mem mem\<^sub>1) P \<and> 
        lpred_eval (to_prog_mem (mem\<^sub>2 [\<parallel>\<^sub>2 A])) P = lpred_eval (to_prog_mem mem\<^sub>2) P"
    apply -
    apply(rule conjI)
    by(rule lpred_eval_vars_det,force simp: tyenv_wellformed_def env_consistent_def to_prog_mem_def stable_def)+
    
  hence preds: 
        "pred P ?pmem\<^sub>1' = pred P ?pmem\<^sub>1" 
        "pred P ?pmem\<^sub>2' = pred P ?pmem\<^sub>2"
    by(simp add: pred_def)+

  have "?agrels AsmRel = insert global_invariant_rel (lock_no_release_rel ` \<S>)"
    using `tyenv_wellformed env \<Gamma> \<S> P` unfolding tyenv_wellformed_def env_consistent_def by simp
   

  with  `global_invariant mem\<^sub>1` `global_invariant mem\<^sub>2` A_updates_rel  have
    ginv': "global_invariant mem\<^sub>1 [\<parallel>\<^sub>1 A]" "global_invariant mem\<^sub>2 [\<parallel>\<^sub>2 A]"
    by auto

  have "\<forall>l\<in>\<S>. lock_no_release_rel l \<in> ?agrels AsmRel"
    using `tyenv_wellformed env \<Gamma> \<S> P`
    unfolding tyenv_wellformed_def env_consistent_def by force 
  moreover have "\<forall>l\<in>\<S>. ev\<^sub>L mem\<^sub>1 l" using `stable_consistent \<S> mem\<^sub>1` unfolding stable_consistent_def by blast
  ultimately have "\<forall>l\<in>\<S>. ev\<^sub>L (mem\<^sub>1 [\<parallel>\<^sub>1 A]) l"
    using A_updates_rel unfolding lock_no_release_rel_def by force
  with `stable_consistent \<S> mem\<^sub>1` have "stable_consistent \<S> (mem\<^sub>1 [\<parallel>\<^sub>1 A])"
    unfolding stable_consistent_def
    using mem_Collect_eq subset_eq by blast
 
  with ginv' preds \<Gamma>_props tyenv_eq' sec'
  show "\<langle>c\<^sub>1, env, mem\<^sub>1 [\<parallel>\<^sub>1 A]\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, env, mem\<^sub>2 [\<parallel>\<^sub>2 A]\<rangle>"
    by auto
qed


lemma \<R>\<^sub>3_closed_glob_consistent:
  "closed_glob_consistent (\<R>\<^sub>3 \<Gamma>' \<S>' P')"
unfolding closed_glob_consistent_def
proof(clarsimp cong: option.case_cong)
  fix c\<^sub>1 mds agrels mem\<^sub>1 c\<^sub>2 mem\<^sub>2 A
  assume in_\<R>\<^sub>3: "\<langle>c\<^sub>1, (mds, agrels), mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, (mds, agrels), mem\<^sub>2\<rangle>"
  assume A_modifies_vars: "\<forall>x. case A x of None \<Rightarrow> True | Some (v, v') \<Rightarrow> (mem\<^sub>1 x = v \<longrightarrow> mem\<^sub>2 x \<noteq> v') \<longrightarrow> \<not> var_asm_not_written mds x"
  assume A_modifies_dma: "\<forall>x. dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x \<noteq> dma mem\<^sub>1 x \<longrightarrow> \<not> var_asm_not_written mds x"
  assume A_modifies_sec: "\<forall>x. dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x = Low \<and> (x \<in> mds AsmNoReadOrWrite \<longrightarrow> x \<in> \<C>) \<longrightarrow> mem\<^sub>1 [\<parallel>\<^sub>1 A] x = mem\<^sub>2 [\<parallel>\<^sub>2 A] x"
  assume A_respects_agrels: " \<forall>R\<in>agrels AsmRel. (mem\<^sub>1, mem\<^sub>1 [\<parallel>\<^sub>1 A]) \<in> R \<and> (mem\<^sub>2, mem\<^sub>2 [\<parallel>\<^sub>2 A]) \<in> R"
  (* do a bit of massaging to get the goal state set-up nicely for the induction rule *)
  define lc\<^sub>1 where "lc\<^sub>1 \<equiv> \<langle>c\<^sub>1, (mds, agrels), mem\<^sub>1\<rangle>"
  define lc\<^sub>2 where "lc\<^sub>2 \<equiv> \<langle>c\<^sub>2, (mds, agrels), mem\<^sub>2\<rangle>"
  from lc\<^sub>1_def lc\<^sub>2_def in_\<R>\<^sub>3 have "lc\<^sub>1 \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> lc\<^sub>2" by simp
  from this lc\<^sub>1_def lc\<^sub>2_def A_modifies_vars A_modifies_dma A_modifies_sec A_respects_agrels
  show "\<langle>c\<^sub>1, (mds, agrels), mem\<^sub>1 [\<parallel>\<^sub>1 A]\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, (mds, agrels), mem\<^sub>2 [\<parallel>\<^sub>2 A]\<rangle>"
proof(induct arbitrary: c\<^sub>1 mds agrels mem\<^sub>1 c\<^sub>2 mem\<^sub>2 rule: \<R>\<^sub>3_aux.induct)
  case (intro\<^sub>1 c\<^sub>1 mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mem\<^sub>2 c \<Gamma>' \<S>' P' mds agrels)
  show ?case
    apply (rule \<R>\<^sub>3_aux.intro\<^sub>1[OF _ intro\<^sub>1(2)])
    using \<R>\<^sub>1_closed_glob_consistent intro\<^sub>1
    unfolding closed_glob_consistent_def by(auto cong: option.case_cong)
next
  case (intro\<^sub>3 c\<^sub>1 mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mem\<^sub>2 c \<Gamma>' \<S>' P' mds agrels)
  show ?case
    apply(rule \<R>\<^sub>3_aux.intro\<^sub>3)
     apply(rule intro\<^sub>3(2))
     using intro\<^sub>3 apply simp+
     done
  qed
qed


lemma \<R>_closed_glob_consistent: "closed_glob_consistent (\<R> \<Gamma>' \<S>' P')"
  unfolding closed_glob_consistent_def
proof (clarify, erule \<R>_elim, simp_all cong: option.case_cong)
  fix c\<^sub>1 mem\<^sub>1 c\<^sub>2 mem\<^sub>2 A mds agrels
  assume R1: "\<langle>c\<^sub>1, (mds,agrels), mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, (mds,agrels), mem\<^sub>2\<rangle>"
  and A_modifies_vars: "\<forall>x. case A x of None \<Rightarrow> True | Some (v, v') \<Rightarrow> (mem\<^sub>1 x = v \<longrightarrow> mem\<^sub>2 x \<noteq> v') \<longrightarrow> \<not> var_asm_not_written mds x"
  and A_modifies_dma: "\<forall>x. dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x \<noteq> dma mem\<^sub>1 x \<longrightarrow> \<not> var_asm_not_written mds x"
  and A_modifies_sec: "\<forall>x. dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x = Low \<and> (x \<in> mds AsmNoReadOrWrite \<longrightarrow> x \<in> \<C>) \<longrightarrow> mem\<^sub>1 [\<parallel>\<^sub>1 A] x = mem\<^sub>2 [\<parallel>\<^sub>2 A] x"  
  and A_respects_agrels: " \<forall>R\<in>agrels AsmRel. (mem\<^sub>1, mem\<^sub>1 [\<parallel>\<^sub>1 A]) \<in> R \<and> (mem\<^sub>2, mem\<^sub>2 [\<parallel>\<^sub>2 A]) \<in> R"
  show
    "\<langle>c\<^sub>1, (mds,agrels), mem\<^sub>1 [\<parallel>\<^sub>1 A]\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, (mds,agrels), mem\<^sub>2 [\<parallel>\<^sub>2 A]\<rangle>"
    apply(rule intro\<^sub>1)
    apply clarify
    using \<R>\<^sub>1_closed_glob_consistent unfolding closed_glob_consistent_def
    using R1 A_modifies_vars A_modifies_dma A_modifies_sec A_respects_agrels
    by (auto cong: option.case_cong)
next
  fix c\<^sub>1 mem\<^sub>1 c\<^sub>2 mem\<^sub>2 x A mds agrels
  assume R3: "\<langle>c\<^sub>1, (mds,agrels), mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, (mds,agrels), mem\<^sub>2\<rangle>"
  and A_modifies_vars: "\<forall>x. case A x of None \<Rightarrow> True | Some (v, v') \<Rightarrow> (mem\<^sub>1 x = v \<longrightarrow> mem\<^sub>2 x \<noteq> v') \<longrightarrow> \<not> var_asm_not_written mds x"
  and A_modifies_dma: "\<forall>x. dma (mem\<^sub>1 [\<parallel>\<^sub>1 A]) x \<noteq> dma mem\<^sub>1 x \<longrightarrow> \<not> var_asm_not_written mds x"
  and A_modifies_sec: "\<forall>x. dma mem\<^sub>1 [\<parallel>\<^sub>1 A] x = Low \<and> (x \<in> mds AsmNoReadOrWrite \<longrightarrow> x \<in> \<C>) \<longrightarrow> mem\<^sub>1 [\<parallel>\<^sub>1 A] x = mem\<^sub>2 [\<parallel>\<^sub>2 A] x"
  and A_respects_agrels: " \<forall>R\<in>agrels AsmRel. (mem\<^sub>1, mem\<^sub>1 [\<parallel>\<^sub>1 A]) \<in> R \<and> (mem\<^sub>2, mem\<^sub>2 [\<parallel>\<^sub>2 A]) \<in> R"
  show "\<langle>c\<^sub>1, (mds,agrels), mem\<^sub>1 [\<parallel>\<^sub>1 A]\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, (mds,agrels), mem\<^sub>2 [\<parallel>\<^sub>2 A]\<rangle> "
    apply(rule intro\<^sub>3)
    using \<R>\<^sub>3_closed_glob_consistent unfolding closed_glob_consistent_def
    using R3 A_modifies_vars A_modifies_dma A_modifies_sec A_respects_agrels
    by (auto cong: option.case_cong)
qed

(* It now remains to show that steps of the first of two related configurations
    can be matched by the second: *)

lemma types_wellformed_mode_update:
  "types_wellformed \<Gamma> \<Longrightarrow>
   types_wellformed (\<Gamma> \<oplus>\<^sub>\<S> upd)"
  unfolding types_wellformed_def add_anno_def to_total_def restrict_map_def
  by (force split: if_splits)


lemma lock_acq_mds_upd_AsmNoWrite:
  "fst env AsmNoWrite = ProgVar ` stable_NoW \<S> \<Longrightarrow>
       lock_interp x1 = (x1a, x2) \<Longrightarrow>
       lock_acq_mds_upd x1 (fst env) AsmNoWrite = ProgVar ` stable_NoW (insert x1 \<S>)"
  apply(clarsimp simp: lock_acq_mds_upd_def stable_NoW_def)
  apply force
  done

lemma lock_acq_mds_upd_AsmNoReadOrWrite:
  "fst env AsmNoReadOrWrite = ProgVar ` stable_NoRW \<S> \<Longrightarrow>
   lock_interp x1 = (x1a, x2) \<Longrightarrow>
   lock_acq_mds_upd x1 (fst env) AsmNoReadOrWrite = ProgVar ` stable_NoRW (insert x1 \<S>)"
  apply(clarsimp simp: lock_acq_mds_upd_def stable_NoRW_def)
  apply force
  done

lemma lock_acq_mds_upd_GuarNoWrite:
  "fst env GuarNoWrite = ProgVar ` stable_NoW (- \<S>) \<Longrightarrow>
   lock_interp x1 = (x1a, x2) \<Longrightarrow>
   lock_acq_mds_upd x1 (fst env) GuarNoWrite = ProgVar ` stable_NoW (- insert x1 \<S>)"
  apply(clarsimp simp: lock_acq_mds_upd_def stable_NoW_def)
  apply(rule equalityI)
   apply force
  apply(clarsimp simp: image_def | safe)+
  using lone_lock_per_var by auto  

lemma lock_acq_mds_upd_GuarNoReadOrWrite:
  "fst env GuarNoReadOrWrite = ProgVar ` stable_NoRW (- \<S>) \<Longrightarrow>
   lock_interp x1 = (x1a, x2) \<Longrightarrow>
   lock_acq_mds_upd x1 (fst env) GuarNoReadOrWrite = ProgVar ` stable_NoRW (- insert x1 \<S>)"
  apply(clarsimp simp: lock_acq_mds_upd_def stable_NoRW_def)
  apply(rule equalityI)
   apply force
  apply(clarsimp simp: image_def | safe)+
  using lone_lock_per_var by auto  

text {* 
   a LOT of technical lemmas follow to show how lock_no_release_rel and global_invariant_rel
   do and don't interact
*}
lemma lock_no_release_rel_inject:
  "lock_no_release_rel l \<subseteq> lock_no_release_rel l' \<Longrightarrow> l' = l"
proof(rule ccontr)
  assume noteq: "l' \<noteq> l"
  assume subset: "lock_no_release_rel l \<subseteq> lock_no_release_rel l'"
  from locks_can_be_taken obtain v where
  v: "ev\<^sub>L (undefined(Lock l := v)) l" by blast
  from locks_can_be_taken obtain v' where
  v': "ev\<^sub>L (undefined(Lock l := v,Lock l' := v')) l'" by blast
  let ?mem = "(undefined(Lock l := v,Lock l' := v'))"
  from locks_can_be_released obtain v'' where
  v'': "\<not> ev\<^sub>L (?mem(Lock l' := v'')) l'" by blast
  
  from v' v noteq have v: "ev\<^sub>L ?mem l" using eval_det\<^sub>L
    by (metis Var.inject(1) fun_upd_def)
  let ?mem' = "(?mem(Lock l' := v''))"
  from v noteq have "ev\<^sub>L ?mem' l" using eval_det\<^sub>L
    by (metis Var.inject(1) fun_upd_def)
  with v have "(?mem,?mem') \<in> lock_no_release_rel l"
    by(simp add: lock_no_release_rel_def)
  moreover have "(?mem,?mem') \<notin> lock_no_release_rel l'"
    using v' v'' by (simp add: lock_no_release_rel_def)
  ultimately show "False" using subset by blast
qed

lemma Lock_finite:
  "finite (X::'Lock set)"
  apply(rule_tac B=UNIV in finite_subset, simp)
  apply(rule_tac f="\<lambda>l. Lock l" in Finite_Set.finite_imageD)
   apply(rule_tac B=UNIV in finite_subset)
    apply simp
   using Var_finite apply simp
  by (meson Var.inject(1) injI)

lemma any_lock_can_be_unlocked:
  "\<exists>mem. (\<forall>l\<in>L. \<not> ev\<^sub>L mem l)"
using Lock_finite[of L] proof(induct rule: finite.induct)
  show "\<exists>mem. \<forall>l\<in>{}. \<not> ev\<^sub>L mem l" by simp
next
  fix L l
  assume "\<exists>mem. \<forall>l\<in>L. \<not> ev\<^sub>L mem l"
  from this obtain mem where mem: "\<forall>l\<in>L. \<not> ev\<^sub>L mem l" by blast
  from locks_can_be_released obtain v where mem': "\<not> ev\<^sub>L (mem(Lock l := v)) l" by blast
  show "\<exists>mem. \<forall>l\<in>insert l L. \<not> ev\<^sub>L mem l"
  proof(cases "l \<in> L")
    assume "l \<in> L"
    with mem show ?thesis  by auto
  next
    assume notin: "l \<notin> L"
    hence "\<And>l'. l'\<in>L \<Longrightarrow> ev\<^sub>L (mem(Lock l := v)) l' = ev\<^sub>L mem l'"
      by (metis Var.inject(1) eval_det\<^sub>L fun_upd_other)
    hence " \<forall>l'\<in>insert l L. \<not> ev\<^sub>L (mem(Lock l := v)) l'"
      using mem mem' notin by auto
    thus ?thesis by blast
  qed
qed

lemma any_lock_can_be_locked:
  "\<exists>mem. (\<forall>l\<in>L. ev\<^sub>L mem l)"
using Lock_finite[of L] proof(induct rule: finite.induct)
  show "\<exists>mem. \<forall>l\<in>{}. ev\<^sub>L mem l" by simp
next
  fix L l
  assume "\<exists>mem. \<forall>l\<in>L. ev\<^sub>L mem l"
  from this obtain mem where mem: "\<forall>l\<in>L. ev\<^sub>L mem l" by blast
  from locks_can_be_taken obtain v where mem': "ev\<^sub>L (mem(Lock l := v)) l" by blast
  show "\<exists>mem. \<forall>l\<in>insert l L. ev\<^sub>L mem l"
  proof(cases "l \<in> L")
    assume "l \<in> L"
    with mem show ?thesis  by auto
  next
    assume notin: "l \<notin> L"
    hence "\<And>l'. l'\<in>L \<Longrightarrow> ev\<^sub>L (mem(Lock l := v)) l' = ev\<^sub>L mem l'"
      by (metis Var.inject(1) eval_det\<^sub>L fun_upd_other)
    hence " \<forall>l'\<in>insert l L. ev\<^sub>L (mem(Lock l := v)) l'"
      using mem mem' notin by auto
    thus ?thesis by blast
  qed
qed

lemma any_lock_can_be_locked_or_unlocked:
  "\<exists>mem. (\<forall>l\<in>L. ev\<^sub>L mem l) \<and> (\<forall>l\<in>-L. \<not> ev\<^sub>L mem l)"
proof -
  from any_lock_can_be_locked obtain mem where
  mem: "(\<forall>l\<in>L. ev\<^sub>L mem l)" by blast
  from any_lock_can_be_unlocked obtain mem' where
  mem': "(\<forall>l\<in>-L. \<not> ev\<^sub>L mem' l)" by blast
  define mem'' where "mem'' \<equiv> \<lambda>x. case x of Lock l \<Rightarrow> if l \<in> L then mem (Lock l) else mem' (Lock l) |
                             ProgVar x \<Rightarrow> mem (ProgVar x)"
  have "(\<forall>l\<in>L. ev\<^sub>L mem'' l) \<and> (\<forall>l\<in>-L. \<not> ev\<^sub>L mem'' l)"
    apply(rule conjI)
     using mem eval_det\<^sub>L mem''_def
     apply (metis (full_types, lifting) ComplD Var.simps(5))
    using mem' eval_det\<^sub>L mem''_def
    by (metis (full_types, lifting) ComplD Var.simps(5))
  thus ?thesis by blast
qed
  


lemma global_invariant_rel_lock_no_release_rel:
  "global_invariant_rel = lock_no_release_rel l \<Longrightarrow> False"
proof(cases "\<exists>mem mem'. lpred_eval mem (lock_inv l) \<and> \<not> lpred_eval mem' (lock_inv l)")
  assume equiv: "global_invariant_rel = lock_no_release_rel l"
  assume " \<exists>mem mem'. lpred_eval mem (lock_inv l) \<and> \<not> lpred_eval mem' (lock_inv l)"
  from this obtain mem mem' where mem: "lpred_eval mem (lock_inv l)" and mem': "\<not> lpred_eval mem' (lock_inv l)"
    by blast+
  from any_lock_can_be_locked_or_unlocked[where L="-{l}"] obtain lmem where
  lmem: "(\<forall>l'. l' \<noteq> l \<longrightarrow> ev\<^sub>L lmem l') \<and> \<not> ev\<^sub>L lmem l" by auto
  define mem\<^sub>L where "mem\<^sub>L \<equiv> \<lambda>x. case x of ProgVar x \<Rightarrow> mem x 
                          | Lock _ \<Rightarrow> lmem x"
  define mem\<^sub>L' where "mem\<^sub>L' \<equiv> \<lambda>x. case x of ProgVar x \<Rightarrow> mem' x 
                          | Lock _ \<Rightarrow> lmem x"
  have a: "\<And>x. (to_prog_mem mem\<^sub>L) x = mem x" by(simp add: mem\<^sub>L_def to_prog_mem_def)
  have b: "\<And>x. (to_prog_mem mem\<^sub>L') x = mem' x" by(simp add: mem\<^sub>L'_def to_prog_mem_def)
  have lock_inv: "lpred_eval (to_prog_mem mem\<^sub>L) (lock_inv l)"
       "\<not> lpred_eval (to_prog_mem mem\<^sub>L') (lock_inv l)"
    using mem mem' a b lpred_eval_vars_det by metis+
  moreover have eq: "\<forall>l. ev\<^sub>L mem\<^sub>L l = ev\<^sub>L lmem l \<and> ev\<^sub>L mem\<^sub>L' l = ev\<^sub>L lmem l"
    using mem\<^sub>L'_def mem\<^sub>L'_def eval_det\<^sub>L
    by (metis Var.simps(5) mem\<^sub>L_def)
  ultimately have "global_invariant mem\<^sub>L" "\<not> global_invariant mem\<^sub>L'"
    using lmem
    unfolding global_invariant_def by auto
  hence "(mem\<^sub>L,mem\<^sub>L') \<notin> global_invariant_rel"
    by(simp)
  moreover have "(mem\<^sub>L,mem\<^sub>L') \<in> lock_no_release_rel l"
    by(simp add: lock_no_release_rel_def eq)
  ultimately show "False" using equiv by blast
next
  assume equiv: "global_invariant_rel = lock_no_release_rel l"
  assume "\<not> (\<exists>mem mem'. lpred_eval mem (lock_inv l) \<and> \<not> lpred_eval mem' (lock_inv l))"
  hence all_eq: "\<forall>mem mem'. lpred_eval mem (lock_inv l) = lpred_eval mem' (lock_inv l)" by blast
  with lock_invs_sat have triv: "\<forall>mem. lpred_eval mem (lock_inv l)" by blast
  from any_lock_can_be_locked_or_unlocked[where L="-{l}"] obtain lmem where
  lmem: "(\<forall>l'. l' \<noteq> l \<longrightarrow> ev\<^sub>L lmem l') \<and> \<not> ev\<^sub>L lmem l" by auto
  from locks_can_be_taken obtain v where a: "ev\<^sub>L (lmem(Lock l := v)) l" by blast
  let ?lmem' = "(lmem(Lock l := v))"
  have c: "\<And>l'. ev\<^sub>L ?lmem' l'"
    apply(case_tac "l' = l")
     apply(simp add: a)
    using lmem eval_det\<^sub>L 
    by (metis Var.pred_inject(1) fun_upd_apply)
  with lmem have "(?lmem',lmem) \<notin> lock_no_release_rel l" unfolding lock_no_release_rel_def by blast
  moreover
  from c have gi': "global_invariant ?lmem'" unfolding global_invariant_def by simp
  from lmem triv have gi: "global_invariant lmem" unfolding global_invariant_def by auto
  from gi' gi have "(?lmem',lmem) \<in> global_invariant_rel" by simp
  ultimately show "False" using equiv by blast
qed
  

  
lemma lock_acq_agrels_upd_GuarRel:
  "snd env GuarRel = insert global_invariant_rel (lock_no_release_rel ` (- \<S>)) \<Longrightarrow>
   lock_acq_agrels_upd x1 (snd env) GuarRel =
   insert global_invariant_rel (lock_no_release_rel ` (- insert x1 \<S>))"
  apply(clarsimp simp: lock_acq_agrels_upd_def)
  apply(auto simp: image_def dest: global_invariant_rel_lock_no_release_rel simp: lock_no_release_rel_inject)+
  done

lemma lock_rel_mds_upd_AsmNoWrite:
  "fst env AsmNoWrite = ProgVar ` stable_NoW \<S> \<Longrightarrow>
   lock_rel_mds_upd l (fst env) AsmNoWrite = ProgVar ` stable_NoW (\<S> - {l})"
  apply(clarsimp simp: lock_rel_mds_upd_def split: prod.splits simp: stable_NoW_def)
  apply(clarsimp simp: image_def)
  apply(rule equalityI)
   apply clarsimp
   apply(metis DiffI fst_conv singletonD)
  using lone_lock_per_var
  by auto

lemma lock_rel_mds_upd_AsmNoReadOrWrite:
  "fst env AsmNoReadOrWrite = ProgVar ` stable_NoRW \<S> \<Longrightarrow>
  lock_rel_mds_upd l (fst env) AsmNoReadOrWrite = ProgVar ` stable_NoRW (\<S> - {l})"
  apply(clarsimp simp: lock_rel_mds_upd_def split: prod.splits simp: stable_NoRW_def)
  apply(clarsimp simp: image_def)
  apply(rule equalityI)
   apply fastforce
  using lone_lock_per_var by auto

lemma lock_rel_mds_upd_GuarNoWrite:
  "fst env GuarNoWrite = ProgVar ` stable_NoW (- \<S>) \<Longrightarrow>
   lock_rel_mds_upd l (fst env) GuarNoWrite = ProgVar ` stable_NoW (insert l (- \<S>))"
  apply(clarsimp simp: lock_rel_mds_upd_def split: prod.splits simp: stable_NoW_def)
  apply(clarsimp simp: image_def)
  apply(rule equalityI)
   apply auto[1]
  using lone_lock_per_var by auto

lemma lock_rel_mds_upd_GuarNoReadOrWrite:
  "fst env GuarNoReadOrWrite = ProgVar ` stable_NoRW (- \<S>) \<Longrightarrow>
   lock_rel_mds_upd l (fst env) GuarNoReadOrWrite = ProgVar ` stable_NoRW (insert l (- \<S>))"
  apply(clarsimp simp: lock_rel_mds_upd_def split: prod.splits simp: stable_NoRW_def)
  apply(clarsimp simp: image_def)
  apply(rule equalityI)
   apply auto[1]
  using lone_lock_per_var by auto

lemma lock_rel_agrels_upd_AsmRel:
  " snd env AsmRel = insert global_invariant_rel (lock_no_release_rel ` \<S>) \<Longrightarrow>
         lock_rel_agrels_upd l (snd env) AsmRel =
         insert global_invariant_rel (lock_no_release_rel ` (\<S> - {l}))"
  apply(clarsimp simp: lock_rel_agrels_upd_def)
  apply(auto simp: image_def dest: global_invariant_rel_lock_no_release_rel simp: lock_no_release_rel_inject)+
  done

lemma lock_rel_agrels_upd_GuarRel:
  "snd env GuarRel = insert global_invariant_rel (lock_no_release_rel ` (- \<S>)) \<Longrightarrow>
   lock_rel_agrels_upd l (snd env) GuarRel =
   insert global_invariant_rel (insert (lock_no_release_rel l) (lock_no_release_rel ` (- \<S>)))"
  apply(clarsimp simp: lock_rel_agrels_upd_def)
  apply(auto simp: image_def dest: global_invariant_rel_lock_no_release_rel simp: lock_no_release_rel_inject)+
  done

(* Adaptation of technical lemma *)
(* This is saying that if the mode state is consistent with the type,
   then after updating both of them as required by "upd", they remain consistent *)
lemma mode_update_add_anno:
  "env_consistent env \<Gamma> \<S> P \<Longrightarrow> 
   env_consistent (update_env upd env) 
                  (\<Gamma> \<oplus>\<^sub>\<S> upd) 
                  (add_anno_stable \<S> upd) 
                  (P |`' {v. stable (add_anno_stable \<S> upd) v})"
  apply(case_tac upd)
   apply(clarsimp simp: update_env_def  add_anno_stable_def env_consistent_def
                       add_anno_def stable_def  add_anno_dom_def
                  split:prod.splits)
   apply(rule conjI)
    apply (fastforce simp: lock_acq_mds_upd_AsmNoWrite)
   apply(rule conjI)
    apply (fastforce simp: lock_acq_mds_upd_AsmNoReadOrWrite)
   apply(rule conjI)
    apply (fastforce simp: lock_acq_mds_upd_GuarNoWrite)
   apply(rule conjI)
    apply (fastforce simp: lock_acq_mds_upd_GuarNoReadOrWrite)
   apply(rule conjI)
    apply(fastforce simp: lock_acq_agrels_upd_def)
   apply(rule conjI)
    apply(fastforce simp: lock_acq_agrels_upd_GuarRel)
   apply(rule conjI)
    apply(clarsimp simp: stable_NoW_def stable_NoRW_def)
    apply fastforce
   using restrict_preds_to_vars_restricts
   apply (metis (no_types, lifting) mem_Collect_eq rev_subsetD)
  apply(clarsimp simp:env_consistent_def)
  apply(rule conjI)
   apply(clarsimp simp: add_anno_stable_def update_env_def split: prod.splits)
   apply(fastforce simp: lock_rel_mds_upd_AsmNoWrite)
  apply(rule conjI)
   apply(clarsimp simp: add_anno_stable_def update_env_def split: prod.splits)
   apply(fastforce simp: lock_rel_mds_upd_AsmNoReadOrWrite)
  apply(rule conjI)
   apply(clarsimp simp: add_anno_stable_def update_env_def split: prod.splits)
   apply(fastforce simp: lock_rel_mds_upd_GuarNoWrite)
  apply(rule conjI)
   apply(clarsimp simp: add_anno_stable_def update_env_def split: prod.splits)
   apply(fastforce simp: lock_rel_mds_upd_GuarNoReadOrWrite)
  apply(rule conjI)
   apply(clarsimp simp: add_anno_stable_def update_env_def split: prod.splits)
   apply(fastforce simp: lock_rel_agrels_upd_AsmRel)
  apply(rule conjI)
   apply(clarsimp simp: add_anno_stable_def update_env_def split: prod.splits)
   apply(fastforce simp: lock_rel_agrels_upd_GuarRel)
   apply(rule conjI)
   (* old proofs that we don't want to re-examine here *)
   apply(clarsimp simp: add_anno_def)
   apply(clarsimp simp: add_anno_dom_def)
   apply(clarsimp simp: Image_eq_UN split:prod.splits)
   apply(rename_tac NoW NoRW)
   apply(clarsimp simp: stable_def Image_eq_UN stable_NoRW_def stable_NoW_def)
   apply(clarsimp simp: add_anno_stable_def Image_eq_UN)
   apply(rule set_eqI)
   apply clarsimp
   apply safe
  apply(clarsimp simp:stable_def add_anno_stable_def restrict_preds_to_vars_def Ball_Collect[symmetric]
                 split:prod.splits)
  apply((metis (no_types, lifting) UnCI fst_conv snd_conv DiffI old.prod.inject prod.inject prod.collapse singletonD lone_lock_per_var)+)[15]
  using restrict_preds_to_vars_restricts
  by (meson contra_subsetD mem_Collect_eq)

lemma types_mention_only_control_vars:
  assumes a2: "types_wellformed \<Gamma>"
  assumes a3: "v \<in> lpred_vars t"
  assumes a4: "\<Gamma> x = Some t"
  shows "ProgVar v \<in> \<C>"
proof -
  from a2 a3 a4 have "x \<in> dom \<Gamma>"
    by blast
  then show "ProgVar v \<in> \<C>"
    using a4 a3 a2  by (metis Union_iff image_eqI option.sel subsetCE type_wellformed_def types_wellformed_def)
qed


fun
  lock_of :: "'Lock LockUpd \<Rightarrow> 'Lock"
where
  "lock_of (Acq l) = l" |
  "lock_of (Rel l) = l"

lemma types_stable_mode_update:
   "types_stable \<Gamma> \<S> \<Longrightarrow> types_wellformed \<Gamma> \<Longrightarrow> anno_type_stable \<Gamma> \<S> upd \<Longrightarrow>
    types_stable (\<Gamma> \<oplus>\<^sub>\<S> upd) (add_anno_stable \<S> upd)"
proof -
  assume "types_stable \<Gamma> \<S>"
         "types_wellformed \<Gamma>"
         "anno_type_stable \<Gamma> \<S> upd"
  define l where "l \<equiv> lock_of upd"
  obtain NoW NoRW where interp [simp]: "lock_interp l = (NoW,NoRW)" using prod.exhaust by metis
  define l_footprint where "l_footprint \<equiv> NoW \<union> NoRW"
  with lock_interp_\<C>_vars\<^sub>V have
  "\<And>x c. ProgVar c \<in> \<C>_vars (ProgVar x) \<Longrightarrow>
     (c \<in> fst (lock_interp l) \<union> snd (lock_interp l)) = (x \<in> fst (lock_interp l) \<union> snd (lock_interp l))"
    by blast
  show "types_stable \<Gamma> \<oplus>\<^sub>\<S> upd (add_anno_stable \<S> upd)"
  proof(case_tac upd)
    fix l'
    assume "upd = Acq l'"
    hence upd_def [simp]: "upd = Acq l"
      by(simp add: l_def)
    show "types_stable \<Gamma> \<oplus>\<^sub>\<S> upd (add_anno_stable \<S> upd)"
    proof(clarsimp simp: types_stable_def)
      fix x v t
      assume xt: "(\<Gamma> \<oplus>\<^sub>\<S> Acq l) x = Some t"
      assume v: "v \<in> lpred_vars t"
      from xt have "\<Gamma> x = Some t \<or> (x \<in> l_footprint \<and> t = bexp_to_lpred (dma_type x))"
        by(clarsimp simp: add_anno_def add_anno_dom_def restrict_map_def to_total_def l_footprint_def split: if_splits prod.splits)
      thus "stable (add_anno_stable \<S> (Acq l)) v"
      proof
        assume "\<Gamma> x = Some t"
        thus ?thesis
          using `types_stable \<Gamma> \<S>` v 
          apply(clarsimp simp: stable_def add_anno_stable_def types_stable_def stable_NoRW_def stable_NoW_def)
          by force
      next
        assume a: "x \<in> l_footprint \<and> t = bexp_to_lpred (dma_type x)"
        hence "\<C>_vars\<^sub>V x \<subseteq> l_footprint"
          using lock_interp_\<C>_vars\<^sub>V unfolding l_footprint_def using interp
          using \<C>_vars\<^sub>V_def \<open>\<And>x c. ProgVar c \<in> \<C>_vars (ProgVar x) \<Longrightarrow> (c \<in> fst (lock_interp l) \<union> snd (lock_interp l)) = (x \<in> fst (lock_interp l) \<union> snd (lock_interp l))\<close> by fastforce
        moreover have "v \<in> \<C>_vars\<^sub>V x"
          using a v \<C>_vars_correct \<C>_vars\<^sub>V_def
          by (simp add: bexp_to_lpred_vars imageI mem_Collect_eq)
        ultimately have "v \<in> l_footprint"
          by blast
        thus "stable (add_anno_stable \<S> (Acq l)) v"
          using l_footprint_def interp
          by(auto simp: stable_def add_anno_stable_def)
      qed
    qed
  next
    fix l'
    assume "upd = Rel l'"
    hence upd_def [simp]: "upd = Rel l"
      by(simp add: l_def)
    show "types_stable \<Gamma> \<oplus>\<^sub>\<S> upd (add_anno_stable \<S> upd)"
    proof(clarsimp simp: types_stable_def)
      fix x p v t
      assume xt: "(\<Gamma> \<oplus>\<^sub>\<S> Rel l) x = Some t"
      assume v: "v \<in> lpred_vars t"
      from xt have \<Gamma>x: "\<Gamma> x = Some t" and disj: "(x \<in> stable_NoW \<S> \<and> x \<notin> NoW \<or> x \<in> stable_NoRW \<S> \<and> x \<notin> NoRW)"
        apply(clarsimp simp:  add_anno_def add_anno_dom_def restrict_map_def to_total_def split: if_splits prod.splits)+
        done
      from \<Gamma>x disj have "x \<in> add_anno_dom \<Gamma> \<S> upd"
        by(auto simp add: add_anno_dom_def)

      moreover from `anno_type_stable \<Gamma> \<S> upd` have
        "\<forall>v\<in>l_footprint.  ProgVar v \<in> \<C> \<longrightarrow> (\<forall>x\<in>add_anno_dom \<Gamma> \<S> upd. v \<notin> lpred_vars (the (\<Gamma> x)))" 
        unfolding anno_type_stable_def
        by (clarsimp simp: l_footprint_def)

      moreover have "ProgVar v \<in> \<C>" using v \<Gamma>x  
        using \<open>types_wellformed \<Gamma>\<close> types_mention_only_control_vars by auto

      ultimately have vnotin: "v \<notin> l_footprint"
        using \<Gamma>x v 
        using option.sel by fastforce
      
      have "stable \<S> v"
        using \<Gamma>x v `types_stable \<Gamma> \<S>` unfolding types_stable_def
        using domI option.sel by force
      with vnotin show "stable (add_anno_stable \<S> (Rel l)) v"
      by (auto simp: stable_def add_anno_stable_def stable_NoRW_def stable_NoW_def l_footprint_def)
    qed
  qed
qed

lemma tyenv_wellformed_env_update:
  "tyenv_wellformed env \<Gamma> \<S> P \<Longrightarrow> anno_type_stable \<Gamma> \<S> upd \<Longrightarrow>
   tyenv_wellformed (update_env upd env) 
                  (\<Gamma> \<oplus>\<^sub>\<S> upd) 
                  (add_anno_stable \<S> upd) 
                  (P |`' {v. stable (add_anno_stable \<S> upd) v})"
  apply(clarsimp simp: tyenv_wellformed_def)
  apply(rule conjI)
   apply(blast intro: mode_update_add_anno)
  apply(rule conjI)
   apply(blast intro: types_wellformed_mode_update)
  apply(blast intro: types_stable_mode_update)
  done

lemma stop_cxt :
  "\<lbrakk> \<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P' ; c = Stop \<rbrakk> \<Longrightarrow> 
  context_equiv \<Gamma> P \<Gamma>'\<and> \<S>' = \<S> \<and>  P \<turnstile> P' \<and> (\<forall>env. tyenv_wellformed env \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed env \<Gamma>' \<S> P')"
  apply (induct rule: has_type.induct)
          apply (auto intro: context_equiv_trans context_equiv_entailment pred_entailment_trans)
  done


lemma lpred_vars_tyenv_wellformed_pres:
  " lpred_vars Pw \<subseteq> lpred_vars P \<Longrightarrow>
    \<forall>env. tyenv_wellformed env \<Gamma> L P \<longrightarrow> tyenv_wellformed env \<Gamma> L Pw"
  apply(clarsimp simp: tyenv_wellformed_def)
  apply(clarsimp simp: env_consistent_def)
  apply (meson mem_Collect_eq subsetCE)
  done



lemma tyenv_wellformed_preds_update:
  "lpred_vars P' \<subseteq> {v. stable \<S> v} \<Longrightarrow>
  tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<Longrightarrow> tyenv_wellformed (mds,agrels) \<Gamma> \<S> P'"
  apply(clarsimp simp: tyenv_wellformed_def)
  apply(clarsimp simp: env_consistent_def)
  by (meson mem_Collect_eq subsetCE)





lemma env_consistent_preds_tyenv_update:
  "lpred_vars P' \<subseteq>  {v. stable \<S> v} \<Longrightarrow> v \<in> dom \<Gamma> \<Longrightarrow>
  env_consistent mds \<Gamma> \<S> P \<Longrightarrow> env_consistent mds (\<Gamma>(v \<mapsto> t)) \<S> P'"
  apply(clarsimp simp: env_consistent_def)
  apply(clarsimp simp:  add_pred_def split: if_splits | safe)+
  by (meson mem_Collect_eq subsetCE)



lemma types_wellformed_update:
  "types_wellformed \<Gamma> \<Longrightarrow> type_wellformed t \<Longrightarrow> types_wellformed (\<Gamma>(x \<mapsto> t))"
  by(auto simp: types_wellformed_def)

lemma types_stable_update:
  "types_stable \<Gamma> \<S> \<Longrightarrow> type_stable \<S> t \<Longrightarrow> types_stable (\<Gamma>(x \<mapsto> t)) \<S>"
  by(auto simp: types_stable_def)

(* this is used below too, not sure how it's supposed to be phrased now  
lemma tyenv_wellformed_sub:
  "\<lbrakk>P\<^sub>1 \<subseteq> P\<^sub>2;
  \<Gamma>\<^sub>2 = \<Gamma>\<^sub>1; tyenv_wellformed (mds,agrels) \<Gamma>\<^sub>2 \<S> P\<^sub>2\<rbrakk> \<Longrightarrow>
     tyenv_wellformed (mds,agrels) \<Gamma>\<^sub>1 \<S> P\<^sub>1"
  apply(clarsimp simp: tyenv_wellformed_def | safe)+
  apply(clarsimp simp: env_consistent_def)
  apply(drule (1) subsetD, fastforce)
  done
*)

abbreviation
  tyenv_sec :: "('Lock, 'Var) Var Mds \<Rightarrow> ('Var,'Val) TyEnv \<Rightarrow> (('Lock, 'Var) Var,'Val) Mem \<Rightarrow> bool"
where
  "tyenv_sec mds \<Gamma> mem \<equiv> \<forall>x\<in>dom \<Gamma>. ProgVar x \<notin> mds AsmNoReadOrWrite \<longrightarrow> type_max (the (\<Gamma> x)) (to_prog_mem mem) \<le> dma mem (ProgVar x)"

(* This lemma is saying that type environment security is preserved by a lock acquisition *)
lemma tyenv_sec_lock_acq:
assumes
  stable_def_mds: "((ProgVar ` stable_NoW \<S>, ProgVar ` stable_NoRW \<S>)) = (mds AsmNoWrite, mds AsmNoReadOrWrite)" and
  stable_upd: "\<S>'' = add_anno_stable \<S> upd" and
  type_was_stable: "type_stable \<S> P" and
  tyenv_upd: "\<Gamma>'' = \<Gamma> \<oplus>\<^sub>\<S> upd" and
  tyenv_sec: "tyenv_sec mds \<Gamma> mem" and
  upd_def: "upd = Acq l"
shows
  "tyenv_sec (lock_acq_mds_upd l mds) \<Gamma>'' mem"
proof(clarsimp)
  fix x t l
  assume
    "\<Gamma>'' x = Some t"
    "ProgVar x \<notin> lock_acq_mds_upd l mds AsmNoReadOrWrite"
  with assms
  show "type_max t (to_prog_mem mem) \<le> dma mem (ProgVar x)"
   apply clarsimp
   apply(clarsimp simp:add_anno_def to_total_def split:if_splits)
   apply(clarsimp simp:type_max_def)
   apply(drule_tac x=x in fun_cong)
   apply(clarsimp split:if_splits) 
   apply safe
    apply(erule_tac x=x in ballE)
     apply(clarsimp split:if_splits)
     apply(clarsimp simp: lock_acq_mds_upd_def to_prog_mem_def split:prod.splits if_splits)
     apply(rename_tac NoW NoRW )
     apply(clarsimp simp:restrict_map_def split: if_splits)
     using less_eq_Sec_def apply auto[1]
    using less_eq_Sec_def
    apply auto[1]
   apply(erule_tac x=x in ballE)
    apply(clarsimp split:if_splits)
    apply(clarsimp simp: lock_acq_mds_upd_def to_prog_mem_def split:prod.splits if_splits)
    apply(rename_tac NoW NoRW)
    apply(clarsimp simp:restrict_map_def split: if_splits)
    apply(clarsimp simp:less_eq_Sec_def)
    apply(clarsimp simp: to_prog_mem_def dma_correct split:if_splits simp: bexp_to_lpred_correct)
   apply clarsimp
   apply(clarsimp simp: lock_acq_mds_upd_def to_prog_mem_def split:prod.splits if_splits)
   apply(rename_tac NoW NoRW)
   apply(clarsimp simp:restrict_map_def split: if_splits)
   apply(clarsimp simp:less_eq_Sec_def)
   apply(clarsimp simp: to_prog_mem_def dma_correct split:if_splits simp: bexp_to_lpred_correct)
   done
qed

lemma tyenv_sec_lock_rel:
assumes
  new_entails_old: "(\<forall>x\<in>dom \<Gamma>. (the ( \<Gamma> x)) \<le>:\<^sub>P''  (to_total \<Gamma>'' x))" and
  P''_sat: "pred P'' (to_prog_mem mem')" and
  stable_def_mds: "((ProgVar ` stable_NoW \<S>, ProgVar ` stable_NoRW \<S>)) = (mds AsmNoWrite, mds AsmNoReadOrWrite)" and
  stable_upd: "\<S>'' = add_anno_stable \<S> upd" and
  type_was_stable: "type_stable \<S> P" and
  P''_def: "P'' = P |`' {v. stable \<S>'' v}" and
  tyenv_upd: "\<Gamma>'' = \<Gamma> \<oplus>\<^sub>\<S> upd" and
  tyenv_sec: "tyenv_sec mds \<Gamma> mem" and
  upd_def: "upd = Rel l" and
  mem'_l: "\<not> ev\<^sub>L mem' l" and
  mem'_x: "\<forall>x. x \<noteq> Lock l \<longrightarrow> mem' x = mem x"
shows
  "tyenv_sec (lock_rel_mds_upd l mds) \<Gamma>'' mem'"
proof(clarsimp)
next
  (* The Rel case *)
  fix x t
  assume
    x_keeps_type: "\<Gamma>'' x = Some t" and
    x_NoRW_dropped: "ProgVar x \<notin> lock_rel_mds_upd l mds AsmNoReadOrWrite"
  (* x continues to have a type, so it must have continued to be stable *)
  from x_keeps_type tyenv_upd stable_upd upd_def
  have x_stable': "stable \<S>'' x"
    unfolding stable_def stable_NoW_def stable_NoRW_def add_anno_def restrict_map_def add_anno_dom_def add_anno_stable_def
    by (fastforce split:LockUpd.splits if_splits)
  (* for it to continue to be stable,
     there must also be an Asm for x that isn't managed by the lock being released *)
  from x_keeps_type x_stable' tyenv_upd upd_def
  have l_x_not_both: "x \<notin> fst (lock_interp l) \<or> x \<notin> snd (lock_interp l)"
    unfolding add_anno_def restrict_map_def add_anno_dom_def
    by (clarsimp split:LockUpd.splits if_splits)

  (* If NoRW was dropped for x, we know that AsmNoRW is managed by the lock released *)
  from x_NoRW_dropped upd_def
  have l_NoRW_x: "ProgVar x \<in> mds AsmNoReadOrWrite \<Longrightarrow> x \<in> snd (lock_interp l)"
    unfolding lock_rel_mds_upd_def
    by (clarsimp split:prod.splits)
  (* We also know AsmNoW is NOT managed by the lock released *)
  with l_x_not_both
  have l_not_NoW_x: "ProgVar x \<in> mds AsmNoReadOrWrite \<Longrightarrow> x \<notin>  fst (lock_interp l)"
    by clarsimp

  (* x has to have been stable, because Rel only releases assumptions, and
     x was stable after the Rel *)
  from x_stable' stable_upd upd_def
  have x_stable: "stable \<S> x"
    unfolding stable_def add_anno_stable_def stable_NoW_def stable_NoRW_def
    by (clarsimp split:prod.splits)
  (* That means it must have had an assumption on it *)
  with stable_def_mds
  have x_asms: "ProgVar x \<in> mds AsmNoWrite \<or> ProgVar x \<in> mds AsmNoReadOrWrite"
    unfolding stable_def
    by (auto split: prod.splits)

  (* The same reasoning for \<S>'' *)
  from x_stable' upd_def stable_def_mds x_asms stable_upd
  have "ProgVar x \<in> lock_rel_mds_upd l mds AsmNoWrite \<or> ProgVar x \<in> lock_rel_mds_upd l mds AsmNoReadOrWrite"
    unfolding stable_def lock_rel_mds_upd_def add_anno_stable_def
    apply(clarsimp split: prod.splits LockUpd.splits simp: image_def)
    apply(drule_tac t="mds AsmNoWrite" in sym)
    apply(drule_tac t="mds AsmNoReadOrWrite" in sym)
    apply(clarsimp simp: stable_NoW_def stable_NoRW_def)
    using lone_lock_per_var by force
  (* That means it continues to have a NoWrite assumption on it *)
  with x_NoRW_dropped
  have x_NoW_kept: "ProgVar x \<in> lock_rel_mds_upd l mds AsmNoWrite"
    by simp
  hence x_had_NoW: "ProgVar x \<in> mds AsmNoWrite"
    unfolding upd_def lock_rel_mds_upd_def
    by (clarsimp split:prod.splits)
  hence x_in_fst_\<S>: "x \<in> stable_NoW \<S>"
    using stable_def_mds
    apply -
    apply(drule sym)
    by(clarsimp simp: image_def)
  with lock_interp_no_overlap 
  have "x \<notin> stable_NoRW \<S>"
    using lone_lock_per_var unfolding stable_NoW_def stable_NoRW_def by blast
  hence x_RW: "ProgVar x \<notin> mds AsmNoReadOrWrite"
    using stable_def_mds
    apply -
    apply(drule sym)
    by clarsimp

  have doms: "dom \<Gamma>'' \<subseteq> dom \<Gamma>"
    by(clarsimp simp: tyenv_upd add_anno_def add_anno_dom_def `upd = Rel l` stable_NoW_def stable_NoRW_def)

    
  (* x's old type was also t *)
  from new_entails_old x_keeps_type P''_sat tyenv_upd upd_def
  have x_had_type: "\<Gamma> x = Some t"
    apply -
    apply(erule_tac x=x in ballE)
     unfolding to_total_def
     apply(clarsimp split:if_splits)
      unfolding subtype_def
      unfolding pred_entailment_def
      apply clarsimp
      apply(erule_tac x="to_prog_mem mem" in allE)
      unfolding add_anno_def restrict_map_def
      apply(clarsimp split:if_splits)
      unfolding to_total_def
      apply clarsimp
     apply(clarsimp split:if_splits)
     unfolding add_anno_dom_def
     apply clarsimp
    using doms by blast

  have x_in_upd_dom: "x \<in> add_anno_dom \<Gamma> \<S> (Rel l)"
    using x_had_type tyenv_upd x_keeps_type upd_def
    apply -
    unfolding add_anno_def restrict_map_def
    by (clarsimp split:if_splits)

  from tyenv_sec x_had_type mem'_l mem'_x
  have thesis_RW: "ProgVar x \<notin> mds AsmNoReadOrWrite \<Longrightarrow> type_max t (to_prog_mem mem') \<le> dma mem' (ProgVar x)"
    apply -
    apply(erule_tac x=x in ballE)
     apply simp
     apply(erule_tac x="ProgVar x" in allE)
     apply clarsimp
     apply(clarsimp simp:type_max_def to_prog_mem_def split:if_splits)
      apply(rule conjI)
       apply clarsimp
       unfolding dma_correct
       apply clarsimp
       using less_eq_Sec_def apply blast
      apply clarsimp
      unfolding less_eq_Sec_def apply clarsimp
      unfolding to_prog_mem_def
      apply (simp add: mem'_x)
     apply clarsimp
     apply (simp add: mem'_x)
    apply clarsimp
    done
  with x_RW
  show "type_max t (to_prog_mem mem') \<le> dma mem' (ProgVar x)"
    by clarsimp
qed

lemma tyenv_sec_eq:
  "\<forall>x\<in>\<C>. mem x = mem' x \<Longrightarrow> types_wellformed \<Gamma> \<Longrightarrow> tyenv_sec mds \<Gamma> mem = tyenv_sec mds \<Gamma> mem'"
  apply(rule ball_cong[OF HOL.refl])
  apply(rename_tac x)
  apply(rule imp_cong[OF HOL.refl])
  apply(subgoal_tac "type_max (the (\<Gamma> x)) (to_prog_mem mem) = type_max (the (\<Gamma> x)) (to_prog_mem mem')")
   using dma_\<C> apply fastforce
  by (simp add: \<C>\<^sub>V_def \<C>_eq_type_max_eq to_prog_mem_def types_wellformed_def)

lemma context_equiv_tyenv_sec:
  "context_equiv \<Gamma>\<^sub>2 P\<^sub>2 \<Gamma>\<^sub>1 \<Longrightarrow>
    pred P\<^sub>2 (to_prog_mem mem) \<Longrightarrow> tyenv_sec mds \<Gamma>\<^sub>2 mem \<Longrightarrow> tyenv_sec mds \<Gamma>\<^sub>1 mem"
   apply(clarsimp simp: context_equiv_def type_equiv_def)
   apply(rename_tac x y)
   apply(rule_tac y="type_max (the (\<Gamma>\<^sub>2 x)) (to_prog_mem mem)" in order_trans)
    apply(rule subtype_sound[rule_format])
     apply force
    apply assumption
   apply force
   done

(* This might give a hint at what the commented-out lemmas above are meant to have instead
   of subset: entailment *)
lemma add_pred_entailment:
  "P +\<^sub>\<S> p \<turnstile> P"
  apply(simp add: add_pred_def pred_entailment_def)
  done

lemma unprotected_modifiable:
  assumes nin_dom: "x \<notin> dom \<Gamma>"
  assumes nin_\<C>: "x \<notin> \<C>\<^sub>V"
  assumes wf: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P"
  assumes unprotected: "unprotected x"
  shows "var_modifiable \<S> x"
proof -
  from nin_\<C> nin_dom wf have "\<not> stable \<S> x"
    unfolding tyenv_wellformed_def env_consistent_def by (auto simp: \<C>_def2)
  thus ?thesis
    unfolding var_modifiable_def stable_def stable_NoW_def stable_NoRW_def 
    apply clarsimp
    using unprotected unfolding unprotected_def by auto
qed

lemma stable_consistent_trivial:
  "\<forall>l. mem (Lock l) = mem' (Lock l) \<Longrightarrow> stable_consistent \<S> mem \<Longrightarrow> stable_consistent \<S> mem'"
  apply(clarsimp simp: stable_consistent_def)
  using eval_det\<^sub>L apply blast
  done


lemma lock_acq_type_stable:
  "type_stable \<S> P \<Longrightarrow> type_stable (\<S> \<union> {l}) (PConj P (lock_inv l))"
  using lock_invs apply(clarsimp simp: stable_def stable_NoW_def stable_NoRW_def)
  by (metis Un_iff contra_subsetD fst_conv prod.exhaust snd_conv)

lemma add_anno_stable_Acq [simp]:
  "add_anno_stable \<S> (Acq l) = \<S> \<union> {l}"
  by(auto simp: add_anno_stable_def)

lemma add_anno_stable_Rel [simp]:
  "add_anno_stable \<S> (Rel l) = \<S> - {l}"
  by(auto simp: add_anno_stable_def)

abbreviation
  preservation_props
where
  "preservation_props env \<Gamma> \<S> P mem \<equiv> tyenv_wellformed env \<Gamma> \<S> P \<and> pred P (to_prog_mem mem) \<and>
           stable_consistent \<S> mem \<and> global_invariant mem"

lemma restrict_preds_to_vars_weakens:
  "P \<turnstile> restrict_preds_to_vars P V"
  apply(simp add: pred_entailment_def)
  using restrict_preds_to_vars_weakens' by blast

(* This lemma is phrased this way to be easy to make use of in proofs of locally/globally
   sound env use. The last conjunct in particular is needed in the globally sound env 
   use proofs. *)
lemma preservation: 
  "\<lbrakk>\<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P';
    \<langle>c, (mds, agrels), mem\<rangle> \<leadsto>  \<langle>c', (mds',agrels'), mem'\<rangle>\<rbrakk> \<Longrightarrow> 
  \<exists> \<Gamma>'' \<S>'' P''. (\<turnstile> \<Gamma>'',\<S>'',P'' { c' } \<Gamma>',\<S>',P') \<and> 
         (preservation_props (mds,agrels) \<Gamma> \<S> P mem \<longrightarrow> preservation_props (mds',agrels') \<Gamma>'' \<S>'' P'' mem') \<and> 
         (preservation_props (mds,agrels) \<Gamma> \<S> P mem \<and> tyenv_sec mds \<Gamma> mem \<longrightarrow>
           tyenv_sec mds' \<Gamma>'' mem') \<and>
         (preservation_props (mds,agrels) \<Gamma> \<S> P mem \<longrightarrow> (\<forall>l. l \<in> \<S>'' \<and> l \<notin> \<S> \<longrightarrow> \<not> ev\<^sub>L mem l))"
proof (induct arbitrary: c' mds agrels rule: has_type.induct)
  case stop_type
  with stop_no_eval show ?case by blast
next
  case skip_type
  hence "c' = Stop \<and> mds' = mds \<and> mem' = mem \<and> agrels' = agrels"
    using prod.inject skipD by force
  thus ?case
    by (metis stop_type)
next
  case (assign\<^sub>1 x \<Gamma> e t P P' \<S> \<Gamma>' c' mds agrels)
  hence upd: "c' = Stop \<and> mds' = mds \<and> agrels' = agrels \<and> mem' = mem (ProgVar x := ev\<^sub>A (to_prog_mem mem) e)"
    by (metis prod.inject assignD)
  from assign\<^sub>1 upd have \<C>_eq: "\<forall>x\<in>\<C>. mem x = mem' x"
    by (auto simp: \<C>_def2)
  from upd have " \<turnstile> \<Gamma>,\<S>,P' {c'} \<Gamma>,\<S>,P'"
    by (metis stop_type)
  moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (mds',agrels') \<Gamma> \<S> P'"
    using upd tyenv_wellformed_preds_update assign\<^sub>1 restrict_preds_to_vars_restricts by metis
  moreover have "pred P (to_prog_mem mem) \<longrightarrow> pred P' (to_prog_mem mem')"
    using assign\<^sub>1 upd restrict_preds_to_vars_weakens assign_post_sound unfolding pred_entailment_def
    using prog_mem_assign_helper by blast
    
  moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P  \<and> tyenv_sec mds \<Gamma> mem \<longrightarrow> tyenv_sec mds \<Gamma> mem'"
    using tyenv_sec_eq[OF \<C>_eq, where \<Gamma>=\<Gamma>]
    unfolding tyenv_wellformed_def by blast
  moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<and> stable_consistent \<S> mem \<and> global_invariant mem \<longrightarrow> global_invariant mem'"
    using var_modifiable_global_invariant_preserved upd unprotected_modifiable
    using assign\<^sub>1.hyps(1) assign\<^sub>1.hyps(2) assign\<^sub>1.hyps(3) by blast
  moreover have "stable_consistent \<S> mem \<longrightarrow> stable_consistent \<S> mem'"
     by(fastforce simp: upd intro: stable_consistent_trivial)
  ultimately show ?case
    using assign\<^sub>1.hyps
    by (metis upd)
next
  case (assign\<^sub>2 x \<Gamma> \<S> e t P' P \<Gamma>' c' mds agrels)
  hence upd: "c' = Stop \<and> mds' = mds \<and> mem' = mem (ProgVar x := ev\<^sub>A (to_prog_mem mem) e) \<and> agrels' = agrels"
    by (metis assignD prod.inject)
  let ?\<Gamma>' = "\<Gamma> (x \<mapsto> t)"
  from upd have ty: " \<turnstile> ?\<Gamma>',\<S>,P' {c'} ?\<Gamma>',\<S>,P'"
    by (metis stop_type)
  have wf: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (mds',agrels') ?\<Gamma>' \<S> P'"
  proof
    assume tyenv_wf: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P"
    hence wf: "types_wellformed \<Gamma>"
      unfolding tyenv_wellformed_def by blast
    hence  "type_wellformed t"
      using assign\<^sub>2 type_aexpr_type_wellformed
      by blast
    with wf have wf': "types_wellformed ?\<Gamma>'"
      using types_wellformed_update by metis
    from tyenv_wf have stable': "types_stable ?\<Gamma>' \<S>"
      using types_stable_update
            assign\<^sub>2.hyps(4)
      unfolding tyenv_wellformed_def by blast
    have m: "env_consistent (mds,agrels)\<Gamma> \<S> P"
      using tyenv_wf unfolding tyenv_wellformed_def by blast
    have "env_consistent (mds',agrels') (\<Gamma>(x \<mapsto> t)) \<S> P'"
      apply(rule env_consistent_preds_tyenv_update)
      using upd m assign\<^sub>2.hyps restrict_preds_to_vars_restricts by simp+
    from wf' stable' this show "tyenv_wellformed (mds',agrels') ?\<Gamma>' \<S> P'"
      unfolding tyenv_wellformed_def by blast
  qed
  have p: "pred P (to_prog_mem mem) \<longrightarrow> pred P' (to_prog_mem mem')"
    using assign\<^sub>2 upd prog_mem_assign_helper restrict_preds_to_vars_weakens assign_post_sound unfolding pred_entailment_def
    using prog_mem_assign_helper by blast
  have sec: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<Longrightarrow> pred P (to_prog_mem mem) \<Longrightarrow> tyenv_sec mds \<Gamma> mem \<Longrightarrow> tyenv_sec mds' ?\<Gamma>' mem'"
  proof(clarify)
    assume wf: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P"
    assume pred: "pred P (to_prog_mem mem)"
    assume sec: "tyenv_sec mds \<Gamma> mem"
    from pred p have pred': "pred P' (to_prog_mem mem')" by blast
    fix v t'
    assume \<Gamma>v: "(\<Gamma>(x \<mapsto> t)) v = Some t'"
    assume "ProgVar v \<notin> mds' AsmNoReadOrWrite"
    show "type_max (the ((\<Gamma>(x \<mapsto> t)) v)) (to_prog_mem mem') \<le> dma mem' (ProgVar v)"
    proof(cases "v = x")
      assume [simp]: "v = x"
      hence [simp]: "(the ((\<Gamma>(x \<mapsto> t)) v)) = t" and t_def: "t = t'"
        using \<Gamma>v by auto
      from `ProgVar v \<notin> mds' AsmNoReadOrWrite` upd wf have readable: "x \<notin> stable_NoRW \<S>"
        by(auto simp: tyenv_wellformed_def env_consistent_def stable_NoRW_def)
      with assign\<^sub>2.hyps(6) have "t \<le>:\<^sub>P' bexp_to_lpred (dma_type x)" by fastforce
      with pred' show ?thesis
        using type_max_dma_type subtype_correct
        by fastforce
    next
      assume neq: "v \<noteq> x"
      hence [simp]: "((\<Gamma>(x \<mapsto> t)) v) = \<Gamma> v"
        by simp
      with \<Gamma>v have \<Gamma>v: "\<Gamma> v = Some t'" by simp
      with sec upd `ProgVar v \<notin> mds' AsmNoReadOrWrite` have f_leq: "type_max t' (to_prog_mem mem) \<le> dma mem (ProgVar v)"
        by auto
      have \<C>_eq: "\<forall>x\<in>\<C>. mem x = mem' x"
        using wf assign\<^sub>2 upd by(auto simp: tyenv_wellformed_def env_consistent_def)
      hence dma_eq: "dma mem = dma mem'"
        by(rule dma_\<C>)
      have f_eq: "type_max t' (to_prog_mem mem) = type_max t' (to_prog_mem mem')"
        apply(rule \<C>_eq_type_max_eq)
         using \<Gamma>v wf apply(force simp: tyenv_wellformed_def types_wellformed_def)
        by(auto simp: \<C>_eq \<C>\<^sub>V_def to_prog_mem_def)     
      from neq \<Gamma>v f_leq dma_eq f_eq show ?thesis
        by simp
    qed
  qed

  have ginv_pres: "global_invariant mem \<and> stable_consistent \<S> mem \<longrightarrow> global_invariant mem'"
    using var_modifiable_global_invariant_preserved
    using assign\<^sub>2.hyps(2) upd by blast

  have sc: "stable_consistent \<S> mem \<longrightarrow> stable_consistent \<S> mem'"
     by(fastforce simp: upd intro: stable_consistent_trivial)

  from ty wf p sec assign\<^sub>2.hyps ginv_pres sc show ?case
    by blast
next
  case (assign\<^sub>\<C> x \<S> \<Gamma> e t P P' \<Gamma>' c' mds agrels)
  (* this case follows from the same argument as assign\<^sub>1 *)
  hence upd: "c' = Stop \<and> mds' = mds \<and> mem' = mem (ProgVar x := ev\<^sub>A (to_prog_mem mem) e) \<and> agrels' = agrels"
    by (metis prod.inject assignD)
  hence " \<turnstile> \<Gamma>,\<S>,P' {c'} \<Gamma>,\<S>,P'"
    by (metis stop_type)
  moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (mds',agrels') \<Gamma> \<S> P'"
    using upd tyenv_wellformed_preds_update assign\<^sub>\<C> restrict_preds_to_vars_restricts by metis
  moreover have "pred P (to_prog_mem mem) \<longrightarrow> pred P' (to_prog_mem mem')"
    using assign\<^sub>\<C> upd prog_mem_assign_helper restrict_preds_to_vars_weakens assign_post_sound unfolding pred_entailment_def
    by blast
  moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<and> pred P (to_prog_mem mem) \<and> tyenv_sec mds \<Gamma> mem \<Longrightarrow> tyenv_sec mds' \<Gamma> mem'"
  proof(clarify)
    fix v t'
    assume wf: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P"
    assume pred: "pred P (to_prog_mem mem)"
    hence pred': "pred P' (to_prog_mem mem')" using `pred P (to_prog_mem mem) \<longrightarrow> pred P' (to_prog_mem mem')` by blast
    assume sec: "tyenv_sec mds \<Gamma> mem"
    assume \<Gamma>v: "\<Gamma> v = Some t'"
    assume readable': "ProgVar v \<notin> mds' AsmNoReadOrWrite"
    with upd have readable: "ProgVar v \<notin> mds AsmNoReadOrWrite" by simp
    with wf have "v \<notin> stable_NoRW \<S>" by(auto simp: tyenv_wellformed_def env_consistent_def)
    show "type_max (the (\<Gamma> v)) (to_prog_mem mem') \<le> dma mem' (ProgVar v)"
    proof(cases "x \<in> \<C>_vars\<^sub>V v")
      assume "x \<in> \<C>_vars\<^sub>V v"
      with assign\<^sub>\<C>.hyps `v \<notin> stable_NoRW \<S>` have "(to_total \<Gamma> v) \<le>:\<^sub>P' (bexp_to_lpred (dma_type v))"
      by auto
      from pred' \<Gamma>v subtype_correct this show ?thesis
        using type_max_dma_type by(auto simp: to_total_def split: if_splits)
    next
      assume "x \<notin> \<C>_vars\<^sub>V v"
      hence  "\<forall>v'\<in>\<C>_vars\<^sub>V v. mem (ProgVar v') = mem' (ProgVar v')"
        using upd by auto
      hence  "\<forall>v'\<in>\<C>_vars (ProgVar v). mem v' = mem' v'"
        using \<C>_vars\<^sub>V_def
        using fun_upd_apply mem_Collect_eq upd by auto
      hence dma_eq: "dma mem (ProgVar v) = dma mem' (ProgVar v)"
        by(rule dma_\<C>_vars)
      from \<Gamma>v assign\<^sub>\<C> have "x \<notin> lpred_vars t'"
        by (metis domIff option.sel option.simps(3))
      have "type_wellformed t'"
        using wf \<Gamma>v by(force simp: tyenv_wellformed_def types_wellformed_def)
      with `x \<notin> lpred_vars t'` upd have f_eq: "type_max t' (to_prog_mem mem) = type_max t' (to_prog_mem mem')"
        using vars_of_type_eq_type_max_eq to_prog_mem_def
        by (simp add: fun_upd_apply prog_mem_assign_helper)
      from sec \<Gamma>v readable have "type_max t' (to_prog_mem mem) \<le> dma mem (ProgVar v)"
        by auto
      with f_eq dma_eq \<Gamma>v show ?thesis
        by simp
    qed
  qed

  moreover have ginv_pres: "global_invariant mem \<and> stable_consistent \<S> mem \<longrightarrow> global_invariant mem'"
    using var_modifiable_global_invariant_preserved
    using assign\<^sub>\<C>.hyps(2) upd by blast

  moreover have sc: "stable_consistent \<S> mem \<longrightarrow> stable_consistent \<S> mem'"
     by(fastforce simp: upd intro: stable_consistent_trivial)

  ultimately show ?case
    using assign\<^sub>\<C>.hyps
    by (metis)
next
  case (if_type \<Gamma> e t P p \<S> th \<Gamma>' \<S>' P' el \<Gamma>'' P'' \<Gamma>''' P''' c' mds agrels)
  from if_type(15)
  show ?case
  proof(rule ifD)
    assume [simp]: "ev\<^sub>B (to_prog_mem mem) e" and [simp]: "c' = th" and [simp]: "mem' = mem" and [simp]: "(mds',agrels') = (mds,agrels)"
    hence [simp]: "mds' = mds" and [simp]: "agrels' = agrels" by(metis prod.inject)+
    from if_type(3) if_type(4) have " \<turnstile> \<Gamma>,\<S>,P +\<^sub>\<S> p {c'} \<Gamma>',\<S>',P'" by simp
    hence "\<turnstile> \<Gamma>,\<S>,P +\<^sub>\<S> p {c'} \<Gamma>''',\<S>',P'''"
      apply(rule sub)
           apply simp+
         using if_type apply fastforce
        using if_type apply blast
       apply simp
      using if_type apply(blast intro: pred_entailment_trans)
      done
    moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (mds',agrels') \<Gamma> \<S> (P +\<^sub>\<S> p)"
      by(auto simp: tyenv_wellformed_def env_consistent_def add_pred_def)
    moreover have "pred P (to_prog_mem mem) \<longrightarrow> pred (P +\<^sub>\<S> p) (to_prog_mem mem')"
      by(simp add: add_pred_def if_type(3) bexp_to_lpred_correct)
    moreover have "tyenv_sec mds \<Gamma> mem \<longrightarrow> tyenv_sec mds' \<Gamma> mem'"
      by(simp)
    moreover have ginv_pres: "global_invariant mem \<longrightarrow> global_invariant mem'"
      by simp
    moreover have sc: "stable_consistent \<S> mem \<longrightarrow> stable_consistent \<S> mem'"
      by(fastforce)

    ultimately show ?case by blast
  next
    assume [simp]: "\<not> ev\<^sub>B (to_prog_mem mem) e" and [simp]: "c' = el" and [simp]: "mem' = mem" and [simp]: "(mds',agrels') = (mds,agrels)"
    hence [simp]: "mds' = mds" and [simp]: "agrels' = agrels" by(metis prod.inject)+
    from if_type(6) have " \<turnstile> \<Gamma>,\<S>,P +\<^sub>\<S> (PNeg p) {c'} \<Gamma>'',\<S>',P''" by simp
    hence "\<turnstile> \<Gamma>,\<S>,P +\<^sub>\<S> (PNeg p) {c'} \<Gamma>''',\<S>',P'''"
      apply(rule sub)
           apply simp+
         using if_type apply fastforce
        using if_type apply blast
       apply simp
      using if_type apply(blast intro: pred_entailment_trans)
      done
    moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (mds',agrels') \<Gamma> \<S> (P +\<^sub>\<S> (PNeg p))"
      by(auto simp: tyenv_wellformed_def env_consistent_def add_pred_def)
    moreover have "pred P (to_prog_mem mem) \<longrightarrow> pred (P +\<^sub>\<S> (PNeg p)) (to_prog_mem mem')"
      by(simp add: add_pred_def if_type(3) bexp_to_lpred_correct)
    moreover have "tyenv_sec mds \<Gamma> mem \<longrightarrow> tyenv_sec mds' \<Gamma> mem'"
      by(simp)
    moreover have ginv_pres: "global_invariant mem \<longrightarrow> global_invariant mem'"
      by simp
    moreover have sc: "stable_consistent \<S> mem \<longrightarrow> stable_consistent \<S> mem'"
      by(fastforce)
    ultimately show ?case  by blast
  qed
next
  case (while_type \<Gamma> e t P \<S> c c' mds)
  hence [simp]: "mds' = mds \<and> c' = If e (c ;; While e c) Stop \<and> mem' = mem \<and> agrels' = agrels"
    by (metis prod.inject whileD)
  have " \<turnstile> \<Gamma>,\<S>,P {c'} \<Gamma>,\<S>,P"
    apply simp
    apply(rule if_type)
               apply(rule while_type(1))
              apply(rule while_type(2))
             apply(rule HOL.refl)
            apply(rule seq_type)
             apply(rule while_type(3))
            apply(rule has_type.while_type)
              apply(rule while_type(1))
             apply(rule while_type(2))
            apply(rule while_type(3))
           apply(rule `bexp_readable \<S> e`)
          apply(rule stop_type)
         apply simp
        apply simp
       apply simp
      apply(rule add_pred_entailment)
      apply simp+
    by(blast intro!: tyenv_wellformed_add_pred `bexp_readable \<S> e`)+
  thus ?case
    by fastforce
next
  case (seq_type \<Gamma> \<S> P c\<^sub>1 \<Gamma>\<^sub>1 \<S>\<^sub>1 P\<^sub>1 c\<^sub>2 \<Gamma>\<^sub>2 \<S>\<^sub>2 P\<^sub>2 c' mds)
  thus ?case
  proof (cases "c\<^sub>1 = Stop")
    assume [simp]: "c\<^sub>1 = Stop"
    with seq_type have [simp]: "mds' = mds" and [simp]: "c' = c\<^sub>2" and [simp]: "mem' = mem"
                   and [simp]: "agrels' = agrels"
      by (metis prod.inject seq_stopD)+
    have context_eq: "context_equiv \<Gamma> P \<Gamma>\<^sub>1" and [simp]: "\<S>\<^sub>1 = \<S>" and entail: "P \<turnstile> P\<^sub>1" and
             wf_imp: "\<forall>env. tyenv_wellformed env \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed env \<Gamma>\<^sub>1 \<S> P\<^sub>1"
      using stop_cxt seq_type(1) by simp+
    have "\<turnstile> \<Gamma>,\<S>,P {c\<^sub>2} \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2"
      apply(rule sub)
            using seq_type(3) apply simp
           apply(rule context_eq)
          using wf_imp apply(fastforce)
         apply simp+
       apply(rule entail)
      apply(rule pred_entailment_refl)
      done
    thus ?case
      by fastforce
  next
    assume "c\<^sub>1 \<noteq> Stop"
    then obtain c\<^sub>1' where step: "\<langle>c\<^sub>1, (mds,agrels), mem\<rangle> \<leadsto> \<langle>c\<^sub>1', (mds',agrels'), mem'\<rangle> \<and> c' = (c\<^sub>1' ;; c\<^sub>2)"
      by (metis seqD seq_type.prems)
    then obtain \<Gamma>''' \<S>''' P''' where "\<turnstile> \<Gamma>''',\<S>''',P''' {c\<^sub>1'} \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1 \<and>
      (preservation_props (mds,agrels) \<Gamma> \<S> P mem \<longrightarrow> preservation_props (mds',agrels') \<Gamma>''' \<S>''' P''' mem') \<and>
      (preservation_props (mds,agrels) \<Gamma> \<S> P mem \<and> tyenv_sec mds \<Gamma> mem \<longrightarrow> tyenv_sec mds' \<Gamma>''' mem') \<and>
      (preservation_props (mds,agrels) \<Gamma> \<S> P mem \<longrightarrow> (\<forall>l. l \<in> \<S>''' \<and> l \<notin> \<S> \<longrightarrow> \<not> ev\<^sub>L mem l))"
      using step seq_type(2) 
      by blast
    moreover
    from seq_type have "\<turnstile> \<Gamma>\<^sub>1,\<S>\<^sub>1,P\<^sub>1 {c\<^sub>2} \<Gamma>\<^sub>2,\<S>\<^sub>2,P\<^sub>2" by auto
    moreover
    ultimately show ?case
      apply (rule_tac x = \<Gamma>''' in exI)
      using `\<langle>c\<^sub>1, (mds,agrels), mem\<rangle> \<leadsto> \<langle>c\<^sub>1', (mds',agrels'), mem'\<rangle> \<and> c' = c\<^sub>1' ;; c\<^sub>2` by blast
  qed
next
  case (sub \<Gamma>\<^sub>1 \<S> P\<^sub>1 c \<Gamma>\<^sub>1' \<S>' P\<^sub>1' \<Gamma>\<^sub>2 P\<^sub>2 \<Gamma>\<^sub>2' P\<^sub>2' c' mds)
  then obtain \<Gamma>'' \<S>'' P'' where stuff: "\<turnstile> \<Gamma>'',\<S>'',P'' { c' } \<Gamma>\<^sub>1',\<S>',P\<^sub>1' \<and>
    (preservation_props (mds,agrels) \<Gamma>\<^sub>1 \<S> P\<^sub>1 mem \<longrightarrow> preservation_props (mds',agrels') \<Gamma>'' \<S>'' P'' mem') \<and>
    (preservation_props (mds,agrels) \<Gamma>\<^sub>1 \<S> P\<^sub>1 mem \<and> tyenv_sec mds \<Gamma>\<^sub>1 mem \<longrightarrow> tyenv_sec mds' \<Gamma>'' mem') \<and>
    (preservation_props (mds,agrels) \<Gamma>\<^sub>1 \<S> P\<^sub>1 mem \<longrightarrow> (\<forall>l. l \<in> \<S>'' \<and> l \<notin> \<S> \<longrightarrow> \<not> ev\<^sub>L mem l))"
    by metis

  have imp: "tyenv_wellformed (mds,agrels) \<Gamma>\<^sub>2 \<S> P\<^sub>2 \<and> pred P\<^sub>2 (to_prog_mem mem) \<Longrightarrow> 
             tyenv_wellformed (mds,agrels) \<Gamma>\<^sub>1 \<S> P\<^sub>1 \<and> pred P\<^sub>1 (to_prog_mem mem)"
    apply(rule conjI)
     using sub(5) sub(4)  unfolding pred_def
     apply blast
    by (meson  sifum_types.pred_entailment_def sifum_types_axioms sub.hyps(7))

  have imp': "tyenv_wellformed (mds,agrels) \<Gamma>\<^sub>2 \<S> P\<^sub>2 \<and> pred P\<^sub>2 (to_prog_mem mem) \<and> tyenv_sec mds \<Gamma>\<^sub>2 mem \<Longrightarrow> 
              tyenv_sec mds \<Gamma>\<^sub>1 mem"
    using sub(3) context_equiv_tyenv_sec unfolding pred_def by blast
  show ?case
    apply (rule_tac x = \<Gamma>'' in exI, rule_tac x = "\<S>''" in exI, rule_tac x="P''" in exI)  
    apply (rule conjI)
     apply(rule has_type.sub)
           apply(rule stuff[THEN conjunct1])
          apply simp+
        using sub(5) apply fastforce
       using sub(6) apply fastforce
      apply simp
     using sub apply blast
    apply(rule conjI)
     using imp stuff apply blast
    using imp imp' stuff apply blast
    done
next
  case (lock_acq_type upd l \<Gamma>' \<Gamma> \<S> \<S>' P' P c')
  have "(mds', agrels') = (mds, agrels) \<and> mem' = mem \<and> c' = (LockAcq l) \<or>
  c' = Stop \<and>
  \<not> ev\<^sub>L mem l \<and>
  ev\<^sub>L mem' l \<and>
  (mem' = mem(Lock l := lock_acq_sem l (mem (Lock l)))) \<and>
  (mds', agrels') = (lock_acq_mds_upd l (fst (mds, agrels)), lock_acq_agrels_upd l (snd (mds, agrels)))"
    using lock_acq_type lock_acqD by metis (* a little slow *)
  thus ?case
  proof(rule disjE)
    assume upd: "(mds', agrels') = (mds, agrels) \<and> mem' = mem \<and> c' = LockAcq l"
    have "\<turnstile> \<Gamma>,\<S>,P {LockAcq l} \<Gamma>',\<S>',P'"
      apply(rule has_type.lock_acq_type)
      using lock_acq_type upd by auto
    with upd show ?thesis
      using lock_acq_type by fastforce
  next 
    assume upd': " c' = Stop \<and>
      \<not> ev\<^sub>L mem l \<and>
      ev\<^sub>L mem' l \<and>
      mem' = mem(Lock l := lock_acq_sem l (mem (Lock l))) \<and>
      (mds', agrels') = (lock_acq_mds_upd l (fst (mds, agrels)), lock_acq_agrels_upd l (snd (mds, agrels)))"
    with lock_acq_type have upd: "c' = Stop" 
      "\<not> ev\<^sub>L mem l" "ev\<^sub>L mem' l"  "\<forall>x. x \<noteq> Lock l \<longrightarrow> mem' x = mem x" 
      "(mds',agrels') = update_env upd (mds,agrels)"
      using lock_acqD update_env_def 
      by (blast | auto simp: update_env_def)+
    moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (update_env upd (mds,agrels)) \<Gamma>' \<S>' P'"
    proof
      assume a: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P"
      have "P = restrict_preds_to_vars P {v. stable \<S> v}"
        apply(subst restrict_preds_to_vars_noop)
         using `tyenv_wellformed (mds,agrels) \<Gamma> \<S> P`
         by(auto simp: tyenv_wellformed_def  env_consistent_def)
      hence "P' = restrict_preds_to_vars (PConj P (lock_inv l)) {v. stable \<S>' v}"
        apply -
        apply(simp add: `P' = PConj P (lock_inv l)`)
        apply(subst restrict_preds_to_vars_noop)
         apply(subst lock_acq_type(3))
         apply(subst lock_acq_type(1))
         apply(subst add_anno_stable_Acq)
         apply(drule sym,drule restrict_preds_to_vars_noopD)
         apply(fastforce simp: stable_def stable_NoW_def stable_NoRW_def) 
        apply simp
        apply(subst restrict_preds_to_vars_noop)
         apply(subst lock_acq_type(3))
         apply(subst lock_acq_type(1))
         apply(subst add_anno_stable_Acq)
         apply(clarsimp simp: stable_def stable_NoW_def stable_NoRW_def)
         using lock_invs[OF surjective_pairing, where l=l] apply blast
        by simp 
      with a show "tyenv_wellformed (update_env upd (mds,agrels)) \<Gamma>' \<S>' P'"
      using lock_acq_type
            tyenv_wellformed_env_update
      using tyenv_wellformed_preds_update
      using anno_type_stable_trivial
      by (metis restrict_preds_to_vars_noopD upd(5))
    qed
    moreover
    have pred: "global_invariant mem \<and> pred P (to_prog_mem mem) \<longrightarrow> pred P' (to_prog_mem mem)"
      apply(clarsimp simp: pred_def lock_acq_type(4))
      using upd global_invariant_def by blast
    moreover
    have "global_invariant mem \<and> tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<and> pred P (to_prog_mem mem) \<and> tyenv_sec mds \<Gamma> mem \<longrightarrow> tyenv_sec (fst (update_env upd (mds,agrels))) \<Gamma>' mem"
      apply(rule impI)
      apply(simp add: update_env_def `upd = Acq l`)
      apply(rule tyenv_sec_lock_acq)
           unfolding tyenv_wellformed_def env_consistent_def apply force
          using lock_acq_type apply fastforce
         using lock_acq_type(3) apply blast
        using lock_acq_type apply(fastforce simp: tyenv_wellformed_def env_consistent_def)
       apply simp
      using lock_acq_type by simp
    moreover have ginv_pres: "global_invariant mem \<longrightarrow> global_invariant mem'"
      using upd
      apply(clarsimp simp: global_invariant_def)
      using lock_invs
      by (metis (mono_tags, lifting) Var.inject(1) Var.simps(4) eval_det\<^sub>L lpred_eval_vars_det to_prog_mem_def)
    moreover have sc: "stable_consistent \<S> mem \<longrightarrow> stable_consistent \<S>' mem'"
      using upd lock_acq_type(3)
      apply(clarsimp simp: stable_consistent_def)
      using add_anno_stable_Acq eval_det\<^sub>L
      by (metis Collect_mem_eq Collect_mono_iff Un_insert_right Var.inject(1) insert_iff lock_acq_type.hyps(1) sup_bot.right_neutral)
    ultimately show ?case
      apply(rule_tac x=\<Gamma>' in exI)
      apply(rule_tac x=\<S>' in exI)
      apply(rule_tac x=P' in exI)
      apply clarsimp
      apply(rule conjI)
       apply(rule stop_type', simp+)
      apply(rule conjI)
       apply(simp add: pred_def to_prog_mem_def)
      apply(rule conjI)
       apply clarsimp
       apply(rename_tac x t)
       apply (metis (mono_tags, lifting) Var.simps(4) dma\<^sub>V_dma domI fst_conv option.sel to_prog_mem_def type_max_dma_type vars_of_type_eq_type_max_eq)
      by (clarsimp simp: lock_acq_type(3) `upd = Acq l`)
  qed
next
  case (lock_rel_type upd l \<Gamma>' \<Gamma> \<S> \<S>' P P')
  hence upd: "c' = Stop"
   "\<not> ev\<^sub>L mem' l" "mem' = mem(Lock l := lock_rel_sem l (mem (Lock l)))"
   "mds' = lock_rel_mds_upd l mds" "agrels' = lock_rel_agrels_upd l agrels"
    using lock_relD lock_rel_type update_env_def apply (fastforce+)
    done

  moreover
  have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (update_env upd (mds,agrels)) \<Gamma>' \<S>' P'"
    using lock_rel_type
    using tyenv_wellformed_env_update[where upd=upd] add_anno_stable_def[where upd=upd]
    by (clarsimp split: LockUpd.splits)
  moreover
  have pred: "pred P (to_prog_mem mem) \<longrightarrow> pred P' (to_prog_mem mem')"
    using lock_rel_type
    apply clarsimp using upd
    unfolding to_prog_mem_def
    apply clarsimp
    using restrict_preds_to_vars_weakens by(auto simp: pred_entailment_def)
  moreover
  have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<and> pred P (to_prog_mem mem) \<and> tyenv_sec mds \<Gamma> mem \<longrightarrow> tyenv_sec (lock_rel_mds_upd l mds) \<Gamma>' mem'"
    apply(rule impI)
    apply(rule tyenv_sec_lock_rel[where P=P, OF `\<forall>x\<in>dom \<Gamma>. the (\<Gamma> x) \<le>:\<^sub>P' to_total \<Gamma>' x`])
             using lock_rel_type pred apply fastforce
            apply(clarsimp simp: tyenv_wellformed_def env_consistent_def)
            apply blast
           using lock_rel_type apply blast
          apply(fastforce simp: tyenv_wellformed_def env_consistent_def)
         using lock_rel_type apply blast
        using lock_rel_type apply fastforce
       using lock_rel_type apply fastforce
      using lock_rel_type apply blast
     using upd apply blast
    using upd apply fastforce
    done
  moreover have ginv_pres: "pred P (to_prog_mem mem) \<and> global_invariant mem \<longrightarrow> global_invariant mem'"
  proof(clarify)
    assume p: "pred P (to_prog_mem mem)"
    assume ginv: "global_invariant mem"
    from p have "pred (lock_inv l) (to_prog_mem mem)"
      using `P \<turnstile> (lock_inv l)`
      using pred_entailment_def by auto
    hence "lpred_eval (to_prog_mem mem')  (lock_inv l)"
      using upd pred_def
      by (simp add: Var.distinct(1) eval_vars_det\<^sub>B singletonI to_prog_mem_def)
    with ginv upd show "global_invariant mem'"
      unfolding global_invariant_def
      by (metis (mono_tags, lifting) Var.inject(1) Var.simps(4) eval_det\<^sub>L lpred_eval_vars_det fun_upd_other to_prog_mem_def)
  qed
  moreover have sc: "stable_consistent \<S> mem \<longrightarrow> stable_consistent \<S>' mem'"
      using upd lock_rel_type(3) lock_rel_type(1) 
      apply(clarsimp simp: stable_consistent_def)
      using add_anno_stable_Rel 
      apply clarsimp
      using eval_det\<^sub>L
      by (metis Var.inject(1) fun_upd_apply mem_Collect_eq subsetCE)
  ultimately
  show ?case
    apply(rule_tac x=\<Gamma>' in exI)
    apply(rule_tac x=\<S>' in exI)
    apply(rule_tac x=P' in exI)
    by(fastforce simp: lone_lock_per_var update_env_def `upd = Rel l` `\<S>' = add_anno_stable \<S> upd` add_anno_stable_def stop_type')
qed


fun bisim_helper :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow>
  (('Var, 'Lock, 'AExp, 'BExp) Stmt, ('Lock, 'Var) Var, 'Val) LocalConf \<Rightarrow> bool"
  where
  "bisim_helper \<langle>c\<^sub>1, env, mem\<^sub>1\<rangle> \<langle>c\<^sub>2, env\<^sub>2, mem\<^sub>2\<rangle> = mem\<^sub>1 =\<^bsub>(fst env)\<^esub>\<^sup>l mem\<^sub>2"

lemma \<R>\<^sub>3_mem_eq: "\<langle>c\<^sub>1, env, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, env, mem\<^sub>2\<rangle> \<Longrightarrow> mem\<^sub>1 =\<^bsub>(fst env)\<^esub>\<^sup>l mem\<^sub>2"
  apply (subgoal_tac "bisim_helper \<langle>c\<^sub>1, env, mem\<^sub>1\<rangle> \<langle>c\<^sub>2, env, mem\<^sub>2\<rangle>")
   apply simp
  apply (induct rule: \<R>\<^sub>3_aux.induct)
  by (auto simp: \<R>\<^sub>1_mem_eq)

lemma pred_fold_PConj:
  "pred (fold PConj Ps Q) mem \<Longrightarrow> (\<forall>P. P \<in> set Ps \<longrightarrow> pred P mem) \<and> pred Q mem"
  apply(induct Ps arbitrary: Q, clarsimp)
  by (metis fold_simps(2) lpred_eval.simps(4) set_ConsD) (* use sledgehammer here since simp hangs *)

lemma [simp, intro!]:
  "finite (V::'Var set)"
  apply(rule_tac f=ProgVar in finite_imageD)
   apply(rule finite_subset[OF _ Var_finite])
   apply simp
  apply(auto intro: inj_onI)
  done

lemma [simp, intro!]:
  "finite (V::'Lock set)"
  apply(rule_tac f=Lock in finite_imageD)
   apply(rule finite_subset[OF _ Var_finite])
   apply simp
  apply(auto intro: inj_onI)
  done

lemma ev\<^sub>A_eq:
  assumes tyenv_eq: "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assumes pred: "pred P (to_prog_mem mem\<^sub>1)" 
  assumes e_type: "\<Gamma> \<turnstile>\<^sub>a e \<in> t"
  assumes subtype: "t \<le>:\<^sub>P bexp_to_lpred (dma_type v)"
  assumes is_Low: "dma mem\<^sub>1 (ProgVar v) = Low"
  shows "ev\<^sub>A (to_prog_mem mem\<^sub>1) e = ev\<^sub>A (to_prog_mem mem\<^sub>2) e"
proof(rule eval_vars_det\<^sub>A, clarify)
  fix x
  assume in_vars: "x \<in> aexp_vars e"
  have "type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) = Low"
  proof -
    from subtype_sound[OF subtype] pred have "type_max t (to_prog_mem mem\<^sub>1) \<le> dma mem\<^sub>1 (ProgVar v)"
      by(auto)
    with is_Low have "type_max t (to_prog_mem mem\<^sub>1) = Low" by(auto simp: less_eq_Sec_def)
    with e_type in_vars show ?thesis
      apply -
      apply(erule type_aexpr.cases)
      apply(clarsimp simp: type_max_def split: if_splits)
      apply(rule pred_fold_PConj[THEN conjunct1, rule_format])
       apply assumption
      by simp
  qed
  thus "(to_prog_mem mem\<^sub>1) x = (to_prog_mem mem\<^sub>2) x"
    using tyenv_eq unfolding tyenv_eq_def to_prog_mem_def by simp
qed

lemma ev\<^sub>A_eq':
  assumes tyenv_eq: "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assumes pred: "pred P (to_prog_mem mem\<^sub>1)" 
  assumes e_type: "\<Gamma> \<turnstile>\<^sub>a e \<in> t"
  assumes subtype: "P \<turnstile> t"
  shows "ev\<^sub>A (to_prog_mem mem\<^sub>1) e = ev\<^sub>A (to_prog_mem mem\<^sub>2) e"
proof(rule eval_vars_det\<^sub>A, clarify)
  fix x
  assume in_vars: "x \<in> aexp_vars e"
  have "type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) = Low"
  proof -
    from subtype pred have "type_max t (to_prog_mem mem\<^sub>1) \<le> Low"
      by(auto simp: type_max_def pred_entailment_def pred_def)
    hence "type_max t (to_prog_mem mem\<^sub>1) = Low" by(auto simp: less_eq_Sec_def)
    with e_type in_vars show ?thesis
      apply -
      apply(erule type_aexpr.cases)
      apply(clarsimp simp: type_max_def split: if_splits)
      apply(rule pred_fold_PConj[THEN conjunct1, rule_format])
       apply assumption
      by simp
  qed
  thus "(to_prog_mem mem\<^sub>1) x = (to_prog_mem mem\<^sub>2) x"
    using tyenv_eq unfolding tyenv_eq_def to_prog_mem_def by simp
qed

lemma ev\<^sub>B_eq':
  assumes tyenv_eq: "mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2"
  assumes pred: "pred P (to_prog_mem mem\<^sub>1)" 
  assumes e_type: "\<Gamma> \<turnstile>\<^sub>b e \<in> t"
  assumes subtype: "P \<turnstile> t"
  shows "ev\<^sub>B (to_prog_mem mem\<^sub>1) e = ev\<^sub>B (to_prog_mem mem\<^sub>2) e"
proof(rule eval_vars_det\<^sub>B, clarify)
  fix x
  assume in_vars: "x \<in> bexp_vars e"
  have "type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) = Low"
  proof -
    from subtype pred have "type_max t (to_prog_mem mem\<^sub>1) \<le> Low"
      by(auto simp: type_max_def pred_entailment_def pred_def)
    hence "type_max t (to_prog_mem mem\<^sub>1) = Low" by(auto simp: less_eq_Sec_def)
    with e_type in_vars show ?thesis
      apply -
      apply(erule type_bexpr.cases)
      apply(clarsimp simp: type_max_def split: if_splits)
      apply(rule pred_fold_PConj[THEN conjunct1, rule_format])
       apply assumption
      by simp
  qed
  thus "(to_prog_mem mem\<^sub>1) x = (to_prog_mem mem\<^sub>2) x"
    using tyenv_eq unfolding tyenv_eq_def to_prog_mem_def by simp
qed

lemma R1_equiv_entailment:
  "\<langle>c, env, mem\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c', env', mem'\<rangle> \<Longrightarrow>  
  context_equiv \<Gamma>' P' \<Gamma>'' \<Longrightarrow> P' \<turnstile> P'' \<Longrightarrow>
  \<forall>env. tyenv_wellformed env \<Gamma>' \<S>' P' \<longrightarrow> tyenv_wellformed env \<Gamma>'' \<S>' P'' \<Longrightarrow>
   \<langle>c, env, mem\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>'',\<S>',P''\<^esub> \<langle>c', env', mem'\<rangle>"
  apply(induct rule: \<R>\<^sub>1.induct)
  apply(rule \<R>\<^sub>1.intro)
       apply(blast intro: sub context_equiv_refl pred_entailment_refl)+
  done

(* HERE *)
lemma R3_equiv_entailment:
  "\<langle>c, env, mem\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c', env', mem'\<rangle> \<Longrightarrow> 
    context_equiv \<Gamma>' P' \<Gamma>'' \<Longrightarrow> P' \<turnstile> P'' \<Longrightarrow> 
  \<forall>env. tyenv_wellformed env \<Gamma>' \<S>' P' \<longrightarrow> tyenv_wellformed env \<Gamma>'' \<S>' P'' \<Longrightarrow>
  \<langle>c, env, mem\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>'',\<S>',P''\<^esub> \<langle>c', env', mem'\<rangle>"
  apply(induct rule: \<R>\<^sub>3_aux.induct)
   apply(erule \<R>\<^sub>3_aux.intro\<^sub>1)
   apply(blast intro: sub context_equiv_refl pred_entailment_refl)
  apply(erule \<R>\<^sub>3_aux.intro\<^sub>3)
  apply(blast intro: sub context_equiv_refl pred_entailment_refl)
  done

lemma R_equiv_entailment:
  "lc\<^sub>1 \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> lc\<^sub>2 \<Longrightarrow> 
    context_equiv \<Gamma>' P' \<Gamma>'' \<Longrightarrow> P' \<turnstile> P'' \<Longrightarrow> 
  \<forall>env. tyenv_wellformed env \<Gamma>' \<S>' P' \<longrightarrow> tyenv_wellformed env \<Gamma>'' \<S>' P'' \<Longrightarrow>
  lc\<^sub>1 \<R>\<^sup>u\<^bsub>\<Gamma>'',\<S>',P''\<^esub> lc\<^sub>2"
  apply(induct rule: \<R>.induct)
   apply clarsimp
   apply(rule \<R>.intro\<^sub>1)
   apply(fastforce intro: R1_equiv_entailment)
  apply(rule \<R>.intro\<^sub>3)
  apply(fastforce intro: R3_equiv_entailment)
  done

lemma context_equiv_tyenv_eq:
  "tyenv_eq \<Gamma> mem mem' \<Longrightarrow> context_equiv \<Gamma> P \<Gamma>' \<Longrightarrow> pred P (to_prog_mem mem) \<Longrightarrow> tyenv_eq \<Gamma>' mem mem'"
  apply(clarsimp simp: tyenv_eq_def to_total_def context_equiv_def split: if_splits simp: type_equiv_def)
  using subtype_trans subtype_sound
proof -
  fix x :: 'Var and y :: "('Var, 'Val) lpred"
  assume a1: "\<Gamma>' x = Some y"
  assume a2: "\<forall>x. (type_max (the (\<Gamma> x)) (to_prog_mem mem) = Low \<longrightarrow> x \<in> dom \<Gamma>' \<longrightarrow> mem (ProgVar x) = mem' (ProgVar x)) \<and> (dma mem (ProgVar x) = Low \<longrightarrow> x \<in> dom \<Gamma>' \<or> mem (ProgVar x) = mem' (ProgVar x))"
  assume a3: "\<forall>x\<in>dom \<Gamma>'. the (\<Gamma> x) \<le>:\<^sub>P the (\<Gamma>' x) \<and> the (\<Gamma>' x) \<le>:\<^sub>P the (\<Gamma> x)"
  assume a4: "pred P (to_prog_mem mem)"
  assume a5: "type_max y (to_prog_mem mem) = Low"
  have "\<forall>f. \<not> pred (PConj P y) f \<or> pred (the (\<Gamma> x)) f"
    using a3 a1 by (simp add: domI pred_entailment_def subtype_def)
  then show "mem (ProgVar x) = mem' (ProgVar x)"
    using a5 a4 a2 a1 by (metis (no_types) domI lpred_eval.simps(4) type_max_def)
qed

(* This is the main part of the proof and used in \<R>\<^sub>1_weak_bisim: *)
lemma \<R>_typed_step:
  shows
"\<lbrakk> \<turnstile> \<Gamma>,\<S>,P { c\<^sub>1 } \<Gamma>',\<S>',P' ;
  tyenv_wellformed (mds,agrels) \<Gamma> \<S> P; mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2; pred P (to_prog_mem mem\<^sub>1);
        pred P (to_prog_mem mem\<^sub>2); global_invariant mem\<^sub>1; global_invariant mem\<^sub>2;  tyenv_sec mds \<Gamma> mem\<^sub>1;
     stable_consistent \<S> mem\<^sub>1; \<langle>c\<^sub>1, (mds,agrels), mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', (mds',agrels'), mem\<^sub>1'\<rangle>\<rbrakk> \<Longrightarrow>
   (\<exists> c\<^sub>2' mem\<^sub>2'. \<langle>c\<^sub>1, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', (mds',agrels'), mem\<^sub>2'\<rangle> \<and>
                 \<langle>c\<^sub>1', (mds',agrels'), mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2', (mds',agrels'), mem\<^sub>2'\<rangle>)"
proof (induct arbitrary: mds agrels c\<^sub>1' rule: has_type.induct)
  case (seq_type \<Gamma> \<S> P c\<^sub>1 \<Gamma>'' \<S>'' P'' c\<^sub>2 \<Gamma>' \<S>' P' mds agrels)
  show ?case
  proof (cases "c\<^sub>1 = Stop")
    assume "c\<^sub>1 = Stop"
    hence [simp]: "c\<^sub>1' = c\<^sub>2" "mds' = mds" "mem\<^sub>1' = mem\<^sub>1" "agrels' = agrels"
      using seq_type
      by (auto dest: seq_stopD)
    from seq_type `c\<^sub>1 = Stop` have "context_equiv \<Gamma> P \<Gamma>''" and "\<S> = \<S>''" and "P \<turnstile> P''" and
                                   "(\<forall>env. tyenv_wellformed env \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed env \<Gamma>'' \<S> P'')"
      by (metis stop_cxt)+
    hence "\<turnstile> \<Gamma>,\<S>,P { c\<^sub>2 } \<Gamma>',\<S>',P'"
      apply -
      apply(rule sub)
          using seq_type(3) apply simp
         by auto
    have "\<langle>c\<^sub>2, (mds,agrels), mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, (mds,agrels), mem\<^sub>2\<rangle>"
      apply (rule \<R>\<^sub>1.intro [of \<Gamma>])
           apply(rule `\<turnstile> \<Gamma>,\<S>,P { c\<^sub>2 } \<Gamma>',\<S>',P'`)
          using seq_type by auto
    thus ?case
      using \<R>.intro\<^sub>1
      apply clarify
      apply (rule_tac x = c\<^sub>2 in exI)
      apply (rule_tac x = mem\<^sub>2 in exI)
      by (auto simp: `c\<^sub>1 = Stop` seq_stop_eval\<^sub>w  \<R>.intro\<^sub>1)
  next
    assume "c\<^sub>1 \<noteq> Stop"
    with `\<langle>c\<^sub>1 ;; c\<^sub>2, (mds,agrels), mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', (mds',agrels'), mem\<^sub>1'\<rangle>` obtain c\<^sub>1'' where c\<^sub>1''_props:
      "\<langle>c\<^sub>1, (mds,agrels), mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1'', (mds',agrels'), mem\<^sub>1'\<rangle> \<and> c\<^sub>1' = c\<^sub>1'' ;; c\<^sub>2"
      by (metis seqD)
    with seq_type(2) obtain c\<^sub>2'' mem\<^sub>2' where c\<^sub>2''_props:
      "\<langle>c\<^sub>1, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2'', (mds',agrels'), mem\<^sub>2'\<rangle> \<and> \<langle>c\<^sub>1'', (mds',agrels'), mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>'',\<S>'',P''\<^esub> \<langle>c\<^sub>2'', (mds',agrels'), mem\<^sub>2'\<rangle>"
      using seq_type.prems(1) seq_type.prems(2) seq_type.prems(3) seq_type.prems(4) seq_type.prems(5) seq_type.prems(6) seq_type.prems(7) c\<^sub>1''_props
            seq_type.prems(8) seq_type.prems(9)
      by blast
    hence "\<langle>c\<^sub>1'' ;; c\<^sub>2, (mds',agrels'), mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2'' ;; c\<^sub>2, (mds',agrels'), mem\<^sub>2'\<rangle>"
      apply (rule conjE)
      apply (erule \<R>_elim, auto)
       apply (metis \<R>.intro\<^sub>3 \<R>\<^sub>3_aux.intro\<^sub>1 seq_type(3))
      by (metis \<R>.intro\<^sub>3 \<R>\<^sub>3_aux.intro\<^sub>3 seq_type(3))
    moreover
    from c\<^sub>2''_props have "\<langle>c\<^sub>1 ;; c\<^sub>2, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2'' ;; c\<^sub>2, (mds',agrels'), mem\<^sub>2'\<rangle>"
      by (metis eval\<^sub>w.seq)
    ultimately show ?case
      by (metis c\<^sub>1''_props)
  qed
next
  case stop_type
  with stop_no_eval show ?case by blast
next
  case (skip_type \<Gamma> \<S> P mds agrels)
  moreover
  with skip_type have [simp]: "mds' = mds" "c\<^sub>1' = Stop" "mem\<^sub>1' = mem\<^sub>1" "agrels' = agrels"
    by(auto dest: skipD)
  with skip_type have "\<langle>Stop, (mds,agrels), mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>Stop, (mds,agrels), mem\<^sub>2\<rangle>"
    by auto
  thus ?case
    using \<R>.intro\<^sub>1 and unannotated [where c = Skip]
    apply clarsimp
    by (metis (mono_tags, lifting) \<R>.intro\<^sub>1 old.prod.case skip_eval\<^sub>w)
next (* assign\<^sub>1 *)
  case (assign\<^sub>1 x  \<Gamma> e t P P' \<S> \<Gamma>' mds agrels)
  hence upd [simp]: "c\<^sub>1' = Stop" "mds' = mds" "mem\<^sub>1' = mem\<^sub>1 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)"
                    "agrels' = agrels"
    by(auto dest: assignD)
  from assign\<^sub>1 upd have \<C>_eq: "\<forall>x\<in>\<C>. mem\<^sub>1 x = mem\<^sub>1' x"
    using \<C>\<^sub>V_def
    by auto
  hence dma_eq [simp]: "dma mem\<^sub>1 = dma mem\<^sub>1'"
    using dma_\<C>  by blast
  have "mem\<^sub>1 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e) =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)"
  unfolding tyenv_eq_def
  proof(safe)
    fix v
    assume is_Low': "type_max (to_total \<Gamma> v) (to_prog_mem (mem\<^sub>1(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e))) = Low"
    show "(mem\<^sub>1(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) (ProgVar v) = (mem\<^sub>2(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)) (ProgVar v)"
    proof(cases "v \<in> dom \<Gamma>")
      assume [simp]: "v \<in> dom \<Gamma>"
      then obtain t' where [simp]: "\<Gamma> v = Some t'" by force
      hence [simp]: "(to_total \<Gamma> v) = t'"
        unfolding to_total_def by (auto split: if_splits)
      have "type_max t' (to_prog_mem mem\<^sub>1) = type_max t' (to_prog_mem mem\<^sub>1')"
        apply(rule \<C>_eq_type_max_eq)
         using `\<Gamma> v = Some t'` assign\<^sub>1 
         unfolding tyenv_wellformed_def types_wellformed_def
         apply (metis `v \<in> dom \<Gamma>` option.sel)
        using assign\<^sub>1
        by (simp add: \<C>\<^sub>V_def \<C>_eq to_prog_mem_def)
      with is_Low' have is_Low: "type_max (to_total \<Gamma> v) (to_prog_mem mem\<^sub>1) = Low"
        by simp
      from assign\<^sub>1(1) `v \<in> dom \<Gamma>` have "x \<noteq> v" by auto
      thus ?thesis
        apply simp
        using is_Low assign\<^sub>1 unfolding tyenv_eq_def by auto
    next
      assume "v \<notin> dom \<Gamma>"
      hence [simp]: "\<And>mem. type_max (to_total \<Gamma> v) (to_prog_mem mem) = dma mem (ProgVar v)"
        unfolding to_total_def by simp
      with is_Low' have "dma mem\<^sub>1' (ProgVar v) = Low" by simp
      with dma_eq have dma_v_Low: "dma mem\<^sub>1 (ProgVar v) = Low" by simp
      hence is_Low: "type_max (to_total \<Gamma> v) (to_prog_mem mem\<^sub>1) = Low" by simp
      show ?thesis
      proof(cases "x = v")
        assume "x \<noteq> v"
        thus ?thesis
          apply simp
          using is_Low assign\<^sub>1 unfolding tyenv_eq_def by blast
      next
        assume "x = v"
        thus ?thesis
          using ev\<^sub>A_eq assign\<^sub>1 dma_v_Low
          apply simp
          apply(rule ev\<^sub>A_eq)
              apply(rule assign\<^sub>1(10))
             apply(rule assign\<^sub>1(11))
            apply(rule assign\<^sub>1(4))
           apply(rule assign\<^sub>1(5))
          using dma_v_Low by simp
      qed
    qed
  next
    fix l
    have "mem\<^sub>1 (Lock l) = mem\<^sub>2 (Lock l)"
      using `tyenv_eq \<Gamma> mem\<^sub>1 mem\<^sub>2`
      unfolding tyenv_eq_def by blast
    thus "(mem\<^sub>1(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) (Lock l) =
          (mem\<^sub>2(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)) (Lock l)"
      by simp
  qed

  moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (mds',agrels') \<Gamma> \<S> P'"
    using upd tyenv_wellformed_preds_update assign\<^sub>1
    using restrict_preds_to_vars_restricts by auto
  moreover have "pred P (to_prog_mem mem\<^sub>1) \<longrightarrow> pred P' (to_prog_mem mem\<^sub>1')"
    using assign\<^sub>1 upd prog_mem_assign_helper
    using assign_post_sound pred_entailment_def restrict_preds_to_vars_weakens by blast

  moreover have "pred P (to_prog_mem mem\<^sub>2) \<longrightarrow> pred P' (to_prog_mem (mem\<^sub>2(ProgVar x :=
    ev\<^sub>A (to_prog_mem mem\<^sub>2) e)))"
    using assign\<^sub>1 upd prog_mem_assign_helper
    using assign_post_sound pred_entailment_def restrict_preds_to_vars_weakens 
    proof -
      have "\<And>f. pred P' f \<or> \<not> pred (assign_post P x e) f"
        using assign\<^sub>1.hyps(6) pred_entailment_def restrict_preds_to_vars_weakens by blast
      then show ?thesis
        by (meson assign_eval\<^sub>w' assign_post_sound)
    qed

  moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P  \<and> tyenv_sec mds \<Gamma> mem\<^sub>1 \<longrightarrow> tyenv_sec mds \<Gamma> mem\<^sub>1'"
    using tyenv_sec_eq[OF \<C>_eq, where \<Gamma>=\<Gamma>]
    unfolding tyenv_wellformed_def by blast

  moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<and> stable_consistent \<S> mem\<^sub>1 \<and> global_invariant mem\<^sub>1 \<longrightarrow> global_invariant mem\<^sub>1'"
    using var_modifiable_global_invariant_preserved upd unprotected_modifiable
    using assign\<^sub>1.hyps(1) assign\<^sub>1.hyps(2) assign\<^sub>1.hyps(3) by blast

  moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<and> stable_consistent \<S> mem\<^sub>2 \<and> global_invariant mem\<^sub>2 \<longrightarrow> global_invariant (mem\<^sub>2(ProgVar x :=
    ev\<^sub>A (to_prog_mem mem\<^sub>2) e))"
    using var_modifiable_global_invariant_preserved upd unprotected_modifiable
    using assign\<^sub>1.hyps(1) assign\<^sub>1.hyps(2) assign\<^sub>1.hyps(3) by blast
 
  moreover have "stable_consistent \<S> mem\<^sub>2"
    using `stable_consistent \<S> mem\<^sub>1` `tyenv_eq \<Gamma> mem\<^sub>1 mem\<^sub>2` by (blast intro: stable_consistent_tyenv_eq)

  moreover have "stable_consistent \<S> mem\<^sub>1'"
    using upd(3) `stable_consistent \<S> mem\<^sub>1` apply(clarsimp simp: stable_consistent_def)
    using eval_det\<^sub>L
    by (metis Var.distinct(1) fun_upd_other mem_Collect_eq subsetCE)

  ultimately have \<R>':
    "\<langle>Stop, (mds',agrels'), mem\<^sub>1 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P'\<^esub> \<langle>Stop, (mds',agrels'), mem\<^sub>2 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
    apply -
    apply (rule \<R>.intro\<^sub>1, auto simp: assign\<^sub>1 upd(3) simp del: dma_eq)
    done

  have a: "\<langle>x \<leftarrow> e, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>Stop, (mds',agrels'), mem\<^sub>2 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
  using  eval\<^sub>w.unannotated eval\<^sub>w_simple.assign
  by (simp add: assign\<^sub>1.hyps assign_eval\<^sub>w')

  from \<R>' a show ?case
    using `c\<^sub>1' = Stop` and `mem\<^sub>1' = mem\<^sub>1 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)` assign\<^sub>1.hyps
    by blast
next (* assign\<^sub>\<C> *)
  case (assign\<^sub>\<C> x \<S> \<Gamma> e t P P' \<Gamma>' mds agrels)
  hence upd [simp]: "c\<^sub>1' = Stop" "mds' = mds" "mem\<^sub>1' = mem\<^sub>1 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)"
                    "agrels' = agrels"
    by (auto dest: assignD)
  have "mem\<^sub>1 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e) =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)"
  unfolding tyenv_eq_def
  proof(safe)
    fix v
    assume is_Low': "type_max (to_total \<Gamma> v) (to_prog_mem (mem\<^sub>1(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e))) = Low"
    show "(mem\<^sub>1(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) (ProgVar v) = (mem\<^sub>2(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)) (ProgVar v)"
    proof(cases "v \<in> dom \<Gamma>")
      assume in_dom [simp]: "v \<in> dom \<Gamma>"
      then obtain t' where \<Gamma>v [simp]: "\<Gamma> v = Some t'" by force
      hence [simp]: "(to_total \<Gamma> v) = t'"
        unfolding to_total_def by (auto split: if_splits)
      from assign\<^sub>\<C> have x_nin_C: "x \<notin> lpred_vars t'"
        using in_dom \<Gamma>v 
        by (metis option.sel snd_conv)
      have \<Gamma>v_wf: "type_wellformed t'"
        using in_dom \<Gamma>v assign\<^sub>\<C> unfolding tyenv_wellformed_def types_wellformed_def
        by (metis option.sel)
      with x_nin_C have f_eq: "type_max t' (to_prog_mem mem\<^sub>1) = type_max t' (to_prog_mem mem\<^sub>1')"
        using vars_of_type_eq_type_max_eq
        by (simp add: fun_upd_other prog_mem_assign_helper upd(3))
      with is_Low' have is_Low: "type_max (to_total \<Gamma> v) (to_prog_mem mem\<^sub>1) = Low"
        by simp  
      from assign\<^sub>\<C> `v \<in> dom \<Gamma>` have "x \<noteq> v"
        by(auto simp: tyenv_wellformed_def env_consistent_def \<C>\<^sub>V_def)
      thus ?thesis
        apply simp
        using is_Low assign\<^sub>\<C> unfolding tyenv_eq_def by auto
    next
      assume nin_dom: "v \<notin> dom \<Gamma>"
      hence [simp]: "\<And>mem. type_max (to_total \<Gamma> v) (to_prog_mem mem) = dma mem (ProgVar v)"
        unfolding to_total_def  by simp
      with is_Low' have "dma mem\<^sub>1' (ProgVar v) = Low" by simp
      show ?thesis
      proof(cases "x = v")
        assume "x = v"
        thus ?thesis
          apply simp
          apply(rule ev\<^sub>A_eq')
             apply(rule assign\<^sub>\<C>(11))
            apply(rule assign\<^sub>\<C>(12))
           apply(rule assign\<^sub>\<C>(3))
          by (rule assign\<^sub>\<C>(4))
      next
        assume [simp]: "x \<noteq> v"
        show ?thesis
        proof(cases "x \<in> \<C>_vars\<^sub>V v")
          assume in_\<C>_vars: "x \<in> \<C>_vars\<^sub>V v"
          hence "v \<notin> \<C>\<^sub>V"
            using \<C>\<^sub>V_def \<C>_vars\<^sub>V_def
            using \<C>_vars_\<C> by auto
          with nin_dom have "v \<notin> stable_NoRW \<S>"
            using assign\<^sub>\<C>
            apply(clarsimp simp: tyenv_wellformed_def env_consistent_def stable_def stable_NoRW_def)
            using \<C>\<^sub>V_def by auto
          with in_\<C>_vars have "P \<turnstile> (to_total \<Gamma> v)"
            using assign\<^sub>\<C>(7) by metis
          with assign\<^sub>\<C>.prems have "type_max (to_total \<Gamma> v) (to_prog_mem mem\<^sub>1) = Low"
            by(auto simp: type_max_def pred_def pred_entailment_def)
          thus ?thesis
            using not_sym[OF `x \<noteq> v`]
            apply simp
            using assign\<^sub>\<C>.prems
            unfolding tyenv_eq_def by auto
        next
          assume "x \<notin> \<C>_vars\<^sub>V v"
          with is_Low' have "dma mem\<^sub>1 (ProgVar v) = Low"
            using dma_\<C>_vars `\<And>mem. type_max (to_total \<Gamma> v) (to_prog_mem mem) = dma mem (ProgVar v)`
                  \<C>_vars\<^sub>V_def
            by (metis CollectI fun_upd_other)
          thus ?thesis
            using not_sym[OF `x \<noteq> v`]
            apply simp
            using assign\<^sub>\<C>.prems
            unfolding tyenv_eq_def by auto            
        qed
      qed
    qed
  next
    fix l
    have "mem\<^sub>1 (Lock l) = mem\<^sub>2 (Lock l)"
      using `tyenv_eq \<Gamma> mem\<^sub>1 mem\<^sub>2`
      unfolding tyenv_eq_def by blast
    thus "(mem\<^sub>1(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) (Lock l) =
          (mem\<^sub>2(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)) (Lock l)"
      by simp
  qed
  moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (mds',agrels') \<Gamma> \<S> P'"
    using upd tyenv_wellformed_preds_update assign\<^sub>\<C> restrict_preds_to_vars_restricts by metis
  moreover have "pred P (to_prog_mem mem\<^sub>1) \<longrightarrow> pred P' (to_prog_mem mem\<^sub>1')"
    using  assign\<^sub>\<C> upd assign_post_sound
    by (meson sifum_types.pred_entailment_def sifum_types.restrict_preds_to_vars_weakens sifum_types_axioms)
  moreover have "pred P (to_prog_mem mem\<^sub>2) \<longrightarrow> pred P' (to_prog_mem (mem\<^sub>2(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)))"
    using  assign\<^sub>\<C> upd assign_post_sound
    by (meson assign_eval\<^sub>w' pred_entailment_def restrict_preds_to_vars_weakens)
  moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<and> pred P (to_prog_mem mem\<^sub>1) \<and> tyenv_sec mds \<Gamma> mem\<^sub>1 \<Longrightarrow> tyenv_sec mds' \<Gamma> mem\<^sub>1'"
  proof(clarify)
    fix v t'
    assume wf: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P"
    assume pred: "pred P (to_prog_mem mem\<^sub>1)"
    hence pred': "pred P' (to_prog_mem mem\<^sub>1')" using `pred P (to_prog_mem mem\<^sub>1) \<longrightarrow> pred P' (to_prog_mem mem\<^sub>1')` by blast
    assume sec: "tyenv_sec mds \<Gamma> mem\<^sub>1"
    assume \<Gamma>v: "\<Gamma> v = Some t'"
    assume readable': "ProgVar v \<notin> mds' AsmNoReadOrWrite"
    with upd have readable: "ProgVar v \<notin> mds AsmNoReadOrWrite" by simp
    with wf have "v \<notin> stable_NoRW \<S>" by(auto simp: tyenv_wellformed_def env_consistent_def)
    show "type_max (the (\<Gamma> v)) (to_prog_mem mem\<^sub>1') \<le> dma mem\<^sub>1' (ProgVar v)"
    proof(cases "x \<in> \<C>_vars\<^sub>V v")
      assume "x \<in> \<C>_vars\<^sub>V v"
      with assign\<^sub>\<C>(7) `v \<notin> stable_NoRW \<S>` have "(to_total \<Gamma> v) \<le>:\<^sub>P' bexp_to_lpred (dma_type v)" by metis
      from pred' \<Gamma>v subtype_correct this show ?thesis
        using type_max_dma_type by(auto simp: to_total_def split: if_splits)
    next
      assume "x \<notin> \<C>_vars\<^sub>V v"
      hence "\<forall>v'\<in>\<C>_vars\<^sub>V v. mem\<^sub>1 (ProgVar v') = mem\<^sub>1' (ProgVar v')"
        using upd by auto
      hence dma_eq: "dma mem\<^sub>1 (ProgVar v) = dma mem\<^sub>1' (ProgVar v)"
        using dma_\<C>_vars \<C>_vars\<^sub>V_def
        by (metis (no_types, lifting) fun_upd_other mem_Collect_eq upd(3))
      from \<Gamma>v assign\<^sub>\<C>(5) have "x \<notin> lpred_vars t'" by force
      have "type_wellformed t'"
        using wf \<Gamma>v by(force simp: tyenv_wellformed_def types_wellformed_def)
      with `x \<notin> lpred_vars t'` upd have f_eq: "type_max t' (to_prog_mem mem\<^sub>1) = type_max t' (to_prog_mem mem\<^sub>1')"
        using vars_of_type_eq_type_max_eq
        by (simp add: fun_upd_other prog_mem_assign_helper)
      from sec \<Gamma>v readable have "type_max t' (to_prog_mem mem\<^sub>1) \<le> dma mem\<^sub>1 (ProgVar v)"
        by auto
      with f_eq dma_eq \<Gamma>v show ?thesis
        by simp
    qed
  qed

  moreover have ginv_pres: "global_invariant mem\<^sub>1 \<and> stable_consistent \<S> mem\<^sub>1 \<longrightarrow> global_invariant mem\<^sub>1'"
    using var_modifiable_global_invariant_preserved
    using assign\<^sub>\<C>.hyps(2) upd by blast

  moreover have ginv_pres\<^sub>2: "global_invariant mem\<^sub>2 \<and> stable_consistent \<S> mem\<^sub>2 \<longrightarrow> global_invariant (mem\<^sub>2(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e))"
    using var_modifiable_global_invariant_preserved
    using assign\<^sub>\<C>.hyps(2) upd by blast

  moreover have sc: "stable_consistent \<S> mem\<^sub>1 \<longrightarrow> stable_consistent \<S> mem\<^sub>1'"
     by(fastforce simp: upd intro: stable_consistent_trivial)

  moreover have sc\<^sub>2: "stable_consistent \<S> mem\<^sub>2" using stable_consistent_tyenv_eq
    using assign\<^sub>\<C>.prems(2) assign\<^sub>\<C>.prems(8) by blast

  ultimately have \<R>':
    "\<langle>Stop, (mds',agrels'), mem\<^sub>1 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P'\<^esub> \<langle>Stop, (mds',agrels'), mem\<^sub>2 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
    apply -
    by (rule \<R>.intro\<^sub>1, auto simp: assign\<^sub>\<C>)

  have a: "\<langle>x \<leftarrow> e, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>Stop, (mds',agrels'), mem\<^sub>2 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
    by (simp add: assign\<^sub>\<C>.hyps assign_eval\<^sub>w')

  from \<R>' a show ?case
    using `c\<^sub>1' = Stop` and `mem\<^sub>1' = mem\<^sub>1 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)` assign\<^sub>\<C>.hyps
    by blast
next (* assign\<^sub>2 *)
  case (assign\<^sub>2 x \<Gamma> \<S> e t P' P \<Gamma>' mds agrels)
  have upd [simp]: "c\<^sub>1' = Stop" "mds' = mds" "mem\<^sub>1' = mem\<^sub>1 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)" "agrels' = agrels"
    using assign\<^sub>2  assignD fst_conv by blast+

  from `x \<in> dom \<Gamma>` `tyenv_wellformed (mds,agrels) \<Gamma> \<S> P`
  have x_nin_\<C>: "ProgVar x \<notin> \<C>"
    by(auto simp: tyenv_wellformed_def env_consistent_def)
  hence dma_eq [simp]: "dma mem\<^sub>1' = dma mem\<^sub>1"
    using dma_\<C> assign\<^sub>2
    by auto
    
  let ?\<Gamma>' = "\<Gamma> (x \<mapsto> t)"
  have "\<langle>x \<leftarrow> e, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>Stop, (mds,agrels), mem\<^sub>2 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
    using assign\<^sub>2
    by (simp add: assign_eval\<^sub>w')
    
  moreover
  have tyenv_eq': "mem\<^sub>1(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e) =\<^bsub>\<Gamma>(x \<mapsto> t)\<^esub> mem\<^sub>2(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)"
  unfolding tyenv_eq_def
  proof(safe)
    fix v
    assume is_Low': "type_max (to_total (\<Gamma>(x \<mapsto> t)) v) (to_prog_mem (mem\<^sub>1(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e))) = Low"
    show "(mem\<^sub>1(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) (ProgVar v) = (mem\<^sub>2(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)) (ProgVar v)"
    proof(cases "v = x")
      assume neq: "v \<noteq> x"
      hence "type_max (to_total \<Gamma> v) (to_prog_mem mem\<^sub>1) = Low"
      proof(cases "v \<in> dom \<Gamma>")
        assume "v \<in> dom \<Gamma>"
        then obtain t' where [simp]: "\<Gamma> v = Some t'" by force
        hence [simp]: "(to_total \<Gamma> v) = t'"
          unfolding to_total_def by (auto split: if_splits)
        hence [simp]: "(to_total ?\<Gamma>' v) = t'"
          using neq by(auto simp: to_total_def)
        have "type_max t' (to_prog_mem mem\<^sub>1) = type_max t' (to_prog_mem mem\<^sub>1')"
          apply(rule \<C>_eq_type_max_eq)
            using assign\<^sub>2.prems 
            apply(clarsimp simp: tyenv_wellformed_def types_wellformed_def)
            using `v \<in> dom \<Gamma>` `\<Gamma> v = Some t'` apply(metis option.sel)
           using x_nin_\<C> \<C>\<^sub>V_def 
           by (simp add: fun_upd_apply mem_Collect_eq to_prog_mem_def upd(3))
        from this is_Low' neq neq[THEN not_sym] show "type_max (to_total \<Gamma> v) (to_prog_mem mem\<^sub>1) = Low"
          by auto
      next
        assume "v \<notin> dom \<Gamma>"
        with is_Low' neq
        have "dma mem\<^sub>1' (ProgVar v) = Low"
          by(auto simp: to_total_def  split: if_splits)
        with dma_eq `v \<notin> dom \<Gamma>` show ?thesis
          by(auto simp: to_total_def  split: if_splits)
      qed
      with neq assign\<^sub>2.prems show "(mem\<^sub>1(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) (ProgVar v) = (mem\<^sub>2(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)) (ProgVar v)"
        by(auto simp: tyenv_eq_def)
    next
      assume eq[simp]: "v = x"
      with is_Low' `x \<in> dom \<Gamma>` have t_Low': "type_max t (to_prog_mem mem\<^sub>1') = Low"
        by(auto simp: to_total_def split: if_splits)
      have wf_t: "type_wellformed t"
        using type_aexpr_type_wellformed assign\<^sub>2
        by(fastforce simp: tyenv_wellformed_def)
      with t_Low' `ProgVar x \<notin> \<C>` have t_Low: "type_max t (to_prog_mem mem\<^sub>1) = Low"
        using \<C>_eq_type_max_eq
        by (metis (no_types, lifting) \<C>\<^sub>V_def fun_upd_def mem_Collect_eq prog_mem_assign_helper upd(3))
      show ?thesis
      proof(simp, rule eval_vars_det\<^sub>A, clarify)
        fix y  
        assume in_vars: "y \<in> aexp_vars e"
        have "type_max (to_total \<Gamma> y) (to_prog_mem mem\<^sub>1) = Low"
        proof -
          from t_Low in_vars assign\<^sub>2 show ?thesis
            apply -
            apply(erule type_aexpr.cases)
            apply(clarsimp simp: type_max_def split: if_splits)
            apply(rule pred_fold_PConj[THEN conjunct1, rule_format])
             apply assumption
            by simp
        qed
        thus "(to_prog_mem mem\<^sub>1) y = (to_prog_mem mem\<^sub>2) y"
          using assign\<^sub>2 unfolding tyenv_eq_def to_prog_mem_def by blast
      qed
    qed
  next
    fix l
    have "mem\<^sub>1 (Lock l) = mem\<^sub>2 (Lock l)"
      using `tyenv_eq \<Gamma> mem\<^sub>1 mem\<^sub>2`
      unfolding tyenv_eq_def by blast
    thus "(mem\<^sub>1(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)) (Lock l) =
          (mem\<^sub>2(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)) (Lock l)"
      by simp
  qed

  from upd have ty: " \<turnstile> ?\<Gamma>',\<S>,P' {c\<^sub>1'} ?\<Gamma>',\<S>,P'"
    by (metis stop_type)
  have wf: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (mds',agrels') ?\<Gamma>' \<S> P'"
  proof
    assume tyenv_wf: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P"
    hence wf: "types_wellformed \<Gamma>"
      unfolding tyenv_wellformed_def by blast
    hence  "type_wellformed t"
      using assign\<^sub>2 type_aexpr_type_wellformed
      by blast
    with wf have wf': "types_wellformed ?\<Gamma>'"
      using types_wellformed_update by metis
    from tyenv_wf have stable': "types_stable ?\<Gamma>' \<S>"
      using types_stable_update
            assign\<^sub>2(4)
      unfolding tyenv_wellformed_def by blast
    have m: "env_consistent (mds,agrels) \<Gamma> \<S> P"
      using tyenv_wf unfolding tyenv_wellformed_def by blast
    from assign\<^sub>2(5) assign\<^sub>2(1)
    have "env_consistent (mds',agrels') (\<Gamma>(x \<mapsto> t)) \<S> P'"
      apply -
      apply(rule env_consistent_preds_tyenv_update)
      using upd m restrict_preds_to_vars_restricts by blast+
    from wf' stable' this show "tyenv_wellformed (mds',agrels') ?\<Gamma>' \<S> P'"
      unfolding tyenv_wellformed_def by blast
  qed


  have p: "pred P (to_prog_mem mem\<^sub>1) \<longrightarrow> pred P' (to_prog_mem mem\<^sub>1')"
    using  assign\<^sub>2 upd prog_mem_assign_helper
    using assign_post_sound pred_entailment_def restrict_preds_to_vars_weakens by blast
  have p\<^sub>2: "pred P (to_prog_mem mem\<^sub>2) \<longrightarrow> pred P' (to_prog_mem (mem\<^sub>2(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)))"
    using assign\<^sub>2 upd prog_mem_assign_helper
    by (metis (no_types, lifting) assign_post_sound calculation sifum_types.pred_entailment_def sifum_types.restrict_preds_to_vars_weakens sifum_types_axioms)

  have sec: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<Longrightarrow> pred P (to_prog_mem mem\<^sub>1) \<Longrightarrow> tyenv_sec mds \<Gamma> mem\<^sub>1 \<Longrightarrow> tyenv_sec mds' ?\<Gamma>' mem\<^sub>1'"
  proof(clarify)
    assume wf: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P"
    assume pred: "pred P (to_prog_mem mem\<^sub>1)"
    assume sec: "tyenv_sec mds \<Gamma> mem\<^sub>1"
    from pred p have pred': "pred P' (to_prog_mem mem\<^sub>1')" by blast
    fix v t'
    assume \<Gamma>v: "(\<Gamma>(x \<mapsto> t)) v = Some t'"
    assume "ProgVar v \<notin> mds' AsmNoReadOrWrite"
    show "type_max (the ((\<Gamma>(x \<mapsto> t)) v)) (to_prog_mem mem\<^sub>1') \<le> dma mem\<^sub>1' (ProgVar v)"
    proof(cases "v = x")
      assume [simp]: "v = x"
      hence [simp]: "(the ((\<Gamma>(x \<mapsto> t)) v)) = t" and t_def: "t = t'"
        using \<Gamma>v by auto
      from `ProgVar v \<notin> mds' AsmNoReadOrWrite` upd wf have readable: "x \<notin> stable_NoRW \<S>"
        by(auto simp: tyenv_wellformed_def env_consistent_def stable_NoRW_def)
      with assign\<^sub>2.hyps(6) have "t \<le>:\<^sub>P' bexp_to_lpred (dma_type x)" by fastforce
      with pred' show ?thesis
        using type_max_dma_type subtype_correct
        by fastforce
    next
      assume neq: "v \<noteq> x"
      hence [simp]: "((\<Gamma>(x \<mapsto> t)) v) = \<Gamma> v"
        by simp
      with \<Gamma>v have \<Gamma>v: "\<Gamma> v = Some t'" by simp
      with sec upd `ProgVar v \<notin> mds' AsmNoReadOrWrite` have f_leq: "type_max t' (to_prog_mem mem\<^sub>1) \<le> dma mem\<^sub>1 (ProgVar v)"
        by auto
      have \<C>_eq: "\<forall>x\<in>\<C>. mem\<^sub>1 x = mem\<^sub>1' x"
        using wf assign\<^sub>2 upd by(auto simp: tyenv_wellformed_def env_consistent_def)
      hence dma_eq: "dma mem\<^sub>1 = dma mem\<^sub>1'"
        by(rule dma_\<C>)
      have f_eq: "type_max t' (to_prog_mem mem\<^sub>1) = type_max t' (to_prog_mem mem\<^sub>1')"
        apply(rule \<C>_eq_type_max_eq)
         using \<Gamma>v wf apply(force simp: tyenv_wellformed_def types_wellformed_def)
        by(auto simp: \<C>_eq \<C>\<^sub>V_def to_prog_mem_def)     
      from neq \<Gamma>v f_leq dma_eq f_eq show ?thesis
        by simp
    qed
  qed

  have ginv_pres: "global_invariant mem\<^sub>1 \<and> stable_consistent \<S> mem\<^sub>1 \<longrightarrow> global_invariant mem\<^sub>1'"
    using var_modifiable_global_invariant_preserved
    using assign\<^sub>2.hyps(2) upd by blast

  moreover have ginv_pres\<^sub>2: "global_invariant mem\<^sub>2 \<and> stable_consistent \<S> mem\<^sub>2 \<longrightarrow> global_invariant (mem\<^sub>2(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e))"
    using var_modifiable_global_invariant_preserved
    using assign\<^sub>2.hyps(2) upd by blast


  moreover have sc\<^sub>2: "stable_consistent \<S> mem\<^sub>2" using stable_consistent_tyenv_eq
    using assign\<^sub>2.prems(2) assign\<^sub>2.prems(8) by blast

  moreover have sc: "stable_consistent \<S> mem\<^sub>1 \<longrightarrow> stable_consistent \<S> mem\<^sub>1'"
     by(fastforce simp: upd intro: stable_consistent_trivial)

  ultimately have inone: "\<langle>Stop, (mds,agrels), mem\<^sub>1 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)\<rangle> \<R>\<^sup>1\<^bsub>?\<Gamma>',\<S>,P'\<^esub> \<langle>Stop, (mds,agrels), mem\<^sub>2 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
    apply -
    apply(rule \<R>\<^sub>1.intro)
         apply blast
        using wf assign\<^sub>2 apply fastforce
       apply(rule tyenv_eq')
      using p assign\<^sub>2 apply fastforce
     using p\<^sub>2 assign\<^sub>2 apply fastforce
    using sec assign\<^sub>2
    using upd(2) upd(3)
    using fst_conv by auto
    
  have "\<langle>x \<leftarrow> e, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>Stop, (mds',agrels'), mem\<^sub>2 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>"
    "\<langle>Stop, (mds',agrels'), mem\<^sub>1 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>(x \<mapsto> t),\<S>,P'\<^esub> \<langle>Stop, (mds',agrels'), mem\<^sub>2 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>" 
    using \<open>\<langle>x \<leftarrow> e, (mds, agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>Stop, (mds, agrels), mem\<^sub>2 (ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>2) e)\<rangle>\<close> upd(2) upd(4) apply blast
    using \<R>.intro\<^sub>1 inone
    by (simp add: case_prod_conv upd(2) upd(4))
  with assign\<^sub>2.hyps show ?case
    using `mds' = mds` `c\<^sub>1' = Stop` `mem\<^sub>1' = mem\<^sub>1(ProgVar x := ev\<^sub>A (to_prog_mem mem\<^sub>1) e)` `agrels' = agrels`
    by blast
next (* if *)
  case (if_type \<Gamma> e t P p \<S> th \<Gamma>' \<S>' P' el \<Gamma>'' P'' \<Gamma>''' P''' mds agrels)
  let ?P = "if (ev\<^sub>B (to_prog_mem mem\<^sub>1) e) then P +\<^sub>\<S> p else P +\<^sub>\<S> (PNeg p)"
  from `\<langle>Stmt.If e th el, (mds,agrels), mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', (mds',agrels'), mem\<^sub>1'\<rangle>` have ty: "\<turnstile> \<Gamma>,\<S>,?P {c\<^sub>1'} \<Gamma>''',\<S>',P'''"
  proof (rule ifD)
    assume "c\<^sub>1' = th" "mem\<^sub>1' = mem\<^sub>1" "(mds',agrels') = (mds,agrels)" "ev\<^sub>B (to_prog_mem mem\<^sub>1) e"
    with if_type(4)
    show ?thesis
      apply simp
      apply(erule sub)
         using if_type apply simp+
      done
  next
    assume "c\<^sub>1' = el" "mem\<^sub>1' = mem\<^sub>1" "(mds',agrels') = (mds,agrels)" "\<not> ev\<^sub>B (to_prog_mem mem\<^sub>1) e"
    with if_type(6)
    show ?thesis
      apply simp
      apply(erule sub)
         using if_type apply simp+
      done
  qed
  have ev\<^sub>B_eq [simp]: "ev\<^sub>B (to_prog_mem mem\<^sub>1) e = ev\<^sub>B (to_prog_mem mem\<^sub>2) e"
    apply(rule ev\<^sub>B_eq')
       apply(rule `mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2`)
      apply(rule `pred P (to_prog_mem mem\<^sub>1)`)
     apply(rule `\<Gamma> \<turnstile>\<^sub>b e \<in> t`)
    by(rule `P \<turnstile> t`)
  have "(\<langle>c\<^sub>1', (mds,agrels), mem\<^sub>1\<rangle>, \<langle>c\<^sub>1', (mds,agrels), mem\<^sub>2\<rangle>) \<in> \<R> \<Gamma>''' \<S>' P'''"
    apply (rule intro\<^sub>1)
    apply clarify
    apply (rule \<R>\<^sub>1.intro [where \<Gamma> = \<Gamma> and \<Gamma>' = \<Gamma>''' and \<S> = \<S> and P = ?P])
         apply(rule ty)
        using `tyenv_wellformed (mds,agrels) \<Gamma> \<S> P`
        apply(auto simp: tyenv_wellformed_def env_consistent_def add_pred_def)[1]
       apply(rule `mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2`)
      using `pred P (to_prog_mem mem\<^sub>1)` `p = bexp_to_lpred e` apply(clarsimp simp: pred_def add_pred_def bexp_to_lpred_correct)
     using `pred P (to_prog_mem mem\<^sub>2)` `p = bexp_to_lpred e` apply(fastforce simp: pred_def add_pred_def bexp_to_lpred_correct)
    using `tyenv_sec mds \<Gamma> mem\<^sub>1` if_type apply simp+
    done

  show ?case
  proof -
    from ev\<^sub>B_eq if_type(13) have "(\<langle>If e th el, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>1', (mds,agrels), mem\<^sub>2\<rangle>)"
      apply (cases "ev\<^sub>B (to_prog_mem mem\<^sub>1) e")
       apply (subgoal_tac "c\<^sub>1' = th")
        apply clarify
        apply (metis eval\<^sub>w_simplep.if_true eval\<^sub>wp.unannotated eval\<^sub>wp_eval\<^sub>w_eq if_type(8))
       using if_type.prems(9) apply blast
      apply (subgoal_tac "c\<^sub>1' = el")
       apply (metis (hide_lams, mono_tags) eval\<^sub>w.unannotated eval\<^sub>w_simple.if_false if_type(8))
      using if_type.prems(9) by blast
    with `\<langle>c\<^sub>1', (mds,agrels), mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>''',\<S>',P'''\<^esub> \<langle>c\<^sub>1', (mds,agrels), mem\<^sub>2\<rangle>` show ?thesis  
    using if_type.prems(9) by blast
  qed
next (* while *)
  case (while_type \<Gamma> e t P \<S> c)
  hence [simp]: "c\<^sub>1' = (If e (c ;; While e c) Stop)" and
    [simp]: "mds' = mds" and
    [simp]: "mem\<^sub>1' = mem\<^sub>1" and [simp]: "agrels' = agrels"
    by (auto dest: whileD)

  with while_type have "\<langle>While e c, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>1', (mds,agrels), mem\<^sub>2\<rangle>"
    by (metis  eval\<^sub>w_simplep.while eval\<^sub>wp.unannotated eval\<^sub>wp_eval\<^sub>w_eq)
    
  moreover have ty: " \<turnstile> \<Gamma>,\<S>,P {c\<^sub>1'} \<Gamma>,\<S>,P"
    apply simp
    apply(rule if_type)
               apply(rule while_type(1))
              apply(rule while_type(2))
             apply simp
             apply(rule seq_type)
              apply(rule while_type(3))
             apply(rule has_type.while_type)
               apply(rule while_type(1))
              apply(rule while_type(2))
             apply(rule while_type(3))
            apply(rule `bexp_readable \<S> e`)
           apply(rule stop_type)
          apply simp+
      apply(rule add_pred_entailment)
     apply simp
    apply(blast intro!:  tyenv_wellformed_add_pred `bexp_readable \<S> e`)+
    done
  moreover
  have "\<langle>c\<^sub>1', (mds,agrels), mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>1', (mds,agrels), mem\<^sub>2\<rangle>"
    apply (rule \<R>\<^sub>1.intro [where \<Gamma> = \<Gamma>])
         apply(rule ty)
        using while_type apply simp+
    done
  hence "\<langle>c\<^sub>1', (mds,agrels), mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>1', (mds,agrels), mem\<^sub>2\<rangle>"
    using \<R>.intro\<^sub>1 by auto
  ultimately show ?case
    by fastforce
next
  case (sub \<Gamma>\<^sub>1 \<S> P\<^sub>1 c \<Gamma>\<^sub>1' \<S>' P\<^sub>1' \<Gamma>\<^sub>2 P\<^sub>2 \<Gamma>\<^sub>2' P\<^sub>2' mds agrels c\<^sub>1')
  have imp: "tyenv_wellformed (mds,agrels) \<Gamma>\<^sub>2 \<S> P\<^sub>2 \<and> pred P\<^sub>2 (to_prog_mem mem\<^sub>1) \<and> pred P\<^sub>2 (to_prog_mem mem\<^sub>2) \<and> tyenv_sec mds \<Gamma>\<^sub>2 mem\<^sub>1 \<Longrightarrow> 
             tyenv_wellformed (mds,agrels) \<Gamma>\<^sub>1 \<S> P\<^sub>1 \<and> pred P\<^sub>1 (to_prog_mem mem\<^sub>1) \<and> pred P\<^sub>1 (to_prog_mem mem\<^sub>2) \<and> tyenv_sec mds \<Gamma>\<^sub>1 mem\<^sub>1"
    apply(rule conjI)
     using sub(5) sub(4)
     apply blast
    apply(rule conjI)
     using  pred_entailment_def sub.hyps(7) apply auto[1]
    apply(rule conjI)
     using  pred_entailment_def sub.hyps(7) apply auto[1]
    using sub(3) context_equiv_tyenv_sec unfolding pred_def by blast

  with sub have conc: "tyenv_wellformed (mds,agrels) \<Gamma>\<^sub>1 \<S> P\<^sub>1 \<and> pred P\<^sub>1 (to_prog_mem mem\<^sub>1) \<and> pred P\<^sub>1 (to_prog_mem mem\<^sub>2) \<and> tyenv_sec mds \<Gamma>\<^sub>1 mem\<^sub>1"
    by blast

  have tyenv_eq: "mem\<^sub>1 =\<^bsub>\<Gamma>\<^sub>1\<^esub> mem\<^sub>2"
    using context_equiv_tyenv_eq sub by blast
    
  from conc sub(2)[where mds1=mds and agrels1=agrels, simplified conc tyenv_eq `global_invariant mem\<^sub>1` `global_invariant mem\<^sub>2` `stable_consistent \<S> mem\<^sub>1`]
   obtain c\<^sub>2' mem\<^sub>2' where c\<^sub>2'_props: "\<langle>c, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', (mds',agrels'), mem\<^sub>2'\<rangle>"
    "\<langle>c\<^sub>1', (mds',agrels'), mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>\<^sub>1',\<S>',P\<^sub>1'\<^esub> \<langle>c\<^sub>2', (mds',agrels'), mem\<^sub>2'\<rangle>"
    by (meson sub.prems(9))
  with R_equiv_entailment `P\<^sub>1' \<turnstile> P\<^sub>2'` show ?case
    using sub.hyps(6) sub.hyps(5) by blast
next
  case (lock_acq_type upd l \<Gamma>' \<Gamma> \<S> \<S>' P' P)
  hence upd: "((mds', agrels') = (mds, agrels) \<and> mem\<^sub>1' = mem\<^sub>1 \<and> c\<^sub>1' = (LockAcq l) \<and> ev\<^sub>L mem\<^sub>1 l) \<or> (c\<^sub>1' = Stop \<and>
    \<not> ev\<^sub>L mem\<^sub>1 l \<and> ev\<^sub>L mem\<^sub>1' l \<and> mem\<^sub>1' = mem\<^sub>1(Lock l := lock_acq_sem l (mem\<^sub>1(Lock l))) \<and>
    mds' = lock_acq_mds_upd l mds \<and> agrels' = lock_acq_agrels_upd l agrels)"
    by (auto dest: lock_acqD simp: update_env_def)+
  thus ?case
  proof
    assume noop: "(mds', agrels') = (mds, agrels) \<and> mem\<^sub>1' = mem\<^sub>1 \<and> c\<^sub>1' = LockAcq l \<and> ev\<^sub>L mem\<^sub>1 l"
    hence ty: " \<turnstile> \<Gamma>,\<S>,P {LockAcq l} \<Gamma>',\<S>',P'"
      apply simp
      apply(rule has_type.lock_acq_type)    
      using lock_acq_type apply simp+
      done

    from noop `mem\<^sub>1 =\<^bsub>\<Gamma>\<^esub> mem\<^sub>2`
    have "ev\<^sub>L mem\<^sub>2 l"
      unfolding tyenv_eq_def using eval_det\<^sub>L by metis
    hence "\<langle>LockAcq l, (mds, agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>LockAcq l, (mds', agrels'), mem\<^sub>2\<rangle>"
      using lock_wait noop by metis
    moreover have "\<langle>c\<^sub>1', (mds', agrels'), mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>LockAcq l, (mds', agrels'), mem\<^sub>2\<rangle>"
      apply(rule \<R>.intros)
      apply(simp add: noop)
      apply(rule \<R>\<^sub>1.intro)
           apply(rule ty) 
          using lock_acq_type apply simp+
      done  
    ultimately show "\<exists>c\<^sub>2' mem\<^sub>2'.
       \<langle>LockAcq l, (mds, agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', (mds', agrels'), mem\<^sub>2'\<rangle> \<and>
       \<langle>c\<^sub>1', (mds', agrels'), mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2', (mds', agrels'), mem\<^sub>2'\<rangle>"
      using noop by blast
  next
    assume " c\<^sub>1' = Stop \<and>
      \<not> ev\<^sub>L mem\<^sub>1 l \<and>
      ev\<^sub>L mem\<^sub>1' l \<and>
      mem\<^sub>1' = mem\<^sub>1(Lock l := lock_acq_sem l (mem\<^sub>1(Lock l))) \<and> mds' = lock_acq_mds_upd l mds \<and> agrels' = lock_acq_agrels_upd l agrels"
    hence upd: "c\<^sub>1' = Stop"
      "\<not> ev\<^sub>L mem\<^sub>1 l" "ev\<^sub>L mem\<^sub>1' l" "mem\<^sub>1' = mem\<^sub>1(Lock l := lock_acq_sem l (mem\<^sub>1(Lock l)))"
      "mds' = lock_acq_mds_upd l mds" "agrels' = lock_acq_agrels_upd l agrels" by blast+
    hence "mem\<^sub>1 =\<^bsub>\<Gamma>'\<^esub> mem\<^sub>2"
    proof(clarsimp simp: tyenv_eq_def,safe)
      fix x  
      assume a: "type_max (to_total \<Gamma>' x) (to_prog_mem mem\<^sub>1) = Low"
      have "to_total \<Gamma>' x = to_total \<Gamma> x"
        by(auto simp: to_total_def ` \<Gamma>' = \<Gamma> \<oplus>\<^sub>\<S> upd` add_anno_def `upd = Acq l` add_anno_dom_def)
      hence leq: "type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) \<le> type_max (to_total \<Gamma>' x) (to_prog_mem mem\<^sub>1)"
        by simp
      hence "type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) = Low"
      proof -
        have pred: "global_invariant mem\<^sub>1 \<and> pred P (to_prog_mem mem\<^sub>1) \<longrightarrow> pred P' (to_prog_mem mem\<^sub>1)"
          apply(clarsimp simp: pred_def lock_acq_type(4))
          using upd global_invariant_def by blast
        with subtype_correct lock_acq_type.hyps a `global_invariant mem\<^sub>1` 
        show ?thesis
          using less_eq_Sec_def leq
          by (metis lock_acq_type.prems(3))
      qed
      thus "mem\<^sub>1 (ProgVar x) = mem\<^sub>2 (ProgVar x)"
        using lock_acq_type.prems(2)
        unfolding tyenv_eq_def by blast
    next
      fix l'
      from `tyenv_eq \<Gamma> mem\<^sub>1 mem\<^sub>2` 
      show "mem\<^sub>1 (Lock l') = mem\<^sub>2 (Lock l')"
        unfolding tyenv_eq_def by blast
    qed

    from upd `tyenv_eq \<Gamma> mem\<^sub>1 mem\<^sub>2` have upd\<^sub>2: "\<not> ev\<^sub>L mem\<^sub>2 l"
      unfolding tyenv_eq_def using eval_det\<^sub>L
      by blast
    let ?mem\<^sub>2' = "mem\<^sub>2(Lock l := lock_acq_sem l (mem\<^sub>2 (Lock l)))"
    from upd\<^sub>2 upd have upd\<^sub>2: "\<not> ev\<^sub>L mem\<^sub>2 l \<and> ev\<^sub>L ?mem\<^sub>2' l \<and> (\<forall>x. x \<noteq> Lock l \<longrightarrow> ?mem\<^sub>2' x = mem\<^sub>2 x)"
     using eval_det\<^sub>L locks_can_be_taken
     by (meson fun_upd_apply)

    moreover have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (update_env upd (mds,agrels)) \<Gamma>' \<S>' P'"
    proof
      assume a: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P"
      have "P = restrict_preds_to_vars P {v. stable \<S> v}"
        apply(subst restrict_preds_to_vars_noop)
         using `tyenv_wellformed (mds,agrels) \<Gamma> \<S> P`
         by(auto simp: tyenv_wellformed_def  env_consistent_def)
      hence "P' = restrict_preds_to_vars (PConj P (lock_inv l)) {v. stable \<S>' v}"
        apply -
        apply(simp add: `P' = PConj P (lock_inv l)`)
        apply(subst restrict_preds_to_vars_noop)
         apply(subst lock_acq_type(3))
         apply(subst lock_acq_type(1))
         apply(subst add_anno_stable_Acq)
         apply(drule sym, drule restrict_preds_to_vars_noopD)
         apply(fastforce simp: stable_def stable_NoW_def stable_NoRW_def)
        apply simp
        apply(rule sym, rule restrict_preds_to_vars_noop)
        apply(clarsimp simp: ` \<S>' = add_anno_stable \<S> upd` add_anno_stable_def `upd = Acq l` stable_def stable_NoW_def stable_NoRW_def)
        using lock_invs
        by (metis (no_types, hide_lams) Un_iff fst_conv prod_eq_iff snd_conv subsetCE)
      with a lone_lock_per_var show "tyenv_wellformed (update_env upd (mds,agrels)) \<Gamma>' \<S>' P'"
      using lock_acq_type
            tyenv_wellformed_env_update
      using tyenv_wellformed_preds_update anno_type_stable_trivial
      by (metis prod.exhaust restrict_preds_to_vars_restricts)
    qed

    moreover have "global_invariant mem\<^sub>1 \<and> pred P (to_prog_mem mem\<^sub>1) \<longrightarrow> pred P' (to_prog_mem mem\<^sub>1')"
    proof -
      have pred: "global_invariant mem\<^sub>1 \<and> pred P (to_prog_mem mem\<^sub>1) \<longrightarrow> pred P' (to_prog_mem mem\<^sub>1)"
        apply(clarsimp simp: pred_def lock_acq_type(4))
        using upd global_invariant_def by blast

      moreover have "to_prog_mem mem\<^sub>1' = to_prog_mem mem\<^sub>1"
        apply(rule ext)
        apply(auto simp: to_prog_mem_def upd(4))
        done

      ultimately show "global_invariant mem\<^sub>1 \<and> pred P (to_prog_mem mem\<^sub>1) \<longrightarrow> pred P' (to_prog_mem mem\<^sub>1')"
        by auto
    qed

    moreover have "global_invariant mem\<^sub>2 \<and> pred P (to_prog_mem mem\<^sub>2) \<longrightarrow> pred P' (to_prog_mem ?mem\<^sub>2')"
    proof -
      have pred: "global_invariant mem\<^sub>2 \<and> pred P (to_prog_mem mem\<^sub>2) \<longrightarrow> pred P' (to_prog_mem mem\<^sub>2)"
        apply(clarsimp simp: pred_def lock_acq_type(4))
        using upd\<^sub>2 global_invariant_def by blast

      moreover have "to_prog_mem ?mem\<^sub>2' = to_prog_mem mem\<^sub>2"
        apply(rule ext)
        apply(auto simp: to_prog_mem_def upd(4))
        done

      ultimately show "global_invariant mem\<^sub>2 \<and> pred P (to_prog_mem mem\<^sub>2) \<longrightarrow> pred P' (to_prog_mem ?mem\<^sub>2')"
        by auto
    qed

    moreover
    have "global_invariant mem\<^sub>1 \<and> tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<and> pred P (to_prog_mem mem\<^sub>1) \<and> tyenv_sec mds \<Gamma> mem\<^sub>1 \<longrightarrow> tyenv_sec (fst (update_env upd (mds,agrels))) \<Gamma>' mem\<^sub>1'"
    proof -

      have "global_invariant mem\<^sub>1 \<and> tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<and> pred P (to_prog_mem mem\<^sub>1) \<and> tyenv_sec mds \<Gamma> mem\<^sub>1 \<longrightarrow> tyenv_sec (fst (update_env upd (mds,agrels))) \<Gamma>' mem\<^sub>1"
        apply(rule impI)
        apply(simp add: update_env_def `upd = Acq l`)
        apply(rule tyenv_sec_lock_acq)
             unfolding tyenv_wellformed_def env_consistent_def apply force
            using lock_acq_type apply fastforce
           using lock_acq_type(3) apply blast
          using lock_acq_type apply(fastforce simp: tyenv_wellformed_def env_consistent_def)
         apply simp
      using lock_acq_type by simp
      thus ?thesis
        using upd
        by (metis (mono_tags, lifting) fun_upd_other Var.simps(4) dma_\<C> no_locks_in_\<C> to_prog_mem_def vars_of_type_eq_type_max_eq)
    qed

    moreover from `tyenv_eq \<Gamma>' mem\<^sub>1 mem\<^sub>2` have "mem\<^sub>1' =\<^bsub>\<Gamma>'\<^esub> mem\<^sub>2(Lock l := mem\<^sub>1' (Lock l))"
      apply(clarsimp simp: tyenv_eq_def, safe)
       using upd(4) tyenv_eq_def 
       apply (metis (mono_tags, hide_lams) fun_upd_other Var.simps(4) to_prog_mem_def vars_of_type_eq_type_max_eq) 
      using upd(4) by simp

    
    moreover have "mem\<^sub>2 (Lock l) = mem\<^sub>1 (Lock l)"
      using `tyenv_eq \<Gamma> mem\<^sub>1 mem\<^sub>2` by(auto simp: tyenv_eq_def)
    hence "mem\<^sub>1' (Lock l) = ?mem\<^sub>2' (Lock l)"
      by (simp add: upd)

    moreover have ginv_pres: "global_invariant mem\<^sub>1 \<longrightarrow> global_invariant mem\<^sub>1'"
      using upd
      apply(clarsimp simp: global_invariant_def)
      using lock_invs 
      by (metis (mono_tags, lifting) Var.inject(1) Var.simps(4) eval_det\<^sub>L lpred_eval_vars_det to_prog_mem_def fun_upd_other)
    moreover have ginv_pres\<^sub>2: "global_invariant mem\<^sub>2 \<longrightarrow> global_invariant ?mem\<^sub>2'"
      using upd\<^sub>2
      apply(clarsimp simp: global_invariant_def)
      using lock_invs 
      by (metis (mono_tags, lifting) Var.inject(1) Var.simps(4) eval_det\<^sub>L lpred_eval_vars_det to_prog_mem_def upd\<^sub>2)
    moreover have sc: "stable_consistent \<S> mem\<^sub>1 \<longrightarrow> stable_consistent \<S>' mem\<^sub>1'"
      using upd lock_acq_type(3)
      apply(clarsimp simp: stable_consistent_def)
      using add_anno_stable_Acq eval_det\<^sub>L
      by (metis Un_insert_right Var.inject(1) fun_upd_apply insert_iff lock_acq_type.hyps(1) mem_Collect_eq subsetCE sup_bot.right_neutral)
    moreover have sc\<^sub>2: "stable_consistent \<S> mem\<^sub>2 \<longrightarrow> stable_consistent \<S>' ?mem\<^sub>2'"
      using upd\<^sub>2 lock_acq_type(3)
      apply(clarsimp simp: stable_consistent_def)
      using add_anno_stable_Acq eval_det\<^sub>L
      by (metis Collect_mem_eq Collect_mono_iff Un_insert_right Var.inject(1) insert_iff lock_acq_type.hyps(1) upd\<^sub>2 sup_bot.right_neutral)

    ultimately obtain c\<^sub>2' mem\<^sub>2' where "(\<langle>LockAcq l, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', (mds',agrels'), mem\<^sub>2'\<rangle> \<and>
      \<langle>c\<^sub>1', (mds',agrels'), mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2', (mds',agrels'), mem\<^sub>2'\<rangle>)"
      apply (subgoal_tac "\<exists>mem\<^sub>2'. \<langle>LockAcq l, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>Stop, (mds',agrels'), mem\<^sub>2'\<rangle> \<and> \<langle>c\<^sub>1', (mds',agrels'), mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>Stop, (mds',agrels'), mem\<^sub>2'\<rangle>")
       apply clarsimp
      apply (rule_tac x="?mem\<^sub>2'" in exI)
      apply(rule conjI)
       using upd apply simp
       apply (rule lock_acq_eval\<^sub>w[where env="(mds,agrels)", simplified])
           using upd\<^sub>2 apply simp
          using upd\<^sub>2 apply simp
         using upd\<^sub>2 apply simp
        using upd(5) lock_acq_type(1) update_env_def apply clarsimp
       using upd(1) apply clarsimp
      using upd(1) apply clarsimp
      apply(rule \<R>.intro\<^sub>1)
      apply clarsimp
      apply(rule_tac \<Gamma>=\<Gamma>' and \<S>=\<S>' and P=P' in \<R>\<^sub>1.intro)
              apply(rule stop_type)
             using lock_acq_type
             using LockUpd.simps(5) \<open>c\<^sub>1' = Stop \<and> \<not> ev\<^sub>L mem\<^sub>1 l \<and> ev\<^sub>L mem\<^sub>1' l \<and> mem\<^sub>1' = mem\<^sub>1 (Lock l := lock_acq_sem l (mem\<^sub>1 (Lock l))) \<and> mds' = lock_acq_mds_upd l mds \<and> agrels' = lock_acq_agrels_upd l agrels\<close> fst_conv snd_conv update_env_def apply auto[1]    
            apply assumption
           using lock_acq_type.prems(3) lock_acq_type.prems(5) apply blast
          using lock_acq_type.prems(4) lock_acq_type.prems(6) apply blast
         using `global_invariant mem\<^sub>1` apply blast
        using `global_invariant mem\<^sub>2` apply blast
       using `stable_consistent \<S> mem\<^sub>1` apply blast
      apply clarsimp
      by (metis (no_types, lifting) LockUpd.simps(5) \<open>c\<^sub>1' = Stop \<and> \<not> ev\<^sub>L mem\<^sub>1 l \<and> ev\<^sub>L mem\<^sub>1' l \<and> mem\<^sub>1' = mem\<^sub>1 (Lock l := lock_acq_sem l (mem\<^sub>1 (Lock l))) \<and> mds' = lock_acq_mds_upd l mds \<and> agrels' = lock_acq_agrels_upd l agrels\<close> \<open>global_invariant mem\<^sub>1\<close> domI fst_conv lock_acq_type.hyps(1) lock_acq_type.prems(1) lock_acq_type.prems(3) lock_acq_type.prems(7) option.sel update_env_def)

    with upd
    show ?case
      by blast
  qed
next
  case (lock_rel_type upd l \<Gamma>' \<Gamma> \<S> \<S>' P P' mds agrels)
  hence upd: "c\<^sub>1' = Stop" 
    "\<not> ev\<^sub>L mem\<^sub>1' l" "mem\<^sub>1' = mem\<^sub>1 (Lock l := lock_rel_sem l (mem\<^sub>1 (Lock l)))"
    "mds' = lock_rel_mds_upd l mds" "agrels' = lock_rel_agrels_upd l agrels"
    by (fastforce dest: lock_relD simp: update_env_def)+

  let ?mem\<^sub>2' = "mem\<^sub>2(Lock l := lock_rel_sem l (mem\<^sub>2 (Lock l)))"


  have "mem\<^sub>1 =\<^bsub>\<Gamma>'\<^esub> mem\<^sub>2"
  proof(clarsimp simp: tyenv_eq_def, safe)
    fix x  
    assume a: "type_max (to_total \<Gamma>' x) (to_prog_mem mem\<^sub>1) = Low"
    have leq: "type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) \<le> type_max (to_total \<Gamma>' x) (to_prog_mem mem\<^sub>1)"
    proof(cases "x \<in> dom \<Gamma>")
      assume "x \<in> dom \<Gamma>"
      moreover from `pred P (to_prog_mem mem\<^sub>1)` have p: "pred P' (to_prog_mem mem\<^sub>1)"
        using lock_rel_type.hyps
        using pred_entailment_def restrict_preds_to_vars_weakens by blast
      ultimately show 
      "type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) \<le> type_max (to_total \<Gamma>' x) (to_prog_mem mem\<^sub>1)"
        apply -
        apply(rule subtype_correct[THEN iffD1, rule_format, OF _ p])
        using lock_rel_type(6) by (auto simp: to_total_def)
    next
      assume nin: "x \<notin> dom \<Gamma>"
      hence "x \<notin> dom \<Gamma>'"
        by(auto simp: `\<Gamma>' = \<Gamma> \<oplus>\<^sub>\<S> upd` `upd = Rel l` add_anno_def add_anno_dom_def)
      with nin  show ?thesis
        by(auto simp: type_max_def to_total_def)
    qed
    hence "type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) = Low"
    proof -
      from `pred P (to_prog_mem mem\<^sub>1)` have "pred P' (to_prog_mem mem\<^sub>1)"
        using lock_rel_type.hyps
        using pred_entailment_def restrict_preds_to_vars_weakens by blast
      with subtype_correct lock_rel_type.hyps a 
      show ?thesis
        using less_eq_Sec_def leq by metis 
    qed
    thus "mem\<^sub>1 (ProgVar x) = mem\<^sub>2 (ProgVar x)"
      using lock_rel_type.prems(2)
      unfolding tyenv_eq_def by blast
  next
    fix l
    from `tyenv_eq \<Gamma> mem\<^sub>1 mem\<^sub>2` show "mem\<^sub>1 (Lock l) = mem\<^sub>2 (Lock l)"
     unfolding tyenv_eq_def by blast
  qed
  moreover have [simp]: "to_prog_mem mem\<^sub>1' = to_prog_mem mem\<^sub>1" "\<And>x. mem\<^sub>1' (ProgVar x) = mem\<^sub>1 (ProgVar x)"
    apply -
    apply(rule ext, auto simp: to_prog_mem_def upd(3))
    done
  ultimately have eq': "mem\<^sub>1' =\<^bsub>\<Gamma>'\<^esub> mem\<^sub>2(Lock l := mem\<^sub>1' (Lock l))"
    apply(clarsimp simp: tyenv_eq_def)
    using upd(3) by auto
  moreover have "mem\<^sub>2(Lock l := mem\<^sub>1' (Lock l)) = ?mem\<^sub>2'"
    apply(rule ext, simp add: upd)
    using `tyenv_eq \<Gamma> mem\<^sub>1 mem\<^sub>2` unfolding tyenv_eq_def by auto


  moreover 
  have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<longrightarrow> tyenv_wellformed (update_env upd (mds,agrels)) \<Gamma>' \<S>' P'"
    using lock_rel_type
    using tyenv_wellformed_env_update[where upd=upd] add_anno_stable_def[where upd=upd]
    by (clarsimp split: LockUpd.splits)
  moreover
  have pred: "pred P (to_prog_mem mem\<^sub>1) \<longrightarrow> pred P' (to_prog_mem mem\<^sub>1')"
    using lock_rel_type
    apply clarsimp
    using pred_entailment_def restrict_preds_to_vars_weakens by blast
  moreover
  have pred\<^sub>2: "pred P (to_prog_mem mem\<^sub>2) \<longrightarrow> pred P' (to_prog_mem ?mem\<^sub>2')"
    using lock_rel_type
    apply clarsimp
    using upd
    apply clarsimp
    unfolding to_prog_mem_def
    by (metis (no_types, lifting) Var.simps(4) fun_upd_def lpred_eval_vars_det restrict_preds_to_vars_weakens sifum_types.pred_entailment_def sifum_types_axioms)
  moreover
  have "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P \<and> pred P (to_prog_mem mem\<^sub>1) \<and> tyenv_sec mds \<Gamma> mem\<^sub>1 \<longrightarrow> tyenv_sec (lock_rel_mds_upd l mds) \<Gamma>' mem\<^sub>1'"
    apply(rule impI)
    apply(rule tyenv_sec_lock_rel[where P=P, OF `\<forall>x\<in>dom \<Gamma>. the (\<Gamma> x) \<le>:\<^sub>P' to_total \<Gamma>' x`])
             using lock_rel_type pred apply fastforce
            apply(clarsimp simp: tyenv_wellformed_def env_consistent_def)
            apply blast
           using lock_rel_type apply blast
          apply(fastforce simp: tyenv_wellformed_def env_consistent_def)
         using lock_rel_type apply blast
        using lock_rel_type apply fastforce
       using lock_rel_type apply fastforce
      using lock_rel_type apply blast
     using upd apply blast
    using upd apply force
    done

  moreover have ginv_pres: "pred P (to_prog_mem mem\<^sub>1) \<and> global_invariant mem\<^sub>1 \<longrightarrow> global_invariant mem\<^sub>1'"
  proof(clarify)
    assume p: "pred P (to_prog_mem mem\<^sub>1)"
    assume ginv: "global_invariant mem\<^sub>1"
    from p have "pred (lock_inv l) (to_prog_mem mem\<^sub>1)"
      using `P \<turnstile> (lock_inv l)`
      using pred_entailment_def by auto
    hence "lpred_eval (to_prog_mem mem\<^sub>1')  (lock_inv l)"
      using upd pred_def
      by (simp add: Var.distinct(1) eval_vars_det\<^sub>B singletonI to_prog_mem_def)
    with ginv upd show "global_invariant mem\<^sub>1'"
      unfolding global_invariant_def
      by (metis (mono_tags, lifting) Var.inject(1) Var.simps(4) eval_det\<^sub>L lpred_eval_vars_det to_prog_mem_def fun_upd_other)      
  qed

  moreover have ginv_pres: "pred P (to_prog_mem mem\<^sub>2) \<and> global_invariant mem\<^sub>2 \<longrightarrow> global_invariant ?mem\<^sub>2'"
  proof(clarify)
    assume p: "pred P (to_prog_mem mem\<^sub>2)"
    assume ginv: "global_invariant mem\<^sub>2"
    from p have "pred (lock_inv l) (to_prog_mem mem\<^sub>2)"
      using `P \<turnstile> lock_inv l`
      using pred_entailment_def by auto
    hence "lpred_eval (to_prog_mem ?mem\<^sub>2')  (lock_inv l)"
      using upd pred_def
      by (simp add: Var.distinct(1) eval_vars_det\<^sub>B singletonI to_prog_mem_def)
    with ginv  show "global_invariant ?mem\<^sub>2'"
      unfolding global_invariant_def
      by (metis (mono_tags, lifting) Var.inject(1) Var.simps(4) eval_det\<^sub>L lpred_eval_vars_det fun_upd_other to_prog_mem_def)      
  qed

  moreover have sc: "stable_consistent \<S> mem\<^sub>1 \<longrightarrow> stable_consistent \<S>' mem\<^sub>1'"
      using upd lock_rel_type(3) lock_rel_type(1) 
      apply(clarsimp simp: stable_consistent_def)
      using add_anno_stable_Rel 
      apply clarsimp
      using eval_det\<^sub>L
      by (metis Var.inject(1) fun_upd_other mem_Collect_eq subsetCE)


  ultimately
  obtain c\<^sub>2' mem\<^sub>2' where "(\<langle>LockRel l, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', (mds',agrels'), mem\<^sub>2'\<rangle> \<and>
    \<langle>c\<^sub>1', (mds',agrels'), mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2', (mds',agrels'), mem\<^sub>2'\<rangle>)"
    apply (subgoal_tac "\<exists>mem\<^sub>2'. \<langle>LockRel l, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>Stop, (mds',agrels'), mem\<^sub>2'\<rangle> \<and> \<langle>c\<^sub>1', (mds',agrels'), mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>Stop, (mds',agrels'), mem\<^sub>2'\<rangle>")
     apply blast (* slow *)
    apply (rule_tac x="?mem\<^sub>2'" in exI)
    apply(rule conjI)
     using upd(5) apply simp
     apply(rule lock_rel_eval\<^sub>w[where env="(mds,agrels)", simplified])
      using upd(2) apply (metis eval_det\<^sub>L fun_upd_apply)
     using upd(4) lock_rel_type(1) update_env_def apply clarsimp
    using upd(2) apply clarsimp
    apply(rule \<R>.intro\<^sub>1)
    apply(clarsimp split: prod.splits simp: upd(1))
    apply(rule_tac \<Gamma>=\<Gamma>' and \<S>=\<S>' and P=P' in \<R>\<^sub>1.intro)
            apply(rule stop_type)
           using upd(4)
           apply (simp add: LockUpd.simps(6) fst_conv lock_rel_type.hyps(1) lock_rel_type.prems(1) snd_conv upd(5) update_env_def)
          apply blast
         apply (simp add: lock_rel_type.prems(3))
        apply(clarsimp simp:pred_def to_prog_mem_def)
        using lpred_eval_vars_det
        apply (metis (mono_tags, lifting) lock_rel_type.prems(4) to_prog_mem_def)
       using `global_invariant mem\<^sub>1` `pred P (to_prog_mem mem\<^sub>1)` apply blast
      using `global_invariant mem\<^sub>2` `pred P (to_prog_mem mem\<^sub>2)` apply blast
     using `stable_consistent \<S> mem\<^sub>1` apply blast
    apply clarsimp
    by (metis \<open>pred P (to_prog_mem mem\<^sub>1)\<close> domI lock_rel_type.prems(1) lock_rel_type.prems(7) option.sel upd(4))
  with upd
  show ?case by blast
qed

lemma is_final_\<R>\<^sub>u_is_final:
  "\<langle>c\<^sub>1, (mds,agrels), mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, (mds,agrels), mem\<^sub>2\<rangle> \<Longrightarrow> is_final c\<^sub>1 \<Longrightarrow> is_final c\<^sub>2"
  by (fastforce dest: bisim_simple_\<R>\<^sub>u)

lemma my_\<R>\<^sub>3_aux_induct [consumes 1, case_names intro\<^sub>1 intro\<^sub>3]: 
  "\<lbrakk>\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>;
   \<And>c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mem\<^sub>2 c \<Gamma>' \<S>' P'. 
     \<lbrakk>\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>1\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>; 
      \<turnstile> \<Gamma>,\<S>,P {c} \<Gamma>',\<S>',P'\<rbrakk> \<Longrightarrow> 
    Q (c\<^sub>1 ;; c) mds mem\<^sub>1 \<Gamma>' \<S>' P' (c\<^sub>2 ;; c) mds mem\<^sub>2;
   \<And>c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mem\<^sub>2 c \<Gamma>' \<S>' P'. 
     \<lbrakk>\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>; 
      Q c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mds mem\<^sub>2; 
      \<turnstile> \<Gamma>,\<S>,P {c} \<Gamma>',\<S>',P'\<rbrakk> \<Longrightarrow> 
    Q (c\<^sub>1 ;; c) mds mem\<^sub>1 \<Gamma>' \<S>' P' (c\<^sub>2 ;; c) mds mem\<^sub>2\<rbrakk> \<Longrightarrow> 
  Q c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mds mem\<^sub>2"
using \<R>\<^sub>3_aux.induct[where 
    ?x1.0 = "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>" and
    ?x2.0 = \<Gamma> and
    ?x3.0 = \<S> and
    ?x4.0 = P and
    ?x5.0 = "\<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>" and
    ?P = "\<lambda>ctx\<^sub>1 \<Gamma> \<S> P ctx\<^sub>2. Q (fst (fst ctx\<^sub>1)) (snd (fst ctx\<^sub>1)) (snd ctx\<^sub>1) \<Gamma> \<S> P (fst (fst ctx\<^sub>2)) (snd (fst ctx\<^sub>2)) (snd ctx\<^sub>2)"]
by force

(* We can now show that \<R>\<^sub>1 and \<R>\<^sub>3 are weak bisimulations of \<R>,: *)
lemma \<R>\<^sub>1_weak_bisim:
  "weak_bisim (\<R>\<^sub>1 \<Gamma>' \<S>' P') (\<R> \<Gamma>' \<S>' P')"
  unfolding weak_bisim_def
  apply clarsimp
  apply(erule \<R>\<^sub>1_elim)
  apply(rule \<R>_typed_step, auto)
  done


lemma \<R>_to_\<R>\<^sub>3: "\<lbrakk> \<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> ; \<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P' \<rbrakk> \<Longrightarrow>
  \<langle>c\<^sub>1 ;; c, mds, mem\<^sub>1\<rangle> \<R>\<^sup>3\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2 ;; c, mds, mem\<^sub>2\<rangle>"
  apply (erule \<R>_elim)
  by auto


lemma \<R>\<^sub>3_weak_bisim:
  "weak_bisim (\<R>\<^sub>3 \<Gamma>' \<S>' P') (\<R> \<Gamma>' \<S>' P')"
proof -
  {
    fix c\<^sub>1 mds mem\<^sub>1 c\<^sub>2 mem\<^sub>2 c\<^sub>1' mds' mem\<^sub>1'
    assume case3: "(\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle>) \<in> \<R>\<^sub>3 \<Gamma>' \<S>' P'"
    assume eval: "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle>"
    have "\<exists> c\<^sub>2' mem\<^sub>2'. \<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle> \<and> \<langle>c\<^sub>1', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2', mds', mem\<^sub>2'\<rangle>"
      using case3 eval
      apply simp
      
    proof (induct arbitrary: c\<^sub>1' rule: \<R>\<^sub>3_aux.induct)
      case (intro\<^sub>1 c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mem\<^sub>2 c \<Gamma>' \<S>' P')
      hence [simp]: "c\<^sub>2 = c\<^sub>1"
        by (metis (lifting) \<R>\<^sub>1_elim)
      thus ?case
      proof (cases "c\<^sub>1 = Stop")
        assume [simp]: "c\<^sub>1 = Stop"
        from intro\<^sub>1(1) show ?thesis
          apply (rule \<R>\<^sub>1_elim)
          apply simp
          apply (rule_tac x = c in exI)
          apply (rule_tac x = mem\<^sub>2 in exI)
          apply (rule conjI)
           apply (metis `c\<^sub>1 = Stop`  eval\<^sub>w_simplep.seq_stop eval\<^sub>wp.unannotated eval\<^sub>wp_eval\<^sub>w_eq intro\<^sub>1.prems seq_stopD)
          apply (rule \<R>.intro\<^sub>1, clarify)
          apply (subgoal_tac "c\<^sub>1' = c")
           apply simp
           apply (rule \<R>\<^sub>1.intro)
                   apply(rule intro\<^sub>1(2))
                  apply (metis (no_types, lifting) `c\<^sub>1 = Stop` intro\<^sub>1.prems seq_stopD stop_cxt tyenv_wellformed_add_pred)
                 using `c\<^sub>1 = Stop` intro\<^sub>1.prems seq_stopD stop_cxt context_equiv_tyenv_eq
                 apply (metis (no_types, lifting))
                using `c\<^sub>1 = Stop` intro\<^sub>1.prems pred_entailment_def seq_stopD stop_cxt apply blast
               using pred_entailment_def stop_cxt apply blast
              apply (metis (no_types, lifting) `c\<^sub>1 = Stop` context_equiv_def intro\<^sub>1.prems less_eq_Sec_def seq_stopD stop_cxt subtype_sound type_equiv_def)
             using intro\<^sub>1.prems seq_stopD apply auto[1]
            using \<open>c\<^sub>1 = Stop\<close> intro\<^sub>1.prems seq_stopD stop_cxt apply blast
           apply (metis (no_types, hide_lams) \<open>c\<^sub>1 = Stop\<close> context_equiv_tyenv_sec intro\<^sub>1.prems seq_stopD stop_cxt)
          using intro\<^sub>1.prems seq_stopD by auto         
      next
        assume "c\<^sub>1 \<noteq> Stop"
        from intro\<^sub>1
        obtain c\<^sub>1'' where "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1'', mds', mem\<^sub>1'\<rangle> \<and> c\<^sub>1' = (c\<^sub>1'' ;; c)"
          by (metis `c\<^sub>1 \<noteq> Stop` intro\<^sub>1.prems seqD)
        with intro\<^sub>1
        obtain c\<^sub>2'' mem\<^sub>2' where "\<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2'', mds', mem\<^sub>2'\<rangle>" "\<langle>c\<^sub>1'', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2'', mds', mem\<^sub>2'\<rangle>"
          using \<R>\<^sub>1_weak_bisim and weak_bisim_def
          by blast
        thus ?thesis
          using intro\<^sub>1(2) \<R>_to_\<R>\<^sub>3
          apply (rule_tac x = "c\<^sub>2'' ;; c" in exI)
          apply (rule_tac x = mem\<^sub>2' in exI)
          apply (rule conjI)
           apply (metis eval\<^sub>w.seq)
          apply auto
          apply (rule \<R>.intro\<^sub>3)
          
          by (simp add: \<R>_to_\<R>\<^sub>3 `\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1'', mds', mem\<^sub>1'\<rangle> \<and> c\<^sub>1' = c\<^sub>1'' ;; c`)
      qed
    next
      case (intro\<^sub>3 c\<^sub>1 mds mem\<^sub>1 \<Gamma> \<S> P c\<^sub>2 mem\<^sub>2 c \<Gamma>' \<S>' P')
      thus ?case
        apply (cases "c\<^sub>1 = Stop")
         apply blast
      proof -
        assume "c\<^sub>1 \<noteq> Stop"
        then obtain c\<^sub>1'' where "\<langle>c\<^sub>1, mds, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1'', mds', mem\<^sub>1'\<rangle>" "c\<^sub>1' = (c\<^sub>1'' ;; c)"
          by (metis intro\<^sub>3.prems seqD)
        then obtain c\<^sub>2'' mem\<^sub>2' where "\<langle>c\<^sub>2, mds, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2'', mds', mem\<^sub>2'\<rangle>" "\<langle>c\<^sub>1'', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2'', mds', mem\<^sub>2'\<rangle>"
          using intro\<^sub>3(2) by metis
        thus ?thesis
          apply (rule_tac x = "c\<^sub>2'' ;; c" in exI)
          apply (rule_tac x = mem\<^sub>2' in exI)
          apply (rule conjI)
           apply (metis eval\<^sub>w.seq)
          apply (erule \<R>_elim)
            apply simp_all
            apply (metis \<R>.intro\<^sub>3 \<R>_to_\<R>\<^sub>3 `\<langle>c\<^sub>1'', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2'', mds', mem\<^sub>2'\<rangle>` `c\<^sub>1' = c\<^sub>1'' ;; c` intro\<^sub>3(3))
          apply (metis (lifting) \<R>.intro\<^sub>3 \<R>_to_\<R>\<^sub>3 `\<langle>c\<^sub>1'', mds', mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>,\<S>,P\<^esub> \<langle>c\<^sub>2'', mds', mem\<^sub>2'\<rangle>` `c\<^sub>1' = c\<^sub>1'' ;; c` intro\<^sub>3(3))
          done
      qed
    qed
  }
  thus ?thesis
    unfolding weak_bisim_def
    by auto
qed

(* Hence \<R> is a bisimulation: *)
lemma \<R>_bisim: "strong_low_bisim_mm (\<R> \<Gamma>' \<S>' P')"
  unfolding strong_low_bisim_mm_def
proof (auto)
  from \<R>_sym show "sym (\<R> \<Gamma>' \<S>' P')" .
next
  from \<R>_closed_glob_consistent show "closed_glob_consistent (\<R> \<Gamma>' \<S>' P')" .
next
  fix c\<^sub>1 mds agrels mem\<^sub>1 c\<^sub>2 mem\<^sub>2
  assume "\<langle>c\<^sub>1, (mds,agrels), mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, (mds,agrels), mem\<^sub>2\<rangle>"
  thus "mem\<^sub>1 =\<^bsub>mds\<^esub>\<^sup>l mem\<^sub>2"
    apply (rule \<R>_elim)
    by (auto dest: \<R>\<^sub>1_mem_eq \<R>\<^sub>3_mem_eq)
next
  fix c\<^sub>1 mds agrels mem\<^sub>1 c\<^sub>2 mem\<^sub>2 c\<^sub>1' mds' agrels' mem\<^sub>1'
  assume eval: "\<langle>c\<^sub>1, (mds,agrels), mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', (mds',agrels'), mem\<^sub>1'\<rangle>"
  assume R: "\<langle>c\<^sub>1, (mds,agrels), mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2, (mds,agrels), mem\<^sub>2\<rangle>"
  from R show "\<exists> c\<^sub>2' mem\<^sub>2'. \<langle>c\<^sub>2, (mds,agrels), mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', (mds',agrels'), mem\<^sub>2'\<rangle> \<and>
                            \<langle>c\<^sub>1', (mds',agrels'), mem\<^sub>1'\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c\<^sub>2', (mds',agrels'), mem\<^sub>2'\<rangle>"
    apply (rule \<R>_elim)
      apply (insert \<R>\<^sub>1_weak_bisim \<R>\<^sub>3_weak_bisim eval weak_bisim_def)
      apply auto
    done
qed

lemma Typed_in_\<R>:
  assumes typeable: "\<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P'"
  assumes wf: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P"
  assumes mem_eq: "\<forall> x. type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) = Low \<longrightarrow> mem\<^sub>1 (ProgVar x) = mem\<^sub>2 (ProgVar x)"
  assumes lock_eq: "\<forall> l. mem\<^sub>1 (Lock l) = mem\<^sub>2 (Lock l)"
  assumes pred\<^sub>1: "pred P (to_prog_mem mem\<^sub>1)"
  assumes pred\<^sub>2: "pred P (to_prog_mem mem\<^sub>2)"
  assumes tyenv_sec: "tyenv_sec mds \<Gamma> mem\<^sub>1"
  assumes ginv\<^sub>1: "global_invariant mem\<^sub>1"
  assumes ginv\<^sub>2: "global_invariant mem\<^sub>2"
  assumes stabcon: "stable_consistent \<S> mem\<^sub>1"
  shows "\<langle>c, (mds,agrels), mem\<^sub>1\<rangle> \<R>\<^sup>u\<^bsub>\<Gamma>',\<S>',P'\<^esub> \<langle>c, (mds,agrels), mem\<^sub>2\<rangle>"
  apply (rule \<R>.intro\<^sub>1 [of \<Gamma>'])
  apply clarify
  apply (rule \<R>\<^sub>1.intro [of \<Gamma>])
          apply(rule typeable)
         apply(rule wf)
        using mem_eq lock_eq apply(fastforce simp: tyenv_eq_def)
       using assms apply simp_all
  done

(* We then prove the main soundness theorem using the fact that typeable
    configurations can be related using \<R>\<^sub>1 *)
theorem type_soundness:
  assumes well_typed: "\<turnstile> \<Gamma>,\<S>,P { c } \<Gamma>',\<S>',P'"
  assumes wf: "tyenv_wellformed (mds,agrels) \<Gamma> \<S> P"
  assumes mem_eq: "\<forall> x. type_max (to_total \<Gamma> x) (to_prog_mem mem\<^sub>1) = Low \<longrightarrow> mem\<^sub>1 (ProgVar x) = mem\<^sub>2 (ProgVar x)"
  assumes lock_eq: "\<forall> l. mem\<^sub>1 (Lock l) = mem\<^sub>2 (Lock l)"
  assumes pred\<^sub>1: "pred P (to_prog_mem mem\<^sub>1)"
  assumes pred\<^sub>2: "pred P (to_prog_mem mem\<^sub>2)"
  assumes tyenv_sec: "tyenv_sec mds \<Gamma> mem\<^sub>1"
  assumes ginv\<^sub>1: "global_invariant mem\<^sub>1"
  assumes ginv\<^sub>2: "global_invariant mem\<^sub>2"
  assumes stabcon: "stable_consistent \<S> mem\<^sub>1"
  shows "\<langle>c, (mds,agrels), mem\<^sub>1\<rangle> \<approx> \<langle>c, (mds,agrels), mem\<^sub>2\<rangle>"
  using \<R>_bisim Typed_in_\<R>
  by (metis assms mem_eq mm_equiv.simps well_typed)

definition
  mds\<^sub>0
where
  "mds\<^sub>0 \<equiv> \<lambda>m. case m of GuarNoWrite \<Rightarrow> (ProgVar ` stable_NoW (UNIV))
                      | GuarNoReadOrWrite \<Rightarrow> ProgVar ` stable_NoRW (UNIV)
                      | _ \<Rightarrow> {}"


(* The typing relation for lists of commands ("thread pools"). *)
inductive type_global :: "(('Var, 'Lock, 'AExp, 'BExp) Stmt \<times> (('Lock, 'Var) Var,'Val) Env) list \<Rightarrow> bool"
  ("\<turnstile> _" [120] 1000)
  where
  "\<lbrakk> list_all (\<lambda> (c,(m,a)). (\<exists> \<Gamma>' \<S>' P'. \<turnstile> (Map.empty),{},PTrue { c } \<Gamma>',\<S>',P') \<and> m = (mds\<^sub>0) \<and> a = agrels\<^sub>0) cs;
       \<forall> mem. INIT mem \<longrightarrow> sound_env_use (cs, mem) \<and> global_invariant mem
    \<rbrakk> \<Longrightarrow>
    type_global cs"


inductive_cases type_global_elim: "\<turnstile> cs"

lemma of_mds_tyenv_wellformed: "tyenv_wellformed (mds\<^sub>0,agrels\<^sub>0) Map.empty {} PTrue"
  apply(clarsimp simp: tyenv_wellformed_def  env_consistent_def stable_def
                   types_wellformed_def types_stable_def   
                   type_wellformed_def dma_\<C>_vars \<C>_def  \<C>_vars_correct
                   stable_NoW_def stable_NoRW_def mds\<^sub>0_def agrels\<^sub>0_def
             split: if_splits)
  apply auto
  done

lemma \<Gamma>_of_mds_tyenv_sec:
  "tyenv_sec (\<lambda>_. {}) Map.empty mem\<^sub>1"
  apply(auto)
  done

lemma type_max_pred_False [simp]:
  "type_max PFalse mem = High"
  apply(simp add: type_max_def)
  done

definition
  env\<^sub>0
where
  "env\<^sub>0 \<equiv> (mds\<^sub>0, agrels\<^sub>0)"
  
lemma typed_secure:
  "\<lbrakk> \<turnstile> Map.empty,{},PTrue { c } \<Gamma>',\<S>',P'; (\<And>mem. INIT mem \<Longrightarrow> global_invariant mem) \<rbrakk> \<Longrightarrow> com_sifum_secure (c,env\<^sub>0)"
  apply (clarsimp simp: com_sifum_secure_def low_indistinguishable_def)
  apply (simp add: env\<^sub>0_def)
  apply (erule type_soundness)
          apply(rule of_mds_tyenv_wellformed)
         apply(fastforce simp: to_total_def type_max_dma_type low_mds_eq_def mds\<^sub>0_def)
        using locks_are_Low apply(fastforce simp: low_mds_eq_def mds\<^sub>0_def)
       apply(fastforce simp: pred_def)
      apply(fastforce simp: pred_def)
     apply(fastforce)
    apply blast
   apply blast
  apply(fastforce simp: stable_consistent_def)
  done

lemma list_all_set: "\<forall> x \<in> set xs. P x \<Longrightarrow> list_all P xs"
  by (metis (lifting) list_all_iff)

theorem type_soundness_global:
  assumes typeable: "\<turnstile> cs"
  shows "prog_sifum_secure_cont cs"
  using typeable
  apply (rule type_global_elim)
  apply (subgoal_tac "\<forall> c \<in> set cs. com_sifum_secure c")
   apply(rule sifum_compositionality_cont)
    using list_all_set apply fastforce
   apply fastforce
  apply(drule list_all_iff[THEN iffD1])
  apply clarsimp
  apply(rename_tac c m a)
  apply(drule_tac x="(c,m,a)" in bspec)
   apply assumption
  apply clarsimp
  apply(rule typed_secure[simplified env\<^sub>0_def])
   apply assumption
  apply blast
  done

end
end
