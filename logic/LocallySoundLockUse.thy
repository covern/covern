(*
Author: Toby Murray
*)

section {* A Method for Establishing Locally Sound Use of Modes with Locking Primitives *}

theory LocallySoundLockUse
imports Main LockTypeSystem
begin

context sifum_types begin


(* The following part contains some lemmas about evaluation of
   commands annotated using \<otimes> and characterisations of loc_reach for
   commands. *)

(* Note: The loc_reach lemmas here don't talk about memory. I *think* this is sufficient
   for now while we don't have commands for which the parts of memory that they access
   doesn't depend on memory. However if that changes, then in future we would want to
   have these loc_reach lemmas assume the program has been well typed first, i.e.
   assume \<Gamma>, \<S>, P {c} \<Gamma>', \<S>', P' and have loc_reach say that in mem' P still holds etc.
   and perhaps the program remains well-typed in mem' and therefore the induction still
   goes through. In fact we might need that anyway. We'll see.
*)
lemma stop_loc_reach:
  "\<lbrakk> \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>Stop, mds, mem\<rangle> \<rbrakk> \<Longrightarrow>
  c' = Stop \<and> mds' = mds"
  apply (induct rule: loc_reach.induct)
  by (auto dest: stop_no_eval)

lemma stop_doesnt_access:
  "doesnt_modify Stop mem x \<and> doesnt_read_or_modify Stop mem x"
  unfolding doesnt_modify_def and doesnt_read_or_modify_def doesnt_read_or_modify_vars_def
  using stop_no_eval
  by force

lemma skip_eval_step:
  "\<langle>Skip, mds, mem\<rangle> \<leadsto> \<langle>Stop, mds, mem\<rangle>"
  by (metis  eval\<^sub>w.unannotated eval\<^sub>w_simple.skip)

lemma skip_eval_elim:
  "\<lbrakk> \<langle>Skip, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle> \<rbrakk> \<Longrightarrow> c' = Stop \<and> mds' = mds \<and> mem' = mem"
  apply (rule ccontr)
  apply (insert skip_eval_step deterministic)
  by blast

lemma skip_doesnt_read:
  "doesnt_read_or_modify Skip mem x"
  apply (clarsimp simp: doesnt_read_or_modify_def doesnt_read_or_modify_vars_def)
  by (metis (mono_tags) skipD skip_eval_step subst_overrides)

lemma skip_doesnt_write:
  "doesnt_modify Skip mem x"
  apply (auto simp: doesnt_modify_def dest: skipD)
  done

lemma skip_loc_reach:
  "\<lbrakk> \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>Skip, mds, mem\<rangle> \<rbrakk> \<Longrightarrow>
  (c' = Stop \<and> mds' = mds) \<or> (c' = Skip \<and> mds' = mds)"
  apply (induct rule: loc_reach.induct)
    apply (metis fst_conv snd_conv)
   apply (metis skip_eval_elim stop_no_eval)
  by metis

lemma skip_doesnt_access:
  "\<lbrakk> lc \<in> loc_reach \<langle>Skip, mds, mem\<rangle> ; lc = \<langle>c', mds', mem'\<rangle> \<rbrakk> \<Longrightarrow> doesnt_read_or_modify c' mem x \<and> doesnt_modify c' mem x"
  apply (subgoal_tac "(c' = Stop \<and> mds' = mds) \<or> (c' = Skip \<and> mds' = mds)")
   apply (rule conjI, erule disjE)
     apply (force simp: doesnt_read_or_modify_def doesnt_read_or_modify_vars_def dest: stop_no_eval)
    apply (metis (lifting) skip_doesnt_read)
   apply (erule disjE)
    apply (force simp: doesnt_modify_def dest: stop_no_eval)
   apply (metis (lifting) skip_doesnt_write)
  by (metis skip_loc_reach)

definition
  progvars :: "('Lock,'Var) Var set \<Rightarrow> 'Var set"
where
  "progvars V \<equiv> {x. ProgVar x \<in> V}"

lemma doesnt_modify_vars_and_\<C>:
  " (\<forall> mds c' mds' mem'. (\<langle> c, mds, mem \<rangle> \<leadsto> \<langle> c', mds', mem' \<rangle>) \<longrightarrow>
                       (\<forall>x \<in> vars_and_\<C> V. 
                       mem x = mem' x)) \<Longrightarrow> doesnt_modify c mem V"
  apply(clarsimp simp: doesnt_modify_def vars_and_\<C>_def vars_\<C>_def)
  apply(rule dma_\<C>_vars)
  apply blast
  done

lemma assign_doesnt_modify:
  "\<lbrakk> ProgVar x \<notin> vars_and_\<C> V \<rbrakk> \<Longrightarrow> doesnt_modify (x \<leftarrow> e) mem V"
  apply (rule doesnt_modify_vars_and_\<C>)
  apply clarsimp
  apply(drule assignD)
  apply(auto)
  done

lemma assign_eval:
  "\<langle>x \<leftarrow> e, mds, mem\<rangle> \<leadsto> \<langle>Stop, mds, mem (ProgVar x := ev\<^sub>A (to_prog_mem mem) e)\<rangle>"
  apply (rule eval\<^sub>w.unannotated)
     apply (rule eval\<^sub>w_simple.assign)
    apply simp
   apply (metis prog_mem_assign_helper)
  unfolding to_lock_mem_def by simp

lemma assign_eval_elim:
  "\<lbrakk> \<langle>x \<leftarrow> e, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle> \<rbrakk> \<Longrightarrow>
  c' = Stop \<and> mds' = mds \<and> mem' = mem (ProgVar x := ev\<^sub>A (to_prog_mem mem) e) "
  apply (rule ccontr)
  apply (insert deterministic assign_eval)
  apply blast
  done

lemma subst_mem_upd_commute:
  "x \<notin> dom \<sigma> \<Longrightarrow> subst \<sigma> (mem(x := blah)) = (subst \<sigma> mem)(x := blah)"
  apply(rule ext)
  apply(simp add: subst_def)
  apply(auto split: option.splits)
  done

lemma assign_doesnt_read:
  "\<lbrakk> ProgVar x \<notin> vars_and_\<C> V; vars_and_\<C> V \<inter> vars_and_\<C> (ProgVar ` aexp_vars e)  = {} \<rbrakk> \<Longrightarrow> doesnt_read_or_modify (x \<leftarrow> e) mem V"
unfolding doesnt_read_or_modify_def doesnt_read_or_modify_vars_def
proof(clarsimp)
  fix mds agrels c' mds' agrels' mem' \<sigma>\<^sub>1 \<sigma>\<^sub>2
  assume a: "ProgVar x \<notin> vars_and_\<C> V"
         "vars_and_\<C> V \<inter> vars_and_\<C> (ProgVar ` aexp_vars e) = {}"
         "dom (\<sigma>\<^sub>1:: ('Lock, 'Var) Var \<Rightarrow> 'Val option) = vars_and_\<C> V"
         "dom (\<sigma>\<^sub>2:: ('Lock, 'Var) Var \<Rightarrow> 'Val option) = vars_and_\<C> V"
  assume step: "\<langle>x \<leftarrow> e, (mds, agrels), subst \<sigma>\<^sub>1 mem\<rangle> \<leadsto> \<langle>c', (mds', agrels'), mem'\<rangle>"
  from step have [simp]: "c' = Stop"  "mds' = mds" "agrels' = agrels" "mem' = (subst \<sigma>\<^sub>1 mem) (ProgVar x := ev\<^sub>A (to_prog_mem (subst \<sigma>\<^sub>1 mem)) e)"
    using assign_eval_elim by fastforce+

  have "\<langle>x \<leftarrow> e, (mds, agrels), subst \<sigma>\<^sub>2 mem\<rangle> \<leadsto> \<langle>Stop, (mds, agrels), (subst \<sigma>\<^sub>2 mem) (ProgVar x := ev\<^sub>A (to_prog_mem (subst \<sigma>\<^sub>2 mem)) e)\<rangle>"
    using assign_eval by blast

  moreover from a(2)
  have "vars_and_\<C> V \<inter> (ProgVar ` aexp_vars e) = {}"
    by(auto simp: vars_and_\<C>_def)
 
  with a(3) a(4)
  have "ev\<^sub>A (to_prog_mem (subst \<sigma>\<^sub>2 mem)) e = ev\<^sub>A (to_prog_mem (subst \<sigma>\<^sub>1 mem)) e"
    apply -
    apply(rule eval_vars_det\<^sub>A)
    apply(simp add: to_prog_mem_def)
    using subst_not_in_dom
    by (metis (full_types) disjoint_iff_not_equal image_eqI)


  hence "(subst \<sigma>\<^sub>2 mem) (ProgVar x := ev\<^sub>A (to_prog_mem (subst \<sigma>\<^sub>2 mem)) e) = subst \<sigma>\<^sub>2 mem'"
    apply simp
    using subst_mem_upd_commute a(4) a(1) 
    by (metis a(3) subst_overrides)

  ultimately show
    "\<langle>x \<leftarrow> e, (mds, agrels), subst \<sigma>\<^sub>2 mem\<rangle> \<leadsto> \<langle>c', (mds', agrels'), subst \<sigma>\<^sub>2 mem'\<rangle>"
    by simp
qed

lemma step_other_trans:
  "(\<forall>R\<in>snd (snd (fst lc)) AsmRel. trans R) \<Longrightarrow> step_other lc lc' \<Longrightarrow> step_other lc' lc'' \<Longrightarrow> step_other lc lc''"
  apply(fastforce elim!: step_other.cases intro!: step_other.intros elim: transE)
  done

lemma assign_loc_reach:
  "\<lbrakk> \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>x \<leftarrow> e, env, mem\<rangle>; (\<forall>R\<in>snd env AsmRel. trans R) \<rbrakk> \<Longrightarrow>
  (c' = Stop \<and> env' = env)  \<or> (c' = x \<leftarrow> e \<and> env' = env \<and> (mem' = mem \<or> step_other \<langle>c',env,mem\<rangle> \<langle>c',env,mem'\<rangle>))"
  apply (induct rule: loc_reach.induct)
    apply clarsimp
   apply clarsimp
   apply(erule disjE)
    apply(clarsimp dest!: stop_no_eval)
   apply (auto dest: assignD)[1]
  apply clarsimp
  apply(rename_tac mds agrels mem' mem'')
  apply(subgoal_tac "step_other \<langle>x \<leftarrow> e, (mds, agrels), mem'\<rangle> \<langle>x \<leftarrow> e, (mds, agrels), mem''\<rangle>")
   apply(erule disjE)
    apply clarsimp
   apply(fastforce dest: step_other_trans[rotated])
  using step_other.intros
  by (simp add: fst_conv snd_conv)
 
lemma lock_acq_loc_reach:
  "\<lbrakk> \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>LockAcq l, env, mem\<rangle>; \<forall>R\<in>snd env AsmRel. trans R \<rbrakk> \<Longrightarrow>
  (c' = Stop \<and> env' = (lock_acq_mds_upd l (fst env),lock_acq_agrels_upd l (snd env))) \<or> (c' = LockAcq l \<and> env' = env \<and> (mem' = mem \<or> step_other \<langle>c',env,mem\<rangle> \<langle>c',env,mem'\<rangle>))"
  apply (induct rule: loc_reach.induct)
    apply clarsimp
   apply clarsimp
   apply(erule disjE)
    apply(clarsimp dest!: stop_no_eval)
   apply (auto dest: lock_acqD)[1]
  apply clarsimp
  apply(rename_tac mds agrels mem' mem'')
  apply(subgoal_tac "step_other \<langle>LockAcq l, (mds, agrels), mem'\<rangle> \<langle>LockAcq l, (mds, agrels), mem''\<rangle>")
   apply(erule disjE)
    apply clarsimp
   apply(fastforce dest: step_other_trans[rotated])
  using step_other.intros
  by (simp add: fst_conv snd_conv)

lemma lock_acq_doesnt_modify:
  "Lock l \<notin> X \<Longrightarrow> doesnt_modify (LockAcq l) mem X"
  unfolding doesnt_modify_def
  apply clarsimp
  apply(drule lock_acqD)
  apply clarsimp
  using dma_\<C>_vars by auto

lemma eval\<^sub>L_neq_mem_neq: "\<not> ev\<^sub>L mem l \<Longrightarrow> ev\<^sub>L mem' l \<Longrightarrow> mem (Lock l) \<noteq> mem' (Lock l)"
  using eval_det\<^sub>L
  by blast

lemma lock_acqE[elim]:
  "\<langle>LockAcq l, env, mem\<rangle> \<leadsto> \<langle>c', env', mem'\<rangle> \<Longrightarrow>
   (env' = env \<Longrightarrow> mem' = mem  \<Longrightarrow> c' = LockAcq l \<Longrightarrow> ev\<^sub>L mem l \<Longrightarrow> R) \<Longrightarrow>
   (c' = Stop \<Longrightarrow> \<not> ev\<^sub>L mem l \<Longrightarrow> ev\<^sub>L mem' l \<Longrightarrow>
    mem' = mem(Lock l := lock_acq_sem l (mem (Lock l))) \<Longrightarrow> env' = (lock_acq_mds_upd l (fst env), lock_acq_agrels_upd l (snd env)) \<Longrightarrow> R)
   \<Longrightarrow> R"
  apply(drule lock_acqD, auto)
  done

lemma lock_acq_doesnt_read_progvars:
  "Lock l \<notin> X \<Longrightarrow> doesnt_read_or_modify (LockAcq l) mem X"
unfolding doesnt_read_or_modify_def doesnt_read_or_modify_vars_def
proof(clarsimp)
  assume X: "Lock l \<notin> X"
  fix mds agrels c' mds' agrels' mem' \<sigma>\<^sub>1 \<sigma>\<^sub>2
  assume a:
         "dom (\<sigma>\<^sub>1:: ('Lock, 'Var) Var \<Rightarrow> 'Val option) = vars_and_\<C> X"
         "dom (\<sigma>\<^sub>2:: ('Lock, 'Var) Var \<Rightarrow> 'Val option) = vars_and_\<C> X"
  assume step: "\<langle>LockAcq l, (mds, agrels), subst \<sigma>\<^sub>1 mem\<rangle> \<leadsto> \<langle>c', (mds', agrels'), mem'\<rangle>"

  have l: "Lock l \<notin> vars_and_\<C> X"
    by (fastforce simp: X vars_and_\<C>_def image_def vars_\<C>_def)

  from step show "\<langle>LockAcq l, (mds, agrels), subst \<sigma>\<^sub>2 mem\<rangle> \<leadsto> \<langle>c', (mds', agrels'), subst \<sigma>\<^sub>2 mem'\<rangle>"
  proof(rule lock_acqE)
    (* case where we wait for the lock *)
    assume [simp]: "(mds', agrels') = (mds, agrels)"
                   "mem' = subst \<sigma>\<^sub>1 mem"
                   "c' = LockAcq l" and
           lock_held: "ev\<^sub>L (subst \<sigma>\<^sub>1 mem) l"
    thus ?thesis
      apply simp
      apply(subst subst_twice)
       using a apply fastforce
      apply(rule lock_wait[simplified])
        using a l eval_det\<^sub>L subst_not_in_dom apply metis
       by simp_all
  next
    (* case where lock is acquired *)
    assume a' [simp]: "c' = Stop"
                   "mem' = (subst \<sigma>\<^sub>1 mem)(Lock l := lock_acq_sem l (subst \<sigma>\<^sub>1 mem (Lock l)))"
                   "(mds', agrels') = (lock_acq_mds_upd l (fst (mds, agrels)), lock_acq_agrels_upd l (snd (mds, agrels)))"
    assume not_held: "\<not> ev\<^sub>L (subst \<sigma>\<^sub>1 mem) l"
    assume acquired: "ev\<^sub>L mem' l"
    from not_held l a have not_held\<^sub>2: "\<not> ev\<^sub>L (subst \<sigma>\<^sub>2 mem) l"
      using eval_det\<^sub>L subst_not_in_dom by metis
    let ?mem\<^sub>2' = "(subst \<sigma>\<^sub>2 mem) (Lock l := mem' (Lock l))"
    have ag: "agrels' = lock_acq_agrels_upd l (snd (mds, agrels))" 
      apply simp
      using a'(3)
      using snd_conv by fastforce
    have step\<^sub>2: "\<langle>LockAcq l, (mds, agrels), subst \<sigma>\<^sub>2 mem\<rangle> \<leadsto> \<langle>c', (mds', agrels'), ?mem\<^sub>2'\<rangle>"
      apply(subst ag, subst a'(1))
      apply(rule lock_acq[simplified])
          apply(rule not_held\<^sub>2)
         using eval_det\<^sub>L acquired 
         apply (metis a'(2) a(1) a(2) fun_upd_same l subst_not_in_dom)
        apply simp
       using a' by fastforce+
    have "?mem\<^sub>2' = subst \<sigma>\<^sub>2 mem'"
    proof(rule ext, rename_tac x, case_tac x)
      fix x l'
      assume x: "(x::('Lock,'Var) Var) = Lock l'" 
      have l': "l' \<noteq> l \<longrightarrow> mem' (Lock l') = subst \<sigma>\<^sub>1 mem (Lock l')"
        using a'(2)
        by simp
      with x show "((subst \<sigma>\<^sub>2 mem)(Lock l := mem' (Lock l))) x = (subst \<sigma>\<^sub>2 mem') x"
        apply(clarsimp, safe)
          using l subst_not_in_dom a
          apply (metis fun_upd_apply)
         using  a l  apply clarsimp
          apply(simp add: subst_not_in_dom)
         apply(clarsimp simp: subst_def split: option.splits)
         using a apply blast
         done
    next
      fix x y
      assume "(x::('Lock,'Var) Var) = ProgVar y"
      thus "((subst \<sigma>\<^sub>2 mem)(Lock l := mem' (Lock l))) x = (subst \<sigma>\<^sub>2 mem') x"
        apply(clarsimp)
        apply(case_tac "ProgVar y \<in> dom \<sigma>\<^sub>2")
         apply(fastforce simp: subst_def)
        apply(clarsimp simp: subst_not_in_dom a)
        done
    qed
    with step\<^sub>2 show ?thesis
      by simp
  qed
qed


lemma lock_rel_loc_reach:
  "\<lbrakk> \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>LockRel l, env, mem\<rangle>; \<forall>R\<in>snd env AsmRel. trans R \<rbrakk> \<Longrightarrow>
  (c' = Stop \<and>  (fst env') = lock_rel_mds_upd l (fst env) \<and>
   snd env' = lock_rel_agrels_upd l (snd env))
  \<or> (c' = LockRel l \<and> env' = env \<and> (mem' = mem \<or> step_other \<langle>c',env,mem\<rangle> \<langle>c',env,mem'\<rangle>))"
  apply (induct rule: loc_reach.induct)
    apply clarsimp
   apply clarsimp
   apply(erule disjE)
    apply(clarsimp dest!: stop_no_eval)
   apply (auto dest: lock_relD)[1]
  apply(rename_tac env agrels mem'')
  apply(subgoal_tac "step_other \<langle>LockRel l, env', mem'\<rangle> \<langle>LockRel l, env', mem''\<rangle>")
   apply clarsimp
   apply(fastforce dest: step_other_trans[rotated])
  using step_other.intros
  by (simp add: fst_conv snd_conv)

lemma lock_relE[elim]:
  "\<langle>LockRel l, env, mem\<rangle> \<leadsto> \<langle>c', env', mem'\<rangle> \<Longrightarrow>
   (c' = Stop \<Longrightarrow> \<not> ev\<^sub>L mem' l \<Longrightarrow> mem' = (mem(Lock l := lock_rel_sem l (mem (Lock l)))) \<Longrightarrow>
    env' = (lock_rel_mds_upd l (fst env), lock_rel_agrels_upd l (snd env)) \<Longrightarrow> R) \<Longrightarrow> R"
  using lock_relD by blast

lemma lock_rel_doesnt_modify_progvars:
  "Lock l \<notin> X \<Longrightarrow> doesnt_modify (LockRel l) mem X"
  unfolding doesnt_modify_def
  apply clarsimp
  apply(drule lock_relD)
  using dma_\<C>_vars by fastforce

lemma lock_rel_doesnt_read_progvars:
  "Lock l \<notin> X \<Longrightarrow> doesnt_read_or_modify (LockRel l) mem X"
unfolding doesnt_read_or_modify_def doesnt_read_or_modify_vars_def
proof(clarsimp)
  assume X: "Lock l \<notin> X"
  fix mds agrels c' mds' agrels' mem' \<sigma>\<^sub>1 \<sigma>\<^sub>2
  assume a:
         "dom (\<sigma>\<^sub>1:: ('Lock, 'Var) Var \<Rightarrow> 'Val option) = vars_and_\<C> X"
         "dom (\<sigma>\<^sub>2:: ('Lock, 'Var) Var \<Rightarrow> 'Val option) = vars_and_\<C> X"
  assume step: "\<langle>LockRel l, (mds, agrels), subst \<sigma>\<^sub>1 mem\<rangle> \<leadsto> \<langle>c', (mds', agrels'), mem'\<rangle>"

  have l: "Lock l \<notin> vars_and_\<C> X"
    by (fastforce simp: X vars_and_\<C>_def image_def vars_\<C>_def)

  from step show "\<langle>LockRel l, (mds, agrels), subst \<sigma>\<^sub>2 mem\<rangle> \<leadsto> \<langle>c', (mds', agrels'), subst \<sigma>\<^sub>2 mem'\<rangle>"
  proof(rule lock_relE)
    assume a' [simp]: "c' = Stop"
                   " mem' = (subst \<sigma>\<^sub>1 mem)(Lock l := lock_rel_sem l (subst \<sigma>\<^sub>1 mem (Lock l)))"
                   "(mds', agrels') = (lock_rel_mds_upd l (fst (mds, agrels)), lock_rel_agrels_upd l (snd (mds, agrels)))"
    assume released: "\<not> ev\<^sub>L mem' l"
    let ?mem\<^sub>2' = "(subst \<sigma>\<^sub>2 mem) (Lock l := mem' (Lock l))"
    have ag: "agrels' = lock_rel_agrels_upd l (snd (mds, agrels))" 
      apply simp
      using a'(3)
      using snd_conv by fastforce
    have step\<^sub>2: "\<langle>LockRel l, (mds, agrels), subst \<sigma>\<^sub>2 mem\<rangle> \<leadsto> \<langle>c', (mds', agrels'), ?mem\<^sub>2'\<rangle>"
      apply(subst ag, subst a'(1))
      apply(rule lock_rel[simplified])
         using released eval_det\<^sub>L
         apply (metis a'(2) a(1) a(2) fun_upd_def l subst_not_in_dom)
        apply simp
       using a' by fastforce+
    have "?mem\<^sub>2' = subst \<sigma>\<^sub>2 mem'"
    proof(rule ext, rename_tac x, case_tac x)
      fix x l'
      assume x: "(x::('Lock,'Var) Var) = Lock l'" 
      have l': "l' \<noteq> l \<longrightarrow> mem' (Lock l') = subst \<sigma>\<^sub>1 mem (Lock l')"
        using a'(2) by simp
      with x show "((subst \<sigma>\<^sub>2 mem)(Lock l := mem' (Lock l))) x = (subst \<sigma>\<^sub>2 mem') x"
        apply(clarsimp, safe)
         using l subst_not_in_dom a
         apply (metis fun_upd_def)
         using  a l  apply clarsimp
          apply(simp add: subst_not_in_dom)
         apply(clarsimp simp: subst_def split: option.splits)
         using a apply blast
         done
    next
      fix x y
      assume "(x::('Lock,'Var) Var) = ProgVar y"
      thus "((subst \<sigma>\<^sub>2 mem)(Lock l := mem' (Lock l))) x = (subst \<sigma>\<^sub>2 mem') x"
        apply(clarsimp)
        apply(case_tac "ProgVar y \<in> dom \<sigma>\<^sub>2")
         apply(fastforce simp: subst_def)
        apply(clarsimp simp: subst_not_in_dom a)
        done
    qed
    with step\<^sub>2 show ?thesis
      by simp
  qed
qed


lemma if_doesnt_modify:
  "doesnt_modify (If e c\<^sub>1 c\<^sub>2) mem X"
  by (auto simp: doesnt_modify_def)

lemma vars_eval\<^sub>B:
  "x \<notin> bexp_vars e \<Longrightarrow> ev\<^sub>B (to_prog_mem mem) e = ev\<^sub>B (to_prog_mem (mem(ProgVar x := v))) e"
  by (simp add: eval_vars_det\<^sub>B to_prog_mem_def)
  

lemma if_doesnt_read:
  "vars_and_\<C> V \<inter> vars_and_\<C> (ProgVar ` bexp_vars e)  = {} \<Longrightarrow> doesnt_read_or_modify (If e c\<^sub>1 c\<^sub>2) mem V"
unfolding doesnt_read_or_modify_def doesnt_read_or_modify_vars_def
proof(clarsimp)
  fix mds agrels c' mds' agrels' mem' \<sigma>\<^sub>1 \<sigma>\<^sub>2
  assume a: "vars_and_\<C> V \<inter> vars_and_\<C> (ProgVar ` bexp_vars e) = {}"
         "dom (\<sigma>\<^sub>1:: ('Lock, 'Var) Var \<Rightarrow> 'Val option) = vars_and_\<C> V"
         "dom (\<sigma>\<^sub>2:: ('Lock, 'Var) Var \<Rightarrow> 'Val option) = vars_and_\<C> V"
  assume step: "\<langle>If e c\<^sub>1 c\<^sub>2, (mds, agrels), subst \<sigma>\<^sub>1 mem\<rangle> \<leadsto> \<langle>c', (mds', agrels'), mem'\<rangle>"
  from step
  show "\<langle>If e c\<^sub>1 c\<^sub>2, (mds, agrels), subst \<sigma>\<^sub>2 mem\<rangle> \<leadsto> \<langle>c', (mds', agrels'), subst \<sigma>\<^sub>2 mem'\<rangle>"
  proof (rule ifD)
    assume a' [simp]: "c' = c\<^sub>1"
                     "mem' = subst \<sigma>\<^sub>1 mem"
                     "(mds', agrels') = (mds, agrels)"
      and t: "ev\<^sub>B (to_prog_mem (subst \<sigma>\<^sub>1 mem)) e"

    have "ev\<^sub>B (to_prog_mem (subst \<sigma>\<^sub>2 mem)) e = ev\<^sub>B (to_prog_mem (subst \<sigma>\<^sub>1 mem)) e"
      apply -
      apply(rule eval_vars_det\<^sub>B)
    apply(simp add: to_prog_mem_def)
    using subst_not_in_dom a
    by (metis UnCI disjoint_iff_not_equal image_eqI vars_and_\<C>_def) (* slowish *)
    
    with t have "ev\<^sub>B (to_prog_mem (subst \<sigma>\<^sub>2 mem)) e" by simp
 
    moreover have "subst \<sigma>\<^sub>2 mem' = subst \<sigma>\<^sub>2 mem"
      by(simp add: a subst_twice)

    ultimately show ?thesis
      using if_eval\<^sub>w 
      using a'(1) a'(3) by presburger
  next
    assume a' [simp]: "c' = c\<^sub>2"
                     "mem' = subst \<sigma>\<^sub>1 mem"
                     "(mds', agrels') = (mds, agrels)"
      and t: "\<not> ev\<^sub>B (to_prog_mem (subst \<sigma>\<^sub>1 mem)) e"

    have "ev\<^sub>B (to_prog_mem (subst \<sigma>\<^sub>2 mem)) e = ev\<^sub>B (to_prog_mem (subst \<sigma>\<^sub>1 mem)) e"
      apply -
      apply(rule eval_vars_det\<^sub>B)
    apply(simp add: to_prog_mem_def)
    using subst_not_in_dom a
    by (metis UnCI disjoint_iff_not_equal image_eqI vars_and_\<C>_def) (* slowish *)
    
    with t have "\<not> ev\<^sub>B (to_prog_mem (subst \<sigma>\<^sub>2 mem)) e" by simp
 
    moreover have "subst \<sigma>\<^sub>2 mem' = subst \<sigma>\<^sub>2 mem"
      by(simp add: a subst_twice)

    ultimately show ?thesis
      using if_eval\<^sub>w 
      using a'(1) a'(3) by presburger
  qed
qed
  
lemma if_eval_true:
  "\<lbrakk> ev\<^sub>B (to_prog_mem mem) e \<rbrakk> \<Longrightarrow>
  \<langle>If e c\<^sub>1 c\<^sub>2, mds, mem\<rangle> \<leadsto> \<langle>c\<^sub>1, mds, mem\<rangle>"
  by (metis  eval\<^sub>w.unannotated eval\<^sub>w_simple.if_true)

lemma if_eval_false:
  "\<lbrakk> \<not> ev\<^sub>B (to_prog_mem mem) e \<rbrakk> \<Longrightarrow>
  \<langle>If e c\<^sub>1 c\<^sub>2, mds, mem\<rangle> \<leadsto> \<langle>c\<^sub>2, mds, mem\<rangle>"
  by (metis eval\<^sub>w.unannotated eval\<^sub>w_simple.if_false)

lemma if_eval_elim:
  "\<lbrakk> \<langle>If e c\<^sub>1 c\<^sub>2, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle> \<rbrakk> \<Longrightarrow>
  ((c' = c\<^sub>1 \<and> ev\<^sub>B (to_prog_mem mem) e) \<or> (c' = c\<^sub>2 \<and> \<not> ev\<^sub>B (to_prog_mem mem) e)) \<and> mds' = mds \<and> mem' = mem"
  apply (rule ccontr)
  apply (cases "ev\<^sub>B (to_prog_mem mem) e")
   apply (insert if_eval_true deterministic)
   apply blast
  using if_eval_false deterministic
  by blast

lemma if_eval_elim':
  "\<lbrakk> \<langle>If e c\<^sub>1 c\<^sub>2, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle> \<rbrakk> \<Longrightarrow>
  ((c' = c\<^sub>1 \<and> ev\<^sub>B (to_prog_mem mem) e) \<or> (c' = c\<^sub>2 \<and> \<not> ev\<^sub>B (to_prog_mem mem) e)) \<and> mds' = mds \<and> mem' = mem"
  using if_eval_elim
  by auto

lemma loc_reach_refl':
  "\<langle>c, mds, mem\<rangle> \<in> loc_reach \<langle>c, mds, mem\<rangle>"
  apply (subgoal_tac "\<exists> lc. lc \<in> loc_reach lc \<and> lc = \<langle>c, mds, mem\<rangle>")
   apply blast
  by (metis loc_reach.refl fst_conv snd_conv)

lemma step_other_com_irrelevant:
  "step_other \<langle>c, env, mem\<rangle> \<langle>c, env, mem'\<rangle> \<Longrightarrow>
   step_other \<langle>c', env, mem\<rangle> \<langle>c', env, mem'\<rangle>"
  apply(erule step_other.cases)
  apply(auto intro: step_other.intros)
  done

lemma step_other_loc_reach:
  "step_other lc lc' \<Longrightarrow> lc' \<in> loc_reach lc"
  apply(erule step_other.cases)
  apply clarsimp
  apply(rule loc_reach.mem_diff)
    using loc_reach_refl' apply blast
   by auto
 

lemma if_loc_reach:
  "\<lbrakk> \<langle>c', env', mem'\<rangle> \<in> loc_reach lc ; lc = \<langle>If e c\<^sub>1 c\<^sub>2, env, mem\<rangle>; (\<forall>R\<in>snd (snd (fst lc)) AsmRel. trans R) \<rbrakk> \<Longrightarrow>
  (c' = If e c\<^sub>1 c\<^sub>2 \<and> env' = env \<and> (mem' = mem \<or> step_other lc \<langle>c', env', mem'\<rangle>)) \<or>
  (\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env,mem\<rangle> \<and> ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow> ev\<^sub>B (to_prog_mem mem) e)) \<or>
  (\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, env, mem\<rangle>  \<and> ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow> \<not> (ev\<^sub>B (to_prog_mem mem)) e))" 
proof (induct arbitrary: e c\<^sub>1 c\<^sub>2 env mem rule: loc_reach.induct)
  case (refl)
  thus ?case by auto
next
  case (step c' env' mem' c'' env'' mem'')
  from step(2)[OF step (4) step(5)] have
    "c' = Stmt.If e c\<^sub>1 c\<^sub>2 \<and> env' = env \<and> (mem' = mem \<or> step_other lc \<langle>c', env', mem'\<rangle>) \<or>
     \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env, mem\<rangle> \<and>
     ((\<forall>a\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar a)) \<longrightarrow> ev\<^sub>B (to_prog_mem mem) e)
     \<or> \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, env, mem\<rangle> \<and>
     ((\<forall>a\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar a)) \<longrightarrow> \<not> ev\<^sub>B (to_prog_mem mem) e)"
     by blast
  thus ?case
  proof
    assume a: "c' = Stmt.If e c\<^sub>1 c\<^sub>2 \<and> env' = env \<and> (mem' = mem \<or> step_other lc \<langle>c', env', mem'\<rangle>)"
    hence "mem' = mem \<or> step_other lc \<langle>c', env', mem'\<rangle>"
      by blast
    thus ?thesis
    proof
      assume "mem' = mem"
      with step(3) a
      show ?thesis
        apply -
        apply(rule disjI2)
        using if_eval_elim 
        using loc_reach_refl' 
        by metis
    next
      assume other: "step_other lc \<langle>c', env', mem'\<rangle>"
      hence uc: "((\<forall>a\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar a)) \<longrightarrow> (ev\<^sub>B (to_prog_mem mem) e) = ev\<^sub>B (to_prog_mem mem') e)"
        apply(rule step_other.cases)
        using eval_vars_det\<^sub>B step(4) a[THEN conjunct1] a[THEN conjunct2, THEN conjunct1]
        by (simp add: fst_conv snd_conv to_prog_mem_def)
      from other have other': "step_other \<langle>c\<^sub>1, env, mem\<rangle> \<langle>c\<^sub>1, env, mem'\<rangle> \<and> step_other \<langle>c\<^sub>2, env, mem\<rangle> \<langle>c\<^sub>2, env, mem'\<rangle>"
        using step_other_com_irrelevant
        using a step.prems by blast
      from a step(3) have
        "\<langle>If e c\<^sub>1 c\<^sub>2, env', mem'\<rangle> \<leadsto> \<langle>c'', env'', mem''\<rangle>" by blast
      thus ?case
      apply(rule ifD)
       using other' step_other_loc_reach 
       apply (simp add: a uc)
      using other' step_other_loc_reach 
      apply (simp add: a uc)
      done
    qed
  next
    assume "\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env, mem\<rangle> \<and>
    ((\<forall>a\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar a)) \<longrightarrow> ev\<^sub>B (to_prog_mem mem) e) \<or>
    \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, env, mem\<rangle> \<and>
    ((\<forall>a\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar a)) \<longrightarrow>
     \<not> ev\<^sub>B (to_prog_mem mem) e)"
    with step(3)
    show ?thesis
    apply -
    apply(rule disjI2) 
    using loc_reach.intros(2) by blast
  qed
next
  case (mem_diff c' env' mem' mem'')
  have other': "step_other  \<langle>c', env', mem'\<rangle>  \<langle>c', env', mem''\<rangle>"
    using mem_diff(4) mem_diff(3) using step_other.intros by metis
  from mem_diff(2)[OF mem_diff(5) mem_diff(6)] have a:
    "c' = Stmt.If e c\<^sub>1 c\<^sub>2 \<and> env' = env \<and> (mem' = mem \<or> step_other lc \<langle>c', env', mem'\<rangle>) \<or>
     \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env, mem\<rangle> \<and>
  ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow> ev\<^sub>B (to_prog_mem mem) e) \<or>
  \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, env, mem\<rangle> \<and>
  ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow> \<not> ev\<^sub>B (to_prog_mem mem) e) "
    by blast
  thus ?case
  proof
    assume " c' = Stmt.If e c\<^sub>1 c\<^sub>2 \<and> env' = env \<and> (mem' = mem \<or> step_other lc \<langle>c', env', mem'\<rangle>)"
    hence "c' = Stmt.If e c\<^sub>1 c\<^sub>2 \<and> env' = env \<and> (mem'' = mem \<or> step_other lc \<langle>c', env', mem''\<rangle>)"
      using other' step_other_trans mem_diff(6)
      using mem_diff.prems(1) by blast
    thus ?case by blast
  next
    assume "\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env, mem\<rangle> \<and>
    ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow> ev\<^sub>B (to_prog_mem mem) e) \<or>
    \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, env, mem\<rangle> \<and>
    ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow>
     \<not> ev\<^sub>B (to_prog_mem mem) e)"
    thus ?case
      apply clarsimp
      apply(erule disjE)
       apply clarsimp
       using mem_diff(3,4) loc_reach.mem_diff
       apply presburger
      apply clarsimp
      using mem_diff(3,4) loc_reach.mem_diff
      by simp
  qed
qed

lemma if_loc_reach':
  "\<lbrakk> \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>If e c\<^sub>1 c\<^sub>2, env, mem\<rangle>; (\<forall>R\<in>snd env AsmRel. trans R) \<rbrakk> \<Longrightarrow>
  (c' = If e c\<^sub>1 c\<^sub>2 \<and> env' = env \<and> (mem' = mem \<or> step_other \<langle>c', env', mem\<rangle> \<langle>c', env', mem'\<rangle>)) \<or>
  (\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env,mem\<rangle> \<and> ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow> ev\<^sub>B (to_prog_mem mem) e)) \<or>
  (\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, env, mem\<rangle>  \<and> ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow> \<not> (ev\<^sub>B (to_prog_mem mem)) e))" 
  using if_loc_reach
  by (metis fst_conv snd_conv) 

lemma seq_loc_reach:
  "\<lbrakk> \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1 ;; c\<^sub>2, mds, mem\<rangle> \<rbrakk> \<Longrightarrow>
  (\<exists> c''. c' = c'' ;; c\<^sub>2 \<and> \<langle>c'', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, mds, mem\<rangle>) \<or>
  (\<exists> c'' mds'' mem''. \<langle>Stop, mds'', mem''\<rangle> \<in> loc_reach \<langle>c\<^sub>1, mds, mem\<rangle> \<and> 
                      \<langle>c', mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, mds'', mem''\<rangle>)"
  apply (induct rule: loc_reach.induct)
    apply simp
    apply (metis  loc_reach_refl')
   using loc_reach.step loc_reach_refl' prod.collapse seqD seq_stopD
   apply blast
  apply(erule disjE)
   apply(metis (no_types, lifting) loc_reach.mem_diff)
  by (metis (no_types, lifting) loc_reach.mem_diff)

lemma seq_doesnt_read:
  "\<lbrakk> doesnt_read_or_modify c mem X \<rbrakk> \<Longrightarrow> doesnt_read_or_modify (c ;; c') mem X"
proof(clarsimp simp: doesnt_read_or_modify_def doesnt_read_or_modify_vars_def)
  fix mds agrels c'' mds' agrels' mem' \<sigma>\<^sub>1 \<sigma>\<^sub>2
  assume a [simp]: "dom \<sigma>\<^sub>1 = vars_and_\<C> X"
                   "dom (\<sigma>\<^sub>2::('Lock,'Var) Var \<Rightarrow> 'Val option) = vars_and_\<C> X" and
             step: "\<langle>c ;; c', (mds, agrels), subst \<sigma>\<^sub>1 mem\<rangle> \<leadsto> \<langle>c'', (mds', agrels'), mem'\<rangle>"
  assume c: "\<forall>a b c' aa ba mem' \<sigma>\<^sub>1 \<sigma>\<^sub>2.
          dom \<sigma>\<^sub>1 = vars_and_\<C> X \<and> dom \<sigma>\<^sub>2 = vars_and_\<C> X \<longrightarrow>
          \<langle>c, (a, b), subst \<sigma>\<^sub>1 mem\<rangle> \<leadsto> \<langle>c', (aa, ba), mem'\<rangle> \<longrightarrow>
          \<langle>c, (a, b), subst \<sigma>\<^sub>2 mem\<rangle> \<leadsto> \<langle>c', (aa, ba), subst \<sigma>\<^sub>2 mem'\<rangle>"
  show "\<langle>c ;; c', (mds, agrels), subst \<sigma>\<^sub>2 mem\<rangle> \<leadsto> \<langle>c'', (mds', agrels'), subst \<sigma>\<^sub>2 mem'\<rangle>"
  proof(case_tac "c = Stop")
    assume "c = Stop"
    show ?thesis using step seq_stopD
      by (metis (no_types, lifting) \<open>c = Stop\<close> a(1) a(2) seq_stop_eval\<^sub>w subst_overrides)
  next
    assume dontstop: "c \<noteq> Stop"
    with step seqD obtain c\<^sub>s where
      "\<langle>c, (mds,agrels), subst \<sigma>\<^sub>1 mem\<rangle> \<leadsto> \<langle>c\<^sub>s, (mds',agrels'), mem'\<rangle>" and c''_def: "c'' = c\<^sub>s ;; c'"
      by blast
    with c a have
      "\<langle>c, (mds, agrels), subst \<sigma>\<^sub>2 mem\<rangle> \<leadsto> \<langle>c\<^sub>s, (mds', agrels'), subst \<sigma>\<^sub>2 mem'\<rangle>"
      by blast
    thus ?thesis
      using dontstop seq c''_def by metis
  qed
qed
  
lemma seq_doesnt_modify:
  "\<lbrakk> doesnt_modify c mem X \<rbrakk> \<Longrightarrow> doesnt_modify (c ;; c') mem X"
  apply (clarsimp simp: doesnt_modify_def, safe)
   apply (case_tac "c = Stop")
    apply (clarsimp | safe)+
     apply (metis (lifting) seq_stopD)
    apply (metis (no_types) seqD)
  apply (case_tac "c = Stop")
  apply clarsimp
   apply (metis (lifting) seq_stopD)
  apply (metis (no_types) seqD)
  done
  
inductive_cases seq_stop_elim': "\<langle>Stop ;; c, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle>"

lemma seq_stop_elim: "\<langle>Stop ;; c, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle> \<Longrightarrow>
  c' = c \<and> mds' = mds \<and> mem' = mem"
  apply (erule seq_stop_elim')
   apply (metis eval\<^sub>w.unannotated seq_stopD)
  apply (metis eval\<^sub>w.seq seq_stopD)
  done

lemma seq_split:
  "\<lbrakk> \<langle>Stop, mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1 ;; c\<^sub>2, mds, mem\<rangle> \<rbrakk> \<Longrightarrow>
  \<exists> mds'' mem''. \<langle>Stop, mds'', mem''\<rangle> \<in> loc_reach \<langle>c\<^sub>1, mds, mem\<rangle> \<and>
                 \<langle>Stop, mds', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, mds'', mem''\<rangle>"
  apply (drule seq_loc_reach)
  using Stmt.simps
  by fast

lemma while_eval:
  "\<langle>While e c, mds, mem\<rangle> \<leadsto> \<langle>(If e (c ;; While e c) Stop), mds, mem\<rangle>"
  apply (rule_tac pmem="to_prog_mem mem" and pmem'="to_prog_mem mem" in eval\<^sub>w.unannotated)
     apply (metis (lifting) eval\<^sub>w_simple.while)
    apply simp+
  done

lemma while_eval':
  "\<langle>While e c, mds, mem\<rangle> \<leadsto> \<langle>If e (c ;; While e c) Stop, mds, mem\<rangle>"
  using while_eval
  by auto

lemma while_eval_elim:
  "\<lbrakk> \<langle>While e c, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle> \<rbrakk> \<Longrightarrow>
   (c' = If e (c ;; While e c) Stop \<and> mds' = mds \<and> mem' = mem)"
  apply (rule ccontr)
  apply (insert while_eval deterministic)
  by blast

lemma while_eval_elim':
  "\<lbrakk> \<langle>While e c, mds, mem\<rangle> \<leadsto> \<langle>c', mds', mem'\<rangle> \<rbrakk> \<Longrightarrow>
   (c' = If e (c ;; While e c) Stop \<and> mds' = mds \<and> mem' = mem)"
  using while_eval_elim
  by auto

lemma while_doesnt_read:
  "doesnt_read_or_modify (While e c) mem X"
  unfolding doesnt_read_or_modify_def doesnt_read_or_modify_vars_def
  using while_eval while_eval_elim
  by (metis (no_types, lifting) subst_overrides)

lemma while_doesnt_modify:
  "doesnt_modify (While e c) mem X"
  unfolding doesnt_modify_def
  using while_eval_elim
  by metis

inductive_cases step_otherE': "step_other \<langle>c, env, mem\<rangle> \<langle>c, env, mem'\<rangle>"

lemma stable_var_asm_not_written:
  "stable \<S> x \<Longrightarrow> env_consistent env \<Gamma> \<S> P \<Longrightarrow> var_asm_not_written (fst env) (ProgVar x)"
  apply(clarsimp simp: stable_def env_consistent_def stable_NoW_def stable_NoRW_def var_asm_not_written_def image_def)
  done

lemma step_other_preservation:
  "step_other \<langle>c, env, mem\<rangle> \<langle>c, env, mem'\<rangle> \<Longrightarrow>
    tyenv_wellformed env \<Gamma> \<S> P \<Longrightarrow>
    pred P (to_prog_mem mem) \<Longrightarrow>
    stable_consistent \<S> mem \<Longrightarrow> global_invariant mem \<Longrightarrow>
    pred P (to_prog_mem mem') \<and>
    stable_consistent \<S> mem' \<and> global_invariant mem'"
proof(induct rule: step_other.cases)
  case (mem_diff)
  show ?case
  proof(intro conjI)
    have P_stable: "type_stable \<S> P"
      using mem_diff(5) unfolding tyenv_wellformed_def env_consistent_def by blast
    have "\<And>x. x \<in> lpred_vars P \<Longrightarrow> var_asm_not_written (fst env) (ProgVar x)"
      apply(rule stable_var_asm_not_written)
       using P_stable apply fast
      using mem_diff(5) unfolding tyenv_wellformed_def by blast
    hence "lpred_eval (to_prog_mem mem) P = lpred_eval (to_prog_mem mem') P"
      using mem_diff(1) mem_diff(2) mem_diff(3) lpred_eval_vars_det
      by (metis (no_types, lifting) Pair_inject fst_conv mem_diff.prems(1) to_prog_mem_def)
    thus "pred P (to_prog_mem mem')"
      using mem_diff(6) unfolding pred_def by blast
  next
    have "\<And>l. l \<in> \<S> \<Longrightarrow> lock_no_release_rel l \<in> (snd env) AsmRel"
      using mem_diff(5) unfolding tyenv_wellformed_def env_consistent_def
      by clarsimp
    hence "\<And>l. l \<in> \<S> \<Longrightarrow> ev\<^sub>L mem l \<longrightarrow> ev\<^sub>L mem' l"
      using mem_diff(1) mem_diff(2) mem_diff(4) unfolding lock_no_release_rel_def
      using Product_Type.Collect_case_prodD fst_conv snd_conv by fastforce
    with `stable_consistent \<S> mem`
    show "stable_consistent \<S> mem'"
      unfolding stable_consistent_def by blast
  next
    from mem_diff(4) `global_invariant mem` mem_diff(1) mem_diff(2) mem_diff(5)
    show "global_invariant mem'"
      by(clarsimp simp: tyenv_wellformed_def env_consistent_def)
  qed
qed

lemma loc_reach_as_rtranclp':
  "lc' \<in> loc_reach lc = (\<lambda>x y. x \<leadsto> y \<or> step_other x y)\<^sup>*\<^sup>* lc lc'"
  using lc_expand loc_reach_as_rtranclp
  by (metis (mono_tags))

lemma step_other_local_unchanged:
  "step_other lc lc' \<Longrightarrow> fst lc = fst lc'"
  by(auto elim: step_other.cases)

text {*
   This states what we know @{term loc_reach} preserves. Note that it doesn't currently
   preserve @{term tyenv_sec} because @{term step_other} doesn't preserve this condition. 
   AFAICT, this happens becasue we don't currently enforce the condition
   that the control vars of all variables in @{term \<Gamma>} are also in @{term \<Gamma>} --- 
   in particular, after rewriting a type to remove its dependency on a control variable, 
   the control variable can be removed from @{term \<Gamma>} without violating the 
   wellformedness conditions.
*}
lemma loc_reach_preservation:
  "lc' \<in> loc_reach lc \<Longrightarrow> lc = \<langle>c, env, mem\<rangle> \<Longrightarrow> lc' = \<langle>c', env', mem'\<rangle> \<Longrightarrow>
    \<turnstile> \<Gamma>,\<S>,P {c} \<Gamma>',\<S>',P' \<Longrightarrow>
  (\<exists>\<Gamma>'' \<S>'' P''.
   \<turnstile> \<Gamma>'',\<S>'',P'' {c'} \<Gamma>',\<S>',P' \<and>
   (preservation_props env \<Gamma> \<S> P mem \<longrightarrow> preservation_props env' \<Gamma>'' \<S>'' P'' mem'))"
proof(subst (asm) loc_reach_as_rtranclp', induct arbitrary: c env mem c' env' mem' \<Gamma> \<S> P rule: converse_rtranclp_induct)
  case base
  thus ?case by blast
next
  case (step lc lc'')
  from step(1) show ?case
  proof
    assume steps: "lc \<leadsto> lc''"
    obtain c'' env'' mem'' where lc''_def: "lc'' = \<langle>c'', env'', mem''\<rangle>"
      using lc_expand surjective_pairing by metis
    from steps have steps: "\<langle>c, env, mem\<rangle> \<leadsto> \<langle>c'', env'', mem''\<rangle>"
      using step(4) lc''_def by blast
    from preservation[OF step(6)] steps
    obtain \<Gamma>'' \<S>'' P'' where
      typed'': "\<turnstile> \<Gamma>'',\<S>'',P'' {c''} \<Gamma>',\<S>',P'" and
      pres'': "(preservation_props env \<Gamma> \<S> P mem \<longrightarrow> preservation_props env'' \<Gamma>'' \<S>'' P'' mem'')"  
      by (metis (no_types, lifting) prod.collapse)
    with step(3)[OF lc''_def step(5) typed''] show ?case by blast
  next
    assume steps: "step_other lc lc''"
    obtain c'' env'' mem'' where lc''_def: "lc'' = \<langle>c'', env'', mem''\<rangle>"
      using lc_expand surjective_pairing by metis
    from steps have steps: "step_other \<langle>c, env, mem\<rangle> \<langle>c, env, mem''\<rangle>" and
                    lc''_def: "lc'' = \<langle>c, env, mem''\<rangle>"
      using lc''_def step(4) step_other_local_unchanged
      using fst_conv by fastforce+
    from step(3)[OF lc''_def step(5) step(6)] obtain \<Gamma>''' \<S>''' P'' where
    " \<turnstile> \<Gamma>''',\<S>''',P'' {c'} \<Gamma>',\<S>',P' \<and>
      (preservation_props env \<Gamma> \<S> P mem'' \<longrightarrow> preservation_props env' \<Gamma>''' \<S>''' P'' mem') "
      by blast
    from this show ?case
      using step_other_preservation[OF steps] by blast
  qed
qed

lemma stable_NoW_\<C>_vars\<^sub>V:
  "x \<in> stable_NoW \<S> \<Longrightarrow> (\<C>_vars\<^sub>V x \<subseteq> stable_NoW \<S>)"
  apply(clarsimp simp: stable_NoW_def \<C>_vars\<^sub>V_def | safe)+
  using lock_interp_\<C>_vars\<^sub>V apply force
  done

lemma stable_NoW_\<C>_vars\<^sub>V_reverse:
  "x \<in> stable_NoW \<S> \<Longrightarrow> ({y. x \<in> \<C>_vars\<^sub>V y} \<subseteq> stable_NoW \<S>)"
  apply(clarsimp simp: stable_NoW_def \<C>_vars\<^sub>V_def | safe)+
  using lock_interp_\<C>_vars\<^sub>V apply force
  done

lemma stable_NoRW_\<C>_vars\<^sub>V:
  "x \<in> stable_NoRW \<S> \<Longrightarrow> (\<C>_vars\<^sub>V x \<subseteq> stable_NoRW \<S>)"
  apply(clarsimp simp: stable_NoRW_def \<C>_vars\<^sub>V_def | safe)+
  using lock_interp_\<C>_vars\<^sub>V apply force
  done

lemma stable_NoRW_\<C>_vars\<^sub>V_reverse:
  "x \<in> stable_NoRW \<S> \<Longrightarrow> ({y. x \<in> \<C>_vars\<^sub>V y} \<subseteq> stable_NoRW \<S>)"
  apply(clarsimp simp: stable_NoRW_def \<C>_vars\<^sub>V_def | safe)+
  using lock_interp_\<C>_vars\<^sub>V apply force
  done

lemma vars_and_\<C>_mds_subset:
  "env_consistent env \<Gamma> \<S> P \<Longrightarrow> vars_and_\<C> ((fst env) m) \<subseteq> (fst env) m"
  using stable_NoW_\<C>_vars\<^sub>V stable_NoRW_\<C>_vars\<^sub>V \<C>_vars_def2
  apply(case_tac m; (clarsimp simp: env_consistent_def vars_and_\<C>_def vars_\<C>_def, meson imageI subsetCE))
  done

lemma \<C>_vars_ProgVar:
  "x \<in> \<C>_vars y \<Longrightarrow> \<exists>x' y'. x  = ProgVar x' \<and> y = ProgVar y'"
  apply(case_tac y)
   using \<C>_vars_correct(2) apply fastforce
  using \<C>_vars_correct(1) apply fastforce
  done

(* FIXME: rewrite this proof to at least use more automation *)
lemma vars_and_\<C>_mds_subset_reverse:
  "env_consistent env \<Gamma> \<S> P \<Longrightarrow> {y. (\<exists>x. x \<in> (fst env m) \<and> x \<in> \<C>_vars y)} \<subseteq> fst env m"
  apply(case_tac m; (clarsimp simp: env_consistent_def vars_and_\<C>_def vars_\<C>_def \<C>_vars_def2))
     apply(frule \<C>_vars_ProgVar, clarsimp)
     apply(drule stable_NoRW_\<C>_vars\<^sub>V_reverse[THEN subsetD])
      apply (fastforce simp: \<C>_vars\<^sub>V_def)
     apply blast
    apply(frule \<C>_vars_ProgVar, clarsimp)
    apply(drule stable_NoW_\<C>_vars\<^sub>V_reverse[THEN subsetD])
     apply (fastforce simp: \<C>_vars\<^sub>V_def)
    apply blast
   apply(frule \<C>_vars_ProgVar, clarsimp)
   apply(drule stable_NoRW_\<C>_vars\<^sub>V_reverse[THEN subsetD])
    apply (fastforce simp: \<C>_vars\<^sub>V_def)
   apply blast
  apply(frule \<C>_vars_ProgVar, clarsimp)
  apply(drule stable_NoW_\<C>_vars\<^sub>V_reverse[THEN subsetD])
   apply (fastforce simp: \<C>_vars\<^sub>V_def)
  apply blast
  done

lemma vars_and_\<C>_mds_sep:
  "env_consistent env \<Gamma> \<S> P \<Longrightarrow> (fst env) m \<inter> X = {} \<Longrightarrow> vars_and_\<C> (fst env m) \<inter> vars_and_\<C> X = {}"
proof -
  assume con: "env_consistent env \<Gamma> \<S> P"
  assume sep: "fst env m \<inter> X = {}"
  have "\<And>x. x \<in> vars_and_\<C> (fst env m) \<Longrightarrow> x \<in> vars_and_\<C> X \<Longrightarrow> False"
  proof -
    fix x
    assume "x \<in> vars_and_\<C> (fst env m)"
    hence "x \<in> fst env m"
      using vars_and_\<C>_mds_subset con by blast
    assume "x \<in> vars_and_\<C> X"
    hence "x \<in> X \<or> x \<in> vars_\<C> X"
      unfolding vars_and_\<C>_def by blast
    thus "False"
    proof
      assume "x \<in> X"
      with `x \<in> fst env m` and sep show ?thesis by blast
    next
       assume "x \<in> vars_\<C> X"
       from this obtain y where y: "y \<in> X \<and> x \<in> \<C>_vars y" unfolding vars_\<C>_def by blast
       with `x \<in> fst env m` have "y \<in> fst env m"  using vars_and_\<C>_mds_subset_reverse       
         using con mem_Collect_eq subset_iff by blast
       with y sep show "False" by blast
    qed
  qed
  thus "vars_and_\<C> (fst env m) \<inter> vars_and_\<C> X = {}" by blast
qed

lemma var_modifiable_no_GuarNoReadOrWrite:
  "env_consistent env \<Gamma> \<S> P \<Longrightarrow> var_modifiable \<S> x \<Longrightarrow>  ProgVar x \<notin> fst env GuarNoReadOrWrite"
  by(auto simp add: env_consistent_def var_modifiable_def stable_NoRW_def image_def dest: sym)

lemma var_modifiable_no_GuarNoWrite:
  "env_consistent env \<Gamma> \<S> P \<Longrightarrow> var_modifiable \<S> x \<Longrightarrow>  ProgVar x \<notin> fst env GuarNoWrite"
  by(auto simp add: env_consistent_def var_modifiable_def stable_NoW_def image_def dest: sym)

lemma aexp_readable_no_GuarNoReadOrWrite:
  "env_consistent env \<Gamma> \<S> P \<Longrightarrow> aexp_readable \<S> e \<Longrightarrow> fst env GuarNoReadOrWrite \<inter> ProgVar ` aexp_vars e = {}"
  by(auto simp add: env_consistent_def aexp_readable_def var_readable_def stable_NoRW_def image_def dest: sym)

lemma bexp_readable_no_GuarNoReadOrWrite:
  "env_consistent env \<Gamma> \<S> P \<Longrightarrow> bexp_readable \<S> e \<Longrightarrow> fst env GuarNoReadOrWrite \<inter> ProgVar ` bexp_vars e = {}"
  by(auto simp add: env_consistent_def bexp_readable_def var_readable_def stable_NoRW_def image_def dest: sym)



lemma add_predI:
  "(\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow> ev\<^sub>B (to_prog_mem mem) e \<Longrightarrow>
   env_consistent env \<Gamma> \<S> P \<Longrightarrow>
    pred P (to_prog_mem mem) \<Longrightarrow>
     pred P +\<^sub>\<S> (bexp_to_lpred e) (to_prog_mem mem)"
  apply(clarsimp simp:  env_consistent_def add_pred_def stable_def var_asm_not_written_def stable_NoW_def stable_NoRW_def image_def bexp_to_lpred_correct bexp_to_lpred_vars | safe)+
  done

lemma add_bexp_neg_predI:
  "(\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow> \<not> (ev\<^sub>B (to_prog_mem mem) e) \<Longrightarrow>
   env_consistent env \<Gamma> \<S> P \<Longrightarrow>
    pred P (to_prog_mem mem) \<Longrightarrow>
     pred P +\<^sub>\<S> (PNeg (bexp_to_lpred e)) (to_prog_mem mem)"
  apply(clarsimp simp:  env_consistent_def add_pred_def stable_def var_asm_not_written_def stable_NoW_def stable_NoRW_def image_def bexp_to_lpred_correct bexp_to_lpred_vars | safe)+
  done

lemma Stop_in_loc_reachD':
  "\<langle>Stop, env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem\<rangle> \<Longrightarrow>
   \<turnstile> \<Gamma>,\<S>,P {c} \<Gamma>',\<S>',P' \<Longrightarrow>  preservation_props env \<Gamma> \<S> P mem \<Longrightarrow>
   (\<S>' = \<S> \<longrightarrow> env' = env) \<and> preservation_props env' \<Gamma>' \<S>' P' mem'"
  apply(drule loc_reach_preservation)
     apply fastforce
    apply fastforce
   apply assumption
  apply clarsimp
  apply(drule stop_cxt, fast)
  apply clarsimp
  apply(rule conjI)
   apply clarsimp
   apply(case_tac env, rename_tac mds agrels, case_tac env', rename_tac mds' agrels')
   apply clarsimp
   apply(rule conjI)
    apply(rule ext, rename_tac m, case_tac m)
    apply(auto simp: tyenv_wellformed_def env_consistent_def)[4]
  apply(rule ext, rename_tac m, case_tac m)
    apply(auto simp: tyenv_wellformed_def env_consistent_def)[2]
  apply(simp add: pred_entailment_def)
  by (metis prod.collapse)

lemma Stop_in_loc_reachD:
  "\<langle>Stop, env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem\<rangle> \<Longrightarrow> 
   \<turnstile> \<Gamma>,\<S>,P {c} \<Gamma>',\<S>',P' \<Longrightarrow>  preservation_props env \<Gamma> \<S> P mem \<Longrightarrow>
   (\<S>' = \<S> \<longrightarrow> env' = env) \<and> preservation_props env' \<Gamma>' \<S>' P' mem'"
  apply(drule Stop_in_loc_reachD')
  by auto
   
lemma tyenv_wellformed_AsmRel_trans:
  "tyenv_wellformed env \<Gamma> \<S> P \<Longrightarrow> R \<in> snd env AsmRel \<Longrightarrow> trans R"
  apply(auto simp: tyenv_wellformed_def env_consistent_def intro: transI simp: lock_no_release_rel_def)
  done

lemma to_lock_mem_ev\<^sub>L:
  "to_lock_mem mem = to_lock_mem mem' \<Longrightarrow> ev\<^sub>L mem l = ev\<^sub>L mem' l"
  using eval_det\<^sub>L to_lock_mem_def
  by metis

lemma blah:
  "\<forall>x. x \<noteq> Lock la \<longrightarrow> mem' x = mem x \<Longrightarrow> \<forall>x. x \<noteq> la \<longrightarrow> ev\<^sub>L mem x = ev\<^sub>L mem' x"
  by(fastforce intro!: eval_det\<^sub>L)

lemma eval_notin_lock_no_release_rel:
  "\<langle>c,env,mem\<rangle> \<leadsto> \<langle>c',env',mem'\<rangle> \<Longrightarrow> (mem,mem') \<notin> lock_no_release_rel l \<Longrightarrow> (\<forall>a b. c \<noteq> Seq a b) \<Longrightarrow> c = LockRel l"
  apply(induct rule: eval\<^sub>w.induct)
  apply(force simp: lock_no_release_rel_def dest: to_lock_mem_ev\<^sub>L dest!: blah)+
  apply(clarsimp simp: lock_no_release_rel_def dest: to_lock_mem_ev\<^sub>L dest!: blah)+
  using locks_can_be_taken
   apply (metis Var.inject(1) eval\<^sub>L_neq_mem_neq fun_upd_other)
  apply(clarsimp simp: lock_no_release_rel_def)
  using eval_det\<^sub>L
  by (metis Var.inject(1) fun_upd_other)


lemma GuarRel_refl:
  "tyenv_wellformed env \<Gamma> \<S> P \<Longrightarrow>
   R\<in>snd env GuarRel \<Longrightarrow> (mem,mem) \<in> R"
  apply(auto simp: tyenv_wellformed_def env_consistent_def lock_no_release_rel_def)
  done

lemma GuarRel_no_lock:
  "tyenv_wellformed env \<Gamma> \<S> P \<Longrightarrow>
   R\<in>snd env GuarRel \<Longrightarrow> global_invariant mem' \<Longrightarrow> (\<And>l. mem' (Lock l) = mem (Lock l)) \<Longrightarrow> (mem,mem') \<in> R"
  apply(clarsimp simp: tyenv_wellformed_def env_consistent_def lock_no_release_rel_def | safe)+
  using eval_det\<^sub>L by metis

lemma global_invariant_pres:
  "has_type \<Gamma> \<S> P c \<Gamma>' \<S>' P' \<Longrightarrow> preservation_props env \<Gamma> \<S> P mem \<Longrightarrow>
   \<langle>c,env,mem\<rangle> \<leadsto> \<langle>c',env',mem'\<rangle> \<Longrightarrow> global_invariant mem'"
  apply(drule preservation)
   apply(subst (asm) surjective_pairing[where t=env], subst (asm) surjective_pairing[where t=env'], assumption)
  apply fastforce
  done

lemma seq_respects_guarantee_rel:
  "c'' \<noteq> Stop \<Longrightarrow>
   respects_guarantee_rel c'' env' mem' x       \<Longrightarrow> respects_guarantee_rel (c'' ;; c) env' mem' x"
  apply(clarsimp simp: respects_guarantee_rel_def)
  apply(drule (1) seqD)
  apply auto
  done

lemma lpred_vars_add_pred_stable:
  "lpred_vars P \<subseteq> {v. stable \<S> v} \<Longrightarrow> lpred_vars P +\<^sub>\<S> p \<subseteq> {v. stable \<S> v}"
  apply(auto simp add: add_pred_def)
  done

lemma has_type_locally_sound_env_use':
  assumes typeable: "has_type \<Gamma> \<S> P c \<Gamma>' \<S>' P'"
  assumes pres_props: "preservation_props env \<Gamma> \<S> P mem"
  assumes in_loc_reach: "\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem\<rangle>"
  shows "doesnt_read_or_modify c' mem' (fst env' GuarNoReadOrWrite) \<and>
         doesnt_modify c' mem' (fst env' GuarNoWrite) \<and>
         (\<forall>R\<in>snd env' GuarRel. respects_guarantee_rel c' env' mem' R)"
using assms
proof (induct arbitrary: env  mem c' env' mem' rule: has_type.induct)
  case (stop_type \<Gamma> \<S> P)
  thus ?case
    apply -
    apply(drule stop_loc_reach)
    apply(clarsimp simp: stop_doesnt_access respects_guarantee_rel_def | safe)+
    using stop_no_eval by blast 
next
  case (skip_type \<Gamma> \<S> P)
  thus ?case
    apply -
    apply(drule skip_loc_reach)
    apply(clarsimp simp: skip_doesnt_access skip_doesnt_read skip_doesnt_write 
          stop_doesnt_access respects_guarantee_rel_def dest!: stop_no_eval | safe)+
    using skip_eval_elim  GuarRel_refl
    by blast
next
  case (assign\<^sub>\<C> x \<S> \<Gamma> e t P P' \<Gamma>')
  from `\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>x \<leftarrow> e, env, mem\<rangle>`
  show ?case
  apply -
  apply(drule assign_loc_reach)
   using tyenv_wellformed_AsmRel_trans `preservation_props env \<Gamma> \<S> P mem` apply fastforce
  apply(erule disjE)
   apply(clarsimp simp: stop_doesnt_access respects_guarantee_rel_def dest!: stop_no_eval)
  apply clarsimp
  apply(rule conjI)
   apply(rule assign_doesnt_read)
    apply(rule notI)
    apply(drule subsetD[OF vars_and_\<C>_mds_subset, rotated])
     using `preservation_props env \<Gamma> \<S> P mem`  
     unfolding tyenv_wellformed_def apply fastforce
    using var_modifiable_no_GuarNoReadOrWrite[OF _ `var_modifiable \<S> x`]
          `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def
           apply fastforce
   apply(rule vars_and_\<C>_mds_sep)
    using `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def apply blast
   apply(rule aexp_readable_no_GuarNoReadOrWrite)
    using `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def apply blast
   apply(rule `aexp_readable \<S> e`)
  apply(rule conjI)
   apply(rule assign_doesnt_modify)
   apply(rule notI)
   apply(drule subsetD[OF vars_and_\<C>_mds_subset, rotated])
    using `preservation_props env \<Gamma> \<S> P mem` 
    unfolding tyenv_wellformed_def apply fastforce
   using var_modifiable_no_GuarNoWrite[OF _ `var_modifiable \<S> x`]
          `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def
           apply fastforce
  apply(clarsimp simp: respects_guarantee_rel_def)
  apply(rule GuarRel_no_lock)
     using assign\<^sub>\<C>(10) apply fastforce+
   apply(rename_tac mama)
   apply(rule_tac env=env and mem=mem' in global_invariant_pres)
      apply(rule has_type.assign\<^sub>\<C>, (rule assign\<^sub>\<C>)+)
     using `preservation_props env \<Gamma> \<S> P mem` step_other_preservation apply blast
   apply assumption
  apply(fastforce dest: assignD)
  done
next
  case (assign\<^sub>1 x \<Gamma> e t P P' \<S> \<Gamma>')
  from `unprotected x` have "var_modifiable \<S> x"
  using unprotected_def var_modifiable_def by auto
  from `\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>x \<leftarrow> e, env, mem\<rangle>`
  show ?case
  apply -
  apply(drule assign_loc_reach)
   using tyenv_wellformed_AsmRel_trans `preservation_props env \<Gamma> \<S> P mem` apply fastforce
  apply(erule disjE)
   apply(clarsimp simp: stop_doesnt_access respects_guarantee_rel_def dest!: stop_no_eval)
  apply clarsimp
  apply(rule conjI)
   apply(rule assign_doesnt_read)
    apply(rule notI)
    apply(drule subsetD[OF vars_and_\<C>_mds_subset, rotated])
     using `preservation_props env \<Gamma> \<S> P mem`  
     unfolding tyenv_wellformed_def apply fastforce
    using var_modifiable_no_GuarNoReadOrWrite[OF _ `var_modifiable \<S> x`]
          `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def
           apply fastforce
   apply(rule vars_and_\<C>_mds_sep)
    using `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def apply blast
   apply(rule aexp_readable_no_GuarNoReadOrWrite)
    using `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def apply blast
   apply(rule `aexp_readable \<S> e`)
  apply(rule conjI)
   apply(rule assign_doesnt_modify)
   apply(rule notI)
   apply(drule subsetD[OF vars_and_\<C>_mds_subset, rotated])
    using `preservation_props env \<Gamma> \<S> P mem` 
    unfolding tyenv_wellformed_def apply fastforce
   using var_modifiable_no_GuarNoWrite[OF _ `var_modifiable \<S> x`]
          `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def
           apply fastforce
  apply(clarsimp simp: respects_guarantee_rel_def)
  apply(rule GuarRel_no_lock)
     using `preservation_props env \<Gamma> \<S> P mem` apply fastforce+
   apply(rename_tac mama)
   apply(rule_tac env=env and mem=mem' in global_invariant_pres)
      apply(rule has_type.assign\<^sub>1, (rule assign\<^sub>1)+)
     using `preservation_props env \<Gamma> \<S> P mem` step_other_preservation apply blast
   apply assumption
  apply(fastforce dest: assignD)
  done
next
  case (assign\<^sub>2 x \<Gamma> \<S> e t P' P \<Gamma>')
  from `\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>x \<leftarrow> e, env, mem\<rangle>`
  show ?case
  apply -
  apply(drule assign_loc_reach)
   using tyenv_wellformed_AsmRel_trans `preservation_props env \<Gamma> \<S> P mem` apply fastforce
  apply(erule disjE)
   apply(clarsimp simp: stop_doesnt_access respects_guarantee_rel_def dest!: stop_no_eval)
  apply clarsimp
  apply(rule conjI)
   apply(rule assign_doesnt_read)
    apply(rule notI)
    apply(drule subsetD[OF vars_and_\<C>_mds_subset, rotated])
     using `preservation_props env \<Gamma> \<S> P mem`  
     unfolding tyenv_wellformed_def apply fastforce
    using var_modifiable_no_GuarNoReadOrWrite[OF _ `var_modifiable \<S> x`]
          `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def
           apply fastforce
   apply(rule vars_and_\<C>_mds_sep)
    using `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def apply blast
   apply(rule aexp_readable_no_GuarNoReadOrWrite)
    using `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def apply blast
   apply(rule `aexp_readable \<S> e`)
  apply(rule conjI)
   apply(rule assign_doesnt_modify)
   apply(rule notI)
   apply(drule subsetD[OF vars_and_\<C>_mds_subset, rotated])
    using `preservation_props env \<Gamma> \<S> P mem` 
    unfolding tyenv_wellformed_def apply fastforce
   using var_modifiable_no_GuarNoWrite[OF _ `var_modifiable \<S> x`]
          `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def
           apply fastforce
  apply(clarsimp simp: respects_guarantee_rel_def)
  apply(rule GuarRel_no_lock)
     using `preservation_props env \<Gamma> \<S> P mem` apply fastforce+
   apply(rename_tac mama)
   apply(rule_tac env=env and mem=mem' in global_invariant_pres)
      apply(rule has_type.assign\<^sub>2, (rule assign\<^sub>2)+)
     using `preservation_props env \<Gamma> \<S> P mem` step_other_preservation apply blast
   apply assumption
  apply(fastforce dest: assignD)
  done
next
  case (if_type \<Gamma> e t P p \<S> c\<^sub>1 \<Gamma>' \<S>' P' c\<^sub>2 \<Gamma>'' P'' \<Gamma>''' P''')
  from `\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>If e c\<^sub>1 c\<^sub>2, env, mem\<rangle>`
  show ?case
  apply -
  apply(drule if_loc_reach)
    apply force
   using `preservation_props env \<Gamma> \<S> P mem` 
   apply(fastforce intro: tyenv_wellformed_AsmRel_trans)
  proof -
    assume " c' = Stmt.If e c\<^sub>1 c\<^sub>2 \<and>
    env' = env \<and> (mem' = mem \<or> step_other \<langle>Stmt.If e c\<^sub>1 c\<^sub>2, env, mem\<rangle> \<langle>c', env', mem'\<rangle>) \<or>
    \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env, mem\<rangle> \<and>
    ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow> ev\<^sub>B (to_prog_mem mem) e) \<or>
    \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, env, mem\<rangle> \<and>
    ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow>
     \<not> ev\<^sub>B (to_prog_mem mem) e)"
    thus "doesnt_read_or_modify c' mem' (fst env' GuarNoReadOrWrite) \<and>
    doesnt_modify c' mem' (fst env' GuarNoWrite) \<and>
    (\<forall>a\<in>snd env' GuarRel. respects_guarantee_rel c' env' mem' a)"
    proof
      assume " c' = Stmt.If e c\<^sub>1 c\<^sub>2 \<and>
    env' = env \<and> (mem' = mem \<or> step_other \<langle>Stmt.If e c\<^sub>1 c\<^sub>2, env, mem\<rangle> \<langle>c', env', mem'\<rangle>)"
      thus ?thesis
        apply(clarsimp)
        apply(rule conjI)
         apply(rule if_doesnt_read)
         apply(rule vars_and_\<C>_mds_sep)
          using `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def apply blast
         apply(rule bexp_readable_no_GuarNoReadOrWrite)
          using `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def apply blast
         apply(rule `bexp_readable \<S> e`)
        apply(rule conjI)
         apply(rule if_doesnt_modify)
        apply(clarsimp simp: respects_guarantee_rel_def)
        apply(erule ifD)
         apply clarsimp
         apply(rule GuarRel_refl)
          using `preservation_props env \<Gamma> \<S> P mem` apply fastforce
         apply clarsimp
        apply clarsimp
        apply(rule GuarRel_refl)
         using `preservation_props env \<Gamma> \<S> P mem` apply fastforce
        apply clarsimp
        done
    next
      assume "\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env, mem\<rangle> \<and>
    ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow> ev\<^sub>B (to_prog_mem mem) e) \<or>
    \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, env, mem\<rangle> \<and>
    ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow>
     \<not> ev\<^sub>B (to_prog_mem mem) e)"
      thus ?thesis
      proof 
        assume a: "\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env, mem\<rangle> \<and>
    ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow> ev\<^sub>B (to_prog_mem mem) e)"
        have pp: "preservation_props env \<Gamma> \<S> P +\<^sub>\<S> p mem"
          apply(rule conjI)
           apply(case_tac env, rename_tac mds agrels, clarsimp)
           apply(rule tyenv_wellformed_preds_update)
            apply(rule lpred_vars_add_pred_stable)
            using `preservation_props env \<Gamma> \<S> P mem`
            apply (fastforce simp: env_consistent_def tyenv_wellformed_def)
           using `preservation_props env \<Gamma> \<S> P mem` apply fastforce
          using `preservation_props env \<Gamma> \<S> P mem` apply clarsimp
          using a[THEN conjunct2] add_predI 
          using tyenv_wellformed_def
          using if_type.hyps(3) by blast
        show ?thesis
          apply(rule if_type(5))
           apply(rule pp)
          using a if_type(15) by simp
      next
        assume a: "\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, env, mem\<rangle> \<and>
    ((\<forall>x\<in>bexp_vars e. var_asm_not_written (fst env) (ProgVar x)) \<longrightarrow>
     \<not> ev\<^sub>B (to_prog_mem mem) e)"
        have pp: "preservation_props env \<Gamma> \<S> P +\<^sub>\<S> (PNeg p) mem"
          apply(rule conjI)
           apply(case_tac env, rename_tac mds agrels, clarsimp)
           apply(rule tyenv_wellformed_preds_update)
            apply(rule lpred_vars_add_pred_stable)
            using `preservation_props env \<Gamma> \<S> P mem`
            apply (fastforce simp: env_consistent_def tyenv_wellformed_def)
           using `preservation_props env \<Gamma> \<S> P mem` apply fastforce
          using `preservation_props env \<Gamma> \<S> P mem` apply clarsimp
          using a[THEN conjunct2] add_bexp_neg_predI
          using tyenv_wellformed_def
          using if_type.hyps(3) by blast
        show ?thesis
          apply(rule if_type(7))
           apply(rule pp)
          using a if_type(15) by simp
      qed
    qed
  qed
next
  case (while_type \<Gamma> e t P \<S> c)
  have while_loc_reach: "\<And> c' env' mem' mem.
  \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>While e c, env, mem\<rangle> \<Longrightarrow>
  preservation_props env \<Gamma> \<S> P mem \<Longrightarrow>
  c' = While e c \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem' \<or>
  c' = Stmt.If e (c ;; While e c) Stop \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem' \<or>
  c' = Stop \<and> env' = env \<or>
  (\<exists>c'' mem''.
      c' = c'' ;; While e c \<and> preservation_props env \<Gamma> \<S> (add_pred P \<S> (bexp_to_lpred e)) mem'' \<and>
       (c'' = Stop \<longrightarrow> env' = env \<and> preservation_props env \<Gamma> \<S> P mem') \<and> 
       \<langle>c'', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem''\<rangle>)"
  proof -
    fix c' env' mem' mem
    assume "\<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>While e c, env, mem\<rangle>"
           "preservation_props env \<Gamma> \<S> P mem"
    thus "  c' = While e c \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem' \<or>
  c' = Stmt.If e (c ;; While e c) Stop \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem' \<or>
  c' = Stop \<and> env' = env \<or>
  (\<exists>c'' mem''.
      c' = c'' ;; While e c \<and> preservation_props env \<Gamma> \<S> (add_pred P \<S> (bexp_to_lpred e)) mem'' \<and>
       (c'' = Stop \<longrightarrow> env' = env \<and> preservation_props env \<Gamma> \<S> P mem') \<and> \<langle>c'', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem''\<rangle>)"
    proof(induct rule: loc_reach.induct)
      case refl
      thus ?case
        by clarsimp
    next
      case (step c' env' mem' c'' env'' mem'')
      from step(2)[OF step(4)] have
      " c' = While e c \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem' \<or>
  c' = Stmt.If e (c ;; While e c) Stop \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem' \<or>
  c' = Stop \<and> env' = env \<or>
  (\<exists>c'' mem''.
      c' = c'' ;; While e c \<and>
      preservation_props env \<Gamma> \<S> P +\<^sub>\<S> (bexp_to_lpred e) mem'' \<and>
       (c'' = Stop \<longrightarrow> env' = env \<and> preservation_props env \<Gamma> \<S> P mem') \<and> \<langle>c'', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem''\<rangle>) " by blast
      thus ?case
      proof(elim disjE)
        assume a: "c' = While e c \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem'"
        with step(3) have 
        "c'' = Stmt.If e (c ;; While e c) Stop \<and> env'' = env \<and> mem'' = mem'"
          using while_eval_elim 
          by simp
        thus ?thesis
          using a by blast
      next
        assume a: "c' = Stmt.If e (c ;; While e c) Stop \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem'"
        with step(3) have d: "(c'' = c ;; While e c \<and> ev\<^sub>B (to_prog_mem mem') e \<or>
                            c'' = Stop \<and> \<not> ev\<^sub>B (to_prog_mem mem') e)"
                      and b [simp]: "env'' = env \<and> mem'' = mem'" by blast+
        from d show ?case
        proof
          assume a': "c'' = c ;; While e c \<and> ev\<^sub>B (to_prog_mem mem') e"
          from a 
          have "preservation_props env \<Gamma> \<S> P +\<^sub>\<S> (bexp_to_lpred e) mem''"
            apply(clarsimp simp: a)
            apply(rule conjI)
             apply(case_tac env, rename_tac mds agrels, clarsimp)
             apply(rule tyenv_wellformed_preds_update)
             apply(rule lpred_vars_add_pred_stable)
              using a
              apply (fastforce simp: env_consistent_def tyenv_wellformed_def)
             using a apply fastforce
            using a' a apply(auto simp: pred_def add_pred_def bexp_to_lpred_correct)
            done
          with a show ?case
          using a' b loc_reach_refl by blast
        next
          assume "c'' = Stop \<and> \<not> ev\<^sub>B (to_prog_mem mem') e"
          with a show ?case by auto
        qed
      next
        assume "c' = Stop \<and> env' = env"
        with step(3) show ?case
        using stop_no_eval by (auto dest!: stop_no_eval)
      next
        assume " \<exists>c'' mem''.
          c' = c'' ;; While e c \<and>
          preservation_props env \<Gamma> \<S> P +\<^sub>\<S> (bexp_to_lpred e) mem'' \<and>
           (c'' = Stop \<longrightarrow> env' = env  \<and> preservation_props env \<Gamma> \<S> P mem') \<and> \<langle>c'', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem''\<rangle>"
        from this obtain c''' mem''' where
          c''': "c' = c''' ;; While e c \<and>
           preservation_props env \<Gamma> \<S> P +\<^sub>\<S> (bexp_to_lpred e) mem''' \<and>
           (c''' = Stop \<longrightarrow> env' = env \<and> preservation_props env \<Gamma> \<S> P mem') \<and> \<langle>c''', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem'''\<rangle>"
          by blast
        show ?case
        proof(cases "c''' = Stop")
          assume Stop: "c''' = Stop"
          with c''' step(3) seq_stop_elim
          have "c'' = While e c" "env'' = env'" "mem' = mem''" "env' = env" by blast+
          thus ?thesis using Stop 
                       using c''' by auto
        next
          assume notStop: "c''' \<noteq> Stop"
          with c''' step(3) seqD obtain c'''' where
          step': "\<langle>c''', env', mem'\<rangle> \<leadsto> \<langle>c'''', env'', mem''\<rangle>" and c''_def: "c'' = Seq c'''' (While e c)"  by blast+
          have loc_reach': "\<langle>c'''', env'', mem''\<rangle> \<in> loc_reach \<langle>c, env, mem'''\<rangle>"
            apply(rule loc_reach.step)
             prefer 2
             apply(rule step')
            using c''' by blast
          with c''_def show ?thesis
            apply -
            apply(rule disjI2)+
            apply(rule_tac x="c''''" in exI)
            apply(rule_tac x="mem'''" in exI)
            apply(clarsimp simp: c''' notStop step)
            apply(drule Stop_in_loc_reachD)
              apply(rule while_type(3))
             using c''' apply fast
            by clarsimp
        qed
      qed
    next
      case (mem_diff c' env' mem' mem'')
      from mem_diff(2)[OF mem_diff(5)] have disj:
      " c' = While e c \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem' \<or>
  c' = Stmt.If e (c ;; While e c) Stop \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem' \<or>
  c' = Stop \<and> env' = env \<or>
  (\<exists>c'' mem''.
      c' = c'' ;; While e c \<and>
      preservation_props env \<Gamma> \<S> (P +\<^sub>\<S> (bexp_to_lpred e)) mem'' \<and>
      (c'' = Stop \<longrightarrow> env' = env \<and> preservation_props env \<Gamma> \<S> P mem') \<and>
      \<langle>c'', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem''\<rangle>)" by blast
      have other: "step_other  \<langle>c',env',mem'\<rangle> \<langle>c',env',mem''\<rangle>" 
        using mem_diff(3) mem_diff(4) step_other.intros by blast

      from disj show ?case
      proof(elim disjE)
        assume c': "c' = While e c \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem'"
        hence "preservation_props env \<Gamma> \<S> P mem''"
          using c' step_other_preservation other
          using fst_conv
          by blast
        thus ?case using c' by blast
      next
        assume c': "c' = Stmt.If e (c ;; While e c) Stop \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem'"
        hence "preservation_props env \<Gamma> \<S> P mem''"
          using c' step_other_preservation other
          using fst_conv  by blast
        thus ?case using c' by blast
      next
        assume "c' = Stop \<and> env' = env" thus ?case by blast
      next
        assume "\<exists>c'' mem''.
       c' = c'' ;; While e c \<and>
       preservation_props env \<Gamma> \<S> P +\<^sub>\<S> (bexp_to_lpred e) mem'' \<and>
       (c'' = Stop \<longrightarrow> env' = env \<and> preservation_props env \<Gamma> \<S> P mem') \<and>
       \<langle>c'', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem''\<rangle>"
        from this obtain c'' mem''' where
        c': "c' = c'' ;; While e c \<and>
         preservation_props env \<Gamma> \<S> P +\<^sub>\<S> (bexp_to_lpred e) mem''' \<and>
         (c'' = Stop \<longrightarrow> env' = env \<and> preservation_props env \<Gamma> \<S> P mem') \<and>
         \<langle>c'', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem'''\<rangle>" by blast
        have lr: "\<langle>c'', env', mem''\<rangle> \<in> loc_reach \<langle>c, env, mem'''\<rangle>"
          apply(rule loc_reach.mem_diff)
            using c' apply fastforce
           using mem_diff(3) mem_diff(4) by auto
        show ?case
          apply(rule disjI2)+
          apply(rule_tac x=c'' in exI)
          apply(rule_tac x=mem''' in exI)
          apply(simp add: c' lr)
          using c' step_other_preservation other
          using fst_conv by blast
      qed
    qed
  qed 
  with while_type(7)  while_type(6) have
  " c' = While e c \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem' \<or>
  c' = Stmt.If e (c ;; While e c) Stop \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem' \<or>
  c' = Stop \<and> env' = env \<or>
  (\<exists>c'' mem''.
      c' = c'' ;; While e c \<and>
      preservation_props env \<Gamma> \<S> P +\<^sub>\<S> (bexp_to_lpred e) mem'' \<and>
      (c'' = Stop \<longrightarrow> env' = env \<and> preservation_props env \<Gamma> \<S> P mem') \<and>
      \<langle>c'', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem''\<rangle>)"
    by (simp add: fst_conv)
  thus ?case
  proof(elim disjE)
    assume "c' = While e c \<and> env' = env \<and> preservation_props env \<Gamma> \<S> P mem'"
    thus ?thesis
      using while_doesnt_read while_doesnt_modify apply simp
      apply(clarsimp simp: respects_guarantee_rel_def)
      apply(drule whileD)
      apply clarsimp
      apply(rule GuarRel_refl)
       apply fastforce+
      done
  next
    assume "c' = Stmt.If e (c ;; While e c) Stop \<and>
            env' = env \<and> preservation_props env \<Gamma> \<S> P mem'"
    thus ?thesis
      apply -
      apply(rule conjI)
       apply clarsimp
       apply(rule if_doesnt_read)
       apply(rule vars_and_\<C>_mds_sep)
       using `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def apply blast
        apply(rule bexp_readable_no_GuarNoReadOrWrite)
        using `preservation_props env \<Gamma> \<S> P mem` unfolding tyenv_wellformed_def apply blast
       apply(rule `bexp_readable \<S> e`)
      apply clarsimp
      apply(rule conjI)
       apply(rule if_doesnt_modify)
      apply(clarsimp simp: respects_guarantee_rel_def)
      apply(erule ifD)
       apply clarsimp
       apply(rule GuarRel_refl)
        apply (fastforce simp: tyenv_wellformed_def)
       apply clarsimp
      apply clarsimp
      apply(rule GuarRel_refl)
       apply (fastforce simp: tyenv_wellformed_def)
      apply clarsimp
      done
  next
    assume "c' = Stop \<and> env' = env" thus ?thesis by (clarsimp simp: stop_doesnt_access respects_guarantee_rel_def dest!: stop_no_eval)
  next
    assume "\<exists>c'' mem''.
       c' = c'' ;; While e c \<and>
       preservation_props env \<Gamma> \<S> P +\<^sub>\<S> (bexp_to_lpred e) mem'' \<and>
       (c'' = Stop \<longrightarrow> env' = env \<and> preservation_props env \<Gamma> \<S> P mem') \<and>
       \<langle>c'', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem''\<rangle>"
    from this obtain c'' mem'' where
      "c' = c'' ;; While e c \<and>
       preservation_props env \<Gamma> \<S> P +\<^sub>\<S> (bexp_to_lpred e) mem'' \<and>
       (c'' = Stop \<longrightarrow> env' = env \<and> preservation_props env \<Gamma> \<S> P mem') \<and>
       \<langle>c'', env', mem'\<rangle> \<in> loc_reach \<langle>c, env, mem''\<rangle>" by blast
    thus ?case
      apply -
      apply(rule conjI)
       apply clarsimp
       apply(rule seq_doesnt_read)
       using while_type(4) while_type(7)
       using fst_conv apply auto[1]
      apply clarsimp
      apply(rule conjI)
       apply(rule seq_doesnt_modify)
       using while_type(4) while_type(7)
       using fst_conv apply auto[1]
      apply(case_tac "c'' = Stop")
       apply(clarsimp simp: respects_guarantee_rel_def)
       apply(drule seq_stop_elim)
       apply clarsimp
       apply(rule GuarRel_refl)
        using `preservation_props env \<Gamma> \<S> P mem` apply fastforce
       apply clarsimp
      apply clarsimp
      apply(erule seq_respects_guarantee_rel)
      using while_type(4)
      by blast   
  qed
next
  case (seq_type  \<Gamma> \<S> P c\<^sub>1 \<Gamma>' \<S>' P' c\<^sub>2 \<Gamma>'' \<S>'' P'')
  have disj: "(\<exists>c''. c' = c'' ;; c\<^sub>2 \<and> \<langle>c'', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env, mem\<rangle>) \<or>
(\<exists>c'' mds'' mem''.
    \<langle>Stop, mds'', mem''\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env, mem\<rangle> \<and>
    \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, mds'', mem''\<rangle>)"
    by(rule seq_loc_reach[OF seq_type(6)])
  thus ?case
  proof
    assume "\<exists>c''. c' = c'' ;; c\<^sub>2 \<and> \<langle>c'', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env, mem\<rangle>"
    thus ?case
      apply -
      apply clarsimp
      apply(rule conjI)
       apply(rule seq_doesnt_read)
       using seq_type(2)
       using seq_type.prems(1) seq_type.prems(2) apply blast
      apply(rule conjI)
       apply(rule seq_doesnt_modify)
       using seq_type(2)
       using seq_type.prems(1) seq_type.prems(2) apply blast
      apply clarsimp
      apply(case_tac "c'' = Stop")
       apply(clarsimp simp: respects_guarantee_rel_def)
       apply(drule seq_stop_elim)
       apply clarsimp
       apply(rename_tac mds agrels)
       apply(drule loc_reach_preservation, simp+)
        apply(rule ` \<turnstile> \<Gamma>,\<S>,P {c\<^sub>1} \<Gamma>',\<S>',P'`)
       apply clarsimp
       apply(rule_tac env="(mds,agrels)" in GuarRel_refl)
        using `preservation_props env \<Gamma> \<S> P mem` apply fastforce
       apply clarsimp      
      apply(erule seq_respects_guarantee_rel)
      using seq_type(2)
      using \<open>preservation_props env \<Gamma> \<S> P mem\<close> by blast
  next
    assume " \<exists>c'' mds'' mem''.
       \<langle>Stop, mds'', mem''\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env, mem\<rangle> \<and>
       \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, mds'', mem''\<rangle>"
    from this obtain c'' mds'' mem'' where
    c'': "\<langle>Stop, mds'', mem''\<rangle> \<in> loc_reach \<langle>c\<^sub>1, env, mem\<rangle> \<and>
          \<langle>c', env', mem'\<rangle> \<in> loc_reach \<langle>c\<^sub>2, mds'', mem''\<rangle>"
    by blast
    from c'' Stop_in_loc_reachD'[OF _ seq_type(1)] seq_type(4) show ?case
      by (metis prod.collapse seq_type.prems(1) seq_type.prems(2))
  qed
next
  case (sub \<Gamma>\<^sub>1 \<S> P\<^sub>1 c \<Gamma>\<^sub>1' \<S>' P\<^sub>1' \<Gamma>\<^sub>2 P\<^sub>2 \<Gamma>\<^sub>2' P\<^sub>2')
  from sub(4) sub(8)  `P\<^sub>2 \<turnstile> P\<^sub>1` pred_entailment_def
  have "preservation_props env \<Gamma>\<^sub>1 \<S> P\<^sub>1 mem"
    using sub.prems(1) by blast
  with sub(2)[OF this] show ?case
  using sub.prems(2) by blast
next
  case (lock_acq_type upd l \<Gamma>' \<Gamma> \<S> \<S>' P' P)
  have "\<forall>R\<in>snd env AsmRel. trans R"
    using tyenv_wellformed_AsmRel_trans `preservation_props env \<Gamma> \<S> P mem` by fastforce
  from lock_acq_loc_reach[OF lock_acq_type(6) this]
  have "c' = Stop \<and> env' = (lock_acq_mds_upd l (fst env), lock_acq_agrels_upd l (snd env)) \<or> c' = LockAcq l \<and> env' = env \<and> (mem' = mem \<or> step_other \<langle>c',env,mem\<rangle> \<langle>c',env,mem'\<rangle>)"
    by blast
  thus ?case
  proof
    assume "c' = Stop \<and> env' = (lock_acq_mds_upd l (fst env), lock_acq_agrels_upd l (snd env))"
    thus ?case by (clarsimp simp: stop_doesnt_access respects_guarantee_rel_def dest!: stop_no_eval)
  next
    assume "c' = LockAcq l \<and> env' = env \<and> (mem' = mem \<or> step_other \<langle>c',env,mem\<rangle> \<langle>c',env,mem'\<rangle>)"
    thus ?case
      apply -
      using lock_acq_type(6)  lock_acq_type(5)
      apply (clarsimp simp: tyenv_wellformed_def env_consistent_def)
      apply (rule conjI)
       apply(rule lock_acq_doesnt_read_progvars)
       apply clarsimp
      apply(rule conjI)
       apply(rule lock_acq_doesnt_modify)
       apply(clarsimp simp: image_def)
      apply(rule conjI)
       apply(clarsimp simp: respects_guarantee_rel_def)
       apply(rename_tac mama)
       apply(rule_tac mem=mem' in global_invariant_pres)
          apply(rule has_type.lock_acq_type, (rule lock_acq_type)+)
         apply clarsimp
         apply(erule disjE)
          using `preservation_props env \<Gamma> \<S> P mem` apply fastforce
         using `preservation_props env \<Gamma> \<S> P mem` step_other_preservation 
         apply presburger
        apply assumption
       apply(clarsimp simp: respects_guarantee_rel_def)
       apply(rule ccontr)
       apply(drule (1) eval_notin_lock_no_release_rel, simp+)
       done
  qed
next
  case (lock_rel_type upd l \<Gamma>' \<Gamma> \<S> \<S>' P P')
  have "\<forall>R\<in>snd env AsmRel. trans R"
    using tyenv_wellformed_AsmRel_trans `preservation_props env \<Gamma> \<S> P mem` by fastforce
  from lock_rel_loc_reach[OF lock_rel_type(10) this]
  have "c' = Stop \<and> fst env' = lock_rel_mds_upd l (fst env) \<and> snd env' = lock_rel_agrels_upd l (snd env) \<or> c' = LockRel l \<and> env' = env \<and> (mem' = mem \<or> step_other \<langle>c', env, mem\<rangle> \<langle>c', env, mem'\<rangle>)"
    by simp
  thus ?case
  proof
    assume "c' = Stop \<and> fst env' = lock_rel_mds_upd l (fst env) \<and> snd env' = lock_rel_agrels_upd l (snd env)"
    thus ?case by (clarsimp simp: stop_doesnt_access respects_guarantee_rel_def dest!: stop_no_eval)
  next
    assume "c' = LockRel l \<and> env' = env \<and> (mem' = mem \<or> step_other \<langle>c', env, mem\<rangle> \<langle>c', env, mem'\<rangle>)"
    thus ?case
      apply -
      using lock_rel_type(10)  lock_rel_type(9)
      apply (clarsimp simp: tyenv_wellformed_def env_consistent_def)
      apply (rule conjI)
       apply(rule lock_rel_doesnt_read_progvars)
       apply clarsimp
      apply(rule conjI)
       apply(rule lock_rel_doesnt_modify_progvars)
       apply(clarsimp simp: image_def)
      apply(rule conjI)
       apply(clarsimp simp: respects_guarantee_rel_def)
       apply(rename_tac mama)
       apply(rule_tac mem=mem' in global_invariant_pres)
         apply(rule has_type.lock_rel_type, (rule lock_rel_type)+)
        apply(erule disjE)
         using `preservation_props env \<Gamma> \<S> P mem` apply fastforce
         apply clarsimp
         using `preservation_props env \<Gamma> \<S> P mem` step_other_preservation 
         apply presburger
        apply assumption
       apply(clarsimp simp: respects_guarantee_rel_def)
       apply(rule ccontr)
       apply(drule (1) eval_notin_lock_no_release_rel, (fastforce simp add: `l \<in> \<S>`)+)
      done
  qed
qed

lemma has_type_locally_sound_env_use:
  "\<turnstile> \<Gamma>,\<S>,P {c} \<Gamma>',\<S>',P' \<Longrightarrow>
  preservation_props env \<Gamma> \<S> P mem \<Longrightarrow> locally_sound_env_use \<langle>c,env,mem\<rangle>"
  apply(fastforce dest: has_type_locally_sound_env_use' simp: locally_sound_env_use_def)
  done

end

end