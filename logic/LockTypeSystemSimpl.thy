(*
Author: Toby Murray
*)

theory LockTypeSystemSimpl
imports LockTypeSystem "~~/src/HOL/Eisbach/Eisbach"
begin

context sifum_types begin

section "Helper Reasoning Lemmas"

lemma has_type_strengthen_post: 
  "has_type \<Gamma> L P c \<Gamma>' L' P' \<Longrightarrow> P' \<turnstile> Pw \<Longrightarrow> lpred_vars Pw \<subseteq> lpred_vars P' \<Longrightarrow> 
   has_type \<Gamma> L P c \<Gamma>' L' Pw"
  apply(erule sub)
       apply(blast intro: context_equiv_refl)
      apply blast
     apply(blast dest: lpred_vars_tyenv_wellformed_pres)
    apply(blast intro: context_equiv_refl)
   apply(blast intro: pred_entailment_refl)+
  done

lemma has_type_weaken_pre: 
  "P \<turnstile> Pw \<Longrightarrow> lpred_vars Pw \<subseteq> lpred_vars P \<Longrightarrow> has_type \<Gamma> L Pw c \<Gamma>' L' P' \<Longrightarrow> 
   has_type \<Gamma> L P c \<Gamma>' L' P'"
  apply(erule sub)
       apply(blast intro: context_equiv_refl)
      apply(blast dest: lpred_vars_tyenv_wellformed_pres)
     apply blast
    apply(blast intro: context_equiv_refl)
   apply blast
  apply(blast intro: pred_entailment_refl)
  done

section "Simplifying Predicates"

(* strengthen this as needed *)
inductive pred_simpl :: "('Var,'Val) lpred preds \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> bool"
where
  "pred_simpl (PConj P P) P" |
  "pred_simpl (PConj P PTrue) P" |
  "pred_simpl (PDisj P P) P" |
  "pred_simpl (PConj PTrue P) P" |
  "pred_simpl (PEx (\<lambda>x. P)) P" |
  "pred_simpl (PAll (\<lambda>x. P)) P" |
  "pred_simpl P P' \<Longrightarrow> pred_simpl (PConj P Q) (PConj P' Q)" |
  "pred_simpl Q Q' \<Longrightarrow> pred_simpl (PConj P Q) (PConj P Q')" |
  "pred_simpl P P' \<Longrightarrow> pred_simpl (PDisj P Q) (PDisj P' Q)" |
  "pred_simpl Q Q' \<Longrightarrow> pred_simpl (PDisj P Q) (PDisj P Q')"

lemma pred_simpl_sound:
  "pred_simpl P Q \<Longrightarrow> P \<turnstile> Q \<and> Q \<turnstile> P \<and> lpred_vars Q = lpred_vars P"
  apply(induct rule: pred_simpl.induct, auto simp: pred_entailment_def)
  done

lemma has_type_simpl_pre:
  "pred_simpl P Pw \<Longrightarrow> has_type \<Gamma> L Pw c \<Gamma>' L' P' \<Longrightarrow> 
   has_type \<Gamma> L P c \<Gamma>' L' P'"
  using has_type_weaken_pre pred_simpl_sound by blast


lemma pred_simpl_type_equiv:
  "pred_simpl x y \<Longrightarrow> type_equiv x P y"
  apply(drule pred_simpl_sound)
  apply(clarsimp simp: type_equiv_def subtype_def pred_entailment_def)
  done

inductive 
  context_simpl :: "('Var \<Rightarrow> ('Var,'Val) lpred preds option) \<Rightarrow> ('Var \<Rightarrow> ('Var,'Val) lpred preds option) \<Rightarrow> bool"
where
  "\<Gamma> = (\<Gamma>'(x \<mapsto> y)) \<Longrightarrow> pred_simpl y z \<Longrightarrow> context_simpl \<Gamma> (\<Gamma>'(x \<mapsto> z))" |
  "\<Gamma> = (\<Gamma>'(x \<mapsto> y)) \<Longrightarrow> context_simpl \<Gamma>' \<Gamma>'' \<Longrightarrow> context_simpl \<Gamma> (\<Gamma>''(x \<mapsto> y))"

lemma context_simpl_sound:
  "context_simpl \<Gamma> \<Gamma>' \<Longrightarrow> \<exists>x y z. \<Gamma> x = Some y \<and> (pred_simpl y z \<or> z = y) \<and> \<Gamma>' = \<Gamma>(x \<mapsto> z)"
proof(induct rule: context_simpl.induct)
  case (1 \<Gamma> \<Gamma>' x y z)
  hence "\<Gamma>'(x \<mapsto> z) = \<Gamma>(x \<mapsto> z)"
    by simp
  with "1" show ?case by fastforce
next
  case (2 \<Gamma> \<Gamma>' x y \<Gamma>'')
  from this obtain x' y' z' where
  "\<Gamma>' x' = Some y' \<and> (pred_simpl y' z' \<or> z' = y') \<and> \<Gamma>'' = \<Gamma>'(x' \<mapsto> z')" by blast
  thus ?case using "2" by fastforce
qed

lemma map_upd_trivial:
  "\<Gamma> x = Some y \<Longrightarrow> (\<Gamma>(x \<mapsto> y)) = \<Gamma>"
  apply auto
  done

lemma type_equiv_sym:
  "type_equiv t P t' \<Longrightarrow> type_equiv t' P t"
  by (simp add: type_equiv_def)

lemma has_type_context_simpl_pre':
  notes  map_upd_trivial [simp]
  shows "\<Gamma> x = Some y \<Longrightarrow> pred_simpl y z \<or> pred_simpl z y \<or> z = y \<Longrightarrow> has_type (\<Gamma>(x \<mapsto> z)) L P c \<Gamma>' L' P' \<Longrightarrow> has_type \<Gamma> L P c \<Gamma>' L' P'"
  apply(erule sub)
       apply(clarsimp simp: context_equiv_def)
       using pred_simpl_type_equiv
       using insert_dom type_equiv_refl type_equiv_sym apply auto[1]
      apply(erule disjE)
       apply(drule pred_simpl_sound)
       apply(clarsimp simp: tyenv_wellformed_def env_consistent_def types_wellformed_def type_wellformed_def)[1]
       apply (metis (no_types, lifting) domI insert_dom mem_Collect_eq option.sel types_stable_def types_stable_update)
      apply clarsimp
      apply(erule disjE)
       apply(drule pred_simpl_sound)
       apply(clarsimp simp: tyenv_wellformed_def env_consistent_def types_wellformed_def type_wellformed_def)[1]
       apply (metis (no_types, lifting) domI insert_dom mem_Collect_eq option.sel types_stable_def types_stable_update)
      apply clarsimp
     apply(blast intro: context_equiv_refl pred_entailment_refl)+
  done

lemma has_type_context_simpl_pre:
  "context_simpl \<Gamma> \<Gamma>'' \<Longrightarrow> has_type \<Gamma>'' L P c \<Gamma>' L' P' \<Longrightarrow> has_type \<Gamma> L P c \<Gamma>' L' P'"
  apply(drule context_simpl_sound)
  apply clarsimp
  apply(rule has_type_context_simpl_pre')
  apply auto
  done

lemma has_type_context_simpl_pre_rev:
  "context_simpl \<Gamma>'' \<Gamma> \<Longrightarrow> has_type \<Gamma>'' L P c \<Gamma>' L' P' \<Longrightarrow> has_type \<Gamma> L P c \<Gamma>' L' P'"
  apply(drule context_simpl_sound)
  apply clarsimp
  apply(cut_tac c=c and \<Gamma>="\<Gamma>''(x \<mapsto> z)" and z=y and y=z and x=x in has_type_context_simpl_pre')
     apply simp
    apply blast
   apply(subgoal_tac "\<Gamma>''(x \<mapsto> z, x \<mapsto> y) = \<Gamma>''")
    apply fastforce
   apply fastforce+
  done


lemma has_type_context_simpl_post':
  notes  map_upd_trivial [simp]
  shows "\<Gamma>' x = Some y \<Longrightarrow> pred_simpl y z \<or> pred_simpl z y \<or> z = y \<Longrightarrow> has_type \<Gamma> L P c (\<Gamma>'(x \<mapsto> z)) L' P' \<Longrightarrow> has_type \<Gamma> L P c \<Gamma>' L' P'"
  apply(erule sub)
       apply(blast intro: context_equiv_refl)
      apply blast
     apply(erule disjE)
      apply(drule pred_simpl_sound)
      apply(clarsimp simp: tyenv_wellformed_def env_consistent_def types_wellformed_def type_wellformed_def)[1]
      apply(rule conjI)
       apply (metis (no_types, lifting) domI insert_dom mem_Collect_eq option.sel types_stable_def types_stable_update)
      apply(rule conjI)
       apply (metis (no_types, lifting) domI insert_dom mem_Collect_eq option.sel types_stable_def types_stable_update)
      apply(clarsimp simp: types_stable_def split: if_splits)
      apply (metis (mono_tags) domI insert_dom mem_Collect_eq option.sel)
     apply clarsimp
     apply(erule disjE)
      apply(drule pred_simpl_sound)
      apply(clarsimp simp: tyenv_wellformed_def env_consistent_def types_wellformed_def type_wellformed_def)[1]
      apply(rule conjI)
       apply (metis (no_types, lifting) domI insert_dom mem_Collect_eq option.sel types_stable_def types_stable_update)
      apply(rule conjI)
       apply(clarsimp simp: types_stable_def split: if_splits)
       apply (metis (no_types, lifting) option.sel type_wellformed_def types_mention_only_control_vars types_wellformed_def)
      apply(clarsimp simp: types_stable_def split: if_splits)
      apply (metis (mono_tags) domI insert_dom mem_Collect_eq option.sel)
     apply clarsimp
    apply(erule disjE)
     apply(clarsimp simp: context_equiv_def)[1]
     using pred_simpl_type_equiv type_equiv_sym apply blast
    apply(erule disjE)
     apply(clarsimp simp: context_equiv_def)[1]
     using pred_simpl_type_equiv type_equiv_sym apply blast
    apply clarsimp
   apply(blast intro: context_equiv_refl pred_entailment_refl)+
  done

lemma has_type_context_simpl_post:
  "context_simpl \<Gamma>' \<Gamma>'' \<Longrightarrow> has_type \<Gamma> L P c \<Gamma>'' L' P' \<Longrightarrow> has_type \<Gamma> L P c \<Gamma>' L' P'"
  apply(drule context_simpl_sound)
  apply clarsimp
  apply(rule has_type_context_simpl_post')
  apply auto
  done

lemma has_type_context_simpl_post_rev:
  "context_simpl \<Gamma>'' \<Gamma>' \<Longrightarrow> has_type \<Gamma> L P c \<Gamma>'' L' P' \<Longrightarrow> has_type \<Gamma> L P c \<Gamma>' L' P'"
  apply(drule context_simpl_sound)
  apply clarsimp
  apply(cut_tac c=c and \<Gamma>'="\<Gamma>''(x \<mapsto> z)" and z=y and y=z and x=x in has_type_context_simpl_post')
     apply simp
    apply blast
   apply(subgoal_tac "\<Gamma>''(x \<mapsto> z, x \<mapsto> y) = \<Gamma>''")
    apply fastforce
   apply fastforce+
  done

text {*
  An assignment rule that simplifies the types added to @{term \<Gamma>} as they are added. 
*}
lemma assign\<^sub>2'_simpl:
  "x \<in> dom \<Gamma> \<Longrightarrow>
  var_modifiable \<S> x \<Longrightarrow>
  \<Gamma> \<turnstile>\<^sub>a e \<in> t \<Longrightarrow>
  type_stable \<S> t \<Longrightarrow>
  P' = assign_post P x e |`' {v. stable \<S> v} \<Longrightarrow>
  x \<notin> stable_NoRW \<S> \<longrightarrow> t \<le>:\<^sub>P' bexp_to_lpred (dma_type x) \<Longrightarrow> \<S>' = \<S> \<Longrightarrow>
  pred_simpl t t' \<Longrightarrow>
  \<Gamma>' = \<Gamma>(x \<mapsto> t') \<Longrightarrow> aexp_readable \<S> e \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P {x \<leftarrow> e} \<Gamma>',\<S>',P'"
  apply(rule_tac \<Gamma>''="\<Gamma>(x \<mapsto> t)" in has_type_context_simpl_post_rev)
   apply simp
   apply(rule context_simpl.intros(1))
    apply simp
   apply simp
  apply(blast intro: assign\<^sub>2)
  done  


section "Support for Reasoning about If-Statements"


definition
  combine_preds :: "('Var,'Val) lpred preds \<Rightarrow> ('Var,'Val) lpred preds \<Rightarrow> ('Var,'Val) lpred preds"
where
  "combine_preds P Q \<equiv> restrict_preds_to_vars (PDisj P Q) (lpred_vars P \<inter> lpred_vars Q)"

lemma PDisjI1:
  "P \<turnstile> PDisj P Q"
  by(auto simp: pred_entailment_def)

lemma PDisjI2:
  "Q \<turnstile> PDisj P Q"
  by(auto simp: pred_entailment_def)

lemma if_type':
  "\<Gamma> \<turnstile>\<^sub>b e \<in> t  \<Longrightarrow>
   P \<turnstile> t \<Longrightarrow>
   p = bexp_to_lpred e \<Longrightarrow>
   \<turnstile> \<Gamma>,\<S>,P +\<^sub>\<S> p {c\<^sub>1} \<Gamma>',\<S>',P' \<Longrightarrow>
   \<turnstile> \<Gamma>,\<S>,P +\<^sub>\<S> PNeg p {c\<^sub>2} \<Gamma>',\<S>',P'' \<Longrightarrow>
   bexp_readable \<S> e \<Longrightarrow> \<turnstile> \<Gamma>,\<S>,P {Stmt.If e c\<^sub>1 c\<^sub>2} \<Gamma>',\<S>',(combine_preds P' P'')"  
  apply(rule if_type, (blast intro: context_equiv_refl lpred_vars_tyenv_wellformed_pres[rule_format])+)
      apply(simp add: combine_preds_def)
      using PDisjI1 restrict_preds_to_vars_weakens pred_entailment_trans apply metis
     apply(simp add: combine_preds_def)
     using PDisjI2 restrict_preds_to_vars_weakens pred_entailment_trans apply metis
    apply(rule lpred_vars_tyenv_wellformed_pres)
    apply(simp add: combine_preds_def)
    using restrict_preds_to_vars_restricts apply blast
   apply(rule lpred_vars_tyenv_wellformed_pres)
   apply(simp add: combine_preds_def)
   using restrict_preds_to_vars_restricts apply blast
  apply assumption
  done 

end

end