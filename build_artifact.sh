#!/bin/bash

ARTIFACT_NAME=wr-compiler-itp19

set -x

tar -T FILES_${ARTIFACT_NAME} -cf ${ARTIFACT_NAME}.tar --transform "s,^,${ARTIFACT_NAME}/," --transform "s,README_${ARTIFACT_NAME},README," --transform "s,FILES_${ARTIFACT_NAME},FILES," --transform "s,ROOTS_${ARTIFACT_NAME},ROOTS," --transform "s,ROOT_${ARTIFACT_NAME},ROOT,"
